﻿using BusinessLayer.Services.Lekovi;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Lekovi
{
    public partial class PregledPoMestimaTroskaReportForm : Form
    {
        private readonly DateTime _datumOd;
        private readonly DateTime _datumDo;
        public PregledPoMestimaTroskaReportForm(DateTime datumOd, DateTime datumDo)
        {
            InitializeComponent();
            _datumOd = datumOd;
            _datumDo = datumDo;
        }

        private void PregledPoMestimaTroska_Load(object sender, EventArgs e)
        {
            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("DatumOd", _datumOd.ToString("dd/MM/yyyy"), true);
            parameters[1] = new ReportParameter("DatumDo", _datumDo.ToString("dd/MM/yyyy"), true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();


            var upToCurrentDate = _datumOd.AddDays(-1);
            upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 23, 59, 59);
            var upToCurrentDate2 = _datumDo.AddDays(1);
            upToCurrentDate2 = new DateTime(upToCurrentDate2.Year, upToCurrentDate2.Month, upToCurrentDate2.Day, 0, 0, 0);

            reportDataSource.Value = zIzdatnicaService.PregledPoMestimaTroska(upToCurrentDate, upToCurrentDate2);

            // Add any parameters to the collection
            this.reportViewer1.LocalReport.SetParameters(parameters);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
