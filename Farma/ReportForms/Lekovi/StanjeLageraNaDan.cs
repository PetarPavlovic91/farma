﻿using BusinessLayer.Services.Lekovi;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Lekovi
{
    public partial class StanjeLageraNaDan : Form
    {
        private readonly DateTime _datum;
        public StanjeLageraNaDan(DateTime datum)
        {
            InitializeComponent();
            _datum = datum;
        }

        private void StanjeLageraNaDan_Load(object sender, EventArgs e)
        {
            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("DatumOd", _datum.ToString("dd/MM/yyyy"), true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            KarticaService karticaService = new KarticaService();
            var upToCurrentDate = _datum.AddDays(1);
            upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 0, 0, 0);
            reportDataSource.Value = karticaService.StanjeLageraNaDan(upToCurrentDate, true);

            // Add any parameters to the collection
            this.reportViewer1.LocalReport.SetParameters(parameters);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
