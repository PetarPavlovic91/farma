﻿using BusinessLayer.Services.Lekovi;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Lekovi
{
    public partial class TrebovanjeForm : Form
    {
        private readonly string _brDok;
        private readonly string _datum;
        private readonly string _naslov;
        public TrebovanjeForm(string brDok, string datum, string naslov)
        {
            InitializeComponent();
            _brDok = brDok;
            _datum = datum;
            _naslov = naslov;
        }

        private void TrebovanjeForm_Load(object sender, EventArgs e)
        {
            ReportParameter[] parameters = new ReportParameter[3];
            parameters[0] = new ReportParameter("BrojDokumenta", _brDok, true);
            parameters[1] = new ReportParameter("Datum", _datum, true);
            parameters[2] = new ReportParameter("Naslov", _naslov, true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
            reportDataSource.Value = sIzdatnicaService.TrebovanjeReport(_brDok);

            // Add any parameters to the collection
            this.reportViewer1.LocalReport.SetParameters(parameters);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
