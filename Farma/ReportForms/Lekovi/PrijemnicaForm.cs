﻿using BusinessLayer.Services.Lekovi;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Lekovi
{
    public partial class PrijemnicaForm : Form
    {
        private readonly string _brDok;
        private readonly string _datum;
        private readonly string _naslov;
        private readonly string _dobavljac;
        private readonly string _nazivDobavljaca;
        private readonly string _dokumentDobavljaca;
        public PrijemnicaForm(string brDok, string datum, string naslov, string dobavljac, string nazivDobavljaca, string dokumentDobavljaca)
        {
            InitializeComponent();
            _brDok = brDok;
            _datum = datum;
            _naslov = naslov;
            _dobavljac = dobavljac;
            _nazivDobavljaca = nazivDobavljaca;
            _dokumentDobavljaca = dokumentDobavljaca;
        }

        private void PrijemnicaForm_Load(object sender, EventArgs e)
        {
        }

        private void PrijemnicaForm_Load_1(object sender, EventArgs e)
        {

            ReportParameter[] parameters = new ReportParameter[6];
            parameters[0] = new ReportParameter("BrojDokumenta", _brDok, true);
            parameters[1] = new ReportParameter("Datum", _datum, true);
            parameters[2] = new ReportParameter("Naslov", _naslov, true);
            parameters[3] = new ReportParameter("Dobavljac", _dobavljac, true);
            parameters[4] = new ReportParameter("NazivDobavljaca", _nazivDobavljaca, true);
            parameters[5] = new ReportParameter("DokumentDobavljaca", _dokumentDobavljaca, true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
            reportDataSource.Value = sPrijemnicaService.PrijemnicaReport(_brDok);

            // Add any parameters to the collection
            this.reportViewer1.LocalReport.SetParameters(parameters);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
