﻿using BusinessLayer.Services.Lekovi;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Lekovi
{
    public partial class PregledPoVeterinarskimTehnicarimaReportForm : Form
    {
        private readonly DateTime dateOd;
        private readonly DateTime dateDo;

        public PregledPoVeterinarskimTehnicarimaReportForm(DateTime dateOd,DateTime dateDo)
        {
            InitializeComponent();
            this.dateOd = dateOd;
            this.dateDo = dateDo;
        }

        private void PregledPoVeterinarskimTehnicarimaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            ReportParameter[] parameters = new ReportParameter[2];
            parameters[0] = new ReportParameter("DatumOd", dateOd.ToString("dd/MM/yyyy"), true);
            parameters[1] = new ReportParameter("DatumDo", dateDo.ToString("dd/MM/yyyy"), true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();


            var upToCurrentDate = dateOd.AddDays(-1);
            upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 23, 59, 59);
            var upToCurrentDate2 = dateDo.AddDays(1);
            upToCurrentDate2 = new DateTime(upToCurrentDate2.Year, upToCurrentDate2.Month, upToCurrentDate2.Day, 0, 0, 0);

            reportDataSource.Value = zIzdatnicaService.PregledPoVeterinarskimTehnicarima(upToCurrentDate, upToCurrentDate2);

            // Add any parameters to the collection
            this.reportViewer1.LocalReport.SetParameters(parameters);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
