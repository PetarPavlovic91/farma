﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class MaticniKartonKrmaceReportForm : Form
    {
        private readonly int _krmacaId;
        public MaticniKartonKrmaceReportForm(int krmacaId)
        {
            InitializeComponent();
            _krmacaId = krmacaId;
        }

        private void MaticniKartonKrmaceReportForm_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            //ReportParameter[] parameters = new ReportParameter[3];
            //parameters[0] = new ReportParameter("BrojDokumenta", _brDok, true);
            //parameters[1] = new ReportParameter("Datum", _datum, true);
            //parameters[2] = new ReportParameter("Naslov", _naslov, true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            KartotekaService kartotekaService = new KartotekaService();
            reportDataSource.Value = kartotekaService.GetAllForKrmacaReport(_krmacaId);

            // Add any parameters to the collection
            //this.reportViewer1.LocalReport.SetParameters(parameters);
            var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
            if (dataSet1 != null)
            {
                this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
            }
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
