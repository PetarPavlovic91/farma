﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class IzvodIzGlavneMaticneEvidencijeReportForm : Form
    {
        private readonly DateTime dan;
        private readonly int paritet;
        private readonly bool isHBRB;
        private readonly bool isNB;
        private readonly bool isNerastovi;

        public IzvodIzGlavneMaticneEvidencijeReportForm(DateTime dan, int paritet, bool isHBRB, bool isNB, bool isNerastovi)
        {
            InitializeComponent();
            this.dan = dan;
            this.paritet = paritet;
            this.isHBRB = isHBRB;
            this.isNB = isNB;
            this.isNerastovi = isNerastovi;
        }

        private void IzvodIzGlavneMaticneEvidencijeReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("Dan", dan.ToShortDateString(), true);
                parameters[1] = new ReportParameter("Paritet", paritet.ToString(), true);
                var vrsta = "";
                if (isHBRB)
                {
                    vrsta = "Grla sa HB ili RB brojem";
                }
                if (isNB)
                {
                    vrsta += " Grla sa NB brojem";
                }
                if (isNerastovi)
                {
                    vrsta += " Nerastovi";
                }
                parameters[2] = new ReportParameter("Vrsta", vrsta, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = izvestajiService.IzvodIzGlavneMaticneEvidencije(dan, paritet, isHBRB, isNB, isNerastovi);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
