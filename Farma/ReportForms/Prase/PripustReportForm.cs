﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class PripustReportForm : Form
    {
        private readonly DateTime _danOd;
        private readonly DateTime _danDo;
        public PripustReportForm(DateTime danOd, DateTime danDo)
        {
            InitializeComponent();
            _danOd = danOd;
            _danDo = danDo;
        }

        private void PripustReportForm_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            //ReportParameter[] parameters = new ReportParameter[3];
            //parameters[0] = new ReportParameter("BrojDokumenta", _brDok, true);
            //parameters[1] = new ReportParameter("Datum", _datum, true);
            //parameters[2] = new ReportParameter("Naslov", _naslov, true);

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "DataSet1";
            PripustService pripustService = new PripustService();
            reportDataSource.Value = pripustService.SP_GetAllByDateReport(_danOd, _danDo);

            // Add any parameters to the collection
            //this.reportViewer1.LocalReport.SetParameters(parameters);
            this.reportViewer1.LocalReport.DataSources.RemoveAt(0);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}
