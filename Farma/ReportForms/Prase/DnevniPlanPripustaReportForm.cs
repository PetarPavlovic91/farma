﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Reports.Prase._01Pripust
{
    public partial class DnevniPlanPripustaReportForm : Form
    {
        private string _krmaca;
        public DnevniPlanPripustaReportForm(string krmaca)
        {
            InitializeComponent();
            _krmaca = krmaca;
        }

        private void DnevniPlanPripusta_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

            try {
                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("Naslov", "Dnevni Plan Pripusta", true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";
                PripustService pripustService = new PripustService();
                reportDataSource.Value = pripustService.DnevniPlanPripustaReport(DateTime.Now, _krmaca);

                // Add any parameters to the collection
                this.reportViewer1.LocalReport.SetParameters(parameters);
                //this.reportViewer1.LocalReport.DataSources.RemoveAt(0);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
