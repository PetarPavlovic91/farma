﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class GrlaBezOtvorenogPoreklaReportForm : Form
    {
        private readonly bool isPoreklo;
        private readonly string vrstaGrla;

        public GrlaBezOtvorenogPoreklaReportForm(bool isPoreklo, string vrstaGrla)
        {
            InitializeComponent();
            this.isPoreklo = isPoreklo;
            this.vrstaGrla = vrstaGrla;
        }

        private void GrlaBezOtvorenogPoreklaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("VrstaGrla", vrstaGrla, true);
                var tip = "Pedigre";
                if (isPoreklo)
                {
                    tip = "Poreklo";
                }
                parameters[1] = new ReportParameter("Tip", tip, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = kartotekaService.GrlaBezOtvorenogPorekla(isPoreklo, vrstaGrla);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
