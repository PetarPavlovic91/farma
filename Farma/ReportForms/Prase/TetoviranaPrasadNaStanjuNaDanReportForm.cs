﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class TetoviranaPrasadNaStanjuNaDanReportForm : Form
    {
        private readonly DateTime dan;
        private readonly int danaOd;
        private readonly string pol;

        public TetoviranaPrasadNaStanjuNaDanReportForm(DateTime dan, int danaOd, string pol)
        {
            InitializeComponent();
            this.dan = dan;
            this.danaOd = danaOd;
            this.pol = pol;
        }

        private void TetoviranaPrasadNaStanjuNaDanReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("Dan", dan.ToShortDateString(), true);
                parameters[1] = new ReportParameter("Vrsta", pol, true);
                parameters[2] = new ReportParameter("BrojDana", danaOd.ToString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = kartotekaService.TetoviranaPrasadNaStanjuNaDan(dan, danaOd, pol);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
