﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class ZahtevZaRegistracijuZenskaGrlaReportForm : Form
    {
        private readonly DateTime danOd;
        private readonly DateTime danDo;
        private readonly string rasa;
        private readonly string adresa;
        private readonly int paritet;

        public ZahtevZaRegistracijuZenskaGrlaReportForm(DateTime danOd, DateTime danDo, string rasa, string adresa, int paritet)
        {
            InitializeComponent();
            this.danOd = danOd;
            this.danDo = danDo;
            this.rasa = rasa;
            this.adresa = adresa;
            this.paritet = paritet;
        }

        private void ZahtevZaRegistracijuZenskaGrlaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                ReportParameter[] parameters = new ReportParameter[5];
                parameters[0] = new ReportParameter("DanOd", danOd.ToShortDateString(), true);
                parameters[1] = new ReportParameter("DanDo", danDo.ToShortDateString(), true);
                parameters[2] = new ReportParameter("Rasa", rasa, true);
                parameters[3] = new ReportParameter("Adresa", adresa, true);
                parameters[4] = new ReportParameter("Paritet", paritet.ToString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = izvestajiService.ZahtevZaRegistracijuZenskaGrla(danOd,danDo,rasa,paritet);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
