﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class KrmaceZaZalucenjeReportForm : Form
    {
        private readonly int brDana;
        private readonly string sort;

        public KrmaceZaZalucenjeReportForm(int brDana, string sort)
        {
            InitializeComponent();
            this.brDana = brDana;
            this.sort = sort;
        }

        private void KrmaceZaZalucenjeReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                ZalucenjeService zalucenjeService = new ZalucenjeService();

                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("BRojDana", brDana.ToString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = zalucenjeService.KrmaceZaZalucenje(brDana, sort);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
