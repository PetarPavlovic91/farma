﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class NIIManjePrasadiUMPrasenjaReportForm : Form
    {
        private readonly int m;
        private readonly int n;
        private readonly string rasa;
        private readonly bool prvih;

        public NIIManjePrasadiUMPrasenjaReportForm(int m, int n, string rasa,bool prvih)
        {
            InitializeComponent();
            this.m = m;
            this.n = n;
            this.rasa = rasa;
            this.prvih = prvih;
        }

        private void NIIManjePrasadiUMPrasenjaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();

                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("N", n.ToString(), true);
                parameters[1] = new ReportParameter("M", m.ToString(), true);
                parameters[2] = new ReportParameter("Rasa", rasa, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                //reportDataSource.Value = prasenjeService.NIIManjePrasadiUMPrasenja(m, n, rasa, prvih);
                //var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                //if (dataSet1 != null)
                //{
                //    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                //}
                //this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
