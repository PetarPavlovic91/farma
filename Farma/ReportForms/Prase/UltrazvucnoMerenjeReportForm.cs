﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class UltrazvucnoMerenjeReportForm : Form
    {
        private readonly DateTime dan;
        private readonly bool isTest;

        public UltrazvucnoMerenjeReportForm(DateTime dan, bool isTest)
        {
            InitializeComponent();
            this.dan = dan;
            this.isTest = isTest;
        }

        private void UltrazvucnoMerenjeReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("Dan", dan.ToShortDateString(), true);
                var vrsta = "Odabir";
                if (isTest)
                {
                    vrsta = "Test";
                }
                parameters[1] = new ReportParameter("Vrsta", vrsta, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = izvestajiService.UltrazvucnoMerenje(dan,isTest);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
