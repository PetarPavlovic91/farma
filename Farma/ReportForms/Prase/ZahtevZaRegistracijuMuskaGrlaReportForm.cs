﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class ZahtevZaRegistracijuMuskaGrlaReportForm : Form
    {
        private readonly DateTime danOd;
        private readonly string adresa;

        public ZahtevZaRegistracijuMuskaGrlaReportForm(DateTime danOd, string adresa)
        {
            InitializeComponent();
            this.danOd = danOd;
            this.adresa = adresa;
        }

        private void ZahtevZaRegistracijuMuskaGrlaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("Dan", danOd.ToShortDateString(), true);
                parameters[1] = new ReportParameter("Adresa", adresa, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = izvestajiService.ZahtevZaRegistracijuMuskaGrla(danOd);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
