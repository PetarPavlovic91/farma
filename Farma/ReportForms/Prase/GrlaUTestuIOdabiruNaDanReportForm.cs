﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class GrlaUTestuIOdabiruNaDanReportForm : Form
    {
        private readonly DateTime dan;
        private readonly int danaOd;
        private readonly bool testirane;
        private readonly bool nazimice;

        public GrlaUTestuIOdabiruNaDanReportForm(DateTime dan, int danaOd, bool nazimice, bool testirane)
        {
            InitializeComponent();
            this.dan = dan;
            this.danaOd = danaOd;
            this.testirane = testirane;
            this.nazimice = nazimice;
        }

        private void GrlaUTestuIOdabiruNaDanReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("Dan", dan.ToShortDateString(), true);

                var vrsta = "Testirane nazimice";
                if (testirane && !nazimice)
                {
                    vrsta = "Testirani nerastovi";
                }
                else if (!testirane && nazimice)
                {
                    vrsta = "Odabrane nazimice";
                }
                else if (!testirane && !nazimice)
                {
                    vrsta = "Odabrani nerastovi";
                }

                parameters[1] = new ReportParameter("Vrsta", vrsta, true);
                parameters[2] = new ReportParameter("BrojDana", danaOd.ToString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = kartotekaService.GrlaUTestuIOdabiruNaDan(dan, danaOd, testirane, nazimice);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
