﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class GrlaBezPotpunogPoreklaReportForm : Form
    {
        private readonly bool isPoreklo;
        private readonly string vrstaGrla;
        private readonly bool aktivan;

        public GrlaBezPotpunogPoreklaReportForm(bool isPoreklo, string vrstaGrla, bool aktivan)
        {
            InitializeComponent();
            this.isPoreklo = isPoreklo;
            this.vrstaGrla = vrstaGrla;
            this.aktivan = aktivan;
        }

        private void GrlaBezPotpunogPoreklaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("VrstaGrla", vrstaGrla, true);
                var tip = "Pedigre";
                if (isPoreklo)
                {
                    tip = "Poreklo";
                }
                parameters[1] = new ReportParameter("Tip", tip, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = kartotekaService.GrlaBezPotpunogPorekla(isPoreklo, vrstaGrla, aktivan);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
