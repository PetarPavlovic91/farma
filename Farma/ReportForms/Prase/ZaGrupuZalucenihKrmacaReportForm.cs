﻿using BusnissLayer.Services.Prase;
using Farma.Reports.Prase._01Pripust;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class ZaGrupuZalucenihKrmacaReportForm : Form
    {
        private DateTime _datumZalucenja;
        public ZaGrupuZalucenihKrmacaReportForm(DateTime datumZalucenja)
        {
            InitializeComponent();
            _datumZalucenja = datumZalucenja;
        }

        private void ZaGrupuZalucenihKrmacaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

            try
            {
                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("Naslov", "Dnevni Plan Pripusta Za Grupu Zalucenih Krmaca", true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";
                PripustService pripustService = new PripustService();
                reportDataSource.Value = pripustService.PlanPripustaZaGrupuZalucenihKrmacaReport(_datumZalucenja);

                // Add any parameters to the collection
                this.reportViewer1.LocalReport.SetParameters(parameters);
                //this.reportViewer1.LocalReport.DataSources.RemoveAt(0);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
