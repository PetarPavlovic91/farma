﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class RezimeSmotreRadniSpisakReportForm : Form
    {
        private readonly string rasa;
        private readonly string objekat;

        public RezimeSmotreRadniSpisakReportForm(string rasa,string objekat)
        {
            InitializeComponent();
            this.rasa = rasa;
            this.objekat = objekat;
        }

        private void RezimeSmotreRadniSpisakReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();

                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("Rasa", rasa, true);
                parameters[1] = new ReportParameter("Objekat", objekat, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = smotraGrlaService.RezimeSMotreRadniSpisakZaKrmace(rasa);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
