﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class DrugaDetekcijaSuprasnostiReportForm : Form
    {
        private readonly int _danOd;
        private readonly int _danDo;
        private readonly string _naslov;
        private readonly string _sort;

        public DrugaDetekcijaSuprasnostiReportForm(int danOd, int danDo, string naslov, string sort)
        {
            InitializeComponent();
            _danOd = danOd;
            _danDo = danDo;
            _naslov = naslov;
            _sort = sort;
        }

        private void DrugaDetekcijaSuprasnostiReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                OstaleFazeService ostaleFazeService = new OstaleFazeService();

                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("DanaOd", _danOd.ToString(), true);
                parameters[1] = new ReportParameter("DanaDo", _danDo.ToString(), true);
                parameters[2] = new ReportParameter("Naslov", _naslov, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = ostaleFazeService.DrugaDetekcijaSuprasnosti(_danOd, _danDo, _sort);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
