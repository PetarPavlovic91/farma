﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class KomisijskiZapisnikZaNerastoveReportForm : Form
    {
        private readonly DateTime dateOd;
        private readonly DateTime dateDo;
        private readonly DateTime datumSmotre;
        private readonly bool sviNerastovi;
        private readonly string mesto;
        private readonly string rasa;

        public KomisijskiZapisnikZaNerastoveReportForm(DateTime dateOd, DateTime dateDo, DateTime datumSmotre, bool sviNerastovi, string mesto, string rasa)
        {
            InitializeComponent();
            this.dateOd = dateOd;
            this.dateDo = dateDo;
            this.datumSmotre = datumSmotre;
            this.sviNerastovi = sviNerastovi;
            this.mesto = mesto;
            this.rasa = rasa;
        }

        private void KomisijskiZapisnikZaNerastoveReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();

                ReportParameter[] parameters = new ReportParameter[5];
                parameters[0] = new ReportParameter("DateOd", dateOd.ToShortDateString(), true);
                parameters[1] = new ReportParameter("DateDo", dateDo.ToShortDateString(), true);
                parameters[2] = new ReportParameter("DatumSmotre", datumSmotre.ToShortDateString(), true);
                parameters[3] = new ReportParameter("Mesto", mesto, true);
                parameters[4] = new ReportParameter("Rasa", rasa, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = smotraGrlaService.KomisijskiZapisnikZaNerastove(rasa, datumSmotre, dateOd, dateDo, sviNerastovi);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
