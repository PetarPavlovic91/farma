﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class RegistarPrasadiReportForm : Form
    {
        private readonly DateTime dateOd;
        private readonly DateTime dateDo;
        private readonly string pol;

        public RegistarPrasadiReportForm(DateTime dateOd, DateTime dateDo, string pol)
        {
            InitializeComponent();
            this.dateOd = dateOd;
            this.dateDo = dateDo;
            this.pol = pol;
        }

        private void RegistarPrasadiReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();

                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("DateOd", dateOd.ToShortDateString(), true);
                parameters[1] = new ReportParameter("DateDo", dateDo.ToShortDateString(), true);
                parameters[2] = new ReportParameter("Pol", pol, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = kartotekaService.RegistarPrasadi(dateOd, dateDo, "");
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
