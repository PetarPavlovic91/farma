﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class KomisijskiZapisnikZaKrmaceDopunaReportForm : Form
    {
        private readonly DateTime datumSmotre;
        private readonly string mesto;
        private readonly string rasa;
        private readonly DateTime dateOd;
        private readonly DateTime dateDo;
        private readonly int paritet;

        public KomisijskiZapisnikZaKrmaceDopunaReportForm(DateTime datumSmotre, string mesto, string rasa, DateTime dateOd, DateTime dateDo, int paritet)
        {
            InitializeComponent();
            this.datumSmotre = datumSmotre;
            this.mesto = mesto;
            this.rasa = rasa;
            this.dateOd = dateOd;
            this.dateDo = dateDo;
            this.paritet = paritet;
        }

        private void KomisijskiZapisnikZaKrmaceDopunaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                ReportParameter[] parameters = new ReportParameter[6];
                parameters[0] = new ReportParameter("DatumSmotre", datumSmotre.ToShortDateString(), true);
                parameters[1] = new ReportParameter("Mesto", mesto, true);
                parameters[2] = new ReportParameter("Rasa", rasa, true);
                parameters[3] = new ReportParameter("DateOd", dateOd.ToShortDateString(), true);
                parameters[4] = new ReportParameter("DateDo", dateDo.ToShortDateString(), true);
                parameters[5] = new ReportParameter("Paritet", paritet.ToString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = izvestajiService.KomisijskiZapisnikZaKrmace(datumSmotre, rasa);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
