﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class TestKrmacaNaPlodnost_SI2ReportForm : Form
    {
        private readonly string rasa;

        public TestKrmacaNaPlodnost_SI2ReportForm(string rasa)
        {
            InitializeComponent();
            this.rasa = rasa;
        }

        private void TestKrmacaNaPlodnost_SI2ReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                SelekcijaService selekcijaService = new SelekcijaService();

                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("Rasa", rasa, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = selekcijaService.TestKrmacaNaPlodnost_SI2(rasa);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
