﻿using BusnissLayer.ViewModels.Prase.Pripust.KontrolaProduktivnostiNerastova;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class BioloskiTestMladihNerastovaReportForm : Form
    {
        private List<BioloskiTestMladihNerastovaVM> _nerastovi;
        private DateTime _odDate;
        private DateTime _doDate;
        public BioloskiTestMladihNerastovaReportForm(List<BioloskiTestMladihNerastovaVM> nerastovi, DateTime odDate, DateTime doDate)
        {
            InitializeComponent();
            _nerastovi = nerastovi;
            _odDate = odDate;
            _doDate = doDate;
        }

        private void BioloskiTestMladihNerastovaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                ReportParameter[] parameters = new ReportParameter[2];
                parameters[0] = new ReportParameter("DateOd", _odDate.ToShortDateString(), true);
                parameters[1] = new ReportParameter("DateDo", _doDate.ToShortDateString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                reportDataSource.Value = _nerastovi;
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
