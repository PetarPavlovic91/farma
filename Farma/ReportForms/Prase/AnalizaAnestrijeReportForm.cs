﻿using BusnissLayer.ViewModels.Prase._01Pripust.ReportModels;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class AnalizaAnestrijeReportForm : Form
    {
        private List<AnalizaAnestrijeVM> _analiza;
        private DateTime _odDate;
        private DateTime _doDate;
        private string _naslov;
        public AnalizaAnestrijeReportForm(List<AnalizaAnestrijeVM> analiza, DateTime odDate, DateTime doDate, string naslov)
        {
            InitializeComponent();
            _analiza = analiza;
            _odDate = odDate;
            _doDate = doDate;
            _naslov = naslov;
        }

        private void AnalizaAnestrijeReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

            try
            {
                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("Naslov", _naslov, true);
                parameters[1] = new ReportParameter("DateOd", _odDate.ToShortDateString(), true);
                parameters[2] = new ReportParameter("DateDo", _doDate.ToShortDateString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                reportDataSource.Value = _analiza;
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
