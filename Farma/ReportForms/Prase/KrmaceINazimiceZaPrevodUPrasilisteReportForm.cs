﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class KrmaceINazimiceZaPrevodUPrasilisteReportForm : Form
    {

        private readonly int _brDana;
        private readonly string _sort;
        public KrmaceINazimiceZaPrevodUPrasilisteReportForm(int brDana, string sort)
        {
            InitializeComponent();
            _brDana = brDana;
            _sort = sort;
        }

        private void KrmaceINazimiceZaPrevodUPrasilisteReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();

                ReportParameter[] parameters = new ReportParameter[1];
                parameters[0] = new ReportParameter("BrojDana", _brDana.ToString(), true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = prasenjeService.KrmaceINazimiceZaPrevodUPrasiliste(_brDana, _sort);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
