﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class AktivneKrmaceNaDanRasnaStrukturaReportForm : Form
    {
        private readonly DateTime date;
        private readonly string rasa;
        private readonly bool isNazimica;

        public AktivneKrmaceNaDanRasnaStrukturaReportForm(DateTime date, string rasa, bool isNazimica)
        {
            InitializeComponent();
            this.date = date;
            this.rasa = rasa;
            this.isNazimica = isNazimica;
        }

        private void AktivneKrmaceNaDanRasnaStrukturaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();

                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("Dan", date.ToShortDateString(), true);
                parameters[1] = new ReportParameter("Rasa", rasa, true);
                var vrsta = "Krmaca";
                if (isNazimica)
                {
                    vrsta = "Nazimica";
                }
                parameters[2] = new ReportParameter("Vrsta", vrsta, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = kartotekaService.AktivneKrmaceNaDanRasnaStruktura(date, rasa, isNazimica);
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
