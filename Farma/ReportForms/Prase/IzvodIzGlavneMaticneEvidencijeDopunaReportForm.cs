﻿using BusnissLayer.Services.Prase;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.ReportForms.Prase
{
    public partial class IzvodIzGlavneMaticneEvidencijeDopunaReportForm : Form
    {
        private readonly DateTime dan;
        private readonly int paritet;
        private readonly bool isHBRB;
        private readonly bool isNB;
        private readonly DateTime prasenjeOd;
        private readonly DateTime prasenjeDo;

        public IzvodIzGlavneMaticneEvidencijeDopunaReportForm(DateTime dan, int paritet, bool isHBRB, bool isNB, DateTime prasenjeOd, DateTime prasenjeDo)
        {
            InitializeComponent();
            this.dan = dan;
            this.paritet = paritet;
            this.isHBRB = isHBRB;
            this.isNB = isNB;
            this.prasenjeOd = prasenjeOd;
            this.prasenjeDo = prasenjeDo;
        }

        private void IzvodIzGlavneMaticneEvidencijeDopunaReportForm_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                ReportParameter[] parameters = new ReportParameter[3];
                parameters[0] = new ReportParameter("Dan", dan.ToShortDateString(), true);
                parameters[1] = new ReportParameter("Paritet", paritet.ToString(), true);
                var vrsta = "";
                if (isHBRB)
                {
                    vrsta = "Grla sa HB ili RB brojem";
                }
                if (isNB)
                {
                    vrsta += " Grla sa NB brojem";
                }
                parameters[2] = new ReportParameter("Vrsta", vrsta, true);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Name = "DataSet1";

                this.reportViewer1.LocalReport.SetParameters(parameters);

                reportDataSource.Value = izvestajiService.IzvodIzGlavneMaticneEvidencijeDopuna(dan, paritet, isHBRB, isNB, prasenjeOd, prasenjeDo, "Izvestaji.IzvodIzGlavneMaticneEvidencijeDopuna");
                var dataSet1 = this.reportViewer1.LocalReport.DataSources.FirstOrDefault(x => x.Name == "DataSet1");
                if (dataSet1 != null)
                {
                    this.reportViewer1.LocalReport.DataSources.Remove(dataSet1);
                }
                this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
                this.reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
