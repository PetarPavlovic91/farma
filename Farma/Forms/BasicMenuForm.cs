﻿using Farma.Forms;
using Farma.Forms.Prase;
using Farma.Forms.Prase._01Pripusti._02PregledPodataka;
using Farma.Forms.Prase._04Iskljucenje._01EvidencijaIskljucenja;
using Farma.Forms.Prase._04Iskljucenje._02PregledPodataka;
using Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla;
using Farma.Forms.Prase._07Katroteka._03Krmace._02AktivneKrmaceNaDan;
using Farma.Forms.Prase._07Katroteka._03Krmace._03PregledHBBrojevaKrmaca;
using Farma.Forms.Prase._07Katroteka._03Krmace._05PregledPotomstvaPoMajci;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._01KatalogNerastova;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._02MaticniProizvodniKartonNerasta;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._03AnalizaPotomstvaPoOcu;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._04NerastoviNaDan;
using Farma.Forms.Prase._07Katroteka._05Nazimice;
using Farma.Forms.Prase._07Katroteka._06OdabirITest;
using Farma.Forms.Prase._07Katroteka._07RegistarPrasadi;
using Farma.Forms.Prase._09SmotraGrla._01EvidencijaPodatakaSaSmotreGrla;
using Farma.Forms.Prase._09SmotraGrla._02RezimeSmotre;
using Farma.Forms.Prase._09SmotraGrla._04SmotraKrmaca;
using Farma.Forms.Prase._09SmotraGrla._05SmotraNazimica;
using Farma.Forms.Prase._09SmotraGrla._06SmotraMladihNerastova;
using Farma.Forms.Prase._09SmotraGrla._07Analize;
using Farma.Forms.Prase.Krmaca;
using Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja;
using Farma.Forms.Prase.Pripusti._01EvidencijaPripusta;
using Farma.Forms.Prase.Pripusti._02PregledPodataka;
using Farma.Forms.Prase.Zalucenje._01EvidencijaZalucenja;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm
{
    public partial class BasicMenuForm : Form
    {
        //private Form _loginFrom;
        //public BasicMenuForm(Form loginFrom)
        public BasicMenuForm()
        {
            //_loginFrom = loginFrom;
            InitializeComponent();
        }


        private void BasicMenuForm_FormClosed(object sender, FormClosedEventArgs e)
        {
           // _loginFrom.Close();
        }

        private void buttonLekoviApp_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            var lekoviForm = new LekoviForm(this);
            lekoviForm.TopLevel = true;
            lekoviForm.Dock = DockStyle.Fill;
            //lekoviForm.BringToFront();
            lekoviForm.Show();
        }

        private void buttonGlavna_Click(object sender, EventArgs e)
        {
            //var lekoviForm = new TrebovanjeForm("2/22", "10/10/2021", "Izdatnica");
            var praseForm = new PraseForm();
            praseForm.TopLevel = true;
            praseForm.Dock = DockStyle.Fill;
            praseForm.BringToFront();
            praseForm.Show();
        }

        private void buttonFarma_Click(object sender, EventArgs e)
        {
            var praseForm = new ListForm();
            praseForm.TopLevel = true;
            praseForm.Dock = DockStyle.Fill;
            praseForm.BringToFront();
            praseForm.Show();
        }
    }
}
