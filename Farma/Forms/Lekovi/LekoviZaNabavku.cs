﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class LekoviZaNabavku : Form
    {
        public LekoviZaNabavku()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void LekoviZaNabavku_Load(object sender, EventArgs e)
        {
            try
            {
                ArtikliService artikliService = new ArtikliService();
                dataGridView1.DataSource = artikliService.LekoviZaNabavku();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
