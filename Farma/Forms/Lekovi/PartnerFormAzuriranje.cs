﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class PartnerFormAzuriranje : Form
    {
        public PartnerFormAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void PartnerFormAzuriranje_Load(object sender, EventArgs e)
        {
            PartnerService partnerService = new PartnerService();
            dataGridView1.DataSource = partnerService.GetAll();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    PartnerService partnerService = new PartnerService();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var partner = partnerService.GetById(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = partner.SIFPP;
                    textBox4.Text = partner.Naziv;
                    textBox8.Text = partner.ZiroRacun;
                    textBox7.Text = partner.Telefon;
                    textBox1.Text = partner.Saradnik;
                    textBox2.Text = partner.Mesto;
                    textBox3.Text = partner.Adresa;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox5.Text = "";
            textBox4.Text = "";
            textBox8.Text = "";
            textBox7.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
            }
            else
            {
                PartnerService partnerService = new PartnerService();
                ReturnObject result = partnerService.Delete(int.Parse(textBox6.Text));
                if (result.Success)
                {
                    dataGridView1.DataSource = partnerService.GetAll();
                    textBox6.Text = "";
                    textBox5.Text = "";
                    textBox4.Text = "";
                    textBox8.Text = "";
                    textBox7.Text = "";
                    textBox1.Text = "";
                }
                else
                {
                    MessageBox.Show("Prvo morate izbrisati sve vezano za ovog poslovnog partnera. Greska: " + result.Message);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                PartnerVM partnerVM = new PartnerVM() {
                    Saradnik = textBox1.Text,
                    Telefon = textBox7.Text,
                    ZiroRacun = textBox8.Text,
                    SIFPP = textBox5.Text,
                    Naziv = textBox4.Text,
                    Mesto = textBox2.Text,
                    Adresa = textBox3.Text
                };
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    PartnerService partnerService = new PartnerService();
                    var result = partnerService.Save(partnerVM);
                    if (result.Success)
                    {
                        textBox6.Text = result.Id.ToString();
                        dataGridView1.DataSource = partnerService.GetAll();
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Trenutno ne podrzavamo izmenu podataka");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
