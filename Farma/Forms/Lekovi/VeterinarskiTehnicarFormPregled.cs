﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class VeterinarskiTehnicarFormPregled : Form
    {
        public VeterinarskiTehnicarFormPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void VeterinarskiTehnicarFormPregled_Load(object sender, EventArgs e)
        {

            VeterinarskiTehnicarService veterinarskiTehnicarService = new VeterinarskiTehnicarService();
            dataGridView1.DataSource = veterinarskiTehnicarService.GetAll();
        }
    }
}
