﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class AzuriranjeVrsteArtiklForm : Form
    {
        public AzuriranjeVrsteArtiklForm()
        {
            InitializeComponent();
        }

        private void dataGridViewPregledVrsteArtikla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void AzuriranjeVrsteArtiklForm_Load(object sender, EventArgs e)
        {
            dataGridViewPregledVrsteArtikla.DefaultCellStyle.ForeColor = Color.Black;
            VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
            dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
            var textForSearch = textBoxPretraga.Text;
            if (!string.IsNullOrWhiteSpace(textForSearch))
            {
                dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetFiltered(textForSearch);
            }
            else
            {
                dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetAll();
            }
        }

        private void buttonDodajNovi_Click(object sender, EventArgs e)
        {
            textBoxId.Text = "";
            textBoxNaziv.Text = "";
            textBoxVGP.Text = "";
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            VrsteArtiklaVM vrsteArtiklaVM = new VrsteArtiklaVM();
            if (string.IsNullOrEmpty(textBoxId.Text))
            {
                vrsteArtiklaVM.Id = 0;
            }
            else
            {
                vrsteArtiklaVM.Id = int.Parse(textBoxId.Text);
            }
            vrsteArtiklaVM.VGP = textBoxVGP.Text;
            vrsteArtiklaVM.Naziv = textBoxNaziv.Text;

            VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
            var result = vrsteArtiklaService.Save(vrsteArtiklaVM);
            if (result.Success)
            {
                textBoxId.Text = result.Id.ToString();
            }
            dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetAll();
        }

        private void buttonIzbrisi_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxId.Text))
            {
                MessageBox.Show("Neuspesno! Ne postoji Id.");
            }
            else
            {
                VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
                var result = vrsteArtiklaService.Delete(int.Parse(textBoxId.Text));
                if (result)
                {
                    textBoxId.Text = "";
                    textBoxNaziv.Text = "";
                    textBoxVGP.Text = "";
                    dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetAll();
                    MessageBox.Show("Uspesno izbrisan");
                }
                else
                {
                    MessageBox.Show("Neuspesno! Prvo morate izbrisati sve vezano za ovu vrstu artikla. ");
                }
            }
        }

        private void dataGridViewPregledVrsteArtikla_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
                    var id = (int)dataGridViewPregledVrsteArtikla.Rows[e.RowIndex].Cells[0].Value;
                    var vrstaArtikla = vrsteArtiklaService.GetById(id);
                    textBoxId.Text = id.ToString();
                    textBoxNaziv.Text = vrstaArtikla.Naziv;
                    textBoxVGP.Text = vrstaArtikla.VGP;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
