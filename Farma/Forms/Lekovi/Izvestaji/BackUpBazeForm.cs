﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi.Izvestaji
{
    public partial class BackUpBazeForm : Form
    {
        public BackUpBazeForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                var result = smotraGrlaService.BackupBaze();
                if (result)
                {
                    MessageBox.Show("Uspesno zavrsen backup");
                }
                else
                {
                    MessageBox.Show("Greska u toku backupa baze.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Greska u toku backupa baze. Detalji: " + ex.Message);
            }
        }
    }
}
