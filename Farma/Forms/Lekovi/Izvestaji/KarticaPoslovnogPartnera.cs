﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi.Izvestaji
{
    public partial class KarticaPoslovnogPartnera : Form
    {
        public KarticaPoslovnogPartnera()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            dateTimePicker1.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dateTimePicker2.Value = DateTime.Now;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                PartnerService partnerService = new PartnerService();
                dataGridView2.DataSource = partnerService.GetAllShortVM(textBox2.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
                DateTime from = Convert.ToDateTime(dateTimePicker1.Value);
                DateTime to = Convert.ToDateTime(dateTimePicker2.Value);
                if (from > to)
                {
                    MessageBox.Show("Prvi datum mora biti manji ili jednak drugom");
                }

                dataGridView1.DataSource = zPrijemnicaService.KarticaPoslovnogPartnera(from, to, textBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView2_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                textBox1.Text = dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString();
            }
        }
    }
}
