﻿using BusinessLayer.Services.Lekovi;
using Farm.Utils;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi.Izvestaji
{
    public partial class PregledPoVeterinarskimTehnicarima : Form
    {
        public PregledPoVeterinarskimTehnicarima()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dateTimePicker1.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dateTimePicker2.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime from = Convert.ToDateTime(dateTimePicker1.Value);
                DateTime to = Convert.ToDateTime(dateTimePicker2.Value);
                if (from > to)
                {
                    MessageBox.Show("Prvi datum mora biti manji ili jednak drugom");
                }
                ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();

                var upToCurrentDate = from.AddDays(-1);
                upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 23, 59, 59);
                var upToCurrentDate2 = to.AddDays(1);
                upToCurrentDate2 = new DateTime(upToCurrentDate2.Year, upToCurrentDate2.Month, upToCurrentDate2.Day, 0, 0, 0);
                dataGridView1.DataSource = zIzdatnicaService.PregledPoVeterinarskimTehnicarima(upToCurrentDate, upToCurrentDate2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPoVeterinarskimTehnicarimaReportForm(dateTimePicker1.Value, dateTimePicker2.Value));
        }
    }
}
