﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi.Izvestaji
{
    public partial class PoPoslovnimPartnerimaForm : Form
    {
        public PoPoslovnimPartnerimaForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dateTimePicker1.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dateTimePicker2.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime from = Convert.ToDateTime(dateTimePicker1.Value);
                DateTime to = Convert.ToDateTime(dateTimePicker2.Value);
                if (from > to)
                {
                    MessageBox.Show("Prvi datum mora biti manji ili jednak drugom");
                }
                ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
                dataGridView1.DataSource = zPrijemnicaService.PregledPoPoslovnimPartnerima(from, to);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
