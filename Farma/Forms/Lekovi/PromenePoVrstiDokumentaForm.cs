﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class PromenePoVrstiDokumentaForm : Form
    {
        public PromenePoVrstiDokumentaForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dateTimePicker1.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dateTimePicker2.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime from = Convert.ToDateTime(dateTimePicker1.Value);
                DateTime to = Convert.ToDateTime(dateTimePicker2.Value);
                if (from > to)
                {
                    MessageBox.Show("Prvi datum mora biti manji ili jednak drugom");
                }
                if (radioButton1.Checked)
                {
                    ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
                    dataGridView1.DataSource = zPrijemnicaService.PregledPoVrstiDokumenta(from, to);
                }
                else if (radioButton2.Checked)
                {
                    ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
                    dataGridView1.DataSource = zIzdatnicaService.PregledPoVrstiDokumenta(from, to);
                }
                else
                {
                    dataGridView1.DataSource = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
