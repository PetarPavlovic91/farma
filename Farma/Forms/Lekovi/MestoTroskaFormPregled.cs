﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class MestoTroskaFormPregled : Form
    {
        public MestoTroskaFormPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void MestoTroskaFormPregled_Load(object sender, EventArgs e)
        {
            VrstaTroskaService vrstaTroskaService = new VrstaTroskaService();
            dataGridView1.DataSource = vrstaTroskaService.GetAll();
        }
    }
}
