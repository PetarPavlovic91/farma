﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class PrijemnicaLekovaForm : Form
    {
        public PrijemnicaLekovaForm()
        {
            InitializeComponent();
        }

        private Form activeForm = null;

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new PrijemnicaLekovaAzuriranjeForm(), activeForm, panel3);
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new PrijemnicaLekovaPregledForm(), activeForm, panel3);
        }
    }
}
