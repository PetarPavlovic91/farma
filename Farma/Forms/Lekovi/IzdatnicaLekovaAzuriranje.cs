﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class IzdatnicaLekovaAzuriranje : Form
    {
        public IzdatnicaLekovaAzuriranje()
        {
            InitializeComponent();
            ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
            dataGridView1.DataSource = zIzdatnicaService.GetAll();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView3.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView4.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();

                    var brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var allByDok = sIzdatnicaService.GetAll(brDok);
                    dataGridView2.DataSource = allByDok;
                    if (allByDok.Count == 0)
                    {
                        try
                        {
                            textBox5.Text = sIzdatnicaService.GetNewRB(id.ToString()).ToString();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Prvo odaberite za koju izdatnicu zelite da unesete podatke");
                        }
                        textBox6.Text = "";
                        textBox4.Text = "";
                        textBox8.Text = "";
                        textBox7.Text = "";
                        textBox10.Text = "";
                    }

                    ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
                    var zIzdatnica = zIzdatnicaService.GetById(id);
                    textBox3.Text = id.ToString();
                    textBox2.Text = zIzdatnica.Br_dok;
                    dateTimePicker1.Value = zIzdatnica.Datum;
                    //textBox20.Text = zIzdatnica.Vet;
                    dateTimePicker2.Value = zIzdatnica.DatumTrebovanja;
                    textBox9.Text = zIzdatnica.BR_TREB;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            try
            {
                ZIzdatnicaVM zIzdatnicaVM = new ZIzdatnicaVM()
                {
                    Id = string.IsNullOrEmpty(textBox3.Text) ? 0 : int.Parse(textBox3.Text),
                    Br_dok = textBox2.Text,
                    Datum = dateTimePicker1.Value,
                    DatumTrebovanja = dateTimePicker2.Value,
                    BR_TREB = textBox9.Text,
                    //Vet = textBox20.Text
                };
                ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
                var result = zIzdatnicaService.Save(zIzdatnicaVM);
                if (result.Success)
                {
                    textBox3.Text = result.Id.ToString();
                }
                else
                {
                    MessageBox.Show(result.Message);
                }
                dataGridView1.DataSource = zIzdatnicaService.GetAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonDodajNovi_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            textBox2.Text = "";
            dateTimePicker1.Value = DateTime.Now;
            dateTimePicker2.Value = DateTime.Now;
            textBox9.Text = "";
            //textBox20.Text = "";
    }

        private void buttonIzbrisi_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
            }
            else
            {
                ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
                var result = zIzdatnicaService.Delete(int.Parse(textBox3.Text));
                if (result.Success)
                {
                    dataGridView1.DataSource = zIzdatnicaService.GetAll();
                    textBox3.Text = "";
                    textBox2.Text = "";
                    dateTimePicker1.Value = DateTime.Now;
                    //textBox20.Text = "";
                    textBox9.Text = "";
                    dateTimePicker2.Value = DateTime.Now;
                }
                else
                {
                    MessageBox.Show(result.Message);
                }
            }
        }

        private void dataGridView2_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                    var id = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value;
                    var sIzdatnica = sIzdatnicaService.GetById(id);
                    if (sIzdatnica != null)
                    {
                        textBox5.Text = sIzdatnica.Rb.ToString();
                        textBox4.Text = sIzdatnica.Cena.ToString();
                        textBox8.Text = sIzdatnica.Kolicina.ToString();
                        textBox7.Text = sIzdatnica.Lek;
                        textBox10.Text = sIzdatnica.Vt;
                        textBox20.Text = sIzdatnica.VeterinarskiTehnicar;
                    }
                    textBox6.Text = id.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView2_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                    var id = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value;
                    var sIzdatnica = sIzdatnicaService.GetById(id);
                    textBox6.Text = id.ToString();
                    if (sIzdatnica != null)
                    {
                        textBox5.Text = sIzdatnica.Rb.ToString();
                        textBox4.Text = sIzdatnica.Cena.ToString();
                        textBox8.Text = sIzdatnica.Kolicina.ToString();
                        textBox7.Text = sIzdatnica.Lek;
                        textBox10.Text = sIzdatnica.Vt;
                        textBox20.Text = sIzdatnica.VeterinarskiTehnicar;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                textBox5.Text = sIzdatnicaService.GetNewRB(textBox3.Text).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Prvo odaberite za koju izdatnicu zelite da unesete podatke");
            }
            textBox6.Text = "";
            textBox4.Text = "";
            textBox8.Text = "";
            textBox7.Text = "";
            textBox10.Text = "";
            textBox20.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SIzdatnicaAzuriranjeVM sIzdatnica = new SIzdatnicaAzuriranjeVM()
                {
                    Rb = double.Parse(textBox5.Text),
                    //Cena = double.Parse(textBox4.Text),
                    Kolicina = decimal.Parse(textBox8.Text),
                    Lek = textBox7.Text,
                    Vt = textBox10.Text,
                    Br_dok = textBox2.Text,
                    ZIzdatnicaId = int.Parse(textBox3.Text),
                    Datum = dateTimePicker1.Value,
                    VeterinarskiTehnicar = textBox20.Text
                };
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                    ReturnObject result = sIzdatnicaService.Save(sIzdatnica);
                    if (result.Success)
                    {
                        textBox6.Text = result.Id.ToString();
                        if (!string.IsNullOrEmpty(result.Message))
                        {
                            MessageBox.Show(result.Message);
                        }
                        dataGridView2.DataSource = sIzdatnicaService.GetAll(textBox2.Text);
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Trenutno ne podrzavamo izmenu podataka");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
            }
            else
            {
                SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                ReturnObject result = sIzdatnicaService.Delete(int.Parse(textBox6.Text));
                if (result.Success)
                {
                    dataGridView2.DataSource = sIzdatnicaService.GetAll(textBox2.Text);
                    textBox6.Text = "";
                    textBox5.Text = "";
                    textBox4.Text = "";
                    textBox8.Text = "";
                    textBox7.Text = "";
                    textBox10.Text = "";
                }
                else
                {
                    MessageBox.Show(result.Message);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VeterinarskiTehnicarService veterinarskiTehnicarService = new VeterinarskiTehnicarService();
            dataGridView3.DataSource = veterinarskiTehnicarService.GetAllShortVM();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView4.DataSource = artikliService.GetAllShortVM(textBox1.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            VrstaTroskaService vrstaTroskaService = new VrstaTroskaService();
            dataGridView4.DataSource = vrstaTroskaService.GetAllShortVM(textBox1.Text);
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dateTimePicker1.Focus();
            }
        }

        private void dateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dateTimePicker2.Focus();
            }
        }

        private void textBox20_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox5.Focus();
            }
        }

        private void dateTimePicker2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox9.Focus();
            }
        }

        private void textBox9_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox8.Focus();
            }
        }

        private void textBox8_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox7.Focus();
            }
        }

        private void textBox7_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox10.Focus();
            }
        }

        private void textBox10_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox20.Focus();
            }
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1.Focus();
            }
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1.Focus();
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }
    }
}
