﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class KatalogLekovaAzuriranjeForm : Form
    {
        public KatalogLekovaAzuriranjeForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView4.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void KatalogLekovaAzuriranjeForm_Load(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView1.DataSource = artikliService.GetAllAzuriranjeVM();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
            dataGridView4.DataSource = vrsteArtiklaService.GetAllShortVM();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    ArtikliService artikliService = new ArtikliService();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var lek = artikliService.GetById(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = lek.Lek;
                    textBox4.Text = lek.Naziv;
                    textBox8.Text = lek.Grupa;
                    textBox7.Text = lek.GR_NAB.ToString();
                    textBox1.Text = lek.Jm;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox5.Text = "";
            textBox4.Text = "";
            textBox8.Text = "";
            textBox7.Text = "";
            textBox1.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
                }
                else
                {
                    ArtikliService artikliService = new ArtikliService();
                    ReturnObject result = artikliService.Delete(int.Parse(textBox6.Text));
                    if (result.Success)
                    {
                        dataGridView1.DataSource = artikliService.GetAll();
                        textBox6.Text = "";
                        textBox5.Text = "";
                        textBox4.Text = "";
                        textBox8.Text = "";
                        textBox7.Text = "";
                        textBox1.Text = "";
                    }
                    else
                    {
                        MessageBox.Show("Prvo morate izbrisati sve vezano za ovaj artikl. Greska: " + result.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                ArtiklPregledVM artiklPregledVM = new ArtiklPregledVM()
                {
                    Jm = textBox1.Text,
                    GR_NAB = decimal.Parse(textBox7.Text),
                    Grupa = textBox8.Text,
                    Lek = textBox5.Text,
                    Naziv = textBox4.Text
                };
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    ArtikliService artikliService = new ArtikliService();
                    var result = artikliService.Save(artiklPregledVM);
                    if (result.Success)
                    {
                        textBox6.Text = result.Id.ToString();
                        dataGridView1.DataSource = artikliService.GetAllAzuriranjeVM();
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Trenutno ne podrzavamo izmenu podataka");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView2.DataSource = artikliService.GetJediniceMere();
        }
    }
}
