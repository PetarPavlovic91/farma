﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class PartnerFormPregled : Form
    {
        public PartnerFormPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void PartnerFormPregled_Load(object sender, EventArgs e)
        {
            PartnerService partnerService = new PartnerService();
            dataGridView1.DataSource = partnerService.GetAll();
        }
    }
}
