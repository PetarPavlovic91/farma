﻿using BusinessLayer.Services.Lekovi;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class IzdatnicaLekovaPregled : Form
    {
        private string _brDok;
        public IzdatnicaLekovaPregled()
        {
            InitializeComponent();
            _brDok = "";
        }

        private void IzdatnicaLekovaPregled_Load(object sender, EventArgs e)
        {
            ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
            dataGridView1.DataSource = zIzdatnicaService.GetAll();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                    _brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                    dataGridView2.DataSource = sIzdatnicaService.GetAll(_brDok);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
            var datum = zIzdatnicaService.GetDatumByBrDok(_brDok);
            var lekoviForm = new TrebovanjeForm(_brDok, datum.ToString("dd/MM/yyyy"), "Trebovanje");
            lekoviForm.TopLevel = true;
            lekoviForm.Dock = DockStyle.Fill;
            lekoviForm.BringToFront();
            lekoviForm.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ZIzdatnicaService zIzdatnicaService = new ZIzdatnicaService();
            var datum = zIzdatnicaService.GetDatumByBrDok(_brDok);
            var lekoviForm = new IzdatnicaForm(_brDok, datum.ToString("dd/MM/yyyy"), "Izdatnica");
            lekoviForm.TopLevel = true;
            lekoviForm.Dock = DockStyle.Fill;
            lekoviForm.BringToFront();
            lekoviForm.Show();
        }
    }
}
