﻿
namespace Farma.Forms.Lekovi
{
    partial class PartnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonPregled = new System.Windows.Forms.Button();
            this.buttonAzuriranje = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1064, 37);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(421, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(218, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Katalog poslovnih partnera";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonPregled);
            this.panel2.Controls.Add(this.buttonAzuriranje);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1064, 39);
            this.panel2.TabIndex = 1;
            // 
            // buttonPregled
            // 
            this.buttonPregled.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPregled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.buttonPregled.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonPregled.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonPregled.Location = new System.Drawing.Point(531, 0);
            this.buttonPregled.Name = "buttonPregled";
            this.buttonPregled.Size = new System.Drawing.Size(533, 39);
            this.buttonPregled.TabIndex = 1;
            this.buttonPregled.Text = "Pregled";
            this.buttonPregled.UseVisualStyleBackColor = false;
            this.buttonPregled.Click += new System.EventHandler(this.buttonPregled_Click);
            // 
            // buttonAzuriranje
            // 
            this.buttonAzuriranje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAzuriranje.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.buttonAzuriranje.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonAzuriranje.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAzuriranje.Location = new System.Drawing.Point(0, 0);
            this.buttonAzuriranje.Name = "buttonAzuriranje";
            this.buttonAzuriranje.Size = new System.Drawing.Size(525, 39);
            this.buttonAzuriranje.TabIndex = 0;
            this.buttonAzuriranje.Text = "Azuriranje";
            this.buttonAzuriranje.UseVisualStyleBackColor = false;
            this.buttonAzuriranje.Click += new System.EventHandler(this.buttonAzuriranje_Click);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 76);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1064, 577);
            this.panel3.TabIndex = 2;
            // 
            // PartnerForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1064, 653);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.Name = "PartnerForm";
            this.Text = "PartnerForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonPregled;
        private System.Windows.Forms.Button buttonAzuriranje;
        private System.Windows.Forms.Panel panel3;
    }
}