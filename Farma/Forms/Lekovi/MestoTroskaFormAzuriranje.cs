﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class MestoTroskaFormAzuriranje : Form
    {
        public MestoTroskaFormAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    VrstaTroskaService vrstaTroskaService = new VrstaTroskaService();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var vrstaTroska = vrstaTroskaService.GetById(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = vrstaTroska.VT;
                    textBox4.Text = vrstaTroska.Naziv;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MestoTroskaFormAzuriranje_Load(object sender, EventArgs e)
        {
            VrstaTroskaService vrstaTroskaService = new VrstaTroskaService();
            dataGridView1.DataSource = vrstaTroskaService.GetAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox5.Text = "";
            textBox4.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
            }
            else
            {
                VrstaTroskaService vrstaTroskaService = new VrstaTroskaService();
                ReturnObject result = vrstaTroskaService.Delete(int.Parse(textBox6.Text));
                if (result.Success)
                {
                    dataGridView1.DataSource = vrstaTroskaService.GetAll();
                    textBox6.Text = "";
                    textBox5.Text = "";
                    textBox4.Text = "";
                }
                else
                {
                    MessageBox.Show("Prvo morate izbrisati sve vezano za ovo mesto utroska. Greska: " + result.Message);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                VrstaTroskaVM vrstaTroskaVM = new VrstaTroskaVM()
                {
                    VT = textBox5.Text,
                    Naziv = textBox4.Text
                };
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    VrstaTroskaService vrstaTroskaService = new VrstaTroskaService();
                    var result = vrstaTroskaService.Save(vrstaTroskaVM);
                    if (result.Success)
                    {
                        textBox6.Text = result.Id.ToString();
                        dataGridView1.DataSource = vrstaTroskaService.GetAll();
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Trenutno ne podrzavamo izmenu podataka");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
