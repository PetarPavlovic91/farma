﻿using BusinessLayer.Services.Lekovi;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class StanjeLageraNaDanForm : Form
    {
        public StanjeLageraNaDanForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KarticaService karticaService = new KarticaService();
                var upToCurrentDate = dateTimePicker1.Value.AddDays(1);
                upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 0, 0, 0);
                dataGridView1.DataSource = karticaService.StanjeLageraNaDan(upToCurrentDate);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            var lekoviForm = new StanjeLageraNaDan(dateTimePicker1.Value);
            lekoviForm.TopLevel = true;
            lekoviForm.Dock = DockStyle.Fill;
            lekoviForm.BringToFront();
            lekoviForm.Show();
        }
    }
}
