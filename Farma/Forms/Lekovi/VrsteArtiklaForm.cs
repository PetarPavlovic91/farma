﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class VrsteArtiklaForm : Form
    {
        public VrsteArtiklaForm()
        {
            InitializeComponent();
        }

        private Form activeForm = null;
        private void OpenChildForm(Form childForm)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelVrstaArtikla.Controls.Add(childForm);
            panelVrstaArtikla.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void buttonAzuriranjeVrsteArtikla_Click(object sender, EventArgs e)
        {
            OpenChildForm(new AzuriranjeVrsteArtiklForm());
        }

        private void buttonPregledVrsteArtikla_Click(object sender, EventArgs e)
        {
            OpenChildForm(new PregledVrsteArtiklaForm());
        }
    }
}
