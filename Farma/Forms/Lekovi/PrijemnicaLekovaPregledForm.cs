﻿using BusinessLayer.Services.Lekovi;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class PrijemnicaLekovaPregledForm : Form
    {
        private string _brDok;
        public PrijemnicaLekovaPregledForm()
        {
            InitializeComponent();
            _brDok = "";
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void PrijemnicaLekovaPregledForm_Load(object sender, EventArgs e)
        {
            ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
            dataGridView1.DataSource = zPrijemnicaService.GetAllWithPartner();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
            _brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            var brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            dataGridView2.DataSource = sPrijemnicaService.GetAllByBrDok(brDok);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
            _brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            var brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            dataGridView2.DataSource = sPrijemnicaService.GetAllByBrDok(brDok);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
            var datum = zPrijemnicaService.GetDatumByBrDok(_brDok);
            var dobavljac = zPrijemnicaService.GetPartnerInfoByDoc(_brDok);
            if (dobavljac != null)
            {
                var lekoviForm = new PrijemnicaForm(_brDok, datum.ToString("dd/MM/yyyy"), "Trebovanje", dobavljac.Sifpp, dobavljac.Naziv, dobavljac.Dok_dobav);
                lekoviForm.TopLevel = true;
                lekoviForm.Dock = DockStyle.Fill;
                lekoviForm.BringToFront();
                lekoviForm.Show();
            }
        }
    }
}
