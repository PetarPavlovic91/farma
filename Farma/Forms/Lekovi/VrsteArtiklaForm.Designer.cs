﻿
namespace Farm.FormeZaLekove
{
    partial class VrsteArtiklaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelSubjectVrsteArtikla = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonPregledVrsteArtikla = new System.Windows.Forms.Button();
            this.buttonAzuriranjeVrsteArtikla = new System.Windows.Forms.Button();
            this.panelVrstaArtikla = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panel1.Controls.Add(this.labelSubjectVrsteArtikla);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1064, 37);
            this.panel1.TabIndex = 0;
            // 
            // labelSubjectVrsteArtikla
            // 
            this.labelSubjectVrsteArtikla.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelSubjectVrsteArtikla.AutoSize = true;
            this.labelSubjectVrsteArtikla.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.labelSubjectVrsteArtikla.Location = new System.Drawing.Point(468, 9);
            this.labelSubjectVrsteArtikla.Name = "labelSubjectVrsteArtikla";
            this.labelSubjectVrsteArtikla.Size = new System.Drawing.Size(111, 21);
            this.labelSubjectVrsteArtikla.TabIndex = 0;
            this.labelSubjectVrsteArtikla.Text = "Grupa lekova";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonPregledVrsteArtikla);
            this.panel2.Controls.Add(this.buttonAzuriranjeVrsteArtikla);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1064, 39);
            this.panel2.TabIndex = 1;
            // 
            // buttonPregledVrsteArtikla
            // 
            this.buttonPregledVrsteArtikla.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPregledVrsteArtikla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.buttonPregledVrsteArtikla.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonPregledVrsteArtikla.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonPregledVrsteArtikla.Location = new System.Drawing.Point(531, 0);
            this.buttonPregledVrsteArtikla.Name = "buttonPregledVrsteArtikla";
            this.buttonPregledVrsteArtikla.Size = new System.Drawing.Size(533, 39);
            this.buttonPregledVrsteArtikla.TabIndex = 1;
            this.buttonPregledVrsteArtikla.Text = "Pregled";
            this.buttonPregledVrsteArtikla.UseVisualStyleBackColor = false;
            this.buttonPregledVrsteArtikla.Click += new System.EventHandler(this.buttonPregledVrsteArtikla_Click);
            // 
            // buttonAzuriranjeVrsteArtikla
            // 
            this.buttonAzuriranjeVrsteArtikla.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAzuriranjeVrsteArtikla.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.buttonAzuriranjeVrsteArtikla.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.buttonAzuriranjeVrsteArtikla.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAzuriranjeVrsteArtikla.Location = new System.Drawing.Point(0, 0);
            this.buttonAzuriranjeVrsteArtikla.Name = "buttonAzuriranjeVrsteArtikla";
            this.buttonAzuriranjeVrsteArtikla.Size = new System.Drawing.Size(525, 39);
            this.buttonAzuriranjeVrsteArtikla.TabIndex = 0;
            this.buttonAzuriranjeVrsteArtikla.Text = "Azuriranje";
            this.buttonAzuriranjeVrsteArtikla.UseVisualStyleBackColor = false;
            this.buttonAzuriranjeVrsteArtikla.Click += new System.EventHandler(this.buttonAzuriranjeVrsteArtikla_Click);
            // 
            // panelVrstaArtikla
            // 
            this.panelVrstaArtikla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVrstaArtikla.Location = new System.Drawing.Point(0, 76);
            this.panelVrstaArtikla.Name = "panelVrstaArtikla";
            this.panelVrstaArtikla.Size = new System.Drawing.Size(1064, 577);
            this.panelVrstaArtikla.TabIndex = 2;
            // 
            // VrsteArtiklaForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1064, 653);
            this.Controls.Add(this.panelVrstaArtikla);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.Name = "VrsteArtiklaForm";
            this.Text = "VrsteArtiklaForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelSubjectVrsteArtikla;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonPregledVrsteArtikla;
        private System.Windows.Forms.Button buttonAzuriranjeVrsteArtikla;
        private System.Windows.Forms.Panel panelVrstaArtikla;
    }
}