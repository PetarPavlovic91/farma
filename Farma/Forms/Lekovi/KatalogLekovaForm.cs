﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class KatalogLekovaForm : Form
    {
        public KatalogLekovaForm()
        {
            InitializeComponent();
        }

        private Form _activeForm = null;
        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new KatalogLekovaPregledForm(), _activeForm, panel3);
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new KatalogLekovaAzuriranjeForm(), _activeForm, panel3);
        }
    }
}
