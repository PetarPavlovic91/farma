﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class PartnerForm : Form
    {
        public PartnerForm()
        {
            InitializeComponent();
        }

        private Form _activeForm = null;
        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new PartnerFormPregled(), _activeForm, panel3);
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new PartnerFormAzuriranje(), _activeForm, panel3);
        }
    }
}
