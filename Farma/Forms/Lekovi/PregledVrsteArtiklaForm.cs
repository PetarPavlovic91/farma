﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class PregledVrsteArtiklaForm : Form
    {
        public PregledVrsteArtiklaForm()
        {
            InitializeComponent();
        }

        private void PregledVrsteArtiklaForm_Load(object sender, EventArgs e)
        {
            dataGridViewPregledVrsteArtikla.DefaultCellStyle.ForeColor = Color.Black;
            VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
            dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VrsteArtiklaService vrsteArtiklaService = new VrsteArtiklaService();
            var textForSearch = textBox1.Text;
            if (!string.IsNullOrWhiteSpace(textForSearch))
            {
                dataGridViewPregledVrsteArtikla.DataSource = vrsteArtiklaService.GetFiltered(textForSearch);
            }
        }
    }
}
