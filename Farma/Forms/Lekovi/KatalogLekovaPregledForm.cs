﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class KatalogLekovaPregledForm : Form
    {
        public KatalogLekovaPregledForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void KatalogLekovaPregledForm_Load(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView1.DataSource = artikliService.GetAll();
        }
    }
}
