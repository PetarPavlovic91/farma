﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class IzdatnicaLekovaForm : Form
    {
        public IzdatnicaLekovaForm()
        {
            InitializeComponent();
        }
        private Form activeForm = null;
        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new IzdatnicaLekovaAzuriranje(), activeForm, panel3);
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenChildForm(new IzdatnicaLekovaPregled(), activeForm, panel3);
        }
    }
}
