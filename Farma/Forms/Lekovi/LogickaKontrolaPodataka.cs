﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class LogickaKontrolaPodataka : Form
    {
        public LogickaKontrolaPodataka()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void LogickaKontrolaPodataka_Load(object sender, EventArgs e)
        {
            try
            {
                ArtikliService artikliService = new ArtikliService();
                dataGridView1.DataSource = artikliService.LogickaKontrolaPodataka();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
