﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm.FormeZaLekove
{
    public partial class PrijemnicaLekovaAzuriranjeForm : Form
    {
        public PrijemnicaLekovaAzuriranjeForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView3.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView4.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void PrijemnicaLekovaAzuriranjeForm_Load(object sender, EventArgs e)
        {
            ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
            dataGridView1.DataSource = zPrijemnicaService.GetAllWithPartner();
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            //SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();

            //var brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            //var allByDok = sPrijemnicaService.GetAllByBrDok(brDok);
            //dataGridView2.DataSource = allByDok;
            //if (allByDok.Count == 0)
            //{
            //    textBox6.Text = "";
            //    textBox5.Text = "";
            //    textBox4.Text = "";
            //    textBox8.Text = "";
            //    textBox7.Text = "";
            //}

            //ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
            //var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            //var zPrijemnica = zPrijemnicaService.GetByIdWithPartner(id);
            //textBox3.Text = id.ToString();
            //textBox2.Text = zPrijemnica.Br_dok;
            //textBox1.Text = zPrijemnica.Datum.ToString("MM/dd/yyyy");
            //textBox20.Text = zPrijemnica.Dok_dobav;
            //textBox21.Text = zPrijemnica.Sifpp;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();

                    var brDok = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var allByDok = sPrijemnicaService.GetAllByBrDok(brDok);
                    dataGridView2.DataSource = allByDok;
                    if (allByDok.Count == 0)
                    {
                        try
                        {
                            textBox5.Text = sPrijemnicaService.GetNewRB(id.ToString()).ToString();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Prvo odaberite za koju izdatnicu zelite da unesete podatke");
                        }
                        textBox6.Text = "";
                        textBox5.Text = "";
                        textBox4.Text = "";
                        textBox8.Text = "";
                        textBox7.Text = "";
                    }

                    ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
                    var zPrijemnica = zPrijemnicaService.GetByIdWithPartner(id);
                    textBox3.Text = id.ToString();
                    textBox2.Text = zPrijemnica.Br_dok;
                    dateTimePicker1.Value = zPrijemnica.Datum;
                    textBox20.Text = zPrijemnica.Dok_dobav;
                    textBox21.Text = zPrijemnica.Sifpp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBoxNaziv_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBoxVGP_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBoxId_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            PartnerService partnerService = new PartnerService();
            dataGridView3.DataSource = partnerService.GetAllShortVM(textBox9.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView4.DataSource = artikliService.GetAllShortVM(textBox1.Text);
        }

        private void buttonDodajNovi_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            textBox2.Text = "";
            dateTimePicker1.Value = DateTime.Now;
            textBox20.Text = "";
            textBox21.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SPrijemnicaLekovaAzuriranjeVM sPrijemnica = new SPrijemnicaLekovaAzuriranjeVM()
                {
                    RB = double.Parse(textBox5.Text),
                    Cena = decimal.Parse(textBox4.Text),
                    Kolicina = decimal.Parse(textBox8.Text),
                    Lek = textBox7.Text,
                    Br_dok = textBox2.Text,
                    ZPrijemnicaId = int.Parse(textBox3.Text),
                    Datum = dateTimePicker1.Value,
                    Sifpp = textBox21.Text
                };
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
                    ReturnObject result = sPrijemnicaService.SaveWithLek(sPrijemnica);
                    if (result.Success)
                    {
                        textBox6.Text = result.Id.ToString();
                        dataGridView2.DataSource = sPrijemnicaService.GetAllByBrDok(textBox2.Text);
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Trenutno ne podrzavamo izmenu podataka");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonSacuvaj_Click(object sender, EventArgs e)
        {
            try
            {
                ZPrijemnicaLekovaPregledVM zPrijemnica = new ZPrijemnicaLekovaPregledVM()
                {
                    Id = string.IsNullOrEmpty(textBox3.Text) ? 0 : int.Parse(textBox3.Text),
                    Sifpp = textBox21.Text,
                    Dok_dobav = textBox20.Text,
                    Br_dok = textBox2.Text,
                    Datum = dateTimePicker1.Value
                };
                ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
                var result = zPrijemnicaService.SaveWithPartnerSifpp(zPrijemnica);
                if (result.Success)
                {
                    textBox3.Text = result.Id.ToString();
                }
                else
                {
                    MessageBox.Show(result.Message);
                }
                dataGridView1.DataSource = zPrijemnicaService.GetAllWithPartner();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonIzbrisi_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox3.Text))
                {
                    MessageBox.Show("Prvo selektujte sta zelite da obrisete.");
                }
                else
                {
                    ZPrijemnicaService zPrijemnicaService = new ZPrijemnicaService();
                    var result = zPrijemnicaService.Delete(int.Parse(textBox3.Text));
                    if (result.Success)
                    {
                        dataGridView1.DataSource = zPrijemnicaService.GetAllWithPartner();
                        textBox3.Text = "";
                        textBox2.Text = "";
                        dateTimePicker1.Value = DateTime.Now;
                        textBox20.Text = "";
                        textBox21.Text = "";
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView2_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
                    var id = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value;
                    var sPrijemnica = sPrijemnicaService.GetByIdWithLek(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = sPrijemnica.RB.ToString();
                    textBox4.Text = sPrijemnica.Cena.ToString();
                    textBox8.Text = sPrijemnica.Kolicina.ToString();
                    textBox7.Text = sPrijemnica.Lek;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView2_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
                    var id = (int)dataGridView2.Rows[e.RowIndex].Cells[0].Value;
                    var sPrijemnica = sPrijemnicaService.GetByIdWithLek(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = sPrijemnica.RB.ToString();
                    textBox4.Text = sPrijemnica.Cena.ToString();
                    textBox8.Text = sPrijemnica.Kolicina.ToString();
                    textBox7.Text = sPrijemnica.Lek;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
                textBox5.Text = sPrijemnicaService.GetNewRB(textBox3.Text).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Prvo odaberite za koju prijemnicu zelite da unesete podatke");
            }
            textBox6.Text = "";
            textBox4.Text = "";
            textBox8.Text = "";
            textBox7.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
                }
                else
                {
                    SPrijemnicaService sPrijemnicaService = new SPrijemnicaService();
                    ReturnObject result = sPrijemnicaService.Delete(int.Parse(textBox6.Text));
                    if (result.Success)
                    {
                        dataGridView2.DataSource = sPrijemnicaService.GetAllByBrDok(textBox2.Text);
                        textBox6.Text = "";
                        textBox5.Text = "";
                        textBox4.Text = "";
                        textBox8.Text = "";
                        textBox7.Text = "";
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dateTimePicker1.Focus();
            }
        }

        private void dateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox20.Focus();
            }
        }

        private void textBox20_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox21.Focus();
            }
        }

        private void textBox21_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox9.Focus();
            }
        }

        private void textBox9_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox5.Focus();
            }
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox4.Focus();
            }
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox8.Focus();
            }
        }

        private void textBox8_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox7.Focus();
            }
        }

        private void textBox7_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1.Focus();
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }
    }
}
