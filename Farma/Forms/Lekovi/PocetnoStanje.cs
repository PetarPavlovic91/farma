﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class PocetnoStanje : Form
    {
        public PocetnoStanje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            PocetnoStanjeService pocetnoStanjeService = new PocetnoStanjeService();
            dataGridView1.DataSource = pocetnoStanjeService.GetAll();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView2.DataSource = artikliService.GetAllShortVM(textBox1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
            }
            else
            {
                PocetnoStanjeService pocetnoStanjeService = new PocetnoStanjeService();
                bool result = pocetnoStanjeService.Delete(int.Parse(textBox6.Text));
                if (result)
                {
                    dataGridView1.DataSource = pocetnoStanjeService.GetAll();
                    textBox6.Text = "";
                    textBox5.Text = "";
                    textBox4.Text = "";
                    textBox8.Text = "";
                }
                else
                {
                    MessageBox.Show("Nije izbrisano pocetno stanje");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox8.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                PocetnoStanjeVM pocetnoStanje = new PocetnoStanjeVM()
                {
                    Id = string.IsNullOrEmpty(textBox6.Text) ? 0 : Convert.ToInt32(textBox6.Text),
                    Cena = string.IsNullOrEmpty(textBox5.Text) ? 0 : Convert.ToDecimal(textBox5.Text),
                    Kolicina = string.IsNullOrEmpty(textBox4.Text) ? 0 : Convert.ToDecimal(textBox4.Text),
                    SifraArtikla = textBox8.Text,
                    Datum = dateTimePicker1.Value
                };

                PocetnoStanjeService pocetnoStanjeService = new PocetnoStanjeService();
                var result = pocetnoStanjeService.Save(pocetnoStanje);
                dataGridView1.DataSource = pocetnoStanjeService.GetAll();
                if (result.Success)
                {
                    MessageBox.Show("Uspesno cuvanje");
                }
                else
                {
                    MessageBox.Show(result.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    PocetnoStanjeService pocetnoStanjeService = new PocetnoStanjeService();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var pocetnoStanje = pocetnoStanjeService.GetById(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = pocetnoStanje.Cena?.ToString();
                    textBox4.Text = pocetnoStanje.Kolicina?.ToString();
                    textBox8.Text = pocetnoStanje.SifraArtikla;
                    dateTimePicker1.Value = pocetnoStanje.Datum;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
