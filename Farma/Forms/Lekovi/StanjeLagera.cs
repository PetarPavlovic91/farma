﻿using BusinessLayer.Services.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class StanjeLagera : Form
    {
        public StanjeLagera()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void StanjeLagera_Load(object sender, EventArgs e)
        {
            ArtikliService artikliService = new ArtikliService();
            dataGridView1.DataSource = artikliService.GetStanjeLageraLekova();
        }
    }
}
