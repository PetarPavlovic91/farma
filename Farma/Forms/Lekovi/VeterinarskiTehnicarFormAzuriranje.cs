﻿using BusinessLayer.Services.Lekovi;
using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class VeterinarskiTehnicarFormAzuriranje : Form
    {
        public VeterinarskiTehnicarFormAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void VeterinarskiTehnicarFormAzuriranje_Load(object sender, EventArgs e)
        {
            VeterinarskiTehnicarService veterinarskiTehnicarService = new VeterinarskiTehnicarService();
            dataGridView1.DataSource = veterinarskiTehnicarService.GetAll();
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    VeterinarskiTehnicarService veterinarskiTehnicarService = new VeterinarskiTehnicarService();
                    var id = (int)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                    var vetTehnicar = veterinarskiTehnicarService.GetById(id);
                    textBox6.Text = id.ToString();
                    textBox5.Text = vetTehnicar.VET;
                    textBox4.Text = vetTehnicar.Naziv;
                    textBox8.Text = vetTehnicar.Mesto;
                    textBox7.Text = vetTehnicar.Ulica;
                    textBox1.Text = vetTehnicar.Telefon;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox5.Text = "";
            textBox4.Text = "";
            textBox8.Text = "";
            textBox7.Text = "";
            textBox1.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                VeterinarskiTehnicarVM veterinarskiTehnicarVM = new VeterinarskiTehnicarVM()
                {
                    Telefon = textBox1.Text,
                    Ulica = textBox7.Text,
                    Mesto = textBox8.Text,
                    VET = textBox5.Text,
                    Naziv = textBox4.Text
                };
                if (string.IsNullOrEmpty(textBox6.Text))
                {
                    VeterinarskiTehnicarService veterinarskiTehnicarService = new VeterinarskiTehnicarService();
                    var result = veterinarskiTehnicarService.Save(veterinarskiTehnicarVM);
                    if (result.Success)
                    {
                        textBox6.Text = result.Id.ToString();
                        dataGridView1.DataSource = veterinarskiTehnicarService.GetAll();
                    }
                    else
                    {
                        MessageBox.Show(result.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Trenutno ne podrzavamo izmenu podataka");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("Prvo odaberite sta zelite da obrisete.");
            }
            else
            {
                VeterinarskiTehnicarService veterinarskiTehnicarService = new VeterinarskiTehnicarService();
                ReturnObject result = veterinarskiTehnicarService.Delete(int.Parse(textBox6.Text));
                if (result.Success)
                {
                    dataGridView1.DataSource = veterinarskiTehnicarService.GetAll();
                    textBox6.Text = "";
                    textBox5.Text = "";
                    textBox4.Text = "";
                    textBox8.Text = "";
                    textBox7.Text = "";
                    textBox1.Text = "";
                }
                else
                {
                    MessageBox.Show("Prvo morate izbrisati sve vezano za ovog tehnicara. Greska: " + result.Message);
                }
            }
        }
    }
}
