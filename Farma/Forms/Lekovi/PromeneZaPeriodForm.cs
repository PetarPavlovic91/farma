﻿using BusinessLayer.Services.Lekovi;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Lekovi
{
    public partial class PromeneZaPeriodForm : Form
    {
        public PromeneZaPeriodForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dateTimePicker1.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dateTimePicker2.Value = DateTime.Now;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KarticaService karticaService = new KarticaService();
                var upToCurrentDate = dateTimePicker1.Value.AddDays(-1);
                upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 23, 59, 59);
                var upToCurrentDate2 = dateTimePicker2.Value.AddDays(1);
                upToCurrentDate2 = new DateTime(upToCurrentDate2.Year, upToCurrentDate2.Month, upToCurrentDate2.Day, 0, 0, 0);
                dataGridView1.DataSource = karticaService.PromeneZaPeriod(upToCurrentDate, upToCurrentDate2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var lekoviForm = new PromeneZaPeriod(dateTimePicker1.Value, dateTimePicker2.Value);
            lekoviForm.TopLevel = true;
            lekoviForm.Dock = DockStyle.Fill;
            lekoviForm.BringToFront();
            lekoviForm.Show();
        }
    }
}
