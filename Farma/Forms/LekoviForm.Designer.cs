﻿
namespace Farm
{
    partial class LekoviForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelLekoviSideMenu = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.button55 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button50 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.button44 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button35 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonPrijemnicaLekova = new System.Windows.Forms.Button();
            this.buttonBazaPodataka = new System.Windows.Forms.Button();
            this.panelBazaPodataka = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.button34 = new System.Windows.Forms.Button();
            this.panelLekoviSideMenu.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelBazaPodataka.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLekoviSideMenu
            // 
            this.panelLekoviSideMenu.AutoScroll = true;
            this.panelLekoviSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelLekoviSideMenu.Controls.Add(this.panel10);
            this.panelLekoviSideMenu.Controls.Add(this.button51);
            this.panelLekoviSideMenu.Controls.Add(this.panel9);
            this.panelLekoviSideMenu.Controls.Add(this.button45);
            this.panelLekoviSideMenu.Controls.Add(this.panel8);
            this.panelLekoviSideMenu.Controls.Add(this.button36);
            this.panelLekoviSideMenu.Controls.Add(this.panel1);
            this.panelLekoviSideMenu.Controls.Add(this.buttonBazaPodataka);
            this.panelLekoviSideMenu.Controls.Add(this.panelBazaPodataka);
            this.panelLekoviSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLekoviSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelLekoviSideMenu.Name = "panelLekoviSideMenu";
            this.panelLekoviSideMenu.Size = new System.Drawing.Size(276, 704);
            this.panelLekoviSideMenu.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.button55);
            this.panel10.Controls.Add(this.button54);
            this.panel10.Controls.Add(this.button53);
            this.panel10.Controls.Add(this.button52);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 794);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(259, 120);
            this.panel10.TabIndex = 8;
            // 
            // button55
            // 
            this.button55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button55.Dock = System.Windows.Forms.DockStyle.Top;
            this.button55.FlatAppearance.BorderSize = 0;
            this.button55.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button55.Location = new System.Drawing.Point(0, 90);
            this.button55.Name = "button55";
            this.button55.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button55.Size = new System.Drawing.Size(259, 30);
            this.button55.TabIndex = 7;
            this.button55.Text = "4. Promena sifre leka";
            this.button55.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button55.UseVisualStyleBackColor = false;
            // 
            // button54
            // 
            this.button54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button54.Dock = System.Windows.Forms.DockStyle.Top;
            this.button54.FlatAppearance.BorderSize = 0;
            this.button54.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button54.Location = new System.Drawing.Point(0, 60);
            this.button54.Name = "button54";
            this.button54.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button54.Size = new System.Drawing.Size(259, 30);
            this.button54.TabIndex = 6;
            this.button54.Text = "3. Reindeksacija baze podataka";
            this.button54.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button54.UseVisualStyleBackColor = false;
            // 
            // button53
            // 
            this.button53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button53.Dock = System.Windows.Forms.DockStyle.Top;
            this.button53.FlatAppearance.BorderSize = 0;
            this.button53.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button53.Location = new System.Drawing.Point(0, 30);
            this.button53.Name = "button53";
            this.button53.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button53.Size = new System.Drawing.Size(259, 30);
            this.button53.TabIndex = 5;
            this.button53.Text = "2. Backup baze";
            this.button53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button53.UseVisualStyleBackColor = false;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button52
            // 
            this.button52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button52.Dock = System.Windows.Forms.DockStyle.Top;
            this.button52.FlatAppearance.BorderSize = 0;
            this.button52.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button52.Location = new System.Drawing.Point(0, 0);
            this.button52.Name = "button52";
            this.button52.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button52.Size = new System.Drawing.Size(259, 30);
            this.button52.TabIndex = 4;
            this.button52.Text = "1. Logicka kontrola podataka";
            this.button52.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button52.UseVisualStyleBackColor = false;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // button51
            // 
            this.button51.Dock = System.Windows.Forms.DockStyle.Top;
            this.button51.FlatAppearance.BorderSize = 0;
            this.button51.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button51.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button51.Location = new System.Drawing.Point(0, 759);
            this.button51.Name = "button51";
            this.button51.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button51.Size = new System.Drawing.Size(259, 35);
            this.button51.TabIndex = 7;
            this.button51.Text = "4. Pomocni programi";
            this.button51.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.button50);
            this.panel9.Controls.Add(this.button49);
            this.panel9.Controls.Add(this.button48);
            this.panel9.Controls.Add(this.button47);
            this.panel9.Controls.Add(this.button46);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 604);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(259, 155);
            this.panel9.TabIndex = 6;
            // 
            // button50
            // 
            this.button50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button50.Dock = System.Windows.Forms.DockStyle.Top;
            this.button50.FlatAppearance.BorderSize = 0;
            this.button50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button50.Location = new System.Drawing.Point(0, 120);
            this.button50.Name = "button50";
            this.button50.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button50.Size = new System.Drawing.Size(259, 30);
            this.button50.TabIndex = 7;
            this.button50.Text = "5. Preg. lekova po mestima troska";
            this.button50.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button50.UseVisualStyleBackColor = false;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // button49
            // 
            this.button49.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button49.Dock = System.Windows.Forms.DockStyle.Top;
            this.button49.FlatAppearance.BorderSize = 0;
            this.button49.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button49.Location = new System.Drawing.Point(0, 90);
            this.button49.Name = "button49";
            this.button49.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button49.Size = new System.Drawing.Size(259, 30);
            this.button49.TabIndex = 6;
            this.button49.Text = "4. Preg. izdavanja lekova po tehni.";
            this.button49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button49.UseVisualStyleBackColor = false;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // button48
            // 
            this.button48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button48.Dock = System.Windows.Forms.DockStyle.Top;
            this.button48.FlatAppearance.BorderSize = 0;
            this.button48.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button48.Location = new System.Drawing.Point(0, 60);
            this.button48.Name = "button48";
            this.button48.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button48.Size = new System.Drawing.Size(259, 30);
            this.button48.TabIndex = 5;
            this.button48.Text = "3. Lekovi za nabavku";
            this.button48.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button48.UseVisualStyleBackColor = false;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button47
            // 
            this.button47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button47.Dock = System.Windows.Forms.DockStyle.Top;
            this.button47.FlatAppearance.BorderSize = 0;
            this.button47.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button47.Location = new System.Drawing.Point(0, 30);
            this.button47.Name = "button47";
            this.button47.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button47.Size = new System.Drawing.Size(259, 30);
            this.button47.TabIndex = 4;
            this.button47.Text = "2. Kartica poslovnog partnera";
            this.button47.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button47.UseVisualStyleBackColor = false;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // button46
            // 
            this.button46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button46.Dock = System.Windows.Forms.DockStyle.Top;
            this.button46.FlatAppearance.BorderSize = 0;
            this.button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button46.Location = new System.Drawing.Point(0, 0);
            this.button46.Name = "button46";
            this.button46.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button46.Size = new System.Drawing.Size(259, 30);
            this.button46.TabIndex = 3;
            this.button46.Text = "1. Po poslovnim partnerima";
            this.button46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button46.UseVisualStyleBackColor = false;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // button45
            // 
            this.button45.Dock = System.Windows.Forms.DockStyle.Top;
            this.button45.FlatAppearance.BorderSize = 0;
            this.button45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button45.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button45.Location = new System.Drawing.Point(0, 569);
            this.button45.Name = "button45";
            this.button45.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button45.Size = new System.Drawing.Size(259, 35);
            this.button45.TabIndex = 5;
            this.button45.Text = "3. Izvestaji";
            this.button45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.button44);
            this.panel8.Controls.Add(this.button43);
            this.panel8.Controls.Add(this.button39);
            this.panel8.Controls.Add(this.button38);
            this.panel8.Controls.Add(this.button37);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(0, 415);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(259, 154);
            this.panel8.TabIndex = 4;
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button44.Dock = System.Windows.Forms.DockStyle.Top;
            this.button44.FlatAppearance.BorderSize = 0;
            this.button44.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button44.Location = new System.Drawing.Point(0, 120);
            this.button44.Name = "button44";
            this.button44.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button44.Size = new System.Drawing.Size(259, 30);
            this.button44.TabIndex = 15;
            this.button44.Text = "5. Stanje lagera na dan";
            this.button44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button44.UseVisualStyleBackColor = false;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // button43
            // 
            this.button43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button43.Dock = System.Windows.Forms.DockStyle.Top;
            this.button43.FlatAppearance.BorderSize = 0;
            this.button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button43.Location = new System.Drawing.Point(0, 90);
            this.button43.Name = "button43";
            this.button43.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button43.Size = new System.Drawing.Size(259, 30);
            this.button43.TabIndex = 14;
            this.button43.Text = "4. Promene za period";
            this.button43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button43.UseVisualStyleBackColor = false;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button39
            // 
            this.button39.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button39.Dock = System.Windows.Forms.DockStyle.Top;
            this.button39.FlatAppearance.BorderSize = 0;
            this.button39.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button39.Location = new System.Drawing.Point(0, 60);
            this.button39.Name = "button39";
            this.button39.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button39.Size = new System.Drawing.Size(259, 30);
            this.button39.TabIndex = 4;
            this.button39.Text = "3. Promene po vrsti dokumenta";
            this.button39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button39.UseVisualStyleBackColor = false;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // button38
            // 
            this.button38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button38.Dock = System.Windows.Forms.DockStyle.Top;
            this.button38.FlatAppearance.BorderSize = 0;
            this.button38.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button38.Location = new System.Drawing.Point(0, 30);
            this.button38.Name = "button38";
            this.button38.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button38.Size = new System.Drawing.Size(259, 30);
            this.button38.TabIndex = 3;
            this.button38.Text = "2. Kartica leka";
            this.button38.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button38.UseVisualStyleBackColor = false;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button37
            // 
            this.button37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button37.Dock = System.Windows.Forms.DockStyle.Top;
            this.button37.FlatAppearance.BorderSize = 0;
            this.button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button37.Location = new System.Drawing.Point(0, 0);
            this.button37.Name = "button37";
            this.button37.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button37.Size = new System.Drawing.Size(259, 30);
            this.button37.TabIndex = 2;
            this.button37.Text = "1. Stanje lagera";
            this.button37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button37.UseVisualStyleBackColor = false;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button36
            // 
            this.button36.Dock = System.Windows.Forms.DockStyle.Top;
            this.button36.FlatAppearance.BorderSize = 0;
            this.button36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button36.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button36.Location = new System.Drawing.Point(0, 380);
            this.button36.Name = "button36";
            this.button36.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.button36.Size = new System.Drawing.Size(259, 35);
            this.button36.TabIndex = 3;
            this.button36.Text = "2. Pregledi Lagera Lekova";
            this.button36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panel1.Controls.Add(this.button35);
            this.panel1.Controls.Add(this.button33);
            this.panel1.Controls.Add(this.button32);
            this.panel1.Controls.Add(this.button31);
            this.panel1.Controls.Add(this.button30);
            this.panel1.Controls.Add(this.button29);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.buttonPrijemnicaLekova);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 112);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(259, 268);
            this.panel1.TabIndex = 2;
            // 
            // button35
            // 
            this.button35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button35.Dock = System.Windows.Forms.DockStyle.Top;
            this.button35.FlatAppearance.BorderSize = 0;
            this.button35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button35.Location = new System.Drawing.Point(0, 240);
            this.button35.Name = "button35";
            this.button35.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button35.Size = new System.Drawing.Size(259, 30);
            this.button35.TabIndex = 15;
            this.button35.Text = "6. Unos pocetnog stanja";
            this.button35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button35.UseVisualStyleBackColor = false;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // button33
            // 
            this.button33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button33.Dock = System.Windows.Forms.DockStyle.Top;
            this.button33.FlatAppearance.BorderSize = 0;
            this.button33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button33.Location = new System.Drawing.Point(0, 210);
            this.button33.Name = "button33";
            this.button33.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button33.Size = new System.Drawing.Size(259, 30);
            this.button33.TabIndex = 14;
            this.button33.Text = "5. Mesto utroska lekova";
            this.button33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button33.UseVisualStyleBackColor = false;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button32
            // 
            this.button32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button32.Dock = System.Windows.Forms.DockStyle.Top;
            this.button32.FlatAppearance.BorderSize = 0;
            this.button32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button32.Location = new System.Drawing.Point(0, 180);
            this.button32.Name = "button32";
            this.button32.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button32.Size = new System.Drawing.Size(259, 30);
            this.button32.TabIndex = 13;
            this.button32.Text = "4. Katalog veterinara i tehnicara";
            this.button32.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button32.UseVisualStyleBackColor = false;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button31
            // 
            this.button31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button31.Dock = System.Windows.Forms.DockStyle.Top;
            this.button31.FlatAppearance.BorderSize = 0;
            this.button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button31.Location = new System.Drawing.Point(0, 150);
            this.button31.Name = "button31";
            this.button31.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button31.Size = new System.Drawing.Size(259, 30);
            this.button31.TabIndex = 12;
            this.button31.Text = "3. Grupe lekova";
            this.button31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button31.UseVisualStyleBackColor = false;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button30
            // 
            this.button30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button30.Dock = System.Windows.Forms.DockStyle.Top;
            this.button30.FlatAppearance.BorderSize = 0;
            this.button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button30.Location = new System.Drawing.Point(0, 120);
            this.button30.Name = "button30";
            this.button30.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button30.Size = new System.Drawing.Size(259, 30);
            this.button30.TabIndex = 11;
            this.button30.Text = "2. Poslovni Partneri";
            this.button30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button30.UseVisualStyleBackColor = false;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // button29
            // 
            this.button29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button29.Dock = System.Windows.Forms.DockStyle.Top;
            this.button29.FlatAppearance.BorderSize = 0;
            this.button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button29.Location = new System.Drawing.Point(0, 90);
            this.button29.Name = "button29";
            this.button29.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button29.Size = new System.Drawing.Size(259, 30);
            this.button29.TabIndex = 10;
            this.button29.Text = "1. Katalog lekova";
            this.button29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button29.UseVisualStyleBackColor = false;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(0, 60);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(259, 30);
            this.button2.TabIndex = 2;
            this.button2.Text = "3. Katalozi i sifrarnici";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(0, 30);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(259, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "2. Izdatnica - trebovanje lekova";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonPrijemnicaLekova
            // 
            this.buttonPrijemnicaLekova.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.buttonPrijemnicaLekova.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonPrijemnicaLekova.FlatAppearance.BorderSize = 0;
            this.buttonPrijemnicaLekova.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPrijemnicaLekova.Location = new System.Drawing.Point(0, 0);
            this.buttonPrijemnicaLekova.Name = "buttonPrijemnicaLekova";
            this.buttonPrijemnicaLekova.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.buttonPrijemnicaLekova.Size = new System.Drawing.Size(259, 30);
            this.buttonPrijemnicaLekova.TabIndex = 0;
            this.buttonPrijemnicaLekova.Text = "1. Prijemnica lekova";
            this.buttonPrijemnicaLekova.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPrijemnicaLekova.UseVisualStyleBackColor = false;
            this.buttonPrijemnicaLekova.Click += new System.EventHandler(this.buttonPrijemnicaLekova_Click);
            // 
            // buttonBazaPodataka
            // 
            this.buttonBazaPodataka.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonBazaPodataka.FlatAppearance.BorderSize = 0;
            this.buttonBazaPodataka.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBazaPodataka.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonBazaPodataka.Location = new System.Drawing.Point(0, 77);
            this.buttonBazaPodataka.Name = "buttonBazaPodataka";
            this.buttonBazaPodataka.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonBazaPodataka.Size = new System.Drawing.Size(259, 35);
            this.buttonBazaPodataka.TabIndex = 1;
            this.buttonBazaPodataka.Text = "1. Baza Podataka";
            this.buttonBazaPodataka.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBazaPodataka.UseVisualStyleBackColor = true;
            this.buttonBazaPodataka.Click += new System.EventHandler(this.buttonBazaPodataka_Click);
            // 
            // panelBazaPodataka
            // 
            this.panelBazaPodataka.Controls.Add(this.label2);
            this.panelBazaPodataka.Controls.Add(this.label1);
            this.panelBazaPodataka.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelBazaPodataka.Location = new System.Drawing.Point(0, 0);
            this.panelBazaPodataka.Name = "panelBazaPodataka";
            this.panelBazaPodataka.Size = new System.Drawing.Size(259, 77);
            this.panelBazaPodataka.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(30, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "LEK - Magacin lekova";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Farma Voganj";
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(276, 0);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(1064, 704);
            this.panelChildForm.TabIndex = 1;
            this.panelChildForm.Paint += new System.Windows.Forms.PaintEventHandler(this.panelChildForm_Paint);
            // 
            // button34
            // 
            this.button34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button34.Dock = System.Windows.Forms.DockStyle.Top;
            this.button34.FlatAppearance.BorderSize = 0;
            this.button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button34.Location = new System.Drawing.Point(0, 240);
            this.button34.Name = "button34";
            this.button34.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.button34.Size = new System.Drawing.Size(276, 30);
            this.button34.TabIndex = 15;
            this.button34.Text = "6. Unos pocetnog stanja";
            this.button34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button34.UseVisualStyleBackColor = false;
            // 
            // LekoviForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1340, 704);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panelLekoviSideMenu);
            this.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.MinimumSize = new System.Drawing.Size(900, 650);
            this.Name = "LekoviForm";
            this.Text = "LekoviForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LekoviForm_FormClosed);
            this.Load += new System.EventHandler(this.LekoviForm_Load);
            this.panelLekoviSideMenu.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panelBazaPodataka.ResumeLayout(false);
            this.panelBazaPodataka.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLekoviSideMenu;
        private System.Windows.Forms.Button buttonBazaPodataka;
        private System.Windows.Forms.Panel panelBazaPodataka;
        private System.Windows.Forms.Panel panelChildForm;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonPrijemnicaLekova;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}