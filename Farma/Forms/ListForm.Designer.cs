﻿
namespace Farma.Forms
{
    partial class ListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.bazaPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaNalogaObrtaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaIshraneStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidPrihodaITroskovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.proceneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaListaIObrtaStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizeIshraneStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrihodaITroskovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizePodPoKategorijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servisniModulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.procenaTezineNaStanjuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mesecnoStanjeZalihaHraneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parametriCeneKostanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cenovnikKategorijaStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cenovnikKoncentrataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kursnaListaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogKategorijaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogPromenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogKoncentrataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogKupacaStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogPrihodaITroskovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediNalogaObrtaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediIshraneStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediProdajeStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodajaPoKupcimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrihodaITroskovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPoBrojuNalogaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNalogaPoDatumuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNalogaPoKategorijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregleNalogaPoPromeniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPoBrojuNalogaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNalogaPoDatumuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNalogaPoKategorijiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNalogaPoKoncentratuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPoBrojuDokumentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledProdajePoDatumuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledProdajePoKategorijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledProdajePoKupcuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPoVrstiDokumentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrihodaITroskovaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrihodaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledTroskovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledZaIzabranuSifruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listStokeIObrtZaKategorijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listStanjaStokeNaFarmiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promenaBrojnogStanjaStokeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kalkulacijaCeneKostanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prognozaIsporuTovljenikaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrevodaPoKategorijamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.isporukaPoKategorijamaIMesecimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.planObrtaStadaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dnevniListStanjaStokeNaFarmiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.utrosakKoncentrataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prirastIUtrosakHranePoMesecuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUtroskaHraneIIsporukeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ukupniPrihodiITroskoviToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prihodiITroskoviPoMesecimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrihodaITroskovaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrihodaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledTroskovaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledZaIzabranuSifruToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.preglediPrihodaITroskovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPrihodaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledTroskovaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledZaIzabranuSifruToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizeZaIzabraniPeriodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uporednaAnalizaPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPodatakaPoGodinamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.obrtPoMesecimaZaKategorijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUginucaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrirastaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaProdajeisporukeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrinudnogKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaStanjaStokeNaFarmiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUtroskaKoncentrataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUginucaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrirastaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaProdajeisporukeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrinudnogKlanjaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaStanjaStokeNaFarmiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUtroskaKoncentrataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUginucaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrirastaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaProdajeisporukeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrinudnogKlanjaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaStanjaStokeNaFarmiToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUtroskaKoncentrataToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.reindeksacijaBazePodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logickaKontrolaPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upravljanjeStampacemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1340, 54);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(596, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "List Application";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.menuStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1340, 599);
            this.panel2.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bazaPodatakaToolStripMenuItem,
            this.preglediPodatakaToolStripMenuItem,
            this.analizaListaIObrtaStokeToolStripMenuItem,
            this.analizeIshraneStokeToolStripMenuItem,
            this.analizaPrihodaITroskovaToolStripMenuItem,
            this.analizePodPoKategorijiToolStripMenuItem,
            this.servisniModulToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1340, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // bazaPodatakaToolStripMenuItem
            // 
            this.bazaPodatakaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaNalogaObrtaToolStripMenuItem,
            this.evidencijaIshraneStokeToolStripMenuItem,
            this.evidPrihodaITroskovaToolStripMenuItem,
            this.toolStripMenuItem2,
            this.proceneToolStripMenuItem,
            this.toolStripMenuItem3});
            this.bazaPodatakaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bazaPodatakaToolStripMenuItem.Name = "bazaPodatakaToolStripMenuItem";
            this.bazaPodatakaToolStripMenuItem.Size = new System.Drawing.Size(107, 20);
            this.bazaPodatakaToolStripMenuItem.Text = "1. Baza podataka";
            // 
            // evidencijaNalogaObrtaToolStripMenuItem
            // 
            this.evidencijaNalogaObrtaToolStripMenuItem.Name = "evidencijaNalogaObrtaToolStripMenuItem";
            this.evidencijaNalogaObrtaToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.evidencijaNalogaObrtaToolStripMenuItem.Text = "1. Evidencija naloga obrta";
            this.evidencijaNalogaObrtaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaNalogaObrtaToolStripMenuItem_Click);
            // 
            // evidencijaIshraneStokeToolStripMenuItem
            // 
            this.evidencijaIshraneStokeToolStripMenuItem.Name = "evidencijaIshraneStokeToolStripMenuItem";
            this.evidencijaIshraneStokeToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.evidencijaIshraneStokeToolStripMenuItem.Text = "2. Evidencija ishrane stoke";
            // 
            // evidPrihodaITroskovaToolStripMenuItem
            // 
            this.evidPrihodaITroskovaToolStripMenuItem.Name = "evidPrihodaITroskovaToolStripMenuItem";
            this.evidPrihodaITroskovaToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.evidPrihodaITroskovaToolStripMenuItem.Text = "3. Evid. prihoda i troskova";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(246, 22);
            this.toolStripMenuItem2.Text = "4. Otpremnice - racun";
            // 
            // proceneToolStripMenuItem
            // 
            this.proceneToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.procenaTezineNaStanjuToolStripMenuItem,
            this.mesecnoStanjeZalihaHraneToolStripMenuItem,
            this.parametriCeneKostanjaToolStripMenuItem,
            this.cenovnikKategorijaStokeToolStripMenuItem,
            this.cenovnikKoncentrataToolStripMenuItem,
            this.kursnaListaToolStripMenuItem});
            this.proceneToolStripMenuItem.Name = "proceneToolStripMenuItem";
            this.proceneToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
            this.proceneToolStripMenuItem.Text = "5. Procene, parametri i cenovnici";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.katalogKategorijaToolStripMenuItem,
            this.katalogPromenaToolStripMenuItem,
            this.katalogKoncentrataToolStripMenuItem,
            this.katalogKupacaStokeToolStripMenuItem,
            this.katalogPrihodaITroskovaToolStripMenuItem});
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(246, 22);
            this.toolStripMenuItem3.Text = "6. Odrzavanje kataloga";
            // 
            // preglediPodatakaToolStripMenuItem
            // 
            this.preglediPodatakaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preglediNalogaObrtaToolStripMenuItem,
            this.preglediIshraneStokeToolStripMenuItem,
            this.preglediProdajeStokeToolStripMenuItem,
            this.prodajaPoKupcimaToolStripMenuItem,
            this.pregledPrihodaITroskovaToolStripMenuItem});
            this.preglediPodatakaToolStripMenuItem.Name = "preglediPodatakaToolStripMenuItem";
            this.preglediPodatakaToolStripMenuItem.Size = new System.Drawing.Size(126, 20);
            this.preglediPodatakaToolStripMenuItem.Text = "2. Pregledi podataka";
            // 
            // analizaListaIObrtaStokeToolStripMenuItem
            // 
            this.analizaListaIObrtaStokeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listStokeIObrtZaKategorijuToolStripMenuItem,
            this.listStanjaStokeNaFarmiToolStripMenuItem,
            this.promenaBrojnogStanjaStokeToolStripMenuItem,
            this.kalkulacijaCeneKostanjaToolStripMenuItem,
            this.prognozaIsporuTovljenikaToolStripMenuItem,
            this.analizaPrevodaPoKategorijamaToolStripMenuItem,
            this.isporukaPoKategorijamaIMesecimaToolStripMenuItem,
            this.planObrtaStadaToolStripMenuItem,
            this.dnevniListStanjaStokeNaFarmiToolStripMenuItem});
            this.analizaListaIObrtaStokeToolStripMenuItem.Name = "analizaListaIObrtaStokeToolStripMenuItem";
            this.analizaListaIObrtaStokeToolStripMenuItem.Size = new System.Drawing.Size(161, 20);
            this.analizaListaIObrtaStokeToolStripMenuItem.Text = "3. Analize lista i obrta stoke";
            // 
            // analizeIshraneStokeToolStripMenuItem
            // 
            this.analizeIshraneStokeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.utrosakKoncentrataToolStripMenuItem,
            this.prirastIUtrosakHranePoMesecuToolStripMenuItem,
            this.analizaUtroskaHraneIIsporukeToolStripMenuItem});
            this.analizeIshraneStokeToolStripMenuItem.Name = "analizeIshraneStokeToolStripMenuItem";
            this.analizeIshraneStokeToolStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.analizeIshraneStokeToolStripMenuItem.Text = "4. Analize ishrane stoke";
            // 
            // analizaPrihodaITroskovaToolStripMenuItem
            // 
            this.analizaPrihodaITroskovaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ukupniPrihodiITroskoviToolStripMenuItem,
            this.prihodiITroskoviPoMesecimaToolStripMenuItem});
            this.analizaPrihodaITroskovaToolStripMenuItem.Name = "analizaPrihodaITroskovaToolStripMenuItem";
            this.analizaPrihodaITroskovaToolStripMenuItem.Size = new System.Drawing.Size(167, 20);
            this.analizaPrihodaITroskovaToolStripMenuItem.Text = "5. Analiza prihoda i troskova";
            // 
            // analizePodPoKategorijiToolStripMenuItem
            // 
            this.analizePodPoKategorijiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizeZaIzabraniPeriodToolStripMenuItem,
            this.uporednaAnalizaPodatakaToolStripMenuItem,
            this.analizaPodatakaPoGodinamaToolStripMenuItem,
            this.obrtPoMesecimaZaKategorijuToolStripMenuItem});
            this.analizePodPoKategorijiToolStripMenuItem.Name = "analizePodPoKategorijiToolStripMenuItem";
            this.analizePodPoKategorijiToolStripMenuItem.Size = new System.Drawing.Size(165, 20);
            this.analizePodPoKategorijiToolStripMenuItem.Text = "6. Analize pod, po kategoriji";
            // 
            // servisniModulToolStripMenuItem
            // 
            this.servisniModulToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reindeksacijaBazePodatakaToolStripMenuItem,
            this.logickaKontrolaPodatakaToolStripMenuItem,
            this.upravljanjeStampacemToolStripMenuItem});
            this.servisniModulToolStripMenuItem.Name = "servisniModulToolStripMenuItem";
            this.servisniModulToolStripMenuItem.Size = new System.Drawing.Size(109, 20);
            this.servisniModulToolStripMenuItem.Text = "7. Servisni modul";
            // 
            // procenaTezineNaStanjuToolStripMenuItem
            // 
            this.procenaTezineNaStanjuToolStripMenuItem.Name = "procenaTezineNaStanjuToolStripMenuItem";
            this.procenaTezineNaStanjuToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.procenaTezineNaStanjuToolStripMenuItem.Text = "1. Procena tezine na stanju";
            // 
            // mesecnoStanjeZalihaHraneToolStripMenuItem
            // 
            this.mesecnoStanjeZalihaHraneToolStripMenuItem.Name = "mesecnoStanjeZalihaHraneToolStripMenuItem";
            this.mesecnoStanjeZalihaHraneToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.mesecnoStanjeZalihaHraneToolStripMenuItem.Text = "2. Mesecno stanje zaliha hrane";
            this.mesecnoStanjeZalihaHraneToolStripMenuItem.Click += new System.EventHandler(this.mesecnoStanjeZalihaHraneToolStripMenuItem_Click);
            // 
            // parametriCeneKostanjaToolStripMenuItem
            // 
            this.parametriCeneKostanjaToolStripMenuItem.Name = "parametriCeneKostanjaToolStripMenuItem";
            this.parametriCeneKostanjaToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.parametriCeneKostanjaToolStripMenuItem.Text = "3. Parametri cene kostanja";
            // 
            // cenovnikKategorijaStokeToolStripMenuItem
            // 
            this.cenovnikKategorijaStokeToolStripMenuItem.Name = "cenovnikKategorijaStokeToolStripMenuItem";
            this.cenovnikKategorijaStokeToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.cenovnikKategorijaStokeToolStripMenuItem.Text = "4. Cenovnik kategorija stoke";
            // 
            // cenovnikKoncentrataToolStripMenuItem
            // 
            this.cenovnikKoncentrataToolStripMenuItem.Name = "cenovnikKoncentrataToolStripMenuItem";
            this.cenovnikKoncentrataToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.cenovnikKoncentrataToolStripMenuItem.Text = "5. Cenovnik koncentrata";
            // 
            // kursnaListaToolStripMenuItem
            // 
            this.kursnaListaToolStripMenuItem.Name = "kursnaListaToolStripMenuItem";
            this.kursnaListaToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.kursnaListaToolStripMenuItem.Text = "6. Kursna lista";
            // 
            // katalogKategorijaToolStripMenuItem
            // 
            this.katalogKategorijaToolStripMenuItem.Name = "katalogKategorijaToolStripMenuItem";
            this.katalogKategorijaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.katalogKategorijaToolStripMenuItem.Text = "1. Katalog kategorija";
            // 
            // katalogPromenaToolStripMenuItem
            // 
            this.katalogPromenaToolStripMenuItem.Name = "katalogPromenaToolStripMenuItem";
            this.katalogPromenaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.katalogPromenaToolStripMenuItem.Text = "2. Katalog promena";
            // 
            // katalogKoncentrataToolStripMenuItem
            // 
            this.katalogKoncentrataToolStripMenuItem.Name = "katalogKoncentrataToolStripMenuItem";
            this.katalogKoncentrataToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.katalogKoncentrataToolStripMenuItem.Text = "3. Katalog koncentrata";
            // 
            // katalogKupacaStokeToolStripMenuItem
            // 
            this.katalogKupacaStokeToolStripMenuItem.Name = "katalogKupacaStokeToolStripMenuItem";
            this.katalogKupacaStokeToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.katalogKupacaStokeToolStripMenuItem.Text = "4. Katalog kupaca stoke";
            // 
            // katalogPrihodaITroskovaToolStripMenuItem
            // 
            this.katalogPrihodaITroskovaToolStripMenuItem.Name = "katalogPrihodaITroskovaToolStripMenuItem";
            this.katalogPrihodaITroskovaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.katalogPrihodaITroskovaToolStripMenuItem.Text = "5. Katalog prihoda i troskova";
            // 
            // preglediNalogaObrtaToolStripMenuItem
            // 
            this.preglediNalogaObrtaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledPoBrojuNalogaToolStripMenuItem,
            this.pregledNalogaPoDatumuToolStripMenuItem,
            this.pregledNalogaPoKategorijiToolStripMenuItem,
            this.pregleNalogaPoPromeniToolStripMenuItem});
            this.preglediNalogaObrtaToolStripMenuItem.Name = "preglediNalogaObrtaToolStripMenuItem";
            this.preglediNalogaObrtaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.preglediNalogaObrtaToolStripMenuItem.Text = "1. Pregledi naloga obrta";
            // 
            // preglediIshraneStokeToolStripMenuItem
            // 
            this.preglediIshraneStokeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledPoBrojuNalogaToolStripMenuItem1,
            this.pregledNalogaPoDatumuToolStripMenuItem1,
            this.pregledNalogaPoKategorijiToolStripMenuItem1,
            this.pregledNalogaPoKoncentratuToolStripMenuItem});
            this.preglediIshraneStokeToolStripMenuItem.Name = "preglediIshraneStokeToolStripMenuItem";
            this.preglediIshraneStokeToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.preglediIshraneStokeToolStripMenuItem.Text = "2. Pregledi ishrane stoke";
            // 
            // preglediProdajeStokeToolStripMenuItem
            // 
            this.preglediProdajeStokeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledPoBrojuDokumentaToolStripMenuItem,
            this.pregledProdajePoDatumuToolStripMenuItem,
            this.pregledProdajePoKategorijiToolStripMenuItem,
            this.pregledProdajePoKupcuToolStripMenuItem,
            this.pregledPoVrstiDokumentaToolStripMenuItem});
            this.preglediProdajeStokeToolStripMenuItem.Name = "preglediProdajeStokeToolStripMenuItem";
            this.preglediProdajeStokeToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.preglediProdajeStokeToolStripMenuItem.Text = "3. Pregledi prodaje stoke";
            // 
            // prodajaPoKupcimaToolStripMenuItem
            // 
            this.prodajaPoKupcimaToolStripMenuItem.Name = "prodajaPoKupcimaToolStripMenuItem";
            this.prodajaPoKupcimaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.prodajaPoKupcimaToolStripMenuItem.Text = "4. Prodaja po kupcima";
            // 
            // pregledPrihodaITroskovaToolStripMenuItem
            // 
            this.pregledPrihodaITroskovaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledPrihodaITroskovaToolStripMenuItem1,
            this.pregledPrihodaToolStripMenuItem,
            this.pregledTroskovaToolStripMenuItem,
            this.pregledZaIzabranuSifruToolStripMenuItem});
            this.pregledPrihodaITroskovaToolStripMenuItem.Name = "pregledPrihodaITroskovaToolStripMenuItem";
            this.pregledPrihodaITroskovaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pregledPrihodaITroskovaToolStripMenuItem.Text = "5. Pregled prihoda i troskova";
            // 
            // pregledPoBrojuNalogaToolStripMenuItem
            // 
            this.pregledPoBrojuNalogaToolStripMenuItem.Name = "pregledPoBrojuNalogaToolStripMenuItem";
            this.pregledPoBrojuNalogaToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.pregledPoBrojuNalogaToolStripMenuItem.Text = "1. Pregled po broju naloga";
            // 
            // pregledNalogaPoDatumuToolStripMenuItem
            // 
            this.pregledNalogaPoDatumuToolStripMenuItem.Name = "pregledNalogaPoDatumuToolStripMenuItem";
            this.pregledNalogaPoDatumuToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.pregledNalogaPoDatumuToolStripMenuItem.Text = "2. Pregled naloga po datumu";
            // 
            // pregledNalogaPoKategorijiToolStripMenuItem
            // 
            this.pregledNalogaPoKategorijiToolStripMenuItem.Name = "pregledNalogaPoKategorijiToolStripMenuItem";
            this.pregledNalogaPoKategorijiToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.pregledNalogaPoKategorijiToolStripMenuItem.Text = "3. Pregled naloga po kategoriji";
            // 
            // pregleNalogaPoPromeniToolStripMenuItem
            // 
            this.pregleNalogaPoPromeniToolStripMenuItem.Name = "pregleNalogaPoPromeniToolStripMenuItem";
            this.pregleNalogaPoPromeniToolStripMenuItem.Size = new System.Drawing.Size(234, 22);
            this.pregleNalogaPoPromeniToolStripMenuItem.Text = "4. Pregle naloga po promeni";
            // 
            // pregledPoBrojuNalogaToolStripMenuItem1
            // 
            this.pregledPoBrojuNalogaToolStripMenuItem1.Name = "pregledPoBrojuNalogaToolStripMenuItem1";
            this.pregledPoBrojuNalogaToolStripMenuItem1.Size = new System.Drawing.Size(249, 22);
            this.pregledPoBrojuNalogaToolStripMenuItem1.Text = "1. Pregled po broju naloga";
            // 
            // pregledNalogaPoDatumuToolStripMenuItem1
            // 
            this.pregledNalogaPoDatumuToolStripMenuItem1.Name = "pregledNalogaPoDatumuToolStripMenuItem1";
            this.pregledNalogaPoDatumuToolStripMenuItem1.Size = new System.Drawing.Size(249, 22);
            this.pregledNalogaPoDatumuToolStripMenuItem1.Text = "2. Pregled naloga po datumu";
            // 
            // pregledNalogaPoKategorijiToolStripMenuItem1
            // 
            this.pregledNalogaPoKategorijiToolStripMenuItem1.Name = "pregledNalogaPoKategorijiToolStripMenuItem1";
            this.pregledNalogaPoKategorijiToolStripMenuItem1.Size = new System.Drawing.Size(249, 22);
            this.pregledNalogaPoKategorijiToolStripMenuItem1.Text = "3. Pregled naloga po kategoriji";
            // 
            // pregledNalogaPoKoncentratuToolStripMenuItem
            // 
            this.pregledNalogaPoKoncentratuToolStripMenuItem.Name = "pregledNalogaPoKoncentratuToolStripMenuItem";
            this.pregledNalogaPoKoncentratuToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.pregledNalogaPoKoncentratuToolStripMenuItem.Text = "4. Pregled naloga po koncentratu";
            // 
            // pregledPoBrojuDokumentaToolStripMenuItem
            // 
            this.pregledPoBrojuDokumentaToolStripMenuItem.Name = "pregledPoBrojuDokumentaToolStripMenuItem";
            this.pregledPoBrojuDokumentaToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.pregledPoBrojuDokumentaToolStripMenuItem.Text = "1. Pregled po broju dokumenta";
            // 
            // pregledProdajePoDatumuToolStripMenuItem
            // 
            this.pregledProdajePoDatumuToolStripMenuItem.Name = "pregledProdajePoDatumuToolStripMenuItem";
            this.pregledProdajePoDatumuToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.pregledProdajePoDatumuToolStripMenuItem.Text = "2. Pregled prodaje po datumu";
            // 
            // pregledProdajePoKategorijiToolStripMenuItem
            // 
            this.pregledProdajePoKategorijiToolStripMenuItem.Name = "pregledProdajePoKategorijiToolStripMenuItem";
            this.pregledProdajePoKategorijiToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.pregledProdajePoKategorijiToolStripMenuItem.Text = "3. Pregled prodaje po kategoriji";
            // 
            // pregledProdajePoKupcuToolStripMenuItem
            // 
            this.pregledProdajePoKupcuToolStripMenuItem.Name = "pregledProdajePoKupcuToolStripMenuItem";
            this.pregledProdajePoKupcuToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.pregledProdajePoKupcuToolStripMenuItem.Text = "4. Pregled prodaje po kupcu";
            // 
            // pregledPoVrstiDokumentaToolStripMenuItem
            // 
            this.pregledPoVrstiDokumentaToolStripMenuItem.Name = "pregledPoVrstiDokumentaToolStripMenuItem";
            this.pregledPoVrstiDokumentaToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.pregledPoVrstiDokumentaToolStripMenuItem.Text = "5. Pregled po vrsti dokumenta";
            // 
            // pregledPrihodaITroskovaToolStripMenuItem1
            // 
            this.pregledPrihodaITroskovaToolStripMenuItem1.Name = "pregledPrihodaITroskovaToolStripMenuItem1";
            this.pregledPrihodaITroskovaToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.pregledPrihodaITroskovaToolStripMenuItem1.Text = "1. Pregled prihoda i troskova";
            // 
            // pregledPrihodaToolStripMenuItem
            // 
            this.pregledPrihodaToolStripMenuItem.Name = "pregledPrihodaToolStripMenuItem";
            this.pregledPrihodaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pregledPrihodaToolStripMenuItem.Text = "2. Pregled prihoda";
            // 
            // pregledTroskovaToolStripMenuItem
            // 
            this.pregledTroskovaToolStripMenuItem.Name = "pregledTroskovaToolStripMenuItem";
            this.pregledTroskovaToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pregledTroskovaToolStripMenuItem.Text = "3. Pregled troskova";
            // 
            // pregledZaIzabranuSifruToolStripMenuItem
            // 
            this.pregledZaIzabranuSifruToolStripMenuItem.Name = "pregledZaIzabranuSifruToolStripMenuItem";
            this.pregledZaIzabranuSifruToolStripMenuItem.Size = new System.Drawing.Size(224, 22);
            this.pregledZaIzabranuSifruToolStripMenuItem.Text = "4. Pregled za izabranu sifru";
            // 
            // listStokeIObrtZaKategorijuToolStripMenuItem
            // 
            this.listStokeIObrtZaKategorijuToolStripMenuItem.Name = "listStokeIObrtZaKategorijuToolStripMenuItem";
            this.listStokeIObrtZaKategorijuToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.listStokeIObrtZaKategorijuToolStripMenuItem.Text = "1. List stoke i obrt za kategoriju";
            // 
            // listStanjaStokeNaFarmiToolStripMenuItem
            // 
            this.listStanjaStokeNaFarmiToolStripMenuItem.Name = "listStanjaStokeNaFarmiToolStripMenuItem";
            this.listStanjaStokeNaFarmiToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.listStanjaStokeNaFarmiToolStripMenuItem.Text = "2. List stanja stoke na farmi";
            // 
            // promenaBrojnogStanjaStokeToolStripMenuItem
            // 
            this.promenaBrojnogStanjaStokeToolStripMenuItem.Name = "promenaBrojnogStanjaStokeToolStripMenuItem";
            this.promenaBrojnogStanjaStokeToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.promenaBrojnogStanjaStokeToolStripMenuItem.Text = "3. Promena brojnog stanja stoke";
            // 
            // kalkulacijaCeneKostanjaToolStripMenuItem
            // 
            this.kalkulacijaCeneKostanjaToolStripMenuItem.Name = "kalkulacijaCeneKostanjaToolStripMenuItem";
            this.kalkulacijaCeneKostanjaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.kalkulacijaCeneKostanjaToolStripMenuItem.Text = "4. Kalkulacija cene kostanja";
            // 
            // prognozaIsporuTovljenikaToolStripMenuItem
            // 
            this.prognozaIsporuTovljenikaToolStripMenuItem.Name = "prognozaIsporuTovljenikaToolStripMenuItem";
            this.prognozaIsporuTovljenikaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.prognozaIsporuTovljenikaToolStripMenuItem.Text = "5. Prognoza isporu. tovljenika";
            // 
            // analizaPrevodaPoKategorijamaToolStripMenuItem
            // 
            this.analizaPrevodaPoKategorijamaToolStripMenuItem.Name = "analizaPrevodaPoKategorijamaToolStripMenuItem";
            this.analizaPrevodaPoKategorijamaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.analizaPrevodaPoKategorijamaToolStripMenuItem.Text = "6. Analiza prevoda po kategorijama";
            // 
            // isporukaPoKategorijamaIMesecimaToolStripMenuItem
            // 
            this.isporukaPoKategorijamaIMesecimaToolStripMenuItem.Name = "isporukaPoKategorijamaIMesecimaToolStripMenuItem";
            this.isporukaPoKategorijamaIMesecimaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.isporukaPoKategorijamaIMesecimaToolStripMenuItem.Text = "7. Isporuka po kategorijama i mesecima";
            // 
            // planObrtaStadaToolStripMenuItem
            // 
            this.planObrtaStadaToolStripMenuItem.Name = "planObrtaStadaToolStripMenuItem";
            this.planObrtaStadaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.planObrtaStadaToolStripMenuItem.Text = "8. Plan obrta stada";
            // 
            // dnevniListStanjaStokeNaFarmiToolStripMenuItem
            // 
            this.dnevniListStanjaStokeNaFarmiToolStripMenuItem.Name = "dnevniListStanjaStokeNaFarmiToolStripMenuItem";
            this.dnevniListStanjaStokeNaFarmiToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.dnevniListStanjaStokeNaFarmiToolStripMenuItem.Text = "9. Dnevni list stanja stoke na farmi";
            // 
            // utrosakKoncentrataToolStripMenuItem
            // 
            this.utrosakKoncentrataToolStripMenuItem.Name = "utrosakKoncentrataToolStripMenuItem";
            this.utrosakKoncentrataToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.utrosakKoncentrataToolStripMenuItem.Text = "1. Utrosak koncentrata";
            // 
            // prirastIUtrosakHranePoMesecuToolStripMenuItem
            // 
            this.prirastIUtrosakHranePoMesecuToolStripMenuItem.Name = "prirastIUtrosakHranePoMesecuToolStripMenuItem";
            this.prirastIUtrosakHranePoMesecuToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.prirastIUtrosakHranePoMesecuToolStripMenuItem.Text = "2. Prirast i utrosak hrane po mesecu";
            // 
            // analizaUtroskaHraneIIsporukeToolStripMenuItem
            // 
            this.analizaUtroskaHraneIIsporukeToolStripMenuItem.Name = "analizaUtroskaHraneIIsporukeToolStripMenuItem";
            this.analizaUtroskaHraneIIsporukeToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.analizaUtroskaHraneIIsporukeToolStripMenuItem.Text = "3. Analiza utroska hrane i isporuke";
            // 
            // ukupniPrihodiITroskoviToolStripMenuItem
            // 
            this.ukupniPrihodiITroskoviToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledPrihodaITroskovaToolStripMenuItem2,
            this.pregledPrihodaToolStripMenuItem1,
            this.pregledTroskovaToolStripMenuItem1,
            this.pregledZaIzabranuSifruToolStripMenuItem1});
            this.ukupniPrihodiITroskoviToolStripMenuItem.Name = "ukupniPrihodiITroskoviToolStripMenuItem";
            this.ukupniPrihodiITroskoviToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.ukupniPrihodiITroskoviToolStripMenuItem.Text = "1. Ukupni prihodi i troskovi";
            // 
            // prihodiITroskoviPoMesecimaToolStripMenuItem
            // 
            this.prihodiITroskoviPoMesecimaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.preglediPrihodaITroskovaToolStripMenuItem,
            this.pregledPrihodaToolStripMenuItem2,
            this.pregledTroskovaToolStripMenuItem2,
            this.pregledZaIzabranuSifruToolStripMenuItem2});
            this.prihodiITroskoviPoMesecimaToolStripMenuItem.Name = "prihodiITroskoviPoMesecimaToolStripMenuItem";
            this.prihodiITroskoviPoMesecimaToolStripMenuItem.Size = new System.Drawing.Size(249, 22);
            this.prihodiITroskoviPoMesecimaToolStripMenuItem.Text = "2. Prihodi i troskovi po mesecima";
            // 
            // pregledPrihodaITroskovaToolStripMenuItem2
            // 
            this.pregledPrihodaITroskovaToolStripMenuItem2.Name = "pregledPrihodaITroskovaToolStripMenuItem2";
            this.pregledPrihodaITroskovaToolStripMenuItem2.Size = new System.Drawing.Size(224, 22);
            this.pregledPrihodaITroskovaToolStripMenuItem2.Text = "1. Pregled prihoda i troskova";
            // 
            // pregledPrihodaToolStripMenuItem1
            // 
            this.pregledPrihodaToolStripMenuItem1.Name = "pregledPrihodaToolStripMenuItem1";
            this.pregledPrihodaToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.pregledPrihodaToolStripMenuItem1.Text = "2. Pregled prihoda";
            // 
            // pregledTroskovaToolStripMenuItem1
            // 
            this.pregledTroskovaToolStripMenuItem1.Name = "pregledTroskovaToolStripMenuItem1";
            this.pregledTroskovaToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.pregledTroskovaToolStripMenuItem1.Text = "3. Pregled troskova";
            // 
            // pregledZaIzabranuSifruToolStripMenuItem1
            // 
            this.pregledZaIzabranuSifruToolStripMenuItem1.Name = "pregledZaIzabranuSifruToolStripMenuItem1";
            this.pregledZaIzabranuSifruToolStripMenuItem1.Size = new System.Drawing.Size(224, 22);
            this.pregledZaIzabranuSifruToolStripMenuItem1.Text = "4. Pregled za izabranu sifru";
            // 
            // preglediPrihodaITroskovaToolStripMenuItem
            // 
            this.preglediPrihodaITroskovaToolStripMenuItem.Name = "preglediPrihodaITroskovaToolStripMenuItem";
            this.preglediPrihodaITroskovaToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.preglediPrihodaITroskovaToolStripMenuItem.Text = "1. Pregledi prihoda i troskova";
            // 
            // pregledPrihodaToolStripMenuItem2
            // 
            this.pregledPrihodaToolStripMenuItem2.Name = "pregledPrihodaToolStripMenuItem2";
            this.pregledPrihodaToolStripMenuItem2.Size = new System.Drawing.Size(227, 22);
            this.pregledPrihodaToolStripMenuItem2.Text = "2. Pregled prihoda";
            // 
            // pregledTroskovaToolStripMenuItem2
            // 
            this.pregledTroskovaToolStripMenuItem2.Name = "pregledTroskovaToolStripMenuItem2";
            this.pregledTroskovaToolStripMenuItem2.Size = new System.Drawing.Size(227, 22);
            this.pregledTroskovaToolStripMenuItem2.Text = "3. Pregled troskova";
            // 
            // pregledZaIzabranuSifruToolStripMenuItem2
            // 
            this.pregledZaIzabranuSifruToolStripMenuItem2.Name = "pregledZaIzabranuSifruToolStripMenuItem2";
            this.pregledZaIzabranuSifruToolStripMenuItem2.Size = new System.Drawing.Size(227, 22);
            this.pregledZaIzabranuSifruToolStripMenuItem2.Text = "4. Pregled za izabranu sifru";
            // 
            // analizeZaIzabraniPeriodToolStripMenuItem
            // 
            this.analizeZaIzabraniPeriodToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizaUginucaToolStripMenuItem,
            this.analizaPrirastaToolStripMenuItem,
            this.analizaProdajeisporukeToolStripMenuItem,
            this.analizaPrinudnogKlanjaToolStripMenuItem,
            this.analizaStanjaStokeNaFarmiToolStripMenuItem,
            this.analizaUtroskaKoncentrataToolStripMenuItem});
            this.analizeZaIzabraniPeriodToolStripMenuItem.Name = "analizeZaIzabraniPeriodToolStripMenuItem";
            this.analizeZaIzabraniPeriodToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.analizeZaIzabraniPeriodToolStripMenuItem.Text = "1. Analize za izabrani period";
            // 
            // uporednaAnalizaPodatakaToolStripMenuItem
            // 
            this.uporednaAnalizaPodatakaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizaUginucaToolStripMenuItem1,
            this.analizaPrirastaToolStripMenuItem1,
            this.analizaProdajeisporukeToolStripMenuItem1,
            this.analizaPrinudnogKlanjaToolStripMenuItem1,
            this.analizaStanjaStokeNaFarmiToolStripMenuItem1,
            this.analizaUtroskaKoncentrataToolStripMenuItem1});
            this.uporednaAnalizaPodatakaToolStripMenuItem.Name = "uporednaAnalizaPodatakaToolStripMenuItem";
            this.uporednaAnalizaPodatakaToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.uporednaAnalizaPodatakaToolStripMenuItem.Text = "2. Uporedna analiza podataka";
            // 
            // analizaPodatakaPoGodinamaToolStripMenuItem
            // 
            this.analizaPodatakaPoGodinamaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizaUginucaToolStripMenuItem2,
            this.analizaPrirastaToolStripMenuItem2,
            this.analizaProdajeisporukeToolStripMenuItem2,
            this.analizaPrinudnogKlanjaToolStripMenuItem2,
            this.analizaStanjaStokeNaFarmiToolStripMenuItem2,
            this.analizaUtroskaKoncentrataToolStripMenuItem2});
            this.analizaPodatakaPoGodinamaToolStripMenuItem.Name = "analizaPodatakaPoGodinamaToolStripMenuItem";
            this.analizaPodatakaPoGodinamaToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.analizaPodatakaPoGodinamaToolStripMenuItem.Text = "3. Analiza podataka po godinama";
            // 
            // obrtPoMesecimaZaKategorijuToolStripMenuItem
            // 
            this.obrtPoMesecimaZaKategorijuToolStripMenuItem.Name = "obrtPoMesecimaZaKategorijuToolStripMenuItem";
            this.obrtPoMesecimaZaKategorijuToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.obrtPoMesecimaZaKategorijuToolStripMenuItem.Text = "4. Obrt po mesecima za kategoriju";
            // 
            // analizaUginucaToolStripMenuItem
            // 
            this.analizaUginucaToolStripMenuItem.Name = "analizaUginucaToolStripMenuItem";
            this.analizaUginucaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.analizaUginucaToolStripMenuItem.Text = "1. Analiza uginuca";
            // 
            // analizaPrirastaToolStripMenuItem
            // 
            this.analizaPrirastaToolStripMenuItem.Name = "analizaPrirastaToolStripMenuItem";
            this.analizaPrirastaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.analizaPrirastaToolStripMenuItem.Text = "2. Analiza prirasta";
            // 
            // analizaProdajeisporukeToolStripMenuItem
            // 
            this.analizaProdajeisporukeToolStripMenuItem.Name = "analizaProdajeisporukeToolStripMenuItem";
            this.analizaProdajeisporukeToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.analizaProdajeisporukeToolStripMenuItem.Text = "3. Analiza prodaje/isporuke";
            // 
            // analizaPrinudnogKlanjaToolStripMenuItem
            // 
            this.analizaPrinudnogKlanjaToolStripMenuItem.Name = "analizaPrinudnogKlanjaToolStripMenuItem";
            this.analizaPrinudnogKlanjaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.analizaPrinudnogKlanjaToolStripMenuItem.Text = "4. Analiza prinudnog klanja";
            // 
            // analizaStanjaStokeNaFarmiToolStripMenuItem
            // 
            this.analizaStanjaStokeNaFarmiToolStripMenuItem.Name = "analizaStanjaStokeNaFarmiToolStripMenuItem";
            this.analizaStanjaStokeNaFarmiToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.analizaStanjaStokeNaFarmiToolStripMenuItem.Text = "5. Analiza stanja stoke na farmi";
            // 
            // analizaUtroskaKoncentrataToolStripMenuItem
            // 
            this.analizaUtroskaKoncentrataToolStripMenuItem.Name = "analizaUtroskaKoncentrataToolStripMenuItem";
            this.analizaUtroskaKoncentrataToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.analizaUtroskaKoncentrataToolStripMenuItem.Text = "6. Analiza utroska koncentrata";
            // 
            // analizaUginucaToolStripMenuItem1
            // 
            this.analizaUginucaToolStripMenuItem1.Name = "analizaUginucaToolStripMenuItem1";
            this.analizaUginucaToolStripMenuItem1.Size = new System.Drawing.Size(236, 22);
            this.analizaUginucaToolStripMenuItem1.Text = "1. Analiza uginuca";
            this.analizaUginucaToolStripMenuItem1.Click += new System.EventHandler(this.analizaUginucaToolStripMenuItem1_Click);
            // 
            // analizaPrirastaToolStripMenuItem1
            // 
            this.analizaPrirastaToolStripMenuItem1.Name = "analizaPrirastaToolStripMenuItem1";
            this.analizaPrirastaToolStripMenuItem1.Size = new System.Drawing.Size(236, 22);
            this.analizaPrirastaToolStripMenuItem1.Text = "2. Analiza prirasta";
            // 
            // analizaProdajeisporukeToolStripMenuItem1
            // 
            this.analizaProdajeisporukeToolStripMenuItem1.Name = "analizaProdajeisporukeToolStripMenuItem1";
            this.analizaProdajeisporukeToolStripMenuItem1.Size = new System.Drawing.Size(236, 22);
            this.analizaProdajeisporukeToolStripMenuItem1.Text = "3. Analiza prodaje/isporuke";
            // 
            // analizaPrinudnogKlanjaToolStripMenuItem1
            // 
            this.analizaPrinudnogKlanjaToolStripMenuItem1.Name = "analizaPrinudnogKlanjaToolStripMenuItem1";
            this.analizaPrinudnogKlanjaToolStripMenuItem1.Size = new System.Drawing.Size(236, 22);
            this.analizaPrinudnogKlanjaToolStripMenuItem1.Text = "4. Analiza prinudnog klanja";
            // 
            // analizaStanjaStokeNaFarmiToolStripMenuItem1
            // 
            this.analizaStanjaStokeNaFarmiToolStripMenuItem1.Name = "analizaStanjaStokeNaFarmiToolStripMenuItem1";
            this.analizaStanjaStokeNaFarmiToolStripMenuItem1.Size = new System.Drawing.Size(236, 22);
            this.analizaStanjaStokeNaFarmiToolStripMenuItem1.Text = "5. Analiza stanja stoke na farmi";
            // 
            // analizaUtroskaKoncentrataToolStripMenuItem1
            // 
            this.analizaUtroskaKoncentrataToolStripMenuItem1.Name = "analizaUtroskaKoncentrataToolStripMenuItem1";
            this.analizaUtroskaKoncentrataToolStripMenuItem1.Size = new System.Drawing.Size(236, 22);
            this.analizaUtroskaKoncentrataToolStripMenuItem1.Text = "6. Analiza utroska koncentrata";
            // 
            // analizaUginucaToolStripMenuItem2
            // 
            this.analizaUginucaToolStripMenuItem2.Name = "analizaUginucaToolStripMenuItem2";
            this.analizaUginucaToolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.analizaUginucaToolStripMenuItem2.Text = "1. Analiza uginuca";
            // 
            // analizaPrirastaToolStripMenuItem2
            // 
            this.analizaPrirastaToolStripMenuItem2.Name = "analizaPrirastaToolStripMenuItem2";
            this.analizaPrirastaToolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.analizaPrirastaToolStripMenuItem2.Text = "2. Analiza prirasta";
            // 
            // analizaProdajeisporukeToolStripMenuItem2
            // 
            this.analizaProdajeisporukeToolStripMenuItem2.Name = "analizaProdajeisporukeToolStripMenuItem2";
            this.analizaProdajeisporukeToolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.analizaProdajeisporukeToolStripMenuItem2.Text = "3. Analiza prodaje/isporuke";
            // 
            // analizaPrinudnogKlanjaToolStripMenuItem2
            // 
            this.analizaPrinudnogKlanjaToolStripMenuItem2.Name = "analizaPrinudnogKlanjaToolStripMenuItem2";
            this.analizaPrinudnogKlanjaToolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.analizaPrinudnogKlanjaToolStripMenuItem2.Text = "4. Analiza prinudnog klanja";
            // 
            // analizaStanjaStokeNaFarmiToolStripMenuItem2
            // 
            this.analizaStanjaStokeNaFarmiToolStripMenuItem2.Name = "analizaStanjaStokeNaFarmiToolStripMenuItem2";
            this.analizaStanjaStokeNaFarmiToolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.analizaStanjaStokeNaFarmiToolStripMenuItem2.Text = "5. Analiza stanja stoke na farmi";
            // 
            // analizaUtroskaKoncentrataToolStripMenuItem2
            // 
            this.analizaUtroskaKoncentrataToolStripMenuItem2.Name = "analizaUtroskaKoncentrataToolStripMenuItem2";
            this.analizaUtroskaKoncentrataToolStripMenuItem2.Size = new System.Drawing.Size(236, 22);
            this.analizaUtroskaKoncentrataToolStripMenuItem2.Text = "6. Analiza utroska koncentrata";
            // 
            // reindeksacijaBazePodatakaToolStripMenuItem
            // 
            this.reindeksacijaBazePodatakaToolStripMenuItem.Name = "reindeksacijaBazePodatakaToolStripMenuItem";
            this.reindeksacijaBazePodatakaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.reindeksacijaBazePodatakaToolStripMenuItem.Text = "1. Reindeksacija baze podataka";
            // 
            // logickaKontrolaPodatakaToolStripMenuItem
            // 
            this.logickaKontrolaPodatakaToolStripMenuItem.Name = "logickaKontrolaPodatakaToolStripMenuItem";
            this.logickaKontrolaPodatakaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.logickaKontrolaPodatakaToolStripMenuItem.Text = "2. Logicka kontrola podataka";
            // 
            // upravljanjeStampacemToolStripMenuItem
            // 
            this.upravljanjeStampacemToolStripMenuItem.Name = "upravljanjeStampacemToolStripMenuItem";
            this.upravljanjeStampacemToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.upravljanjeStampacemToolStripMenuItem.Text = "3. Upravljanje stampacem";
            // 
            // ListForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1340, 653);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.ForeColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(900, 650);
            this.Name = "ListForm";
            this.Text = "ListForm";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem bazaPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaNalogaObrtaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaIshraneStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidPrihodaITroskovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem proceneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem procenaTezineNaStanjuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mesecnoStanjeZalihaHraneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parametriCeneKostanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cenovnikKategorijaStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cenovnikKoncentrataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kursnaListaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogKategorijaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogPromenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogKoncentrataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogKupacaStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogPrihodaITroskovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaListaIObrtaStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizeIshraneStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPrihodaITroskovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizePodPoKategorijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servisniModulToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediNalogaObrtaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPoBrojuNalogaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledNalogaPoDatumuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledNalogaPoKategorijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregleNalogaPoPromeniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediIshraneStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPoBrojuNalogaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pregledNalogaPoDatumuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pregledNalogaPoKategorijiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pregledNalogaPoKoncentratuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediProdajeStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPoBrojuDokumentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledProdajePoDatumuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledProdajePoKategorijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledProdajePoKupcuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPoVrstiDokumentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prodajaPoKupcimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPrihodaITroskovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPrihodaITroskovaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pregledPrihodaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledTroskovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledZaIzabranuSifruToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listStokeIObrtZaKategorijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listStanjaStokeNaFarmiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promenaBrojnogStanjaStokeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kalkulacijaCeneKostanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prognozaIsporuTovljenikaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPrevodaPoKategorijamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem isporukaPoKategorijamaIMesecimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem planObrtaStadaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dnevniListStanjaStokeNaFarmiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem utrosakKoncentrataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prirastIUtrosakHranePoMesecuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaUtroskaHraneIIsporukeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ukupniPrihodiITroskoviToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPrihodaITroskovaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pregledPrihodaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pregledTroskovaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem pregledZaIzabranuSifruToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem prihodiITroskoviPoMesecimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preglediPrihodaITroskovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPrihodaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pregledTroskovaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pregledZaIzabranuSifruToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem analizeZaIzabraniPeriodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaUginucaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPrirastaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaProdajeisporukeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPrinudnogKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaStanjaStokeNaFarmiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaUtroskaKoncentrataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uporednaAnalizaPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaUginucaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analizaPrirastaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analizaProdajeisporukeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analizaPrinudnogKlanjaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analizaStanjaStokeNaFarmiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analizaUtroskaKoncentrataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analizaPodatakaPoGodinamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obrtPoMesecimaZaKategorijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaUginucaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem analizaPrirastaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem analizaProdajeisporukeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem analizaPrinudnogKlanjaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem analizaStanjaStokeNaFarmiToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem analizaUtroskaKoncentrataToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reindeksacijaBazePodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logickaKontrolaPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upravljanjeStampacemToolStripMenuItem;
    }
}