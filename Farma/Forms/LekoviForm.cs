﻿using Farm.FormeZaLekove;
using Farma.Forms.Lekovi;
using Farma.Forms.Lekovi.Izvestaji;
using Farma.ReportForms.Lekovi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Farm
{
    public partial class LekoviForm : Form
    {
        private Form _basicMenuForm;
        public LekoviForm(Form basicMenuForm)
        {
            _basicMenuForm = basicMenuForm;
            InitializeComponent();
            CustomizeDesign();
        }

        private void LekoviForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            _basicMenuForm.Visible = true;
        }

        private void CustomizeDesign()
        {
            panel1.Visible = false;
            panel8.Visible = false;
            panel9.Visible = false;
            panel10.Visible = false;
        }
        private void HideSubMenu()
        {
            if (panel1.Visible)
            {
                panel1.Visible = false;
            }
            if (panel8.Visible)
            {
                panel8.Visible = false;
            }
            if (panel9.Visible)
            {
                panel9.Visible = false;
            }
            if (panel10.Visible)
            {
                panel10.Visible = false;
            }
        }
        private void ShowSubMenu(Panel subMenu)
        {
            if (!subMenu.Visible)
            {
                HideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }
        private void ShowSubMenu1(Panel subMenu, Panel subMenu2)
        {
            if (!subMenu.Visible)
            {
                HideSubMenu();
                subMenu.Visible = true;
                subMenu2.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
                subMenu2.Visible = false;
            }
        }

        private void buttonBazaPodataka_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panel1);
        }

        private void buttonArtikli_Click(object sender, EventArgs e)
        {
            OpenChildForm(new ArtikliForm());
            //...
            HideSubMenu();
        }

        private void buttonVrsteArtikala_Click(object sender, EventArgs e)
        {
            OpenChildForm(new VrsteArtiklaForm());
            //...
            HideSubMenu();
        }

        private Form activeForm = null;
        private void OpenChildForm(Form childForm)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }

        private void LekoviForm_Load(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            //ShowSubMenu1(panel5, panel3);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //...
            HideSubMenu();
        }

        private void buttonPrijemnicaLekova_Click(object sender, EventArgs e)
        {
            //...
            HideSubMenu();
            OpenChildForm(new PrijemnicaLekovaForm());
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new IzdatnicaLekovaForm());
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //...
            HideSubMenu();
        }

        private void button36_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panel8);
        }

        private void button45_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panel9);
        }

        private void button51_Click(object sender, EventArgs e)
        {
            ShowSubMenu(panel10);
        }

        private void button38_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new KarticaLekaForm());
        }

        private void panelChildForm_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button31_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new VrsteArtiklaForm());
        }

        private void button29_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new KatalogLekovaForm());
        }

        private void button33_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new MestoTroskaForm());
        }

        private void button30_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PartnerForm());
        }

        private void button32_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new VeterinarskiTehnicarForm());
        }

        private void button39_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PromenePoVrstiDokumentaForm());
        }

        private void button37_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new StanjeLagera());
        }

        private void button44_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new StanjeLageraNaDanForm());
        }

        private void button43_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PromeneZaPeriodForm());
        }

        private void button46_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PoPoslovnimPartnerimaForm());
        }

        private void button47_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new KarticaPoslovnogPartnera());
        }

        private void button49_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PregledPoVeterinarskimTehnicarima());
        }

        private void button50_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PregledPoMestimaTroska());
        }

        private void button48_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new LekoviZaNabavku());
        }

        private void button53_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new BackUpBazeForm());
        }

        private void button52_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new LogickaKontrolaPodataka());
        }

        private void button35_Click(object sender, EventArgs e)
        {
            HideSubMenu();
            OpenChildForm(new PocetnoStanje());
        }
    }
}
