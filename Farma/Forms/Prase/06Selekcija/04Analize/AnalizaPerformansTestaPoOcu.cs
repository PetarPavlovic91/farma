﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._06Selekcija._04Analize
{
    public partial class AnalizaPerformansTestaPoOcu : Form
    {
        public AnalizaPerformansTestaPoOcu()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var kramca = true;
                if (radioButton2.Checked)
                {
                    kramca = false;
                }
                SelekcijaService selekcijaService = new SelekcijaService();
                dataGridView1.DataSource = selekcijaService.AnalizaPerformansTestaPoOcu(dateTimePicker1.Value, dateTimePicker2.Value, kramca, textBox1.Text, textBox2.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var kramca = true;
            if (radioButton2.Checked)
            {
                kramca = false;
            }
            Helper.OpenNewForm(new AnalizaPerformansTestaPoOcuReportForm(dateTimePicker1.Value, dateTimePicker2.Value, kramca, textBox1.Text, textBox2.Text));
        }
    }
}
