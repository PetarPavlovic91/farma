﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._06Selekcija._03SelekcijskiIndeksi
{
    public partial class ProgeniTestKrmacaINerastovaSI3 : Form
    {
        public ProgeniTestKrmacaINerastovaSI3()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                SelekcijaService selekcijaService = new SelekcijaService();
                var krmace = true;
                if (radioButton2.Checked)
                {
                    krmace = false;
                }
                dataGridView1.DataSource = selekcijaService.ProgeniTestKrmacaINerastovaSI3(textBox1.Text, krmace);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var krmace = true;
            if (radioButton2.Checked)
            {
                krmace = false;
            }
            Helper.OpenNewForm(new ProgeniTestKrmacaINerastovaSI3ReportForm(textBox1.Text, krmace));
        }
    }
}
