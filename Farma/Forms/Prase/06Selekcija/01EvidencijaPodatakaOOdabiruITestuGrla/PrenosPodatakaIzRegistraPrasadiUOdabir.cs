﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._06Selekcija;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla
{
    public partial class PrenosPodatakaIzRegistraPrasadiUOdabir : Form
    {
        private List<PrenosPodatakaIzRegistraPrasadiUOdabirVM> _listaZaPrenos;
        public PrenosPodatakaIzRegistraPrasadiUOdabir()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
            dataGridView1.MultiSelect = true;
            _listaZaPrenos = new List<PrenosPodatakaIzRegistraPrasadiUOdabirVM>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SelekcijaService selekcijaService = new SelekcijaService();
                _listaZaPrenos = selekcijaService.GetPrenosPodatakaIzRegistraPrasadiUOdabir(dateTimePicker1.Value, dateTimePicker2.Value, dateTimePicker3.Value);
                if (_listaZaPrenos.Count > 0)
                {
                    dataGridView1.DataSource = _listaZaPrenos;
                }
                else
                {
                    MessageBox.Show("Za period od " + dateTimePicker1.Value + " do " + dateTimePicker2.Value.ToString() + " nema unesesenih ili su sva grla prenesena.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> selectedRowRbs = dataGridView1.SelectedRows
                .OfType<DataGridViewRow>()
                .Where(row => !row.IsNewRow)
                .Select(x => Convert.ToInt32(x.Cells[0].Value)).ToList();

                var listaZaPrenos = _listaZaPrenos.Where(x => !selectedRowRbs.Contains(x.Id)).ToList();

                SelekcijaService selekcijaService = new SelekcijaService();
                var result = selekcijaService.PrenesiPodatkeIzRegistraPrasadiUOdabir(listaZaPrenos);

                _listaZaPrenos = selekcijaService.GetPrenosPodatakaIzRegistraPrasadiUOdabir(dateTimePicker1.Value, dateTimePicker2.Value, dateTimePicker3.Value);
                if (_listaZaPrenos.Count > 0)
                {
                    dataGridView1.DataSource = _listaZaPrenos;
                }
                else
                {
                    MessageBox.Show("Za period od " + dateTimePicker1.Value + " do " + dateTimePicker2.Value.ToString() + " nema unesesenih ili su sva grla prenesena.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
