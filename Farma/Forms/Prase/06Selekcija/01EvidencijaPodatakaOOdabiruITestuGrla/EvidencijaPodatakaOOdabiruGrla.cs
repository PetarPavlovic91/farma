﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla
{
    public partial class EvidencijaPodatakaOOdabiruGrla : Form
    {
        public EvidencijaPodatakaOOdabiruGrla()
        {
            InitializeComponent();
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPodatakaOOdabiruGrlaPregled());
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPodatakaOOdabiruGrlaAzuriranje());
        }
    }
}
