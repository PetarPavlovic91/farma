﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._06Selekcija;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla
{
    public partial class EvidencijaPodatakaOOdabiruGrlaAzuriranje : Form
    {
        public EvidencijaPodatakaOOdabiruGrlaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SelekcijaService selekcijaService = new SelekcijaService();
                dataGridView1.DataSource = selekcijaService.EvidencijaPodatakaOOdabiruGrlaPregled(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@Tezina", row.Cells["Tezina"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Tezina"].Value)));
                    list.Add(new SqlParameter("@Prirast", row.Cells["Prirast"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Prirast"].Value)));
                    list.Add(new SqlParameter("@IzLegla", row.Cells["IzLegla"].Value == null ? null : row.Cells["IzLegla"].Value.ToString()));
                    list.Add(new SqlParameter("@Ekst", row.Cells["Ekst"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Ekst"].Value)));
                    list.Add(new SqlParameter("@Markica", row.Cells["Markica"].Value == null ? null : row.Cells["Markica"].Value.ToString()));
                    list.Add(new SqlParameter("@Sisa", row.Cells["Sisa"].Value == null ? null : row.Cells["Sisa"].Value.ToString()));
                    SelekcijaService selekcijaService = new SelekcijaService();
                    selekcijaService.SP_Save_Odabir(list);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //private void button3_Click(object sender, EventArgs e)
        //{

        //    try
        //    {
        //        var id = int.Parse(textBox6.Text);
        //        SelekcijaService selekcijaService = new SelekcijaService();
        //        var result = selekcijaService.DeleteOdabira(id);
        //        if (!result.Success)
        //        {
        //            MessageBox.Show(result.Message);
        //        }

        //        dataGridView1.DataSource = selekcijaService.EvidencijaPodatakaOOdabiruGrlaPregled(dateTimePicker1.Value);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}
    }
}
