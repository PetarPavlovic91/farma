﻿
namespace Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla
{
    partial class EvidencijaPodatakaSaTestaGrlaZaPriplodAzuriranje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tetovir = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rasa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rodjeno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mat_br_oca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mat_br_majke = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Iz_legla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ekst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sisa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ulaz_u_test = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Poc_tez = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tezina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Hrana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.L_sl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.B_sl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_sl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.D_mis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Procenat_Mesa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Si1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Iskljuceno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Razl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Napomena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.U_priplodu_od = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Markica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.textBox8);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 62);
            this.panel1.TabIndex = 0;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(466, 20);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(162, 20);
            this.textBox8.TabIndex = 9;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Cornsilk;
            this.label10.Location = new System.Drawing.Point(387, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Maticni broj";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Cornsilk;
            this.label13.Location = new System.Drawing.Point(26, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Datum testa";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(711, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Pretrazi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(158, 19);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1171, 503);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Pol,
            this.Tetovir,
            this.Rasa,
            this.Rodjeno,
            this.Mat_br_oca,
            this.Mat_br_majke,
            this.Iz_legla,
            this.Ekst,
            this.Sisa,
            this.Ulaz_u_test,
            this.Poc_tez,
            this.Tezina,
            this.Hrana,
            this.L_sl,
            this.B_sl,
            this.D_sl,
            this.D_mis,
            this.Procenat_Mesa,
            this.Si1,
            this.Iskljuceno,
            this.Razl,
            this.Napomena,
            this.U_priplodu_od,
            this.Markica});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1171, 503);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 565);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1171, 79);
            this.panel3.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // Pol
            // 
            this.Pol.DataPropertyName = "Pol";
            this.Pol.HeaderText = "Pol";
            this.Pol.Name = "Pol";
            this.Pol.ReadOnly = true;
            // 
            // Tetovir
            // 
            this.Tetovir.DataPropertyName = "Tetovir";
            this.Tetovir.HeaderText = "Tetovir";
            this.Tetovir.Name = "Tetovir";
            this.Tetovir.ReadOnly = true;
            // 
            // Rasa
            // 
            this.Rasa.DataPropertyName = "Rasa";
            this.Rasa.HeaderText = "Rasa";
            this.Rasa.Name = "Rasa";
            this.Rasa.ReadOnly = true;
            // 
            // Rodjeno
            // 
            this.Rodjeno.DataPropertyName = "Rodjeno";
            this.Rodjeno.HeaderText = "Rodjeno";
            this.Rodjeno.Name = "Rodjeno";
            this.Rodjeno.ReadOnly = true;
            // 
            // Mat_br_oca
            // 
            this.Mat_br_oca.DataPropertyName = "Mat_br_oca";
            this.Mat_br_oca.HeaderText = "Mat_br_oca";
            this.Mat_br_oca.Name = "Mat_br_oca";
            this.Mat_br_oca.ReadOnly = true;
            // 
            // Mat_br_majke
            // 
            this.Mat_br_majke.DataPropertyName = "Mat_br_majke";
            this.Mat_br_majke.HeaderText = "Mat_br_majke";
            this.Mat_br_majke.Name = "Mat_br_majke";
            this.Mat_br_majke.ReadOnly = true;
            // 
            // Iz_legla
            // 
            this.Iz_legla.DataPropertyName = "Iz_legla";
            this.Iz_legla.HeaderText = "Iz_legla";
            this.Iz_legla.Name = "Iz_legla";
            this.Iz_legla.ReadOnly = true;
            // 
            // Ekst
            // 
            this.Ekst.DataPropertyName = "Ekst";
            this.Ekst.HeaderText = "Ekst";
            this.Ekst.Name = "Ekst";
            // 
            // Sisa
            // 
            this.Sisa.DataPropertyName = "Sisa";
            this.Sisa.HeaderText = "Sisa";
            this.Sisa.Name = "Sisa";
            // 
            // Ulaz_u_test
            // 
            this.Ulaz_u_test.DataPropertyName = "Ulaz_u_test";
            this.Ulaz_u_test.HeaderText = "Ulaz_u_test";
            this.Ulaz_u_test.Name = "Ulaz_u_test";
            // 
            // Poc_tez
            // 
            this.Poc_tez.DataPropertyName = "Poc_tez";
            this.Poc_tez.HeaderText = "Poc_tez";
            this.Poc_tez.Name = "Poc_tez";
            // 
            // Tezina
            // 
            this.Tezina.DataPropertyName = "Tezina";
            this.Tezina.HeaderText = "Tezina";
            this.Tezina.Name = "Tezina";
            // 
            // Hrana
            // 
            this.Hrana.DataPropertyName = "Hrana";
            this.Hrana.HeaderText = "Hrana";
            this.Hrana.Name = "Hrana";
            // 
            // L_sl
            // 
            this.L_sl.DataPropertyName = "L_sl";
            this.L_sl.HeaderText = "L_sl";
            this.L_sl.Name = "L_sl";
            // 
            // B_sl
            // 
            this.B_sl.DataPropertyName = "B_sl";
            this.B_sl.HeaderText = "B_sl";
            this.B_sl.Name = "B_sl";
            // 
            // D_sl
            // 
            this.D_sl.DataPropertyName = "D_sl";
            this.D_sl.HeaderText = "D_sl";
            this.D_sl.Name = "D_sl";
            // 
            // D_mis
            // 
            this.D_mis.DataPropertyName = "D_mis";
            this.D_mis.HeaderText = "D_mis";
            this.D_mis.Name = "D_mis";
            // 
            // Procenat_Mesa
            // 
            this.Procenat_Mesa.DataPropertyName = "Procenat_Mesa";
            this.Procenat_Mesa.HeaderText = "Procenat_Mesa";
            this.Procenat_Mesa.Name = "Procenat_Mesa";
            // 
            // Si1
            // 
            this.Si1.DataPropertyName = "Si1";
            this.Si1.HeaderText = "Si1";
            this.Si1.Name = "Si1";
            // 
            // Iskljuceno
            // 
            this.Iskljuceno.DataPropertyName = "Iskljuceno";
            this.Iskljuceno.HeaderText = "Iskljuceno";
            this.Iskljuceno.Name = "Iskljuceno";
            this.Iskljuceno.ReadOnly = true;
            // 
            // Razl
            // 
            this.Razl.DataPropertyName = "Razl";
            this.Razl.HeaderText = "Razl";
            this.Razl.Name = "Razl";
            this.Razl.ReadOnly = true;
            // 
            // Napomena
            // 
            this.Napomena.DataPropertyName = "Napomena";
            this.Napomena.HeaderText = "Napomena";
            this.Napomena.Name = "Napomena";
            // 
            // U_priplodu_od
            // 
            this.U_priplodu_od.DataPropertyName = "U_priplodu_od";
            this.U_priplodu_od.HeaderText = "U_priplodu_od";
            this.U_priplodu_od.Name = "U_priplodu_od";
            // 
            // Markica
            // 
            this.Markica.DataPropertyName = "Markica";
            this.Markica.HeaderText = "Markica";
            this.Markica.Name = "Markica";
            // 
            // EvidencijaPodatakaSaTestaGrlaZaPriplodAzuriranje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1171, 644);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "EvidencijaPodatakaSaTestaGrlaZaPriplodAzuriranje";
            this.Text = "Evidencija Podataka Sa Testa Grla Za Priplod - Azuriranje";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tetovir;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rasa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rodjeno;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mat_br_oca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mat_br_majke;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iz_legla;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ekst;
        private System.Windows.Forms.DataGridViewTextBoxColumn Sisa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ulaz_u_test;
        private System.Windows.Forms.DataGridViewTextBoxColumn Poc_tez;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tezina;
        private System.Windows.Forms.DataGridViewTextBoxColumn Hrana;
        private System.Windows.Forms.DataGridViewTextBoxColumn L_sl;
        private System.Windows.Forms.DataGridViewTextBoxColumn B_sl;
        private System.Windows.Forms.DataGridViewTextBoxColumn D_sl;
        private System.Windows.Forms.DataGridViewTextBoxColumn D_mis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Procenat_Mesa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Si1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Iskljuceno;
        private System.Windows.Forms.DataGridViewTextBoxColumn Razl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Napomena;
        private System.Windows.Forms.DataGridViewTextBoxColumn U_priplodu_od;
        private System.Windows.Forms.DataGridViewTextBoxColumn Markica;
    }
}