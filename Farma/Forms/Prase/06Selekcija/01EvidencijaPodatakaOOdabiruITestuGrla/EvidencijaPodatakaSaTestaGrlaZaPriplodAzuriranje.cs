﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla
{
    public partial class EvidencijaPodatakaSaTestaGrlaZaPriplodAzuriranje : Form
    {
        public EvidencijaPodatakaSaTestaGrlaZaPriplodAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SelekcijaService selekcijaService = new SelekcijaService();
                dataGridView1.DataSource = selekcijaService.EvidencijaPodatakaSaTestaGrlaZaPriplod(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@Tezina", row.Cells["Tezina"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Tezina"].Value)));
                    list.Add(new SqlParameter("@Poc_tez", row.Cells["Poc_tez"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Poc_tez"].Value)));
                    list.Add(new SqlParameter("@Hrana", row.Cells["Hrana"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Hrana"].Value)));
                    list.Add(new SqlParameter("@L_sl", row.Cells["L_sl"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["L_sl"].Value)));
                    list.Add(new SqlParameter("@B_sl", row.Cells["B_sl"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["B_sl"].Value)));
                    list.Add(new SqlParameter("@D_sl", row.Cells["D_sl"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["D_sl"].Value)));
                    list.Add(new SqlParameter("@D_mis", row.Cells["D_mis"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["D_mis"].Value)));
                    list.Add(new SqlParameter("@Si1", row.Cells["Si1"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Si1"].Value)));
                    list.Add(new SqlParameter("@Procenat_Mesa", row.Cells["Procenat_Mesa"].Value == DBNull.Value ? 0 : Convert.ToDouble(row.Cells["Procenat_Mesa"].Value)));
                    list.Add(new SqlParameter("@Iz_legla", row.Cells["Iz_legla"].Value == null ? null : row.Cells["Iz_legla"].Value.ToString()));
                    list.Add(new SqlParameter("@Ekst", row.Cells["Ekst"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Ekst"].Value)));
                    list.Add(new SqlParameter("@Markica", row.Cells["Markica"].Value == null ? null : row.Cells["Markica"].Value.ToString()));
                    list.Add(new SqlParameter("@Sisa", row.Cells["Sisa"].Value == null ? null : row.Cells["Sisa"].Value.ToString()));
                    list.Add(new SqlParameter("@Ulaz_u_test", row.Cells["Ulaz_u_test"].Value == null ? null : (DateTime?)Convert.ToDateTime(row.Cells["Ulaz_u_test"].Value.ToString())));
                    list.Add(new SqlParameter("@U_priplodu_od", row.Cells["U_priplodu_od"].Value == null ? null : (DateTime?)Convert.ToDateTime(row.Cells["U_priplodu_od"].Value.ToString())));
                    list.Add(new SqlParameter("@Napomena", row.Cells["Napomena"].Value == null ? null : row.Cells["Napomena"].Value.ToString()));
                    SelekcijaService selekcijaService = new SelekcijaService();
                    selekcijaService.SP_Save_Test(list);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
