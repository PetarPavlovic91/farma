﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._04Iskljucenje._03AnalizaIskljucenjaKrmaca
{
    public partial class AnalizaPrinudnogKlanjaKrmaca : Form
    {
        public AnalizaPrinudnogKlanjaKrmaca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IskljucenjeService iskljucenjeService = new IskljucenjeService();
                dataGridView1.DataSource = iskljucenjeService.UkupnaIskljucenjaKrmacaZaOdredjenuGrupu(dateTimePicker1.Value, dateTimePicker2.Value, "7");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPrinudnogKlanjaKrmacaReportForm(dateTimePicker1.Value, dateTimePicker2.Value, "Analiza Prinudnog Klanja Krmaca", "7"));
        }
    }
}
