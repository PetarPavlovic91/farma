﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._04Iskljucenje._01EvidencijaIskljucenja
{
    public partial class EvidencijaIskljucenjaPregled : Form
    {
        public EvidencijaIskljucenjaPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                if (item.Width > Helper.MaxColumnWidth)
                {
                    item.Width = Helper.MaxColumnWidth;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IskljucenjeService iskljucenjeService = new IskljucenjeService();
                dataGridView1.DataSource = iskljucenjeService.EvidencijaIskljucenjaPregledVM(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
