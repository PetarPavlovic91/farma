﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._04Iskljucenje;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._04Iskljucenje._01EvidencijaIskljucenja
{
    public partial class EvidencijaIskljucenjaAzuriranje : Form
    {
        public EvidencijaIskljucenjaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                if (item.Width > Helper.MaxColumnWidth)
                {
                    item.Width = Helper.MaxColumnWidth;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IskljucenjeService iskljucenjeService = new IskljucenjeService();
                dataGridView1.DataSource = iskljucenjeService.SP_GetAllByDate(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    var columnIndex = currentCell.OwningColumn.Index;
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["TETOVIR"].Value == null)
                    {
                        MessageBox.Show("Polje TETOVIR prvo mora biti popunjeno");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@TETOVIR", row.Cells["TETOVIR"].Value == null ? null : row.Cells["TETOVIR"].Value.ToString()));
                    list.Add(new SqlParameter("@RAZLOG", row.Cells["RAZLOG"].Value == null ? null : row.Cells["RAZLOG"].Value.ToString()));
                    list.Add(new SqlParameter("@D_SKART", dateTimePicker1.Value));
                    IskljucenjeService iskljucenjeService = new IskljucenjeService();
                    iskljucenjeService.SP_Save(list);
                    dataGridView1.DataSource = iskljucenjeService.SP_GetAllByDate(dateTimePicker1.Value);
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[columnIndex];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Da li ste sigurni da zelite da izbrisete ovo iskljucenje?", "Brisanje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataGridViewRow row = dataGridView1.CurrentRow;
                        List<SqlParameter> list = new List<SqlParameter>();
                        var iskljucenjeId = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                        list.Add(new SqlParameter("@Id", iskljucenjeId));
                        IskljucenjeService iskljucenjeService = new IskljucenjeService();
                        iskljucenjeService.SP_Delete(list);
                    }
                    else
                        e.Cancel = true;
                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(() => MoveCell()));
        }
        private void MoveCell()
        {
            if (dataGridView1.CurrentCell != null && dataGridView1.CurrentRow != null)
            {
                var row = dataGridView1.CurrentRow;
                var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                if (id == 0)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[2];
                }
            }
        }
    }
}
