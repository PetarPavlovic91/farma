﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.Forms.Prase.Pripusti._02PregledPodataka;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._04Iskljucenje._02PregledPodataka
{
    public partial class PregledPodatakaIskljucenje : Form
    {
        public PregledPodatakaIskljucenje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IskljucenjeService iskljucenjeService = new IskljucenjeService();
                dataGridView1.DataSource = iskljucenjeService.PregledPodatakaIskljucenje(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "1. Iskljucenja Krmaca Po Grupama Razloga")
            {
                Helper.OpenNewForm(new UkupnaIskljucenjaKrmacaPoGrupamaRazloga());
            }
            else if (comboBox1.Text == "2. Iskljucenja Krmaca Po Razlozima")
            {
                Helper.OpenNewForm(new UkupnaIskljucenjaKrmaca());
            }
            else if (comboBox1.Text == "3. Iskljucenja Nerastova Po Razlozima")
            {
                Helper.OpenNewForm(new UkupnaIskljucenjaNerastova());
            }
            else if (comboBox1.Text == "4. Iskljucenja Nazimica Po Razlozima")
            {
                Helper.OpenNewForm(new UkupnaIskljucenjaNazimica());
            }
            else if (comboBox1.Text == "5. Iskljucenje Grla Iz Testa")
            {
                Helper.OpenNewForm(new AnalizaIskljucenjaGrlaIzTesta());
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaIskljucenjeReportForm(dateTimePicker1.Value, dateTimePicker2.Value));
        }
    }
}
