﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._03Zalucenje._03KalendarZalucenja
{
    public partial class LeglaZaTetoviranjePrasadi : Form
    {
        public LeglaZaTetoviranjePrasadi()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var sort = "ObjBox";
                if (radioButton1.Checked)
                {
                    sort = "Dani";
                }
                ZalucenjeService zalucenjeService = new ZalucenjeService();
                dataGridView1.DataSource = zalucenjeService.LeglaZaTetoviranjePrasadi(dateTimePicker1.Value, dateTimePicker2.Value, textBox1.Text, sort);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var sort = "ObjBox";
            if (radioButton1.Checked)
            {
                sort = "Dani";
            }
            Helper.OpenNewForm(new LeglaZaTetoviranjePrasadiReportForm(dateTimePicker1.Value, dateTimePicker2.Value, textBox1.Text, sort));
        }
    }
}
