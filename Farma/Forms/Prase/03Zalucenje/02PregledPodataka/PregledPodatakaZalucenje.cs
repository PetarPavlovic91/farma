﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._03Zalucenje._02PregledPodataka
{
    public partial class PregledPodatakaZalucenje : Form
    {
        public PregledPodatakaZalucenje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ZalucenjeService zalucenjeService = new ZalucenjeService();
                dataGridView1.DataSource = zalucenjeService.PregledPodatakaZalucenja(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZalucenjeReportForm(dateTimePicker1.Value, dateTimePicker2.Value));
        }
    }
}
