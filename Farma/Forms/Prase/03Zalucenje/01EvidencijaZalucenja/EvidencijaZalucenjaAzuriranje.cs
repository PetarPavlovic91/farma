﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Zalucenje;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Zalucenje._01EvidencijaZalucenja
{
    public partial class EvidencijaZalucenjaAzuriranje : Form
    {
        public EvidencijaZalucenjaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            //foreach (DataGridViewColumn item in dataGridView1.Columns)
            //{
            //    if (item.Width > Helper.MaxColumnWidth)
            //    {
            //        item.Width = Helper.MaxColumnWidth;
            //    }
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ZalucenjeService zalucenjeService = new ZalucenjeService();
                dataGridView1.DataSource = zalucenjeService.SP_GetAllByDate(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var currentCell = dataGridView1.CurrentCell;
                if (currentCell != null)
                {

                    var rowIndex = currentCell.OwningRow.Index;
                    var columnIndex = currentCell.OwningColumn.Index;

                    if (dataGridView1.CurrentRow != null)
                    {
                        DataGridViewRow row = dataGridView1.CurrentRow;

                        if (row.Cells["KRMACA"].Value == null)
                        {
                            MessageBox.Show("Polje Krmaca prvo mora biti popunjeno");
                        }

                        var krmaca = row.Cells["Krmaca"].Value == null ? null : row.Cells["Krmaca"].Value.ToString();
                        ZalucenjeService zalucenjeService = new ZalucenjeService();
                        var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                        if (id == 0)
                        {
                            string checkIskljucenje = zalucenjeService.CheckIskljucenje(krmaca);
                            if (checkIskljucenje != null)
                            {
                                MessageBox.Show(checkIskljucenje);
                                return;
                            }
                        }

                        List<SqlParameter> list = new List<SqlParameter>();
                        list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                        list.Add(new SqlParameter("@KRMACA", row.Cells["KRMACA"].Value == null ? null : row.Cells["KRMACA"].Value.ToString()));
                        list.Add(new SqlParameter("@DAT_ZAL", dateTimePicker1.Value));
                        list.Add(new SqlParameter("@PARIT", row.Cells["PARIT"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["PARIT"].Value)));
                        list.Add(new SqlParameter("@OBJ", row.Cells["OBJ"].Value == null ? null : row.Cells["OBJ"].Value.ToString()));
                        list.Add(new SqlParameter("@BOX", row.Cells["BOX"].Value == null ? null : row.Cells["BOX"].Value.ToString()));
                        list.Add(new SqlParameter("@PZ", row.Cells["PZ"].Value == null ? null : row.Cells["PZ"].Value.ToString()));
                        list.Add(new SqlParameter("@RAZ_PZ", row.Cells["RAZ_PZ"].Value == null ? null : row.Cells["RAZ_PZ"].Value.ToString()));
                        list.Add(new SqlParameter("@ZDR_ST", row.Cells["ZDR_ST"].Value == null ? null : row.Cells["ZDR_ST"].Value.ToString()));
                        list.Add(new SqlParameter("@OL_Z", row.Cells["OL_Z"].Value == null ? null : row.Cells["OL_Z"].Value.ToString()));
                        list.Add(new SqlParameter("@ZALUC", row.Cells["ZALUC"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["ZALUC"].Value)));
                        list.Add(new SqlParameter("@TEZ_ZAL", row.Cells["TEZ_ZAL"].Value == DBNull.Value ? 0 : Convert.ToDouble(row.Cells["TEZ_ZAL"].Value)));
                        list.Add(new SqlParameter("@RBZ", row.Cells["RBZ"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["RBZ"].Value)));
                        list.Add(new SqlParameter("@DOJARA", row.Cells["DOJARA"].Value == null ? null : row.Cells["DOJARA"].Value.ToString()));
                        list.Add(new SqlParameter("@REFK", row.Cells["REFK"].Value == null ? null : row.Cells["REFK"].Value.ToString()));
                        zalucenjeService.SP_Save(list);
                        //zalucenjeService.SP_Save_KlasaKrmace(row.Cells["KRMACA"].Value == null ? null : row.Cells["KRMACA"].Value.ToString());
                        dataGridView1.DataSource = zalucenjeService.SP_GetAllByDate(dateTimePicker1.Value);
                        dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[columnIndex];

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Da li ste sigurni da zelite da izbrisete ovo zalucenje?", "Brisanje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataGridViewRow row = dataGridView1.CurrentRow;
                        List<SqlParameter> list = new List<SqlParameter>();
                        var zalucenjeId = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                        list.Add(new SqlParameter("@Id", zalucenjeId));
                        ZalucenjeService zalucenjeService = new ZalucenjeService();
                        if (!zalucenjeService.IsLatestZalucenje(zalucenjeId))
                        {
                            e.Cancel = true;
                            MessageBox.Show("Neuspesno brisanje. Morate prvo izbrisati poslednje uneta zalucenja za ovu krmacu.");
                        }
                        else
                            zalucenjeService.SP_Delete(list);
                    }
                    else
                        e.Cancel = true;
                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                MessageBox.Show("Neuspesno brisanje. " + ex.Message);
            }
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(() => MoveCell()));
        }
        private void MoveCell()
        {
            if (dataGridView1.CurrentCell != null && dataGridView1.CurrentRow != null)
            {
                var row = dataGridView1.CurrentRow;
                var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                if (id == 0)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[2];
                }
            }
        }
    }
}
