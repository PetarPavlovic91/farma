﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Zalucenje._01EvidencijaZalucenja
{
    public partial class Evidencija_Zalucenja_Pregled : Form
    {
        public Evidencija_Zalucenja_Pregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                if (item.Width > Helper.MaxColumnWidth)
                {
                    item.Width = Helper.MaxColumnWidth;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ZalucenjeService zalucenjeService = new ZalucenjeService();
                dataGridView1.DataSource = zalucenjeService.EvidencijaPrasenjaPregled(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
