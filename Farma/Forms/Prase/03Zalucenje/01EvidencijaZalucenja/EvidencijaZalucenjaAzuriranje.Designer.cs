﻿
namespace Farma.Forms.Prase.Zalucenje._01EvidencijaZalucenja
{
    partial class EvidencijaZalucenjaAzuriranje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KRMACA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OBJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RAZ_PZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZALUC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEZ_ZAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZDR_ST = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OL_Z = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RBZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DOJARA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REFK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateModified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 62);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(336, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Pretrazi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(87, 23);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1171, 582);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.RB,
            this.KRMACA,
            this.PARIT,
            this.OBJ,
            this.BOX,
            this.PZ,
            this.RAZ_PZ,
            this.ZALUC,
            this.TEZ_ZAL,
            this.ZDR_ST,
            this.OL_Z,
            this.RBZ,
            this.DOJARA,
            this.REFK,
            this.DateCreated,
            this.DateModified});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1171, 582);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // RB
            // 
            this.RB.DataPropertyName = "RB";
            this.RB.HeaderText = "RB";
            this.RB.Name = "RB";
            this.RB.ReadOnly = true;
            this.RB.Width = 30;
            // 
            // KRMACA
            // 
            this.KRMACA.DataPropertyName = "KRMACA";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KRMACA.DefaultCellStyle = dataGridViewCellStyle1;
            this.KRMACA.HeaderText = "KRMACA";
            this.KRMACA.Name = "KRMACA";
            this.KRMACA.Width = 60;
            // 
            // PARIT
            // 
            this.PARIT.DataPropertyName = "PARIT";
            this.PARIT.HeaderText = "PARIT";
            this.PARIT.Name = "PARIT";
            this.PARIT.ReadOnly = true;
            this.PARIT.Width = 50;
            // 
            // OBJ
            // 
            this.OBJ.DataPropertyName = "OBJ";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.OBJ.DefaultCellStyle = dataGridViewCellStyle2;
            this.OBJ.HeaderText = "OBJ";
            this.OBJ.Name = "OBJ";
            this.OBJ.Width = 30;
            // 
            // BOX
            // 
            this.BOX.DataPropertyName = "BOX";
            this.BOX.HeaderText = "BOX";
            this.BOX.Name = "BOX";
            this.BOX.Width = 30;
            // 
            // PZ
            // 
            this.PZ.DataPropertyName = "PZ";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PZ.DefaultCellStyle = dataGridViewCellStyle3;
            this.PZ.HeaderText = "PZ";
            this.PZ.Name = "PZ";
            this.PZ.Width = 30;
            // 
            // RAZ_PZ
            // 
            this.RAZ_PZ.DataPropertyName = "RAZ_PZ";
            this.RAZ_PZ.HeaderText = "RAZ_PZ";
            this.RAZ_PZ.Name = "RAZ_PZ";
            this.RAZ_PZ.Width = 50;
            // 
            // ZALUC
            // 
            this.ZALUC.DataPropertyName = "ZALUC";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ZALUC.DefaultCellStyle = dataGridViewCellStyle4;
            this.ZALUC.HeaderText = "ZALUC";
            this.ZALUC.Name = "ZALUC";
            this.ZALUC.Width = 50;
            // 
            // TEZ_ZAL
            // 
            this.TEZ_ZAL.DataPropertyName = "TEZ_ZAL";
            this.TEZ_ZAL.HeaderText = "TEZ_ZAL";
            this.TEZ_ZAL.Name = "TEZ_ZAL";
            this.TEZ_ZAL.Width = 60;
            // 
            // ZDR_ST
            // 
            this.ZDR_ST.DataPropertyName = "ZDR_ST";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ZDR_ST.DefaultCellStyle = dataGridViewCellStyle5;
            this.ZDR_ST.HeaderText = "ZDR_ST";
            this.ZDR_ST.Name = "ZDR_ST";
            this.ZDR_ST.Width = 50;
            // 
            // OL_Z
            // 
            this.OL_Z.DataPropertyName = "OL_Z";
            this.OL_Z.HeaderText = "OL_Z";
            this.OL_Z.Name = "OL_Z";
            this.OL_Z.Width = 50;
            // 
            // RBZ
            // 
            this.RBZ.DataPropertyName = "RBZ";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.RBZ.DefaultCellStyle = dataGridViewCellStyle6;
            this.RBZ.HeaderText = "RBZ";
            this.RBZ.Name = "RBZ";
            this.RBZ.Width = 40;
            // 
            // DOJARA
            // 
            this.DOJARA.DataPropertyName = "DOJARA";
            this.DOJARA.HeaderText = "DOJARA";
            this.DOJARA.Name = "DOJARA";
            this.DOJARA.Width = 50;
            // 
            // REFK
            // 
            this.REFK.DataPropertyName = "REFK";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.REFK.DefaultCellStyle = dataGridViewCellStyle7;
            this.REFK.HeaderText = "REFK";
            this.REFK.Name = "REFK";
            this.REFK.Width = 40;
            // 
            // DateCreated
            // 
            this.DateCreated.DataPropertyName = "DateCreated";
            this.DateCreated.HeaderText = "DateCreated";
            this.DateCreated.Name = "DateCreated";
            // 
            // DateModified
            // 
            this.DateModified.DataPropertyName = "DateModified";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DateModified.DefaultCellStyle = dataGridViewCellStyle8;
            this.DateModified.HeaderText = "DateModified";
            this.DateModified.Name = "DateModified";
            // 
            // EvidencijaZalucenjaAzuriranje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1171, 644);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "EvidencijaZalucenjaAzuriranje";
            this.Text = "Evidencija Zalucenja Azuriranje";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn RB;
        private System.Windows.Forms.DataGridViewTextBoxColumn KRMACA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn OBJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn BOX;
        private System.Windows.Forms.DataGridViewTextBoxColumn PZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn RAZ_PZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZALUC;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEZ_ZAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZDR_ST;
        private System.Windows.Forms.DataGridViewTextBoxColumn OL_Z;
        private System.Windows.Forms.DataGridViewTextBoxColumn RBZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn DOJARA;
        private System.Windows.Forms.DataGridViewTextBoxColumn REFK;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCreated;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateModified;
    }
}