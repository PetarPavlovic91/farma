﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Zalucenje._01EvidencijaZalucenja
{
    public partial class EvidencijaZalucenja : Form
    {
        public EvidencijaZalucenja()
        {
            InitializeComponent();
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Evidencija_Zalucenja_Pregled());
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaZalucenjaAzuriranje());
        }
    }
}
