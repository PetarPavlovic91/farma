﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._05Nazimice
{
    public partial class KatalogNazimicaPregled : Form
    {
        public KatalogNazimicaPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var iskljucena = false;
                if (radioButton2.Checked)
                {
                    iskljucena = true;
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.KartotekaNazimicaPregled(iskljucena);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
