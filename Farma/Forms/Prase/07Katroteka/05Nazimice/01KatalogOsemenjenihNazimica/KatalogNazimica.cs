﻿using Farm.Utils;
using Farma.Forms.Prase._07Katroteka._05Nazimice._01KatalogOsemenjenihNazimica;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._05Nazimice
{
    public partial class KatalogNazimica : Form
    {
        public KatalogNazimica()
        {
            InitializeComponent();
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KatalogNazimicaAzuriranje());
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KatalogNazimicaPregled());
        }
    }
}
