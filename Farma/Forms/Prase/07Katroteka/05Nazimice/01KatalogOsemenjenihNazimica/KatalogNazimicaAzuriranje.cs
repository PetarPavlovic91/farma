﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Krmaca;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._05Nazimice._01KatalogOsemenjenihNazimica
{
    public partial class KatalogNazimicaAzuriranje : Form
    {
        public KatalogNazimicaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox10.Text = "";
            numericUpDown1.Value = 0;
            textBox12.Text = "";
            textBox14.Text = "";
            textBox15.Text = "";
            textBox16.Text = "";
            textBox17.Text = "";
            textBox18.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(textBox6.Text);
                KartotekaService kartotekaService = new KartotekaService();
                var result = kartotekaService.DeleteNazimice(id);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                }

                var iskljucena = false;
                if (radioButton2.Checked)
                {
                    iskljucena = true;
                }
                dataGridView1.DataSource = kartotekaService.KartotekaNazimicaPregled(iskljucena);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    KrmacaService krmacaService = new KrmacaService();
                    var krmaca = krmacaService.GetById(int.Parse(textBox6.Text));
                    if (krmaca != null)
                    {
                        textBox6.Text = krmaca.Id.ToString();
                        textBox2.Text = krmaca.T_KRM;
                        textBox12.Text = krmaca.KLASA;
                        numericUpDown1.Value = krmaca.EKST ?? 0;
                        textBox10.Text = krmaca.BR_SISA;
                        textBox7.Text = krmaca.SI1?.ToString();
                        textBox18.Text = krmaca.NAPOMENA;
                        textBox17.Text = krmaca.MARKICA;
                        textBox16.Text = krmaca.HBBROJ;
                        textBox15.Text = krmaca.RBBROJ;
                        textBox14.Text = krmaca.OPIS;
                        textBox4.Text = krmaca.RASA;
                        textBox3.Text = krmaca.MBR_OCA;
                        textBox1.Text = krmaca.MBR_MAJKE;
                        textBox9.Text = krmaca.VLA;
                        textBox8.Text = krmaca.ODG;
                        if (krmaca.DAT_ROD.HasValue)
                        {
                            dateTimePicker2.Value = (DateTime)krmaca.DAT_ROD;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var iskljucena = false;
                if (radioButton2.Checked)
                {
                    iskljucena = true;
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.KartotekaNazimicaPregled(iskljucena);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            try
            {
                KrmacaAzuriranjeVM krmaca = new KrmacaAzuriranjeVM();
                krmaca.Id = string.IsNullOrEmpty(textBox6.Text) ? 0 : int.Parse(textBox6.Text);
                krmaca.T_KRM = textBox2.Text;
                krmaca.KLASA = textBox12.Text;
                krmaca.EKST = (int)numericUpDown1.Value;
                krmaca.BR_SISA = textBox10.Text;
                krmaca.SI1 = textBox7.Text != null ? int.Parse(textBox7.Text) : 0; ;
                krmaca.NAPOMENA = textBox18.Text;
                krmaca.MARKICA = textBox17.Text;
                krmaca.HBBROJ = textBox16.Text;
                krmaca.RBBROJ = textBox15.Text;
                krmaca.OPIS = textBox14.Text;
                krmaca.VLA = textBox9.Text;
                krmaca.ODG = textBox8.Text;
                krmaca.RASA = textBox4.Text;
                krmaca.MBR_OCA = textBox3.Text;
                krmaca.MBR_MAJKE = textBox1.Text;
                krmaca.DAT_ROD = dateTimePicker2.Value;

                KrmacaService krmacaService = new KrmacaService();
                var result = krmacaService.Save(krmaca);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                }
                var iskljucena = false;
                if (radioButton2.Checked)
                {
                    iskljucena = true;
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.KartotekaNazimicaPregled(iskljucena);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
