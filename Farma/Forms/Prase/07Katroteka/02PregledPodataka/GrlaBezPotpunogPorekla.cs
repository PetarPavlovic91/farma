﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._02PregledPodataka
{
    public partial class GrlaBezPotpunogPorekla : Form
    {
        public GrlaBezPotpunogPorekla()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KartotekaService kartotekaService = new KartotekaService();
            if (radioButton1.Checked && radioButton3.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(true, "Krmaca", true);
            }
            else if (radioButton1.Checked && radioButton3.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(true, "Krmaca", false);
            }
            else if (radioButton1.Checked && radioButton4.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(true, "Nerast", true);
            }
            else if (radioButton1.Checked && radioButton4.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(true, "Nerast", false);
            }
            else if (radioButton1.Checked && radioButton5.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(true, "Nazimica", true);
            }
            else if (radioButton1.Checked && radioButton5.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(true, "Nazimica", false);
            }
            else if (radioButton2.Checked && radioButton3.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(false, "Krmaca", true);
            }
            else if (radioButton2.Checked && radioButton3.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(false, "Krmaca", false);
            }
            else if (radioButton2.Checked && radioButton4.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(false, "Nerast", true);
            }
            else if (radioButton2.Checked && radioButton4.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(false, "Nerast", false);
            }
            else if (radioButton2.Checked && radioButton5.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(false, "Nazimica", true);
            }
            else if (radioButton2.Checked && radioButton5.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezPotpunogPorekla(false, "Nazimica", false);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var isPoreklo = true;
            var vrsta = "Krmaca";
            var aktivan = true;
            if (radioButton1.Checked && radioButton3.Checked && radioButton6.Checked)
            {
                vrsta = "Krmaca";
                isPoreklo = true;
                aktivan = true;
            }
            else if (radioButton1.Checked && radioButton3.Checked && radioButton7.Checked)
            {
                vrsta = "Krmaca";
                isPoreklo = true;
                aktivan = false;
            }
            else if (radioButton1.Checked && radioButton4.Checked && radioButton6.Checked)
            {
                vrsta = "Nerast";
                isPoreklo = true;
                aktivan = true;
            }
            else if (radioButton1.Checked && radioButton4.Checked && radioButton7.Checked)
            {
                vrsta = "Nerast";
                isPoreklo = true;
                aktivan = false;
            }
            else if (radioButton1.Checked && radioButton5.Checked && radioButton6.Checked)
            {
                vrsta = "Nazimica";
                isPoreklo = true;
                aktivan = true;
            }
            else if (radioButton1.Checked && radioButton5.Checked && radioButton7.Checked)
            {
                vrsta = "Nazimica";
                isPoreklo = true;
                aktivan = false;
            }
            else if (radioButton2.Checked && radioButton3.Checked && radioButton6.Checked)
            {
                vrsta = "Krmaca";
                isPoreklo = false;
                aktivan = true;
            }
            else if (radioButton2.Checked && radioButton3.Checked && radioButton7.Checked)
            {
                vrsta = "Krmaca";
                isPoreklo = false;
                aktivan = false;
            }
            else if (radioButton2.Checked && radioButton4.Checked && radioButton6.Checked)
            {
                vrsta = "Nerast";
                isPoreklo = false;
                aktivan = true;
            }
            else if (radioButton2.Checked && radioButton4.Checked && radioButton7.Checked)
            {
                vrsta = "Nerast";
                isPoreklo = false;
                aktivan = false;
            }
            else if (radioButton2.Checked && radioButton5.Checked && radioButton6.Checked)
            {
                vrsta = "Nazimica";
                isPoreklo = false;
                aktivan = true;
            }
            else if (radioButton2.Checked && radioButton5.Checked && radioButton7.Checked)
            {
                vrsta = "Nazimica";
                isPoreklo = false;
                aktivan = false;
            }
            Helper.OpenNewForm(new GrlaBezPotpunogPoreklaReportForm(isPoreklo, vrsta, aktivan));
        }
    }
}
