﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Krmaca;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._02PregledPodataka
{
    public partial class StampaPoreklaGrla : Form
    {
        public StampaPoreklaGrla(string tetovirBroj = "")
        {
            InitializeComponent();
            GeneratePorekloForm(tetovirBroj);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GeneratePorekloForm("");
        }

        private void GeneratePorekloForm(string tetovirBroj)
        {
            try
            {
                KrmacaService krmacaService = new KrmacaService();
                PorekloExtendedVM porekloExt = null;
                if (!string.IsNullOrEmpty(tetovirBroj))
                {
                    porekloExt = krmacaService.GetPorekloByTetovir(tetovirBroj);
                }
                else
                {
                    porekloExt = krmacaService.GetPorekloByTetovir(textBox1.Text);
                }

                if (porekloExt == null)
                {
                    MessageBox.Show("Poreklo nije pronadjeno");
                }
                else
                {
                    textBox1.Text = porekloExt.T_GRLA;
                    textBox2.Text = porekloExt.MBR_GRLA;
                    textBox3.Text = porekloExt.MB_GRLA;
                    textBox4.Text = "Farma VOGANJ";
                    textBox6.Text = "Novi Sad";
                    textBox7.Text = porekloExt.MB_MAJKE;
                    textBox8.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    textBox32.Text = porekloExt.GN;
                    textBox33.Text = porekloExt.T_GRLA;

                    if (porekloExt.POL == "Z")
                    {
                        radioButton1.Checked = true;
                    }
                    else if (porekloExt.POL == "M")
                    {
                        radioButton2.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
