﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._02PregledPodataka
{
    public partial class GrlaBezOtvorenogPorekla : Form
    {
        public GrlaBezOtvorenogPorekla()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            KartotekaService kartotekaService = new KartotekaService();
            if (radioButton1.Checked && radioButton3.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(true, "Krmaca");
            }
            else if (radioButton1.Checked && radioButton4.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(true, "Nerast");
            }
            else if (radioButton1.Checked && radioButton5.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(true, "Nazimica");
            }
            else if (radioButton1.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(true, "Test");
            }
            else if (radioButton1.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(true, "Odabir");
            }
            else if (radioButton2.Checked && radioButton3.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(false, "Krmaca");
            }
            else if (radioButton2.Checked && radioButton4.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(false, "Nerast");
            }
            else if (radioButton2.Checked && radioButton5.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(false, "Nazimica");
            }
            else if (radioButton2.Checked && radioButton6.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(false, "Test");
            }
            else if (radioButton2.Checked && radioButton7.Checked)
            {
                dataGridView1.DataSource = kartotekaService.GrlaBezOtvorenogPorekla(false, "Odabir");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var isPoreklo = true;
            var vrsta = "Krmaca";
            if (radioButton1.Checked && radioButton3.Checked)
            {
            }
            else if (radioButton1.Checked && radioButton4.Checked)
            {
                vrsta = "Nerast";
            }
            else if (radioButton1.Checked && radioButton5.Checked)
            {
                vrsta = "Nazimica";
            }
            else if (radioButton1.Checked && radioButton6.Checked)
            {
                vrsta = "Test";
            }
            else if (radioButton1.Checked && radioButton7.Checked)
            {
                vrsta = "Odabir";
            }
            else if (radioButton2.Checked && radioButton3.Checked)
            {
                vrsta = "Krmaca";
                isPoreklo = false;
            }
            else if (radioButton2.Checked && radioButton4.Checked)
            {
                vrsta = "Nerast";
                isPoreklo = false;
            }
            else if (radioButton2.Checked && radioButton5.Checked)
            {
                vrsta = "Nazimica";
                isPoreklo = false;
            }
            else if (radioButton2.Checked && radioButton6.Checked)
            {
                vrsta = "Test";
                isPoreklo = false;
            }
            else if (radioButton2.Checked && radioButton7.Checked)
            {
                vrsta = "Odabir";
                isPoreklo = false;
            }
            Helper.OpenNewForm(new GrlaBezOtvorenogPoreklaReportForm(isPoreklo, vrsta));
        }
    }
}
