﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Krmaca;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._02PregledPodataka
{
    public partial class StampaPedigreaGrla : Form
    {
        public StampaPedigreaGrla(string tetovirBroj = "")
        {
            InitializeComponent();
            GeneratePedigreForm(tetovirBroj);
        }
        private void GeneratePedigreForm(string tetovirBroj)
        {
            try
            {
                KrmacaService krmacaService = new KrmacaService();
                PedigreExtendedVM pedigreExt = null;
                if (!string.IsNullOrEmpty(tetovirBroj))
                {
                    pedigreExt = krmacaService.GetPedigreByTetovir(tetovirBroj);
                }
                else
                {
                    pedigreExt = krmacaService.GetPedigreByTetovir(textBox1.Text);
                }

                if (pedigreExt == null)
                {
                    MessageBox.Show("Poreklo nije pronadjeno");
                }
                else
                {
                    textBox1.Text = pedigreExt.T_GRLA;
                    textBox2.Text = pedigreExt.MBR_GRLA;
                    textBox4.Text = "Farma VOGANJ";
                    textBox6.Text = "Novi Sad";
                    textBox7.Text = pedigreExt.MB_MAJKE;
                    textBox8.Text = DateTime.Now.ToString("dd/MM/yyyy");

                    if (pedigreExt.POL == "Z")
                    {
                        radioButton1.Checked = true;
                    }
                    else if (pedigreExt.POL == "M")
                    {
                        radioButton2.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GeneratePedigreForm("");
        }
    }
}
