﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._06OdabirITest
{
    public partial class GrlaUTestuIOdabiruNaDan : Form
    {
        public GrlaUTestuIOdabiruNaDan()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var nazimice = true;
                if (radioButton2.Checked)
                {
                    nazimice = false;
                }
                var testirane = true;
                if (radioButton4.Checked)
                {
                    testirane = false;
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.GrlaUTestuIOdabiruNaDan(dateTimePicker1.Value, Convert.ToInt32(numericUpDown1.Value), nazimice, testirane);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var nazimice = true;
            if (radioButton2.Checked)
            {
                nazimice = false;
            }
            var testirane = true;
            if (radioButton4.Checked)
            {
                testirane = false;
            }
            Helper.OpenNewForm(new GrlaUTestuIOdabiruNaDanReportForm(dateTimePicker1.Value, Convert.ToInt32(numericUpDown1.Value), nazimice, testirane));
        }
    }
}
