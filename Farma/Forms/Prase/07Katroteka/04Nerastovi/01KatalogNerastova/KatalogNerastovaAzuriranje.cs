﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._07Kartoteka;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._04Nerastovi._01KatalogNerastova
{
    public partial class KatalogNerastovaAzuriranje : Form
    {
        public KatalogNerastovaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.KatalogNerastovaPregled(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(textBox2.Text))
                {
                    KartotekaService kartotekaService = new KartotekaService();
                    var res = kartotekaService.PrenesiPodatkeZaNovogNerastaIzTesta(textBox2.Text);
                    if (!res.Success)
                    {
                        MessageBox.Show(res.Message);
                    }
                    else
                    {
                        var nerast = kartotekaService.GetByT_Kod(textBox2.Text);
                        if (nerast != null)
                        {
                            textBox6.Text = nerast.Id.ToString();
                            textBox2.Text = nerast.Tetovir;
                            textBox12.Text = nerast.Klasa;
                            numericUpDown1.Value = nerast.Ekst ?? 0;
                            textBox10.Text = nerast.Sisa;
                            if (nerast.Na_farmi_od.HasValue)
                            {
                                dateTimePicker3.Value = (DateTime)nerast.Na_farmi_od;
                            }
                            numericUpDown2.Value = nerast.Si1 ?? 0;
                            textBox19.Text = nerast.Pv;
                            textBox18.Text = nerast.Napomena;
                            textBox17.Text = nerast.Markica;
                            textBox16.Text = nerast.HB_Broj;
                            textBox15.Text = nerast.RB_Broj;
                            textBox14.Text = nerast.Opis;
                            textBox8.Text = nerast.Broj_dozvole;
                            textBox5.Text = nerast.Odg;
                            textBox4.Text = nerast.Rasa;
                            textBox3.Text = nerast.Mat_broj_oca;
                            textBox1.Text = nerast.Mat_broj_majke;
                            if (nerast.Rodjen.HasValue)
                            {
                                dateTimePicker2.Value = (DateTime)nerast.Rodjen;
                            }
                            textBox13.Text = nerast.Iz_legla;
                        }
                        dataGridView1.DataSource = kartotekaService.KatalogNerastovaPregled(false);
                    }
                }
                else
                {
                    MessageBox.Show("Morate uneti podatke u polje Nerast, kako bismo znali koju zivotinju iz testa treba da prebacimo.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            numericUpDown1.Value = 0;
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            numericUpDown2.Value = 0;
            textBox12.Text = "";
            textBox13.Text = "";
            textBox14.Text = "";
            textBox15.Text = "";
            textBox16.Text = "";
            textBox17.Text = "";
            textBox18.Text = "";
            textBox19.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(textBox6.Text);
                KartotekaService kartotekaService = new KartotekaService();
                var result = kartotekaService.DeleteNerasta(id);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                }

                dataGridView1.DataSource = kartotekaService.KatalogNerastovaPregled(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                    KartotekaService kartotekaService = new KartotekaService();
                    var nerast = kartotekaService.GetById(int.Parse(textBox6.Text));
                    if (nerast != null)
                    {
                        textBox6.Text = nerast.Id.ToString();
                        textBox2.Text = nerast.Tetovir;
                        textBox12.Text = nerast.Klasa;
                        numericUpDown1.Value = nerast.Ekst ?? 0;
                        textBox10.Text = nerast.Sisa;
                        if (nerast.Na_farmi_od.HasValue)
                        {
                            dateTimePicker3.Value = (DateTime)nerast.Na_farmi_od;
                        }
                        numericUpDown2.Value = nerast.Si1 ?? 0;
                        textBox19.Text = nerast.Pv;
                        textBox18.Text = nerast.Napomena;
                        textBox17.Text = nerast.Markica;
                        textBox16.Text = nerast.HB_Broj;
                        textBox15.Text = nerast.RB_Broj;
                        textBox14.Text = nerast.Opis;
                        textBox8.Text = nerast.Broj_dozvole;
                        textBox9.Text = nerast.Vla;
                        textBox5.Text = nerast.Odg;
                        textBox4.Text = nerast.Rasa;
                        textBox3.Text = nerast.Mat_broj_oca;
                        textBox1.Text = nerast.Mat_broj_majke;
                        if (nerast.Rodjen.HasValue)
                        {
                            dateTimePicker2.Value = (DateTime)nerast.Rodjen;
                        }
                        textBox13.Text = nerast.Iz_legla;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaNerastovaPregledVM nerast = new KartotekaNerastovaPregledVM();
                nerast.Id = string.IsNullOrEmpty(textBox6.Text) ? 0 : int.Parse(textBox6.Text);
                nerast.Tetovir = textBox2.Text;
                nerast.Klasa = textBox12.Text;
                nerast.Ekst = (int)numericUpDown1.Value;
                nerast.Sisa = textBox10.Text;
                nerast.Na_farmi_od = dateTimePicker3.Value;
                nerast.Si1 = (int)numericUpDown2.Value;
                nerast.Pv = textBox19.Text;
                nerast.Napomena = textBox18.Text;
                nerast.Markica = textBox17.Text;
                nerast.HB_Broj = textBox16.Text;
                nerast.RB_Broj = textBox15.Text;
                nerast.Opis = textBox14.Text;
                nerast.Broj_dozvole = textBox8.Text;
                nerast.Vla = textBox9.Text;
                nerast.Odg= textBox5.Text;
                nerast.Rasa = textBox4.Text;
                nerast.Mat_broj_oca = textBox3.Text;
                nerast.Mat_broj_majke = textBox1.Text;
                nerast.Rodjen = dateTimePicker2.Value;
                nerast.Iz_legla = textBox13.Text;

                KartotekaService kartotekaService = new KartotekaService();
                var result = kartotekaService.SaveNerasta(nerast);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
