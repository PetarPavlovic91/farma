﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._04Nerastovi._01KatalogNerastova
{
    public partial class KartotekaNerastova : Form
    {
        public KartotekaNerastova()
        {
            InitializeComponent();
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {

            Helper.OpenNewForm(new KatalogNerastovaAzuriranje());
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KatalogNerastovaPregled());
        }
    }
}
