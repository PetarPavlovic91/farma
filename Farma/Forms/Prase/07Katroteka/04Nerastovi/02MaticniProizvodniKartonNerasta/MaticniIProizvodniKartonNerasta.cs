﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.Forms.Prase.Krmaca;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._04Nerastovi._02MaticniProizvodniKartonNerasta
{
    public partial class MaticniIProizvodniKartonNerasta : Form
    {
        public MaticniIProizvodniKartonNerasta()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                var nerast = kartotekaService.GetByT_Kod(textBox1.Text);
                if (nerast == null)
                {
                    throw new Exception("Nerast nije pronadjen");
                }

                textBox3.Text = nerast.Rasa;
                textBox5.Text = nerast.Mat_broj_nerasta;
                textBox39.Text = nerast.Rodjen != null ? ((DateTime)nerast.Rodjen).ToString("dd/MM/yyyy") : "";
                textBox28.Text = nerast.Iskljucen != null ? ((DateTime)nerast.Iskljucen).ToString("dd/MM/yyyy") : "";
                textBox40.Text = nerast.Prvi_skok != null ? ((DateTime)nerast.Prvi_skok).ToString("dd/MM/yyyy") : "";
                textBox4.Text = nerast.Razl;
                textBox9.Text = nerast.Markica;
                textBox10.Text = nerast.Si1?.ToString();
                textBox17.Text = nerast.Si2?.ToString();
                textBox16.Text = nerast.Si3?.ToString();
                textBox19.Text = nerast.Ekst?.ToString();
                textBox15.Text = nerast.Sisa;
                textBox14.Text = nerast.Klasa;
                textBox31.Text = nerast.HB_Broj;
                textBox32.Text = nerast.RB_Broj;
                textBox26.Text = nerast.Tetovir;
                textBox25.Text = nerast.Napomena;
                textBox20.Text = nerast.GnO;
                textBox21.Text = nerast.Tet_oca;
                textBox24.Text = nerast.Mat_broj_majke;
                textBox23.Text = nerast.GnM;
                textBox22.Text = nerast.Tet_majke;
                textBox13.Text = nerast.Mat_broj_oca;
                textBox27.Text = nerast.Broj_dozvole;

                KrmacaService krmacaService = new KrmacaService();
                dataGridView1.DataSource = krmacaService.KrmacineKlaseByMaticniBroj(nerast.Mat_broj_nerasta);


                dataGridView2.DataSource = kartotekaService.KartotekaNerastovaPregled_Godine(nerast.Mat_broj_nerasta);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView2.DataSource = kartotekaService.KartotekaNerastovaPregled_Godine(textBox5.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PorekloForm(textBox1.Text));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PedigreForm(textBox1.Text));
        }
    }
}
