﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._04Nerastovi._03AnalizaPotomstvaPoOcu
{
    public partial class AnalizaPotomstvaPoOcu : Form
    {
        public AnalizaPotomstvaPoOcu()
        {
            InitializeComponent();
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                KartotekaService kartotekaService = new KartotekaService();
                var nerast = kartotekaService.GetByT_Kod(textBox1.Text);
                if (nerast == null)
                {
                    throw new Exception("Nerast nije pronadjen");
                }

                textBox3.Text = nerast.Rasa;
                textBox5.Text = nerast.Mat_broj_nerasta;
                textBox39.Text = nerast.Rodjen != null ? ((DateTime)nerast.Rodjen).ToString("dd/MM/yyyy") : "";
                textBox28.Text = nerast.Iskljucen != null ? ((DateTime)nerast.Iskljucen).ToString("dd/MM/yyyy") : "";
                textBox40.Text = nerast.Prvi_skok != null ? ((DateTime)nerast.Prvi_skok).ToString("dd/MM/yyyy") : "";
                textBox4.Text = nerast.Razl;
                textBox9.Text = nerast.Markica;
                textBox10.Text = nerast.Si1?.ToString();
                textBox17.Text = nerast.Si2?.ToString();
                textBox16.Text = nerast.Si3?.ToString();
                textBox31.Text = nerast.HB_Broj;
                textBox32.Text = nerast.RB_Broj;
                textBox24.Text = nerast.Mat_broj_majke;
                textBox13.Text = nerast.Mat_broj_oca;
                dataGridView2.DataSource = kartotekaService.AnalizaPotomstvaPoOcu(nerast.Mat_broj_nerasta);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
