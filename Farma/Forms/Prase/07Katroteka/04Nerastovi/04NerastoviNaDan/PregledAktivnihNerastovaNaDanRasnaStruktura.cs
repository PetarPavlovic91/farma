﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._04Nerastovi._04NerastoviNaDan
{
    public partial class PregledAktivnihNerastovaNaDanRasnaStruktura : Form
    {
        private readonly DateTime danDo;
        private readonly bool sviNerastovi;

        public PregledAktivnihNerastovaNaDanRasnaStruktura(DateTime danDo, bool sviNerastovi)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black; 
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.PregledAktivnihNerastovaNaDanRasnaStruktura(danDo, sviNerastovi);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            this.danDo = danDo;
            this.sviNerastovi = sviNerastovi;
        }

        private void PregledAktivnihNerastovaNaDanRasnaStruktura_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledAktivnihNerastovaNaDanRasnaStrukturaReportForm(danDo, sviNerastovi));
        }
    }
}
