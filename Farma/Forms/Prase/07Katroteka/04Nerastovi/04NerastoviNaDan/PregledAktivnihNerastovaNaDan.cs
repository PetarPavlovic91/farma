﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._04Nerastovi._04NerastoviNaDan
{
    public partial class PregledAktivnihNerastovaNaDan : Form
    {
        public PregledAktivnihNerastovaNaDan()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var sviNerastovi = true;
                if (radioButton1.Checked)
                {
                    sviNerastovi = false;
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.PregledAktivnihNerastovaNaDan(dateTimePicker2.Value, sviNerastovi);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            KartotekaService kartotekaService = new KartotekaService();
            var sviNerastovi = true;
            if (radioButton1.Checked)
            {
                sviNerastovi = false;
            }
            if (comboBox1.Text == "1. Rasna struktura")
            {
                Helper.OpenNewForm(new PregledAktivnihNerastovaNaDanRasnaStruktura(dateTimePicker2.Value, sviNerastovi));
            }
            else if (comboBox1.Text == "2. Klasna struktura")
            {
                Helper.OpenNewForm(new PregledAktivnihNerastovaNaDanKlasnaStruktura(dateTimePicker2.Value, sviNerastovi));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var sviNerastovi = true;
            if (radioButton1.Checked)
            {
                sviNerastovi = false;
            }
            Helper.OpenNewForm(new PregledAktivnihNerastovaNaDanReportForm(dateTimePicker2.Value, sviNerastovi));
        }
    }
}
