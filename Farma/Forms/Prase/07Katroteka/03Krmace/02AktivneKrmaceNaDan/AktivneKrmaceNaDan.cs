﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._03Krmace._02AktivneKrmaceNaDan
{
    public partial class AktivneKrmaceNaDan : Form
    {
        public AktivneKrmaceNaDan()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.AktivneKrmaceNaDan(dateTimePicker1.Value, textBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            KartotekaService kartotekaService = new KartotekaService();
            if (comboBox1.Text == "1. Paritetna struktura")
            {
                Helper.OpenNewForm(new AktivneKrmaceNaDanParitetnaStruktura(dateTimePicker1.Value, textBox1.Text));
            }
            else if (comboBox1.Text == "2. Rasna struktura")
            {
                Helper.OpenNewForm(new AktivneKrmaceNaDanRasnaStruktura(dateTimePicker1.Value, textBox1.Text));
            }
            else if (comboBox1.Text == "3. Klasna struktura")
            {
                Helper.OpenNewForm(new AktivneKrmaceNaDanKlasnaStruktura(dateTimePicker1.Value, textBox1.Text));

            }
            else if (comboBox1.Text == "4. HB RB N struktura")
            {
                Helper.OpenNewForm(new AktivneKrmaceNaDanHBRBNStruktura(dateTimePicker1.Value, textBox1.Text));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AktivneKrmaceNaDanReportForm(dateTimePicker1.Value, textBox1.Text, false));
        }
    }
}
