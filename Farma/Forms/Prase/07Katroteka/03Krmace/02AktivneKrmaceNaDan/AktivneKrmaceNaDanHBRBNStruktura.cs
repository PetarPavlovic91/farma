﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._03Krmace._02AktivneKrmaceNaDan
{
    public partial class AktivneKrmaceNaDanHBRBNStruktura : Form
    {
        private readonly DateTime dan;
        private readonly string rasa;
        private readonly bool isNazimica;

        public AktivneKrmaceNaDanHBRBNStruktura(DateTime dan, string rasa, bool isNazimica = false)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.AktivneKrmaceNaDanHBRBNStruktura(dan, rasa, isNazimica);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            this.dan = dan;
            this.rasa = rasa;
            this.isNazimica = isNazimica;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AktivneKrmaceNaDanHBRBNStrukturaReportForm(dan, rasa, isNazimica));
        }
    }
}
