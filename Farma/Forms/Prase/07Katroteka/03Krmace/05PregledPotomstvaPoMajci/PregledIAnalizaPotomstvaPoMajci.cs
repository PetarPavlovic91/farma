﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._03Krmace._05PregledPotomstvaPoMajci
{
    public partial class PregledIAnalizaPotomstvaPoMajci : Form
    {
        public PregledIAnalizaPotomstvaPoMajci()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                KrmacaService krmacaService = new KrmacaService();
                KartotekaService kartotekaService = new KartotekaService();
                var krmaca = krmacaService.GetByT_Kod(textBox1.Text);

                if (krmaca == null)
                {
                    throw new Exception("Krmaca nije pronadjena");
                }
                textBox3.Text = krmaca.RASA;
                textBox5.Text = krmaca.MBR_KRM;
                textBox39.Text = krmaca.DAT_ROD != null ? ((DateTime)krmaca.DAT_ROD).ToString("dd/MM/yyyy") : "";
                textBox40.Text = krmaca.D_SKART != null ? ((DateTime)krmaca.D_SKART).ToString("dd/MM/yyyy") : "";
                textBox4.Text = krmaca.RAZL_SK != null ? krmaca.RAZL_SK.ToString() : "";
                textBox9.Text = krmaca.MARKICA;
                textBox8.Text = krmaca.PARIT != null ? krmaca.PARIT.ToString() : "";
                //obj i box ne postoje, odakle ih vucemo?
                textBox10.Text = krmaca.SI1?.ToString();
                textBox17.Text = krmaca.SI2?.ToString();
                textBox16.Text = krmaca.SI3?.ToString();
                //odakle vucemo drugi text box za Klasu?
                textBox31.Text = krmaca.HBBROJ;
                textBox32.Text = krmaca.RBBROJ;
                textBox30.Text = krmaca.NBBROJ;
                textBox28.Text = krmaca.GRUPA;
                textBox26.Text = krmaca.TETOVIRK10;
                textBox25.Text = krmaca.NAPOMENA;
                textBox24.Text = krmaca.MBR_MAJKE;
                textBox13.Text = krmaca.MBR_OCA;
                dataGridView1.DataSource = kartotekaService.AnalizaPotomstvaPoMajci(krmaca.MBR_KRM);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
