﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._03Krmace._03PregledHBBrojevaKrmaca
{
    public partial class PregledHBBrojevaKrmaca : Form
    {
        public PregledHBBrojevaKrmaca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                if (radioButton1.Checked)
                {
                    dataGridView1.DataSource = kartotekaService.PregledHBBrojevaKrmaca();
                }
                else if (radioButton2.Checked)
                {
                    dataGridView1.DataSource = kartotekaService.PregledRBBrojevaKrmaca();
                }
                else if (radioButton3.Checked)
                {
                    dataGridView1.DataSource = kartotekaService.PregledNBBrojevaKrmaca();
                }
                else if (radioButton4.Checked)
                {
                    dataGridView1.DataSource = kartotekaService.PregledMarkicaKrmaca();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //PregledHBBrojevaKrmacaReportForm
            if (radioButton1.Checked)
            {
                Helper.OpenNewForm(new PregledHBBrojevaKrmacaReportForm());
            }
            else if (radioButton2.Checked)
            {
                //PregledRBBrojevaKrmacaReportForm
                Helper.OpenNewForm(new PregledRBBrojevaKrmacaReportForm());
            }
            else if (radioButton3.Checked)
            {
                //PregledNBBrojevaKrmacaReportForm
                Helper.OpenNewForm(new PregledNBBrojevaKrmacaReportForm());
            }
            else if (radioButton4.Checked)
            {
                //PregledMarkicaKrmacaReportForm
                Helper.OpenNewForm(new PregledMarkicaKrmacaReportForm());
            }
        }
    }
}
