﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._07Kartoteka;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._07RegistarPrasadi
{
    public partial class DopunaRegistraTetoviranihPrasadiAzuriranje : Form
    {
        public DopunaRegistraTetoviranihPrasadiAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.DopunaRegistraTetoviranihPrasadi(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "";
            textBox12.Text = "";
            textBox14.Text = "";
            textBox15.Text = "";
            textBox23.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(textBox6.Text);
                PrasenjeService prasenjeService = new PrasenjeService();
                var result = prasenjeService.Delete(id);
                if (!result.Success)
                {
                    MessageBox.Show(result.Message);
                }

                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.DopunaRegistraTetoviranihPrasadi(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    textBox6.Text =  dataGridView1.Rows[e.RowIndex].Cells[0].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString() : "";
                    textBox5.Text =  dataGridView1.Rows[e.RowIndex].Cells[1].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString() : "";
                    textBox4.Text =  dataGridView1.Rows[e.RowIndex].Cells[2].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString() : "";
                    textBox1.Text =  dataGridView1.Rows[e.RowIndex].Cells[3].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString() : "";
                    textBox2.Text =  dataGridView1.Rows[e.RowIndex].Cells[4].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString() : "";
                    textBox3.Text =  dataGridView1.Rows[e.RowIndex].Cells[5].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString() : "";
                    textBox9.Text =  dataGridView1.Rows[e.RowIndex].Cells[6].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString() : "";
                    textBox11.Text = dataGridView1.Rows[e.RowIndex].Cells[7].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() : "";
                    textBox7.Text =  dataGridView1.Rows[e.RowIndex].Cells[8].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString() : "";
                    textBox10.Text = dataGridView1.Rows[e.RowIndex].Cells[9].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[9].Value.ToString() : "";
                    textBox12.Text = dataGridView1.Rows[e.RowIndex].Cells[10].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[10].Value.ToString() : "";
                    textBox14.Text = dataGridView1.Rows[e.RowIndex].Cells[11].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[11].Value.ToString() : "";
                    textBox15.Text = dataGridView1.Rows[e.RowIndex].Cells[12].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[12].Value.ToString() : "";
                    textBox23.Text = dataGridView1.Rows[e.RowIndex].Cells[13].Value != null ? dataGridView1.Rows[e.RowIndex].Cells[13].Value.ToString() : "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                DopunaRegistraTetoviranihPrasadiVM dopunaRegistraTetoviranihPrasadi = new DopunaRegistraTetoviranihPrasadiVM();
                dopunaRegistraTetoviranihPrasadi.Krmaca = textBox5.Text;
                dopunaRegistraTetoviranihPrasadi.MaticniBroj = textBox4.Text;
                dopunaRegistraTetoviranihPrasadi.Parit =  !string.IsNullOrEmpty(textBox1.Text) ? int.Parse(textBox1.Text) : 0;
                dopunaRegistraTetoviranihPrasadi.Zivo =   !string.IsNullOrEmpty(textBox2.Text) ? (int?)int.Parse(textBox2.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Tmp_od = !string.IsNullOrEmpty(textBox3.Text) ? (int?)int.Parse(textBox3.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Tmp_do = !string.IsNullOrEmpty(textBox9.Text) ? (int?)int.Parse(textBox9.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Tzp_od = !string.IsNullOrEmpty(textBox11.Text) ? (int?)int.Parse(textBox11.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Tzp_do = !string.IsNullOrEmpty(textBox7.Text) ? (int?)int.Parse(textBox7.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Tov_od = !string.IsNullOrEmpty(textBox10.Text) ? (int?)int.Parse(textBox10.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Tov_do = !string.IsNullOrEmpty(textBox12.Text) ? (int?)int.Parse(textBox12.Text) : null;
                dopunaRegistraTetoviranihPrasadi.Broj_registra = textBox12.Text;
                dopunaRegistraTetoviranihPrasadi.Ocena = textBox15.Text;
                dopunaRegistraTetoviranihPrasadi.Uginulo = !string.IsNullOrEmpty(textBox23.Text) ? (int?)int.Parse(textBox23.Text) : null;
                if (!string.IsNullOrEmpty(textBox6.Text))
                {
                    dopunaRegistraTetoviranihPrasadi.Id = int.Parse(textBox6.Text);
                }
                PrasenjeService prasenjeService = new PrasenjeService();
                var res = prasenjeService.SaveRegistraPrasadi(dopunaRegistraTetoviranihPrasadi);
                if (!res.Success)
                {
                    MessageBox.Show(res.Message);
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.DopunaRegistraTetoviranihPrasadi(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
