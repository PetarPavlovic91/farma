﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._07RegistarPrasadi
{
    public partial class TetoviranaPrasadNaStanjuNaDan : Form
    {
        public TetoviranaPrasadNaStanjuNaDan()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var pol = "";
                if (radioButton1.Checked)
                {
                    pol = "M";
                }
                if (radioButton2.Checked)
                {
                    pol = "Z";
                }
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.TetoviranaPrasadNaStanjuNaDan(dateTimePicker1.Value, Convert.ToInt32(numericUpDown1.Value), pol);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var pol = "";
            if (radioButton1.Checked)
            {
                pol = "M";
            }
            if (radioButton2.Checked)
            {
                pol = "Z";
            }
            Helper.OpenNewForm(new TetoviranaPrasadNaStanjuNaDanReportForm(dateTimePicker1.Value, Convert.ToInt32(numericUpDown1.Value), pol));
        }
    }
}
