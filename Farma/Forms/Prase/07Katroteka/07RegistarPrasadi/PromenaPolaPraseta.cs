﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._07RegistarPrasadi
{
    public partial class PromenaPolaPraseta : Form
    {
        public PromenaPolaPraseta()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void PromenaPolaPraseta_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.PromenaPolaPrasadiGet(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["Id"].Value  == DBNull.Value)
                    {
                        throw new Exception("Mozete samo menjati postojece");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@Pol", row.Cells["Pol"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Pol"].Value)));
                    KartotekaService kartotekaService = new KartotekaService();
                    kartotekaService.PromenaPolaPrasadiSave(list);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
