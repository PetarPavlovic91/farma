﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._07RegistarPrasadi
{
    public partial class DopunaBrojaSisaKodPrasadi : Form
    {
        public DopunaBrojaSisaKodPrasadi()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                KartotekaService kartotekaService = new KartotekaService();
                dataGridView1.DataSource = kartotekaService.DopunaBrojaSisaKodPrasadiGet(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["Id"].Value == DBNull.Value)
                    {
                        throw new Exception("Mozete samo menjati postojece");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@S_l", row.Cells["S_l"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["S_l"].Value)));
                    list.Add(new SqlParameter("@S_d", row.Cells["S_d"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["S_d"].Value)));
                    KartotekaService kartotekaService = new KartotekaService();
                    kartotekaService.DopunaBrojaSisaKodPrasadiSave(list);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
