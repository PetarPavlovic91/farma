﻿
namespace Farma.Forms.Prase._07Katroteka._07RegistarPrasadi
{
    partial class DopunaBrojaSisaKodPrasadi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_pras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dat_rodj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mbr_Oca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mbr_majke = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IzLegla = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Priplod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mbr_pras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.S_l = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.S_d = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dateTimePicker2);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 94);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.button1.Location = new System.Drawing.Point(757, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 25);
            this.button1.TabIndex = 10;
            this.button1.Text = "Pretrazi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Cornsilk;
            this.label3.Location = new System.Drawing.Point(440, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "do";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Cornsilk;
            this.label2.Location = new System.Drawing.Point(45, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Period rodjenja";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(502, 32);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 7;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(184, 32);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 94);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1171, 550);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.T_pras,
            this.Pol,
            this.Dat_rodj,
            this.Mbr_Oca,
            this.Mbr_majke,
            this.IzLegla,
            this.Priplod,
            this.Mbr_pras,
            this.S_l,
            this.S_d});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1171, 550);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // T_pras
            // 
            this.T_pras.DataPropertyName = "T_pras";
            this.T_pras.HeaderText = "T_pras";
            this.T_pras.Name = "T_pras";
            this.T_pras.ReadOnly = true;
            this.T_pras.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Pol
            // 
            this.Pol.DataPropertyName = "Pol";
            this.Pol.HeaderText = "Pol";
            this.Pol.Name = "Pol";
            this.Pol.ReadOnly = true;
            // 
            // Dat_rodj
            // 
            this.Dat_rodj.DataPropertyName = "Dat_rodj";
            this.Dat_rodj.HeaderText = "Dat_rodj";
            this.Dat_rodj.Name = "Dat_rodj";
            this.Dat_rodj.ReadOnly = true;
            // 
            // Mbr_Oca
            // 
            this.Mbr_Oca.DataPropertyName = "Mbr_Oca";
            this.Mbr_Oca.HeaderText = "Mbr_Oca";
            this.Mbr_Oca.Name = "Mbr_Oca";
            this.Mbr_Oca.ReadOnly = true;
            // 
            // Mbr_majke
            // 
            this.Mbr_majke.DataPropertyName = "Mbr_majke";
            this.Mbr_majke.HeaderText = "Mbr_majke";
            this.Mbr_majke.Name = "Mbr_majke";
            this.Mbr_majke.ReadOnly = true;
            // 
            // IzLegla
            // 
            this.IzLegla.DataPropertyName = "IzLegla";
            this.IzLegla.HeaderText = "IzLegla";
            this.IzLegla.Name = "IzLegla";
            this.IzLegla.ReadOnly = true;
            // 
            // Priplod
            // 
            this.Priplod.DataPropertyName = "Priplod";
            this.Priplod.HeaderText = "Priplod";
            this.Priplod.Name = "Priplod";
            this.Priplod.ReadOnly = true;
            // 
            // Mbr_pras
            // 
            this.Mbr_pras.DataPropertyName = "Mbr_pras";
            this.Mbr_pras.HeaderText = "Mbr_pras";
            this.Mbr_pras.Name = "Mbr_pras";
            this.Mbr_pras.ReadOnly = true;
            // 
            // S_l
            // 
            this.S_l.DataPropertyName = "S_l";
            this.S_l.HeaderText = "S_l";
            this.S_l.Name = "S_l";
            // 
            // S_d
            // 
            this.S_d.DataPropertyName = "S_d";
            this.S_d.HeaderText = "S_d";
            this.S_d.Name = "S_d";
            // 
            // DopunaBrojaSisaKodPrasadi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1171, 644);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "DopunaBrojaSisaKodPrasadi";
            this.Text = "Dopuna Broja Sisa Kod Prasadi";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_pras;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Dat_rodj;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mbr_Oca;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mbr_majke;
        private System.Windows.Forms.DataGridViewTextBoxColumn IzLegla;
        private System.Windows.Forms.DataGridViewTextBoxColumn Priplod;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mbr_pras;
        private System.Windows.Forms.DataGridViewTextBoxColumn S_l;
        private System.Windows.Forms.DataGridViewTextBoxColumn S_d;
    }
}