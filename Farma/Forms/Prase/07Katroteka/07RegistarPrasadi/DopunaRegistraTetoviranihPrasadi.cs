﻿using Farm.Utils;
using Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._07RegistarPrasadi
{
    public partial class DopunaRegistraTetoviranihPrasadi : Form
    {
        public DopunaRegistraTetoviranihPrasadi()
        {
            InitializeComponent();
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPrasenjaAzuriranje());
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new DopunaRegistraTetoviranihPrasadiPregled());
        }
    }
}
