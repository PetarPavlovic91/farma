﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._09Sifrarnik
{
    public partial class Sifrarnik : Form
    {
        public Sifrarnik(string title, string grupa)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            label2.Text = title;
            KartotekaService kartotekaService = new KartotekaService();
            dataGridView1.DataSource = kartotekaService.Sifrarnik(grupa);
        }

    }
}
