﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._07Katroteka._08MatBrojIFaza
{
    public partial class MaticniBrojIFazaGrla : Form
    {
        public MaticniBrojIFazaGrla()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            KartotekaService kartotekaService = new KartotekaService();
            if (radioButton1.Checked)
            {
                var result = kartotekaService.PregledRBBrojevaKrmaca(textBox1.Text, null);
                textBox2.Text = result.Mat_br;
                textBox6.Text = result.GrloJe;
                textBox3.Text = result.Ciklus_Parit_Faza;
                textBox4.Text = result.DatumRodjenja;
                textBox5.Text = result.Mbr_oca;
                textBox10.Text = result.Mbr_majke;
            }
            else
            {
                var result = kartotekaService.PregledRBBrojevaKrmaca(null, textBox7.Text);
                textBox2.Text = result.Mat_br;
                textBox6.Text = result.GrloJe;
                textBox3.Text = result.Ciklus_Parit_Faza;
                textBox4.Text = result.DatumRodjenja;
                textBox5.Text = result.Mbr_oca;
                textBox10.Text = result.Mbr_majke;
            }
        }
    }
}
