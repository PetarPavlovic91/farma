﻿
namespace Farma.Forms.Prase
{
    partial class PraseMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested001ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested002ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested003ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested001ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.nested004ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested011ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested012ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested111ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nested112ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.testasadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdsadsdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adsasdasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sadasdasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdasdToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdasdToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdasdToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdasdToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdaToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.sadasdasdToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sadasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sadasdasadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adasdaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asdasdToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.sdadasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sadasdaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dasdasdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testToolStripMenuItem,
            this.adsasdasdToolStripMenuItem,
            this.adasdaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(999, 29);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nested001ToolStripMenuItem,
            this.nested002ToolStripMenuItem,
            this.nested003ToolStripMenuItem,
            this.nested001ToolStripMenuItem1,
            this.nested004ToolStripMenuItem});
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(48, 25);
            this.testToolStripMenuItem.Text = "Test";
            // 
            // nested001ToolStripMenuItem
            // 
            this.nested001ToolStripMenuItem.Name = "nested001ToolStripMenuItem";
            this.nested001ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested001ToolStripMenuItem.Text = "Nested001";
            this.nested001ToolStripMenuItem.Click += new System.EventHandler(this.nested001ToolStripMenuItem_Click);
            // 
            // nested002ToolStripMenuItem
            // 
            this.nested002ToolStripMenuItem.Name = "nested002ToolStripMenuItem";
            this.nested002ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested002ToolStripMenuItem.Text = "Nested002";
            // 
            // nested003ToolStripMenuItem
            // 
            this.nested003ToolStripMenuItem.Name = "nested003ToolStripMenuItem";
            this.nested003ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested003ToolStripMenuItem.Text = "Nested003";
            // 
            // nested001ToolStripMenuItem1
            // 
            this.nested001ToolStripMenuItem1.Name = "nested001ToolStripMenuItem1";
            this.nested001ToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.nested001ToolStripMenuItem1.Text = "Nested001";
            this.nested001ToolStripMenuItem1.Click += new System.EventHandler(this.nested001ToolStripMenuItem1_Click);
            // 
            // nested004ToolStripMenuItem
            // 
            this.nested004ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nested011ToolStripMenuItem,
            this.nested012ToolStripMenuItem,
            this.asdasdaToolStripMenuItem,
            this.asdasdadToolStripMenuItem,
            this.asdasdasToolStripMenuItem});
            this.nested004ToolStripMenuItem.Name = "nested004ToolStripMenuItem";
            this.nested004ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested004ToolStripMenuItem.Text = "Nested004";
            // 
            // nested011ToolStripMenuItem
            // 
            this.nested011ToolStripMenuItem.Name = "nested011ToolStripMenuItem";
            this.nested011ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested011ToolStripMenuItem.Text = "Nested011";
            // 
            // nested012ToolStripMenuItem
            // 
            this.nested012ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nested111ToolStripMenuItem,
            this.nested112ToolStripMenuItem,
            this.asdsadsdToolStripMenuItem,
            this.asdasdToolStripMenuItem2});
            this.nested012ToolStripMenuItem.Name = "nested012ToolStripMenuItem";
            this.nested012ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested012ToolStripMenuItem.Text = "Nested012";
            // 
            // nested111ToolStripMenuItem
            // 
            this.nested111ToolStripMenuItem.Name = "nested111ToolStripMenuItem";
            this.nested111ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested111ToolStripMenuItem.Text = "Nested111";
            // 
            // nested112ToolStripMenuItem
            // 
            this.nested112ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.testasadToolStripMenuItem});
            this.nested112ToolStripMenuItem.Name = "nested112ToolStripMenuItem";
            this.nested112ToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.nested112ToolStripMenuItem.Text = "Nested112";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(180, 26);
            this.toolStripMenuItem2.Text = "444";
            // 
            // testasadToolStripMenuItem
            // 
            this.testasadToolStripMenuItem.Name = "testasadToolStripMenuItem";
            this.testasadToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.testasadToolStripMenuItem.Text = "Testasad";
            // 
            // asdsadsdToolStripMenuItem
            // 
            this.asdsadsdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdToolStripMenuItem,
            this.asdasdToolStripMenuItem1});
            this.asdsadsdToolStripMenuItem.Name = "asdsadsdToolStripMenuItem";
            this.asdsadsdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.asdsadsdToolStripMenuItem.Text = "asdsadsd";
            // 
            // asdasdToolStripMenuItem
            // 
            this.asdasdToolStripMenuItem.Name = "asdasdToolStripMenuItem";
            this.asdasdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.asdasdToolStripMenuItem.Text = "asdasd";
            // 
            // asdasdToolStripMenuItem1
            // 
            this.asdasdToolStripMenuItem1.Name = "asdasdToolStripMenuItem1";
            this.asdasdToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.asdasdToolStripMenuItem1.Text = "asdasd";
            // 
            // asdasdToolStripMenuItem2
            // 
            this.asdasdToolStripMenuItem2.Name = "asdasdToolStripMenuItem2";
            this.asdasdToolStripMenuItem2.Size = new System.Drawing.Size(180, 26);
            this.asdasdToolStripMenuItem2.Text = "asdasd";
            // 
            // asdasdaToolStripMenuItem
            // 
            this.asdasdaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdaToolStripMenuItem1,
            this.asdasdasdToolStripMenuItem});
            this.asdasdaToolStripMenuItem.Name = "asdasdaToolStripMenuItem";
            this.asdasdaToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem.Text = "asdasda";
            // 
            // asdasdaToolStripMenuItem1
            // 
            this.asdasdaToolStripMenuItem1.Name = "asdasdaToolStripMenuItem1";
            this.asdasdaToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem1.Text = "asdasda";
            // 
            // asdasdasdToolStripMenuItem
            // 
            this.asdasdasdToolStripMenuItem.Name = "asdasdasdToolStripMenuItem";
            this.asdasdasdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.asdasdasdToolStripMenuItem.Text = "asdasdasd";
            // 
            // asdasdadToolStripMenuItem
            // 
            this.asdasdadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdaToolStripMenuItem2,
            this.asdasdaToolStripMenuItem5});
            this.asdasdadToolStripMenuItem.Name = "asdasdadToolStripMenuItem";
            this.asdasdadToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.asdasdadToolStripMenuItem.Text = "asdasdad";
            // 
            // asdasdaToolStripMenuItem2
            // 
            this.asdasdaToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdaToolStripMenuItem3,
            this.asdasdaToolStripMenuItem4});
            this.asdasdaToolStripMenuItem2.Name = "asdasdaToolStripMenuItem2";
            this.asdasdaToolStripMenuItem2.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem2.Text = "asdasda";
            // 
            // asdasdaToolStripMenuItem3
            // 
            this.asdasdaToolStripMenuItem3.Name = "asdasdaToolStripMenuItem3";
            this.asdasdaToolStripMenuItem3.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem3.Text = "asdasda";
            // 
            // asdasdaToolStripMenuItem4
            // 
            this.asdasdaToolStripMenuItem4.Name = "asdasdaToolStripMenuItem4";
            this.asdasdaToolStripMenuItem4.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem4.Text = "asdasda";
            // 
            // asdasdaToolStripMenuItem5
            // 
            this.asdasdaToolStripMenuItem5.Name = "asdasdaToolStripMenuItem5";
            this.asdasdaToolStripMenuItem5.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem5.Text = "asdasda";
            // 
            // asdasdasToolStripMenuItem
            // 
            this.asdasdasToolStripMenuItem.Name = "asdasdasToolStripMenuItem";
            this.asdasdasToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.asdasdasToolStripMenuItem.Text = "asdasdas";
            // 
            // adsasdasdToolStripMenuItem
            // 
            this.adsasdasdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sadasdasdToolStripMenuItem,
            this.asdasdaToolStripMenuItem6});
            this.adsasdasdToolStripMenuItem.Name = "adsasdasdToolStripMenuItem";
            this.adsasdasdToolStripMenuItem.Size = new System.Drawing.Size(94, 25);
            this.adsasdasdToolStripMenuItem.Text = "adsasdasd";
            // 
            // sadasdasdToolStripMenuItem
            // 
            this.sadasdasdToolStripMenuItem.Name = "sadasdasdToolStripMenuItem";
            this.sadasdasdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.sadasdasdToolStripMenuItem.Text = "sadasdasd";
            // 
            // asdasdaToolStripMenuItem6
            // 
            this.asdasdaToolStripMenuItem6.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdaToolStripMenuItem7,
            this.asdasdToolStripMenuItem3,
            this.asdasdasdToolStripMenuItem3,
            this.sadasdasdToolStripMenuItem1,
            this.sadasdasadToolStripMenuItem});
            this.asdasdaToolStripMenuItem6.Name = "asdasdaToolStripMenuItem6";
            this.asdasdaToolStripMenuItem6.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem6.Text = "asdasda";
            // 
            // asdasdaToolStripMenuItem7
            // 
            this.asdasdaToolStripMenuItem7.Name = "asdasdaToolStripMenuItem7";
            this.asdasdaToolStripMenuItem7.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem7.Text = "asdasda";
            // 
            // asdasdToolStripMenuItem3
            // 
            this.asdasdToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdasdToolStripMenuItem1,
            this.asdasdasdToolStripMenuItem2});
            this.asdasdToolStripMenuItem3.Name = "asdasdToolStripMenuItem3";
            this.asdasdToolStripMenuItem3.Size = new System.Drawing.Size(180, 26);
            this.asdasdToolStripMenuItem3.Text = "asdasd";
            // 
            // asdasdasdToolStripMenuItem1
            // 
            this.asdasdasdToolStripMenuItem1.Name = "asdasdasdToolStripMenuItem1";
            this.asdasdasdToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.asdasdasdToolStripMenuItem1.Text = "asdasdasd";
            // 
            // asdasdasdToolStripMenuItem2
            // 
            this.asdasdasdToolStripMenuItem2.Name = "asdasdasdToolStripMenuItem2";
            this.asdasdasdToolStripMenuItem2.Size = new System.Drawing.Size(180, 26);
            this.asdasdasdToolStripMenuItem2.Text = "asdasdasd";
            // 
            // asdasdasdToolStripMenuItem3
            // 
            this.asdasdasdToolStripMenuItem3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdasdToolStripMenuItem4,
            this.asdasdaToolStripMenuItem8});
            this.asdasdasdToolStripMenuItem3.Name = "asdasdasdToolStripMenuItem3";
            this.asdasdasdToolStripMenuItem3.Size = new System.Drawing.Size(180, 26);
            this.asdasdasdToolStripMenuItem3.Text = "asdasdasd";
            // 
            // asdasdasdToolStripMenuItem4
            // 
            this.asdasdasdToolStripMenuItem4.Name = "asdasdasdToolStripMenuItem4";
            this.asdasdasdToolStripMenuItem4.Size = new System.Drawing.Size(180, 26);
            this.asdasdasdToolStripMenuItem4.Text = "asdasdasd";
            // 
            // asdasdaToolStripMenuItem8
            // 
            this.asdasdaToolStripMenuItem8.Name = "asdasdaToolStripMenuItem8";
            this.asdasdaToolStripMenuItem8.Size = new System.Drawing.Size(180, 26);
            this.asdasdaToolStripMenuItem8.Text = "asdasda";
            // 
            // sadasdasdToolStripMenuItem1
            // 
            this.sadasdasdToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sadasdToolStripMenuItem});
            this.sadasdasdToolStripMenuItem1.Name = "sadasdasdToolStripMenuItem1";
            this.sadasdasdToolStripMenuItem1.Size = new System.Drawing.Size(180, 26);
            this.sadasdasdToolStripMenuItem1.Text = "sadasdasd";
            // 
            // sadasdToolStripMenuItem
            // 
            this.sadasdToolStripMenuItem.Name = "sadasdToolStripMenuItem";
            this.sadasdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.sadasdToolStripMenuItem.Text = "sadasd";
            // 
            // sadasdasadToolStripMenuItem
            // 
            this.sadasdasadToolStripMenuItem.Name = "sadasdasadToolStripMenuItem";
            this.sadasdasadToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.sadasdasadToolStripMenuItem.Text = "sadasdasad";
            this.sadasdasadToolStripMenuItem.Click += new System.EventHandler(this.sadasdasadToolStripMenuItem_Click);
            // 
            // adasdaToolStripMenuItem
            // 
            this.adasdaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.asdasdToolStripMenuItem4});
            this.adasdaToolStripMenuItem.Name = "adasdaToolStripMenuItem";
            this.adasdaToolStripMenuItem.Size = new System.Drawing.Size(71, 25);
            this.adasdaToolStripMenuItem.Text = "adasda";
            // 
            // asdasdToolStripMenuItem4
            // 
            this.asdasdToolStripMenuItem4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sdadasdToolStripMenuItem,
            this.sadasdaToolStripMenuItem});
            this.asdasdToolStripMenuItem4.Name = "asdasdToolStripMenuItem4";
            this.asdasdToolStripMenuItem4.Size = new System.Drawing.Size(180, 26);
            this.asdasdToolStripMenuItem4.Text = "asdasd";
            // 
            // sdadasdToolStripMenuItem
            // 
            this.sdadasdToolStripMenuItem.Name = "sdadasdToolStripMenuItem";
            this.sdadasdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.sdadasdToolStripMenuItem.Text = "sdadasd";
            // 
            // sadasdaToolStripMenuItem
            // 
            this.sadasdaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dasdasdToolStripMenuItem});
            this.sadasdaToolStripMenuItem.Name = "sadasdaToolStripMenuItem";
            this.sadasdaToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.sadasdaToolStripMenuItem.Text = "sadasda";
            // 
            // dasdasdToolStripMenuItem
            // 
            this.dasdasdToolStripMenuItem.Name = "dasdasdToolStripMenuItem";
            this.dasdasdToolStripMenuItem.Size = new System.Drawing.Size(180, 26);
            this.dasdasdToolStripMenuItem.Text = "dasdasd";
            // 
            // PraseMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(999, 586);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PraseMenu";
            this.Text = "PraseMenu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested001ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested002ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested003ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested001ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem nested004ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested011ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested012ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested111ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nested112ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem testasadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdsadsdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asdasdToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asdasdasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem asdasdasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adsasdasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sadasdasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem asdasdToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem asdasdasdToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asdasdasdToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem asdasdasdToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem asdasdasdToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem asdasdaToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem sadasdasdToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sadasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sadasdasadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adasdaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asdasdToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem sdadasdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sadasdaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dasdasdToolStripMenuItem;
    }
}