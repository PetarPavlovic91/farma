﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._05OstaleFaze;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._05OstaleFaze._06PobacajiKrmaca
{
    public partial class EvidencijaPobacajaKrmacaAzuriranje : Form
    {
        public EvidencijaPobacajaKrmacaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OstaleFazeService ostaleFazeService = new OstaleFazeService();
                dataGridView1.DataSource = ostaleFazeService.EvidencijaPobacajaKrmacaAzuriranje(dateTimePicker1.Value);
                dataGridView1.AllowUserToDeleteRows = true;
                dataGridView1.AllowUserToAddRows = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Da li ste sigurno da zelite da izbrisete ovaj pobacaj?", "Brisanje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var id = (int)dataGridView1.CurrentRow.Cells["Id"].Value;
                        OstaleFazeService ostaleFazeService = new OstaleFazeService();
                        var result = ostaleFazeService.DeletePobacaj(id);
                        if (!result.Success)
                        {
                            MessageBox.Show(result.Message);
                        }
                        else
                        {
                            MessageBox.Show("Uspesno izbrisano");

                        }

                        dataGridView1.DataSource = ostaleFazeService.EvidencijaPobacajaKrmacaAzuriranje(dateTimePicker1.Value);
                        dataGridView1.AllowUserToDeleteRows = true;
                        dataGridView1.AllowUserToAddRows = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void dataGridView1_CellValueChanged_1(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["Krmaca"].Value == null)
                    {
                        MessageBox.Show("Polje Krmaca prvo mora biti popunjeno");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@Krmaca", row.Cells["Krmaca"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Krmaca"].Value)));
                    list.Add(new SqlParameter("@DatumPobacaja", dateTimePicker1.Value));
                    OstaleFazeService ostaleFazeService = new OstaleFazeService();
                    ostaleFazeService.SP_Save_Pobacaj(list);
                    dataGridView1.DataSource = ostaleFazeService.EvidencijaPobacajaKrmacaAzuriranje(dateTimePicker1.Value);
                    dataGridView1.AllowUserToDeleteRows = true;
                    dataGridView1.AllowUserToAddRows = true;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_UserDeletingRow_1(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Da li ste sigurno da zelite da izbrisete ovaj pobacaj?", "Brisanje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        var id = (int)dataGridView1.CurrentRow.Cells["Id"].Value;
                        OstaleFazeService ostaleFazeService = new OstaleFazeService();
                        var result = ostaleFazeService.DeletePobacaj(id);
                        if (!result.Success)
                        {
                            MessageBox.Show(result.Message);
                        }
                        else
                        {
                            MessageBox.Show("Uspesno izbrisano");

                        }

                        dataGridView1.DataSource = ostaleFazeService.EvidencijaPobacajaKrmacaAzuriranje(dateTimePicker1.Value);
                        dataGridView1.AllowUserToDeleteRows = true;
                        dataGridView1.AllowUserToAddRows = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
