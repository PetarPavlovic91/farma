﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._05OstaleFaze._06PobacajiKrmaca
{
    public partial class EvidencijaPobacajaKrmaca : Form
    {
        public EvidencijaPobacajaKrmaca()
        {
            InitializeComponent();
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPobacajaKrmacaAzuriranje());
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPobacajaKrmacaPregled());
        }
    }
}
