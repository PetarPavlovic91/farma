﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._05OstaleFaze._07UginucaPoNerastovima
{
    public partial class EvidencijaUginuca : Form
    {
        public EvidencijaUginuca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OstaleFazeService ostaleFazeService = new OstaleFazeService();
                dataGridView1.DataSource = ostaleFazeService.SP_GetAllByDate_Uginuca(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["Nerast"].Value == null)
                    {
                        MessageBox.Show("Polje Nerast prvo mora biti popunjeno");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@Nerast", row.Cells["Nerast"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Nerast"].Value)));
                    list.Add(new SqlParameter("@Kategorija", row.Cells["Kategorija"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Kategorija"].Value)));
                    list.Add(new SqlParameter("@Razlog", row.Cells["Razlog"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Razlog"].Value)));
                    list.Add(new SqlParameter("@Komada", row.Cells["Komada"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Komada"].Value)));
                    list.Add(new SqlParameter("@MestoRodjenja", row.Cells["MestoRodjenja"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["MestoRodjenja"].Value)));
                    list.Add(new SqlParameter("@DatumUginuca", dateTimePicker1.Value));
                    OstaleFazeService ostaleFazeService = new OstaleFazeService();
                    ostaleFazeService.SP_Save_Uginuce(list);
                    dataGridView1.DataSource = ostaleFazeService.SP_GetAllByDate_Uginuca(dateTimePicker1.Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
