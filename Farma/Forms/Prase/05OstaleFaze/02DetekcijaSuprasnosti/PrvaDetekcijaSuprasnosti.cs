﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._05OstaleFaze._02DetekcijaSuprasnosti
{
    public partial class PrvaDetekcijaSuprasnosti : Form
    {
        public PrvaDetekcijaSuprasnosti()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var sort = "ObjBox";
                if (radioButton1.Checked)
                {
                    sort = "Dani";
                }
                OstaleFazeService ostaleFazeService = new OstaleFazeService();
                dataGridView1.DataSource = ostaleFazeService.PrvaDetekcijaSuprasnosti((int)numericUpDown1.Value, (int)numericUpDown2.Value, sort);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var sort = "ObjBox";
            if (radioButton1.Checked)
            {
                sort = "Dani";
            }
            Helper.OpenNewForm(new PrvaDetekcijaSuprasnostiReportForm((int)numericUpDown1.Value, (int)numericUpDown2.Value, "Prva Detekcija Suprasnosti", sort));
        }
    }
}
