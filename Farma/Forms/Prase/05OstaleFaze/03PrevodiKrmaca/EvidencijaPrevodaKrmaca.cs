﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._05OstaleFaze._03PrevodiKrmaca
{
    public partial class EvidencijaPrevodaKrmaca : Form
    {
        public EvidencijaPrevodaKrmaca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OstaleFazeService ostaleFazeService = new OstaleFazeService();
                dataGridView1.DataSource = ostaleFazeService.SP_GetAllByDate_PrevodKrmaca(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["Krmaca"].Value == null)
                    {
                        MessageBox.Show("Polje Krmaca prvo mora biti popunjeno");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@PrvaSuprasnost", row.Cells["PrvaSuprasnost"].Value == DBNull.Value ? false : Convert.ToBoolean(row.Cells["PrvaSuprasnost"].Value)));
                    list.Add(new SqlParameter("@DrugaSuprasnost", row.Cells["DrugaSuprasnost"].Value == DBNull.Value ? false : Convert.ToBoolean(row.Cells["DrugaSuprasnost"].Value)));
                    list.Add(new SqlParameter("@NijeSuprasna", row.Cells["NijeSuprasna"].Value == DBNull.Value ? false : Convert.ToBoolean(row.Cells["NijeSuprasna"].Value)));
                    list.Add(new SqlParameter("@Prevedena", row.Cells["Prevedena"].Value == DBNull.Value ? false : Convert.ToBoolean(row.Cells["Prevedena"].Value)));
                    list.Add(new SqlParameter("@Krmaca", row.Cells["Krmaca"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Krmaca"].Value)));
                    list.Add(new SqlParameter("@Dat_Pristupa", dateTimePicker1.Value));
                    OstaleFazeService ostaleFazeService = new OstaleFazeService();
                    ostaleFazeService.SP_Save_PrevodKrmaca(list);
                    dataGridView1.DataSource = ostaleFazeService.SP_GetAllByDate_PrevodKrmaca(dateTimePicker1.Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
