﻿
namespace Farma.Forms.Prase._05OstaleFaze._03PrevodiKrmaca
{
    partial class EvidencijaPrevodaKrmaca
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrvaSuprasnost = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DrugaSuprasnost = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NijeSuprasna = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Prevedena = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Krmaca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 62);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(336, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Pretrazi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(87, 23);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1171, 582);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.PrvaSuprasnost,
            this.DrugaSuprasnost,
            this.NijeSuprasna,
            this.Prevedena,
            this.Krmaca});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1171, 582);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // PrvaSuprasnost
            // 
            this.PrvaSuprasnost.DataPropertyName = "PrvaSuprasnost";
            this.PrvaSuprasnost.HeaderText = "PrvaSuprasnost";
            this.PrvaSuprasnost.Name = "PrvaSuprasnost";
            this.PrvaSuprasnost.ReadOnly = true;
            this.PrvaSuprasnost.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PrvaSuprasnost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // DrugaSuprasnost
            // 
            this.DrugaSuprasnost.DataPropertyName = "DrugaSuprasnost";
            this.DrugaSuprasnost.HeaderText = "DrugaSuprasnost";
            this.DrugaSuprasnost.Name = "DrugaSuprasnost";
            this.DrugaSuprasnost.ReadOnly = true;
            this.DrugaSuprasnost.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DrugaSuprasnost.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // NijeSuprasna
            // 
            this.NijeSuprasna.DataPropertyName = "NijeSuprasna";
            this.NijeSuprasna.HeaderText = "NijeSuprasna";
            this.NijeSuprasna.Name = "NijeSuprasna";
            this.NijeSuprasna.ReadOnly = true;
            this.NijeSuprasna.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.NijeSuprasna.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Prevedena
            // 
            this.Prevedena.DataPropertyName = "Prevedena";
            this.Prevedena.HeaderText = "Prevedena";
            this.Prevedena.Name = "Prevedena";
            // 
            // Krmaca
            // 
            this.Krmaca.DataPropertyName = "Krmaca";
            this.Krmaca.HeaderText = "Krmaca";
            this.Krmaca.Name = "Krmaca";
            this.Krmaca.ReadOnly = true;
            // 
            // EvidencijaPrevodaKrmaca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1171, 644);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "EvidencijaPrevodaKrmaca";
            this.Text = "Evidencija prevoda krmaca";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PrvaSuprasnost;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DrugaSuprasnost;
        private System.Windows.Forms.DataGridViewCheckBoxColumn NijeSuprasna;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Prevedena;
        private System.Windows.Forms.DataGridViewTextBoxColumn Krmaca;
    }
}