﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._05OstaleFaze._01HormonalniTretman
{
    public partial class EvidencijaDavanjaHormona : Form
    {
        public EvidencijaDavanjaHormona()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OstaleFazeService ostaleFazeService = new OstaleFazeService();
                dataGridView1.DataSource = ostaleFazeService.SP_GetAllByDate_Hormon(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["Krmaca"].Value == null)
                    {
                        MessageBox.Show("Polje Krmaca prvo mora biti popunjeno");
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@Krmaca", row.Cells["Krmaca"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["Krmaca"].Value)));
                    list.Add(new SqlParameter("@TipHormona", row.Cells["TipHormona"].Value == DBNull.Value ? null : Convert.ToString(row.Cells["TipHormona"].Value)));
                    list.Add(new SqlParameter("@DatumDavanjaHormona", dateTimePicker1.Value));
                    OstaleFazeService ostaleFazeService = new OstaleFazeService();
                    ostaleFazeService.SP_Save_Hormon(list);
                    dataGridView1.DataSource = ostaleFazeService.SP_GetAllByDate_Hormon(dateTimePicker1.Value);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
