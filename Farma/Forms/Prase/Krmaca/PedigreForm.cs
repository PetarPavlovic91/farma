﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Krmaca;
using Farm.Utils;
using Farma.Forms.Prase._07Katroteka._02PregledPodataka;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Krmaca
{
    public partial class PedigreForm : Form
    {
        public PedigreForm(string tetovirBroj = "")
        {
            InitializeComponent();
            GeneratePedigreForm(tetovirBroj);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void GeneratePedigreForm(string tetovirBroj)
        {
            try
            {
                KrmacaService krmacaService = new KrmacaService();
                PedigreExtendedVM pedigreExt = null;
                if (!string.IsNullOrEmpty(tetovirBroj))
                {
                    pedigreExt = krmacaService.GetPedigreByTetovir(tetovirBroj);
                }
                else
                {
                    if (!string.IsNullOrEmpty(textBox1.Text))
                    {
                        pedigreExt = krmacaService.GetPedigreByTetovir(textBox1.Text);
                    }
                    else if (!string.IsNullOrEmpty(textBox2.Text))
                    {
                        pedigreExt = krmacaService.GetPedigreByMaticniBroj(textBox2.Text);
                    }
                }

                if (pedigreExt == null)
                {
                    MessageBox.Show("Pedigre nije pronadjen");
                }
                else
                {
                    textBox1.Text = pedigreExt.T_GRLA;
                    textBox2.Text = pedigreExt.MBR_GRLA;
                    textBox3.Text = pedigreExt.HR_GRLA;
                    textBox4.Text = pedigreExt.MB_OCA;
                    textBox5.Text = pedigreExt.HR_OCA;
                    textBox6.Text = pedigreExt.HR_MAJKE;
                    textBox7.Text = pedigreExt.MB_MAJKE;
                    textBox9.Text = pedigreExt.MBO_DEDE;
                    textBox8.Text = pedigreExt.HRO_DEDE;
                    textBox11.Text = pedigreExt.MBO_BABE;
                    textBox10.Text = pedigreExt.HRO_BABE;
                    textBox13.Text = pedigreExt.MBM_DEDE;
                    textBox12.Text = pedigreExt.HRM_DEDE;
                    textBox15.Text = pedigreExt.MBM_BABE;
                    textBox14.Text = pedigreExt.HRM_BABE;
                    textBox17.Text = pedigreExt.MBOD_PRAD;
                    textBox16.Text = pedigreExt.MBOD_PRAB;
                    textBox23.Text = pedigreExt.MBOB_PRAD;
                    textBox22.Text = pedigreExt.MBOB_PRAB;
                    textBox21.Text = pedigreExt.MBMD_PRAD;
                    textBox20.Text = pedigreExt.MBMD_PRAB;
                    textBox19.Text = pedigreExt.MBMB_PRAD;
                    textBox18.Text = pedigreExt.MBMD_PRAB;
                    textBox31.Text = pedigreExt.HROD_PRAD;
                    textBox30.Text = pedigreExt.HROD_PRAB;
                    textBox29.Text = pedigreExt.HROB_PRAD;
                    textBox28.Text = pedigreExt.HROB_PRAB;
                    textBox27.Text = pedigreExt.HRMD_PRAD;
                    textBox26.Text = pedigreExt.HRMD_PRAB;
                    textBox25.Text = pedigreExt.HRMB_PRAD;
                    textBox24.Text = pedigreExt.HRMB_PRAB;

                    if (pedigreExt.POL == "Z")
                    {
                        radioButton1.Checked = true;
                    }
                    else if (pedigreExt.POL == "M")
                    {
                        radioButton2.Checked = true;
                    }
                    else
                    {
                        radioButton3.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GeneratePedigreForm("");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new StampaPedigreaGrla(textBox1.Text));
        }
    }
}
