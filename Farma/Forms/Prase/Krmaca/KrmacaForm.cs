﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Krmaca;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Krmaca
{
    public partial class KrmacaForm : Form
    {
        public KrmacaForm()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                KrmacaAzuriranjeVM krmaca = new KrmacaAzuriranjeVM()
                {
                    BR_SISA = textBox15.Text,
                    CIKLUS = int.Parse(textBox8.Text),
                    //DAT_ROD = dateTimePicker1.Value,
                    //D_SKART = dateTimePicker2.Value,
                    //EKST = textBox19.Text,
                    FAZA = int.Parse(textBox7.Text),
                    GN = textBox2.Text,
                    GNM = textBox23.Text,
                    GNO = textBox23.Text,
                    GRUPA = textBox28.Text,
                    HBBROJ = textBox31.Text,
                    IEZ = int.Parse(textBox18.Text),
                    KLASA = textBox14.Text,
                    KL_P = textBox27.Text,
                    MARKICA = textBox9.Text,
                    MBR_KRM = textBox5.Text,
                    MBR_MAJKE = textBox24.Text,
                    MBR_OCA = textBox13.Text,
                    NAPOMENA = textBox25.Text,
                    NBBROJ = textBox30.Text,
                    OPIS = textBox6.Text,
                    PARIT = int.Parse(textBox8.Text),
                    RASA = textBox3.Text,
                    RAZL_SK = int.Parse(textBox4.Text),
                    RBBROJ = textBox32.Text,
                    SI1 = textBox10.Text != null ? int.Parse(textBox10.Text) : 0,
                    SI2 = textBox17.Text != null ? int.Parse(textBox17.Text) : 0,
                    SI3 = textBox16.Text != null ? int.Parse(textBox16.Text) : 0,
                    TETOVIRK10 = textBox26.Text,
                    T_KRM = textBox1.Text,
                    T_MAJKE10 = textBox24.Text,
                    T_OCA10 = textBox13.Text,
                    UK_LAKT = int.Parse(textBox38.Text),
                    UK_MRTVO = int.Parse(textBox33.Text),
                    UK_PRAZNI = int.Parse(textBox37.Text),
                    UK_PROIZ_D = int.Parse(textBox36.Text),
                    UK_ZAL = int.Parse(textBox34.Text),
                    UK_ZIVO = int.Parse(textBox29.Text)
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                KrmacaService krmacaService = new KrmacaService();
                KrmacaAzuriranjeVM krmaca = new KrmacaAzuriranjeVM();
                if (radioButton1.Checked)
                {
                    if (string.IsNullOrEmpty(textBox1.Text))
                    {
                        throw new Exception("Morate popuniti polje Krmaca");
                    }
                    krmaca = krmacaService.GetByT_Kod(textBox1.Text);
                }
                else
                {
                    if (string.IsNullOrEmpty(textBox9.Text))
                    {
                        throw new Exception("Morate popuniti polje Markica");
                    }
                    krmaca = krmacaService.GetByMarkica(textBox9.Text);
                }
                if (krmaca == null)
                {
                    throw new Exception("Krmaca nije pronadjena");
                }
                textBox2.Text = krmaca.GN;
                textBox3.Text = krmaca.RASA;
                textBox5.Text = krmaca.MBR_KRM;
                textBox39.Text = krmaca.DAT_ROD != null ? ((DateTime)krmaca.DAT_ROD).ToString("MM/dd/yyyy") : "";
                textBox40.Text = krmaca.D_SKART != null ? ((DateTime)krmaca.D_SKART).ToString("MM/dd/yyyy") : "";
                textBox4.Text = krmaca.RAZL_SK != null ? krmaca.RAZL_SK.ToString() : "";
                textBox9.Text = krmaca.MARKICA;
                textBox8.Text = krmaca.PARIT != null ? krmaca.PARIT.ToString() : "";
                textBox7.Text = krmaca.FAZA != null ? krmaca.FAZA.ToString() : "";
                //obj i box ne postoje, odakle ih vucemo?
                textBox10.Text = krmaca.SI1?.ToString();
                textBox17.Text = krmaca.SI2?.ToString();
                textBox16.Text = krmaca.SI3?.ToString();
                textBox18.Text = krmaca.IEZ != null ? krmaca.IEZ.ToString() : "";
                textBox19.Text = krmaca.EKST?.ToString();
                textBox15.Text = krmaca.BR_SISA;
                textBox14.Text = krmaca.KLASA;
                //odakle vucemo drugi text box za Klasu?
                textBox31.Text = krmaca.HBBROJ;
                textBox32.Text = krmaca.RBBROJ;
                textBox30.Text = krmaca.NBBROJ;
                textBox28.Text = krmaca.GRUPA;
                textBox26.Text = krmaca.TETOVIRK10;
                textBox25.Text = krmaca.NAPOMENA;
                textBox20.Text = krmaca.GNO;
                textBox21.Text = krmaca.T_OCA10;
                textBox24.Text = krmaca.MBR_MAJKE;
                textBox23.Text = krmaca.GNM;
                textBox22.Text = krmaca.T_MAJKE10;
                textBox13.Text = krmaca.MBR_OCA;

                dataGridView1.DataSource = krmacaService.KrmacineKlaseByMaticniBroj(krmaca.MBR_KRM);
                var prasenje = krmacaService.GetPrasenje(krmaca.Id);
                var zalucenje = krmacaService.GetZalucenje(krmaca.Id);
                dataGridView2.DataSource = krmacaService.SP_GetAllKrmacaData(krmaca.Id);
                var zivo = krmacaService.GetAvgZivo(prasenje);
                var mrtvo = krmacaService.GetAvgMrtvo(prasenje);
                textBox29.Text = zivo.ToString();
                textBox33.Text = mrtvo.ToString();
                textBox35.Text = (zivo + mrtvo).ToString();
                textBox34.Text = krmacaService.GetAvgZaluceno(zalucenje).ToString();
                textBox38.Text = krmacaService.GetAvgLakt(zalucenje).ToString();
                textBox37.Text = krmacaService.GetAvgM_I(zalucenje).ToString();
                textBox36.Text = krmacaService.GetAvgPrazniDani(zalucenje).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PedigreForm(textBox1.Text));
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PorekloForm(textBox1.Text));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            KrmacaService krmacaService = new KrmacaService();
            KrmacaAzuriranjeVM krmaca = new KrmacaAzuriranjeVM();
            if (radioButton1.Checked)
            {
                if (string.IsNullOrEmpty(textBox1.Text))
                {
                    throw new Exception("Morate popuniti polje Krmaca");
                }
                krmaca = krmacaService.GetByT_Kod(textBox1.Text);
            }
            else
            {
                if (string.IsNullOrEmpty(textBox9.Text))
                {
                    throw new Exception("Morate popuniti polje Markica");
                }
                krmaca = krmacaService.GetByMarkica(textBox9.Text);
            }
            if (krmaca == null)
            {
                throw new Exception("Krmaca nije pronadjena");
            }
            Helper.OpenNewForm(new MaticniKartonKrmaceReportForm(krmaca.Id));
        }
    }
}
