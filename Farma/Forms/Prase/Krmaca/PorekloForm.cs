﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Krmaca;
using Farm.Utils;
using Farma.Forms.Prase._07Katroteka._02PregledPodataka;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Krmaca
{
    public partial class PorekloForm : Form
    {
        public PorekloForm(string tetovirBroj = "")
        {
            InitializeComponent();
            GeneratePorekloForm(tetovirBroj);
        }
        private void GeneratePorekloForm(string tetovirBroj)
        {
            try
            {
                KrmacaService krmacaService = new KrmacaService();
                PorekloExtendedVM porekloExt = null;
                if (!string.IsNullOrEmpty(tetovirBroj))
                {
                    porekloExt = krmacaService.GetPorekloByTetovir(tetovirBroj);
                }
                else
                {
                    porekloExt = krmacaService.GetPorekloByTetovir(textBox1.Text);
                }

                if (porekloExt == null)
                {
                    MessageBox.Show("Poreklo nije pronadjeno");
                }
                else
                {
                    textBox1.Text = porekloExt.T_GRLA;
                    textBox2.Text = porekloExt.MBR_GRLA;
                    textBox3.Text = porekloExt.HR_GRLA;
                    textBox4.Text = porekloExt.MB_OCA;
                    textBox5.Text = porekloExt.HR_OCA;
                    textBox6.Text = porekloExt.HR_MAJKE;
                    textBox7.Text = porekloExt.MB_MAJKE;
                    textBox9.Text = porekloExt.MBO_DEDE;
                    textBox8.Text = porekloExt.HRO_DEDE;
                    textBox11.Text = porekloExt.MBO_BABE;
                    textBox10.Text = porekloExt.HRO_BABE;
                    textBox13.Text = porekloExt.MBM_DEDE;
                    textBox12.Text = porekloExt.HRM_DEDE;
                    textBox15.Text = porekloExt.MBM_BABE;
                    textBox14.Text = porekloExt.HRM_BABE;
                    textBox17.Text = porekloExt.MBOD_PRAD;
                    textBox16.Text = porekloExt.MBOD_PRAB;
                    textBox23.Text = porekloExt.MBOB_PRAD;
                    textBox22.Text = porekloExt.MBOB_PRAB;
                    textBox21.Text = porekloExt.MBMD_PRAD;
                    textBox20.Text = porekloExt.MBMD_PRAB;
                    textBox19.Text = porekloExt.MBMB_PRAD;
                    textBox18.Text = porekloExt.MBMD_PRAB;
                    textBox31.Text = porekloExt.HROD_PRAD;
                    textBox30.Text = porekloExt.HROD_PRAB;
                    textBox29.Text = porekloExt.HROB_PRAD;
                    textBox28.Text = porekloExt.HROB_PRAB;
                    textBox27.Text = porekloExt.HRMD_PRAD;
                    textBox26.Text = porekloExt.HRMD_PRAB;
                    textBox25.Text = porekloExt.HRMB_PRAD;
                    textBox24.Text = porekloExt.HRMB_PRAB;
                    textBox32.Text = porekloExt.GN;
                    textBox33.Text = porekloExt.MB_GRLA;

                    if (porekloExt.POL == "Z")
                    {
                        radioButton1.Checked = true;
                    }
                    else if (porekloExt.POL == "M")
                    {
                        radioButton2.Checked = true;
                    }
                    else
                    {
                        radioButton3.Checked = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            GeneratePorekloForm("");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new StampaPoreklaGrla(textBox1.Text));
        }
    }
}
