﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.Forms.Prase._02Prasenje._04KontrolaProduktivnosti;
using Farma.Forms.Prase._02Prasenje._05Analize;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._02Prasenje._02PregledPodataka
{
    public partial class PregledPodatakaPrasenja : Form
    {
        public PregledPodatakaPrasenja()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                var datumDo = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
                dataGridView1.DataSource = prasenjeService.PregledPodatakaPrasenja(datumOd, datumDo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PrasenjeService prasenjeService = new PrasenjeService();
            if (comboBox1.Text == "1. Produktivnost Krmaca")
            {
                Helper.OpenNewForm(new KontrolaProduktivnostiKrmaca(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "2. Analiza Nerastovskih majki")
            {
                Helper.OpenNewForm(new KontrolaProduktivnostiNerastovskihMajki(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "3. Distribucija Zivo Rodjene Prasadi")
            {
                Helper.OpenNewForm(new DistribucijaZivoRodjenePrasadi(dateTimePicker1.Value, dateTimePicker2.Value));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            var datumDo = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new PrasenjeReportForm(datumOd, datumDo));
        }
    }
}
