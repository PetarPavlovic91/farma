﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._02Prasenje._03KalendarPrasenja
{
    public partial class NeopraseneKrmaceINazimice : Form
    {
        public NeopraseneKrmaceINazimice()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                var sort = "ObjBox";
                if (radioButton1.Checked)
                {
                    sort = "Dani";
                }
                dataGridView1.DataSource = prasenjeService.NeopraseneKrmaceINazimice((int)numericUpDown1.Value, sort);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var sort = "ObjBox";
            if (radioButton1.Checked)
            {
                sort = "Dani";
            }
            Helper.OpenNewForm(new NeopraseneKrmaceINazimiceReportForm((int)numericUpDown1.Value, sort));
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
