﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._02Prasenje._05Analize
{
    public partial class ProizvodniRezultatiKrmacaPoParitetima : Form
    {
        public ProizvodniRezultatiKrmacaPoParitetima()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                string rasa = "";
                if (radioButton2.Checked)
                {
                    rasa = textBox1.Text;
                }
                dataGridView1.DataSource = prasenjeService.ProizvodniRezultatiKrmacaPoParitima(rasa);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string rasa = "";
            if (radioButton2.Checked)
            {
                rasa = textBox1.Text;
            }
            Helper.OpenNewForm(new ProizvodniRezultatiKrmacaPoParitetimaReportForm(rasa));
        }
    }
}
