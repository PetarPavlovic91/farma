﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._02Prasenje._05Analize
{
    public partial class DistribucijaZivoRodjenePrasadi : Form
    {
        public DistribucijaZivoRodjenePrasadi(DateTime? datumOd = null, DateTime? datumDo = null)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            if (datumOd != null && datumDo != null)
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                dataGridView1.DataSource = prasenjeService.DistribucijaZivoRodjenePrasadi(datumOd.Value, datumDo.Value, "");
                dateTimePicker1.Value = datumOd.Value;
                dateTimePicker2.Value = datumDo.Value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                string rasa = "";
                if (radioButton2.Checked)
                {
                    rasa = textBox1.Text;
                }
                dataGridView1.DataSource = prasenjeService.DistribucijaZivoRodjenePrasadi(dateTimePicker1.Value, dateTimePicker2.Value, rasa);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string rasa = "";
            if (radioButton2.Checked)
            {
                rasa = textBox1.Text;
            }
            Helper.OpenNewForm(new DistribucijaZivoRodjenePrasadiReportForm(dateTimePicker1.Value, dateTimePicker2.Value, rasa));
        }
    }
}
