﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._02Prasenje._05Analize
{
    public partial class NIIManjePrasadiUMPrasenja : Form
    {
        public NIIManjePrasadiUMPrasenja()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                string rasa = "";
                if (radioButton2.Checked)
                {
                    rasa = textBox1.Text;
                }

                bool prvih = false;
                if (radioButton3.Checked)
                {
                    prvih = true;
                }
                dataGridView1.DataSource = prasenjeService.NIIManjePrasadiUMPrasenja((int)numericUpDown1.Value, (int)numericUpDown2.Value, rasa, prvih);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string rasa = "";
            if (radioButton2.Checked)
            {
                rasa = textBox1.Text;
            }

            bool prvih = false;
            if (radioButton3.Checked)
            {
                prvih = true;
            }
            Helper.OpenNewForm(new NIIManjePrasadiUMPrasenjaReportForm((int)numericUpDown1.Value, (int)numericUpDown2.Value, rasa, prvih));
        }
    }
}
