﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._02Prasenje._04KontrolaProduktivnosti
{
    public partial class ProizvodniRezultatiKrmaca : Form
    {
        public ProizvodniRezultatiKrmaca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                dataGridView1.DataSource = prasenjeService.ProizvodniRezultatiKrmaca(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ProizvodniRezultatiKrmacaReportForm(dateTimePicker1.Value, dateTimePicker2.Value));
        }
    }
}
