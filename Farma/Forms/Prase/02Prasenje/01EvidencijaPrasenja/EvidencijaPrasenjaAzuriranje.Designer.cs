﻿
namespace Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja
{
    partial class EvidencijaPrasenjaAzuriranje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KRMACA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PARIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OBJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BOX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BAB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RAD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MAT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZIVO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MRTVO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZIVOZ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UBIJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UGNJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ZAPRIM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UGIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DODATO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ODUZETO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GAJI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TEZ_LEG = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ANOMALIJE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TMP_OD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TMP_DO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TZP_OD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TZP_DO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOV_OD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TOV_DO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REG_PRAS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OL_P = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.REFK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateCreated = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateModified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1241, 76);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(448, 28);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 1;
            this.button1.Text = "Pretrazi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(116, 28);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(265, 23);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 76);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1241, 564);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.RB,
            this.KRMACA,
            this.PARIT,
            this.OBJ,
            this.BOX,
            this.BAB,
            this.RAD,
            this.MAT,
            this.ZIVO,
            this.MRTVO,
            this.ZIVOZ,
            this.UBIJ,
            this.UGNJ,
            this.ZAPRIM,
            this.UGIN,
            this.DODATO,
            this.ODUZETO,
            this.GAJI,
            this.TEZ_LEG,
            this.ANOMALIJE,
            this.TMP_OD,
            this.TMP_DO,
            this.TZP_OD,
            this.TZP_DO,
            this.TOV_OD,
            this.TOV_DO,
            this.REG_PRAS,
            this.OL_P,
            this.REFK,
            this.DateCreated,
            this.DateModified});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1241, 564);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 640);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1241, 139);
            this.panel3.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // RB
            // 
            this.RB.DataPropertyName = "RB";
            this.RB.HeaderText = "RB";
            this.RB.Name = "RB";
            this.RB.ReadOnly = true;
            this.RB.Width = 30;
            // 
            // KRMACA
            // 
            this.KRMACA.DataPropertyName = "KRMACA";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KRMACA.DefaultCellStyle = dataGridViewCellStyle1;
            this.KRMACA.HeaderText = "KRMACA";
            this.KRMACA.Name = "KRMACA";
            this.KRMACA.Width = 70;
            // 
            // PARIT
            // 
            this.PARIT.DataPropertyName = "PARIT";
            this.PARIT.HeaderText = "PARIT";
            this.PARIT.Name = "PARIT";
            this.PARIT.ReadOnly = true;
            this.PARIT.Width = 50;
            // 
            // OBJ
            // 
            this.OBJ.DataPropertyName = "OBJ";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.OBJ.DefaultCellStyle = dataGridViewCellStyle2;
            this.OBJ.HeaderText = "OBJ";
            this.OBJ.Name = "OBJ";
            this.OBJ.Width = 40;
            // 
            // BOX
            // 
            this.BOX.DataPropertyName = "BOX";
            this.BOX.HeaderText = "BOX";
            this.BOX.Name = "BOX";
            this.BOX.Width = 40;
            // 
            // BAB
            // 
            this.BAB.DataPropertyName = "BAB";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.BAB.DefaultCellStyle = dataGridViewCellStyle3;
            this.BAB.HeaderText = "BAB";
            this.BAB.Name = "BAB";
            this.BAB.Width = 40;
            // 
            // RAD
            // 
            this.RAD.DataPropertyName = "RAD";
            this.RAD.HeaderText = "RAD";
            this.RAD.Name = "RAD";
            this.RAD.Width = 40;
            // 
            // MAT
            // 
            this.MAT.DataPropertyName = "MAT";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MAT.DefaultCellStyle = dataGridViewCellStyle4;
            this.MAT.HeaderText = "MAT";
            this.MAT.Name = "MAT";
            this.MAT.Width = 40;
            // 
            // ZIVO
            // 
            this.ZIVO.DataPropertyName = "ZIVO";
            this.ZIVO.HeaderText = "ZIVO";
            this.ZIVO.Name = "ZIVO";
            this.ZIVO.Width = 40;
            // 
            // MRTVO
            // 
            this.MRTVO.DataPropertyName = "MRTVO";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.MRTVO.DefaultCellStyle = dataGridViewCellStyle5;
            this.MRTVO.HeaderText = "MRTVO";
            this.MRTVO.Name = "MRTVO";
            this.MRTVO.Width = 50;
            // 
            // ZIVOZ
            // 
            this.ZIVOZ.DataPropertyName = "ZIVOZ";
            this.ZIVOZ.HeaderText = "ZIVOZ";
            this.ZIVOZ.Name = "ZIVOZ";
            this.ZIVOZ.Width = 50;
            // 
            // UBIJ
            // 
            this.UBIJ.DataPropertyName = "UBIJ";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.UBIJ.DefaultCellStyle = dataGridViewCellStyle6;
            this.UBIJ.HeaderText = "UBIJ";
            this.UBIJ.Name = "UBIJ";
            this.UBIJ.Width = 40;
            // 
            // UGNJ
            // 
            this.UGNJ.DataPropertyName = "UGNJ";
            this.UGNJ.HeaderText = "UGNJ";
            this.UGNJ.Name = "UGNJ";
            this.UGNJ.Width = 50;
            // 
            // ZAPRIM
            // 
            this.ZAPRIM.DataPropertyName = "ZAPRIM";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ZAPRIM.DefaultCellStyle = dataGridViewCellStyle7;
            this.ZAPRIM.HeaderText = "ZAPRIM";
            this.ZAPRIM.Name = "ZAPRIM";
            this.ZAPRIM.Width = 50;
            // 
            // UGIN
            // 
            this.UGIN.DataPropertyName = "UGIN";
            this.UGIN.HeaderText = "UGIN";
            this.UGIN.Name = "UGIN";
            this.UGIN.Width = 30;
            // 
            // DODATO
            // 
            this.DODATO.DataPropertyName = "DODATO";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DODATO.DefaultCellStyle = dataGridViewCellStyle8;
            this.DODATO.HeaderText = "DODATO";
            this.DODATO.Name = "DODATO";
            this.DODATO.Width = 30;
            // 
            // ODUZETO
            // 
            this.ODUZETO.DataPropertyName = "ODUZETO";
            this.ODUZETO.HeaderText = "ODUZETO";
            this.ODUZETO.Name = "ODUZETO";
            this.ODUZETO.Width = 30;
            // 
            // GAJI
            // 
            this.GAJI.DataPropertyName = "GAJI";
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.GAJI.DefaultCellStyle = dataGridViewCellStyle9;
            this.GAJI.HeaderText = "GAJI";
            this.GAJI.Name = "GAJI";
            this.GAJI.Width = 30;
            // 
            // TEZ_LEG
            // 
            this.TEZ_LEG.DataPropertyName = "TEZ_LEG";
            this.TEZ_LEG.HeaderText = "TEZ_LEG";
            this.TEZ_LEG.Name = "TEZ_LEG";
            this.TEZ_LEG.Width = 30;
            // 
            // ANOMALIJE
            // 
            this.ANOMALIJE.DataPropertyName = "ANOMALIJE";
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ANOMALIJE.DefaultCellStyle = dataGridViewCellStyle10;
            this.ANOMALIJE.HeaderText = "ANOMALIJE";
            this.ANOMALIJE.Name = "ANOMALIJE";
            this.ANOMALIJE.Width = 30;
            // 
            // TMP_OD
            // 
            this.TMP_OD.DataPropertyName = "TMP_OD";
            this.TMP_OD.HeaderText = "TMP_OD";
            this.TMP_OD.Name = "TMP_OD";
            this.TMP_OD.Width = 50;
            // 
            // TMP_DO
            // 
            this.TMP_DO.DataPropertyName = "TMP_DO";
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TMP_DO.DefaultCellStyle = dataGridViewCellStyle11;
            this.TMP_DO.HeaderText = "TMP_DO";
            this.TMP_DO.Name = "TMP_DO";
            this.TMP_DO.Width = 50;
            // 
            // TZP_OD
            // 
            this.TZP_OD.DataPropertyName = "TZP_OD";
            this.TZP_OD.HeaderText = "TZP_OD";
            this.TZP_OD.Name = "TZP_OD";
            this.TZP_OD.Width = 50;
            // 
            // TZP_DO
            // 
            this.TZP_DO.DataPropertyName = "TZP_DO";
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TZP_DO.DefaultCellStyle = dataGridViewCellStyle12;
            this.TZP_DO.HeaderText = "TZP_DO";
            this.TZP_DO.Name = "TZP_DO";
            this.TZP_DO.Width = 50;
            // 
            // TOV_OD
            // 
            this.TOV_OD.DataPropertyName = "TOV_OD";
            this.TOV_OD.HeaderText = "TOV_OD";
            this.TOV_OD.Name = "TOV_OD";
            this.TOV_OD.Width = 30;
            // 
            // TOV_DO
            // 
            this.TOV_DO.DataPropertyName = "TOV_DO";
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TOV_DO.DefaultCellStyle = dataGridViewCellStyle13;
            this.TOV_DO.HeaderText = "TOV_DO";
            this.TOV_DO.Name = "TOV_DO";
            this.TOV_DO.Width = 30;
            // 
            // REG_PRAS
            // 
            this.REG_PRAS.DataPropertyName = "REG_PRAS";
            this.REG_PRAS.HeaderText = "REG_PRAS";
            this.REG_PRAS.Name = "REG_PRAS";
            this.REG_PRAS.Width = 50;
            // 
            // OL_P
            // 
            this.OL_P.DataPropertyName = "OL_P";
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.OL_P.DefaultCellStyle = dataGridViewCellStyle14;
            this.OL_P.HeaderText = "OL_P";
            this.OL_P.Name = "OL_P";
            this.OL_P.Width = 30;
            // 
            // REFK
            // 
            this.REFK.DataPropertyName = "REFK";
            this.REFK.HeaderText = "REFK";
            this.REFK.Name = "REFK";
            this.REFK.Width = 30;
            // 
            // DateCreated
            // 
            this.DateCreated.DataPropertyName = "DateCreated";
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DateCreated.DefaultCellStyle = dataGridViewCellStyle15;
            this.DateCreated.HeaderText = "DateCreated";
            this.DateCreated.Name = "DateCreated";
            this.DateCreated.ReadOnly = true;
            // 
            // DateModified
            // 
            this.DateModified.DataPropertyName = "DateModified";
            this.DateModified.HeaderText = "DateModified";
            this.DateModified.Name = "DateModified";
            this.DateModified.ReadOnly = true;
            // 
            // EvidencijaPrasenjaAzuriranje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1241, 779);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EvidencijaPrasenjaAzuriranje";
            this.Text = "EvidencijaPrasenjaAzuriranje";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn RB;
        private System.Windows.Forms.DataGridViewTextBoxColumn KRMACA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PARIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn OBJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn BOX;
        private System.Windows.Forms.DataGridViewTextBoxColumn BAB;
        private System.Windows.Forms.DataGridViewTextBoxColumn RAD;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAT;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZIVO;
        private System.Windows.Forms.DataGridViewTextBoxColumn MRTVO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZIVOZ;
        private System.Windows.Forms.DataGridViewTextBoxColumn UBIJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn UGNJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn ZAPRIM;
        private System.Windows.Forms.DataGridViewTextBoxColumn UGIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn DODATO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ODUZETO;
        private System.Windows.Forms.DataGridViewTextBoxColumn GAJI;
        private System.Windows.Forms.DataGridViewTextBoxColumn TEZ_LEG;
        private System.Windows.Forms.DataGridViewTextBoxColumn ANOMALIJE;
        private System.Windows.Forms.DataGridViewTextBoxColumn TMP_OD;
        private System.Windows.Forms.DataGridViewTextBoxColumn TMP_DO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TZP_OD;
        private System.Windows.Forms.DataGridViewTextBoxColumn TZP_DO;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOV_OD;
        private System.Windows.Forms.DataGridViewTextBoxColumn TOV_DO;
        private System.Windows.Forms.DataGridViewTextBoxColumn REG_PRAS;
        private System.Windows.Forms.DataGridViewTextBoxColumn OL_P;
        private System.Windows.Forms.DataGridViewTextBoxColumn REFK;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateCreated;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateModified;
    }
}