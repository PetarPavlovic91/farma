﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja
{
    public partial class EvidencijaPrasenjaPregled : Form
    {
        public EvidencijaPrasenjaPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                if (item.Width > Helper.MaxColumnWidth)
                {
                    item.Width = Helper.MaxColumnWidth;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                dataGridView1.DataSource = prasenjeService.EvidencijaPrasenjaPregled(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
