﻿using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja
{
    public partial class EvidencijaPrasenja : Form
    {
        public EvidencijaPrasenja()
        {
            InitializeComponent();
        }

        private void buttonAzuriranje_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPrasenjaAzuriranje());
        }

        private void buttonPregled_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPrasenjaPregled());
        }
    }
}
