﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Prasenje;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja
{
    public partial class EvidencijaPrasenjaAzuriranje : Form
    {
        public EvidencijaPrasenjaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            //foreach (DataGridViewColumn item in dataGridView1.Columns)
            //{
            //    if (item.Width > Helper.MaxColumnWidth)
            //    {
            //        item.Width = Helper.MaxColumnWidth;
            //    }
            //}
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PrasenjeService prasenjeService = new PrasenjeService();
                dataGridView1.DataSource = prasenjeService.SP_GetAllByDate(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    var columnIndex = currentCell.OwningColumn.Index;
                    DataGridViewRow row = dataGridView1.CurrentRow;

                    if (row.Cells["KRMACA"].Value == null)
                    {
                        MessageBox.Show("Polje Krmaca prvo mora biti popunjeno");
                    }
                    var krmaca = row.Cells["Krmaca"].Value == null ? null : row.Cells["Krmaca"].Value.ToString();
                    PrasenjeService prasenjeService = new PrasenjeService();
                    var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                    if (id == 0)
                    {

                        string checkIskljucenje = prasenjeService.CheckIskljucenje(krmaca);
                        if (checkIskljucenje != null)
                        {
                            MessageBox.Show(checkIskljucenje);
                            return;
                        }
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@DAT_PRAS", dateTimePicker1.Value));
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@PARIT", row.Cells["PARIT"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["PARIT"].Value)));
                    list.Add(new SqlParameter("@OBJ", row.Cells["OBJ"].Value == null ? null : row.Cells["OBJ"].Value.ToString()));
                    list.Add(new SqlParameter("@BOX", row.Cells["BOX"].Value == null ? null : row.Cells["BOX"].Value.ToString()));
                    list.Add(new SqlParameter("@BAB", row.Cells["BAB"].Value == null ? null : row.Cells["BAB"].Value.ToString()));
                    list.Add(new SqlParameter("@RAD", row.Cells["RAD"].Value == null ? null : row.Cells["RAD"].Value.ToString()));
                    list.Add(new SqlParameter("@MAT", row.Cells["MAT"].Value == null ? null : row.Cells["MAT"].Value.ToString()));
                    list.Add(new SqlParameter("@ZIVO", row.Cells["ZIVO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["ZIVO"].Value)));
                    list.Add(new SqlParameter("@MRTVO", row.Cells["MRTVO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["MRTVO"].Value)));
                    list.Add(new SqlParameter("@ZIVOZ", row.Cells["ZIVOZ"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["ZIVOZ"].Value)));
                    list.Add(new SqlParameter("@UBIJ", row.Cells["UBIJ"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["UBIJ"].Value)));
                    list.Add(new SqlParameter("@UGNJ", row.Cells["UGNJ"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["UGNJ"].Value)));
                    list.Add(new SqlParameter("@ZAPRIM", row.Cells["ZAPRIM"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["ZAPRIM"].Value)));
                    list.Add(new SqlParameter("@UGIN", row.Cells["UGIN"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["UGIN"].Value)));
                    list.Add(new SqlParameter("@DODATO", row.Cells["DODATO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["DODATO"].Value)));
                    list.Add(new SqlParameter("@ODUZETO", row.Cells["ODUZETO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["ODUZETO"].Value)));
                    list.Add(new SqlParameter("@GAJI", row.Cells["GAJI"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["GAJI"].Value)));
                    list.Add(new SqlParameter("@TEZ_LEG", row.Cells["TEZ_LEG"].Value == DBNull.Value ? 0 : Convert.ToDouble(row.Cells["TEZ_LEG"].Value)));
                    list.Add(new SqlParameter("@ANOMALIJE", row.Cells["ANOMALIJE"].Value == null ? null : row.Cells["ANOMALIJE"].Value.ToString()));
                    list.Add(new SqlParameter("@TMP_OD", row.Cells["TMP_OD"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["TMP_OD"].Value)));
                    list.Add(new SqlParameter("@TMP_DO", row.Cells["TMP_DO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["TMP_DO"].Value)));
                    list.Add(new SqlParameter("@TZP_OD", row.Cells["TZP_OD"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["TZP_OD"].Value)));
                    list.Add(new SqlParameter("@TZP_DO", row.Cells["TZP_DO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["TZP_DO"].Value)));
                    list.Add(new SqlParameter("@TOV_OD", row.Cells["TOV_OD"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["TOV_OD"].Value)));
                    list.Add(new SqlParameter("@TOV_DO", row.Cells["TOV_DO"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["TOV_DO"].Value)));
                    list.Add(new SqlParameter("@REG_PRAS", row.Cells["REG_PRAS"].Value == null ? null : row.Cells["REG_PRAS"].Value.ToString()));
                    list.Add(new SqlParameter("@OL_P", row.Cells["OL_P"].Value == null ? null : row.Cells["OL_P"].Value.ToString()));
                    list.Add(new SqlParameter("@REFK", row.Cells["REFK"].Value == null ? null : row.Cells["REFK"].Value.ToString()));
                    list.Add(new SqlParameter("@KRMACA", row.Cells["KRMACA"].Value == null ? null : row.Cells["KRMACA"].Value.ToString()));
                    prasenjeService.SP_Save(list);
                    SaveOdDoZenskuPrasadMeth();
                    SaveOdDoMuskuPrasadMeth();
                    dataGridView1.DataSource = prasenjeService.SP_GetAllByDate(dateTimePicker1.Value);
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[columnIndex];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno. " + ex.Message);
            }
        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Da li ste sigurni da zelite da izbrisete ovo prasenje?", "Brisanje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataGridViewRow row = dataGridView1.CurrentRow;
                        List<SqlParameter> list = new List<SqlParameter>();
                        var prasenjeId = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                        list.Add(new SqlParameter("@Id", prasenjeId));
                        PrasenjeService prasenjeService = new PrasenjeService();
                        if (!prasenjeService.IsLatestPrasenje(prasenjeId))
                        {
                            e.Cancel = true;
                            MessageBox.Show("Neuspesno brisanje. Morate prvo izbrisati poslednje uneta prasenja za ovu krmacu.");
                        }
                        else
                            prasenjeService.SP_Delete(list);
                    }
                    else
                        e.Cancel = true;
                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                MessageBox.Show("Neuspesno brisanje. " + ex.Message);
            }
        }

        private void SaveOdDoZenskuPrasadMeth()
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    DataGridViewRow row = dataGridView1.CurrentRow;
                    List<SqlParameter> list = new List<SqlParameter>();
                    var prasenjeId = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                    PrasenjeService prasenjeService = new PrasenjeService();
                    prasenjeService.SaveOdDoZenskuPrasad(prasenjeId);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno. " + ex.Message);
            }
        }

        private void SaveOdDoMuskuPrasadMeth()
        {
            try
            {
                DataGridViewRow row = dataGridView1.CurrentRow;
                List<SqlParameter> list = new List<SqlParameter>();
                var prasenjeId = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                PrasenjeService prasenjeService = new PrasenjeService();
                prasenjeService.SaveOdDoMuskuPrasad(prasenjeId);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Neuspesno. " + ex.Message);
            }
        }

        private void MoveCell()
        {
            if (dataGridView1.CurrentCell != null && dataGridView1.CurrentRow != null)
            {
                var row = dataGridView1.CurrentRow;
                var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                if (id == 0)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[2];
                }
            }
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(() => MoveCell()));
        }
    }
}
