﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._02RezimeSmotre
{
    public partial class RezimeSmotreTrenutnoStanjeGrla : Form
    {
        public RezimeSmotreTrenutnoStanjeGrla()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView3.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView4.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                if (radioButton1.Checked)
                {
                    dataGridView1.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla("", "RezimeSmotreTrenutnoStanjeGrlaKlase");
                    dataGridView2.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla("", "RezimeSmotreTrenutnoStanjeGrlaRase");
                    dataGridView3.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla("", "RezimeSmotreTrenutnoStanjeGrlaParitet");
                    dataGridView4.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla("", "RezimeSmotreTrenutnoStanjeGrlaUkupno");
                    textBox1.Text = smotraGrlaService.GetCountKrmacaByRasa("").ToString();
                    textBox2.Text = smotraGrlaService.GetCountNazimiceByRasa("").ToString();
                    textBox3.Text = smotraGrlaService.GetCountNerastoviByRasa("").ToString();
                }
                else
                {
                    dataGridView1.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaKlase");
                    dataGridView2.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaRase");
                    dataGridView3.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaParitet");
                    dataGridView4.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrla(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaUkupno");
                    textBox1.Text = smotraGrlaService.GetCountKrmacaByRasa(textBox4.Text).ToString();
                    textBox2.Text = smotraGrlaService.GetCountNazimiceByRasa(textBox4.Text).ToString();
                    textBox3.Text = smotraGrlaService.GetCountNerastoviByRasa(textBox4.Text).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
