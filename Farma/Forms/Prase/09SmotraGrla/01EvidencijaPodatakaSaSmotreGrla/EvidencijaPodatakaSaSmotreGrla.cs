﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._01EvidencijaPodatakaSaSmotreGrla
{
    public partial class EvidencijaPodatakaSaSmotreGrla : Form
    {
        public EvidencijaPodatakaSaSmotreGrla()
        {
            InitializeComponent();
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveRezime(sender, e);
        }

        private void SaveRezime(object sender, EventArgs e)
        {
            var tetovir = textBox1.Text;
            var klasa = textBox11.Text;
            var klP = textBox12.Text;
            var markica = textBox16.Text;
            var naFarmiOd = dateTimePicker2.Value;
            var ekst = (int)numericUpDown1.Value;
            var brSisa = textBox14.Text;
            var si = (int)numericUpDown2.Value;
            var napomena = textBox17.Text;
            var hbBroj = textBox18.Text;
            var rbBroj = textBox19.Text;
            var nbBroj = textBox21.Text;
            var grupa = textBox20.Text;
            var obj = textBox23.Text;
            var box = textBox22.Text;
            var faza = (int)numericUpDown3.Value;
            var fazaX = (int)numericUpDown4.Value;
            var predFaza = (int)numericUpDown5.Value;
            if (radioButton1.Checked)
            {
                KrmacaService krmacaService = new KrmacaService();
                bool result = krmacaService.EvidencijaPodatakaSaSmotreGrla(tetovir, klasa, klP, markica, naFarmiOd, ekst, brSisa, si, napomena, hbBroj, rbBroj, nbBroj, grupa, obj, box, faza, fazaX, predFaza);
                if (result)
                {
                    MessageBox.Show("Uspesno sacuvano");
                }
                else
                {
                    MessageBox.Show("Nije uspesno sacuvano");
                }
            }
            else if (radioButton2.Checked)
            {
                KartotekaService kartotekaService = new KartotekaService();
                bool result = kartotekaService.EvidencijaPodatakaSaSmotreGrla(tetovir, klasa, null, markica, naFarmiOd, ekst, brSisa, si, napomena, hbBroj, rbBroj);
                if (result)
                {
                    MessageBox.Show("Uspesno sacuvano");
                }
                else
                {
                    MessageBox.Show("Nije uspesno sacuvano");
                }
            }
            else
            {
                SelekcijaService selekcijaService = new SelekcijaService();
                bool result = selekcijaService.EvidencijaPodatakaSaSmotreGrla(tetovir, markica, ekst, brSisa, si, napomena);
                if (result)
                {
                    MessageBox.Show("Uspesno sacuvano");
                }
                else
                {
                    MessageBox.Show("Nije uspesno sacuvano");
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    if (radioButton1.Checked)
                    {
                        KrmacaService krmacaService = new KrmacaService();
                        var krmaca = krmacaService.GetByT_Kod(textBox1.Text);
                        if (krmaca != null)
                        {
                            textBox2.Text = krmaca.MBR_KRM;
                            if (krmaca.DAT_ROD != null)
                            {
                                dateTimePicker1.Value = (DateTime)krmaca.DAT_ROD;
                            }
                            textBox3.Text = krmaca.GN;
                            textBox4.Text = krmaca.TETOVIRK10;
                            textBox5.Text = krmaca.MBR_OCA;
                            textBox7.Text = krmaca.GNO;
                            textBox6.Text = krmaca.T_OCA10;
                            textBox10.Text = krmaca.MBR_MAJKE;
                            textBox9.Text = krmaca.GNM;
                            textBox8.Text = krmaca.T_MAJKE10;
                            textBox11.Text = krmaca.KLASA;
                            textBox12.Text = krmaca.KL_P;
                            textBox14.Text = krmaca.BR_SISA;
                            numericUpDown1.Value = krmaca.EKST ?? 0;
                            numericUpDown2.Value = krmaca.SI1 ?? 0;
                            textBox17.Text = krmaca.NAPOMENA;
                            textBox16.Text = krmaca.MARKICA;
                            textBox18.Text = krmaca.HBBROJ;
                            textBox19.Text = krmaca.RBBROJ;
                            textBox21.Text = krmaca.NBBROJ;
                            textBox20.Text = krmaca.GRUPA;
                            //textBox23.Text = krmaca.obj;
                            //textBox22.Text = krmaca.box;
                            numericUpDown3.Value = krmaca.FAZA ?? 0;
                            numericUpDown5.Value = krmaca.PREDFAZA ?? 0;

                        }
                        else
                        {
                            MessageBox.Show("Krmaca nije pronadjena");
                        }
                    }
                    else if (radioButton2.Checked)
                    {
                        KartotekaService kartotekaService = new KartotekaService();
                        var nerast = kartotekaService.GetByT_Kod(textBox1.Text);
                        if (nerast != null)
                        {
                            textBox2.Text = nerast.Mat_broj_nerasta;
                            if (nerast.Rodjen != null)
                            {
                                dateTimePicker1.Value = (DateTime)nerast.Rodjen;
                            }
                            textBox3.Text = "";
                            textBox4.Text = nerast.Tetovir;
                            textBox5.Text = nerast.Mat_broj_oca;
                            textBox7.Text = nerast.GnO;
                            textBox6.Text = nerast.Tet_oca;
                            textBox10.Text = nerast.Mat_broj_majke;
                            textBox9.Text = nerast.GnM;
                            textBox8.Text = nerast.Tet_majke;
                            textBox11.Text = nerast.Klasa;
                            textBox12.Text = "";
                            numericUpDown1.Value = nerast.Ekst ?? 0;
                            numericUpDown2.Value = nerast.Si1 ?? 0;
                            textBox14.Text = nerast.Sisa;
                            textBox17.Text = nerast.Napomena;
                            textBox16.Text = nerast.Markica;
                            textBox18.Text = nerast.HB_Broj;
                            textBox19.Text = nerast.RB_Broj;
                            textBox21.Text = "";
                            textBox20.Text = "";
                            //textBox23.Text = krmaca.obj;
                            //textBox22.Text = krmaca.box;
                            numericUpDown3.Value = 0;
                            numericUpDown4.Value = 0;
                            numericUpDown5.Value = 0;

                        }
                        else
                        {
                            MessageBox.Show("Nerast nije pronadjen");
                        }
                    }
                    else
                    {
                        SelekcijaService selekcijaService = new SelekcijaService();
                        var nerast = selekcijaService.GetTestByT_Kod(textBox1.Text);
                        if (nerast != null)
                        {
                            textBox2.Text = nerast.Mat_broj_nerasta;
                            if (nerast.Rodjen != null)
                            {
                                dateTimePicker1.Value = (DateTime)nerast.Rodjen;
                            }
                            textBox3.Text = "";
                            textBox4.Text = nerast.Tetovir;
                            textBox5.Text = nerast.Mat_broj_oca;
                            textBox7.Text = nerast.GnO;
                            textBox6.Text = nerast.Tet_oca;
                            textBox10.Text = nerast.Mat_broj_majke;
                            textBox9.Text = nerast.GnM;
                            textBox8.Text = nerast.Tet_majke;
                            textBox11.Text = nerast.Klasa;
                            textBox12.Text = "";
                            numericUpDown1.Value = nerast.Ekst ?? 0;
                            numericUpDown2.Value = nerast.Si1 ?? 0;
                            textBox14.Text = nerast.Sisa;
                            textBox17.Text = nerast.Napomena;
                            textBox16.Text = nerast.Markica;
                            textBox18.Text = nerast.HB_Broj;
                            textBox19.Text = nerast.RB_Broj;
                            textBox21.Text = "";
                            textBox20.Text = "";
                            //textBox23.Text = krmaca.obj;
                            //textBox22.Text = krmaca.box;
                            numericUpDown3.Value = 0;
                            numericUpDown4.Value = 0;
                            numericUpDown5.Value = 0;

                        }
                        else
                        {
                            MessageBox.Show("Test nije pronadjen");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                textBox2.Focus();
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dateTimePicker1.Focus();
            }
        }

        private void dateTimePicker1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox3.Focus();
            }
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox4.Focus();
            }
        }

        private void textBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox5.Focus();
            }
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox10.Focus();
            }
        }

        private void textBox10_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox7.Focus();
            }
        }

        private void textBox7_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox6.Focus();
            }
        }

        private void textBox6_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox9.Focus();
            }
        }

        private void textBox9_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox8.Focus();
            }
        }

        private void textBox8_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dateTimePicker2.Focus();
            }
        }

        private void dateTimePicker2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox11.Focus();
            }
        }

        private void textBox12_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numericUpDown1.Focus();
            }
        }

        private void textBox11_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox12.Focus();
            }
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox14.Focus();
            }
        }

        private void textBox14_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numericUpDown2.Focus();
            }
        }

        private void numericUpDown2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox17.Focus();
            }
        }

        private void textBox17_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox16.Focus();
            }
        }

        private void textBox16_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox18.Focus();
            }
        }

        private void textBox18_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox19.Focus();
            }
        }

        private void textBox19_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox21.Focus();
            }
        }

        private void textBox21_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox20.Focus();
            }
        }

        private void textBox20_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox23.Focus();
            }
        }

        private void textBox23_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox22.Focus();
            }
        }

        private void textBox22_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numericUpDown3.Focus();
            }
        }

        private void numericUpDown3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numericUpDown4.Focus();
            }
        }

        private void numericUpDown4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                numericUpDown5.Focus();
            }
        }

        private void numericUpDown5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox1.Focus();
                SaveRezime(sender, e);
            }
        }
    }
}
