﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._03SmotraNerastova
{
    public partial class KomisijskiZapisnikZaNerastove : Form
    {
        public KomisijskiZapisnikZaNerastove()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var sviNerastovi = true;
                if (radioButton1.Checked)
                {
                    sviNerastovi = false;
                }
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                dataGridView1.DataSource = smotraGrlaService.KomisijskiZapisnikZaNerastove(textBox2.Text, dateTimePicker2.Value, dateTimePicker1.Value, dateTimePicker3.Value, sviNerastovi);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var sviNerastovi = true;
            if (radioButton1.Checked)
            {
                sviNerastovi = false;
            }
            Helper.OpenNewForm(new KomisijskiZapisnikZaNerastoveReportForm(dateTimePicker1.Value, dateTimePicker3.Value, dateTimePicker2.Value, sviNerastovi, textBox1.Text, textBox2.Text));
        }
    }
}
