﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._07Analize
{
    public partial class SmotraProizvodnjaKrmacaPoRasama : Form
    {
        public SmotraProizvodnjaKrmacaPoRasama()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void SmotraProizvodnjaKrmacaPoRasama_Load(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                dataGridView1.DataSource = smotraGrlaService.SmotraProizvodnjaKrmacaPoRasama();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SmotraProizvodnjaKrmacaPoRasamaReportForm());
        }
    }
}
