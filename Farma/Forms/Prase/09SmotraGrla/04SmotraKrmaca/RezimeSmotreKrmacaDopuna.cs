﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._04SmotraKrmaca
{
    public partial class RezimeSmotreKrmacaDopuna : Form
    {
        public RezimeSmotreKrmacaDopuna()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView3.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView4.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                if (radioButton1.Checked)
                {
                    dataGridView1.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna("", "RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    dataGridView2.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna("", "RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    dataGridView3.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna("", "RezimeSmotreTrenutnoStanjeGrlaParitetSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    dataGridView4.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna("", "RezimeSmotreTrenutnoStanjeGrlaUkupnoSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    textBox1.Text = smotraGrlaService.GetCountKrmacaByRasaDopuna("", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value).ToString();
                }
                else
                {
                    dataGridView1.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    dataGridView2.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    dataGridView3.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaParitetSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    dataGridView4.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaUkupnoSamoKrmaceDopuna", dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value);
                    textBox1.Text = smotraGrlaService.GetCountKrmacaByRasaDopuna(textBox4.Text, dateTimePicker1.Value, dateTimePicker2.Value, (int)numericUpDown1.Value).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
