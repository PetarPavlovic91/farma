﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._04SmotraKrmaca
{
    public partial class RezimeSmotreKrmaca : Form
    {
        public RezimeSmotreKrmaca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView2.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView3.DefaultCellStyle.ForeColor = Color.Black;
            dataGridView4.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                if (radioButton1.Checked)
                {
                    dataGridView1.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace("", "RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmace", dateTimePicker1.Value);
                    dataGridView2.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace("", "RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmace", dateTimePicker1.Value);
                    dataGridView3.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace("", "RezimeSmotreTrenutnoStanjeGrlaParitetSamoKrmace", dateTimePicker1.Value);
                    dataGridView4.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace("", "RezimeSmotreTrenutnoStanjeGrlaUkupnoSamoKrmace", dateTimePicker1.Value);
                    textBox1.Text = smotraGrlaService.GetCountKrmacaByRasa("").ToString();
                }
                else
                {
                    dataGridView1.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmace", dateTimePicker1.Value);
                    dataGridView2.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmace", dateTimePicker1.Value);
                    dataGridView3.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaParitetSamoKrmace", dateTimePicker1.Value);
                    dataGridView4.DataSource = smotraGrlaService.RezimeSmotreTrenutnoStanjeGrlaSamoKrmace(textBox4.Text, "RezimeSmotreTrenutnoStanjeGrlaUkupnoSamoKrmace", dateTimePicker1.Value);
                    textBox1.Text = smotraGrlaService.GetCountKrmacaByRasa(textBox4.Text).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
