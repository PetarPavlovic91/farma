﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._09SmotraGrla._04SmotraKrmaca
{
    public partial class KomisijskiSpisakZaKrmaceDopuna : Form
    {
        public KomisijskiSpisakZaKrmaceDopuna()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SmotraGrlaService smotraGrlaService = new SmotraGrlaService();
                dataGridView1.DataSource = smotraGrlaService.KomisijskiSpisakZaKrmaceDopuna(textBox2.Text, dateTimePicker2.Value, dateTimePicker1.Value, dateTimePicker3.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
