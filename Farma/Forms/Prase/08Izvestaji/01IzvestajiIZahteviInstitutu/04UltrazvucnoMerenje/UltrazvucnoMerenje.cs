﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._04UltrazvucnoMerenje
{
    public partial class UltrazvucnoMerenje : Form
    {
        public UltrazvucnoMerenje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                var datumOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
                bool test = true;
                if (radioButton2.Checked)
                {
                    test = false;
                }
                dataGridView1.DataSource = izvestajiService.UltrazvucnoMerenje(datumOd, test);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var datumOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
            bool test = true;
            if (radioButton2.Checked)
            {
                test = false;
            }
            Helper.OpenNewForm(new UltrazvucnoMerenjeReportForm(datumOd, test));
        }
    }
}
