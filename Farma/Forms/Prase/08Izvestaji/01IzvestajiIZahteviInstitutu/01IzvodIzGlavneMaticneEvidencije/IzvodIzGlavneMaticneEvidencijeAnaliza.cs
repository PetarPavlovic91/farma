﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._01
{
    public partial class IzvodIzGlavneMaticneEvidencijeAnaliza : Form
    {
        public IzvodIzGlavneMaticneEvidencijeAnaliza(DateTime dan, int paritet, bool isHBRB, bool isNB, bool isNerastovi, string procedura)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                dataGridView1.DataSource = izvestajiService.IzvodIzGlavneMaticneEvidencijeAnaliza(dan, paritet, isHBRB, isNB, isNerastovi, procedura);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
