﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._01;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu
{
    public partial class IzvodIzGlavneMaticneEvidencije : Form
    {
        public IzvodIzGlavneMaticneEvidencije()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
                dataGridView1.DataSource = izvestajiService.IzvodIzGlavneMaticneEvidencije(datumDo,(int)numericUpDown1.Value,checkBox1.Checked,checkBox2.Checked,checkBox3.Checked);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
            if (comboBox1.Text == "1. HB RB N struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, "Izvestaji.IzvodIzGlavneMaticneEvidencije_AnalizaHbRbNb"));
            }
            else if (comboBox1.Text == "2. Paritetna struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, "Izvestaji.IzvodIzGlavneMaticneEvidencije_AnalizaParitet"));
            }
            else if (comboBox1.Text == "3. Rasna struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, "Izvestaji.IzvodIzGlavneMaticneEvidencije_AnalizaRasa"));
            }
            else if (comboBox1.Text == "4. Klasna struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, checkBox3.Checked, "Izvestaji.IzvodIzGlavneMaticneEvidencije_AnalizaKlasa"));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeReportForm(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, checkBox3.Checked));
        }
    }
}
