﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._01
{
    public partial class IzvodIzGlavneMaticneEvidencijeDopunaAnaliza : Form
    {
        public IzvodIzGlavneMaticneEvidencijeDopunaAnaliza(DateTime dan, int paritet, bool isHBRB, bool isNB, DateTime prasenjeOd, DateTime prasenjeDo, string procedura)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                dataGridView1.DataSource = izvestajiService.IzvodIzGlavneMaticneEvidencijeDopuna(dan, paritet, isHBRB, isNB, prasenjeOd, prasenjeDo, procedura);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
