﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._01;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu
{
    public partial class IzvodIzGlavneMaticneEvidencijeDopuna : Form
    {
        public IzvodIzGlavneMaticneEvidencijeDopuna()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
                var prasenjeOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
                var prasenjeDo = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);
                dataGridView1.DataSource = izvestajiService.IzvodIzGlavneMaticneEvidencijeDopuna(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, prasenjeOd, prasenjeDo, "Izvestaji.IzvodIzGlavneMaticneEvidencijeDopuna");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
            var prasenjeOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            var prasenjeDo = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);
            if (comboBox1.Text == "1. HB RB N struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeDopunaAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, prasenjeOd, prasenjeDo, "[Izvestaji].[IzvodIzGlavneMaticneEvidencijeDopuna_AnalizaHbRbNb]"));
            }
            else if (comboBox1.Text == "2. Paritetna struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeDopunaAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, prasenjeOd, prasenjeDo, "[Izvestaji].[IzvodIzGlavneMaticneEvidencijeDopuna_AnalizaParitet]"));
            }
            else if (comboBox1.Text == "3. Rasna struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeDopunaAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, prasenjeOd, prasenjeDo, "[Izvestaji].[IzvodIzGlavneMaticneEvidencijeDopuna_AnalizaRasa]"));
            }
            else if (comboBox1.Text == "4. Klasna struktura")
            {
                Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeDopunaAnaliza(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, prasenjeOd, prasenjeDo, "[Izvestaji].[IzvodIzGlavneMaticneEvidencijeDopuna_AnalizaKlasa]"));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
            var prasenjeOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            var prasenjeDo = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeDopunaReportForm(datumDo, (int)numericUpDown1.Value, checkBox1.Checked, checkBox2.Checked, prasenjeOd, prasenjeDo));
        }
    }
}
