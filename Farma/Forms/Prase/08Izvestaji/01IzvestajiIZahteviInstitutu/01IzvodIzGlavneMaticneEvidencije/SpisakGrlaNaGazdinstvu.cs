﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu
{
    public partial class SpisakGrlaNaGazdinstvu : Form
    {
        public SpisakGrlaNaGazdinstvu()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
                dataGridView1.DataSource = izvestajiService.GrlaNaGazdinstvu(datumDo, (int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value, textBox1.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var datumDo = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new SpisakGrlaNaGazdinstvuReportForm(datumDo, textBox1.Text, (int)numericUpDown1.Value, (int)numericUpDown2.Value, (int)numericUpDown3.Value));
        }
    }
}
