﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._05KomisijskiZapisnik
{
    public partial class KomisijskiZapisnikZaKrmaceDopuna : Form
    {
        public KomisijskiZapisnikZaKrmaceDopuna()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);


                var datumOdPrasenje = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
                var datumDoPrasenje = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);

                var paritet = (int)numericUpDown1.Value;
                var rasa = textBox1.Text;
                dataGridView1.DataSource = izvestajiService.KomisijskiZapisnikZaKrmaceDopuna(datumOd, datumOdPrasenje, datumDoPrasenje, paritet, rasa);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23, 59, 59);


            var datumOdPrasenje = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            var datumDoPrasenje = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);

            var paritet = (int)numericUpDown1.Value;
            var rasa = textBox1.Text;
            var mesto = textBox2.Text;
            Helper.OpenNewForm(new KomisijskiZapisnikZaKrmaceDopunaReportForm(datumOd, mesto, rasa, datumOdPrasenje, datumDoPrasenje, paritet));
        }
    }
}
