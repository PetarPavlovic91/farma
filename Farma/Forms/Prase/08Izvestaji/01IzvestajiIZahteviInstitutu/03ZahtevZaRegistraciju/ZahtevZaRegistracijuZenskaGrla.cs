﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._03ZahtevZaRegistraciju
{
    public partial class ZahtevZaRegistracijuZenskaGrla : Form
    {
        public ZahtevZaRegistracijuZenskaGrla()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();
                var paritet = 1;
                if (radioButton2.Checked)
                {
                    paritet = (int)numericUpDown1.Value;
                }
                var datumOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
                var datumDo = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);
                dataGridView1.DataSource = izvestajiService.ZahtevZaRegistracijuZenskaGrla(datumOd, datumDo, textBox1.Text, paritet);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var paritet = 1;
            if (radioButton2.Checked)
            {
                paritet = (int)numericUpDown1.Value;
            }
            var datumOd = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            var datumDo = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new ZahtevZaRegistracijuZenskaGrlaReportForm(datumOd, datumDo, textBox1.Text, textBox2.Text, paritet));
        }
    }
}
