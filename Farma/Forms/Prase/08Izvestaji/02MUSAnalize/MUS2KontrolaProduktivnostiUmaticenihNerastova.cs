﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._08Izvestaji._02MUSAnalize
{
    public partial class MUS2KontrolaProduktivnostiUmaticenihNerastova : Form
    {
        public MUS2KontrolaProduktivnostiUmaticenihNerastova()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                IzvestajiService izvestajiService = new IzvestajiService();

                var datumOdPrasenje = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
                var datumDoPrasenje = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);

                dataGridView1.DataSource = izvestajiService.MUSKontrolaProduktivnostiUmaticenih(datumOdPrasenje, datumDoPrasenje, "Izvestaji.MUS2");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var datumOdPrasenje = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            var datumDoPrasenje = new DateTime(dateTimePicker3.Value.Year, dateTimePicker3.Value.Month, dateTimePicker3.Value.Day, 23, 59, 59);

            Helper.OpenNewForm(new MUS2KontrolaProduktivnostiUmaticenihNerastovaReportForm(datumOdPrasenje, datumDoPrasenje));
        }
    }
}
