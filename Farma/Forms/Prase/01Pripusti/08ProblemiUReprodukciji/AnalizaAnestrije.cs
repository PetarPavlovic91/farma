﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._01Pripusti._08ProblemiUReprodukciji
{
    public partial class AnalizaAnestrije : Form
    {
        public AnalizaAnestrije()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.AnalizaAnestrije(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                var analiza = pripustService.AnalizaAnestrije(dateTimePicker1.Value, dateTimePicker2.Value);
                var odDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                var doDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
                Helper.OpenNewForm(new AnalizaAnestrijeReportForm(analiza, odDate, doDate, "Analiza Anestrije"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
