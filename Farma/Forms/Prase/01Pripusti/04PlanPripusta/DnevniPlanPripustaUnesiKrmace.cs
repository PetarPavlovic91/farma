﻿using BusnissLayer.ViewModels.Prase._01Pripust;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._01Pripusti._04PlanPripusta
{
    public partial class DnevniPlanPripustaUnesiKrmace : Form
    {
        private DnevniPlanPripusta _parentForm;
        public DnevniPlanPripustaUnesiKrmace(DnevniPlanPripusta parentForm)
        {
            InitializeComponent();
            _parentForm = parentForm;
            //if (parentForm.Tetoviri == null)
            //{
            //    dataGridView1.DataSource = new List<T_KrmVM>();
            //}
            //else
            //{
            //    dataGridView1.DataSource = parentForm.Tetoviri;
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _parentForm.Tetoviri = new List<T_KrmVM>();
            foreach (DataGridViewRow item in dataGridView1.Rows)
            {
                if (item.Cells[0].Value != null)
                {
                    _parentForm.Tetoviri.Add(new T_KrmVM() { T_krm = item.Cells[0].Value.ToString() });
                }
            }
            _parentForm.button1_Click(null, null);
            this.Close();
        }
    }
}
