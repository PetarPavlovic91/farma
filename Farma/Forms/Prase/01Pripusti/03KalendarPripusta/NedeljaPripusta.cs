﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Pripust;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._01Pripusti._03KalendarPripusta
{
    public partial class NedeljaPripusta : Form
    {
        private List<PeriodSuprasnostiVM> _periods = new List<PeriodSuprasnostiVM>();
        public NedeljaPripusta()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            numericUpDown1.Value = 1;
            numericUpDown2.Value = 2022;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                var sort = "ObjBox";
                if (radioButton1.Checked)
                {
                    sort = "Dani";
                }
                _periods = pripustService.NedeljaPripusta((int)numericUpDown1.Value, (int)numericUpDown2.Value, sort);
                dataGridView1.DataSource = _periods;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new NedeljaPripustaReportForm(_periods));
        }
    }
}
