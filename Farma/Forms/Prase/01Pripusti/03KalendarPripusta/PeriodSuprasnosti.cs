﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Pripust;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._03KalendarPripusta
{
    public partial class PeriodSuprasnosti : Form
    {
        private List<PeriodSuprasnostiVM> _periods = new List<PeriodSuprasnostiVM>();
        public PeriodSuprasnosti()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var sort = "brDana";
                if (radioButton2.Checked)
                {
                    sort = "box";
                }
                PripustService pripustService = new PripustService();
                _periods = pripustService.PeriodSuprasnosti(int.Parse(numericUpDown1.Value.ToString()), int.Parse(numericUpDown2.Value.ToString()), sort);
                dataGridView1.DataSource = _periods;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PeriodSuprasnostiReportForm(_periods));
        }
    }
}
