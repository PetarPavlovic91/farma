﻿using BusnissLayer.Services.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._05ReproduktivniPokazateljKrmaca
{
    public partial class ReproduktivniPokazateljiZalucenostiKrmacaPoMesecima : Form
    {
        public ReproduktivniPokazateljiZalucenostiKrmacaPoMesecima()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.ReproduktivniPokazateljiZalucenihKrmacaPoMesecima(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
