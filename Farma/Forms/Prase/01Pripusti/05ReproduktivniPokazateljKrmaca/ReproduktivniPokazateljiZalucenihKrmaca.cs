﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Pripust.ReproduktivniPokazateljKrmaca;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._05ReproduktivniPokazateljKrmaca
{
    public partial class ReproduktivniPokazateljiZalucenihKrmaca : Form
    {
        private List<ReproduktivniPokazateljiZalucenihKrmacaVM> _list = new List<ReproduktivniPokazateljiZalucenihKrmacaVM>();
        public ReproduktivniPokazateljiZalucenihKrmaca()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                _list = pripustService.ReproduktivniPokazateljiZalucenihKrmaca(dateTimePicker1.Value, dateTimePicker2.Value);
                dataGridView1.DataSource = _list;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                _list = pripustService.ReproduktivniPokazateljiZalucenihKrmaca(dateTimePicker1.Value, dateTimePicker2.Value);
                Helper.OpenNewForm(new ReproduktivniPokazateljiZalucenihKrmacaReportForm(_list));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
