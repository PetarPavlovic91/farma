﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._01Pripusti._09Analize
{
    public partial class AnalizaPripusta : Form
    {
        public AnalizaPripusta(DateTime? datumOd = null, DateTime? datumDo = null)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            if (datumOd != null && datumDo != null)
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.AnalizaPripusta(datumOd.Value, datumDo.Value);
                dateTimePicker1.Value = datumOd.Value;
                dateTimePicker2.Value = datumDo.Value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                var datumDo = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
                dataGridView1.DataSource = pripustService.AnalizaPripusta(datumOd, datumDo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            var datumDo = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new AnalizaPripusta_Nedelje(datumOd, datumDo));
        }
    }
}
