﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Pripust;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._01EvidencijaPripusta
{
    public partial class EvidencijaPripustaAzuriranje : Form
    {
        public EvidencijaPripustaAzuriranje()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            //foreach (DataGridViewColumn item in dataGridView1.Columns)
            //{
            //    if (item.Width > Helper.MaxColumnWidth)
            //    {
            //        item.Width = Helper.MaxColumnWidth;
            //    }
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.SP_GetAllByDate(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (e.RowIndex != -1)
            //{
            //    textBox6.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            //}
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                //var tetovir = textBox1.Text;
                //PripustService pripustService = new PripustService();
                //var result = pripustService.PrenesiPodatkeZaNovuKrmacuIzTesta(tetovir);
                //if (result.Success)
                //{
                //    MessageBox.Show("Uspesan prenos");
                //}
                //else
                //{
                //    MessageBox.Show(result.Message);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow != null)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    var columnIndex = currentCell.OwningColumn.Index;
                    DataGridViewRow row = dataGridView1.CurrentRow;
                    PripustService pripustService = new PripustService();
                    var krmaca = row.Cells["Krmaca"].Value == null ? null : row.Cells["Krmaca"].Value.ToString();
                    var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                    if (id == 0)
                    {
                        bool isTest = pripustService.ProveriDaLiJeTest(krmaca);
                        if (isTest)
                        {
                            DialogResult dialogResult = MessageBox.Show("Da li zelite da prenesete ovu krmacu iz testa?", "Prenos krmace iz testa", MessageBoxButtons.YesNo);
                            if (dialogResult == DialogResult.Yes)
                            {
                                try
                                {
                                    var result = pripustService.PrenesiPodatkeZaNovuKrmacuIzTesta(krmaca);
                                    if (result.Success)
                                    {
                                        //MessageBox.Show("Uspesan prenos");
                                    }
                                    else
                                    {
                                        MessageBox.Show(result.Message);
                                        return;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                    return;
                                }
                            }
                            else if (dialogResult == DialogResult.No)
                            {
                                //do something else
                            }
                        }

                        string checkIskljucenje = pripustService.CheckIskljucenje(krmaca);
                        if (checkIskljucenje != null)
                        {
                            MessageBox.Show(checkIskljucenje);
                            return;
                        }
                    }
                    List<SqlParameter> list = new List<SqlParameter>();
                    list.Add(new SqlParameter("@DAT_PRIP", dateTimePicker1.Value));
                    list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
                    list.Add(new SqlParameter("@CIKLUS", row.Cells["Ciklus"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Ciklus"].Value)));
                    list.Add(new SqlParameter("@RBP", row.Cells["Rbp"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Rbp"].Value)));
                    list.Add(new SqlParameter("@Krmaca", row.Cells["Krmaca"].Value == null ? null : row.Cells["Krmaca"].Value.ToString()));
                    list.Add(new SqlParameter("@RAZL_P", row.Cells["Raz"].Value == null ? null : row.Cells["Raz"].Value.ToString()));
                    list.Add(new SqlParameter("@OBJ", row.Cells["OBJ"].Value == null ? null : row.Cells["OBJ"].Value.ToString()));
                    list.Add(new SqlParameter("@BOX", row.Cells["BOX"].Value == null ? null : row.Cells["BOX"].Value.ToString()));
                    list.Add(new SqlParameter("@Nerast", row.Cells["Nerast"].Value == null ? null : row.Cells["Nerast"].Value.ToString()));
                    list.Add(new SqlParameter("@PV", row.Cells["PV"].Value == null ? null : row.Cells["PV"].Value.ToString()));
                    list.Add(new SqlParameter("@OSEM", row.Cells["OSEM"].Value == null ? null : row.Cells["OSEM"].Value.ToString()));
                    list.Add(new SqlParameter("@OCENA", row.Cells["OCENA"].Value == null ? null : row.Cells["OCENA"].Value.ToString()));
                    list.Add(new SqlParameter("@Nerast2", row.Cells["Nerast2"].Value == null ? null : row.Cells["Nerast2"].Value.ToString()));
                    list.Add(new SqlParameter("@PV2", row.Cells["PV2"].Value == null ? null : row.Cells["PV2"].Value.ToString()));
                    list.Add(new SqlParameter("@OSEM2", row.Cells["OSEM2"].Value == null ? null : row.Cells["OSEM2"].Value.ToString()));
                    list.Add(new SqlParameter("@OCENA2", row.Cells["OCENA2"].Value == null ? null : row.Cells["OCENA2"].Value.ToString()));
                    list.Add(new SqlParameter("@PRIPUSNICA", row.Cells["PRIPUSNICA"].Value == null ? null : row.Cells["PRIPUSNICA"].Value.ToString()));
                    list.Add(new SqlParameter("@PRIMEDBA", row.Cells["PRIMEDBA"].Value == null ? null : row.Cells["PRIMEDBA"].Value.ToString()));
                    pripustService.SP_Save(list);
                    dataGridView1.DataSource = pripustService.SP_GetAllByDate(dateTimePicker1.Value);
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[columnIndex];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void dataGridView1_RowLeave(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView1_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {

        }

        private void dataGridView1_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (dataGridView1.CurrentRow.Cells["Id"].Value != DBNull.Value)
                {
                    if (MessageBox.Show("Da li ste sigurni da zelite da izbrisete ovaj pripust?", "Brisanje", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        DataGridViewRow row = dataGridView1.CurrentRow;
                        List<SqlParameter> list = new List<SqlParameter>();
                        var pripustId = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                        list.Add(new SqlParameter("@Id", pripustId));
                        PripustService pripustService = new PripustService();
                        if (!pripustService.IsLatestPripust(pripustId))
                        {
                            e.Cancel = true;
                            MessageBox.Show("Neuspesno brisanje. Morate prvo izbrisati poslednje unete pripuste za ovu krmacu.");
                        }
                        else
                            pripustService.SP_Delete(list);
                    }
                    else
                        e.Cancel = true;
                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                MessageBox.Show("Neuspesno brisanje. " + ex.Message);
            }
        }

        private void dataGridView1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            //try
            //{
            //    if (dataGridView1.CurrentRow != null)
            //    {
            //        DataGridViewRow row = dataGridView1.CurrentRow;
            //        List<SqlParameter> list = new List<SqlParameter>();
            //        list.Add(new SqlParameter("@DAT_PRIP", dateTimePicker1.Value));
            //        list.Add(new SqlParameter("@Id", row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value)));
            //        list.Add(new SqlParameter("@CIKLUS", row.Cells["Ciklus"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Ciklus"].Value)));
            //        list.Add(new SqlParameter("@RBP", row.Cells["Rbp"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Rbp"].Value)));
            //        list.Add(new SqlParameter("@Krmaca", row.Cells["Krmaca"].Value == null ? null : row.Cells["Krmaca"].Value.ToString()));
            //        list.Add(new SqlParameter("@RAZL_P", row.Cells["Raz"].Value == null ? null : row.Cells["Raz"].Value.ToString()));
            //        list.Add(new SqlParameter("@OBJ", row.Cells["OBJ"].Value == null ? null : row.Cells["OBJ"].Value.ToString()));
            //        list.Add(new SqlParameter("@BOX", row.Cells["BOX"].Value == null ? null : row.Cells["BOX"].Value.ToString()));
            //        list.Add(new SqlParameter("@Nerast", row.Cells["Nerast"].Value == null ? null : row.Cells["Nerast"].Value.ToString()));
            //        list.Add(new SqlParameter("@PV", row.Cells["PV"].Value == null ? null : row.Cells["PV"].Value.ToString()));
            //        list.Add(new SqlParameter("@OSEM", row.Cells["OSEM"].Value == null ? null : row.Cells["OSEM"].Value.ToString()));
            //        list.Add(new SqlParameter("@OCENA", row.Cells["OCENA"].Value == null ? null : row.Cells["OCENA"].Value.ToString()));
            //        list.Add(new SqlParameter("@Nerast2", row.Cells["Nerast2"].Value == null ? null : row.Cells["Nerast2"].Value.ToString()));
            //        list.Add(new SqlParameter("@PV2", row.Cells["PV2"].Value == null ? null : row.Cells["PV2"].Value.ToString()));
            //        list.Add(new SqlParameter("@OSEM2", row.Cells["OSEM2"].Value == null ? null : row.Cells["OSEM2"].Value.ToString()));
            //        list.Add(new SqlParameter("@OCENA2", row.Cells["OCENA2"].Value == null ? null : row.Cells["OCENA2"].Value.ToString()));
            //        list.Add(new SqlParameter("@PRIPUSNICA", row.Cells["PRIPUSNICA"].Value == null ? null : row.Cells["PRIPUSNICA"].Value.ToString()));
            //        list.Add(new SqlParameter("@PRIMEDBA", row.Cells["PRIMEDBA"].Value == null ? null : row.Cells["PRIMEDBA"].Value.ToString()));
            //        PripustService pripustService = new PripustService();
            //        pripustService.SP_Save(list);
            //        dataGridView1.DataSource = pripustService.SP_GetAllByDate(dateTimePicker1.Value);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.BeginInvoke(new MethodInvoker(() => MoveCell()));
        }

        private void MoveCell()
        {
            if (dataGridView1.CurrentCell != null && dataGridView1.CurrentRow != null)
            {
                var row = dataGridView1.CurrentRow;
                var id = row.Cells["Id"].Value == DBNull.Value ? 0 : Convert.ToInt32(row.Cells["Id"].Value);
                if (id == 0)
                {
                    var currentCell = dataGridView1.CurrentCell;
                    var rowIndex = currentCell.OwningRow.Index;
                    dataGridView1.CurrentCell = dataGridView1.Rows[rowIndex].Cells[2];
                }
            }
        }
    }
}
