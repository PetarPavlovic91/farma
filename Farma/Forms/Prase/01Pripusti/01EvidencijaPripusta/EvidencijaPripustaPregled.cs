﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._01EvidencijaPripusta
{
    public partial class EvidencijaPripustaPregled : Form
    {
        public EvidencijaPripustaPregled()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                if (item.Width > Helper.MaxColumnWidth)
                {
                    item.Width = Helper.MaxColumnWidth;
                }
            }
        }

        private void EvidencijaPripustaPregled_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.GetAllByDate(dateTimePicker1.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
