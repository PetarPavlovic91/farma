﻿
namespace Farma.Forms.Prase.Pripusti._01EvidencijaPripusta
{
    partial class EvidencijaPripustaAzuriranje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Krmaca = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaticniBroj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ciklus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rbp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Raz = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Obj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Box = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nerast = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Osem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ocena = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nerast2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pv2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Osem2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ocena2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pripusnica = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Primedba = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1171, 62);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(336, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Pretrazi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(87, 23);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 62);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1171, 482);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.RB,
            this.Krmaca,
            this.MaticniBroj,
            this.Ciklus,
            this.Rbp,
            this.Raz,
            this.Obj,
            this.Box,
            this.Nerast,
            this.Pv,
            this.Osem,
            this.Ocena,
            this.Nerast2,
            this.Pv2,
            this.Osem2,
            this.Ocena2,
            this.Pripusnica,
            this.Primedba});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1171, 482);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowEnter);
            this.dataGridView1.RowLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowLeave);
            this.dataGridView1.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dataGridView1_RowsRemoved);
            this.dataGridView1.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_RowValidated);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridView1_UserDeletingRow);
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 544);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1171, 100);
            this.panel3.TabIndex = 2;
            // 
            // Id
            // 
            this.Id.DataPropertyName = "Id";
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.Visible = false;
            // 
            // RB
            // 
            this.RB.DataPropertyName = "RB";
            this.RB.HeaderText = "RB";
            this.RB.Name = "RB";
            this.RB.Width = 30;
            // 
            // Krmaca
            // 
            this.Krmaca.DataPropertyName = "Krmaca";
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Krmaca.DefaultCellStyle = dataGridViewCellStyle1;
            this.Krmaca.HeaderText = "Krmaca";
            this.Krmaca.Name = "Krmaca";
            this.Krmaca.Width = 50;
            // 
            // MaticniBroj
            // 
            this.MaticniBroj.DataPropertyName = "MaticniBroj";
            this.MaticniBroj.HeaderText = "MaticniBroj";
            this.MaticniBroj.Name = "MaticniBroj";
            this.MaticniBroj.ReadOnly = true;
            // 
            // Ciklus
            // 
            this.Ciklus.DataPropertyName = "CIKLUS";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Ciklus.DefaultCellStyle = dataGridViewCellStyle2;
            this.Ciklus.HeaderText = "Ciklus";
            this.Ciklus.Name = "Ciklus";
            this.Ciklus.ReadOnly = true;
            this.Ciklus.Width = 50;
            // 
            // Rbp
            // 
            this.Rbp.DataPropertyName = "RBP";
            this.Rbp.HeaderText = "Rbp";
            this.Rbp.Name = "Rbp";
            this.Rbp.ReadOnly = true;
            this.Rbp.Width = 30;
            // 
            // Raz
            // 
            this.Raz.DataPropertyName = "RAZL_P";
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Raz.DefaultCellStyle = dataGridViewCellStyle3;
            this.Raz.HeaderText = "Raz";
            this.Raz.Name = "Raz";
            this.Raz.ReadOnly = true;
            this.Raz.Width = 30;
            // 
            // Obj
            // 
            this.Obj.DataPropertyName = "OBJ";
            this.Obj.HeaderText = "Obj";
            this.Obj.Name = "Obj";
            this.Obj.Width = 30;
            // 
            // Box
            // 
            this.Box.DataPropertyName = "BOX";
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Box.DefaultCellStyle = dataGridViewCellStyle4;
            this.Box.HeaderText = "Box";
            this.Box.Name = "Box";
            this.Box.Width = 30;
            // 
            // Nerast
            // 
            this.Nerast.DataPropertyName = "Nerast";
            this.Nerast.HeaderText = "Nerast";
            this.Nerast.Name = "Nerast";
            this.Nerast.Width = 50;
            // 
            // Pv
            // 
            this.Pv.DataPropertyName = "PV";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Pv.DefaultCellStyle = dataGridViewCellStyle5;
            this.Pv.HeaderText = "Pv";
            this.Pv.Name = "Pv";
            this.Pv.Width = 30;
            // 
            // Osem
            // 
            this.Osem.DataPropertyName = "OSEM";
            this.Osem.HeaderText = "Osem";
            this.Osem.Name = "Osem";
            this.Osem.Width = 40;
            // 
            // Ocena
            // 
            this.Ocena.DataPropertyName = "OCENA";
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Ocena.DefaultCellStyle = dataGridViewCellStyle6;
            this.Ocena.HeaderText = "Ocena";
            this.Ocena.Name = "Ocena";
            this.Ocena.Width = 50;
            // 
            // Nerast2
            // 
            this.Nerast2.DataPropertyName = "Nerast2";
            this.Nerast2.HeaderText = "Nerast2";
            this.Nerast2.Name = "Nerast2";
            this.Nerast2.Width = 50;
            // 
            // Pv2
            // 
            this.Pv2.DataPropertyName = "PV2";
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Pv2.DefaultCellStyle = dataGridViewCellStyle7;
            this.Pv2.HeaderText = "Pv";
            this.Pv2.Name = "Pv2";
            this.Pv2.Width = 30;
            // 
            // Osem2
            // 
            this.Osem2.DataPropertyName = "OSEM2";
            this.Osem2.HeaderText = "Osem";
            this.Osem2.Name = "Osem2";
            this.Osem2.Width = 40;
            // 
            // Ocena2
            // 
            this.Ocena2.DataPropertyName = "OCENA2";
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Ocena2.DefaultCellStyle = dataGridViewCellStyle8;
            this.Ocena2.HeaderText = "Ocena";
            this.Ocena2.Name = "Ocena2";
            this.Ocena2.Width = 50;
            // 
            // Pripusnica
            // 
            this.Pripusnica.DataPropertyName = "PRIPUSNICA";
            this.Pripusnica.HeaderText = "Pripusnica";
            this.Pripusnica.Name = "Pripusnica";
            // 
            // Primedba
            // 
            this.Primedba.DataPropertyName = "PRIMEDBA";
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Primedba.DefaultCellStyle = dataGridViewCellStyle9;
            this.Primedba.HeaderText = "Primedba";
            this.Primedba.Name = "Primedba";
            // 
            // EvidencijaPripustaAzuriranje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1171, 644);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "EvidencijaPripustaAzuriranje";
            this.Text = "EvidencijaPristupaAzuriranje";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn RB;
        private System.Windows.Forms.DataGridViewTextBoxColumn Krmaca;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaticniBroj;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ciklus;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rbp;
        private System.Windows.Forms.DataGridViewTextBoxColumn Raz;
        private System.Windows.Forms.DataGridViewTextBoxColumn Obj;
        private System.Windows.Forms.DataGridViewTextBoxColumn Box;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nerast;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Osem;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ocena;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nerast2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pv2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Osem2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ocena2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pripusnica;
        private System.Windows.Forms.DataGridViewTextBoxColumn Primedba;
    }
}