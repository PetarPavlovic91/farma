﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase.Pripust.KontrolaProduktivnostiNerastova;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._01Pripusti._06KontrolaProduktivnostiNerastova
{
    public partial class _03BioloskiTestMladihNerastova : Form
    {
        private List<BioloskiTestMladihNerastovaVM> _nerastovi = new List<BioloskiTestMladihNerastovaVM>();
        private DateTime _odDate = DateTime.Now;
        private DateTime _doDate = DateTime.Now;
        public _03BioloskiTestMladihNerastova()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                var _odDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                var _doDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
                _nerastovi = pripustService.BioloskiTestMladihNerastova(_odDate, _doDate);
                dataGridView1.DataSource = _nerastovi;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            PripustService pripustService = new PripustService();
            var _odDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            var _doDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
            _nerastovi = pripustService.BioloskiTestMladihNerastova(_odDate, _doDate);
            Helper.OpenNewForm(new BioloskiTestMladihNerastovaReportForm(_nerastovi, _odDate, _doDate));
        }
    }
}
