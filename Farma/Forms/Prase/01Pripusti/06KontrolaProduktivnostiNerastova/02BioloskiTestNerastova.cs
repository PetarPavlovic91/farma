﻿using BusnissLayer.Services.Prase;
using BusnissLayer.ViewModels.Prase._01Pripust.Nerastovi;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase._01Pripusti._06KontrolaProduktivnostiNerastova
{
    public partial class _02BioloskiTestNerastova : Form
    {
        private List<BioloskiTestNerastovaVM> _nerastovi = new List<BioloskiTestNerastovaVM>();
        private DateTime _odDate;
        private DateTime _doDate;
        public _02BioloskiTestNerastova()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                _odDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
                _doDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
                _nerastovi = pripustService.BioloskiTestNerastova(_odDate, _doDate);
                dataGridView1.DataSource = _nerastovi;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _odDate = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            _doDate = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23, 59, 59);
            Helper.OpenNewForm(new BioloskiTestNerastovaReportForm(_nerastovi, _odDate, _doDate));
        }
    }
}
