﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.Forms.Prase._01Pripusti._02PregledPodataka;
using Farma.Forms.Prase._04Iskljucenje._02PregledPodataka;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._02PregledPodataka
{
    public partial class PregledPodataka : Form
    {
        public PregledPodataka()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            foreach (DataGridViewColumn item in dataGridView1.Columns)
            {
                if (item.Width > Helper.MaxColumnWidth)
                {
                    item.Width = Helper.MaxColumnWidth;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                var datumOd = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day,0,0,0);
                var datumDo = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 23,59,59);
                dataGridView1.DataSource = pripustService.PregledPodatakaPripusti(datumOd, datumDo);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           PripustService pripustService = new PripustService();
            if (comboBox1.Text == "1. Oprasivost po nerastovima")
            {
                Helper.OpenNewForm(new OprasivostPoNerastovima(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "2. Oprasivost po osemeniteljima")
            {
                Helper.OpenNewForm(new OprasivostPoOsemeniteljima(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "3. Oprasivost po rasama nerastova")
            {
                Helper.OpenNewForm(new OprasivostPoRasamaNerastova(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "4. Oprasivost po rasama krmaca")
            {
                Helper.OpenNewForm(new OprasivostPoRasamaKrmaca(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "5. Oprasivost po ciklusima")
            {
                Helper.OpenNewForm(new OprasivostPoCiklusima(dateTimePicker1.Value, dateTimePicker2.Value));
            }
            else if (comboBox1.Text == "6. Kombinacije pripusta")
            {
                Helper.OpenNewForm(new KombinacijePripusta(dateTimePicker1.Value, dateTimePicker2.Value));
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PripustReportForm(dateTimePicker1.Value, dateTimePicker2.Value));
        }
    }
}
