﻿using BusnissLayer.Services.Prase;
using Farm.Utils;
using Farma.ReportForms.Prase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms.Prase.Pripusti._02PregledPodataka
{
    public partial class OprasivostPoCiklusima : Form
    {
        public OprasivostPoCiklusima(DateTime? datumOd = null, DateTime? datumDo = null)
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.ForeColor = Color.Black;
            if (datumOd != null && datumDo != null)
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.OprasivostPoCiklusima(datumOd.Value, datumDo.Value);
                dateTimePicker1.Value = datumOd.Value;
                dateTimePicker2.Value = datumDo.Value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PripustService pripustService = new PripustService();
                dataGridView1.DataSource = pripustService.OprasivostPoCiklusima(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new OprasivostPoCiklusimaReportForm(dateTimePicker1.Value, dateTimePicker2.Value));
        }
    }
}
