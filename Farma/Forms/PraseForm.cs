﻿using BusnissLayer.ViewModels.Prase._05OstaleFaze;
using Farm.Utils;
using Farma.Forms.Prase._01Pripusti._02PregledPodataka;
using Farma.Forms.Prase._01Pripusti._03KalendarPripusta;
using Farma.Forms.Prase._01Pripusti._04PlanPripusta;
using Farma.Forms.Prase._01Pripusti._06KontrolaProduktivnostiNerastova;
using Farma.Forms.Prase._01Pripusti._07KontrolaProduktivnostiOsemenitelja;
using Farma.Forms.Prase._01Pripusti._08ProblemiUReprodukciji;
using Farma.Forms.Prase._01Pripusti._09Analize;
using Farma.Forms.Prase._02Prasenje._02PregledPodataka;
using Farma.Forms.Prase._02Prasenje._03KalendarPrasenja;
using Farma.Forms.Prase._02Prasenje._04KontrolaProduktivnosti;
using Farma.Forms.Prase._02Prasenje._05Analize;
using Farma.Forms.Prase._03Zalucenje._02PregledPodataka;
using Farma.Forms.Prase._03Zalucenje._03KalendarZalucenja;
using Farma.Forms.Prase._03Zalucenje._04Analiza;
using Farma.Forms.Prase._04Iskljucenje._01EvidencijaIskljucenja;
using Farma.Forms.Prase._04Iskljucenje._02PregledPodataka;
using Farma.Forms.Prase._04Iskljucenje._03AnalizaIskljucenjaKrmaca;
using Farma.Forms.Prase._05OstaleFaze._01HormonalniTretman;
using Farma.Forms.Prase._05OstaleFaze._02DetekcijaSuprasnosti;
using Farma.Forms.Prase._05OstaleFaze._03PrevodiKrmaca;
using Farma.Forms.Prase._05OstaleFaze._04KrmaceULaktaciji;
using Farma.Forms.Prase._05OstaleFaze._06PobacajiKrmaca;
using Farma.Forms.Prase._05OstaleFaze._07UginucaPoNerastovima;
using Farma.Forms.Prase._05OstaleFaze._08PregledSemena;
using Farma.Forms.Prase._06Selekcija._01EvidencijaPodatakaOOdabiruITestuGrla;
using Farma.Forms.Prase._06Selekcija._02PregledPodataka;
using Farma.Forms.Prase._06Selekcija._03SelekcijskiIndeksi;
using Farma.Forms.Prase._06Selekcija._04Analize;
using Farma.Forms.Prase._06Selekcija._05AnalizaPripustaTestiranihGrla;
using Farma.Forms.Prase._07Katroteka._02PregledPodataka;
using Farma.Forms.Prase._07Katroteka._03KartotekaKrmaca;
using Farma.Forms.Prase._07Katroteka._03Krmace._02AktivneKrmaceNaDan;
using Farma.Forms.Prase._07Katroteka._03Krmace._03PregledHBBrojevaKrmaca;
using Farma.Forms.Prase._07Katroteka._03Krmace._05PregledPotomstvaPoMajci;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._01KatalogNerastova;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._02MaticniProizvodniKartonNerasta;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._03AnalizaPotomstvaPoOcu;
using Farma.Forms.Prase._07Katroteka._04Nerastovi._04NerastoviNaDan;
using Farma.Forms.Prase._07Katroteka._05Nazimice;
using Farma.Forms.Prase._07Katroteka._05Nazimice._02PreglediAnalizeNazimicaPoFazama;
using Farma.Forms.Prase._07Katroteka._05Nazimice._03NazimiceNaStanjuNaDan;
using Farma.Forms.Prase._07Katroteka._06OdabirITest;
using Farma.Forms.Prase._07Katroteka._07RegistarPrasadi;
using Farma.Forms.Prase._07Katroteka._08MatBrojIFaza;
using Farma.Forms.Prase._07Katroteka._09Sifrarnik;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._02Registri;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._03ZahtevZaRegistraciju;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._04UltrazvucnoMerenje;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._05KomisijskiZapisnik;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._06KomisijskiSpisakZaNerastove;
using Farma.Forms.Prase._08Izvestaji._01IzvestajiIZahteviInstitutu._07KomisijskiSpisakZaKrmace;
using Farma.Forms.Prase._08Izvestaji._02MUSAnalize;
using Farma.Forms.Prase._09SmotraGrla._01EvidencijaPodatakaSaSmotreGrla;
using Farma.Forms.Prase._09SmotraGrla._02RezimeSmotre;
using Farma.Forms.Prase._09SmotraGrla._03SmotraNerastova;
using Farma.Forms.Prase._09SmotraGrla._04SmotraKrmaca;
using Farma.Forms.Prase._09SmotraGrla._05SmotraNazimica;
using Farma.Forms.Prase._09SmotraGrla._06SmotraMladihNerastova;
using Farma.Forms.Prase._09SmotraGrla._07Analize;
using Farma.Forms.Prase.Krmaca;
using Farma.Forms.Prase.Prasenje._01EvidencijaPrasenja;
using Farma.Forms.Prase.Pripusti._01EvidencijaPripusta;
using Farma.Forms.Prase.Pripusti._02PregledPodataka;
using Farma.Forms.Prase.Pripusti._03KalendarPripusta;
using Farma.Forms.Prase.Pripusti._05ReproduktivniPokazateljKrmaca;
using Farma.Forms.Prase.Zalucenje._01EvidencijaZalucenja;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farma.Forms
{
    public partial class PraseForm : Form
    {
        public PraseForm()
        {
            InitializeComponent();
        }

        private void evidencijaPripustaKrmacaINazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPripusta());
        }

        private void pregledPodatakaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodataka());
        }

        private void krmaceZaPripustToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KrmaceZaPripust());
        }

        private void periodSuprasnostiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PeriodSuprasnosti());
        }

        private void prevodUCekalisteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PrevodUCekaliste());
        }

        private void zaGrupuZalucenihKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZaGrupuZalucenihKrmaca());
        }

        private void zaGrupuNazimicatestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZaGrupuNazimica());
        }

        private void dnevniPlanPripustaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new DnevniPlanPripusta());
        }

        private void reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ReproduktivniPokazateljiZalucenihKrmaca());
        }

        private void reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ReproduktivniPokazateljiZalucenostiKrmacaPoMesecima());
        }

        private void produktivnostNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KontrolaProduktivnostiUmaticenihNerastova());
        }

        private void kalendarKoriscenjaNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KalendarKoriscenjaNerastova());
        }

        private void kontrolaProduktivnostiOsemeniteljaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KontrolaProduktivnostiOsemenitelja());
        }

        private void evidencijaPrasenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPrasenja());
        }

        private void pregledPodatakaToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaPrasenja());
        }

        private void krmaceZaPrasenjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KrmaceINazimiceZaPrevodUPrasiliste());
        }

        private void neopraseneKrmaceINazimiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new NeopraseneKrmaceINazimice());
        }

        private void evidencijaZalucenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaZalucenja());
        }

        private void pregledPodatakaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaZalucenje());
        }

        private void krmaceZaZalucenjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KrmaceZaZalucenje());
        }

        private void evidencijaIskljucenjaGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaIskljucenja());
        }

        private void pregledPodatakaToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaIskljucenje());
        }

        private void ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new UkupnaIskljucenjaKrmacaPoGrupamaRazloga());
        }

        private void analizaReproduktivnihRazlogaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaReproduktivnihRazloga());
        }

        private void analizaZdravstvenihRazlogaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaZdravstvenihRazloga());
        }

        private void analizaSelekcijskihRazlogaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaSelekcijskihRazloga());
        }

        private void aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPrinudnogKlanjaKrmaca());
        }

        private void analizaUginucaKmracaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaUginucaKrmaca());
        }

        private void ukupnaIskljucenjaKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new UkupnaIskljucenjaKrmaca());
        }

        private void analizaIskljucenjaNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new UkupnaIskljucenjaNerastova());
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new UkupnaIskljucenjaNazimica());
        }

        private void kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PrvaDetekcijaSuprasnosti());
        }

        private void evidencijaPobacajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPobacajaKrmaca());
        }

        private void pregledPodatakaToolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PobacajiKrmacaINazimica());
        }

        private void pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PrenosPodatakaIzRegistraPrasadiUOdabir());
        }

        private void evidencijaOdabirGrlaMerenjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPodatakaOOdabiruGrla());
        }

        private void prenosPodatakaIzOdabiraUTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PrenosPodatakaIzOdabiraUtest());
        }

        private void evidencijaPerofrmansTestGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPodatakaSaTestaGrlaZaPriplod());
        }

        private void pregledOdabranihGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaOdabirGrla());
        }

        private void pregledTestiranihNazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaTestiranjeNazimica());
        }

        private void pregledTestiranjaMladihNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaTestiranjeMladihNerastova());
        }

        private void maticniIProizvodniKartonKrmaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KrmacaForm());
        }

        private void evidencijaPoreklaGrlaGNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PorekloForm());
        }

        private void stampaPoreklaGrlaGNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new StampaPoreklaGrla());
        }

        private void eVidencijaPedigreaGrlaRasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PedigreForm());
        }

        private void stampaPedigreaGrlaRasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new StampaPedigreaGrla());
        }

        private void grlaBezOtvorenogPedigreaporeklaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new GrlaBezOtvorenogPorekla());
        }

        private void grlaBezPotpunogPedigreaporeklaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new GrlaBezPotpunogPorekla());
        }

        private void katalogKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KartotekaKrmaca());
        }

        private void pregledAktivnihKrmacaNaDanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AktivneKrmaceNaDan());
        }

        private void hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledHBBrojevaKrmaca());
        }

        private void pregledKrmacaPoTetovirimaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pregledIAnalizaPotomstvaPoMajciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledIAnalizaPotomstvaPoMajci());
        }

        private void katalogNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KartotekaNerastova());
        }

        private void maticniIProizvodniKartonNerastaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MaticniIProizvodniKartonNerasta());
        }

        private void pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPotomstvaPoOcu());
        }

        private void toolStripMenuItem11_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledAktivnihNerastovaNaDan());
        }

        private void katalogOsemenjenihNazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KatalogNazimica());
        }

        private void pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledNazimicaGrlaZaOsiguranje());
        }

        private void pregledNazimicaMaticniPodaciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledNazimicaMaticniPodaci());
        }

        private void pripustiNazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PripustiNazimica());
        }

        private void pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PripustiNazimicaOcekivaniDatumPrasenja());
        }

        private void opraseneNazimiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new OpraseneNazimice());
        }

        private void zaluceneNazimiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZaluceneNazimice());
        }

        private void grlaUTestuOdabiruNaDanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new GrlaUTestuIOdabiruNaDan());
        }

        private void aPregeldPodatakaZenskaPrasadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPrasadiZenskaPrasad());
        }

        private void bPregledPodatakaMuskaPrasadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPrasadiMuskaPrasad());
        }

        private void cPregedPodatakaObaPolaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPrasadi());
        }

        private void dopunaRegistraPrasenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new DopunaRegistraTetoviranihPrasadi());
        }

        private void prasadNaStanjuNaDanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new TetoviranaPrasadNaStanjuNaDan());
        }

        private void evidencijaPodatakaSaSmotreGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPodatakaSaSmotreGrla());
        }

        private void rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RezimeSmotreTrenutnoStanjeGrla());
        }

        private void komisijskiZapisnikZaNerastoveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiZapisnikZaNerastove());
        }

        private void smotraNerastovaRadniSpisakToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SmotraNerastovaRadniZapisnik());
        }

        private void komisijskiZapisnikZaKrmaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiZapisnikZaKrmace());
        }

        private void komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiZapisnikZaKrmaceDopuna());
        }

        private void rezimeSmotreKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RezimeSmotreKrmaca());
        }

        private void rezimeSmotreKrmacaDopunaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RezimeSmotreKrmacaDopuna());
        }

        private void smotreKrmacaRadniSpiskaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RezimeSmotreRadniSpisak());
        }

        private void smotraKrmacaDopunaRadniSpisakToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RezimeSmotreRadniSpisakDopuna());
        }

        private void smotraNazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiSpisakZaNazimice());
        }

        private void smotraMladihNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SmotraMladihNerastova());
        }

        private void proizvodnjaPoParitetimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SmotraProizvodnjaKrmacaPoParitetima());
        }

        private void proizvodnjaPoRasamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SmotraProizvodnjaKrmacaPoRasama());
        }

        private void proizvodnjaPoKlasamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SmotraProizvodnjaKrmacaPoKlasama());
        }

        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new OprasivostPoRasamaNerastova());
        }

        private void oprasivostPoRasamaKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new OprasivostPoRasamaKrmaca());
        }

        private void oprasivostPoCiklusimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new OprasivostPoCiklusima());
        }

        private void analizaPripustaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPripusta());
        }

        private void pripustiPoRasamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KombinacijePripusta());
        }

        private void analzeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void zaGrupuMerenihNazimicaodabirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZaGrupuMerenihNazimica());
        }

        private void nedeljaPripustaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new NedeljaPripusta());
        }

        private void bioloskiTestNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new _02BioloskiTestNerastova());
        }

        private void bioloskiTestMladihNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new _03BioloskiTestMladihNerastova());
        }

        private void analizaPovadjanjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPovadjanja());
        }

        private void analizaAnestrijeIJalovostiKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaAnestrije());
        }

        private void produktivnostKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KontrolaProduktivnostiKrmaca());
        }

        private void produktivnostNerastovskihMajkiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KontrolaProduktivnostiNerastovskihMajki());
        }

        private void proizvodniRezultatiKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ProizvodniRezultatiKrmaca());
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ProizvodniRezultatiKrmacaPoParitetima());
        }

        private void distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new DistribucijaZivoRodjenePrasadi());
        }

        private void krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new NIIVIsePrasadiUMPrasenja());
        }

        private void krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new NIIManjePrasadiUMPrasenja());
        }

        private void leglaZaTetoviranjePrasadiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new LeglaZaTetoviranjePrasadi());
        }

        private void analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaIskljucenjaGrlaIzTesta());
        }

        private void analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaIskljucenjaGrlaIzRegistraPrasadi());
        }

        private void evidencijaDetekcijeSuprasnostiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaSuprasnosti());
        }

        private void pregledPodatakaToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaSuprasnosti());
        }

        private void kalendarKrmaceZaDruguDetekcijuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new DrugaDetekcijaSuprasnosti());
        }

        private void evidencijaPrevodaKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPrevodaKrmaca());
        }

        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaPrevodiKrmaca());
        }

        private void evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaOPrasenjuILaktaciji());
        }

        private void pregledPodatakaToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaLaktacija());
        }

        private void evidencijaPregledaSemenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPregledaSemena());
        }

        private void pregledPodatakaToolStripMenuItem10_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaSemena());
        }

        private void evidencijaDavanjaHormonaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaDavanjaHormona());
        }

        private void pregledPodatakaToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaDavanjaHormona());
        }

        private void analizaPrinudnoZalucenihKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPrinudnoZalucenihKrmaca());
        }

        private void evidencijaUginucaPoNerastovimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaUginuca());
        }

        private void pregledPodatakaToolStripMenuItem9_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PregledPodatakaUginuca());
        }

        private void analizaUginucaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaUginucaPoNerastovima());
        }

        private void nazimiceNaStanjuNaDanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new NazimiceNaStanjuNaDan());
        }

        private void pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PripustiNazimica());
        }

        private void pregledeTestaOdabiraPoTetovirimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPrasadi());
        }

        private void pregledRegistraPrasadiPoTetovirimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPrasadi());
        }

        private void pregledTetoviranePrasadiZaTovToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void promenaPolaPrasadimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PromenaPolaPraseta());
        }

        private void dopunaBrojaSisaKodPrasadiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new DopunaBrojaSisaKodPrasadi());
        }

        private void maticniBrojIFazaGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MaticniBrojIFazaGrla());
        }

        private void katalogFarmiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Sifrarnik farmi", "VLA_ODG"));
        }

        private void sifrarnikRasaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Sifrarnik rasa", "RASA"));
        }

        private void razloziIskljucenjaKrmacaINazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Razlozi isklj. krmaca", "SKART_K"));
        }

        private void razloziIskljucenjaNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Razlozi isklj. nerastova", "SKART_N"));
        }

        private void sifrarnikBolestiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Sifrarnik bolesti", "BOLEST"));
        }

        private void razloziPrinudnogZalucenjaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Razlozi prinud. zalucenja", "PRIN_ZAL"));
        }

        private void razloziPonovnogPripustaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Razlozi ponovnog pripusta", "PON_PRIP"));
        }

        private void razloziRokadePrasadiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Sifrarnik("Razlozi rokade prasadi", "ROKADA"));
        }

        private void si2TestKrmacaNaPlodnostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new TestKrmacaNaPlodnost_SI2());
        }

        private void si3ProgeniTestNerastovaIKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ProgeniTestKrmacaINerastovaSI3());
        }

        private void si2TestNerastovaNaPlodnostToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new TestNerastovaNaPlodnost_SI2());
        }

        private void si1PerformansTestGrlaUTestuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new EvidencijaPodatakaSaTestaGrlaZaPriplodPregled());
        }

        private void pregledGrlaPoTetovirBrojevimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPrasadi());
        }

        private void analizaPripustaTestiranihGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPripustaTestiranihGrla());
        }

        private void analizaPerformansTestaPoRasamaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPerformansTestaPoRasama());
        }

        private void rangListaOcevaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPerformansTestaPoOcevima());
        }

        private void analizaTestaPoOcuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new AnalizaPerformansTestaPoOcu());
        }

        private void toolStripMenuItem12_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencije());
        }

        private void toolStripMenuItem13_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new IzvodIzGlavneMaticneEvidencijeDopuna());
        }

        private void toolStripMenuItem15_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new SpisakGrlaNaGazdinstvu());
        }

        private void pripustiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Registri_Pripusti());
        }

        private void prasenjaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Registri_prasenja());
        }

        private void registarPriplodnogPodmladkaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new RegistarPriplodnogPodmladka());
        }

        private void registarPrasadiToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Registar_Zalucenja());
        }

        private void registarPrasadiToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Registri_RegistarPrasadi());
        }

        private void skartGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new Registri_SkartGrla());
        }

        private void zahtevZaRegistracijuZenskihGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZahtevZaRegistracijuZenskaGrla());
        }

        private void zahtevZaRegistracijuMuskihGrlaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZahtevZaRegistracijuMuskaGrla());
        }

        private void prijavaPocetkaBioloskogTestaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new PrijavaPocetkaBioloskogTesta());
        }

        private void zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new ZahtevZaIzdavanjeDozvoleOKoriscenjuNerastovaUPriplodu());
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new UltrazvucnoMerenje());
        }

        private void komisijskiZapisnikZaKrmaceToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiZapisnikZaKrmace());
        }

        private void dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiZapisnikZaKrmaceDopuna());
        }

        private void komisijskiZapisnikZaNerastoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new KomisijskiZapisnikZaNerastove());
        }

        private void komisijskiZapisnikZaNazimiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new IzvestajKomisijskiSpisakZaNazimice());
        }

        private void mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MUS1KontrolaProduktivnostiUmaticenihKrmaca());
        }

        private void mUS2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MUS2KontrolaProduktivnostiUmaticenihNerastova());
        }

        private void mUS4PerformansTestNazimicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MUS4());
        }

        private void mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MUS3());
        }

        private void mUS5PerformansTestNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MUS5());
        }

        private void mUS6BioloskiTestNerastovaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Helper.OpenNewForm(new MUS6());
        }
    }
}
