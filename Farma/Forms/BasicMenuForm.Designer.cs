﻿
namespace Farm
{
    partial class BasicMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelImeApplikacije = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonFarma = new System.Windows.Forms.Button();
            this.buttonGlavna = new System.Windows.Forms.Button();
            this.buttonLekoviApp = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelImeApplikacije);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(752, 39);
            this.panel1.TabIndex = 0;
            // 
            // labelImeApplikacije
            // 
            this.labelImeApplikacije.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelImeApplikacije.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold);
            this.labelImeApplikacije.ForeColor = System.Drawing.Color.Cornsilk;
            this.labelImeApplikacije.Location = new System.Drawing.Point(317, 6);
            this.labelImeApplikacije.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelImeApplikacije.Name = "labelImeApplikacije";
            this.labelImeApplikacije.Size = new System.Drawing.Size(118, 24);
            this.labelImeApplikacije.TabIndex = 0;
            this.labelImeApplikacije.Text = "Farma Boganj";
            this.labelImeApplikacije.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonFarma);
            this.panel2.Controls.Add(this.buttonGlavna);
            this.panel2.Controls.Add(this.buttonLekoviApp);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 39);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(752, 47);
            this.panel2.TabIndex = 1;
            // 
            // buttonFarma
            // 
            this.buttonFarma.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonFarma.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.buttonFarma.Location = new System.Drawing.Point(476, 0);
            this.buttonFarma.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonFarma.Name = "buttonFarma";
            this.buttonFarma.Size = new System.Drawing.Size(200, 45);
            this.buttonFarma.TabIndex = 2;
            this.buttonFarma.Text = "List";
            this.buttonFarma.UseVisualStyleBackColor = true;
            this.buttonFarma.Click += new System.EventHandler(this.buttonFarma_Click);
            // 
            // buttonGlavna
            // 
            this.buttonGlavna.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonGlavna.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.buttonGlavna.Location = new System.Drawing.Point(266, 0);
            this.buttonGlavna.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonGlavna.Name = "buttonGlavna";
            this.buttonGlavna.Size = new System.Drawing.Size(210, 45);
            this.buttonGlavna.TabIndex = 1;
            this.buttonGlavna.Text = "Prase";
            this.buttonGlavna.UseVisualStyleBackColor = true;
            this.buttonGlavna.Click += new System.EventHandler(this.buttonGlavna_Click);
            // 
            // buttonLekoviApp
            // 
            this.buttonLekoviApp.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonLekoviApp.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.buttonLekoviApp.Location = new System.Drawing.Point(76, 0);
            this.buttonLekoviApp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonLekoviApp.Name = "buttonLekoviApp";
            this.buttonLekoviApp.Size = new System.Drawing.Size(190, 45);
            this.buttonLekoviApp.TabIndex = 0;
            this.buttonLekoviApp.Text = "Lekovi";
            this.buttonLekoviApp.UseVisualStyleBackColor = true;
            this.buttonLekoviApp.Click += new System.EventHandler(this.buttonLekoviApp_Click);
            // 
            // BasicMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(752, 356);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "BasicMenuForm";
            this.Text = "BasicMenuForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BasicMenuForm_FormClosed);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelImeApplikacije;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button buttonFarma;
        private System.Windows.Forms.Button buttonGlavna;
        private System.Windows.Forms.Button buttonLekoviApp;
    }
}