﻿
namespace Farma.Forms
{
    partial class PraseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.pRIPUSTIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kalendarPripustaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceZaPripustToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodSuprasnostiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prevodUCekalisteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nedeljaPripustaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.planPripustaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zaGrupuZalucenihKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zaGrupuNazimicatestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zaGrupuMerenihNazimicaodabirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dnevniPlanPripustaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reproduktivniPokazateljiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontrolaProduktivnostiNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produktivnostNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bioloskiTestNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bioloskiTestMladihNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kalendarKoriscenjaNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.problemiUReprodkcijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPovadjanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analzeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pripustiPoRasamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.oprasivostPoRasamaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oprasivostPoCiklusimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPripustaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pRASENJAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPrasenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.kAlendarPrasenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceZaPrasenjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.neopraseneKrmaceINazimiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kontrolaProduktivnostiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produktivnostKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produktivnostNerastovskihMajkiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proizvodniRezultatiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zALUCENJAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaZalucenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.kalendarKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceZaZalucenjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leglaZaTetoviranjePrasadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iSKLJUCENJAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaIskljucenjaGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizeIskljucenjaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaReproduktivnihRazlogaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaZdravstvenihRazlogaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaSelekcijskihRazlogaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUginucaKmracaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ukupnaIskljucenjaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaIskljucenjaNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oSTALEFAZEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hormonalniTretmanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaDavanjaHormonaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaHormonalnogTretmanaNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaHormonlanogTretmanaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.detekcijaSuprasnostiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaDetekcijeSuprasnostiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prevodiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPrevodaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceULaktacijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.zdravstvenaZastitaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaObolenjaKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregeldPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceKojeSuBoloveleULaktacijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizeUticajaBolestiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uticajParitetaNaZdravstvenoStanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pobacajiKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPobacajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.uginucaPoNerastovimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaUginucaPoNerastovimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaUginucaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledSemenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPregledaSemenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
            this.sELEKCIJAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaOdabirGrlaMerenjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prenosPodatakaIzOdabiraUTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPerofrmansTestGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledOdabranihGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledTestiranihNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledTestiranjaMladihNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.si1PerformansTestGrlaUTestuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.si2TestKrmacaNaPlodnostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.si2TestNerastovaNaPlodnostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testSaLinijeKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledPodatakaToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.testKrmacaSaLinijeKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testNerastovaSaLinijeKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testPoRasamaSaLinijeKlanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizeRezultataPerformansTestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPerformansTestaPoRasamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rangListaOcevaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaTestaPoOcuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizaPripustaTestiranihGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kARTOTEKAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maticniIProizvodniKartonKrmaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedigreporekloGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPoreklaGrlaGNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stampaPoreklaGrlaGNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eVidencijaPedigreaGrlaRasaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stampaPedigreaGrlaRasaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krmaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledAktivnihKrmacaNaDanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledKrmacaPoTetovirimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nerastoviToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maticniIProizvodniKartonNerastaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
            this.nazimiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogOsemenjenihNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledNazimicaMaticniPodaciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pripustiNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opraseneNazimiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zaluceneNazimiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nazimiceNaStanjuNaDanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odabirITestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grlaUTestuOdabiruNaDanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registarPrasadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPregeldPodatakaZenskaPrasadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bPregledPodatakaMuskaPrasadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cPregedPodatakaObaPolaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dopunaRegistraPrasenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promenaPolaPrasadimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prasadNaStanjuNaDanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledTetoviranePrasadiZaTovToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dopunaBrojaSisaKodPrasadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maticniBrojIFazaGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sifrarnikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.katalogFarmiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sifrarnikRasaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razloziIskljucenjaNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sifrarnikBolestiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razloziPrinudnogZalucenjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razloziPonovnogPripustaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razloziRokadePrasadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.razloziUginucaPoNerastovimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aInterneSifreFarmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promenaFazeGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.promenaMaticnihPodatakaGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unosPocetnogStanjaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bLogickaKontrolaPodatakaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iZVESTAJIIANALIZEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvestajiIZahteviInstitutuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izvodIzGlavneMaticneEvidencijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
            this.registriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pripustiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.prasenjaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registarPriplodnogPodmladkaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registarPrasadiToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.registarPrasadiToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.skartGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pripusniSpisakZaNerastaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportRegistaraZaFakultetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaRegistracijuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prijavaPocetkaBioloskogTestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaNazimiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaIzdavanjePedigreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUSAnalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS4PerformansTestNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS5PerformansTestNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS6BioloskiTestNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mUS7ProizvodniRezultatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPSAnalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS3RasnaStruksturaZapataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS7ReproduktivniPokazateljiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPS11PerformansTestNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upisPodatakaSaInstitutaUBazuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upisRezultataPerformansTestaSi1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upisRegistrovanihGrlaHBIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledProizvodnjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mesecniPregledProizvodnjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nedeljniPregledProizvodnjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dnevniPregledProizvodnjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMOTRAGRLAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smotraNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.smotraNerastovaRadniSpisakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smotraKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezimeSmotreKrmacaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezimeSmotreKrmacaDopunaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smotreKrmacaRadniSpiskaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smotraNazimicaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smotraMladihNerastovaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rezimeSmotreAnalizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proizvodnjaPoParitetimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proizvodnjaPoRasamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.proizvodnjaPoKlasamaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aplicacijaPIGSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printCTRLPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseUtilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pRIPUSTIToolStripMenuItem,
            this.pRASENJAToolStripMenuItem,
            this.zALUCENJAToolStripMenuItem,
            this.iSKLJUCENJAToolStripMenuItem,
            this.oSTALEFAZEToolStripMenuItem,
            this.sELEKCIJAToolStripMenuItem,
            this.kARTOTEKAToolStripMenuItem,
            this.iZVESTAJIIANALIZEToolStripMenuItem,
            this.sMOTRAGRLAToolStripMenuItem,
            this.alatiToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.systemToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1118, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // pRIPUSTIToolStripMenuItem
            // 
            this.pRIPUSTIToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem,
            this.kalendarPripustaToolStripMenuItem,
            this.planPripustaToolStripMenuItem,
            this.reproduktivniPokazateljiKrmacaToolStripMenuItem,
            this.kontrolaProduktivnostiNerastovaToolStripMenuItem,
            this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem,
            this.problemiUReprodkcijiToolStripMenuItem,
            this.analzeToolStripMenuItem});
            this.pRIPUSTIToolStripMenuItem.Name = "pRIPUSTIToolStripMenuItem";
            this.pRIPUSTIToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.pRIPUSTIToolStripMenuItem.Text = "1.PRIPUSTI";
            // 
            // evidencijaPripustaKrmacaINazimicaToolStripMenuItem
            // 
            this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem.Name = "evidencijaPripustaKrmacaINazimicaToolStripMenuItem";
            this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem.Text = "1.Evidencija pripusta krmaca i nazimica";
            this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPripustaKrmacaINazimicaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem
            // 
            this.pregledPodatakaToolStripMenuItem.Name = "pregledPodatakaToolStripMenuItem";
            this.pregledPodatakaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.pregledPodatakaToolStripMenuItem.Text = "2.Pregled podataka";
            this.pregledPodatakaToolStripMenuItem.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem_Click);
            // 
            // kalendarPripustaToolStripMenuItem
            // 
            this.kalendarPripustaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.krmaceZaPripustToolStripMenuItem,
            this.periodSuprasnostiToolStripMenuItem,
            this.prevodUCekalisteToolStripMenuItem,
            this.nedeljaPripustaToolStripMenuItem});
            this.kalendarPripustaToolStripMenuItem.Name = "kalendarPripustaToolStripMenuItem";
            this.kalendarPripustaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.kalendarPripustaToolStripMenuItem.Text = "3.Kalendar pripusta";
            // 
            // krmaceZaPripustToolStripMenuItem
            // 
            this.krmaceZaPripustToolStripMenuItem.Name = "krmaceZaPripustToolStripMenuItem";
            this.krmaceZaPripustToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.krmaceZaPripustToolStripMenuItem.Text = "1. Krmace za pripust";
            this.krmaceZaPripustToolStripMenuItem.Click += new System.EventHandler(this.krmaceZaPripustToolStripMenuItem_Click);
            // 
            // periodSuprasnostiToolStripMenuItem
            // 
            this.periodSuprasnostiToolStripMenuItem.Name = "periodSuprasnostiToolStripMenuItem";
            this.periodSuprasnostiToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.periodSuprasnostiToolStripMenuItem.Text = "2. Period suprasnosti";
            this.periodSuprasnostiToolStripMenuItem.Click += new System.EventHandler(this.periodSuprasnostiToolStripMenuItem_Click);
            // 
            // prevodUCekalisteToolStripMenuItem
            // 
            this.prevodUCekalisteToolStripMenuItem.Name = "prevodUCekalisteToolStripMenuItem";
            this.prevodUCekalisteToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.prevodUCekalisteToolStripMenuItem.Text = "3. Prevod u cekaliste";
            this.prevodUCekalisteToolStripMenuItem.Click += new System.EventHandler(this.prevodUCekalisteToolStripMenuItem_Click);
            // 
            // nedeljaPripustaToolStripMenuItem
            // 
            this.nedeljaPripustaToolStripMenuItem.Name = "nedeljaPripustaToolStripMenuItem";
            this.nedeljaPripustaToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.nedeljaPripustaToolStripMenuItem.Text = "4. Nedelja pripusta";
            this.nedeljaPripustaToolStripMenuItem.Click += new System.EventHandler(this.nedeljaPripustaToolStripMenuItem_Click);
            // 
            // planPripustaToolStripMenuItem
            // 
            this.planPripustaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zaGrupuZalucenihKrmacaToolStripMenuItem,
            this.zaGrupuNazimicatestToolStripMenuItem,
            this.zaGrupuMerenihNazimicaodabirToolStripMenuItem,
            this.dnevniPlanPripustaToolStripMenuItem});
            this.planPripustaToolStripMenuItem.Name = "planPripustaToolStripMenuItem";
            this.planPripustaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.planPripustaToolStripMenuItem.Text = "4.Plan pripusta";
            // 
            // zaGrupuZalucenihKrmacaToolStripMenuItem
            // 
            this.zaGrupuZalucenihKrmacaToolStripMenuItem.Name = "zaGrupuZalucenihKrmacaToolStripMenuItem";
            this.zaGrupuZalucenihKrmacaToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.zaGrupuZalucenihKrmacaToolStripMenuItem.Text = "1. Za grupu zalucenih krmaca";
            this.zaGrupuZalucenihKrmacaToolStripMenuItem.Click += new System.EventHandler(this.zaGrupuZalucenihKrmacaToolStripMenuItem_Click);
            // 
            // zaGrupuNazimicatestToolStripMenuItem
            // 
            this.zaGrupuNazimicatestToolStripMenuItem.Name = "zaGrupuNazimicatestToolStripMenuItem";
            this.zaGrupuNazimicatestToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.zaGrupuNazimicatestToolStripMenuItem.Text = "2. Za grupu nazimica (test)";
            this.zaGrupuNazimicatestToolStripMenuItem.Click += new System.EventHandler(this.zaGrupuNazimicatestToolStripMenuItem_Click);
            // 
            // zaGrupuMerenihNazimicaodabirToolStripMenuItem
            // 
            this.zaGrupuMerenihNazimicaodabirToolStripMenuItem.Name = "zaGrupuMerenihNazimicaodabirToolStripMenuItem";
            this.zaGrupuMerenihNazimicaodabirToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.zaGrupuMerenihNazimicaodabirToolStripMenuItem.Text = "3. Za grupu merenih nazimica (odabir)";
            this.zaGrupuMerenihNazimicaodabirToolStripMenuItem.Click += new System.EventHandler(this.zaGrupuMerenihNazimicaodabirToolStripMenuItem_Click);
            // 
            // dnevniPlanPripustaToolStripMenuItem
            // 
            this.dnevniPlanPripustaToolStripMenuItem.Name = "dnevniPlanPripustaToolStripMenuItem";
            this.dnevniPlanPripustaToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.dnevniPlanPripustaToolStripMenuItem.Text = "4. Dnevni plan pripusta";
            this.dnevniPlanPripustaToolStripMenuItem.Click += new System.EventHandler(this.dnevniPlanPripustaToolStripMenuItem_Click);
            // 
            // reproduktivniPokazateljiKrmacaToolStripMenuItem
            // 
            this.reproduktivniPokazateljiKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem,
            this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem});
            this.reproduktivniPokazateljiKrmacaToolStripMenuItem.Name = "reproduktivniPokazateljiKrmacaToolStripMenuItem";
            this.reproduktivniPokazateljiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.reproduktivniPokazateljiKrmacaToolStripMenuItem.Text = "5.Reproduktivni pokazatelji krmaca";
            // 
            // reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem
            // 
            this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem.Name = "reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem";
            this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem.Size = new System.Drawing.Size(332, 22);
            this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem.Text = "1.Reproduktivni pokazatelji zalucenih krmaca";
            this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem.Click += new System.EventHandler(this.reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem_Click);
            // 
            // reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem
            // 
            this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem.Name = "reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem";
            this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem.Size = new System.Drawing.Size(332, 22);
            this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem.Text = "2.Reproduktivni pokazatelji krmaca po mesecima";
            this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem.Click += new System.EventHandler(this.reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem_Click);
            // 
            // kontrolaProduktivnostiNerastovaToolStripMenuItem
            // 
            this.kontrolaProduktivnostiNerastovaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produktivnostNerastovaToolStripMenuItem,
            this.bioloskiTestNerastovaToolStripMenuItem,
            this.bioloskiTestMladihNerastovaToolStripMenuItem,
            this.kalendarKoriscenjaNerastovaToolStripMenuItem});
            this.kontrolaProduktivnostiNerastovaToolStripMenuItem.Name = "kontrolaProduktivnostiNerastovaToolStripMenuItem";
            this.kontrolaProduktivnostiNerastovaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.kontrolaProduktivnostiNerastovaToolStripMenuItem.Text = "6.Kontrola produktivnosti nerastova";
            // 
            // produktivnostNerastovaToolStripMenuItem
            // 
            this.produktivnostNerastovaToolStripMenuItem.Name = "produktivnostNerastovaToolStripMenuItem";
            this.produktivnostNerastovaToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.produktivnostNerastovaToolStripMenuItem.Text = "1. Produktivnost nerastova";
            this.produktivnostNerastovaToolStripMenuItem.Click += new System.EventHandler(this.produktivnostNerastovaToolStripMenuItem_Click);
            // 
            // bioloskiTestNerastovaToolStripMenuItem
            // 
            this.bioloskiTestNerastovaToolStripMenuItem.Name = "bioloskiTestNerastovaToolStripMenuItem";
            this.bioloskiTestNerastovaToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.bioloskiTestNerastovaToolStripMenuItem.Text = "2. Bioloski test nerastova";
            this.bioloskiTestNerastovaToolStripMenuItem.Click += new System.EventHandler(this.bioloskiTestNerastovaToolStripMenuItem_Click);
            // 
            // bioloskiTestMladihNerastovaToolStripMenuItem
            // 
            this.bioloskiTestMladihNerastovaToolStripMenuItem.Name = "bioloskiTestMladihNerastovaToolStripMenuItem";
            this.bioloskiTestMladihNerastovaToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.bioloskiTestMladihNerastovaToolStripMenuItem.Text = "3. Bioloski test mladih nerastova";
            this.bioloskiTestMladihNerastovaToolStripMenuItem.Click += new System.EventHandler(this.bioloskiTestMladihNerastovaToolStripMenuItem_Click);
            // 
            // kalendarKoriscenjaNerastovaToolStripMenuItem
            // 
            this.kalendarKoriscenjaNerastovaToolStripMenuItem.Name = "kalendarKoriscenjaNerastovaToolStripMenuItem";
            this.kalendarKoriscenjaNerastovaToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.kalendarKoriscenjaNerastovaToolStripMenuItem.Text = "4. Kalendar koriscenja nerastova";
            this.kalendarKoriscenjaNerastovaToolStripMenuItem.Click += new System.EventHandler(this.kalendarKoriscenjaNerastovaToolStripMenuItem_Click);
            // 
            // kontrolaProduktivnostiOsemeniteljaToolStripMenuItem
            // 
            this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem.Name = "kontrolaProduktivnostiOsemeniteljaToolStripMenuItem";
            this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem.Text = "7.Kontrola produktivnosti osemenitelja";
            this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem.Click += new System.EventHandler(this.kontrolaProduktivnostiOsemeniteljaToolStripMenuItem_Click);
            // 
            // problemiUReprodkcijiToolStripMenuItem
            // 
            this.problemiUReprodkcijiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizaPovadjanjaToolStripMenuItem,
            this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem});
            this.problemiUReprodkcijiToolStripMenuItem.Name = "problemiUReprodkcijiToolStripMenuItem";
            this.problemiUReprodkcijiToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.problemiUReprodkcijiToolStripMenuItem.Text = "8.Problemi u reprodukciji";
            // 
            // analizaPovadjanjaToolStripMenuItem
            // 
            this.analizaPovadjanjaToolStripMenuItem.Name = "analizaPovadjanjaToolStripMenuItem";
            this.analizaPovadjanjaToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.analizaPovadjanjaToolStripMenuItem.Text = "1. Analiza povadjanja";
            this.analizaPovadjanjaToolStripMenuItem.Click += new System.EventHandler(this.analizaPovadjanjaToolStripMenuItem_Click);
            // 
            // analizaAnestrijeIJalovostiKrmacaToolStripMenuItem
            // 
            this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem.Name = "analizaAnestrijeIJalovostiKrmacaToolStripMenuItem";
            this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem.Text = "2. Analiza anestrije i jalovosti krmaca";
            this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem.Click += new System.EventHandler(this.analizaAnestrijeIJalovostiKrmacaToolStripMenuItem_Click);
            // 
            // analzeToolStripMenuItem
            // 
            this.analzeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pripustiPoRasamaToolStripMenuItem,
            this.toolStripMenuItem7,
            this.oprasivostPoRasamaKrmacaToolStripMenuItem,
            this.oprasivostPoCiklusimaToolStripMenuItem,
            this.analizaPripustaToolStripMenuItem});
            this.analzeToolStripMenuItem.Name = "analzeToolStripMenuItem";
            this.analzeToolStripMenuItem.Size = new System.Drawing.Size(280, 22);
            this.analzeToolStripMenuItem.Text = "9.Analize";
            this.analzeToolStripMenuItem.Click += new System.EventHandler(this.analzeToolStripMenuItem_Click);
            // 
            // pripustiPoRasamaToolStripMenuItem
            // 
            this.pripustiPoRasamaToolStripMenuItem.Name = "pripustiPoRasamaToolStripMenuItem";
            this.pripustiPoRasamaToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.pripustiPoRasamaToolStripMenuItem.Text = "1. Pripusti po rasama";
            this.pripustiPoRasamaToolStripMenuItem.Click += new System.EventHandler(this.pripustiPoRasamaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem7
            // 
            this.toolStripMenuItem7.Name = "toolStripMenuItem7";
            this.toolStripMenuItem7.Size = new System.Drawing.Size(254, 22);
            this.toolStripMenuItem7.Text = "2. Oprasivost po rasama nerastova";
            this.toolStripMenuItem7.Click += new System.EventHandler(this.toolStripMenuItem7_Click);
            // 
            // oprasivostPoRasamaKrmacaToolStripMenuItem
            // 
            this.oprasivostPoRasamaKrmacaToolStripMenuItem.Name = "oprasivostPoRasamaKrmacaToolStripMenuItem";
            this.oprasivostPoRasamaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.oprasivostPoRasamaKrmacaToolStripMenuItem.Text = "3. Oprasivost po rasama krmaca";
            this.oprasivostPoRasamaKrmacaToolStripMenuItem.Click += new System.EventHandler(this.oprasivostPoRasamaKrmacaToolStripMenuItem_Click);
            // 
            // oprasivostPoCiklusimaToolStripMenuItem
            // 
            this.oprasivostPoCiklusimaToolStripMenuItem.Name = "oprasivostPoCiklusimaToolStripMenuItem";
            this.oprasivostPoCiklusimaToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.oprasivostPoCiklusimaToolStripMenuItem.Text = "4. Oprasivost po ciklusima";
            this.oprasivostPoCiklusimaToolStripMenuItem.Click += new System.EventHandler(this.oprasivostPoCiklusimaToolStripMenuItem_Click);
            // 
            // analizaPripustaToolStripMenuItem
            // 
            this.analizaPripustaToolStripMenuItem.Name = "analizaPripustaToolStripMenuItem";
            this.analizaPripustaToolStripMenuItem.Size = new System.Drawing.Size(254, 22);
            this.analizaPripustaToolStripMenuItem.Text = "5. Analiza pripusta";
            this.analizaPripustaToolStripMenuItem.Click += new System.EventHandler(this.analizaPripustaToolStripMenuItem_Click);
            // 
            // pRASENJAToolStripMenuItem
            // 
            this.pRASENJAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPrasenjaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem4,
            this.kAlendarPrasenjaToolStripMenuItem,
            this.kontrolaProduktivnostiKrmacaToolStripMenuItem,
            this.toolStripMenuItem5});
            this.pRASENJAToolStripMenuItem.Name = "pRASENJAToolStripMenuItem";
            this.pRASENJAToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.pRASENJAToolStripMenuItem.Text = "2.PRASENJA";
            // 
            // evidencijaPrasenjaToolStripMenuItem
            // 
            this.evidencijaPrasenjaToolStripMenuItem.Name = "evidencijaPrasenjaToolStripMenuItem";
            this.evidencijaPrasenjaToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.evidencijaPrasenjaToolStripMenuItem.Text = "1. Evidencija prasenja";
            this.evidencijaPrasenjaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPrasenjaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem4
            // 
            this.pregledPodatakaToolStripMenuItem4.Name = "pregledPodatakaToolStripMenuItem4";
            this.pregledPodatakaToolStripMenuItem4.Size = new System.Drawing.Size(253, 22);
            this.pregledPodatakaToolStripMenuItem4.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem4.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem4_Click);
            // 
            // kAlendarPrasenjaToolStripMenuItem
            // 
            this.kAlendarPrasenjaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.krmaceZaPrasenjeToolStripMenuItem,
            this.neopraseneKrmaceINazimiceToolStripMenuItem});
            this.kAlendarPrasenjaToolStripMenuItem.Name = "kAlendarPrasenjaToolStripMenuItem";
            this.kAlendarPrasenjaToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.kAlendarPrasenjaToolStripMenuItem.Text = "3. Kalendar prasenja";
            // 
            // krmaceZaPrasenjeToolStripMenuItem
            // 
            this.krmaceZaPrasenjeToolStripMenuItem.Name = "krmaceZaPrasenjeToolStripMenuItem";
            this.krmaceZaPrasenjeToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.krmaceZaPrasenjeToolStripMenuItem.Text = "1. Krmace za prasenje";
            this.krmaceZaPrasenjeToolStripMenuItem.Click += new System.EventHandler(this.krmaceZaPrasenjeToolStripMenuItem_Click);
            // 
            // neopraseneKrmaceINazimiceToolStripMenuItem
            // 
            this.neopraseneKrmaceINazimiceToolStripMenuItem.Name = "neopraseneKrmaceINazimiceToolStripMenuItem";
            this.neopraseneKrmaceINazimiceToolStripMenuItem.Size = new System.Drawing.Size(247, 22);
            this.neopraseneKrmaceINazimiceToolStripMenuItem.Text = "2. Neoprasene krmace i nazimice";
            this.neopraseneKrmaceINazimiceToolStripMenuItem.Click += new System.EventHandler(this.neopraseneKrmaceINazimiceToolStripMenuItem_Click);
            // 
            // kontrolaProduktivnostiKrmacaToolStripMenuItem
            // 
            this.kontrolaProduktivnostiKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produktivnostKrmacaToolStripMenuItem,
            this.produktivnostNerastovskihMajkiToolStripMenuItem,
            this.proizvodniRezultatiKrmacaToolStripMenuItem});
            this.kontrolaProduktivnostiKrmacaToolStripMenuItem.Name = "kontrolaProduktivnostiKrmacaToolStripMenuItem";
            this.kontrolaProduktivnostiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.kontrolaProduktivnostiKrmacaToolStripMenuItem.Text = "4. Kontrola produktivnosti krmaca";
            // 
            // produktivnostKrmacaToolStripMenuItem
            // 
            this.produktivnostKrmacaToolStripMenuItem.Name = "produktivnostKrmacaToolStripMenuItem";
            this.produktivnostKrmacaToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.produktivnostKrmacaToolStripMenuItem.Text = "1. Produktivnost krmaca";
            this.produktivnostKrmacaToolStripMenuItem.Click += new System.EventHandler(this.produktivnostKrmacaToolStripMenuItem_Click);
            // 
            // produktivnostNerastovskihMajkiToolStripMenuItem
            // 
            this.produktivnostNerastovskihMajkiToolStripMenuItem.Name = "produktivnostNerastovskihMajkiToolStripMenuItem";
            this.produktivnostNerastovskihMajkiToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.produktivnostNerastovskihMajkiToolStripMenuItem.Text = "2. Produktivnost nerastovskih majki";
            this.produktivnostNerastovskihMajkiToolStripMenuItem.Click += new System.EventHandler(this.produktivnostNerastovskihMajkiToolStripMenuItem_Click);
            // 
            // proizvodniRezultatiKrmacaToolStripMenuItem
            // 
            this.proizvodniRezultatiKrmacaToolStripMenuItem.Name = "proizvodniRezultatiKrmacaToolStripMenuItem";
            this.proizvodniRezultatiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.proizvodniRezultatiKrmacaToolStripMenuItem.Text = "3. Proizvodni rezultati krmaca";
            this.proizvodniRezultatiKrmacaToolStripMenuItem.Click += new System.EventHandler(this.proizvodniRezultatiKrmacaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem6,
            this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem,
            this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem,
            this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem});
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(253, 22);
            this.toolStripMenuItem5.Text = "5. Analize";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(324, 22);
            this.toolStripMenuItem6.Text = "1. Proizvodni rezultati krmaca po paritetima";
            this.toolStripMenuItem6.Click += new System.EventHandler(this.toolStripMenuItem6_Click);
            // 
            // distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem
            // 
            this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem.Name = "distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem";
            this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem.Text = "2. Distribucija zivorodjene prasadi po paritetima";
            this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem.Click += new System.EventHandler(this.distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem_Click);
            // 
            // krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem
            // 
            this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem.Name = "krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem";
            this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem.Text = "3. Krmace sa N i vise prasadi u M prasenja";
            this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem.Click += new System.EventHandler(this.krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem_Click);
            // 
            // krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem
            // 
            this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem.Name = "krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem";
            this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem.Text = "4. Krmace sa N i manje prasadi u M prasenja";
            this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem.Click += new System.EventHandler(this.krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem_Click);
            // 
            // zALUCENJAToolStripMenuItem
            // 
            this.zALUCENJAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaZalucenjaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem2,
            this.kalendarKrmacaToolStripMenuItem,
            this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem});
            this.zALUCENJAToolStripMenuItem.Name = "zALUCENJAToolStripMenuItem";
            this.zALUCENJAToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.zALUCENJAToolStripMenuItem.Text = "3.ZALUCENJA";
            // 
            // evidencijaZalucenjaToolStripMenuItem
            // 
            this.evidencijaZalucenjaToolStripMenuItem.Name = "evidencijaZalucenjaToolStripMenuItem";
            this.evidencijaZalucenjaToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.evidencijaZalucenjaToolStripMenuItem.Text = "1.Evidencija zalucenja";
            this.evidencijaZalucenjaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaZalucenjaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem2
            // 
            this.pregledPodatakaToolStripMenuItem2.Name = "pregledPodatakaToolStripMenuItem2";
            this.pregledPodatakaToolStripMenuItem2.Size = new System.Drawing.Size(268, 22);
            this.pregledPodatakaToolStripMenuItem2.Text = "2.Pregled podataka";
            this.pregledPodatakaToolStripMenuItem2.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem2_Click);
            // 
            // kalendarKrmacaToolStripMenuItem
            // 
            this.kalendarKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.krmaceZaZalucenjeToolStripMenuItem,
            this.leglaZaTetoviranjePrasadiToolStripMenuItem});
            this.kalendarKrmacaToolStripMenuItem.Name = "kalendarKrmacaToolStripMenuItem";
            this.kalendarKrmacaToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.kalendarKrmacaToolStripMenuItem.Text = "3.Kalendar krmaca";
            // 
            // krmaceZaZalucenjeToolStripMenuItem
            // 
            this.krmaceZaZalucenjeToolStripMenuItem.Name = "krmaceZaZalucenjeToolStripMenuItem";
            this.krmaceZaZalucenjeToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.krmaceZaZalucenjeToolStripMenuItem.Text = "1. Krmace za zalucenje";
            this.krmaceZaZalucenjeToolStripMenuItem.Click += new System.EventHandler(this.krmaceZaZalucenjeToolStripMenuItem_Click);
            // 
            // leglaZaTetoviranjePrasadiToolStripMenuItem
            // 
            this.leglaZaTetoviranjePrasadiToolStripMenuItem.Name = "leglaZaTetoviranjePrasadiToolStripMenuItem";
            this.leglaZaTetoviranjePrasadiToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.leglaZaTetoviranjePrasadiToolStripMenuItem.Text = "2. Legla za tetoviranje prasadi";
            this.leglaZaTetoviranjePrasadiToolStripMenuItem.Click += new System.EventHandler(this.leglaZaTetoviranjePrasadiToolStripMenuItem_Click);
            // 
            // analizaPrinudnoZalucenihKrmacaToolStripMenuItem
            // 
            this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem.Name = "analizaPrinudnoZalucenihKrmacaToolStripMenuItem";
            this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem.Text = "4.Analiza prinudno zalucenih krmaca";
            this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem.Click += new System.EventHandler(this.analizaPrinudnoZalucenihKrmacaToolStripMenuItem_Click);
            // 
            // iSKLJUCENJAToolStripMenuItem
            // 
            this.iSKLJUCENJAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaIskljucenjaGrlaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem3,
            this.analizeIskljucenjaKrmacaToolStripMenuItem,
            this.analizaIskljucenjaNerastovaToolStripMenuItem,
            this.toolStripMenuItem4,
            this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem,
            this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem});
            this.iSKLJUCENJAToolStripMenuItem.Name = "iSKLJUCENJAToolStripMenuItem";
            this.iSKLJUCENJAToolStripMenuItem.Size = new System.Drawing.Size(98, 20);
            this.iSKLJUCENJAToolStripMenuItem.Text = "4.ISKLJUCENJA";
            // 
            // evidencijaIskljucenjaGrlaToolStripMenuItem
            // 
            this.evidencijaIskljucenjaGrlaToolStripMenuItem.Name = "evidencijaIskljucenjaGrlaToolStripMenuItem";
            this.evidencijaIskljucenjaGrlaToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.evidencijaIskljucenjaGrlaToolStripMenuItem.Text = "1. Evidencija iskljucenja grla";
            this.evidencijaIskljucenjaGrlaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaIskljucenjaGrlaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem3
            // 
            this.pregledPodatakaToolStripMenuItem3.Name = "pregledPodatakaToolStripMenuItem3";
            this.pregledPodatakaToolStripMenuItem3.Size = new System.Drawing.Size(299, 22);
            this.pregledPodatakaToolStripMenuItem3.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem3.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem3_Click);
            // 
            // analizeIskljucenjaKrmacaToolStripMenuItem
            // 
            this.analizeIskljucenjaKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem,
            this.analizaReproduktivnihRazlogaToolStripMenuItem,
            this.analizaZdravstvenihRazlogaToolStripMenuItem,
            this.analizaSelekcijskihRazlogaToolStripMenuItem,
            this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem,
            this.analizaUginucaKmracaToolStripMenuItem,
            this.ukupnaIskljucenjaKrmacaToolStripMenuItem});
            this.analizeIskljucenjaKrmacaToolStripMenuItem.Name = "analizeIskljucenjaKrmacaToolStripMenuItem";
            this.analizeIskljucenjaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.analizeIskljucenjaKrmacaToolStripMenuItem.Text = "3. Analize iskljucenja krmaca";
            // 
            // ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem
            // 
            this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem.Name = "ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem";
            this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem.Text = "1. Ukupna iskljucenja po grupama razloga";
            this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem.Click += new System.EventHandler(this.ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem_Click);
            // 
            // analizaReproduktivnihRazlogaToolStripMenuItem
            // 
            this.analizaReproduktivnihRazlogaToolStripMenuItem.Name = "analizaReproduktivnihRazlogaToolStripMenuItem";
            this.analizaReproduktivnihRazlogaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.analizaReproduktivnihRazlogaToolStripMenuItem.Text = "2. Analiza reproduktivnih razloga";
            this.analizaReproduktivnihRazlogaToolStripMenuItem.Click += new System.EventHandler(this.analizaReproduktivnihRazlogaToolStripMenuItem_Click);
            // 
            // analizaZdravstvenihRazlogaToolStripMenuItem
            // 
            this.analizaZdravstvenihRazlogaToolStripMenuItem.Name = "analizaZdravstvenihRazlogaToolStripMenuItem";
            this.analizaZdravstvenihRazlogaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.analizaZdravstvenihRazlogaToolStripMenuItem.Text = "3. Analiza zdravstvenih razloga";
            this.analizaZdravstvenihRazlogaToolStripMenuItem.Click += new System.EventHandler(this.analizaZdravstvenihRazlogaToolStripMenuItem_Click);
            // 
            // analizaSelekcijskihRazlogaToolStripMenuItem
            // 
            this.analizaSelekcijskihRazlogaToolStripMenuItem.Name = "analizaSelekcijskihRazlogaToolStripMenuItem";
            this.analizaSelekcijskihRazlogaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.analizaSelekcijskihRazlogaToolStripMenuItem.Text = "4. Analiza selekcijskih razloga";
            this.analizaSelekcijskihRazlogaToolStripMenuItem.Click += new System.EventHandler(this.analizaSelekcijskihRazlogaToolStripMenuItem_Click);
            // 
            // aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem
            // 
            this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem.Name = "aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem";
            this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem.Text = "5. Analiza prinudnog klanja krmaca";
            this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem.Click += new System.EventHandler(this.aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem_Click);
            // 
            // analizaUginucaKmracaToolStripMenuItem
            // 
            this.analizaUginucaKmracaToolStripMenuItem.Name = "analizaUginucaKmracaToolStripMenuItem";
            this.analizaUginucaKmracaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.analizaUginucaKmracaToolStripMenuItem.Text = "6. Analiza uginuca krmaca";
            this.analizaUginucaKmracaToolStripMenuItem.Click += new System.EventHandler(this.analizaUginucaKmracaToolStripMenuItem_Click);
            // 
            // ukupnaIskljucenjaKrmacaToolStripMenuItem
            // 
            this.ukupnaIskljucenjaKrmacaToolStripMenuItem.Name = "ukupnaIskljucenjaKrmacaToolStripMenuItem";
            this.ukupnaIskljucenjaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(294, 22);
            this.ukupnaIskljucenjaKrmacaToolStripMenuItem.Text = "7. Ukupna iskljucenja krmaca";
            this.ukupnaIskljucenjaKrmacaToolStripMenuItem.Click += new System.EventHandler(this.ukupnaIskljucenjaKrmacaToolStripMenuItem_Click);
            // 
            // analizaIskljucenjaNerastovaToolStripMenuItem
            // 
            this.analizaIskljucenjaNerastovaToolStripMenuItem.Name = "analizaIskljucenjaNerastovaToolStripMenuItem";
            this.analizaIskljucenjaNerastovaToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.analizaIskljucenjaNerastovaToolStripMenuItem.Text = "4. Analiza iskljucenja nerastova";
            this.analizaIskljucenjaNerastovaToolStripMenuItem.Click += new System.EventHandler(this.analizaIskljucenjaNerastovaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(299, 22);
            this.toolStripMenuItem4.Text = "5. Analiza iskljucenja nazimica";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem
            // 
            this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem.Name = "analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem";
            this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem.Text = "6. Analiza iskljucenja grla iz testa - odabira";
            this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem.Click += new System.EventHandler(this.analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem_Click);
            // 
            // analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem
            // 
            this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem.Name = "analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem";
            this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem.Text = "7. Analiza iskljucenja grla iz registra prasadi";
            this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem.Click += new System.EventHandler(this.analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem_Click);
            // 
            // oSTALEFAZEToolStripMenuItem
            // 
            this.oSTALEFAZEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hormonalniTretmanToolStripMenuItem,
            this.detekcijaSuprasnostiToolStripMenuItem,
            this.prevodiKrmacaToolStripMenuItem,
            this.krmaceULaktacijiToolStripMenuItem,
            this.zdravstvenaZastitaKrmacaToolStripMenuItem,
            this.pobacajiKrmacaToolStripMenuItem,
            this.uginucaPoNerastovimaToolStripMenuItem,
            this.pregledSemenaToolStripMenuItem});
            this.oSTALEFAZEToolStripMenuItem.Name = "oSTALEFAZEToolStripMenuItem";
            this.oSTALEFAZEToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.oSTALEFAZEToolStripMenuItem.Text = "5.OSTALE FAZE";
            // 
            // hormonalniTretmanToolStripMenuItem
            // 
            this.hormonalniTretmanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaDavanjaHormonaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem5,
            this.kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem,
            this.kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem,
            this.analizaHormonalnogTretmanaNazimicaToolStripMenuItem,
            this.analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem,
            this.analizaHormonlanogTretmanaKrmacaToolStripMenuItem});
            this.hormonalniTretmanToolStripMenuItem.Name = "hormonalniTretmanToolStripMenuItem";
            this.hormonalniTretmanToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.hormonalniTretmanToolStripMenuItem.Text = "1.Hormonalni tretman";
            // 
            // evidencijaDavanjaHormonaToolStripMenuItem
            // 
            this.evidencijaDavanjaHormonaToolStripMenuItem.Name = "evidencijaDavanjaHormonaToolStripMenuItem";
            this.evidencijaDavanjaHormonaToolStripMenuItem.Size = new System.Drawing.Size(365, 22);
            this.evidencijaDavanjaHormonaToolStripMenuItem.Text = "1. Evidencija davanja hormona ";
            this.evidencijaDavanjaHormonaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaDavanjaHormonaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem5
            // 
            this.pregledPodatakaToolStripMenuItem5.Name = "pregledPodatakaToolStripMenuItem5";
            this.pregledPodatakaToolStripMenuItem5.Size = new System.Drawing.Size(365, 22);
            this.pregledPodatakaToolStripMenuItem5.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem5.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem5_Click);
            // 
            // kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem
            // 
            this.kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem.Name = "kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem";
            this.kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem.Size = new System.Drawing.Size(365, 22);
            this.kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem.Text = "3. Kalendar - Prvopraskinje za hormonalni tretman";
            // 
            // kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem
            // 
            this.kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem.Name = "kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem";
            this.kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem.Size = new System.Drawing.Size(365, 22);
            this.kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem.Text = "4. Kalendar - Anestricne krmace za hormonalni tretman";
            // 
            // analizaHormonalnogTretmanaNazimicaToolStripMenuItem
            // 
            this.analizaHormonalnogTretmanaNazimicaToolStripMenuItem.Name = "analizaHormonalnogTretmanaNazimicaToolStripMenuItem";
            this.analizaHormonalnogTretmanaNazimicaToolStripMenuItem.Size = new System.Drawing.Size(365, 22);
            this.analizaHormonalnogTretmanaNazimicaToolStripMenuItem.Text = "5. Analiza hormonalnog tretmana nazimica";
            // 
            // analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem
            // 
            this.analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem.Name = "analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem";
            this.analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem.Size = new System.Drawing.Size(365, 22);
            this.analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem.Text = "6. Analiza hormonalnog tretmana prvopraskinja";
            // 
            // analizaHormonlanogTretmanaKrmacaToolStripMenuItem
            // 
            this.analizaHormonlanogTretmanaKrmacaToolStripMenuItem.Name = "analizaHormonlanogTretmanaKrmacaToolStripMenuItem";
            this.analizaHormonlanogTretmanaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(365, 22);
            this.analizaHormonlanogTretmanaKrmacaToolStripMenuItem.Text = "7. Analiza hormonlanog tretmana krmaca";
            // 
            // detekcijaSuprasnostiToolStripMenuItem
            // 
            this.detekcijaSuprasnostiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaDetekcijeSuprasnostiToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem6,
            this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem,
            this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem});
            this.detekcijaSuprasnostiToolStripMenuItem.Name = "detekcijaSuprasnostiToolStripMenuItem";
            this.detekcijaSuprasnostiToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.detekcijaSuprasnostiToolStripMenuItem.Text = "2.Detekcija suprasnosti";
            // 
            // evidencijaDetekcijeSuprasnostiToolStripMenuItem
            // 
            this.evidencijaDetekcijeSuprasnostiToolStripMenuItem.Name = "evidencijaDetekcijeSuprasnostiToolStripMenuItem";
            this.evidencijaDetekcijeSuprasnostiToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.evidencijaDetekcijeSuprasnostiToolStripMenuItem.Text = "1. Evidencija detekcije suprasnosti";
            this.evidencijaDetekcijeSuprasnostiToolStripMenuItem.Click += new System.EventHandler(this.evidencijaDetekcijeSuprasnostiToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem6
            // 
            this.pregledPodatakaToolStripMenuItem6.Name = "pregledPodatakaToolStripMenuItem6";
            this.pregledPodatakaToolStripMenuItem6.Size = new System.Drawing.Size(283, 22);
            this.pregledPodatakaToolStripMenuItem6.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem6.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem6_Click);
            // 
            // kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem
            // 
            this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem.Name = "kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem";
            this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem.Text = "3. Kalendar -  Krmace za prvu detekciju";
            this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem.Click += new System.EventHandler(this.kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem_Click);
            // 
            // kalendarKrmaceZaDruguDetekcijuToolStripMenuItem
            // 
            this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem.Name = "kalendarKrmaceZaDruguDetekcijuToolStripMenuItem";
            this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem.Text = "4. Kalendar - Krmace za drugu detekciju";
            this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem.Click += new System.EventHandler(this.kalendarKrmaceZaDruguDetekcijuToolStripMenuItem_Click);
            // 
            // prevodiKrmacaToolStripMenuItem
            // 
            this.prevodiKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPrevodaKrmacaToolStripMenuItem,
            this.toolStripMenuItem8});
            this.prevodiKrmacaToolStripMenuItem.Name = "prevodiKrmacaToolStripMenuItem";
            this.prevodiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.prevodiKrmacaToolStripMenuItem.Text = "3.Prevodi krmaca";
            // 
            // evidencijaPrevodaKrmacaToolStripMenuItem
            // 
            this.evidencijaPrevodaKrmacaToolStripMenuItem.Name = "evidencijaPrevodaKrmacaToolStripMenuItem";
            this.evidencijaPrevodaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.evidencijaPrevodaKrmacaToolStripMenuItem.Text = "1. Evidencija prevoda krmaca";
            this.evidencijaPrevodaKrmacaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPrevodaKrmacaToolStripMenuItem_Click);
            // 
            // toolStripMenuItem8
            // 
            this.toolStripMenuItem8.Name = "toolStripMenuItem8";
            this.toolStripMenuItem8.Size = new System.Drawing.Size(227, 22);
            this.toolStripMenuItem8.Text = "2. Pregled podataka";
            this.toolStripMenuItem8.Click += new System.EventHandler(this.toolStripMenuItem8_Click);
            // 
            // krmaceULaktacijiToolStripMenuItem
            // 
            this.krmaceULaktacijiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem7});
            this.krmaceULaktacijiToolStripMenuItem.Name = "krmaceULaktacijiToolStripMenuItem";
            this.krmaceULaktacijiToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.krmaceULaktacijiToolStripMenuItem.Text = "4.Krmace u laktaciji";
            // 
            // evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem
            // 
            this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem.Name = "evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem";
            this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem.Size = new System.Drawing.Size(298, 22);
            this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem.Text = "1. Evidencija podataka o prasenju i laktaciji";
            this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem7
            // 
            this.pregledPodatakaToolStripMenuItem7.Name = "pregledPodatakaToolStripMenuItem7";
            this.pregledPodatakaToolStripMenuItem7.Size = new System.Drawing.Size(298, 22);
            this.pregledPodatakaToolStripMenuItem7.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem7.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem7_Click);
            // 
            // zdravstvenaZastitaKrmacaToolStripMenuItem
            // 
            this.zdravstvenaZastitaKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaObolenjaKrmacaToolStripMenuItem,
            this.pregeldPodatakaToolStripMenuItem,
            this.zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem,
            this.krmaceKojeSuBoloveleULaktacijiToolStripMenuItem,
            this.toolStripMenuItem9,
            this.analizeUticajaBolestiKrmacaToolStripMenuItem});
            this.zdravstvenaZastitaKrmacaToolStripMenuItem.Name = "zdravstvenaZastitaKrmacaToolStripMenuItem";
            this.zdravstvenaZastitaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.zdravstvenaZastitaKrmacaToolStripMenuItem.Text = "5.*Zdravstvena zastita krmaca";
            // 
            // evidencijaObolenjaKrmacaToolStripMenuItem
            // 
            this.evidencijaObolenjaKrmacaToolStripMenuItem.Name = "evidencijaObolenjaKrmacaToolStripMenuItem";
            this.evidencijaObolenjaKrmacaToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.evidencijaObolenjaKrmacaToolStripMenuItem.Text = "1. Evidencija obolenja krmaca";
            // 
            // pregeldPodatakaToolStripMenuItem
            // 
            this.pregeldPodatakaToolStripMenuItem.Name = "pregeldPodatakaToolStripMenuItem";
            this.pregeldPodatakaToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.pregeldPodatakaToolStripMenuItem.Text = "2. Pregeld podataka";
            // 
            // zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem
            // 
            this.zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem.Name = "zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem";
            this.zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem.Text = "3. * Zdravstveno stanje krmace u laktaciji";
            // 
            // krmaceKojeSuBoloveleULaktacijiToolStripMenuItem
            // 
            this.krmaceKojeSuBoloveleULaktacijiToolStripMenuItem.Name = "krmaceKojeSuBoloveleULaktacijiToolStripMenuItem";
            this.krmaceKojeSuBoloveleULaktacijiToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.krmaceKojeSuBoloveleULaktacijiToolStripMenuItem.Text = "4. * Krmace koje su bolovele u laktaciji";
            // 
            // toolStripMenuItem9
            // 
            this.toolStripMenuItem9.Name = "toolStripMenuItem9";
            this.toolStripMenuItem9.Size = new System.Drawing.Size(288, 22);
            this.toolStripMenuItem9.Text = "5. * Analiza bolesti krmaca van laktacije";
            // 
            // analizeUticajaBolestiKrmacaToolStripMenuItem
            // 
            this.analizeUticajaBolestiKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uticajParitetaNaZdravstvenoStanjeToolStripMenuItem,
            this.toolStripMenuItem10,
            this.uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem,
            this.uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem});
            this.analizeUticajaBolestiKrmacaToolStripMenuItem.Name = "analizeUticajaBolestiKrmacaToolStripMenuItem";
            this.analizeUticajaBolestiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.analizeUticajaBolestiKrmacaToolStripMenuItem.Text = "6. * Analize uticaja bolesti krmaca";
            // 
            // uticajParitetaNaZdravstvenoStanjeToolStripMenuItem
            // 
            this.uticajParitetaNaZdravstvenoStanjeToolStripMenuItem.Name = "uticajParitetaNaZdravstvenoStanjeToolStripMenuItem";
            this.uticajParitetaNaZdravstvenoStanjeToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.uticajParitetaNaZdravstvenoStanjeToolStripMenuItem.Text = "1.* Uticaj pariteta na zdravstveno stanje";
            // 
            // toolStripMenuItem10
            // 
            this.toolStripMenuItem10.Name = "toolStripMenuItem10";
            this.toolStripMenuItem10.Size = new System.Drawing.Size(324, 22);
            this.toolStripMenuItem10.Text = "2.* Uticaj zdravstvenog stanja na ulazak u estrus";
            // 
            // uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem
            // 
            this.uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem.Name = "uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem";
            this.uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem.Text = "3.* Uticaj obolenja krmaca na anestriju";
            // 
            // uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem
            // 
            this.uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem.Name = "uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem";
            this.uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem.Text = "4.* Uticaj obolenja krmaca na povadjanje";
            // 
            // pobacajiKrmacaToolStripMenuItem
            // 
            this.pobacajiKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPobacajaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem8});
            this.pobacajiKrmacaToolStripMenuItem.Name = "pobacajiKrmacaToolStripMenuItem";
            this.pobacajiKrmacaToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.pobacajiKrmacaToolStripMenuItem.Text = "6.Pobacaji krmaca";
            // 
            // evidencijaPobacajaToolStripMenuItem
            // 
            this.evidencijaPobacajaToolStripMenuItem.Name = "evidencijaPobacajaToolStripMenuItem";
            this.evidencijaPobacajaToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.evidencijaPobacajaToolStripMenuItem.Text = "1. Evidencija pobacaja";
            this.evidencijaPobacajaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPobacajaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem8
            // 
            this.pregledPodatakaToolStripMenuItem8.Name = "pregledPodatakaToolStripMenuItem8";
            this.pregledPodatakaToolStripMenuItem8.Size = new System.Drawing.Size(190, 22);
            this.pregledPodatakaToolStripMenuItem8.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem8.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem8_Click);
            // 
            // uginucaPoNerastovimaToolStripMenuItem
            // 
            this.uginucaPoNerastovimaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaUginucaPoNerastovimaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem9,
            this.analizaUginucaToolStripMenuItem});
            this.uginucaPoNerastovimaToolStripMenuItem.Name = "uginucaPoNerastovimaToolStripMenuItem";
            this.uginucaPoNerastovimaToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.uginucaPoNerastovimaToolStripMenuItem.Text = "7.Uginuca po nerastovima";
            // 
            // evidencijaUginucaPoNerastovimaToolStripMenuItem
            // 
            this.evidencijaUginucaPoNerastovimaToolStripMenuItem.Name = "evidencijaUginucaPoNerastovimaToolStripMenuItem";
            this.evidencijaUginucaPoNerastovimaToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.evidencijaUginucaPoNerastovimaToolStripMenuItem.Text = "1. Evidencija uginuca po nerastovima";
            this.evidencijaUginucaPoNerastovimaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaUginucaPoNerastovimaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem9
            // 
            this.pregledPodatakaToolStripMenuItem9.Name = "pregledPodatakaToolStripMenuItem9";
            this.pregledPodatakaToolStripMenuItem9.Size = new System.Drawing.Size(270, 22);
            this.pregledPodatakaToolStripMenuItem9.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem9.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem9_Click);
            // 
            // analizaUginucaToolStripMenuItem
            // 
            this.analizaUginucaToolStripMenuItem.Name = "analizaUginucaToolStripMenuItem";
            this.analizaUginucaToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.analizaUginucaToolStripMenuItem.Text = "3. Analiza uginuca";
            this.analizaUginucaToolStripMenuItem.Click += new System.EventHandler(this.analizaUginucaToolStripMenuItem_Click);
            // 
            // pregledSemenaToolStripMenuItem
            // 
            this.pregledSemenaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPregledaSemenaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem10});
            this.pregledSemenaToolStripMenuItem.Name = "pregledSemenaToolStripMenuItem";
            this.pregledSemenaToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.pregledSemenaToolStripMenuItem.Text = "8.Pregled semena";
            // 
            // evidencijaPregledaSemenaToolStripMenuItem
            // 
            this.evidencijaPregledaSemenaToolStripMenuItem.Name = "evidencijaPregledaSemenaToolStripMenuItem";
            this.evidencijaPregledaSemenaToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.evidencijaPregledaSemenaToolStripMenuItem.Text = "1. Evidencija pregleda semena";
            this.evidencijaPregledaSemenaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPregledaSemenaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem10
            // 
            this.pregledPodatakaToolStripMenuItem10.Name = "pregledPodatakaToolStripMenuItem10";
            this.pregledPodatakaToolStripMenuItem10.Size = new System.Drawing.Size(232, 22);
            this.pregledPodatakaToolStripMenuItem10.Text = "2. Pregled podataka";
            this.pregledPodatakaToolStripMenuItem10.Click += new System.EventHandler(this.pregledPodatakaToolStripMenuItem10_Click);
            // 
            // sELEKCIJAToolStripMenuItem
            // 
            this.sELEKCIJAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.pregledPodatakaToolStripMenuItem1,
            this.selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem,
            this.analizeRezultataPerformansTestaToolStripMenuItem,
            this.analizaPripustaTestiranihGrlaToolStripMenuItem});
            this.sELEKCIJAToolStripMenuItem.Name = "sELEKCIJAToolStripMenuItem";
            this.sELEKCIJAToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.sELEKCIJAToolStripMenuItem.Text = "6.SELEKCIJA";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem,
            this.evidencijaOdabirGrlaMerenjeToolStripMenuItem,
            this.prenosPodatakaIzOdabiraUTestToolStripMenuItem,
            this.prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem,
            this.evidencijaPerofrmansTestGrlaToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(306, 22);
            this.toolStripMenuItem2.Text = "1.Evidencija podataka o odabiru i testu grla";
            // 
            // pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem
            // 
            this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem.Name = "pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem";
            this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem.Text = "1. Prenos podataka iz registra prasadi u odabir";
            this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem.Click += new System.EventHandler(this.pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem_Click);
            // 
            // evidencijaOdabirGrlaMerenjeToolStripMenuItem
            // 
            this.evidencijaOdabirGrlaMerenjeToolStripMenuItem.Name = "evidencijaOdabirGrlaMerenjeToolStripMenuItem";
            this.evidencijaOdabirGrlaMerenjeToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.evidencijaOdabirGrlaMerenjeToolStripMenuItem.Text = "2. Evidencija odabir grla - merenje";
            this.evidencijaOdabirGrlaMerenjeToolStripMenuItem.Click += new System.EventHandler(this.evidencijaOdabirGrlaMerenjeToolStripMenuItem_Click);
            // 
            // prenosPodatakaIzOdabiraUTestToolStripMenuItem
            // 
            this.prenosPodatakaIzOdabiraUTestToolStripMenuItem.Name = "prenosPodatakaIzOdabiraUTestToolStripMenuItem";
            this.prenosPodatakaIzOdabiraUTestToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.prenosPodatakaIzOdabiraUTestToolStripMenuItem.Text = "3. Prenos podataka iz odabira u test";
            this.prenosPodatakaIzOdabiraUTestToolStripMenuItem.Click += new System.EventHandler(this.prenosPodatakaIzOdabiraUTestToolStripMenuItem_Click);
            // 
            // prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem
            // 
            this.prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem.Name = "prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem";
            this.prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem.Text = "4. Prenos podataka ULTRASAL aparat - PIGS baza";
            // 
            // evidencijaPerofrmansTestGrlaToolStripMenuItem
            // 
            this.evidencijaPerofrmansTestGrlaToolStripMenuItem.Name = "evidencijaPerofrmansTestGrlaToolStripMenuItem";
            this.evidencijaPerofrmansTestGrlaToolStripMenuItem.Size = new System.Drawing.Size(329, 22);
            this.evidencijaPerofrmansTestGrlaToolStripMenuItem.Text = "5. Evidencija performans test grla";
            this.evidencijaPerofrmansTestGrlaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPerofrmansTestGrlaToolStripMenuItem_Click);
            // 
            // pregledPodatakaToolStripMenuItem1
            // 
            this.pregledPodatakaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledOdabranihGrlaToolStripMenuItem,
            this.pregledTestiranihNazimicaToolStripMenuItem,
            this.pregledTestiranjaMladihNerastovaToolStripMenuItem,
            this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem});
            this.pregledPodatakaToolStripMenuItem1.Name = "pregledPodatakaToolStripMenuItem1";
            this.pregledPodatakaToolStripMenuItem1.Size = new System.Drawing.Size(306, 22);
            this.pregledPodatakaToolStripMenuItem1.Text = "2.Pregled podataka";
            // 
            // pregledOdabranihGrlaToolStripMenuItem
            // 
            this.pregledOdabranihGrlaToolStripMenuItem.Name = "pregledOdabranihGrlaToolStripMenuItem";
            this.pregledOdabranihGrlaToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.pregledOdabranihGrlaToolStripMenuItem.Text = "1. Pregled odabranih grla (ulaz u RC)";
            this.pregledOdabranihGrlaToolStripMenuItem.Click += new System.EventHandler(this.pregledOdabranihGrlaToolStripMenuItem_Click);
            // 
            // pregledTestiranihNazimicaToolStripMenuItem
            // 
            this.pregledTestiranihNazimicaToolStripMenuItem.Name = "pregledTestiranihNazimicaToolStripMenuItem";
            this.pregledTestiranihNazimicaToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.pregledTestiranihNazimicaToolStripMenuItem.Text = "2. Pregled testiranih nazimica";
            this.pregledTestiranihNazimicaToolStripMenuItem.Click += new System.EventHandler(this.pregledTestiranihNazimicaToolStripMenuItem_Click);
            // 
            // pregledTestiranjaMladihNerastovaToolStripMenuItem
            // 
            this.pregledTestiranjaMladihNerastovaToolStripMenuItem.Name = "pregledTestiranjaMladihNerastovaToolStripMenuItem";
            this.pregledTestiranjaMladihNerastovaToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.pregledTestiranjaMladihNerastovaToolStripMenuItem.Text = "3. Pregled testiranja mladih nerastova";
            this.pregledTestiranjaMladihNerastovaToolStripMenuItem.Click += new System.EventHandler(this.pregledTestiranjaMladihNerastovaToolStripMenuItem_Click);
            // 
            // pregledGrlaPoTetovirBrojevimaToolStripMenuItem
            // 
            this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem.Name = "pregledGrlaPoTetovirBrojevimaToolStripMenuItem";
            this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem.Text = "4. Pregled grla po tetovir brojevima";
            this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem.Click += new System.EventHandler(this.pregledGrlaPoTetovirBrojevimaToolStripMenuItem_Click);
            // 
            // selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem
            // 
            this.selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.si1PerformansTestGrlaUTestuToolStripMenuItem,
            this.si2TestKrmacaNaPlodnostToolStripMenuItem,
            this.si2TestNerastovaNaPlodnostToolStripMenuItem,
            this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem,
            this.testSaLinijeKlanjaToolStripMenuItem});
            this.selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem.Name = "selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem";
            this.selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem.Size = new System.Drawing.Size(306, 22);
            this.selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem.Text = "3.Selekcijski indeksi(Si1,Si2, Si3, Linija klanja)";
            // 
            // si1PerformansTestGrlaUTestuToolStripMenuItem
            // 
            this.si1PerformansTestGrlaUTestuToolStripMenuItem.Name = "si1PerformansTestGrlaUTestuToolStripMenuItem";
            this.si1PerformansTestGrlaUTestuToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.si1PerformansTestGrlaUTestuToolStripMenuItem.Text = "1. Si1 - Performans test grla u testu";
            this.si1PerformansTestGrlaUTestuToolStripMenuItem.Click += new System.EventHandler(this.si1PerformansTestGrlaUTestuToolStripMenuItem_Click);
            // 
            // si2TestKrmacaNaPlodnostToolStripMenuItem
            // 
            this.si2TestKrmacaNaPlodnostToolStripMenuItem.Name = "si2TestKrmacaNaPlodnostToolStripMenuItem";
            this.si2TestKrmacaNaPlodnostToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.si2TestKrmacaNaPlodnostToolStripMenuItem.Text = "2. Si2 - Test krmaca na plodnost";
            this.si2TestKrmacaNaPlodnostToolStripMenuItem.Click += new System.EventHandler(this.si2TestKrmacaNaPlodnostToolStripMenuItem_Click);
            // 
            // si2TestNerastovaNaPlodnostToolStripMenuItem
            // 
            this.si2TestNerastovaNaPlodnostToolStripMenuItem.Name = "si2TestNerastovaNaPlodnostToolStripMenuItem";
            this.si2TestNerastovaNaPlodnostToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.si2TestNerastovaNaPlodnostToolStripMenuItem.Text = "3. Si2 - Test nerastova na plodnost";
            this.si2TestNerastovaNaPlodnostToolStripMenuItem.Click += new System.EventHandler(this.si2TestNerastovaNaPlodnostToolStripMenuItem_Click);
            // 
            // si3ProgeniTestNerastovaIKrmacaToolStripMenuItem
            // 
            this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem.Name = "si3ProgeniTestNerastovaIKrmacaToolStripMenuItem";
            this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem.Text = "4. Si3 - Progeni test nerastova i krmaca";
            this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem.Click += new System.EventHandler(this.si3ProgeniTestNerastovaIKrmacaToolStripMenuItem_Click);
            // 
            // testSaLinijeKlanjaToolStripMenuItem
            // 
            this.testSaLinijeKlanjaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem,
            this.pregledPodatakaToolStripMenuItem11,
            this.testKrmacaSaLinijeKlanjaToolStripMenuItem,
            this.testNerastovaSaLinijeKlanjaToolStripMenuItem,
            this.testPoRasamaSaLinijeKlanjaToolStripMenuItem});
            this.testSaLinijeKlanjaToolStripMenuItem.Name = "testSaLinijeKlanjaToolStripMenuItem";
            this.testSaLinijeKlanjaToolStripMenuItem.Size = new System.Drawing.Size(277, 22);
            this.testSaLinijeKlanjaToolStripMenuItem.Text = "5. Test sa linije klanja";
            // 
            // evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem
            // 
            this.evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem.Name = "evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem";
            this.evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem.Text = "1. Evidencija podataka sa linije klanja";
            // 
            // pregledPodatakaToolStripMenuItem11
            // 
            this.pregledPodatakaToolStripMenuItem11.Name = "pregledPodatakaToolStripMenuItem11";
            this.pregledPodatakaToolStripMenuItem11.Size = new System.Drawing.Size(267, 22);
            this.pregledPodatakaToolStripMenuItem11.Text = "2. Pregled podataka";
            // 
            // testKrmacaSaLinijeKlanjaToolStripMenuItem
            // 
            this.testKrmacaSaLinijeKlanjaToolStripMenuItem.Name = "testKrmacaSaLinijeKlanjaToolStripMenuItem";
            this.testKrmacaSaLinijeKlanjaToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.testKrmacaSaLinijeKlanjaToolStripMenuItem.Text = "3. Test krmaca sa linije klanja";
            // 
            // testNerastovaSaLinijeKlanjaToolStripMenuItem
            // 
            this.testNerastovaSaLinijeKlanjaToolStripMenuItem.Name = "testNerastovaSaLinijeKlanjaToolStripMenuItem";
            this.testNerastovaSaLinijeKlanjaToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.testNerastovaSaLinijeKlanjaToolStripMenuItem.Text = "4. Test nerastova sa linije klanja";
            // 
            // testPoRasamaSaLinijeKlanjaToolStripMenuItem
            // 
            this.testPoRasamaSaLinijeKlanjaToolStripMenuItem.Name = "testPoRasamaSaLinijeKlanjaToolStripMenuItem";
            this.testPoRasamaSaLinijeKlanjaToolStripMenuItem.Size = new System.Drawing.Size(267, 22);
            this.testPoRasamaSaLinijeKlanjaToolStripMenuItem.Text = "5. Test po rasama sa linije klanja";
            // 
            // analizeRezultataPerformansTestaToolStripMenuItem
            // 
            this.analizeRezultataPerformansTestaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizaPerformansTestaPoRasamaToolStripMenuItem,
            this.rangListaOcevaToolStripMenuItem,
            this.analizaTestaPoOcuToolStripMenuItem});
            this.analizeRezultataPerformansTestaToolStripMenuItem.Name = "analizeRezultataPerformansTestaToolStripMenuItem";
            this.analizeRezultataPerformansTestaToolStripMenuItem.Size = new System.Drawing.Size(306, 22);
            this.analizeRezultataPerformansTestaToolStripMenuItem.Text = "4.Analize rezultata performans testa";
            // 
            // analizaPerformansTestaPoRasamaToolStripMenuItem
            // 
            this.analizaPerformansTestaPoRasamaToolStripMenuItem.Name = "analizaPerformansTestaPoRasamaToolStripMenuItem";
            this.analizaPerformansTestaPoRasamaToolStripMenuItem.Size = new System.Drawing.Size(274, 22);
            this.analizaPerformansTestaPoRasamaToolStripMenuItem.Text = "1. Analiza performans testa po rasama";
            this.analizaPerformansTestaPoRasamaToolStripMenuItem.Click += new System.EventHandler(this.analizaPerformansTestaPoRasamaToolStripMenuItem_Click);
            // 
            // rangListaOcevaToolStripMenuItem
            // 
            this.rangListaOcevaToolStripMenuItem.Name = "rangListaOcevaToolStripMenuItem";
            this.rangListaOcevaToolStripMenuItem.Size = new System.Drawing.Size(274, 22);
            this.rangListaOcevaToolStripMenuItem.Text = "2. Rang lista oceva";
            this.rangListaOcevaToolStripMenuItem.Click += new System.EventHandler(this.rangListaOcevaToolStripMenuItem_Click);
            // 
            // analizaTestaPoOcuToolStripMenuItem
            // 
            this.analizaTestaPoOcuToolStripMenuItem.Name = "analizaTestaPoOcuToolStripMenuItem";
            this.analizaTestaPoOcuToolStripMenuItem.Size = new System.Drawing.Size(274, 22);
            this.analizaTestaPoOcuToolStripMenuItem.Text = "3. Analiza testa po ocu";
            this.analizaTestaPoOcuToolStripMenuItem.Click += new System.EventHandler(this.analizaTestaPoOcuToolStripMenuItem_Click);
            // 
            // analizaPripustaTestiranihGrlaToolStripMenuItem
            // 
            this.analizaPripustaTestiranihGrlaToolStripMenuItem.Name = "analizaPripustaTestiranihGrlaToolStripMenuItem";
            this.analizaPripustaTestiranihGrlaToolStripMenuItem.Size = new System.Drawing.Size(306, 22);
            this.analizaPripustaTestiranihGrlaToolStripMenuItem.Text = "5.Analiza pripusta testiranih grla";
            this.analizaPripustaTestiranihGrlaToolStripMenuItem.Click += new System.EventHandler(this.analizaPripustaTestiranihGrlaToolStripMenuItem_Click);
            // 
            // kARTOTEKAToolStripMenuItem
            // 
            this.kARTOTEKAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.maticniIProizvodniKartonKrmaceToolStripMenuItem,
            this.pedigreporekloGrlaToolStripMenuItem,
            this.krmaceToolStripMenuItem,
            this.nerastoviToolStripMenuItem,
            this.nazimiceToolStripMenuItem,
            this.odabirITestToolStripMenuItem,
            this.registarPrasadiToolStripMenuItem,
            this.maticniBrojIFazaGrlaToolStripMenuItem,
            this.sifrarnikToolStripMenuItem,
            this.aToolStripMenuItem,
            this.bLogickaKontrolaPodatakaToolStripMenuItem});
            this.kARTOTEKAToolStripMenuItem.Name = "kARTOTEKAToolStripMenuItem";
            this.kARTOTEKAToolStripMenuItem.Size = new System.Drawing.Size(89, 20);
            this.kARTOTEKAToolStripMenuItem.Text = "7.KARTOTEKA";
            // 
            // maticniIProizvodniKartonKrmaceToolStripMenuItem
            // 
            this.maticniIProizvodniKartonKrmaceToolStripMenuItem.Name = "maticniIProizvodniKartonKrmaceToolStripMenuItem";
            this.maticniIProizvodniKartonKrmaceToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.maticniIProizvodniKartonKrmaceToolStripMenuItem.Text = "1. Maticni i proizvodni karton krmace";
            this.maticniIProizvodniKartonKrmaceToolStripMenuItem.Click += new System.EventHandler(this.maticniIProizvodniKartonKrmaceToolStripMenuItem_Click);
            // 
            // pedigreporekloGrlaToolStripMenuItem
            // 
            this.pedigreporekloGrlaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPoreklaGrlaGNToolStripMenuItem,
            this.stampaPoreklaGrlaGNToolStripMenuItem,
            this.eVidencijaPedigreaGrlaRasaToolStripMenuItem,
            this.stampaPedigreaGrlaRasaToolStripMenuItem,
            this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem,
            this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem});
            this.pedigreporekloGrlaToolStripMenuItem.Name = "pedigreporekloGrlaToolStripMenuItem";
            this.pedigreporekloGrlaToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.pedigreporekloGrlaToolStripMenuItem.Text = "2. Pedigre/poreklo grla";
            // 
            // evidencijaPoreklaGrlaGNToolStripMenuItem
            // 
            this.evidencijaPoreklaGrlaGNToolStripMenuItem.Name = "evidencijaPoreklaGrlaGNToolStripMenuItem";
            this.evidencijaPoreklaGrlaGNToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.evidencijaPoreklaGrlaGNToolStripMenuItem.Text = "1. Evidencija porekla grla - GN";
            this.evidencijaPoreklaGrlaGNToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPoreklaGrlaGNToolStripMenuItem_Click);
            // 
            // stampaPoreklaGrlaGNToolStripMenuItem
            // 
            this.stampaPoreklaGrlaGNToolStripMenuItem.Name = "stampaPoreklaGrlaGNToolStripMenuItem";
            this.stampaPoreklaGrlaGNToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.stampaPoreklaGrlaGNToolStripMenuItem.Text = "2. Stampa porekla grla - GN";
            this.stampaPoreklaGrlaGNToolStripMenuItem.Click += new System.EventHandler(this.stampaPoreklaGrlaGNToolStripMenuItem_Click);
            // 
            // eVidencijaPedigreaGrlaRasaToolStripMenuItem
            // 
            this.eVidencijaPedigreaGrlaRasaToolStripMenuItem.Name = "eVidencijaPedigreaGrlaRasaToolStripMenuItem";
            this.eVidencijaPedigreaGrlaRasaToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.eVidencijaPedigreaGrlaRasaToolStripMenuItem.Text = "3. EVidencija pedigrea grla - rasa";
            this.eVidencijaPedigreaGrlaRasaToolStripMenuItem.Click += new System.EventHandler(this.eVidencijaPedigreaGrlaRasaToolStripMenuItem_Click);
            // 
            // stampaPedigreaGrlaRasaToolStripMenuItem
            // 
            this.stampaPedigreaGrlaRasaToolStripMenuItem.Name = "stampaPedigreaGrlaRasaToolStripMenuItem";
            this.stampaPedigreaGrlaRasaToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.stampaPedigreaGrlaRasaToolStripMenuItem.Text = "4. Stampa pedigrea grla - rasa";
            this.stampaPedigreaGrlaRasaToolStripMenuItem.Click += new System.EventHandler(this.stampaPedigreaGrlaRasaToolStripMenuItem_Click);
            // 
            // grlaBezOtvorenogPedigreaporeklaToolStripMenuItem
            // 
            this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem.Name = "grlaBezOtvorenogPedigreaporeklaToolStripMenuItem";
            this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem.Text = "5. Grla bez otvorenog pedigrea/porekla";
            this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem.Click += new System.EventHandler(this.grlaBezOtvorenogPedigreaporeklaToolStripMenuItem_Click);
            // 
            // grlaBezPotpunogPedigreaporeklaToolStripMenuItem
            // 
            this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem.Name = "grlaBezPotpunogPedigreaporeklaToolStripMenuItem";
            this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem.Size = new System.Drawing.Size(279, 22);
            this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem.Text = "6. Grla bez potpunog pedigrea/porekla";
            this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem.Click += new System.EventHandler(this.grlaBezPotpunogPedigreaporeklaToolStripMenuItem_Click);
            // 
            // krmaceToolStripMenuItem
            // 
            this.krmaceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.katalogKrmacaToolStripMenuItem,
            this.pregledAktivnihKrmacaNaDanToolStripMenuItem,
            this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem,
            this.pregledKrmacaPoTetovirimaToolStripMenuItem,
            this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem});
            this.krmaceToolStripMenuItem.Name = "krmaceToolStripMenuItem";
            this.krmaceToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.krmaceToolStripMenuItem.Text = "3. Krmace";
            // 
            // katalogKrmacaToolStripMenuItem
            // 
            this.katalogKrmacaToolStripMenuItem.Name = "katalogKrmacaToolStripMenuItem";
            this.katalogKrmacaToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.katalogKrmacaToolStripMenuItem.Text = "1. Katalog krmaca";
            this.katalogKrmacaToolStripMenuItem.Click += new System.EventHandler(this.katalogKrmacaToolStripMenuItem_Click);
            // 
            // pregledAktivnihKrmacaNaDanToolStripMenuItem
            // 
            this.pregledAktivnihKrmacaNaDanToolStripMenuItem.Name = "pregledAktivnihKrmacaNaDanToolStripMenuItem";
            this.pregledAktivnihKrmacaNaDanToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.pregledAktivnihKrmacaNaDanToolStripMenuItem.Text = "2. Pregled aktivnih krmaca na dan";
            this.pregledAktivnihKrmacaNaDanToolStripMenuItem.Click += new System.EventHandler(this.pregledAktivnihKrmacaNaDanToolStripMenuItem_Click);
            // 
            // hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem
            // 
            this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem.Name = "hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem";
            this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem.Text = "3. HB, RB brojevi markice i grupe krmaca";
            this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem.Click += new System.EventHandler(this.hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem_Click);
            // 
            // pregledKrmacaPoTetovirimaToolStripMenuItem
            // 
            this.pregledKrmacaPoTetovirimaToolStripMenuItem.Name = "pregledKrmacaPoTetovirimaToolStripMenuItem";
            this.pregledKrmacaPoTetovirimaToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.pregledKrmacaPoTetovirimaToolStripMenuItem.Text = "4. Pregled krmaca po tetovirima";
            this.pregledKrmacaPoTetovirimaToolStripMenuItem.Click += new System.EventHandler(this.pregledKrmacaPoTetovirimaToolStripMenuItem_Click);
            // 
            // pregledIAnalizaPotomstvaPoMajciToolStripMenuItem
            // 
            this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem.Name = "pregledIAnalizaPotomstvaPoMajciToolStripMenuItem";
            this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem.Size = new System.Drawing.Size(288, 22);
            this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem.Text = "5. Pregled i analiza potomstva po majci";
            this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem.Click += new System.EventHandler(this.pregledIAnalizaPotomstvaPoMajciToolStripMenuItem_Click);
            // 
            // nerastoviToolStripMenuItem
            // 
            this.nerastoviToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.katalogNerastovaToolStripMenuItem,
            this.maticniIProizvodniKartonNerastaToolStripMenuItem,
            this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem,
            this.toolStripMenuItem11});
            this.nerastoviToolStripMenuItem.Name = "nerastoviToolStripMenuItem";
            this.nerastoviToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.nerastoviToolStripMenuItem.Text = "4. Nerastovi";
            // 
            // katalogNerastovaToolStripMenuItem
            // 
            this.katalogNerastovaToolStripMenuItem.Name = "katalogNerastovaToolStripMenuItem";
            this.katalogNerastovaToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.katalogNerastovaToolStripMenuItem.Text = "1. Katalog nerastova";
            this.katalogNerastovaToolStripMenuItem.Click += new System.EventHandler(this.katalogNerastovaToolStripMenuItem_Click);
            // 
            // maticniIProizvodniKartonNerastaToolStripMenuItem
            // 
            this.maticniIProizvodniKartonNerastaToolStripMenuItem.Name = "maticniIProizvodniKartonNerastaToolStripMenuItem";
            this.maticniIProizvodniKartonNerastaToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.maticniIProizvodniKartonNerastaToolStripMenuItem.Text = "2. Maticni i proizvodni karton nerasta";
            this.maticniIProizvodniKartonNerastaToolStripMenuItem.Click += new System.EventHandler(this.maticniIProizvodniKartonNerastaToolStripMenuItem_Click);
            // 
            // pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem
            // 
            this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem.Name = "pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem";
            this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem.Size = new System.Drawing.Size(271, 22);
            this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem.Text = "3. Pregled i analiza potomstva po ocu";
            this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem.Click += new System.EventHandler(this.pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem_Click);
            // 
            // toolStripMenuItem11
            // 
            this.toolStripMenuItem11.Name = "toolStripMenuItem11";
            this.toolStripMenuItem11.Size = new System.Drawing.Size(271, 22);
            this.toolStripMenuItem11.Text = "4. Nerastovi na stanju na dan";
            this.toolStripMenuItem11.Click += new System.EventHandler(this.toolStripMenuItem11_Click);
            // 
            // nazimiceToolStripMenuItem
            // 
            this.nazimiceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.katalogOsemenjenihNazimicaToolStripMenuItem,
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem,
            this.nazimiceNaStanjuNaDanToolStripMenuItem,
            this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem});
            this.nazimiceToolStripMenuItem.Name = "nazimiceToolStripMenuItem";
            this.nazimiceToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.nazimiceToolStripMenuItem.Text = "5. Nazimice";
            // 
            // katalogOsemenjenihNazimicaToolStripMenuItem
            // 
            this.katalogOsemenjenihNazimicaToolStripMenuItem.Name = "katalogOsemenjenihNazimicaToolStripMenuItem";
            this.katalogOsemenjenihNazimicaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.katalogOsemenjenihNazimicaToolStripMenuItem.Text = "1. Katalog osemenjenih nazimica";
            this.katalogOsemenjenihNazimicaToolStripMenuItem.Click += new System.EventHandler(this.katalogOsemenjenihNazimicaToolStripMenuItem_Click);
            // 
            // pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem
            // 
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem,
            this.pregledNazimicaMaticniPodaciToolStripMenuItem,
            this.pripustiNazimicaToolStripMenuItem,
            this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem,
            this.opraseneNazimiceToolStripMenuItem,
            this.zaluceneNazimiceToolStripMenuItem});
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem.Name = "pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem";
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem.Text = "2. Pregled i analize nazimica po fazama";
            this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem.Click += new System.EventHandler(this.pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem_Click);
            // 
            // pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem
            // 
            this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem.Name = "pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem";
            this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem.Text = "1. Pregled nazimica - Grla za osiguranje";
            this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem.Click += new System.EventHandler(this.pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem_Click);
            // 
            // pregledNazimicaMaticniPodaciToolStripMenuItem
            // 
            this.pregledNazimicaMaticniPodaciToolStripMenuItem.Name = "pregledNazimicaMaticniPodaciToolStripMenuItem";
            this.pregledNazimicaMaticniPodaciToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.pregledNazimicaMaticniPodaciToolStripMenuItem.Text = "2. Pregled nazimica - Maticni podaci";
            this.pregledNazimicaMaticniPodaciToolStripMenuItem.Click += new System.EventHandler(this.pregledNazimicaMaticniPodaciToolStripMenuItem_Click);
            // 
            // pripustiNazimicaToolStripMenuItem
            // 
            this.pripustiNazimicaToolStripMenuItem.Name = "pripustiNazimicaToolStripMenuItem";
            this.pripustiNazimicaToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.pripustiNazimicaToolStripMenuItem.Text = "3. Pripusti nazimica";
            this.pripustiNazimicaToolStripMenuItem.Click += new System.EventHandler(this.pripustiNazimicaToolStripMenuItem_Click);
            // 
            // pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem
            // 
            this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem.Name = "pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem";
            this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem.Text = "4. Pripusti nazimica - ocekivani datum prasenja";
            this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem.Click += new System.EventHandler(this.pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem_Click);
            // 
            // opraseneNazimiceToolStripMenuItem
            // 
            this.opraseneNazimiceToolStripMenuItem.Name = "opraseneNazimiceToolStripMenuItem";
            this.opraseneNazimiceToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.opraseneNazimiceToolStripMenuItem.Text = "5. Oprasene nazimice";
            this.opraseneNazimiceToolStripMenuItem.Click += new System.EventHandler(this.opraseneNazimiceToolStripMenuItem_Click);
            // 
            // zaluceneNazimiceToolStripMenuItem
            // 
            this.zaluceneNazimiceToolStripMenuItem.Name = "zaluceneNazimiceToolStripMenuItem";
            this.zaluceneNazimiceToolStripMenuItem.Size = new System.Drawing.Size(322, 22);
            this.zaluceneNazimiceToolStripMenuItem.Text = "6. Zalucene nazimice";
            this.zaluceneNazimiceToolStripMenuItem.Click += new System.EventHandler(this.zaluceneNazimiceToolStripMenuItem_Click);
            // 
            // nazimiceNaStanjuNaDanToolStripMenuItem
            // 
            this.nazimiceNaStanjuNaDanToolStripMenuItem.Name = "nazimiceNaStanjuNaDanToolStripMenuItem";
            this.nazimiceNaStanjuNaDanToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.nazimiceNaStanjuNaDanToolStripMenuItem.Text = "3. Nazimice na stanju na dan";
            this.nazimiceNaStanjuNaDanToolStripMenuItem.Click += new System.EventHandler(this.nazimiceNaStanjuNaDanToolStripMenuItem_Click);
            // 
            // pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem
            // 
            this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem.Name = "pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem";
            this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem.Text = "4. Pregled osemenjenih nazimica po tetovirima";
            this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem.Click += new System.EventHandler(this.pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem_Click);
            // 
            // odabirITestToolStripMenuItem
            // 
            this.odabirITestToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grlaUTestuOdabiruNaDanToolStripMenuItem,
            this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem});
            this.odabirITestToolStripMenuItem.Name = "odabirITestToolStripMenuItem";
            this.odabirITestToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.odabirITestToolStripMenuItem.Text = "6. Odabir i test";
            // 
            // grlaUTestuOdabiruNaDanToolStripMenuItem
            // 
            this.grlaUTestuOdabiruNaDanToolStripMenuItem.Name = "grlaUTestuOdabiruNaDanToolStripMenuItem";
            this.grlaUTestuOdabiruNaDanToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.grlaUTestuOdabiruNaDanToolStripMenuItem.Text = "1. Grla u testu - odabiru na dan";
            this.grlaUTestuOdabiruNaDanToolStripMenuItem.Click += new System.EventHandler(this.grlaUTestuOdabiruNaDanToolStripMenuItem_Click);
            // 
            // pregledeTestaOdabiraPoTetovirimaToolStripMenuItem
            // 
            this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem.Name = "pregledeTestaOdabiraPoTetovirimaToolStripMenuItem";
            this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem.Text = "2. Preglede testa- odabira po tetovirima";
            this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem.Click += new System.EventHandler(this.pregledeTestaOdabiraPoTetovirimaToolStripMenuItem_Click);
            // 
            // registarPrasadiToolStripMenuItem
            // 
            this.registarPrasadiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aPregeldPodatakaZenskaPrasadToolStripMenuItem,
            this.bPregledPodatakaMuskaPrasadToolStripMenuItem,
            this.cPregedPodatakaObaPolaToolStripMenuItem,
            this.dopunaRegistraPrasenjaToolStripMenuItem,
            this.promenaPolaPrasadimaToolStripMenuItem,
            this.prasadNaStanjuNaDanToolStripMenuItem,
            this.pregledTetoviranePrasadiZaTovToolStripMenuItem,
            this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem,
            this.dopunaBrojaSisaKodPrasadiToolStripMenuItem});
            this.registarPrasadiToolStripMenuItem.Name = "registarPrasadiToolStripMenuItem";
            this.registarPrasadiToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.registarPrasadiToolStripMenuItem.Text = "7. Registar prasadi";
            // 
            // aPregeldPodatakaZenskaPrasadToolStripMenuItem
            // 
            this.aPregeldPodatakaZenskaPrasadToolStripMenuItem.Name = "aPregeldPodatakaZenskaPrasadToolStripMenuItem";
            this.aPregeldPodatakaZenskaPrasadToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.aPregeldPodatakaZenskaPrasadToolStripMenuItem.Text = "1a. Pregeld podataka - zenska prasad";
            this.aPregeldPodatakaZenskaPrasadToolStripMenuItem.Click += new System.EventHandler(this.aPregeldPodatakaZenskaPrasadToolStripMenuItem_Click);
            // 
            // bPregledPodatakaMuskaPrasadToolStripMenuItem
            // 
            this.bPregledPodatakaMuskaPrasadToolStripMenuItem.Name = "bPregledPodatakaMuskaPrasadToolStripMenuItem";
            this.bPregledPodatakaMuskaPrasadToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.bPregledPodatakaMuskaPrasadToolStripMenuItem.Text = "1b. Pregled podataka - muska prasad";
            this.bPregledPodatakaMuskaPrasadToolStripMenuItem.Click += new System.EventHandler(this.bPregledPodatakaMuskaPrasadToolStripMenuItem_Click);
            // 
            // cPregedPodatakaObaPolaToolStripMenuItem
            // 
            this.cPregedPodatakaObaPolaToolStripMenuItem.Name = "cPregedPodatakaObaPolaToolStripMenuItem";
            this.cPregedPodatakaObaPolaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.cPregedPodatakaObaPolaToolStripMenuItem.Text = "1c. Preged podataka -  oba pola";
            this.cPregedPodatakaObaPolaToolStripMenuItem.Click += new System.EventHandler(this.cPregedPodatakaObaPolaToolStripMenuItem_Click);
            // 
            // dopunaRegistraPrasenjaToolStripMenuItem
            // 
            this.dopunaRegistraPrasenjaToolStripMenuItem.Name = "dopunaRegistraPrasenjaToolStripMenuItem";
            this.dopunaRegistraPrasenjaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.dopunaRegistraPrasenjaToolStripMenuItem.Text = "2. Dopuna registra prasenja";
            this.dopunaRegistraPrasenjaToolStripMenuItem.Click += new System.EventHandler(this.dopunaRegistraPrasenjaToolStripMenuItem_Click);
            // 
            // promenaPolaPrasadimaToolStripMenuItem
            // 
            this.promenaPolaPrasadimaToolStripMenuItem.Name = "promenaPolaPrasadimaToolStripMenuItem";
            this.promenaPolaPrasadimaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.promenaPolaPrasadimaToolStripMenuItem.Text = "3. Promena pola prasadima";
            this.promenaPolaPrasadimaToolStripMenuItem.Click += new System.EventHandler(this.promenaPolaPrasadimaToolStripMenuItem_Click);
            // 
            // prasadNaStanjuNaDanToolStripMenuItem
            // 
            this.prasadNaStanjuNaDanToolStripMenuItem.Name = "prasadNaStanjuNaDanToolStripMenuItem";
            this.prasadNaStanjuNaDanToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.prasadNaStanjuNaDanToolStripMenuItem.Text = "4. Prasad na stanju na dan";
            this.prasadNaStanjuNaDanToolStripMenuItem.Click += new System.EventHandler(this.prasadNaStanjuNaDanToolStripMenuItem_Click);
            // 
            // pregledTetoviranePrasadiZaTovToolStripMenuItem
            // 
            this.pregledTetoviranePrasadiZaTovToolStripMenuItem.Name = "pregledTetoviranePrasadiZaTovToolStripMenuItem";
            this.pregledTetoviranePrasadiZaTovToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.pregledTetoviranePrasadiZaTovToolStripMenuItem.Text = "5. Pregled tetovirane prasadi za tov";
            this.pregledTetoviranePrasadiZaTovToolStripMenuItem.Click += new System.EventHandler(this.pregledTetoviranePrasadiZaTovToolStripMenuItem_Click);
            // 
            // pregledRegistraPrasadiPoTetovirimaToolStripMenuItem
            // 
            this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem.Name = "pregledRegistraPrasadiPoTetovirimaToolStripMenuItem";
            this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem.Text = "6. Pregled registra prasadi po tetovirima";
            this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem.Click += new System.EventHandler(this.pregledRegistraPrasadiPoTetovirimaToolStripMenuItem_Click);
            // 
            // dopunaBrojaSisaKodPrasadiToolStripMenuItem
            // 
            this.dopunaBrojaSisaKodPrasadiToolStripMenuItem.Name = "dopunaBrojaSisaKodPrasadiToolStripMenuItem";
            this.dopunaBrojaSisaKodPrasadiToolStripMenuItem.Size = new System.Drawing.Size(283, 22);
            this.dopunaBrojaSisaKodPrasadiToolStripMenuItem.Text = "7. Dopuna broja sisa kod prasadi";
            this.dopunaBrojaSisaKodPrasadiToolStripMenuItem.Click += new System.EventHandler(this.dopunaBrojaSisaKodPrasadiToolStripMenuItem_Click);
            // 
            // maticniBrojIFazaGrlaToolStripMenuItem
            // 
            this.maticniBrojIFazaGrlaToolStripMenuItem.Name = "maticniBrojIFazaGrlaToolStripMenuItem";
            this.maticniBrojIFazaGrlaToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.maticniBrojIFazaGrlaToolStripMenuItem.Text = "8. Maticni broj i faza grla";
            this.maticniBrojIFazaGrlaToolStripMenuItem.Click += new System.EventHandler(this.maticniBrojIFazaGrlaToolStripMenuItem_Click);
            // 
            // sifrarnikToolStripMenuItem
            // 
            this.sifrarnikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.katalogFarmiToolStripMenuItem,
            this.sifrarnikRasaToolStripMenuItem,
            this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem,
            this.razloziIskljucenjaNerastovaToolStripMenuItem,
            this.sifrarnikBolestiToolStripMenuItem,
            this.razloziPrinudnogZalucenjaToolStripMenuItem,
            this.razloziPonovnogPripustaToolStripMenuItem,
            this.razloziRokadePrasadiToolStripMenuItem,
            this.razloziUginucaPoNerastovimaToolStripMenuItem,
            this.aInterneSifreFarmeToolStripMenuItem});
            this.sifrarnikToolStripMenuItem.Name = "sifrarnikToolStripMenuItem";
            this.sifrarnikToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.sifrarnikToolStripMenuItem.Text = "9. Sifrarnik";
            // 
            // katalogFarmiToolStripMenuItem
            // 
            this.katalogFarmiToolStripMenuItem.Name = "katalogFarmiToolStripMenuItem";
            this.katalogFarmiToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.katalogFarmiToolStripMenuItem.Text = "1. Katalog farmi";
            this.katalogFarmiToolStripMenuItem.Click += new System.EventHandler(this.katalogFarmiToolStripMenuItem_Click);
            // 
            // sifrarnikRasaToolStripMenuItem
            // 
            this.sifrarnikRasaToolStripMenuItem.Name = "sifrarnikRasaToolStripMenuItem";
            this.sifrarnikRasaToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.sifrarnikRasaToolStripMenuItem.Text = "2. Sifrarnik rasa";
            this.sifrarnikRasaToolStripMenuItem.Click += new System.EventHandler(this.sifrarnikRasaToolStripMenuItem_Click);
            // 
            // razloziIskljucenjaKrmacaINazimicaToolStripMenuItem
            // 
            this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem.Name = "razloziIskljucenjaKrmacaINazimicaToolStripMenuItem";
            this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem.Text = "3. Razlozi iskljucenja krmaca i nazimica";
            this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem.Click += new System.EventHandler(this.razloziIskljucenjaKrmacaINazimicaToolStripMenuItem_Click);
            // 
            // razloziIskljucenjaNerastovaToolStripMenuItem
            // 
            this.razloziIskljucenjaNerastovaToolStripMenuItem.Name = "razloziIskljucenjaNerastovaToolStripMenuItem";
            this.razloziIskljucenjaNerastovaToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.razloziIskljucenjaNerastovaToolStripMenuItem.Text = "4. Razlozi iskljucenja nerastova";
            this.razloziIskljucenjaNerastovaToolStripMenuItem.Click += new System.EventHandler(this.razloziIskljucenjaNerastovaToolStripMenuItem_Click);
            // 
            // sifrarnikBolestiToolStripMenuItem
            // 
            this.sifrarnikBolestiToolStripMenuItem.Name = "sifrarnikBolestiToolStripMenuItem";
            this.sifrarnikBolestiToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.sifrarnikBolestiToolStripMenuItem.Text = "5. Sifrarnik bolesti";
            this.sifrarnikBolestiToolStripMenuItem.Click += new System.EventHandler(this.sifrarnikBolestiToolStripMenuItem_Click);
            // 
            // razloziPrinudnogZalucenjaToolStripMenuItem
            // 
            this.razloziPrinudnogZalucenjaToolStripMenuItem.Name = "razloziPrinudnogZalucenjaToolStripMenuItem";
            this.razloziPrinudnogZalucenjaToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.razloziPrinudnogZalucenjaToolStripMenuItem.Text = "6. Razlozi prinudnog zalucenja";
            this.razloziPrinudnogZalucenjaToolStripMenuItem.Click += new System.EventHandler(this.razloziPrinudnogZalucenjaToolStripMenuItem_Click);
            // 
            // razloziPonovnogPripustaToolStripMenuItem
            // 
            this.razloziPonovnogPripustaToolStripMenuItem.Name = "razloziPonovnogPripustaToolStripMenuItem";
            this.razloziPonovnogPripustaToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.razloziPonovnogPripustaToolStripMenuItem.Text = "7. Razlozi ponovnog pripusta";
            this.razloziPonovnogPripustaToolStripMenuItem.Click += new System.EventHandler(this.razloziPonovnogPripustaToolStripMenuItem_Click);
            // 
            // razloziRokadePrasadiToolStripMenuItem
            // 
            this.razloziRokadePrasadiToolStripMenuItem.Name = "razloziRokadePrasadiToolStripMenuItem";
            this.razloziRokadePrasadiToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.razloziRokadePrasadiToolStripMenuItem.Text = "8. Razlozi rokade prasadi";
            this.razloziRokadePrasadiToolStripMenuItem.Click += new System.EventHandler(this.razloziRokadePrasadiToolStripMenuItem_Click);
            // 
            // razloziUginucaPoNerastovimaToolStripMenuItem
            // 
            this.razloziUginucaPoNerastovimaToolStripMenuItem.Name = "razloziUginucaPoNerastovimaToolStripMenuItem";
            this.razloziUginucaPoNerastovimaToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.razloziUginucaPoNerastovimaToolStripMenuItem.Text = "9. Razlozi uginuca po nerastovima";
            // 
            // aInterneSifreFarmeToolStripMenuItem
            // 
            this.aInterneSifreFarmeToolStripMenuItem.Name = "aInterneSifreFarmeToolStripMenuItem";
            this.aInterneSifreFarmeToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.aInterneSifreFarmeToolStripMenuItem.Text = "a. Interne sifre farme";
            // 
            // aToolStripMenuItem
            // 
            this.aToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.promenaFazeGrlaToolStripMenuItem,
            this.promenaMaticnihPodatakaGrlaToolStripMenuItem,
            this.kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem,
            this.brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem,
            this.unosPocetnogStanjaToolStripMenuItem});
            this.aToolStripMenuItem.Name = "aToolStripMenuItem";
            this.aToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.aToolStripMenuItem.Text = "A. Izmene podataka u BAZI";
            // 
            // promenaFazeGrlaToolStripMenuItem
            // 
            this.promenaFazeGrlaToolStripMenuItem.Name = "promenaFazeGrlaToolStripMenuItem";
            this.promenaFazeGrlaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.promenaFazeGrlaToolStripMenuItem.Text = "1. Promena faze grla ";
            // 
            // promenaMaticnihPodatakaGrlaToolStripMenuItem
            // 
            this.promenaMaticnihPodatakaGrlaToolStripMenuItem.Name = "promenaMaticnihPodatakaGrlaToolStripMenuItem";
            this.promenaMaticnihPodatakaGrlaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.promenaMaticnihPodatakaGrlaToolStripMenuItem.Text = "2. Promena maticnih podataka grla";
            // 
            // kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem
            // 
            this.kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem.Name = "kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem";
            this.kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem.Text = "3. Kompletno brisanje krmace iz baze podataka";
            // 
            // brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem
            // 
            this.brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem.Name = "brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem";
            this.brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem.Text = "4. Brisanje grla iz testa/odabira za dan";
            // 
            // unosPocetnogStanjaToolStripMenuItem
            // 
            this.unosPocetnogStanjaToolStripMenuItem.Name = "unosPocetnogStanjaToolStripMenuItem";
            this.unosPocetnogStanjaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.unosPocetnogStanjaToolStripMenuItem.Text = "5. Unos pocetnog stanja";
            // 
            // bLogickaKontrolaPodatakaToolStripMenuItem
            // 
            this.bLogickaKontrolaPodatakaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem});
            this.bLogickaKontrolaPodatakaToolStripMenuItem.Name = "bLogickaKontrolaPodatakaToolStripMenuItem";
            this.bLogickaKontrolaPodatakaToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.bLogickaKontrolaPodatakaToolStripMenuItem.Text = "B. Logicka kontrola podataka";
            // 
            // proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem
            // 
            this.proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem.Name = "proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem";
            this.proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem.Size = new System.Drawing.Size(400, 22);
            this.proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem.Text = "1. Provera neslaganja roditelja u pedigreu i maticnoj evidenciji";
            // 
            // iZVESTAJIIANALIZEToolStripMenuItem
            // 
            this.iZVESTAJIIANALIZEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.izvestajiIZahteviInstitutuToolStripMenuItem,
            this.mUSAnalizeToolStripMenuItem,
            this.gPSAnalizeToolStripMenuItem,
            this.upisPodatakaSaInstitutaUBazuToolStripMenuItem,
            this.pregledProizvodnjeToolStripMenuItem});
            this.iZVESTAJIIANALIZEToolStripMenuItem.Name = "iZVESTAJIIANALIZEToolStripMenuItem";
            this.iZVESTAJIIANALIZEToolStripMenuItem.Size = new System.Drawing.Size(134, 20);
            this.iZVESTAJIIANALIZEToolStripMenuItem.Text = "8.IZVESTAJI I ANALIZE";
            // 
            // izvestajiIZahteviInstitutuToolStripMenuItem
            // 
            this.izvestajiIZahteviInstitutuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.izvodIzGlavneMaticneEvidencijeToolStripMenuItem,
            this.registriToolStripMenuItem,
            this.zahtevZaRegistracijuToolStripMenuItem,
            this.toolStripMenuItem3,
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem1,
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem,
            this.komisijskiZapisnikZaNazimiceToolStripMenuItem,
            this.zahtevZaIzdavanjePedigreaToolStripMenuItem});
            this.izvestajiIZahteviInstitutuToolStripMenuItem.Name = "izvestajiIZahteviInstitutuToolStripMenuItem";
            this.izvestajiIZahteviInstitutuToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.izvestajiIZahteviInstitutuToolStripMenuItem.Text = "1.Izvestaji i zahtevi institutu";
            // 
            // izvodIzGlavneMaticneEvidencijeToolStripMenuItem
            // 
            this.izvodIzGlavneMaticneEvidencijeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem12,
            this.toolStripMenuItem13,
            this.toolStripMenuItem14,
            this.toolStripMenuItem15});
            this.izvodIzGlavneMaticneEvidencijeToolStripMenuItem.Name = "izvodIzGlavneMaticneEvidencijeToolStripMenuItem";
            this.izvodIzGlavneMaticneEvidencijeToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.izvodIzGlavneMaticneEvidencijeToolStripMenuItem.Text = "1.Izvod iz glavne maticne evidencije";
            // 
            // toolStripMenuItem12
            // 
            this.toolStripMenuItem12.Name = "toolStripMenuItem12";
            this.toolStripMenuItem12.Size = new System.Drawing.Size(318, 22);
            this.toolStripMenuItem12.Text = "1. Izvod iz glavne maticne evidencije";
            this.toolStripMenuItem12.Click += new System.EventHandler(this.toolStripMenuItem12_Click);
            // 
            // toolStripMenuItem13
            // 
            this.toolStripMenuItem13.Name = "toolStripMenuItem13";
            this.toolStripMenuItem13.Size = new System.Drawing.Size(318, 22);
            this.toolStripMenuItem13.Text = "2. Izvod iz glavne maticne evidencije - Dopuna";
            this.toolStripMenuItem13.Click += new System.EventHandler(this.toolStripMenuItem13_Click);
            // 
            // toolStripMenuItem14
            // 
            this.toolStripMenuItem14.Name = "toolStripMenuItem14";
            this.toolStripMenuItem14.Size = new System.Drawing.Size(318, 22);
            this.toolStripMenuItem14.Text = "3. Spisak umaticenih grla";
            // 
            // toolStripMenuItem15
            // 
            this.toolStripMenuItem15.Name = "toolStripMenuItem15";
            this.toolStripMenuItem15.Size = new System.Drawing.Size(318, 22);
            this.toolStripMenuItem15.Text = "4. Spisak grla na gazdinstvu";
            this.toolStripMenuItem15.Click += new System.EventHandler(this.toolStripMenuItem15_Click);
            // 
            // registriToolStripMenuItem
            // 
            this.registriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pripustiToolStripMenuItem1,
            this.prasenjaToolStripMenuItem1,
            this.registarPriplodnogPodmladkaToolStripMenuItem,
            this.registarPrasadiToolStripMenuItem1,
            this.registarPrasadiToolStripMenuItem2,
            this.skartGrlaToolStripMenuItem,
            this.pripusniSpisakZaNerastaToolStripMenuItem,
            this.exportRegistaraZaFakultetToolStripMenuItem});
            this.registriToolStripMenuItem.Name = "registriToolStripMenuItem";
            this.registriToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.registriToolStripMenuItem.Text = "2.Registri";
            // 
            // pripustiToolStripMenuItem1
            // 
            this.pripustiToolStripMenuItem1.Name = "pripustiToolStripMenuItem1";
            this.pripustiToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.pripustiToolStripMenuItem1.Text = "1. Registar pripusta";
            this.pripustiToolStripMenuItem1.Click += new System.EventHandler(this.pripustiToolStripMenuItem1_Click);
            // 
            // prasenjaToolStripMenuItem1
            // 
            this.prasenjaToolStripMenuItem1.Name = "prasenjaToolStripMenuItem1";
            this.prasenjaToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.prasenjaToolStripMenuItem1.Text = "2. Registar prasenja";
            this.prasenjaToolStripMenuItem1.Click += new System.EventHandler(this.prasenjaToolStripMenuItem1_Click);
            // 
            // registarPriplodnogPodmladkaToolStripMenuItem
            // 
            this.registarPriplodnogPodmladkaToolStripMenuItem.Name = "registarPriplodnogPodmladkaToolStripMenuItem";
            this.registarPriplodnogPodmladkaToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.registarPriplodnogPodmladkaToolStripMenuItem.Text = "3. Registar priplodnog podmlatka";
            this.registarPriplodnogPodmladkaToolStripMenuItem.Click += new System.EventHandler(this.registarPriplodnogPodmladkaToolStripMenuItem_Click);
            // 
            // registarPrasadiToolStripMenuItem1
            // 
            this.registarPrasadiToolStripMenuItem1.Name = "registarPrasadiToolStripMenuItem1";
            this.registarPrasadiToolStripMenuItem1.Size = new System.Drawing.Size(250, 22);
            this.registarPrasadiToolStripMenuItem1.Text = "4. Registar zalucenja";
            this.registarPrasadiToolStripMenuItem1.Click += new System.EventHandler(this.registarPrasadiToolStripMenuItem1_Click);
            // 
            // registarPrasadiToolStripMenuItem2
            // 
            this.registarPrasadiToolStripMenuItem2.Name = "registarPrasadiToolStripMenuItem2";
            this.registarPrasadiToolStripMenuItem2.Size = new System.Drawing.Size(250, 22);
            this.registarPrasadiToolStripMenuItem2.Text = "5. Registar prasadi";
            this.registarPrasadiToolStripMenuItem2.Click += new System.EventHandler(this.registarPrasadiToolStripMenuItem2_Click);
            // 
            // skartGrlaToolStripMenuItem
            // 
            this.skartGrlaToolStripMenuItem.Name = "skartGrlaToolStripMenuItem";
            this.skartGrlaToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.skartGrlaToolStripMenuItem.Text = "6. Registar skart grla";
            this.skartGrlaToolStripMenuItem.Click += new System.EventHandler(this.skartGrlaToolStripMenuItem_Click);
            // 
            // pripusniSpisakZaNerastaToolStripMenuItem
            // 
            this.pripusniSpisakZaNerastaToolStripMenuItem.Name = "pripusniSpisakZaNerastaToolStripMenuItem";
            this.pripusniSpisakZaNerastaToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.pripusniSpisakZaNerastaToolStripMenuItem.Text = "7. Pripusni spisak za nerasta";
            // 
            // exportRegistaraZaFakultetToolStripMenuItem
            // 
            this.exportRegistaraZaFakultetToolStripMenuItem.Name = "exportRegistaraZaFakultetToolStripMenuItem";
            this.exportRegistaraZaFakultetToolStripMenuItem.Size = new System.Drawing.Size(250, 22);
            this.exportRegistaraZaFakultetToolStripMenuItem.Text = "8. Export registara za fakultet";
            // 
            // zahtevZaRegistracijuToolStripMenuItem
            // 
            this.zahtevZaRegistracijuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem,
            this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem,
            this.prijavaPocetkaBioloskogTestaToolStripMenuItem,
            this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem});
            this.zahtevZaRegistracijuToolStripMenuItem.Name = "zahtevZaRegistracijuToolStripMenuItem";
            this.zahtevZaRegistracijuToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.zahtevZaRegistracijuToolStripMenuItem.Text = "3.Zahtev za registraciju (HB i RB brojevi)";
            // 
            // zahtevZaRegistracijuZenskihGrlaToolStripMenuItem
            // 
            this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem.Name = "zahtevZaRegistracijuZenskihGrlaToolStripMenuItem";
            this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem.Size = new System.Drawing.Size(398, 22);
            this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem.Text = "1. Zahtev za registraciju zenskih grla";
            this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem.Click += new System.EventHandler(this.zahtevZaRegistracijuZenskihGrlaToolStripMenuItem_Click);
            // 
            // zahtevZaRegistracijuMuskihGrlaToolStripMenuItem
            // 
            this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem.Name = "zahtevZaRegistracijuMuskihGrlaToolStripMenuItem";
            this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem.Size = new System.Drawing.Size(398, 22);
            this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem.Text = "2. Zahtev za registraciju muskih grla";
            this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem.Click += new System.EventHandler(this.zahtevZaRegistracijuMuskihGrlaToolStripMenuItem_Click);
            // 
            // prijavaPocetkaBioloskogTestaToolStripMenuItem
            // 
            this.prijavaPocetkaBioloskogTestaToolStripMenuItem.Name = "prijavaPocetkaBioloskogTestaToolStripMenuItem";
            this.prijavaPocetkaBioloskogTestaToolStripMenuItem.Size = new System.Drawing.Size(398, 22);
            this.prijavaPocetkaBioloskogTestaToolStripMenuItem.Text = "3. Prijava pocetka bioloskog testa";
            this.prijavaPocetkaBioloskogTestaToolStripMenuItem.Click += new System.EventHandler(this.prijavaPocetkaBioloskogTestaToolStripMenuItem_Click);
            // 
            // zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem
            // 
            this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem.Name = "zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem";
            this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem.Size = new System.Drawing.Size(398, 22);
            this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem.Text = "4. Zahtev za izdavanje dozvole o koriscenju nerasta u priplodu";
            this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem.Click += new System.EventHandler(this.zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(285, 22);
            this.toolStripMenuItem3.Text = "4.Performans test - ultrazvucno merenje";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // komisijskiZapisnikZaKrmaceToolStripMenuItem1
            // 
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem2,
            this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem});
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem1.Name = "komisijskiZapisnikZaKrmaceToolStripMenuItem1";
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem1.Size = new System.Drawing.Size(285, 22);
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem1.Text = "5.Komisijski zapisnik za krmace";
            // 
            // komisijskiZapisnikZaKrmaceToolStripMenuItem2
            // 
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem2.Name = "komisijskiZapisnikZaKrmaceToolStripMenuItem2";
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem2.Size = new System.Drawing.Size(301, 22);
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem2.Text = "1. Komisijski zapisnik za krmace";
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem2.Click += new System.EventHandler(this.komisijskiZapisnikZaKrmaceToolStripMenuItem2_Click);
            // 
            // dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem
            // 
            this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem.Name = "dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem";
            this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem.Size = new System.Drawing.Size(301, 22);
            this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem.Text = "2. Dopuna komisijskog zapisnika za krmace";
            this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem.Click += new System.EventHandler(this.dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem_Click);
            // 
            // komisijskiZapisnikZaNerastoveToolStripMenuItem
            // 
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem.Name = "komisijskiZapisnikZaNerastoveToolStripMenuItem";
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem.Text = "6.Komisijski zapisnik za nerastove";
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem.Click += new System.EventHandler(this.komisijskiZapisnikZaNerastoveToolStripMenuItem_Click);
            // 
            // komisijskiZapisnikZaNazimiceToolStripMenuItem
            // 
            this.komisijskiZapisnikZaNazimiceToolStripMenuItem.Name = "komisijskiZapisnikZaNazimiceToolStripMenuItem";
            this.komisijskiZapisnikZaNazimiceToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.komisijskiZapisnikZaNazimiceToolStripMenuItem.Text = "7.Komisijski zapisnik za nazimice";
            this.komisijskiZapisnikZaNazimiceToolStripMenuItem.Click += new System.EventHandler(this.komisijskiZapisnikZaNazimiceToolStripMenuItem_Click);
            // 
            // zahtevZaIzdavanjePedigreaToolStripMenuItem
            // 
            this.zahtevZaIzdavanjePedigreaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem,
            this.zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem});
            this.zahtevZaIzdavanjePedigreaToolStripMenuItem.Name = "zahtevZaIzdavanjePedigreaToolStripMenuItem";
            this.zahtevZaIzdavanjePedigreaToolStripMenuItem.Size = new System.Drawing.Size(285, 22);
            this.zahtevZaIzdavanjePedigreaToolStripMenuItem.Text = "8.Zahtev za izdavanje pedigrea";
            // 
            // zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem
            // 
            this.zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem.Name = "zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem";
            this.zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem.Text = "1. Zahtev za izdavanje pedigrea nazimica i krmaca";
            // 
            // zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem
            // 
            this.zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem.Name = "zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem";
            this.zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem.Size = new System.Drawing.Size(335, 22);
            this.zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem.Text = "2. Zahtev za izdavanje pedigrea nerastova";
            // 
            // mUSAnalizeToolStripMenuItem
            // 
            this.mUSAnalizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem,
            this.mUS2ToolStripMenuItem,
            this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem,
            this.mUS4PerformansTestNazimicaToolStripMenuItem,
            this.mUS5PerformansTestNerastovaToolStripMenuItem,
            this.mUS6BioloskiTestNerastovaToolStripMenuItem,
            this.mUS7ProizvodniRezultatiToolStripMenuItem});
            this.mUSAnalizeToolStripMenuItem.Name = "mUSAnalizeToolStripMenuItem";
            this.mUSAnalizeToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.mUSAnalizeToolStripMenuItem.Text = "2.MUS Analize";
            // 
            // mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem
            // 
            this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem.Name = "mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem";
            this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem.Text = "1.MUS-1 Kontrola produktivnosti umaticenih krmaca";
            this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem.Click += new System.EventHandler(this.mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem_Click);
            // 
            // mUS2ToolStripMenuItem
            // 
            this.mUS2ToolStripMenuItem.Name = "mUS2ToolStripMenuItem";
            this.mUS2ToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS2ToolStripMenuItem.Text = "2.MUS-2 Kontrola produktivnosti umaticenih nerastova";
            this.mUS2ToolStripMenuItem.Click += new System.EventHandler(this.mUS2ToolStripMenuItem_Click);
            // 
            // mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem
            // 
            this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem.Name = "mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem";
            this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem.Text = "3.MUS-3 Kontrola produktivnosti nerastovkih majki";
            this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem.Click += new System.EventHandler(this.mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem_Click);
            // 
            // mUS4PerformansTestNazimicaToolStripMenuItem
            // 
            this.mUS4PerformansTestNazimicaToolStripMenuItem.Name = "mUS4PerformansTestNazimicaToolStripMenuItem";
            this.mUS4PerformansTestNazimicaToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS4PerformansTestNazimicaToolStripMenuItem.Text = "4.MUS-4 Performans test nazimica";
            this.mUS4PerformansTestNazimicaToolStripMenuItem.Click += new System.EventHandler(this.mUS4PerformansTestNazimicaToolStripMenuItem_Click);
            // 
            // mUS5PerformansTestNerastovaToolStripMenuItem
            // 
            this.mUS5PerformansTestNerastovaToolStripMenuItem.Name = "mUS5PerformansTestNerastovaToolStripMenuItem";
            this.mUS5PerformansTestNerastovaToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS5PerformansTestNerastovaToolStripMenuItem.Text = "5.MUS-5 Performans test nerastova";
            this.mUS5PerformansTestNerastovaToolStripMenuItem.Click += new System.EventHandler(this.mUS5PerformansTestNerastovaToolStripMenuItem_Click);
            // 
            // mUS6BioloskiTestNerastovaToolStripMenuItem
            // 
            this.mUS6BioloskiTestNerastovaToolStripMenuItem.Name = "mUS6BioloskiTestNerastovaToolStripMenuItem";
            this.mUS6BioloskiTestNerastovaToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS6BioloskiTestNerastovaToolStripMenuItem.Text = "6*.MUS-6 Bioloski test nerastova";
            this.mUS6BioloskiTestNerastovaToolStripMenuItem.Click += new System.EventHandler(this.mUS6BioloskiTestNerastovaToolStripMenuItem_Click);
            // 
            // mUS7ProizvodniRezultatiToolStripMenuItem
            // 
            this.mUS7ProizvodniRezultatiToolStripMenuItem.Name = "mUS7ProizvodniRezultatiToolStripMenuItem";
            this.mUS7ProizvodniRezultatiToolStripMenuItem.Size = new System.Drawing.Size(364, 22);
            this.mUS7ProizvodniRezultatiToolStripMenuItem.Text = "7.MUS-7 Proizvodni rezultati";
            // 
            // gPSAnalizeToolStripMenuItem
            // 
            this.gPSAnalizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem,
            this.gPS3RasnaStruksturaZapataToolStripMenuItem,
            this.gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem,
            this.gPS5ToolStripMenuItem,
            this.gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem,
            this.gPS7ReproduktivniPokazateljiToolStripMenuItem,
            this.gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem,
            this.gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem,
            this.gPS10ToolStripMenuItem,
            this.gPS11PerformansTestNazimicaToolStripMenuItem,
            this.gPSToolStripMenuItem});
            this.gPSAnalizeToolStripMenuItem.Name = "gPSAnalizeToolStripMenuItem";
            this.gPSAnalizeToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.gPSAnalizeToolStripMenuItem.Text = "3.GPS Analize";
            // 
            // gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem
            // 
            this.gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem.Name = "gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem";
            this.gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem.Text = "2. GPS-2 Brojnostanje svinja po kategorijama";
            // 
            // gPS3RasnaStruksturaZapataToolStripMenuItem
            // 
            this.gPS3RasnaStruksturaZapataToolStripMenuItem.Name = "gPS3RasnaStruksturaZapataToolStripMenuItem";
            this.gPS3RasnaStruksturaZapataToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS3RasnaStruksturaZapataToolStripMenuItem.Text = "3. GPS-3 Rasna strukstura zapata";
            // 
            // gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem
            // 
            this.gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem.Name = "gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem";
            this.gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem.Text = "4. GPS-4 Paritetna strukstura zapata krmaca";
            // 
            // gPS5ToolStripMenuItem
            // 
            this.gPS5ToolStripMenuItem.Name = "gPS5ToolStripMenuItem";
            this.gPS5ToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS5ToolStripMenuItem.Text = "5. GPS-5 Rezultati oprasivosti";
            // 
            // gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem
            // 
            this.gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem.Name = "gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem";
            this.gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem.Text = "6. GPS-6 Rezultati prasenja i odgoja prasadi";
            // 
            // gPS7ReproduktivniPokazateljiToolStripMenuItem
            // 
            this.gPS7ReproduktivniPokazateljiToolStripMenuItem.Name = "gPS7ReproduktivniPokazateljiToolStripMenuItem";
            this.gPS7ReproduktivniPokazateljiToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS7ReproduktivniPokazateljiToolStripMenuItem.Text = "7. GPS-7 Reproduktivni pokazatelji";
            // 
            // gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem
            // 
            this.gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem.Name = "gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem";
            this.gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem.Text = "8. GPS-8 Uginuce svinja po kategorijama";
            // 
            // gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem
            // 
            this.gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem.Name = "gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem";
            this.gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem.Text = "9.GPS-9 Prirast i utrosak hrane po kategorijama";
            // 
            // gPS10ToolStripMenuItem
            // 
            this.gPS10ToolStripMenuItem.Name = "gPS10ToolStripMenuItem";
            this.gPS10ToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS10ToolStripMenuItem.Text = "10. GPS-10 Proizvodnja tovljenika";
            // 
            // gPS11PerformansTestNazimicaToolStripMenuItem
            // 
            this.gPS11PerformansTestNazimicaToolStripMenuItem.Name = "gPS11PerformansTestNazimicaToolStripMenuItem";
            this.gPS11PerformansTestNazimicaToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPS11PerformansTestNazimicaToolStripMenuItem.Text = "11. GPS-11 Performans test nazimica";
            // 
            // gPSToolStripMenuItem
            // 
            this.gPSToolStripMenuItem.Name = "gPSToolStripMenuItem";
            this.gPSToolStripMenuItem.Size = new System.Drawing.Size(321, 22);
            this.gPSToolStripMenuItem.Text = "12. GPS-12 Performans test nerastova";
            // 
            // upisPodatakaSaInstitutaUBazuToolStripMenuItem
            // 
            this.upisPodatakaSaInstitutaUBazuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.upisRezultataPerformansTestaSi1ToolStripMenuItem,
            this.upisRegistrovanihGrlaHBIToolStripMenuItem,
            this.upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem});
            this.upisPodatakaSaInstitutaUBazuToolStripMenuItem.Name = "upisPodatakaSaInstitutaUBazuToolStripMenuItem";
            this.upisPodatakaSaInstitutaUBazuToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.upisPodatakaSaInstitutaUBazuToolStripMenuItem.Text = "4.Upis podataka sa Instituta u bazu";
            // 
            // upisRezultataPerformansTestaSi1ToolStripMenuItem
            // 
            this.upisRezultataPerformansTestaSi1ToolStripMenuItem.Name = "upisRezultataPerformansTestaSi1ToolStripMenuItem";
            this.upisRezultataPerformansTestaSi1ToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.upisRezultataPerformansTestaSi1ToolStripMenuItem.Text = "1.Upis rezultata performans testa - Si1";
            // 
            // upisRegistrovanihGrlaHBIToolStripMenuItem
            // 
            this.upisRegistrovanihGrlaHBIToolStripMenuItem.Name = "upisRegistrovanihGrlaHBIToolStripMenuItem";
            this.upisRegistrovanihGrlaHBIToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.upisRegistrovanihGrlaHBIToolStripMenuItem.Text = "2.Upis registrovanih grla - HB i RB brojevi";
            // 
            // upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem
            // 
            this.upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem.Name = "upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem";
            this.upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem.Size = new System.Drawing.Size(297, 22);
            this.upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem.Text = "3.Upis rezultata smotre - Klasa i ekst.ocena";
            // 
            // pregledProizvodnjeToolStripMenuItem
            // 
            this.pregledProizvodnjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mesecniPregledProizvodnjeToolStripMenuItem,
            this.nedeljniPregledProizvodnjeToolStripMenuItem,
            this.dnevniPregledProizvodnjeToolStripMenuItem,
            this.pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem});
            this.pregledProizvodnjeToolStripMenuItem.Name = "pregledProizvodnjeToolStripMenuItem";
            this.pregledProizvodnjeToolStripMenuItem.Size = new System.Drawing.Size(256, 22);
            this.pregledProizvodnjeToolStripMenuItem.Text = "5.Pregled proizvodnje";
            // 
            // mesecniPregledProizvodnjeToolStripMenuItem
            // 
            this.mesecniPregledProizvodnjeToolStripMenuItem.Name = "mesecniPregledProizvodnjeToolStripMenuItem";
            this.mesecniPregledProizvodnjeToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.mesecniPregledProizvodnjeToolStripMenuItem.Text = "1. Mesecni pregled proizvodnje";
            // 
            // nedeljniPregledProizvodnjeToolStripMenuItem
            // 
            this.nedeljniPregledProizvodnjeToolStripMenuItem.Name = "nedeljniPregledProizvodnjeToolStripMenuItem";
            this.nedeljniPregledProizvodnjeToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.nedeljniPregledProizvodnjeToolStripMenuItem.Text = "2. Nedeljni pregled proizvodnje";
            // 
            // dnevniPregledProizvodnjeToolStripMenuItem
            // 
            this.dnevniPregledProizvodnjeToolStripMenuItem.Name = "dnevniPregledProizvodnjeToolStripMenuItem";
            this.dnevniPregledProizvodnjeToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.dnevniPregledProizvodnjeToolStripMenuItem.Text = "3. Dnevni pregled proizvodnje";
            // 
            // pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem
            // 
            this.pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem.Name = "pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem";
            this.pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem.Text = "4. Pregled proizvodnje po zadatim intervalima";
            // 
            // sMOTRAGRLAToolStripMenuItem
            // 
            this.sMOTRAGRLAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem,
            this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem,
            this.smotraNerastovaToolStripMenuItem,
            this.smotraKrmacaToolStripMenuItem,
            this.smotraNazimicaToolStripMenuItem,
            this.smotraMladihNerastovaToolStripMenuItem,
            this.rezimeSmotreAnalizeToolStripMenuItem});
            this.sMOTRAGRLAToolStripMenuItem.Name = "sMOTRAGRLAToolStripMenuItem";
            this.sMOTRAGRLAToolStripMenuItem.Size = new System.Drawing.Size(106, 20);
            this.sMOTRAGRLAToolStripMenuItem.Text = "9.SMOTRA GRLA";
            // 
            // evidencijaPodatakaSaSmotreGrlaToolStripMenuItem
            // 
            this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem.Name = "evidencijaPodatakaSaSmotreGrlaToolStripMenuItem";
            this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem.Text = "1.Evidencija podataka sa smotre grla";
            this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem.Click += new System.EventHandler(this.evidencijaPodatakaSaSmotreGrlaToolStripMenuItem_Click);
            // 
            // rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem
            // 
            this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem.Name = "rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem";
            this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem.Text = "2.Rezime smotre - Trenutno stanje grla na farmi";
            this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem.Click += new System.EventHandler(this.rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem_Click);
            // 
            // smotraNerastovaToolStripMenuItem
            // 
            this.smotraNerastovaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem1,
            this.smotraNerastovaRadniSpisakToolStripMenuItem});
            this.smotraNerastovaToolStripMenuItem.Name = "smotraNerastovaToolStripMenuItem";
            this.smotraNerastovaToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.smotraNerastovaToolStripMenuItem.Text = "3.Smotra nerastova";
            // 
            // komisijskiZapisnikZaNerastoveToolStripMenuItem1
            // 
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem1.Name = "komisijskiZapisnikZaNerastoveToolStripMenuItem1";
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem1.Size = new System.Drawing.Size(252, 22);
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem1.Text = "1. Komisijski zapisnik za nerastove";
            this.komisijskiZapisnikZaNerastoveToolStripMenuItem1.Click += new System.EventHandler(this.komisijskiZapisnikZaNerastoveToolStripMenuItem1_Click);
            // 
            // smotraNerastovaRadniSpisakToolStripMenuItem
            // 
            this.smotraNerastovaRadniSpisakToolStripMenuItem.Name = "smotraNerastovaRadniSpisakToolStripMenuItem";
            this.smotraNerastovaRadniSpisakToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.smotraNerastovaRadniSpisakToolStripMenuItem.Text = "2. Smotra nerastova-  radni spisak";
            this.smotraNerastovaRadniSpisakToolStripMenuItem.Click += new System.EventHandler(this.smotraNerastovaRadniSpisakToolStripMenuItem_Click);
            // 
            // smotraKrmacaToolStripMenuItem
            // 
            this.smotraKrmacaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem,
            this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem,
            this.rezimeSmotreKrmacaToolStripMenuItem,
            this.rezimeSmotreKrmacaDopunaToolStripMenuItem,
            this.smotreKrmacaRadniSpiskaToolStripMenuItem,
            this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem});
            this.smotraKrmacaToolStripMenuItem.Name = "smotraKrmacaToolStripMenuItem";
            this.smotraKrmacaToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.smotraKrmacaToolStripMenuItem.Text = "4.Smotra krmaca";
            // 
            // komisijskiZapisnikZaKrmaceToolStripMenuItem
            // 
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem.Name = "komisijskiZapisnikZaKrmaceToolStripMenuItem";
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem.Text = "1.Komisijski zapisnik za krmace";
            this.komisijskiZapisnikZaKrmaceToolStripMenuItem.Click += new System.EventHandler(this.komisijskiZapisnikZaKrmaceToolStripMenuItem_Click);
            // 
            // komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem
            // 
            this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem.Name = "komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem";
            this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem.Text = "2.Komisijski zapisnik za krmace - Dopuna";
            this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem.Click += new System.EventHandler(this.komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem_Click);
            // 
            // rezimeSmotreKrmacaToolStripMenuItem
            // 
            this.rezimeSmotreKrmacaToolStripMenuItem.Name = "rezimeSmotreKrmacaToolStripMenuItem";
            this.rezimeSmotreKrmacaToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.rezimeSmotreKrmacaToolStripMenuItem.Text = "3.Rezime smotre krmaca";
            this.rezimeSmotreKrmacaToolStripMenuItem.Click += new System.EventHandler(this.rezimeSmotreKrmacaToolStripMenuItem_Click);
            // 
            // rezimeSmotreKrmacaDopunaToolStripMenuItem
            // 
            this.rezimeSmotreKrmacaDopunaToolStripMenuItem.Name = "rezimeSmotreKrmacaDopunaToolStripMenuItem";
            this.rezimeSmotreKrmacaDopunaToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.rezimeSmotreKrmacaDopunaToolStripMenuItem.Text = "4.Rezime smotre krmaca - Dopuna";
            this.rezimeSmotreKrmacaDopunaToolStripMenuItem.Click += new System.EventHandler(this.rezimeSmotreKrmacaDopunaToolStripMenuItem_Click);
            // 
            // smotreKrmacaRadniSpiskaToolStripMenuItem
            // 
            this.smotreKrmacaRadniSpiskaToolStripMenuItem.Name = "smotreKrmacaRadniSpiskaToolStripMenuItem";
            this.smotreKrmacaRadniSpiskaToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.smotreKrmacaRadniSpiskaToolStripMenuItem.Text = "5.Smotre krmaca - radni spiska";
            this.smotreKrmacaRadniSpiskaToolStripMenuItem.Click += new System.EventHandler(this.smotreKrmacaRadniSpiskaToolStripMenuItem_Click);
            // 
            // smotraKrmacaDopunaRadniSpisakToolStripMenuItem
            // 
            this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem.Name = "smotraKrmacaDopunaRadniSpisakToolStripMenuItem";
            this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem.Size = new System.Drawing.Size(290, 22);
            this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem.Text = "6.Smotra krmaca - dopuna- radni spisak";
            this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem.Click += new System.EventHandler(this.smotraKrmacaDopunaRadniSpisakToolStripMenuItem_Click);
            // 
            // smotraNazimicaToolStripMenuItem
            // 
            this.smotraNazimicaToolStripMenuItem.Name = "smotraNazimicaToolStripMenuItem";
            this.smotraNazimicaToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.smotraNazimicaToolStripMenuItem.Text = "5.Smotra nazimica";
            this.smotraNazimicaToolStripMenuItem.Click += new System.EventHandler(this.smotraNazimicaToolStripMenuItem_Click);
            // 
            // smotraMladihNerastovaToolStripMenuItem
            // 
            this.smotraMladihNerastovaToolStripMenuItem.Name = "smotraMladihNerastovaToolStripMenuItem";
            this.smotraMladihNerastovaToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.smotraMladihNerastovaToolStripMenuItem.Text = "6.Smotra mladih nerastova";
            this.smotraMladihNerastovaToolStripMenuItem.Click += new System.EventHandler(this.smotraMladihNerastovaToolStripMenuItem_Click);
            // 
            // rezimeSmotreAnalizeToolStripMenuItem
            // 
            this.rezimeSmotreAnalizeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proizvodnjaPoParitetimaToolStripMenuItem,
            this.proizvodnjaPoRasamaToolStripMenuItem,
            this.proizvodnjaPoKlasamaToolStripMenuItem});
            this.rezimeSmotreAnalizeToolStripMenuItem.Name = "rezimeSmotreAnalizeToolStripMenuItem";
            this.rezimeSmotreAnalizeToolStripMenuItem.Size = new System.Drawing.Size(323, 22);
            this.rezimeSmotreAnalizeToolStripMenuItem.Text = "7.Rezime smotre - analize";
            // 
            // proizvodnjaPoParitetimaToolStripMenuItem
            // 
            this.proizvodnjaPoParitetimaToolStripMenuItem.Name = "proizvodnjaPoParitetimaToolStripMenuItem";
            this.proizvodnjaPoParitetimaToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.proizvodnjaPoParitetimaToolStripMenuItem.Text = "1. Proizvodnja po paritetima";
            this.proizvodnjaPoParitetimaToolStripMenuItem.Click += new System.EventHandler(this.proizvodnjaPoParitetimaToolStripMenuItem_Click);
            // 
            // proizvodnjaPoRasamaToolStripMenuItem
            // 
            this.proizvodnjaPoRasamaToolStripMenuItem.Name = "proizvodnjaPoRasamaToolStripMenuItem";
            this.proizvodnjaPoRasamaToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.proizvodnjaPoRasamaToolStripMenuItem.Text = "2. Proizvodnja po rasama";
            this.proizvodnjaPoRasamaToolStripMenuItem.Click += new System.EventHandler(this.proizvodnjaPoRasamaToolStripMenuItem_Click);
            // 
            // proizvodnjaPoKlasamaToolStripMenuItem
            // 
            this.proizvodnjaPoKlasamaToolStripMenuItem.Name = "proizvodnjaPoKlasamaToolStripMenuItem";
            this.proizvodnjaPoKlasamaToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            this.proizvodnjaPoKlasamaToolStripMenuItem.Text = "3. Proizvodnja po klasama";
            this.proizvodnjaPoKlasamaToolStripMenuItem.Click += new System.EventHandler(this.proizvodnjaPoKlasamaToolStripMenuItem_Click);
            // 
            // alatiToolStripMenuItem
            // 
            this.alatiToolStripMenuItem.Name = "alatiToolStripMenuItem";
            this.alatiToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.alatiToolStripMenuItem.Text = "Alati";
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "Window";
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aplicacijaPIGSToolStripMenuItem,
            this.printCTRLPToolStripMenuItem,
            this.printToolStripMenuItem,
            this.databaseUtilityToolStripMenuItem,
            this.kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.systemToolStripMenuItem.Text = "System";
            // 
            // aplicacijaPIGSToolStripMenuItem
            // 
            this.aplicacijaPIGSToolStripMenuItem.Name = "aplicacijaPIGSToolStripMenuItem";
            this.aplicacijaPIGSToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.aplicacijaPIGSToolStripMenuItem.Text = "Aplikacija PIGS Win";
            // 
            // printCTRLPToolStripMenuItem
            // 
            this.printCTRLPToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI Light", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printCTRLPToolStripMenuItem.Name = "printCTRLPToolStripMenuItem";
            this.printCTRLPToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.printCTRLPToolStripMenuItem.Text = "Print...                CTRL+P";
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.printToolStripMenuItem.Text = "Print Setup ...";
            // 
            // databaseUtilityToolStripMenuItem
            // 
            this.databaseUtilityToolStripMenuItem.Name = "databaseUtilityToolStripMenuItem";
            this.databaseUtilityToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.databaseUtilityToolStripMenuItem.Text = "Database Utility";
            // 
            // kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem
            // 
            this.kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem.Name = "kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem";
            this.kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem.Text = "Kreiranje - modifikacija upita korisnika";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // PraseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(1118, 498);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PraseForm";
            this.Text = "PIGS Win Farma VOGANJ";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem pRIPUSTIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPripustaKrmacaINazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kalendarPripustaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem planPripustaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reproduktivniPokazateljiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontrolaProduktivnostiNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontrolaProduktivnostiOsemeniteljaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem problemiUReprodkcijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analzeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pRASENJAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zALUCENJAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iSKLJUCENJAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oSTALEFAZEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sELEKCIJAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem kARTOTEKAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iZVESTAJIIANALIZEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMOTRAGRLAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alatiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem selekcijskiIndeksiSi1Si2Si3LinijaKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizeRezultataPerformansTestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPripustaTestiranihGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvestajiIZahteviInstitutuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUSAnalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPSAnalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upisPodatakaSaInstitutaUBazuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledProizvodnjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPodatakaSaSmotreGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezimeSmotreTrenutnoStanjeGrlaSaFarmeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smotraNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smotraKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaKrmaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaKrmaceDopunaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezimeSmotreKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezimeSmotreKrmacaDopunaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smotreKrmacaRadniSpiskaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smotraNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smotraMladihNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rezimeSmotreAnalizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smotraKrmacaDopunaRadniSpisakToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reproduktivniPokazateljiZalucenihKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reproduktivniPokazateljiKrmacaPoMesecimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaZalucenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem kalendarKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPrinudnoZalucenihKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hormonalniTretmanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem detekcijaSuprasnostiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prevodiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceULaktacijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zdravstvenaZastitaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pobacajiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uginucaPoNerastovimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledSemenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izvodIzGlavneMaticneEvidencijeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaRegistracijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaKrmaceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaNerastoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaNazimiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaIzdavanjePedigreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upisRezultataPerformansTestaSi1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upisRegistrovanihGrlaHBIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaIskljucenjaGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem analizeIskljucenjaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaIskljucenjaNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem analizaIskljucenjaGrlaIzTestaOdabiraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaIskljucenjaGrlaIzRegistraPrasadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS1KontrolaProduktivnostiUmaticenihKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS3KontrolaProduktivnostiNerastovkihMajkiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS4PerformansTestNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS5PerformansTestNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS6BioloskiTestNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mUS7ProizvodniRezultatiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upisRezultataSmotreKlasaIEkstocenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS2BrojnostanjeSvinjaPoKategorijamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS3RasnaStruksturaZapataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS4ParitetnaStruksturaZapataKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS5ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS6RezultatiPrasenjaIOdgojaPrasadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS7ReproduktivniPokazateljiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS8UginuceSvinjaPoKategorijamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS9PrirastIUtrosakHranePoKategorijamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS10ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPS11PerformansTestNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gPSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPrasenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem kAlendarPrasenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kontrolaProduktivnostiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem maticniIProizvodniKartonKrmaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedigreporekloGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nerastoviToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nazimiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odabirITestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registarPrasadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maticniBrojIFazaGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sifrarnikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bLogickaKontrolaPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aplicacijaPIGSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printCTRLPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem distribucijaZivorodjenePrasadiPoParitetimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceSaNIVisePrasadiUMPrasenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceSaNIManjePrasadiUMPrasenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogFarmiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sifrarnikRasaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razloziIskljucenjaKrmacaINazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razloziIskljucenjaNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sifrarnikBolestiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razloziPrinudnogZalucenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razloziPonovnogPripustaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razloziRokadePrasadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem razloziUginucaPoNerastovimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aInterneSifreFarmeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proveraNeslaganjaRoditeljaUPedigreuIMaticnojEvidencijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseUtilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kreiranjeModifikacijaUpitaKorisnikaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceZaPripustToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem periodSuprasnostiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prevodUCekalisteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nedeljaPripustaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zaGrupuZalucenihKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zaGrupuNazimicatestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zaGrupuMerenihNazimicaodabirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dnevniPlanPripustaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produktivnostNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bioloskiTestNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bioloskiTestMladihNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kalendarKoriscenjaNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPovadjanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaAnestrijeIJalovostiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pripustiPoRasamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem oprasivostPoRasamaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oprasivostPoCiklusimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPripustaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceZaPrasenjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem neopraseneKrmaceINazimiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produktivnostKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produktivnostNerastovskihMajkiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proizvodniRezultatiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceZaZalucenjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leglaZaTetoviranjePrasadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ukupnaIskljucenjaPoGrupamaRazlogaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaReproduktivnihRazlogaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaZdravstvenihRazlogaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaSelekcijskihRazlogaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aNalizaPrinudnogKlanjaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaUginucaKmracaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ukupnaIskljucenjaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaDavanjaHormonaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem kalendarPrvopraskinjeZaHormonalniTretmanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kalendarAnestricneKrmaceZaHormonalniTretmanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaHormonalnogTretmanaNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaHormonalnogTretmanaPrvopraskinjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaHormonlanogTretmanaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaDetekcijeSuprasnostiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem kAlendarKrmaceZaPrvuDetekcijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kalendarKrmaceZaDruguDetekcijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPrevodaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPodatakaOPrasenjuILaktacijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem evidencijaObolenjaKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregeldPodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zrdravstvenoStanjeKrmaceULaktacijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krmaceKojeSuBoloveleULaktacijiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem analizeUticajaBolestiKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uticajParitetaNaZdravstvenoStanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem uticajObolenjaKrmacaNaAnestrijuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uticajObolenjaKrmacaNaPovadjanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPobacajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem8;
        private System.Windows.Forms.ToolStripMenuItem evidencijaUginucaPoNerastovimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem9;
        private System.Windows.Forms.ToolStripMenuItem analizaUginucaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPregledaSemenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem10;
        private System.Windows.Forms.ToolStripMenuItem pRenosPodatakaIzRegistraPrasadiUOdabirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaOdabirGrlaMerenjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prenosPodatakaIzOdabiraUTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prenosPodatakaULTRASALAparatPIGSBazaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPerofrmansTestGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledOdabranihGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledTestiranihNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledTestiranjaMladihNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledGrlaPoTetovirBrojevimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem si1PerformansTestGrlaUTestuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem si2TestKrmacaNaPlodnostToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem si2TestNerastovaNaPlodnostToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem si3ProgeniTestNerastovaIKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testSaLinijeKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPodatakaSaLinijeKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledPodatakaToolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem testKrmacaSaLinijeKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testNerastovaSaLinijeKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testPoRasamaSaLinijeKlanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaPerformansTestaPoRasamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rangListaOcevaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizaTestaPoOcuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem evidencijaPoreklaGrlaGNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stampaPoreklaGrlaGNToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eVidencijaPedigreaGrlaRasaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stampaPedigreaGrlaRasaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grlaBezOtvorenogPedigreaporeklaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grlaBezPotpunogPedigreaporeklaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledAktivnihKrmacaNaDanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hBRBBrojeviMarkiceIGrupeKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledKrmacaPoTetovirimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledIAnalizaPotomstvaPoMajciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem katalogNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maticniIProizvodniKartonNerastaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledIAnalizaPotpmstvaPoOcuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
        private System.Windows.Forms.ToolStripMenuItem katalogOsemenjenihNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregkedIAnalizeNazimicaPoFazamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nazimiceNaStanjuNaDanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledOsemenjenihNazimicaPoTetovirimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grlaUTestuOdabiruNaDanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledeTestaOdabiraPoTetovirimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aPregeldPodatakaZenskaPrasadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bPregledPodatakaMuskaPrasadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cPregedPodatakaObaPolaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dopunaRegistraPrasenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promenaPolaPrasadimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prasadNaStanjuNaDanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledTetoviranePrasadiZaTovToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledRegistraPrasadiPoTetovirimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dopunaBrojaSisaKodPrasadiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promenaFazeGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem promenaMaticnihPodatakaGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kompletnoBrisanjeKrmaceIzBazePodatakaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brisanjeGrlaIzTestaodabiraZaDanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unosPocetnogStanjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaNerastoveToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem smotraNerastovaRadniSpisakToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proizvodnjaPoParitetimaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proizvodnjaPoRasamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem proizvodnjaPoKlasamaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledNazimicaGrlaZaOsiguranjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledNazimicaMaticniPodaciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pripustiNazimicaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pripustiNazimicaOcekivaniDatumPrasenjaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opraseneNazimiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zaluceneNazimiceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
        private System.Windows.Forms.ToolStripMenuItem pripustiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem prasenjaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registarPriplodnogPodmladkaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registarPrasadiToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem registarPrasadiToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem skartGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pripusniSpisakZaNerastaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportRegistaraZaFakultetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaRegistracijuZenskihGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaRegistracijuMuskihGrlaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prijavaPocetkaBioloskogTestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaIzdavanjeDozvoleOKoriscenjuNerastaUPriploduToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem komisijskiZapisnikZaKrmaceToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem dopunaKomisijskogZapisnikaZaKrmaceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaIzdavanjePedigreaNazimicaIKrmacaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zahtevZaIzdavanjePedigreaNerastovaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mesecniPregledProizvodnjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nedeljniPregledProizvodnjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dnevniPregledProizvodnjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pregledProizvodnjePoZadatimIntervalimaToolStripMenuItem;
    }
}