﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Farm.Utils
{
    public static class Helper
    {
        public static int MaxColumnWidth = 50;
        public static int MaxDnevniPlanPripustaColumnWidth = 5;
        public static void OpenChildForm(Form childForm, Form activeForm, Panel panel)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panel.Controls.Add(childForm);
            panel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
        public static void OpenNewForm(Form newForm)
        {
            newForm.TopLevel = true;
            newForm.Dock = DockStyle.Fill;
            newForm.BringToFront();
            newForm.Show();
        }
    }
}
