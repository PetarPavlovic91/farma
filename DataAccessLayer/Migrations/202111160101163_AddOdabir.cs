﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOdabir : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Odabir",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        D_ODABIRA = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        POL = c.String(),
                        GN = c.Int(nullable: false),
                        T_TEST = c.String(),
                        VLA = c.String(),
                        ODG = c.String(),
                        RASA = c.String(),
                        MBR_TEST = c.String(),
                        DAT_ROD = c.DateTime(),
                        MBR_OCA = c.String(),
                        MBR_MAJKE = c.String(),
                        IZ_LEGLA = c.String(),
                        MARKICA = c.String(),
                        EKST = c.String(),
                        BR_SISA = c.String(),
                        TEZINA = c.Int(nullable: false),
                        PRIRAST = c.Int(nullable: false),
                        D_SKART = c.DateTime(),
                        RAZL_SK = c.Int(nullable: false),
                        NAPOMENA = c.String(),
                        U_PRIPLOD = c.DateTime(),
                        GNO = c.String(),
                        T_OCA10 = c.String(),
                        GNM = c.String(),
                        T_MAJKE10 = c.String(),
                        REFK = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Odabir");
        }
    }
}
