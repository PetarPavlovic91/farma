﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPedigreAndPorekloToOdabirAndTest : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Poreklo", "PraseId", "dbo.Prase");
            DropForeignKey("dbo.Pedigre", "PraseId", "dbo.Prase");
            DropIndex("dbo.Pedigre", new[] { "PraseId" });
            DropIndex("dbo.Poreklo", new[] { "PraseId" });
            AddColumn("dbo.Nerast", "GN", c => c.String());
            AddColumn("dbo.Pedigre", "OdabirId", c => c.Int());
            AddColumn("dbo.Pedigre", "TestId", c => c.Int());
            AddColumn("dbo.Poreklo", "OdabirId", c => c.Int());
            AddColumn("dbo.Poreklo", "TestId", c => c.Int());
            AlterColumn("dbo.Prase", "GN", c => c.String());
            AlterColumn("dbo.Odabir", "GN", c => c.String());
            AlterColumn("dbo.Test", "GN", c => c.String());
            AlterColumn("dbo.Test", "GEN_MAR", c => c.String());
            AlterColumn("dbo.Test", "HALOTAN", c => c.String());
            AlterColumn("dbo.Test", "SI1F", c => c.String());
            AlterColumn("dbo.Test", "SI1", c => c.String());
            AlterColumn("dbo.Test", "TETOVIRT10", c => c.String());
            CreateIndex("dbo.Pedigre", "OdabirId");
            CreateIndex("dbo.Pedigre", "TestId");
            CreateIndex("dbo.Poreklo", "OdabirId");
            CreateIndex("dbo.Poreklo", "TestId");
            AddForeignKey("dbo.Poreklo", "OdabirId", "dbo.Odabir", "Id");
            AddForeignKey("dbo.Poreklo", "TestId", "dbo.Test", "Id");
            AddForeignKey("dbo.Pedigre", "OdabirId", "dbo.Odabir", "Id");
            AddForeignKey("dbo.Pedigre", "TestId", "dbo.Test", "Id");
            DropColumn("dbo.Pedigre", "PraseId");
            DropColumn("dbo.Poreklo", "PraseId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Poreklo", "PraseId", c => c.Int());
            AddColumn("dbo.Pedigre", "PraseId", c => c.Int());
            DropForeignKey("dbo.Pedigre", "TestId", "dbo.Test");
            DropForeignKey("dbo.Pedigre", "OdabirId", "dbo.Odabir");
            DropForeignKey("dbo.Poreklo", "TestId", "dbo.Test");
            DropForeignKey("dbo.Poreklo", "OdabirId", "dbo.Odabir");
            DropIndex("dbo.Poreklo", new[] { "TestId" });
            DropIndex("dbo.Poreklo", new[] { "OdabirId" });
            DropIndex("dbo.Pedigre", new[] { "TestId" });
            DropIndex("dbo.Pedigre", new[] { "OdabirId" });
            AlterColumn("dbo.Test", "TETOVIRT10", c => c.Int());
            AlterColumn("dbo.Test", "SI1", c => c.Int());
            AlterColumn("dbo.Test", "SI1F", c => c.Int());
            AlterColumn("dbo.Test", "HALOTAN", c => c.Int());
            AlterColumn("dbo.Test", "GEN_MAR", c => c.Int());
            AlterColumn("dbo.Test", "GN", c => c.Int());
            AlterColumn("dbo.Odabir", "GN", c => c.Int());
            AlterColumn("dbo.Prase", "GN", c => c.Int());
            DropColumn("dbo.Poreklo", "TestId");
            DropColumn("dbo.Poreklo", "OdabirId");
            DropColumn("dbo.Pedigre", "TestId");
            DropColumn("dbo.Pedigre", "OdabirId");
            DropColumn("dbo.Nerast", "GN");
            CreateIndex("dbo.Poreklo", "PraseId");
            CreateIndex("dbo.Pedigre", "PraseId");
            AddForeignKey("dbo.Pedigre", "PraseId", "dbo.Prase", "Id");
            AddForeignKey("dbo.Poreklo", "PraseId", "dbo.Prase", "Id");
        }
    }
}
