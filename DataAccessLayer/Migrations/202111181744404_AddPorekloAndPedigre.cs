﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPorekloAndPedigre : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pedigre",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        T_GRLA = c.String(),
                        MBR_GRLA = c.String(),
                        POL = c.String(),
                        MB_OCA = c.String(),
                        MB_MAJKE = c.String(),
                        MBO_DEDE = c.String(),
                        MBO_BABE = c.String(),
                        MBM_DEDE = c.String(),
                        MBM_BABE = c.String(),
                        MBOD_PRAD = c.String(),
                        MBOD_PRAB = c.String(),
                        MBOB_PRAD = c.String(),
                        MBOB_PRAB = c.String(),
                        MBMD_PRAD = c.String(),
                        MBMD_PRAB = c.String(),
                        MBMB_PRAD = c.String(),
                        MBMB_PRAB = c.String(),
                        HR_GRLA = c.String(),
                        HR_OCA = c.String(),
                        HR_MAJKE = c.String(),
                        HRO_DEDE = c.String(),
                        HRO_BABE = c.String(),
                        HRM_DEDE = c.String(),
                        HRM_BABE = c.String(),
                        HROD_PRAD = c.String(),
                        HROD_PRAB = c.String(),
                        HROB_PRAD = c.String(),
                        HROB_PRAB = c.String(),
                        HRMD_PRAD = c.String(),
                        HRMD_PRAB = c.String(),
                        HRMB_PRAD = c.String(),
                        HRMB_PRAB = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Poreklo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        POL = c.String(),
                        T_GRLA = c.String(),
                        MBR_GRLA = c.String(),
                        GN = c.String(),
                        T_GRLA10 = c.String(),
                        MB_GRLA = c.String(),
                        MB_OCA = c.String(),
                        MB_MAJKE = c.String(),
                        MBO_DEDE = c.String(),
                        MBO_BABE = c.String(),
                        MBM_DEDE = c.String(),
                        MBM_BABE = c.String(),
                        MBOD_PRAD = c.String(),
                        MBOD_PRAB = c.String(),
                        MBOB_PRAD = c.String(),
                        MBOB_PRAB = c.String(),
                        MBMD_PRAD = c.String(),
                        MBMD_PRAB = c.String(),
                        MBMB_PRAD = c.String(),
                        MBMB_PRAB = c.String(),
                        HR_GRLA = c.String(),
                        HR_OCA = c.String(),
                        HR_MAJKE = c.String(),
                        HRO_DEDE = c.String(),
                        HRO_BABE = c.String(),
                        HRM_DEDE = c.String(),
                        HRM_BABE = c.String(),
                        HROD_PRAD = c.String(),
                        HROD_PRAB = c.String(),
                        HROB_PRAD = c.String(),
                        HROB_PRAB = c.String(),
                        HRMD_PRAD = c.String(),
                        HRMD_PRAB = c.String(),
                        HRMB_PRAD = c.String(),
                        HRMB_PRAB = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Poreklo");
            DropTable("dbo.Pedigre");
        }
    }
}
