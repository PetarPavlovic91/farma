﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPolutka : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Polutka",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TEZ_NAKR_T = c.Double(nullable: false),
                        TEZ_TOPLE = c.Double(nullable: false),
                        PMP1 = c.Double(nullable: false),
                        PMP2 = c.Double(nullable: false),
                        PMP3 = c.Double(nullable: false),
                        PMP4 = c.Double(nullable: false),
                        PMP5 = c.Double(nullable: false),
                        PMP6 = c.Double(nullable: false),
                        PMP7 = c.Double(nullable: false),
                        PMP8 = c.Double(nullable: false),
                        PMP9 = c.Double(nullable: false),
                        PMP10 = c.Double(nullable: false),
                        PMP11 = c.Double(nullable: false),
                        PMP12 = c.Double(nullable: false),
                        PMP13 = c.Double(nullable: false),
                        PMP14 = c.Double(nullable: false),
                        PMP15 = c.Double(nullable: false),
                        PMP16 = c.Double(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Polutka");
        }
    }
}
