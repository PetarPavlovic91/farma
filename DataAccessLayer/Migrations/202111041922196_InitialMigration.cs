﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Artikl",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SifraArtikla = c.String(maxLength: 10),
                        Naziv = c.String(maxLength: 100),
                        GR_NAB = c.Double(nullable: false),
                        PocetnoStanje = c.Double(nullable: false),
                        Ulaz = c.Double(nullable: false),
                        Izlaz = c.Double(nullable: false),
                        Stanje = c.Double(nullable: false),
                        PR_CENA = c.Double(nullable: false),
                        JedinicaMereId = c.Int(nullable: false),
                        VrstaArtiklaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.JedinicaMere", t => t.JedinicaMereId)
                .ForeignKey("dbo.VrstaArtikla", t => t.VrstaArtiklaId)
                .Index(t => t.JedinicaMereId)
                .Index(t => t.VrstaArtiklaId);
            
            CreateTable(
                "dbo.JedinicaMere",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(nullable: false, maxLength: 50),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Kartica",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Datum = c.DateTime(nullable: false),
                        BR_DOK = c.String(maxLength: 20),
                        U_I = c.String(maxLength: 1),
                        Kolicina = c.Double(nullable: false),
                        Cena = c.Double(nullable: false),
                        VrstaDokumentaId = c.Int(nullable: false),
                        VeterinarskiTehnicarId = c.Int(),
                        ArtiklId = c.Int(nullable: false),
                        VrstaTroskaId = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikl", t => t.ArtiklId)
                .ForeignKey("dbo.VeterinarskiTehnicar", t => t.VeterinarskiTehnicarId)
                .ForeignKey("dbo.VrstaDokumenta", t => t.VrstaDokumentaId)
                .ForeignKey("dbo.VrstaTroska", t => t.VrstaTroskaId)
                .Index(t => t.VrstaDokumentaId)
                .Index(t => t.VeterinarskiTehnicarId)
                .Index(t => t.ArtiklId)
                .Index(t => t.VrstaTroskaId);
            
            CreateTable(
                "dbo.VeterinarskiTehnicar",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VET = c.String(maxLength: 10),
                        Naziv = c.String(maxLength: 100),
                        Mesto = c.String(maxLength: 100),
                        Ulica = c.String(maxLength: 100),
                        Telefon = c.String(maxLength: 20),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ZIzdatnica",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BR_DOC = c.String(maxLength: 20),
                        Datum = c.DateTime(nullable: false),
                        DatumTrebovanja = c.DateTime(nullable: false),
                        BR_TREB = c.String(maxLength: 20),
                        VeterinarskiTehnicarId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.VeterinarskiTehnicar", t => t.VeterinarskiTehnicarId)
                .Index(t => t.VeterinarskiTehnicarId);
            
            CreateTable(
                "dbo.VrstaDokumenta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VD = c.String(maxLength: 10),
                        Naziv = c.String(maxLength: 50),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VrstaTroska",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VT = c.String(maxLength: 10),
                        Naziv = c.String(maxLength: 50),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SIzdatnica",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BR_DOC = c.String(maxLength: 20),
                        RB = c.Double(nullable: false),
                        Cena = c.Double(nullable: false),
                        Kolicina = c.Double(nullable: false),
                        ArtiklId = c.Int(nullable: false),
                        VrstaTroskaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikl", t => t.ArtiklId)
                .ForeignKey("dbo.VrstaTroska", t => t.VrstaTroskaId)
                .Index(t => t.ArtiklId)
                .Index(t => t.VrstaTroskaId);
            
            CreateTable(
                "dbo.PocetnoStanje",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Datum = c.DateTime(nullable: false),
                        RB = c.Double(nullable: false),
                        Kolicina = c.Double(nullable: false),
                        Cena = c.Double(nullable: false),
                        ArtiklId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikl", t => t.ArtiklId)
                .Index(t => t.ArtiklId);
            
            CreateTable(
                "dbo.SPrijemnica",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BR_DOC = c.String(maxLength: 20),
                        RB = c.Double(nullable: false),
                        Cena = c.Double(nullable: false),
                        Kolicina = c.Double(nullable: false),
                        ArtiklId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Artikl", t => t.ArtiklId)
                .Index(t => t.ArtiklId);
            
            CreateTable(
                "dbo.VrstaArtikla",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VGP = c.String(maxLength: 10),
                        Naziv = c.String(maxLength: 100),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Partner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SIFPP = c.String(maxLength: 10),
                        Naziv = c.String(maxLength: 100),
                        Adresa = c.String(maxLength: 100),
                        Mesto = c.String(maxLength: 50),
                        ZiroRacun = c.String(maxLength: 20),
                        Telefon = c.String(maxLength: 20),
                        Saradnik = c.String(maxLength: 50),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ZPrijemnica",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BR_DOC = c.String(maxLength: 20),
                        Datum = c.DateTime(nullable: false),
                        DokumentDobavljaca = c.String(maxLength: 20),
                        PartnerId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Partner", t => t.PartnerId)
                .Index(t => t.PartnerId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Username = c.String(nullable: false, maxLength: 50),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZPrijemnica", "PartnerId", "dbo.Partner");
            DropForeignKey("dbo.Artikl", "VrstaArtiklaId", "dbo.VrstaArtikla");
            DropForeignKey("dbo.SPrijemnica", "ArtiklId", "dbo.Artikl");
            DropForeignKey("dbo.PocetnoStanje", "ArtiklId", "dbo.Artikl");
            DropForeignKey("dbo.Kartica", "VrstaTroskaId", "dbo.VrstaTroska");
            DropForeignKey("dbo.SIzdatnica", "VrstaTroskaId", "dbo.VrstaTroska");
            DropForeignKey("dbo.SIzdatnica", "ArtiklId", "dbo.Artikl");
            DropForeignKey("dbo.Kartica", "VrstaDokumentaId", "dbo.VrstaDokumenta");
            DropForeignKey("dbo.Kartica", "VeterinarskiTehnicarId", "dbo.VeterinarskiTehnicar");
            DropForeignKey("dbo.ZIzdatnica", "VeterinarskiTehnicarId", "dbo.VeterinarskiTehnicar");
            DropForeignKey("dbo.Kartica", "ArtiklId", "dbo.Artikl");
            DropForeignKey("dbo.Artikl", "JedinicaMereId", "dbo.JedinicaMere");
            DropIndex("dbo.ZPrijemnica", new[] { "PartnerId" });
            DropIndex("dbo.SPrijemnica", new[] { "ArtiklId" });
            DropIndex("dbo.PocetnoStanje", new[] { "ArtiklId" });
            DropIndex("dbo.SIzdatnica", new[] { "VrstaTroskaId" });
            DropIndex("dbo.SIzdatnica", new[] { "ArtiklId" });
            DropIndex("dbo.ZIzdatnica", new[] { "VeterinarskiTehnicarId" });
            DropIndex("dbo.Kartica", new[] { "VrstaTroskaId" });
            DropIndex("dbo.Kartica", new[] { "ArtiklId" });
            DropIndex("dbo.Kartica", new[] { "VeterinarskiTehnicarId" });
            DropIndex("dbo.Kartica", new[] { "VrstaDokumentaId" });
            DropIndex("dbo.Artikl", new[] { "VrstaArtiklaId" });
            DropIndex("dbo.Artikl", new[] { "JedinicaMereId" });
            DropTable("dbo.Users");
            DropTable("dbo.ZPrijemnica");
            DropTable("dbo.Partner");
            DropTable("dbo.VrstaArtikla");
            DropTable("dbo.SPrijemnica");
            DropTable("dbo.PocetnoStanje");
            DropTable("dbo.SIzdatnica");
            DropTable("dbo.VrstaTroska");
            DropTable("dbo.VrstaDokumenta");
            DropTable("dbo.ZIzdatnica");
            DropTable("dbo.VeterinarskiTehnicar");
            DropTable("dbo.Kartica");
            DropTable("dbo.JedinicaMere");
            DropTable("dbo.Artikl");
        }
    }
}
