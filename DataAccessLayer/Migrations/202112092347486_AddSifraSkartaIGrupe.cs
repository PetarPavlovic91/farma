﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSifraSkartaIGrupe : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GrupeSifaraSkartas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SifraGrupe = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SifraSkartas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sifra = c.String(),
                        Name = c.String(),
                        GrupeSifaraSkartaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GrupeSifaraSkartas", t => t.GrupeSifaraSkartaId)
                .Index(t => t.GrupeSifaraSkartaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SifraSkartas", "GrupeSifaraSkartaId", "dbo.GrupeSifaraSkartas");
            DropIndex("dbo.SifraSkartas", new[] { "GrupeSifaraSkartaId" });
            DropTable("dbo.SifraSkartas");
            DropTable("dbo.GrupeSifaraSkartas");
        }
    }
}
