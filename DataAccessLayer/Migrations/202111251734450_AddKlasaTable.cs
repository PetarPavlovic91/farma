﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddKlasaTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Klasa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        POL = c.String(),
                        T_GRLA = c.String(),
                        MBR_GRLA = c.String(),
                        GODINA = c.Int(nullable: false),
                        KLASA = c.String(),
                        KL_P = c.String(),
                        EKST = c.Int(),
                        RED = c.String(),
                        DAT_SMOTRE = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Klasa");
        }
    }
}
