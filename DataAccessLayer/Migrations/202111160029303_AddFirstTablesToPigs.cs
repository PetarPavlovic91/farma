﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFirstTablesToPigs : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica");
            CreateTable(
                "dbo.Ciklusxx",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CIKLUS = c.Int(nullable: false),
                        KrmacaId = c.Int(nullable: false),
                        Nerast1Id = c.Int(),
                        DAT_PRIP = c.DateTime(),
                        PV = c.String(),
                        Nerast2Id = c.Int(),
                        DAT_PRIP2 = c.DateTime(),
                        PV2 = c.String(),
                        Nerast3Id = c.Int(),
                        DAT_PRIP3 = c.DateTime(),
                        PV3 = c.String(),
                        DAT_PRAS = c.DateTime(),
                        ZIVO = c.Int(nullable: false),
                        MRTVO = c.Int(nullable: false),
                        ANOMALIJE = c.Int(nullable: false),
                        DODA = c.Int(nullable: false),
                        ODUZ = c.Int(nullable: false),
                        GUBICI = c.Int(nullable: false),
                        DAT_ZAL = c.DateTime(),
                        ZALUC = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .ForeignKey("dbo.Nerast", t => t.Nerast1Id)
                .ForeignKey("dbo.Nerast", t => t.Nerast2Id)
                .ForeignKey("dbo.Nerast", t => t.Nerast3Id)
                .Index(t => t.KrmacaId)
                .Index(t => t.Nerast1Id)
                .Index(t => t.Nerast2Id)
                .Index(t => t.Nerast3Id);
            
            CreateTable(
                "dbo.Krmaca",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MBR_KRM = c.String(),
                        VLA = c.String(),
                        ODG = c.String(),
                        RASA = c.String(),
                        T_KRM = c.String(),
                        GN = c.Int(nullable: false),
                        PARIT = c.Int(nullable: false),
                        CIKLUS = c.Int(nullable: false),
                        FAZA = c.Int(nullable: false),
                        PRED_FAZA = c.Int(nullable: false),
                        DAT_ROD = c.DateTime(),
                        MBR_OCA = c.String(),
                        MBR_MAJKE = c.String(),
                        IZ_LEGLA = c.String(),
                        KLASA = c.String(),
                        EKST = c.String(),
                        BR_SISA = c.Int(nullable: false),
                        NA_FARM_OD = c.DateTime(),
                        GEN_MAR = c.String(),
                        HALOTAN = c.String(),
                        SI1F = c.String(),
                        SI1 = c.String(),
                        SI2 = c.String(),
                        SI3 = c.String(),
                        IEZ = c.Int(nullable: false),
                        UK_ZIVO = c.Int(nullable: false),
                        UK_MRTVO = c.Int(nullable: false),
                        UK_ZIVO_Z = c.Int(nullable: false),
                        UK_ZAL = c.Int(nullable: false),
                        UK_LAKT = c.Int(nullable: false),
                        UK_PRAZNI = c.Int(nullable: false),
                        UK_PROIZ_D = c.Int(nullable: false),
                        D_SKART = c.DateTime(),
                        RAZL_SK = c.Int(nullable: false),
                        HBBROJ = c.String(),
                        RBBROJ = c.String(),
                        NBBROJ = c.String(),
                        GRUPA = c.String(),
                        MARKICA = c.String(),
                        NAPOMENA = c.String(),
                        KL_P = c.String(),
                        TETOVIRK10 = c.String(),
                        GNO = c.String(),
                        T_OCA10 = c.String(),
                        GNM = c.String(),
                        T_MAJKE10 = c.String(),
                        OPIS = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Nerast",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MBR_NER = c.String(),
                        VLA = c.String(),
                        ODG = c.String(),
                        RASA = c.String(),
                        T_NER = c.String(),
                        DAT_ROD = c.DateTime(),
                        MBR_OCA = c.String(),
                        MBR_MAJKE = c.String(),
                        IZ_LEGLA = c.String(),
                        KLASA = c.String(),
                        EKST = c.String(),
                        BR_SISA = c.String(),
                        NA_FARM_OD = c.DateTime(),
                        GEN_MAR = c.String(),
                        HALOTAN = c.String(),
                        SI1F = c.String(),
                        SI1 = c.String(),
                        SI2 = c.String(),
                        SI3 = c.String(),
                        D_P_SKOKA = c.DateTime(),
                        PV = c.String(),
                        UK_ZIVO = c.Int(nullable: false),
                        UK_MRTVO = c.Int(nullable: false),
                        BR_OS = c.Int(nullable: false),
                        BR_OP = c.Int(nullable: false),
                        D_SKART = c.DateTime(),
                        RAZL_SK = c.Int(nullable: false),
                        MARKICA = c.String(),
                        HBBROJ = c.String(),
                        RBBROJ = c.String(),
                        BR_DOZVOLE = c.String(),
                        NAPOMENA = c.String(),
                        TETOVIRN10 = c.String(),
                        GNO = c.String(),
                        T_OCA10 = c.String(),
                        GNM = c.String(),
                        T_MAJKE10 = c.String(),
                        OPIS = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Firma",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FARMS = c.String(),
                        FIRMA = c.String(),
                        FIRMA_C = c.String(),
                        GN = c.Int(nullable: false),
                        ZAPRIMLJEN = c.String(),
                        POCETAK = c.String(),
                        DEBUGMODE = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Izvod",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GODINA = c.Int(nullable: false),
                        POL = c.String(),
                        MBR_GRLA = c.String(),
                        T_GRLA = c.String(),
                        DAT_IZVODA = c.DateTime(),
                        DOPUNA = c.String(),
                        HRN_BROJ = c.String(),
                        D_ZAD_PR = c.DateTime(),
                        RBP = c.Int(nullable: false),
                        RBS = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica");
            DropForeignKey("dbo.Ciklusxx", "Nerast3Id", "dbo.Nerast");
            DropForeignKey("dbo.Ciklusxx", "Nerast2Id", "dbo.Nerast");
            DropForeignKey("dbo.Ciklusxx", "Nerast1Id", "dbo.Nerast");
            DropForeignKey("dbo.Ciklusxx", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Ciklusxx", new[] { "Nerast3Id" });
            DropIndex("dbo.Ciklusxx", new[] { "Nerast2Id" });
            DropIndex("dbo.Ciklusxx", new[] { "Nerast1Id" });
            DropIndex("dbo.Ciklusxx", new[] { "KrmacaId" });
            DropTable("dbo.Izvod");
            DropTable("dbo.Firma");
            DropTable("dbo.Nerast");
            DropTable("dbo.Krmaca");
            DropTable("dbo.Ciklusxx");
            AddForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica", "Id", cascadeDelete: true);
        }
    }
}
