﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationsipPedigrePorekloToKNP : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pedigre", "KrmacaId", c => c.Int());
            AddColumn("dbo.Pedigre", "NerastId", c => c.Int());
            AddColumn("dbo.Pedigre", "PraseId", c => c.Int());
            AddColumn("dbo.Poreklo", "KrmacaId", c => c.Int());
            AddColumn("dbo.Poreklo", "NerastId", c => c.Int());
            AddColumn("dbo.Poreklo", "PraseId", c => c.Int());
            CreateIndex("dbo.Pedigre", "KrmacaId");
            CreateIndex("dbo.Pedigre", "NerastId");
            CreateIndex("dbo.Pedigre", "PraseId");
            CreateIndex("dbo.Poreklo", "KrmacaId");
            CreateIndex("dbo.Poreklo", "NerastId");
            CreateIndex("dbo.Poreklo", "PraseId");
            AddForeignKey("dbo.Pedigre", "KrmacaId", "dbo.Krmaca", "Id");
            AddForeignKey("dbo.Pedigre", "NerastId", "dbo.Nerast", "Id");
            AddForeignKey("dbo.Poreklo", "KrmacaId", "dbo.Krmaca", "Id");
            AddForeignKey("dbo.Poreklo", "NerastId", "dbo.Nerast", "Id");
            AddForeignKey("dbo.Poreklo", "PraseId", "dbo.Prase", "Id");
            AddForeignKey("dbo.Pedigre", "PraseId", "dbo.Prase", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pedigre", "PraseId", "dbo.Prase");
            DropForeignKey("dbo.Poreklo", "PraseId", "dbo.Prase");
            DropForeignKey("dbo.Poreklo", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Poreklo", "KrmacaId", "dbo.Krmaca");
            DropForeignKey("dbo.Pedigre", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Pedigre", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Poreklo", new[] { "PraseId" });
            DropIndex("dbo.Poreklo", new[] { "NerastId" });
            DropIndex("dbo.Poreklo", new[] { "KrmacaId" });
            DropIndex("dbo.Pedigre", new[] { "PraseId" });
            DropIndex("dbo.Pedigre", new[] { "NerastId" });
            DropIndex("dbo.Pedigre", new[] { "KrmacaId" });
            DropColumn("dbo.Poreklo", "PraseId");
            DropColumn("dbo.Poreklo", "NerastId");
            DropColumn("dbo.Poreklo", "KrmacaId");
            DropColumn("dbo.Pedigre", "PraseId");
            DropColumn("dbo.Pedigre", "NerastId");
            DropColumn("dbo.Pedigre", "KrmacaId");
        }
    }
}
