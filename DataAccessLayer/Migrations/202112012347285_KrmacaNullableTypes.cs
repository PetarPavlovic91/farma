﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KrmacaNullableTypes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Krmaca", "GN", c => c.Int());
            AlterColumn("dbo.Krmaca", "PARIT", c => c.Int());
            AlterColumn("dbo.Krmaca", "CIKLUS", c => c.Int());
            AlterColumn("dbo.Krmaca", "FAZA", c => c.Int());
            AlterColumn("dbo.Krmaca", "PRED_FAZA", c => c.Int());
            AlterColumn("dbo.Krmaca", "BR_SISA", c => c.Int());
            AlterColumn("dbo.Krmaca", "IEZ", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_ZIVO", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_MRTVO", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_ZIVO_Z", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_ZAL", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_LAKT", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_PRAZNI", c => c.Int());
            AlterColumn("dbo.Krmaca", "UK_PROIZ_D", c => c.Int());
            AlterColumn("dbo.Krmaca", "RAZL_SK", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Krmaca", "RAZL_SK", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_PROIZ_D", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_PRAZNI", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_LAKT", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_ZAL", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_ZIVO_Z", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_MRTVO", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "UK_ZIVO", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "IEZ", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "BR_SISA", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "PRED_FAZA", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "FAZA", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "CIKLUS", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "PARIT", c => c.Int(nullable: false));
            AlterColumn("dbo.Krmaca", "GN", c => c.Int(nullable: false));
        }
    }
}
