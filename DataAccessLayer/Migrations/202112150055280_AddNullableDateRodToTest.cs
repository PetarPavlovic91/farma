﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNullableDateRodToTest : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Test", "DAT_ROD", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "DAT_ROD", c => c.DateTime(nullable: false));
        }
    }
}
