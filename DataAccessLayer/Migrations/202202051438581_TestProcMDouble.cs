﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestProcMDouble : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Test", "PROC_M", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "PROC_M", c => c.Int());
        }
    }
}
