﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VETtoSIzdatnica : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ZIzdatnica", new[] { "VeterinarskiTehnicarId" });
            AddColumn("dbo.SIzdatnica", "VeterinarskiTehnicarId", c => c.Int());
            AlterColumn("dbo.ZIzdatnica", "VeterinarskiTehnicarId", c => c.Int());
            CreateIndex("dbo.SIzdatnica", "VeterinarskiTehnicarId");
            CreateIndex("dbo.ZIzdatnica", "VeterinarskiTehnicarId");
            AddForeignKey("dbo.SIzdatnica", "VeterinarskiTehnicarId", "dbo.VeterinarskiTehnicar", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SIzdatnica", "VeterinarskiTehnicarId", "dbo.VeterinarskiTehnicar");
            DropIndex("dbo.ZIzdatnica", new[] { "VeterinarskiTehnicarId" });
            DropIndex("dbo.SIzdatnica", new[] { "VeterinarskiTehnicarId" });
            AlterColumn("dbo.ZIzdatnica", "VeterinarskiTehnicarId", c => c.Int(nullable: false));
            DropColumn("dbo.SIzdatnica", "VeterinarskiTehnicarId");
            CreateIndex("dbo.ZIzdatnica", "VeterinarskiTehnicarId");
        }
    }
}
