﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Suprasnost : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Suprasnosts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PrvaSuprasnost = c.Boolean(nullable: false),
                        DrugaSuprasnost = c.Boolean(nullable: false),
                        NijeSuprasna = c.Boolean(nullable: false),
                        PripustId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pripust", t => t.PripustId)
                .Index(t => t.PripustId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Suprasnosts", "PripustId", "dbo.Pripust");
            DropIndex("dbo.Suprasnosts", new[] { "PripustId" });
            DropTable("dbo.Suprasnosts");
        }
    }
}
