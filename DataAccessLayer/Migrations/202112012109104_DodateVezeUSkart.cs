﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DodateVezeUSkart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Skart", "NerastId", c => c.Int());
            AddColumn("dbo.Skart", "KrmacaId", c => c.Int());
            CreateIndex("dbo.Skart", "NerastId");
            CreateIndex("dbo.Skart", "KrmacaId");
            AddForeignKey("dbo.Skart", "KrmacaId", "dbo.Krmaca", "Id");
            AddForeignKey("dbo.Skart", "NerastId", "dbo.Nerast", "Id");
            DropColumn("dbo.Skart", "POL");
            DropColumn("dbo.Skart", "T_GRLA");
            DropColumn("dbo.Skart", "MBR_GRLA");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Skart", "MBR_GRLA", c => c.String());
            AddColumn("dbo.Skart", "T_GRLA", c => c.String());
            AddColumn("dbo.Skart", "POL", c => c.String());
            DropForeignKey("dbo.Skart", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Skart", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Skart", new[] { "KrmacaId" });
            DropIndex("dbo.Skart", new[] { "NerastId" });
            DropColumn("dbo.Skart", "KrmacaId");
            DropColumn("dbo.Skart", "NerastId");
        }
    }
}
