﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CeoPripustToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pripust", "RAZL_P", c => c.String());
            AlterColumn("dbo.Pripust", "OCENA", c => c.String());
            AlterColumn("dbo.Pripust", "OCENA2", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pripust", "OCENA2", c => c.Int());
            AlterColumn("dbo.Pripust", "OCENA", c => c.Int());
            AlterColumn("dbo.Pripust", "RAZL_P", c => c.Int());
        }
    }
}
