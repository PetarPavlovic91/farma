﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ekstToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Krmaca", "EKST", c => c.Int());
            AlterColumn("dbo.Nerast", "EKST", c => c.Int());
            AlterColumn("dbo.Odabir", "EKST", c => c.Int());
            AlterColumn("dbo.Test", "EKST", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "EKST", c => c.String());
            AlterColumn("dbo.Odabir", "EKST", c => c.String());
            AlterColumn("dbo.Nerast", "EKST", c => c.String());
            AlterColumn("dbo.Krmaca", "EKST", c => c.String());
        }
    }
}
