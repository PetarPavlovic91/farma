﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OptionalNerastInPripusti : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Pripust", new[] { "Nerast2Id" });
            DropIndex("dbo.Pripust", new[] { "NerastId" });
            AlterColumn("dbo.Pripust", "Nerast2Id", c => c.Int());
            AlterColumn("dbo.Pripust", "NerastId", c => c.Int());
            CreateIndex("dbo.Pripust", "Nerast2Id");
            CreateIndex("dbo.Pripust", "NerastId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Pripust", new[] { "NerastId" });
            DropIndex("dbo.Pripust", new[] { "Nerast2Id" });
            AlterColumn("dbo.Pripust", "NerastId", c => c.Int(nullable: false));
            AlterColumn("dbo.Pripust", "Nerast2Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Pripust", "NerastId");
            CreateIndex("dbo.Pripust", "Nerast2Id");
        }
    }
}
