﻿// <auto-generated />
namespace DataAccessLayer.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class CeoPripustToString : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CeoPripustToString));
        
        string IMigrationMetadata.Id
        {
            get { return "202112030037028_CeoPripustToString"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
