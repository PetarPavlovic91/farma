﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParitxxOsemAltered : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Paritxx", "OSEM", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Paritxx", "OSEM", c => c.Int());
        }
    }
}
