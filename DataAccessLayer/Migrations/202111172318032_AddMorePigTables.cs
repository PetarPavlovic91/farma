﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMorePigTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sifra",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SIFRA = c.String(),
                        NAZIV = c.String(),
                        OPIS = c.String(),
                        DUZINA = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SifreF",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SIFARNIK = c.String(),
                        SIFRA = c.String(),
                        NAZIV = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Skart",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        D_SKART = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        POL = c.String(),
                        T_GRLA = c.String(),
                        MBR_GRLA = c.String(),
                        RAZL_SK = c.String(),
                        REFK = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TabKor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        POL = c.String(),
                        TEZINA = c.Int(nullable: false),
                        PRIRAST = c.Int(nullable: false),
                        DLSL = c.Int(nullable: false),
                        DBSL = c.Int(nullable: false),
                        KONV = c.Double(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tabnaz",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TEZINA = c.Int(nullable: false),
                        PRIRAST = c.Int(nullable: false),
                        DLSL = c.Int(nullable: false),
                        DBSL = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tabner",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TEZINA = c.Int(nullable: false),
                        PRIRAST = c.Int(nullable: false),
                        KONVERZ = c.Int(nullable: false),
                        DLSL = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Test",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        D_TESTA = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        POL = c.String(),
                        GN = c.Int(nullable: false),
                        T_TEST = c.String(),
                        VLA = c.String(),
                        ODG = c.String(),
                        RASA = c.String(),
                        MBR_TEST = c.String(),
                        DAT_ROD = c.DateTime(nullable: false),
                        MBR_OCA = c.String(),
                        MBR_MAJKE = c.String(),
                        IZ_LEGLA = c.String(),
                        MARKICA = c.String(),
                        EKST = c.Int(),
                        GEN_MAR = c.Int(),
                        HALOTAN = c.Int(),
                        U_TEST = c.Int(),
                        POC_TEZ = c.Int(),
                        BR_SISA = c.Int(),
                        TEZINA = c.Int(),
                        PRIRAST = c.Int(),
                        HRANA = c.Int(),
                        L_SL = c.Int(),
                        B_SL = c.Int(),
                        DUB_S = c.Int(),
                        DUB_M = c.Int(),
                        PROC_M = c.Int(),
                        SI1F = c.Int(),
                        SI1 = c.Int(),
                        D_SKART = c.DateTime(),
                        RAZL_SK = c.String(),
                        NAPOMENA = c.String(),
                        U_PRIPLOD = c.DateTime(),
                        TETOVIRT10 = c.Int(nullable: false),
                        GNO = c.String(),
                        T_OCA10 = c.String(),
                        GNM = c.String(),
                        T_MAJKE10 = c.String(),
                        REFK = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Test");
            DropTable("dbo.Tabner");
            DropTable("dbo.Tabnaz");
            DropTable("dbo.TabKor");
            DropTable("dbo.Skart");
            DropTable("dbo.SifreF");
            DropTable("dbo.Sifra");
        }
    }
}
