﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRelationPrasePrasenje : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prase", "PrasenjeId", c => c.Int());
            CreateIndex("dbo.Prase", "PrasenjeId");
            AddForeignKey("dbo.Prase", "PrasenjeId", "dbo.Prasenje", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prase", "PrasenjeId", "dbo.Prasenje");
            DropIndex("dbo.Prase", new[] { "PrasenjeId" });
            DropColumn("dbo.Prase", "PrasenjeId");
        }
    }
}
