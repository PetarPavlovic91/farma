﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NerastNullableTypes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Nerast", "UK_ZIVO", c => c.Int());
            AlterColumn("dbo.Nerast", "UK_MRTVO", c => c.Int());
            AlterColumn("dbo.Nerast", "BR_OS", c => c.Int());
            AlterColumn("dbo.Nerast", "BR_OP", c => c.Int());
            AlterColumn("dbo.Nerast", "RAZL_SK", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Nerast", "RAZL_SK", c => c.Int(nullable: false));
            AlterColumn("dbo.Nerast", "BR_OP", c => c.Int(nullable: false));
            AlterColumn("dbo.Nerast", "BR_OS", c => c.Int(nullable: false));
            AlterColumn("dbo.Nerast", "UK_MRTVO", c => c.Int(nullable: false));
            AlterColumn("dbo.Nerast", "UK_ZIVO", c => c.Int(nullable: false));
        }
    }
}
