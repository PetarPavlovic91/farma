﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedSIType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Krmaca", "SI1F", c => c.Int());
            AlterColumn("dbo.Krmaca", "SI1", c => c.Int());
            AlterColumn("dbo.Krmaca", "SI2", c => c.Int());
            AlterColumn("dbo.Krmaca", "SI3", c => c.Int());
            AlterColumn("dbo.Nerast", "SI1F", c => c.Int());
            AlterColumn("dbo.Nerast", "SI1", c => c.Int());
            AlterColumn("dbo.Nerast", "SI2", c => c.Int());
            AlterColumn("dbo.Nerast", "SI3", c => c.Int());
            AlterColumn("dbo.Test", "SI1F", c => c.Int());
            AlterColumn("dbo.Test", "SI1", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "SI1", c => c.String());
            AlterColumn("dbo.Test", "SI1F", c => c.String());
            AlterColumn("dbo.Nerast", "SI3", c => c.String());
            AlterColumn("dbo.Nerast", "SI2", c => c.String());
            AlterColumn("dbo.Nerast", "SI1", c => c.String());
            AlterColumn("dbo.Nerast", "SI1F", c => c.String());
            AlterColumn("dbo.Krmaca", "SI3", c => c.String());
            AlterColumn("dbo.Krmaca", "SI2", c => c.String());
            AlterColumn("dbo.Krmaca", "SI1", c => c.String());
            AlterColumn("dbo.Krmaca", "SI1F", c => c.String());
        }
    }
}
