﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BrSisaFromIntToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Krmaca", "BR_SISA", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Krmaca", "BR_SISA", c => c.Int());
        }
    }
}
