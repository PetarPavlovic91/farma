﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Hormon : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Hormons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DatumDavanjaHormona = c.DateTime(nullable: false),
                        TipHormona = c.String(),
                        KrmacaId = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .Index(t => t.KrmacaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Hormons", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Hormons", new[] { "KrmacaId" });
            DropTable("dbo.Hormons");
        }
    }
}
