﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Suprasnost_Prevedena : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Suprasnosts", "Prevedena", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Suprasnosts", "Prevedena");
        }
    }
}
