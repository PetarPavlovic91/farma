﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SIzdatnicaCascadeDeleteKartica : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica");
            AddForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica");
            AddForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica", "Id");
        }
    }
}
