﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsPrenesenaUOdabirToOdabir : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Odabir", "IsPrenesenUTest", c => c.Boolean());
            AlterColumn("dbo.Test", "EKST", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "EKST", c => c.Int());
            DropColumn("dbo.Odabir", "IsPrenesenUTest");
        }
    }
}
