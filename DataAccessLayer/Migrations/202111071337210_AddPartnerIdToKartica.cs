﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPartnerIdToKartica : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Kartica", "PartnerId", c => c.Int());
            CreateIndex("dbo.Kartica", "PartnerId");
            AddForeignKey("dbo.Kartica", "PartnerId", "dbo.Partner", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Kartica", "PartnerId", "dbo.Partner");
            DropIndex("dbo.Kartica", new[] { "PartnerId" });
            DropColumn("dbo.Kartica", "PartnerId");
        }
    }
}
