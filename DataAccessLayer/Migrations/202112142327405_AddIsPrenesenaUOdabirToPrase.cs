﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsPrenesenaUOdabirToPrase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prase", "IsPrenesenaUOdabir", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prase", "IsPrenesenaUOdabir");
        }
    }
}
