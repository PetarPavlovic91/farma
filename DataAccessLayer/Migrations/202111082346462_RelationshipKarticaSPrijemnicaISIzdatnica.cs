﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RelationshipKarticaSPrijemnicaISIzdatnica : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Kartica", "SPrijemnicaId", c => c.Int());
            AddColumn("dbo.Kartica", "SIzdatnicaId", c => c.Int());
            CreateIndex("dbo.Kartica", "SPrijemnicaId");
            CreateIndex("dbo.Kartica", "SIzdatnicaId");
            AddForeignKey("dbo.Kartica", "SIzdatnicaId", "dbo.SIzdatnica", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Kartica", "SPrijemnicaId", "dbo.SPrijemnica");
            DropForeignKey("dbo.Kartica", "SIzdatnicaId", "dbo.SIzdatnica");
            DropIndex("dbo.Kartica", new[] { "SIzdatnicaId" });
            DropIndex("dbo.Kartica", new[] { "SPrijemnicaId" });
            DropColumn("dbo.Kartica", "SIzdatnicaId");
            DropColumn("dbo.Kartica", "SPrijemnicaId");
        }
    }
}
