﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ParitxxOsemReturned : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Paritxx", "OSEM", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Paritxx", "OSEM", c => c.String());
        }
    }
}
