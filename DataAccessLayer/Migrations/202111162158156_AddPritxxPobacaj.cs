﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPritxxPobacaj : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Paritxx",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PARIT = c.Int(),
                        CIKLUS = c.Int(nullable: false),
                        FAZA = c.Int(nullable: false),
                        AKT = c.String(),
                        OBJ = c.String(),
                        BOX = c.String(),
                        RBP = c.Int(nullable: false),
                        RAZL_P = c.Int(),
                        DAT_PRIP = c.DateTime(),
                        PV = c.String(),
                        OSEM = c.Int(nullable: false),
                        DAT_PRAS = c.DateTime(),
                        ZIVO = c.Int(),
                        MRTVO = c.Int(),
                        GAJI = c.Int(),
                        DAT_ZAL = c.DateTime(),
                        ZALUC = c.Int(),
                        TEZ_ZAL = c.Int(),
                        ZDR_ST = c.String(),
                        D_HORMON = c.String(),
                        VR_HOR = c.String(),
                        D_BOLESTI = c.String(),
                        S_BOL = c.String(),
                        DAT_POBAC = c.DateTime(),
                        D_DETEKC = c.String(),
                        RBD = c.String(),
                        ZNAK = c.String(),
                        D_PREVODA = c.String(),
                        BCP = c.String(),
                        D_SKART = c.DateTime(),
                        RAZL_SK = c.Int(),
                        RBZ = c.Int(),
                        DOJARA = c.String(),
                        KrmacaId = c.Int(nullable: false),
                        NerastId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .ForeignKey("dbo.Nerast", t => t.NerastId)
                .Index(t => t.KrmacaId)
                .Index(t => t.NerastId);
            
            CreateTable(
                "dbo.Pobacaj",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DAT_POBAC = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        CIKLUS = c.Int(nullable: false),
                        REFK = c.String(),
                        KrmacaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .Index(t => t.KrmacaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pobacaj", "KrmacaId", "dbo.Krmaca");
            DropForeignKey("dbo.Paritxx", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Paritxx", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Pobacaj", new[] { "KrmacaId" });
            DropIndex("dbo.Paritxx", new[] { "NerastId" });
            DropIndex("dbo.Paritxx", new[] { "KrmacaId" });
            DropTable("dbo.Pobacaj");
            DropTable("dbo.Paritxx");
        }
    }
}
