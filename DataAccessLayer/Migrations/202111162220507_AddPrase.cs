﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPrase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Prase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        T_PRAS = c.String(),
                        POL = c.String(),
                        GN = c.Int(nullable: false),
                        DAT_ROD = c.DateTime(nullable: false),
                        IZ_LEGLA = c.String(),
                        PRIPLOD = c.Boolean(),
                        MBR_PRAS = c.String(),
                        D_SKART = c.String(),
                        RAZL_SK = c.String(),
                        S_L = c.String(),
                        S_D = c.String(),
                        PRAS_BROJ = c.String(),
                        MBR_PRAS1 = c.String(),
                        KrmacaId = c.Int(nullable: false),
                        NerastId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .ForeignKey("dbo.Nerast", t => t.NerastId)
                .Index(t => t.KrmacaId)
                .Index(t => t.NerastId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prase", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Prase", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Prase", new[] { "NerastId" });
            DropIndex("dbo.Prase", new[] { "KrmacaId" });
            DropTable("dbo.Prase");
        }
    }
}
