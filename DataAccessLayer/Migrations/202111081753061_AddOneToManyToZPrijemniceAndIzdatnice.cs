﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOneToManyToZPrijemniceAndIzdatnice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SIzdatnica", "ZIzdatnicaId", c => c.Int(nullable: false));
            AddColumn("dbo.SPrijemnica", "ZPrijemnicaId", c => c.Int(nullable: false));
            CreateIndex("dbo.SPrijemnica", "ZPrijemnicaId");
            CreateIndex("dbo.SIzdatnica", "ZIzdatnicaId");
            AddForeignKey("dbo.SPrijemnica", "ZPrijemnicaId", "dbo.ZPrijemnica", "Id");
            AddForeignKey("dbo.SIzdatnica", "ZIzdatnicaId", "dbo.ZIzdatnica", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SIzdatnica", "ZIzdatnicaId", "dbo.ZIzdatnica");
            DropForeignKey("dbo.SPrijemnica", "ZPrijemnicaId", "dbo.ZPrijemnica");
            DropIndex("dbo.SIzdatnica", new[] { "ZIzdatnicaId" });
            DropIndex("dbo.SPrijemnica", new[] { "ZPrijemnicaId" });
            DropColumn("dbo.SPrijemnica", "ZPrijemnicaId");
            DropColumn("dbo.SIzdatnica", "ZIzdatnicaId");
        }
    }
}
