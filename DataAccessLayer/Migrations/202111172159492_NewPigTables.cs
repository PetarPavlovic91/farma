﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewPigTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pripust",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DAT_PRIP = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        CIKLUS = c.Int(nullable: false),
                        RBP = c.Int(nullable: false),
                        RAZL_P = c.Int(),
                        OBJ = c.String(),
                        BOX = c.String(),
                        PV = c.String(),
                        OSEM = c.Int(nullable: false),
                        OCENA = c.Int(),
                        PV2 = c.String(),
                        OSEM2 = c.Int(nullable: false),
                        OCENA2 = c.Int(),
                        PRIPUSNICA = c.String(),
                        PRIMEDBA = c.String(),
                        REFK = c.String(),
                        Nerast2Id = c.Int(nullable: false),
                        NerastId = c.Int(nullable: false),
                        KrmacaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .ForeignKey("dbo.Nerast", t => t.NerastId)
                .ForeignKey("dbo.Nerast", t => t.Nerast2Id)
                .Index(t => t.Nerast2Id)
                .Index(t => t.NerastId)
                .Index(t => t.KrmacaId);
            
            CreateTable(
                "dbo.Seme",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DAT_SEME = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        MARKICA = c.String(),
                        RB_UZ = c.Int(nullable: false),
                        LIBIDO = c.Int(nullable: false),
                        KOLIC = c.Int(nullable: false),
                        GUST = c.Int(nullable: false),
                        BR_U_ML = c.Int(),
                        VALO = c.Int(),
                        POKR = c.Int(),
                        P_MRT = c.Int(),
                        P_MORF = c.Int(),
                        UPOT = c.Int(),
                        RAZR = c.Int(),
                        PRIP = c.Int(),
                        KOL_RS = c.Int(),
                        DOZA = c.Int(),
                        POK_RS = c.Int(),
                        DAT_PREG = c.DateTime(),
                        POK_PS = c.Int(),
                        NAPOMENA = c.String(),
                        REFK = c.String(),
                        NerastId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nerast", t => t.NerastId)
                .Index(t => t.NerastId);
            
            CreateTable(
                "dbo.Prasenje",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DAT_PRAS = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        PARIT = c.Int(nullable: false),
                        OBJ = c.String(),
                        BOX = c.String(),
                        BAB = c.String(),
                        RAD = c.String(),
                        MAT = c.String(),
                        ZIVO = c.Int(nullable: false),
                        MRTVO = c.Int(nullable: false),
                        ZIVOZ = c.Int(),
                        UBIJ = c.Int(),
                        UGNJ = c.Int(),
                        ZAPRIM = c.Int(nullable: false),
                        UGIN = c.Int(),
                        DODATO = c.Int(nullable: false),
                        ODUZETO = c.Int(nullable: false),
                        GAJI = c.Int(nullable: false),
                        TEZ_LEG = c.Double(nullable: false),
                        ANOMALIJE = c.String(),
                        TMP_OD = c.Int(),
                        TMP_DO = c.Int(),
                        TZP_OD = c.Int(),
                        TZP_DO = c.Int(),
                        TOV_OD = c.Int(),
                        TOV_DO = c.Int(),
                        REG_PRAS = c.String(),
                        OL_P = c.String(),
                        REFK = c.String(),
                        KrmacaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId)
                .Index(t => t.KrmacaId);
            
            CreateTable(
                "dbo.Referent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        REF = c.String(),
                        LOZINKA = c.String(),
                        REFERENT = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sifrarnik",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TIP = c.Int(nullable: false),
                        SIFARNIK = c.String(),
                        NAZIV = c.String(),
                        POC = c.Int(nullable: false),
                        DUZINA = c.Int(nullable: false),
                        DUZINA_Z = c.Int(),
                        SIFRA_Z = c.String(),
                        SORT = c.String(),
                        POMOC = c.String(),
                        BAZE = c.String(),
                        TEKST = c.String(),
                        PODACI = c.String(),
                        VR_OPIS = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prasenje", "KrmacaId", "dbo.Krmaca");
            DropForeignKey("dbo.Seme", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Pripust", "Nerast2Id", "dbo.Nerast");
            DropForeignKey("dbo.Pripust", "NerastId", "dbo.Nerast");
            DropForeignKey("dbo.Pripust", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Prasenje", new[] { "KrmacaId" });
            DropIndex("dbo.Seme", new[] { "NerastId" });
            DropIndex("dbo.Pripust", new[] { "KrmacaId" });
            DropIndex("dbo.Pripust", new[] { "NerastId" });
            DropIndex("dbo.Pripust", new[] { "Nerast2Id" });
            DropTable("dbo.Sifrarnik");
            DropTable("dbo.Referent");
            DropTable("dbo.Prasenje");
            DropTable("dbo.Seme");
            DropTable("dbo.Pripust");
        }
    }
}
