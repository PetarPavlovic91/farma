﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrasenjeToNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prasenje", "ZIVO", c => c.Int());
            AlterColumn("dbo.Prasenje", "MRTVO", c => c.Int());
            AlterColumn("dbo.Prasenje", "ZAPRIM", c => c.Int());
            AlterColumn("dbo.Prasenje", "DODATO", c => c.Int());
            AlterColumn("dbo.Prasenje", "ODUZETO", c => c.Int());
            AlterColumn("dbo.Prasenje", "GAJI", c => c.Int());
            AlterColumn("dbo.Prasenje", "TEZ_LEG", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prasenje", "TEZ_LEG", c => c.Double(nullable: false));
            AlterColumn("dbo.Prasenje", "GAJI", c => c.Int(nullable: false));
            AlterColumn("dbo.Prasenje", "ODUZETO", c => c.Int(nullable: false));
            AlterColumn("dbo.Prasenje", "DODATO", c => c.Int(nullable: false));
            AlterColumn("dbo.Prasenje", "ZAPRIM", c => c.Int(nullable: false));
            AlterColumn("dbo.Prasenje", "MRTVO", c => c.Int(nullable: false));
            AlterColumn("dbo.Prasenje", "ZIVO", c => c.Int(nullable: false));
        }
    }
}
