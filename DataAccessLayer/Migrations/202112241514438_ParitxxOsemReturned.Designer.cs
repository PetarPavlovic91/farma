﻿// <auto-generated />
namespace DataAccessLayer.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class ParitxxOsemReturned : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ParitxxOsemReturned));
        
        string IMigrationMetadata.Id
        {
            get { return "202112241514438_ParitxxOsemReturned"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
