﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddZaluc : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Zaluc",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DAT_ZAL = c.DateTime(nullable: false),
                        RB = c.Int(nullable: false),
                        PARIT = c.Int(nullable: false),
                        OBJ = c.String(),
                        BOX = c.String(),
                        PZ = c.String(),
                        RAZ_PZ = c.String(),
                        ZALUC = c.Int(nullable: false),
                        TEZ_ZAL = c.Int(),
                        ZDR_ST = c.String(),
                        OL_Z = c.String(),
                        RBZ = c.Int(nullable: false),
                        DOJARA = c.String(),
                        REFK = c.String(),
                        KrmacaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Krmaca", t => t.KrmacaId, cascadeDelete: true)
                .Index(t => t.KrmacaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Zaluc", "KrmacaId", "dbo.Krmaca");
            DropIndex("dbo.Zaluc", new[] { "KrmacaId" });
            DropTable("dbo.Zaluc");
        }
    }
}
