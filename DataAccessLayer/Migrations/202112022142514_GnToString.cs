﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GnToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Krmaca", "GN", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Krmaca", "GN", c => c.Int());
        }
    }
}
