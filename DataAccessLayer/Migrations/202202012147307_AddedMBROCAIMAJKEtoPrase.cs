﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMBROCAIMAJKEtoPrase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Prase", "MBR_OCA", c => c.String());
            AddColumn("dbo.Prase", "MBR_MAJKE", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Prase", "MBR_MAJKE");
            DropColumn("dbo.Prase", "MBR_OCA");
        }
    }
}
