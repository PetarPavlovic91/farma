﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestUTestDateTime : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Test", "U_TEST", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "U_TEST", c => c.Int());
        }
    }
}
