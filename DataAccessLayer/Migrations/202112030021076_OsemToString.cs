﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OsemToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pripust", "OSEM", c => c.String());
            AlterColumn("dbo.Pripust", "OSEM2", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pripust", "OSEM2", c => c.Int());
            AlterColumn("dbo.Pripust", "OSEM", c => c.Int());
        }
    }
}
