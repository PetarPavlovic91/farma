﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeysToSkart : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Skart", "PraseId", c => c.Int());
            AddColumn("dbo.Skart", "OdabirId", c => c.Int());
            AddColumn("dbo.Skart", "TestId", c => c.Int());
            CreateIndex("dbo.Skart", "PraseId");
            CreateIndex("dbo.Skart", "OdabirId");
            CreateIndex("dbo.Skart", "TestId");
            AddForeignKey("dbo.Skart", "OdabirId", "dbo.Odabir", "Id");
            AddForeignKey("dbo.Skart", "PraseId", "dbo.Prase", "Id");
            AddForeignKey("dbo.Skart", "TestId", "dbo.Test", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Skart", "TestId", "dbo.Test");
            DropForeignKey("dbo.Skart", "PraseId", "dbo.Prase");
            DropForeignKey("dbo.Skart", "OdabirId", "dbo.Odabir");
            DropIndex("dbo.Skart", new[] { "TestId" });
            DropIndex("dbo.Skart", new[] { "OdabirId" });
            DropIndex("dbo.Skart", new[] { "PraseId" });
            DropColumn("dbo.Skart", "TestId");
            DropColumn("dbo.Skart", "OdabirId");
            DropColumn("dbo.Skart", "PraseId");
        }
    }
}
