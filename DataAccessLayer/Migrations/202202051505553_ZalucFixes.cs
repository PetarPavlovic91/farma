﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ZalucFixes : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Test", "POC_TEZ", c => c.Double());
            AlterColumn("dbo.Test", "TEZINA", c => c.Double());
            AlterColumn("dbo.Test", "PRIRAST", c => c.Double());
            AlterColumn("dbo.Zaluc", "TEZ_ZAL", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Zaluc", "TEZ_ZAL", c => c.Int());
            AlterColumn("dbo.Test", "PRIRAST", c => c.Int());
            AlterColumn("dbo.Test", "TEZINA", c => c.Int());
            AlterColumn("dbo.Test", "POC_TEZ", c => c.Int());
        }
    }
}
