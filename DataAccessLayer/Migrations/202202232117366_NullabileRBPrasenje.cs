﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NullabileRBPrasenje : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Prasenje", "RB", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Prasenje", "RB", c => c.Int(nullable: false));
        }
    }
}
