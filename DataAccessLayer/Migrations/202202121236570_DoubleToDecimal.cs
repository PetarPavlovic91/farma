﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DoubleToDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Artikl", "GR_NAB", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Artikl", "PocetnoStanje", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Artikl", "Ulaz", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Artikl", "Izlaz", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Artikl", "Stanje", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Artikl", "PR_CENA", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Kartica", "Kolicina", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.Kartica", "Cena", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.SPrijemnica", "Cena", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.SPrijemnica", "Kolicina", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.SIzdatnica", "Cena", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.SIzdatnica", "Kolicina", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.PocetnoStanje", "Kolicina", c => c.Decimal(precision: 10, scale: 2));
            AlterColumn("dbo.PocetnoStanje", "Cena", c => c.Decimal(precision: 10, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PocetnoStanje", "Cena", c => c.Double());
            AlterColumn("dbo.PocetnoStanje", "Kolicina", c => c.Double());
            AlterColumn("dbo.SIzdatnica", "Kolicina", c => c.Double());
            AlterColumn("dbo.SIzdatnica", "Cena", c => c.Double());
            AlterColumn("dbo.SPrijemnica", "Kolicina", c => c.Double());
            AlterColumn("dbo.SPrijemnica", "Cena", c => c.Double());
            AlterColumn("dbo.Kartica", "Cena", c => c.Double());
            AlterColumn("dbo.Kartica", "Kolicina", c => c.Double());
            AlterColumn("dbo.Artikl", "PR_CENA", c => c.Double());
            AlterColumn("dbo.Artikl", "Stanje", c => c.Double());
            AlterColumn("dbo.Artikl", "Izlaz", c => c.Double());
            AlterColumn("dbo.Artikl", "Ulaz", c => c.Double());
            AlterColumn("dbo.Artikl", "PocetnoStanje", c => c.Double());
            AlterColumn("dbo.Artikl", "GR_NAB", c => c.Double());
        }
    }
}
