﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveSifreSkarta : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SifraSkartas", "GrupeSifaraSkartaId", "dbo.GrupeSifaraSkartas");
            DropIndex("dbo.SifraSkartas", new[] { "GrupeSifaraSkartaId" });
            DropTable("dbo.GrupeSifaraSkartas");
            DropTable("dbo.SifraSkartas");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SifraSkartas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sifra = c.String(),
                        Name = c.String(),
                        GrupeSifaraSkartaId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.GrupeSifaraSkartas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SifraGrupe = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.SifraSkartas", "GrupeSifaraSkartaId");
            AddForeignKey("dbo.SifraSkartas", "GrupeSifaraSkartaId", "dbo.GrupeSifaraSkartas", "Id");
        }
    }
}
