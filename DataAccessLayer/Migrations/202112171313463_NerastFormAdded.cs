﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NerastFormAdded : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Test", "BR_SISA", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Test", "BR_SISA", c => c.Int());
        }
    }
}
