﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Uginuca : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Uginucas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Kategorija = c.String(),
                        Razlog = c.String(),
                        Komada = c.Int(nullable: false),
                        MestoRodjenja = c.String(),
                        DatumUginuca = c.DateTime(nullable: false),
                        NerastId = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Nerast", t => t.NerastId)
                .Index(t => t.NerastId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Uginucas", "NerastId", "dbo.Nerast");
            DropIndex("dbo.Uginucas", new[] { "NerastId" });
            DropTable("dbo.Uginucas");
        }
    }
}
