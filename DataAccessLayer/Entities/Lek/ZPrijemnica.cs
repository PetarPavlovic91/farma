﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("ZPrijemnica")]
    public class ZPrijemnica : BaseObject
    {
        public ZPrijemnica()
        {
            SPrijemnice = new HashSet<SPrijemnica>();
        }
        public string BR_DOC { get; set; }
        public DateTime Datum { get; set; }
        public string DokumentDobavljaca { get; set; }

        public int PartnerId { get; set; }
        public virtual Partner Partner { get; set; }

        public virtual ICollection<SPrijemnica> SPrijemnice { get; set; }
    }
}
