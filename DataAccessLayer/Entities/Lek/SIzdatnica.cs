﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("SIzdatnica")]
    public class SIzdatnica : BaseObject
    {
        public SIzdatnica()
        {
            Kartice = new HashSet<Kartica>();
        }
        public string BR_DOC { get; set; }
        public double? RB { get; set; }
        public decimal? Cena { get; set; }
        public decimal? Kolicina { get; set; }

        public int ArtiklId { get; set; }
        public virtual Artikl Artikl { get; set; }
        public int VrstaTroskaId { get; set; }
        public virtual VrstaTroska VrstaTroska { get; set; }

        public int? VeterinarskiTehnicarId { get; set; }
        public virtual VeterinarskiTehnicar VeterinarskiTehnicar { get; set; }

        public int ZIzdatnicaId { get; set; }
        public virtual ZIzdatnica ZIzdatnica { get; set; }

        public virtual ICollection<Kartica> Kartice { get; set; }
    }
}
