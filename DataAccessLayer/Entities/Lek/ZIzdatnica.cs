﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("ZIzdatnica")]
    public class ZIzdatnica : BaseObject
    {
        public ZIzdatnica()
        {
            SIzdatnice = new HashSet<SIzdatnica>();
        }
        public string BR_DOC { get; set; }
        public DateTime Datum { get; set; }
        public DateTime DatumTrebovanja { get; set; }
        public string BR_TREB { get; set; }

        public int? VeterinarskiTehnicarId { get; set; }
        public virtual VeterinarskiTehnicar VeterinarskiTehnicar { get; set; }
        public virtual ICollection<SIzdatnica> SIzdatnice { get; set; }

    }
}
