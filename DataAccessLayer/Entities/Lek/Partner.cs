﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("Partner")]
    public class Partner : BaseObject
    {
        public Partner()
        {
            ZPrijemnice = new HashSet<ZPrijemnica>();
            Kartice = new HashSet<Kartica>();   
        }
        public string SIFPP { get; set; }
        public string Naziv { get; set; }
        public string Adresa { get; set; }
        public string Mesto { get; set; }
        public string ZiroRacun { get; set; }
        public string Telefon { get; set; }
        public string Saradnik { get; set; }

        public virtual ICollection<ZPrijemnica> ZPrijemnice { get; set; }
        public virtual ICollection<Kartica> Kartice { get; set; }
    }
}
