﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("SPrijemnica")]
    public class SPrijemnica : BaseObject
    {
        public SPrijemnica()
        {
            Kartice = new HashSet<Kartica>();
        }
        public string BR_DOC { get; set; }
        public double? RB { get; set; }
        public decimal? Cena { get; set; }
        public decimal? Kolicina { get; set; }

        public int ArtiklId { get; set; }
        public virtual Artikl Artikl { get; set; }

        public virtual int ZPrijemnicaId { get; set; }
        public virtual ZPrijemnica ZPrijemnica { get; set; }

        public virtual ICollection<Kartica> Kartice { get; set; }
    }
}
