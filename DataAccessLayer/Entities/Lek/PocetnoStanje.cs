﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("PocetnoStanje")]
    public class PocetnoStanje : BaseObject
    {
        public DateTime Datum { get; set; }
        public double? RB { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Cena { get; set; }

        public int ArtiklId { get; set; }
        public virtual Artikl Artikl { get; set; }
    }
}
