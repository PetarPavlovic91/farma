﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("VrstaTroska")]
    public class VrstaTroska : BaseObject
    {
        public VrstaTroska()
        {
            Kartice = new HashSet<Kartica>();
            SIzdatnice = new HashSet<SIzdatnica>();
        }
        public string VT { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<Kartica> Kartice { get; set; }
        public virtual ICollection<SIzdatnica> SIzdatnice { get; set; }
    }
}
