﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("Artikl")]
    public class Artikl : BaseObject
    {
        public Artikl()
        {
            Kartice = new HashSet<Kartica>();
            PocetnaStanja = new HashSet<PocetnoStanje>();
            SIzdatnice = new HashSet<SIzdatnica>();
            SPrijemnice = new HashSet<SPrijemnica>();
        }
        public string SifraArtikla { get; set; }
        public string Naziv { get; set; }
        public decimal? GR_NAB { get; set; }
        public decimal? PocetnoStanje { get; set; }
        public decimal? Ulaz { get; set; }
        public decimal? Izlaz { get; set; }
        public decimal? Stanje { get; set; }
        public decimal? PR_CENA { get; set; }
        public int JedinicaMereId { get; set; }
        public virtual JedinicaMere JedinicaMere { get; set; }
        public int VrstaArtiklaId { get; set; }
        public virtual VrstaArtikla VrstaArtikla { get; set; }

        public virtual ICollection<Kartica> Kartice { get; set; }
        public virtual ICollection<PocetnoStanje> PocetnaStanja { get; set; }
        public virtual ICollection<SIzdatnica> SIzdatnice { get; set; }
        public virtual ICollection<SPrijemnica> SPrijemnice { get; set; }
    }
}
