﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("VrstaArtikla")]
    public class VrstaArtikla : BaseObject
    {
        public VrstaArtikla()
        {
            Artikli = new HashSet<Artikl>();
        }
        public string VGP { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<Artikl> Artikli { get; set; }
    }
}
