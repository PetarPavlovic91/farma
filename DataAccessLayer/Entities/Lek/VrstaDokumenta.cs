﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("VrstaDokumenta")]
    public class VrstaDokumenta : BaseObject
    {
        public VrstaDokumenta()
        {
            Kartice = new HashSet<Kartica>();
        }
        public string VD { get; set; }
        public string Naziv { get; set; }

        public virtual ICollection<Kartica> Kartice { get; set; }
    }
}
