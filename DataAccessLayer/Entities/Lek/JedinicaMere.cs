﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("JedinicaMere")]
    public class JedinicaMere : BaseObject
    {
        public JedinicaMere()
        {
            Artikli = new HashSet<Artikl>();
        }
        public string Naziv { get; set; }
        public virtual ICollection<Artikl> Artikli { get; set; }
    }
}
