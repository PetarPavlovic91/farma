﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("Kartica")]
    public class Kartica : BaseObject
    {
        public DateTime Datum { get; set; }
        public string BR_DOK { get; set; }
        //ULAZ_IZLAZ
        public string U_I { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Cena { get; set; }
        //public string Veza { get; set; }
        public int VrstaDokumentaId { get; set; }
        public virtual VrstaDokumenta VrstaDokumenta { get; set; }
        public int? VeterinarskiTehnicarId { get; set; }
        public virtual VeterinarskiTehnicar VeterinarskiTehnicar { get; set; }
        public int ArtiklId { get; set; }
        public virtual Artikl Artikl { get; set; }
        public int? VrstaTroskaId { get; set; }
        public virtual VrstaTroska VrstaTroska { get; set; }
        public int? PartnerId { get; set; }
        public virtual Partner Partner { get; set; }

        public int? SPrijemnicaId { get; set; }
        public virtual SPrijemnica SPrijemnica { get; set; }

        public int? SIzdatnicaId { get; set; }
        public virtual SIzdatnica SIzdatnica { get; set; }
    }
}
