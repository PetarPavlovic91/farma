﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
    [Table("VeterinarskiTehnicar")]
    public class VeterinarskiTehnicar : BaseObject
    {
        public VeterinarskiTehnicar()
        {
            Kartice = new HashSet<Kartica>();
            ZIzdatnice = new HashSet<ZIzdatnica>();
            SIzdatnice = new HashSet<SIzdatnica>();
        }
        public string VET { get; set; }
        public string Naziv { get; set; }
        public string Mesto { get; set; }
        public string Ulica { get; set; }
        public string Telefon { get; set; }
        public virtual ICollection<Kartica> Kartice { get; set; }
        public virtual ICollection<ZIzdatnica> ZIzdatnice { get; set; }
        public virtual ICollection<SIzdatnica> SIzdatnice { get; set; }
    }
}
