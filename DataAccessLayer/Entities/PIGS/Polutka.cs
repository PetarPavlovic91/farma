﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Polutka")]
    public class Polutka : BaseObject
    {
        public double? TEZ_NAKR_T { get; set; }
        public double? TEZ_TOPLE { get; set; }
        public double? PMP1 { get; set; }
        public double? PMP2 { get; set; }
        public double? PMP3 { get; set; }
        public double? PMP4 { get; set; }
        public double? PMP5 { get; set; }
        public double? PMP6 { get; set; }
        public double? PMP7 { get; set; }
        public double? PMP8 { get; set; }
        public double? PMP9 { get; set; }
        public double? PMP10 { get; set; }
        public double? PMP11 { get; set; }
        public double? PMP12 { get; set; }
        public double? PMP13 { get; set; }
        public double? PMP14 { get; set; }
        public double? PMP15 { get; set; }
        public double? PMP16 { get; set; }
    }
}
