﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Sifrarnik")]
    public class Sifrarnik : BaseObject
    {
        public int? TIP { get; set; }
        public string SIFARNIK { get; set; }
        public string NAZIV { get; set; }
        public int? POC { get; set; }
        public int? DUZINA { get; set; }
        public int? DUZINA_Z { get; set; }
        public string SIFRA_Z { get; set; }
        public string SORT { get; set; }
        public string POMOC { get; set; }
        public string BAZE { get; set; }
        public string TEKST { get; set; }
        public string PODACI { get; set; }
        public string VR_OPIS { get; set; }
    }
}
