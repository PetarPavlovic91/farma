﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Ciklusxx")]
    public class Ciklusxx : BaseObject
    {
        public int? CIKLUS { get; set; }
        public int KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }

        public int? Nerast1Id { get; set; }
        public virtual Nerast Nerast1 { get; set; }
        public DateTime? DAT_PRIP { get; set; }
        public string PV { get; set; }

        public int? Nerast2Id { get; set; }
        public virtual Nerast Nerast2 { get; set; }
        public DateTime? DAT_PRIP2 { get; set; }
        public string PV2 { get; set; }

        public int? Nerast3Id { get; set; }
        public virtual Nerast Nerast3 { get; set; }
        public DateTime? DAT_PRIP3 { get; set; }
        public string PV3 { get; set; }

        public DateTime? DAT_PRAS { get; set; }
        public int? ZIVO { get; set; }
        public int? MRTVO { get; set; }
        public int? ANOMALIJE { get; set; }
        public int? DODA { get; set; }
        public int? ODUZ { get; set; }
        public int? GUBICI { get; set; }
        public DateTime? DAT_ZAL { get; set; }
        public int? ZALUC { get; set; }
    }
}
