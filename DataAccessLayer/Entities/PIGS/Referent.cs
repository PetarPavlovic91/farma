﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Referent")]
    public class Referent : BaseObject
    {
        public string REF { get; set; }
        public string LOZINKA { get; set; }
        public string REFERENT { get; set; }
    }
}
