﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Firma")]
    public class Firma : BaseObject
    {
        public string FARMS { get; set; }
        public string FIRMA { get; set; }
        public string FIRMA_C { get; set; }
        public int? GN { get; set; }
        public string ZAPRIMLJEN { get; set; }
        public string POCETAK { get; set; }
        public string DEBUGMODE { get; set; }
    }
}
