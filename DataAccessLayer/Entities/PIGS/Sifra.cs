﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Sifra")]
    public class Sifra : BaseObject
    {
        public string SIFRA { get; set; }
        public string NAZIV { get; set; }
        public string OPIS { get; set; }
        public int? DUZINA { get; set; }
    }
}
