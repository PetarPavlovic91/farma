﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Tabnaz")]
    public class Tabnaz : BaseObject
    {
        public int? TEZINA { get; set; }
        public int? PRIRAST { get; set; }
        public int? DLSL { get; set; }
        public int? DBSL { get; set; }
    }
}
