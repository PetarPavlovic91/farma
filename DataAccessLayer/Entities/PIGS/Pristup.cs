﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Pripust")]
    public class Pripust : BaseObject
    {
        public Pripust()
        {
            Suprasnosti = new HashSet<Suprasnost>();
        }
        public DateTime DAT_PRIP { get; set; }
        public int? RB { get; set; }
        public int? CIKLUS { get; set; }
        public int? RBP { get; set; }
        public string RAZL_P { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string PV { get; set; }
        public string OSEM { get; set; }
        public string OCENA { get; set; }
        public string PV2 { get; set; }
        public string OSEM2 { get; set; }
        public string OCENA2 { get; set; }
        public string PRIPUSNICA { get; set; }
        public string PRIMEDBA { get; set; }
        public string REFK { get; set; }

        public int? Nerast2Id { get; set; }
        public virtual Nerast Nerast2 { get; set; }
        public int? NerastId { get; set; }
        public virtual Nerast Nerast { get; set; }
        public int KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }

        public virtual ICollection<Suprasnost> Suprasnosti { get; set; }
    }
}
