﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Zaluc")]
    public class Zaluc : BaseObject
    {
        public DateTime DAT_ZAL { get; set; }
        public int? RB { get; set; }
        public int? PARIT { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string PZ { get; set; }
        public string RAZ_PZ { get; set; }
        public int? ZALUC { get; set; }
        public double? TEZ_ZAL { get; set; }
        public string ZDR_ST { get; set; }
        public string OL_Z { get; set; }
        public int? RBZ { get; set; }
        public string DOJARA { get; set; }
        public string REFK { get; set; }
        public int KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }
    }
}
