﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Izvod")]
    public class Izvod : BaseObject
    {
        public int? GODINA { get; set; }
        public string POL { get; set; }
        public string MBR_GRLA { get; set; }
        public string T_GRLA { get; set; }
        public DateTime? DAT_IZVODA { get; set; }
        public string DOPUNA { get; set; }
        public string HRN_BROJ { get; set; }
        public DateTime? D_ZAD_PR { get; set; }
        public int? RBP { get; set; }
        public int? RBS { get; set; }
    }
}
