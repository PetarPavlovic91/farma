﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    public class Hormon : BaseObject
    {
        public DateTime DatumDavanjaHormona { get; set; }
        public string TipHormona { get; set; }

        public int? KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }
    }
}
