﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    public class Uginuca : BaseObject
    {
        public string Kategorija { get; set; }
        public string Razlog { get; set; }
        public int Komada { get; set; }
        public string MestoRodjenja { get; set; }
        public DateTime DatumUginuca { get; set; }

        public int? NerastId { get; set; }
        public virtual Nerast Nerast { get; set; }
    }
}
