﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Krmaca")]
    public class Krmaca : BaseObject
    {
        public Krmaca()
        {
            Ciklusxxs = new HashSet<Ciklusxx>();
            Pobacaji = new HashSet<Pobacaj>();
            Paritxxs = new HashSet<Paritxx>();
            Prasad = new HashSet<Prase>();
            Prasenja = new HashSet<Prasenje>();
            Pripusti = new HashSet<Pripust>();
            Zalucs = new HashSet<Zaluc>();
            Skartovi = new HashSet<Skart>();
            Pedigre = new HashSet<Pedigre>();
            Poreklo = new HashSet<Poreklo>();
            Hormoni = new HashSet<Hormon>();
        }
        public string MBR_KRM { get; set; }
        public string VLA { get; set; }
        public string ODG { get; set; } 
        public string RASA { get; set; }
        public string T_KRM { get; set; }
        public string GN { get; set; }
        public int? PARIT { get; set; }
        public int? CIKLUS { get; set; }
        public int? FAZA { get; set; }
        public int? PRED_FAZA { get; set; }
        public DateTime? DAT_ROD { get; set; }
        public string MBR_OCA { get; set; }
        public string MBR_MAJKE { get; set; }
        public string IZ_LEGLA { get; set; }
        public string KLASA { get; set; }
        public int? EKST { get; set; }
        public string BR_SISA { get; set; }
        public DateTime? NA_FARM_OD { get; set; }
        public string GEN_MAR { get; set; }
        public string HALOTAN { get; set; }
        public int? SI1F { get; set; }
        public int? SI1 { get; set; }
        public int? SI2 { get; set; }
        public int? SI3 { get; set; }
        public int? IEZ { get; set; }
        public int? UK_ZIVO { get; set; }
        public int? UK_MRTVO { get; set; }
        public int? UK_ZIVO_Z { get; set; }
        public int? UK_ZAL { get; set; }
        public int? UK_LAKT { get; set; }
        public int? UK_PRAZNI { get; set; }
        public int? UK_PROIZ_D { get; set; }
        public DateTime? D_SKART { get; set; }
        public int? RAZL_SK { get; set; }
        public string HBBROJ { get; set; }
        public string RBBROJ { get; set; }
        public string NBBROJ { get; set; }
        public string GRUPA { get; set; }
        public string MARKICA { get; set; }
        public string NAPOMENA { get; set; }
        public string KL_P { get; set; }
        public string TETOVIRK10 { get; set; }
        public string GNO { get; set; }
        public string T_OCA10 { get; set; }
        public string GNM { get; set; }
        public string T_MAJKE10 { get; set; }
        public string OPIS { get; set; }
        public virtual ICollection<Ciklusxx> Ciklusxxs { get; set; }
        public virtual ICollection<Pobacaj> Pobacaji { get; set; }
        public virtual ICollection<Paritxx> Paritxxs { get; set; }
        public virtual ICollection<Prase> Prasad { get; set; }
        public virtual ICollection<Prasenje> Prasenja { get; set; }
        public virtual ICollection<Pripust> Pripusti { get; set; }
        public virtual ICollection<Zaluc> Zalucs { get; set; }
        public virtual ICollection<Skart> Skartovi { get; set; }
        public virtual ICollection<Pedigre> Pedigre { get; set; }
        public virtual ICollection<Poreklo> Poreklo { get; set; }
        public virtual ICollection<Hormon> Hormoni { get; set; }


        public int? MajkaId { get; set; }
        public virtual Krmaca Majka { get; set; }

        public int? OtacId { get; set; }
        public virtual Nerast Otac { get; set; }


        public int? OBabaId { get; set; }
        public virtual Krmaca OBaba { get; set; }

        public int? ODedaId { get; set; }
        public virtual Nerast ODeda { get; set; }

        public int? MBabaId { get; set; }
        public virtual Krmaca MBaba { get; set; }

        public int? MDedaId { get; set; }
        public virtual Nerast MDeda { get; set; }

        public int? OD_PrababaId { get; set; }
        public virtual Krmaca OD_Prababa { get; set; }

        public int? OD_PradedaId { get; set; }
        public virtual Nerast OD_Pradeda { get; set; }

        public int? OB_PrababaId { get; set; }
        public virtual Krmaca OB_Prababa { get; set; }

        public int? OB_PradedaId { get; set; }
        public virtual Nerast OB_Pradeda { get; set; }

        public int? MD_PrababaId { get; set; }
        public virtual Krmaca MD_Prababa { get; set; }

        public int? MD_PradedaId { get; set; }
        public virtual Nerast MD_Pradeda { get; set; }

        public int? MB_PrababaId { get; set; }
        public virtual Krmaca MB_Prababa { get; set; }

        public int? MB_PradedaId { get; set; }
        public virtual Nerast MB_Pradeda { get; set; }
    }
}
