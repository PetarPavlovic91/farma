﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("SifreF")]
    public class SifreF : BaseObject
    {
        public string SIFARNIK { get; set; }
        public string SIFRA { get; set; }
        public string NAZIV { get; set; }
    }
}
