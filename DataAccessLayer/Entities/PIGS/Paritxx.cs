﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Paritxx")]
    public class Paritxx : BaseObject
    {
        public int? PARIT { get; set; }
        public int? CIKLUS { get; set; }
        public int? FAZA { get; set; }
        public string AKT { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public int? RBP { get; set; }
        public int? RAZL_P { get; set; }
        public DateTime? DAT_PRIP { get; set; }
        public string PV { get; set; }
        public int? OSEM { get; set; }
        public DateTime? DAT_PRAS { get; set; }
        public int? ZIVO { get; set; }
        public int? MRTVO { get; set; }
        public int? GAJI { get; set; }
        public DateTime? DAT_ZAL { get; set; }
        public int? ZALUC { get; set; }
        public int? TEZ_ZAL { get; set; }
        public string ZDR_ST { get; set; }
        public string D_HORMON { get; set; }
        public string VR_HOR { get; set; }
        public string D_BOLESTI { get; set; }
        public string S_BOL { get; set; }
        public DateTime? DAT_POBAC { get; set; }
        public string D_DETEKC { get; set; }
        public string RBD { get; set; }
        public string ZNAK { get; set; }
        public string D_PREVODA { get; set; }
        public string BCP { get; set; }
        public DateTime? D_SKART { get; set; }
        public int? RAZL_SK { get; set; }
        public int? RBZ { get; set; }
        public string DOJARA { get; set; }

        public int KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }

        public int NerastId { get; set; }
        public virtual Nerast Nerast { get; set; }
    }
}
