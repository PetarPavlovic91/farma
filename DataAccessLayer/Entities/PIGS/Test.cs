﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Test")]
    public class Test : BaseObject
    {
        public Test()
        {
            Pedigre = new HashSet<Pedigre>();
            Poreklo = new HashSet<Poreklo>();
            Skartovi = new HashSet<Skart>();
        }
        public DateTime D_TESTA { get; set; }
        public int? RB { get; set; }
        public string POL { get; set; }
        public string GN { get; set; }
        public string T_TEST { get; set; }
        public string VLA { get; set; }
        public string ODG { get; set; }
        public string RASA { get; set; }
        public string MBR_TEST { get; set; }
        public DateTime? DAT_ROD { get; set; }
        public string MBR_OCA { get; set; }
        public string MBR_MAJKE { get; set; }
        public string IZ_LEGLA { get; set; }
        public string MARKICA { get; set; }
        public int? EKST { get; set; }
        public string GEN_MAR { get; set; }
        public string HALOTAN { get; set; }
        public DateTime? U_TEST { get; set; }
        public double? POC_TEZ { get; set; }
        public string BR_SISA { get; set; }
        public double? TEZINA { get; set; }
        public double? PRIRAST { get; set; }
        public int? HRANA { get; set; }
        public int? L_SL { get; set; }
        public int? B_SL { get; set; }
        public int? DUB_S { get; set; }
        public int? DUB_M { get; set; }
        public double? PROC_M { get; set; }
        public int? SI1F { get; set; }
        public int? SI1 { get; set; }
        public DateTime? D_SKART { get; set; }
        public string RAZL_SK { get; set; }
        public string NAPOMENA { get; set; }
        public DateTime? U_PRIPLOD { get; set; }
        public string TETOVIRT10 { get; set; }
        public string GNO { get; set; }
        public string T_OCA10 { get; set; }
        public string GNM { get; set; }
        public string T_MAJKE10 { get; set; }
        public string REFK { get; set; }
        public virtual ICollection<Pedigre> Pedigre { get; set; }
        public virtual ICollection<Poreklo> Poreklo { get; set; }
        public virtual ICollection<Skart> Skartovi { get; set; }
    }
}
