﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    public class Suprasnost : BaseObject
    {
        public bool PrvaSuprasnost { get; set; }
        public bool DrugaSuprasnost { get; set; }
        public bool NijeSuprasna { get; set; }
        public bool Prevedena { get; set; }

        public int PripustId { get; set; }
        public virtual Pripust Pripust { get; set; }
    }
}
