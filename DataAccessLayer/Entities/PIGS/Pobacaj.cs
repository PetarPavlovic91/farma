﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Pobacaj")]
    public class Pobacaj : BaseObject
    {
        public DateTime DAT_POBAC { get; set; }
        public int? RB { get; set; }
        public int? CIKLUS { get; set; }
        public string REFK { get; set; }

        public int KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }
    }
}
