﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Prase")]
    public class Prase : BaseObject
    {
        public Prase()
        {
            Skartovi = new HashSet<Skart>();
        }
        public string T_PRAS { get; set; }
        public string POL { get; set; }
        public string GN { get; set; }
        public DateTime DAT_ROD { get; set; }
        public string IZ_LEGLA { get; set; }
        public bool? PRIPLOD { get; set; }
        public string MBR_PRAS { get; set; }
        public string D_SKART { get; set; }
        public string RAZL_SK { get; set; }
        public string S_L { get; set; }
        public string S_D { get; set; }
        public string PRAS_BROJ { get; set; }
        public string MBR_PRAS1 { get; set; }
        public bool? IsPrenesenaUOdabir { get; set; }
        public string MBR_OCA { get; set; }
        public string MBR_MAJKE { get; set; }

        public int KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }

        public int NerastId { get; set; }
        public virtual Nerast Nerast { get; set; }

        public int? PrasenjeId { get; set; }
        public virtual Prasenje Prasenje { get; set; }


        public virtual ICollection<Skart> Skartovi { get; set; }

    }
}
