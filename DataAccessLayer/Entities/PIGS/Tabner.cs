﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Tabner")]
    public class Tabner : BaseObject
    {
        public int? TEZINA { get; set; }
        public int? PRIRAST { get; set; }
        public int? KONVERZ { get; set; }
        public int? DLSL { get; set; }
    }
}
