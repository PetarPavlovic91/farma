﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Skart")]
    public class Skart : BaseObject
    {
        public DateTime D_SKART { get; set; }
        public int? RB { get; set; }
        public string RAZL_SK { get; set; }
        public string REFK { get; set; }

        public int? NerastId { get; set; }
        public virtual Nerast Nerast { get; set; }
        public int? KrmacaId { get; set; }
        public virtual Krmaca Krmaca { get; set; }
        public int? PraseId { get; set; }
        public virtual Prase Prase { get; set; }
        public int? OdabirId { get; set; }
        public virtual Odabir Odabir { get; set; }
        public int? TestId { get; set; }
        public virtual Test Test { get; set; }
    }
}
