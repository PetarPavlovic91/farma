﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities.PIGS
{
    [Table("Klasa")]
    public class Klasa : BaseObject
    {
        public string POL { get; set; }
        public string T_GRLA { get; set; }
        public string MBR_GRLA { get; set; }
        public int? GODINA { get; set; }
        public string KLASA { get; set; }
        public string KL_P { get; set; }
        public int? EKST { get; set; }
        public string RED { get; set; }
        public DateTime? DAT_SMOTRE { get; set; }
    }
}
