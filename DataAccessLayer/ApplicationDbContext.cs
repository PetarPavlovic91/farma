﻿using DataAccessLayer.Entities;
using DataAccessLayer.Entities.PIGS;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString())
        {

        }

        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseObject && (x.State == EntityState.Added || x.State == EntityState.Modified));
            foreach (var item in entities)
            {
                if (((BaseObject)item.Entity).DateCreated == DateTime.MinValue)
                {
                    ((BaseObject)item.Entity).DateCreated = DateTime.Now;
                }
                ((BaseObject)item.Entity).DateModified = DateTime.Now;
            }

            return base.SaveChanges();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Artikl> Artikl { get; set; }
        public DbSet<VrstaArtikla> VrstaArtikla { get; set; }
        public DbSet<JedinicaMere> JedinicaMere { get; set; }
        public DbSet<VeterinarskiTehnicar> VeterinarskiTehnicar { get; set; }
        public DbSet<VrstaDokumenta> VrstaDokumenta { get; set; }
        public DbSet<VrstaTroska> VrstaTroska { get; set; }
        public DbSet<Kartica> Kartica { get; set; }
        public DbSet<Partner> Partner { get; set; }
        public DbSet<PocetnoStanje> PocetnoStanje { get; set; }
        public DbSet<SIzdatnica> SIzdatnica { get; set; }
        public DbSet<SPrijemnica> SPrijemnica { get; set; }
        public DbSet<ZIzdatnica> ZIzdatnica { get; set; }
        public DbSet<ZPrijemnica> ZPrijemnica { get; set; }
        public DbSet<Ciklusxx> Ciklusxx { get; set; }
        public DbSet<Firma> Firma { get; set; }
        public DbSet<Izvod> Izvod { get; set; }
        public DbSet<Krmaca> Krmaca { get; set; }
        public DbSet<Nerast> Nerast { get; set; }
        public DbSet<Odabir> Odabir { get; set; }
        public DbSet<Paritxx> Paritxx { get; set; }
        public DbSet<Pobacaj> Pobacaj { get; set; }
        public DbSet<Polutka> Polutka { get; set; }
        public DbSet<Prase> Prase { get; set; }
        public DbSet<Prasenje> Prasenje { get; set; }
        public DbSet<Pripust> Pripust { get; set; }
        public DbSet<Referent> Referent { get; set; }
        public DbSet<Seme> Seme { get; set; }
        public DbSet<Sifrarnik> Sifrarnik { get; set; }
        public DbSet<Sifra> Sifra { get; set; }
        public DbSet<SifreF> SifreF { get; set; }
        public DbSet<Skart> Skart { get; set; }
        public DbSet<TabKor> TabKor { get; set; }
        public DbSet<Tabnaz> Tabnaz { get; set; }
        public DbSet<Tabner> Tabner { get; set; }
        public DbSet<Test> Test { get; set; }
        public DbSet<Zaluc> Zaluc { get; set; }
        public DbSet<Pedigre> Pedigre { get; set; }
        public DbSet<Poreklo> Poreklo { get; set; }
        public DbSet<Klasa> Klasa { get; set; }
        public DbSet<Suprasnost> Suprasnost { get; set; }
        public DbSet<Hormon> Hormon { get; set; }
        public DbSet<Uginuca> Uginuca { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            //Property Configurations
            modelBuilder.Entity<User>()
                    .Property(s => s.Username)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<JedinicaMere>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(50)
                    .IsRequired();

            modelBuilder.Entity<VrstaArtikla>()
                    .Property(s => s.VGP)
                    .HasMaxLength(10);

            modelBuilder.Entity<VrstaArtikla>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(100);

            modelBuilder.Entity<Artikl>()
                    .Property(s => s.SifraArtikla)
                    .HasMaxLength(10);

            modelBuilder.Entity<Artikl>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(100);

            modelBuilder.Entity<Artikl>()
                    .HasRequired(x => x.VrstaArtikla)
                    .WithMany(x => x.Artikli)
                    .HasForeignKey(x => x.VrstaArtiklaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Artikl>()
                    .HasRequired(x => x.JedinicaMere)
                    .WithMany(x => x.Artikli)
                    .HasForeignKey(x => x.JedinicaMereId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<VrstaDokumenta>()
                    .Property(s => s.VD)
                    .HasMaxLength(10);

            modelBuilder.Entity<VrstaDokumenta>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(50);

            modelBuilder.Entity<VeterinarskiTehnicar>()
                    .Property(s => s.VET)
                    .HasMaxLength(10);
            modelBuilder.Entity<VeterinarskiTehnicar>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(100);
            modelBuilder.Entity<VeterinarskiTehnicar>()
                    .Property(s => s.Mesto)
                    .HasMaxLength(100);
            modelBuilder.Entity<VeterinarskiTehnicar>()
                    .Property(s => s.Ulica)
                    .HasMaxLength(100);
            modelBuilder.Entity<VeterinarskiTehnicar>()
                    .Property(s => s.Telefon)
                    .HasMaxLength(20);
            modelBuilder.Entity<VrstaTroska>()
                    .Property(s => s.VT)
                    .HasMaxLength(10);
            modelBuilder.Entity<VrstaTroska>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(50);
            modelBuilder.Entity<Kartica>()
                    .Property(s => s.U_I)
                    .HasMaxLength(1);
            modelBuilder.Entity<Kartica>()
                    .Property(s => s.BR_DOK)
                    .HasMaxLength(20);

            modelBuilder.Entity<Kartica>()
                    .HasRequired(x => x.Artikl)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.ArtiklId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kartica>()
                    .HasOptional(x => x.VeterinarskiTehnicar)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.VeterinarskiTehnicarId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kartica>()
                    .HasOptional(x => x.VrstaTroska)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.VrstaTroskaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kartica>()
                    .HasRequired(x => x.VrstaDokumenta)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.VrstaDokumentaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Partner>()
                    .Property(s => s.SIFPP)
                    .HasMaxLength(10);
            modelBuilder.Entity<Partner>()
                    .Property(s => s.Naziv)
                    .HasMaxLength(100);
            modelBuilder.Entity<Partner>()
                    .Property(s => s.Adresa)
                    .HasMaxLength(100);
            modelBuilder.Entity<Partner>()
                    .Property(s => s.Mesto)
                    .HasMaxLength(50);
            modelBuilder.Entity<Partner>()
                    .Property(s => s.ZiroRacun)
                    .HasMaxLength(20);
            modelBuilder.Entity<Partner>()
                    .Property(s => s.Telefon)
                    .HasMaxLength(20);
            modelBuilder.Entity<Partner>()
                    .Property(s => s.Saradnik)
                    .HasMaxLength(50);

            modelBuilder.Entity<PocetnoStanje>()
                    .HasRequired(x => x.Artikl)
                    .WithMany(x => x.PocetnaStanja)
                    .HasForeignKey(x => x.ArtiklId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIzdatnica>()
                    .Property(s => s.BR_DOC)
                    .HasMaxLength(20);
            modelBuilder.Entity<SPrijemnica>()
                    .Property(s => s.BR_DOC)
                    .HasMaxLength(20);


            modelBuilder.Entity<SIzdatnica>()
                    .HasRequired(x => x.Artikl)
                    .WithMany(x => x.SIzdatnice)
                    .HasForeignKey(x => x.ArtiklId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIzdatnica>()
                    .HasRequired(x => x.VrstaTroska)
                    .WithMany(x => x.SIzdatnice)
                    .HasForeignKey(x => x.VrstaTroskaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<SPrijemnica>()
                    .HasRequired(x => x.Artikl)
                    .WithMany(x => x.SPrijemnice)
                    .HasForeignKey(x => x.ArtiklId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<ZIzdatnica>()
                    .Property(s => s.BR_DOC)
                    .HasMaxLength(20);
            modelBuilder.Entity<ZPrijemnica>()
                    .Property(s => s.DokumentDobavljaca)
                    .HasMaxLength(20);


            modelBuilder.Entity<ZIzdatnica>()
                    .HasOptional(x => x.VeterinarskiTehnicar)
                    .WithMany(x => x.ZIzdatnice)
                    .HasForeignKey(x => x.VeterinarskiTehnicarId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<ZPrijemnica>()
                    .Property(s => s.BR_DOC)
                    .HasMaxLength(20);
            modelBuilder.Entity<ZIzdatnica>()
                    .Property(s => s.BR_TREB)
                    .HasMaxLength(20);


            modelBuilder.Entity<ZPrijemnica>()
                    .HasRequired(x => x.Partner)
                    .WithMany(x => x.ZPrijemnice)
                    .HasForeignKey(x => x.PartnerId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kartica>()
                    .HasOptional(x => x.Partner)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.PartnerId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<SPrijemnica>()
                    .HasRequired(x => x.ZPrijemnica)
                    .WithMany(x => x.SPrijemnice)
                    .HasForeignKey(x => x.ZPrijemnicaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIzdatnica>()
                    .HasRequired(x => x.ZIzdatnica)
                    .WithMany(x => x.SIzdatnice)
                    .HasForeignKey(x => x.ZIzdatnicaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Kartica>()
                    .HasOptional(x => x.SIzdatnica)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.SIzdatnicaId)
                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<Kartica>()
                    .HasOptional(x => x.SPrijemnica)
                    .WithMany(x => x.Kartice)
                    .HasForeignKey(x => x.SPrijemnicaId)
                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<Ciklusxx>()
                    .HasOptional(x => x.Nerast1)
                    .WithMany(x => x.Ciklusxxs1)
                    .HasForeignKey(x => x.Nerast1Id)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ciklusxx>()
                    .HasOptional(x => x.Nerast2)
                    .WithMany(x => x.Ciklusxxs2)
                    .HasForeignKey(x => x.Nerast2Id)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ciklusxx>()
                    .HasOptional(x => x.Nerast3)
                    .WithMany(x => x.Ciklusxxs3)
                    .HasForeignKey(x => x.Nerast3Id)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ciklusxx>()
                    .HasRequired(x => x.Krmaca)
                    .WithMany(x => x.Ciklusxxs)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pobacaj>()
                    .HasRequired(x => x.Krmaca)
                    .WithMany(x => x.Pobacaji)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paritxx>()
                    .HasRequired(x => x.Krmaca)
                    .WithMany(x => x.Paritxxs)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Paritxx>()
                    .HasRequired(x => x.Nerast)
                    .WithMany(x => x.Paritxxs)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prase>()
                    .HasRequired(x => x.Nerast)
                    .WithMany(x => x.Prasad)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prase>()
                    .HasRequired(x => x.Krmaca)
                    .WithMany(x => x.Prasad)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prasenje>()
                    .HasRequired(x => x.Krmaca)
                    .WithMany(x => x.Prasenja)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pripust>()
                    .HasRequired(x => x.Krmaca)
                    .WithMany(x => x.Pripusti)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pripust>()
                    .HasOptional(x => x.Nerast)
                    .WithMany(x => x.Pripusti)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pripust>()
                    .HasOptional(x => x.Nerast2)
                    .WithMany(x => x.Pripusti2)
                    .HasForeignKey(x => x.Nerast2Id)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Seme>()
                    .HasRequired(x => x.Nerast)
                    .WithMany(x => x.Semena)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Skart>()
                    .HasOptional(x => x.Nerast)
                    .WithMany(x => x.Skartovi)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Skart>()
                    .HasOptional(x => x.Krmaca)
                    .WithMany(x => x.Skartovi)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pedigre>()
                    .HasOptional(x => x.Krmaca)
                    .WithMany(x => x.Pedigre)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pedigre>()
                    .HasOptional(x => x.Nerast)
                    .WithMany(x => x.Pedigre)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pedigre>()
                    .HasOptional(x => x.Odabir)
                    .WithMany(x => x.Pedigre)
                    .HasForeignKey(x => x.OdabirId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Poreklo>()
                    .HasOptional(x => x.Krmaca)
                    .WithMany(x => x.Poreklo)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Poreklo>()
                    .HasOptional(x => x.Nerast)
                    .WithMany(x => x.Poreklo)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Poreklo>()
                    .HasOptional(x => x.Odabir)
                    .WithMany(x => x.Poreklo)
                    .HasForeignKey(x => x.OdabirId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Poreklo>()
                    .HasOptional(x => x.Test)
                    .WithMany(x => x.Poreklo)
                    .HasForeignKey(x => x.TestId)
                    .WillCascadeOnDelete(false);


            modelBuilder.Entity<Pedigre>()
                    .HasOptional(x => x.Test)
                    .WithMany(x => x.Pedigre)
                    .HasForeignKey(x => x.TestId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Skart>()
                    .HasOptional(x => x.Test)
                    .WithMany(x => x.Skartovi)
                    .HasForeignKey(x => x.TestId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Skart>()
                    .HasOptional(x => x.Odabir)
                    .WithMany(x => x.Skartovi)
                    .HasForeignKey(x => x.OdabirId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Skart>()
                    .HasOptional(x => x.Test)
                    .WithMany(x => x.Skartovi)
                    .HasForeignKey(x => x.TestId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Prase>()
                    .HasOptional(x => x.Prasenje)
                    .WithMany(x => x.Prasad)
                    .HasForeignKey(x => x.PrasenjeId)
                    .WillCascadeOnDelete(true);

            modelBuilder.Entity<Artikl>()
                .Property(x => x.GR_NAB).HasPrecision(10, 2);
            modelBuilder.Entity<Artikl>()
                .Property(x => x.PocetnoStanje).HasPrecision(10, 2);
            modelBuilder.Entity<Artikl>()
                .Property(x => x.Ulaz).HasPrecision(10, 2);
            modelBuilder.Entity<Artikl>()
                .Property(x => x.Izlaz).HasPrecision(10, 2);
            modelBuilder.Entity<Artikl>()
                .Property(x => x.Stanje).HasPrecision(10, 2);
            modelBuilder.Entity<Artikl>()
                .Property(x => x.PR_CENA).HasPrecision(10, 2);

            modelBuilder.Entity<Kartica>()
                .Property(x => x.Kolicina).HasPrecision(10, 2);
            modelBuilder.Entity<Kartica>()
                .Property(x => x.Cena).HasPrecision(10, 2);

            modelBuilder.Entity<PocetnoStanje>()
                .Property(x => x.Kolicina).HasPrecision(10, 2);
            modelBuilder.Entity<PocetnoStanje>()
                .Property(x => x.Cena).HasPrecision(10, 2);

            modelBuilder.Entity<SIzdatnica>()
                .Property(x => x.Kolicina).HasPrecision(10, 2);
            modelBuilder.Entity<SIzdatnica>()
                .Property(x => x.Cena).HasPrecision(10, 2);

            modelBuilder.Entity<SPrijemnica>()
                .Property(x => x.Kolicina).HasPrecision(10, 2);
            modelBuilder.Entity<SPrijemnica>()
                .Property(x => x.Cena).HasPrecision(10, 2);

            modelBuilder.Entity<Suprasnost>()
                    .HasRequired(x => x.Pripust)
                    .WithMany(x => x.Suprasnosti)
                    .HasForeignKey(x => x.PripustId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Hormon>()
                    .HasOptional(x => x.Krmaca)
                    .WithMany(x => x.Hormoni)
                    .HasForeignKey(x => x.KrmacaId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Uginuca>()
                    .HasOptional(x => x.Nerast)
                    .WithMany(x => x.Uginuca)
                    .HasForeignKey(x => x.NerastId)
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIzdatnica>()
                    .HasOptional(x => x.VeterinarskiTehnicar)
                    .WithMany(x => x.SIzdatnice)
                    .HasForeignKey(x => x.VeterinarskiTehnicarId)
                    .WillCascadeOnDelete(false);
        }
    }
}
