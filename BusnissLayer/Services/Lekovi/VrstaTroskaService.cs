﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class VrstaTroskaService
    {
        public List<VrstaTroskaVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaTroska.Select(x =>
                new VrstaTroskaVM()
                {
                    Id = x.Id,
                    VT = x.VT,
                    Naziv = x.Naziv
                }).ToList();
            }
        }

        public List<VrstaTroskaShortVM> GetAllShortVM(string deoNaziva)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaTroska.Where(x => x.Naziv.Contains(deoNaziva)).Select(x =>
                new VrstaTroskaShortVM()
                {
                    VT = x.VT,
                    Naziv = x.Naziv
                }).ToList();
            }
        }
        public VrstaTroskaVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaTroska.Where(x => x.Id == id).Select(x =>
                new VrstaTroskaVM()
                {
                    Id = x.Id,
                    VT = x.VT,
                    Naziv = x.Naziv
                }).FirstOrDefault();
            }
        }
        public ReturnObject Save(VrstaTroskaVM vrstaTroskaVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (vrstaTroskaVM.Id == 0)
                    {
                        var entity = applicationDbContext.VrstaTroska.Add(
                            new VrstaTroska()
                            {
                                VT = vrstaTroskaVM.VT,
                                Naziv = vrstaTroskaVM.Naziv
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        var entity = applicationDbContext.VrstaTroska.FirstOrDefault(x => x.Id == vrstaTroskaVM.Id);
                        if (entity != null)
                        {
                            entity.VT = vrstaTroskaVM.VT;
                            entity.Naziv = vrstaTroskaVM.Naziv;
                        }
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception)
            {
                returnObject.Success = false;
                return returnObject;
            }
        }
        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.VrstaTroska.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.VrstaTroska.Remove(entity);
                        applicationDbContext.SaveChanges();
                        return new ReturnObject() { Success = true };
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }
    }
}
