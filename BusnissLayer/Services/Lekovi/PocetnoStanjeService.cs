﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class PocetnoStanjeService
    {
        public List<PocetnoStanjeVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.PocetnoStanje
                        join a in applicationDbContext.Artikl on p.ArtiklId equals a.Id
                        select new PocetnoStanjeVM()
                        {
                            Id = p.Id,
                            Kolicina = p.Kolicina,
                            Cena = p.Cena,
                            SifraArtikla = a.SifraArtikla,
                            NazivArtikla = a.Naziv,
                            Datum = p.Datum
                        }).OrderBy(x => x.NazivArtikla).ToList();
            }
        }
        public PocetnoStanjeVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.PocetnoStanje
                        join a in applicationDbContext.Artikl on p.ArtiklId equals a.Id
                        where p.Id == id
                        select new PocetnoStanjeVM()
                        {
                            Id = p.Id,
                            Kolicina = p.Kolicina,
                            Cena = p.Cena,
                            SifraArtikla = a.SifraArtikla,
                            NazivArtikla = a.Naziv,
                            Datum = p.Datum
                        }).FirstOrDefault();
            }
        }
        public ReturnObject Save(PocetnoStanjeVM pocetnoStanjeVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.SifraArtikla == pocetnoStanjeVM.SifraArtikla);
                    if (artikl == null)
                    {
                        returnObject.Success = false;
                        returnObject.Message = "Artikl nije pronadjen";
                        return returnObject;
                    }
                    if (pocetnoStanjeVM.Id == 0)
                    {
                        var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == artikl.Id);
                        if (pocetnoStanje != null)
                        {
                            returnObject.Success = false;
                            returnObject.Message = "Pocetno stanje za ovaj lek vec postoji";
                            return returnObject;
                        }
                        var entity = applicationDbContext.PocetnoStanje.Add(
                            new PocetnoStanje()
                            {
                                Kolicina = pocetnoStanjeVM.Kolicina,
                                Cena = pocetnoStanjeVM.Cena,
                                ArtiklId = artikl.Id,
                                Datum = pocetnoStanjeVM.Datum
                            });

                        var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                        {
                            artikl.PR_CENA = pocetnoStanjeVM.Cena;
                        }
                        else
                        {
                            artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanjeVM.Cena * pocetnoStanjeVM.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanjeVM.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                        }
                        applicationDbContext.SaveChanges();

                        SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                        sIzdatnicaService.AzurirajProsecneCene(entity.Datum, artikl.Id);
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        var entity = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.Id == pocetnoStanjeVM.Id);
                        if (entity != null)
                        {
                            entity.Kolicina = pocetnoStanjeVM.Kolicina;
                            entity.Cena = pocetnoStanjeVM.Cena;
                            entity.ArtiklId = artikl.Id;
                            entity.Datum = pocetnoStanjeVM.Datum;
                        }
                        var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                        {
                            artikl.PR_CENA = pocetnoStanjeVM.Cena;
                        }
                        else
                        {
                            artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanjeVM.Cena * pocetnoStanjeVM.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanjeVM.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                        }
                        applicationDbContext.SaveChanges();

                        SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                        sIzdatnicaService.AzurirajProsecneCene(entity.Datum, artikl.Id);
                        returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Message = ex.Message;
                returnObject.Success = false;
                return returnObject;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.PocetnoStanje.Remove(entity);
                        applicationDbContext.SaveChanges();


                        var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.Id == entity.ArtiklId);
                        if (artikl == null)
                        {
                            throw new Exception("Ne postoji taj artikl.");
                        }
                        var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == entity.ArtiklId);
                        if (pocetnoStanje != null)
                        {
                            artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                        }
                        else
                        {
                            if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                            {
                                artikl.PR_CENA = 0;
                            }
                            else
                            {
                                artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) - sIzdatnice.Sum(x => x.Kolicina));
                            }
                        }
                        applicationDbContext.SaveChanges();

                        SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                        sIzdatnicaService.AzurirajProsecneCene(entity.Datum, artikl.Id);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
