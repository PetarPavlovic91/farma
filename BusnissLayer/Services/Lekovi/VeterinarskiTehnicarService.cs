﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class VeterinarskiTehnicarService
    {
        public List<VeterinarskiTehnicarVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VeterinarskiTehnicar.Select(x =>
                new VeterinarskiTehnicarVM()
                {
                    Id = x.Id,
                    VET = x.VET,
                    Naziv = x.Naziv,
                    Mesto = x.Mesto,
                    Ulica = x.Ulica,
                    Telefon = x.Telefon
                }).ToList();
            }
        }

        public List<VeterinarskiTehnicarShortVM> GetAllShortVM()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VeterinarskiTehnicar.Select(x =>
                new VeterinarskiTehnicarShortVM()
                {
                    VET = x.VET,
                    Naziv = x.Naziv
                }).ToList();
            }
        }
        public VeterinarskiTehnicarVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VeterinarskiTehnicar.Where(x => x.Id == id).Select(x =>
                new VeterinarskiTehnicarVM()
                {
                    Id = x.Id,
                    VET = x.VET,
                    Naziv = x.Naziv,
                    Mesto = x.Mesto,
                    Ulica = x.Ulica,
                    Telefon = x.Telefon
                }).FirstOrDefault();
            }
        }

        public ReturnObject Save(VeterinarskiTehnicarVM veterinarskiTehnicarVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (veterinarskiTehnicarVM.Id == 0)
                    {
                        var entity = applicationDbContext.VeterinarskiTehnicar.Add(
                            new VeterinarskiTehnicar()
                            {
                                Id = veterinarskiTehnicarVM.Id,
                                VET = veterinarskiTehnicarVM.VET,
                                Naziv = veterinarskiTehnicarVM.Naziv,
                                Mesto = veterinarskiTehnicarVM.Mesto,
                                Ulica = veterinarskiTehnicarVM.Ulica,
                                Telefon = veterinarskiTehnicarVM.Telefon
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        //var entity = applicationDbContext.VeterinarskiTehnicar.FirstOrDefault(x => x.Id == veterinarskiTehnicarVM.Id);
                        //if (entity != null)
                        //{
                        //    entity.VET = veterinarskiTehnicarVM.VET;
                        //    entity.Naziv = veterinarskiTehnicarVM.Naziv;
                        //    entity.Mesto = veterinarskiTehnicarVM.Mesto;
                        //    entity.Ulica = veterinarskiTehnicarVM.Ulica;
                        //    entity.Telefon = veterinarskiTehnicarVM.Telefon;
                        //}
                        //applicationDbContext.SaveChanges();
                        //returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Success = false;
                returnObject.Message = ex.Message;
                return returnObject;
            }
        }
        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.VeterinarskiTehnicar.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.VeterinarskiTehnicar.Remove(entity);
                        applicationDbContext.SaveChanges();
                        return new ReturnObject() { Success = true };
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }
    }
}
