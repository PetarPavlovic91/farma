﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class PartnerService
    {
        public List<PartnerVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Partner.Select(x =>
                new PartnerVM()
                {
                    Id = x.Id,
                    SIFPP = x.SIFPP,
                    Naziv = x.Naziv,
                    Adresa = x.Adresa,
                    Mesto = x.Mesto,
                    ZiroRacun = x.ZiroRacun,
                    Telefon = x.Telefon,
                    Saradnik = x.Saradnik
                }).ToList();
            }
        }

        public List<PartnerShortVM> GetAllShortVM(string deoNaziva)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Partner.Where(x => x.Naziv.Contains(deoNaziva)).Select(x =>
                new PartnerShortVM()
                {
                    SIFPP = x.SIFPP,
                    Naziv = x.Naziv
                }).OrderBy(x => x.SIFPP).ToList();
            }
        }

        public PartnerVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Partner.Where(x => x.Id == id).Select(x =>
                new PartnerVM()
                {
                    Id = x.Id,
                    SIFPP = x.SIFPP,
                    Naziv = x.Naziv,
                    Adresa = x.Adresa,
                    Mesto = x.Mesto,
                    ZiroRacun = x.ZiroRacun,
                    Telefon = x.Telefon,
                    Saradnik = x.Saradnik
                }).FirstOrDefault();
            }
        }
        public ReturnObject Save(PartnerVM partnerVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (partnerVM.Id == 0)
                    {
                        if (applicationDbContext.Partner.Any(x => x.SIFPP == partnerVM.SIFPP))
                        {
                            returnObject.Success = false;
                            returnObject.Message = "Partner sa tom sifrom vec postoji.";
                            return returnObject;
                        }
                        var entity = applicationDbContext.Partner.Add(
                            new Partner()
                            {
                                SIFPP = partnerVM.SIFPP,
                                Naziv = partnerVM.Naziv,
                                Adresa = partnerVM.Adresa,
                                Mesto = partnerVM.Mesto,
                                ZiroRacun = partnerVM.ZiroRacun,
                                Telefon = partnerVM.Telefon,
                                Saradnik = partnerVM.Saradnik
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        //var entity = applicationDbContext.Partner.FirstOrDefault(x => x.Id == partnerVM.Id);
                        //if (entity != null)
                        //{
                        //    entity.SIFPP = partnerVM.SIFPP;
                        //    entity.Naziv = partnerVM.Naziv;
                        //    entity.Adresa = partnerVM.Adresa;
                        //    entity.Mesto = partnerVM.Mesto;
                        //    entity.ZiroRacun = partnerVM.ZiroRacun;
                        //    entity.Telefon = partnerVM.Telefon;
                        //    entity.Saradnik = partnerVM.Saradnik;
                        //}
                        //applicationDbContext.SaveChanges();
                        //returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception)
            {
                returnObject.Success = false;
                return returnObject;
            }
        }
        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.Partner.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.Partner.Remove(entity);
                        applicationDbContext.SaveChanges();
                        return new ReturnObject() { Success = true };
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }
    }
}
