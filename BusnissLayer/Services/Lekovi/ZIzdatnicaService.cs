﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class ZIzdatnicaService
    {
        public List<ZIzdatnicaVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zi in applicationDbContext.ZIzdatnica
                        //join v in applicationDbContext.VeterinarskiTehnicar on zi.VeterinarskiTehnicarId equals v.Id
                        select new ZIzdatnicaVM()
                        {
                            Id = zi.Id,
                            Br_dok = zi.BR_DOC,
                            Datum = zi.Datum,
                            DatumTrebovanja = zi.DatumTrebovanja,
                            //Vet = v.VET,
                            //VeterinarskiTehnicar = v.Naziv,
                            BR_TREB = zi.BR_TREB
                        }).OrderBy(x => x.Br_dok).ToList();
            }
        }

        public DateTime GetDatumByBrDok(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zi in applicationDbContext.ZIzdatnica.Where(x => x.BR_DOC == brDok)
                        select zi.DatumTrebovanja).FirstOrDefault();
            }
        }
        public ZIzdatnicaVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zi in applicationDbContext.ZIzdatnica.Where(x => x.Id == id)
                        //join v in applicationDbContext.VeterinarskiTehnicar on zi.VeterinarskiTehnicarId equals v.Id
                        select new ZIzdatnicaVM()
                        {
                            Id = zi.Id,
                            Br_dok = zi.BR_DOC,
                            Datum = zi.Datum,
                            DatumTrebovanja = zi.DatumTrebovanja,
                            //Vet = v.VET,
                            //VeterinarskiTehnicar = v.Naziv,
                            BR_TREB = zi.BR_TREB
                        }).FirstOrDefault();
            }
        }

        public List<PregledPoVrstiDokumentaVM> PregledPoVrstiDokumenta(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zPrijemnice = (from zi in applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= toDate.Date)
                                   join si in applicationDbContext.SIzdatnica on zi.Id equals si.ZIzdatnicaId
                                   join a in applicationDbContext.Artikl on si.ArtiklId equals a.Id
                                   join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                                   select new PregledPoVrstiDokumentaVM()
                                   {
                                       Ui = "I",
                                       Br_dok = zi.BR_DOC,
                                       Datum = zi.Datum,
                                       Lek = a.SifraArtikla,
                                       Naziv = a.Naziv,
                                       Jm = jm.Naziv,
                                       Kolicina = si.Kolicina,
                                       Cena = si.Cena,
                                       Vrednost = si.Kolicina * si.Cena
                                   }).ToList();

                return zPrijemnice;
            }
        }

        public List<PregledPoVeterinarskimTehnicarimaVM> PregledPoVeterinarskimTehnicarima(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zIzdatnice = (from zi in applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= toDate.Date)
                                  join si in applicationDbContext.SIzdatnica on zi.BR_DOC equals si.BR_DOC
                                  join vet in applicationDbContext.VeterinarskiTehnicar on si.VeterinarskiTehnicarId equals vet.Id
                                  join vt in applicationDbContext.VrstaTroska on si.VrstaTroskaId equals vt.Id
                                  join a in applicationDbContext.Artikl on si.ArtiklId equals a.Id
                                  join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                                  select new PregledPoVeterinarskimTehnicarimaVM()
                                  {
                                      Vet = vet.VET,
                                      Vet_tehn = vet.Naziv,
                                      Vt = vt.VT,
                                      Mesto_tr = vt.Naziv,
                                      Lek = a.SifraArtikla,
                                      Naziv = a.Naziv,
                                      Jm = jm.Naziv,
                                      Kolicina = si.Kolicina,
                                      Vrednost = si.Cena * si.Kolicina
                                  }).ToList();
                return zIzdatnice;
            }
        }

        public List<PregledPoMestimaTroskaVM> PregledPoMestimaTroska(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zIzdatnice = (from zi in applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= toDate.Date)
                                  join si in applicationDbContext.SIzdatnica on zi.BR_DOC equals si.BR_DOC
                                  join vet in applicationDbContext.VeterinarskiTehnicar on si.VeterinarskiTehnicarId equals vet.Id
                                  join vt in applicationDbContext.VrstaTroska on si.VrstaTroskaId equals vt.Id
                                  join a in applicationDbContext.Artikl on si.ArtiklId equals a.Id
                                  join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                                  select new PregledPoMestimaTroskaVM()
                                  {
                                      Vet = vet.VET,
                                      Vet_tehn = vet.Naziv,
                                      Vt = vt.VT,
                                      Mesto_tr = vt.Naziv,
                                      Lek = a.SifraArtikla,
                                      Naziv = a.Naziv,
                                      Jm = jm.Naziv,
                                      Kolicina = si.Kolicina,
                                      Vrednost = si.Cena * si.Kolicina
                                  }).ToList();
                return zIzdatnice;
            }
        }

        public ReturnObject Save(ZIzdatnicaVM zIzdatnicaVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (zIzdatnicaVM.Id == 0)
                    {
                        //var vetTehnicar = applicationDbContext.VeterinarskiTehnicar.FirstOrDefault(x => x.VET == zIzdatnicaVM.Vet);
                        //if (vetTehnicar == null)
                        //{
                        //    throw new Exception("Tehnicar sa tim Vet ne postoji.");
                        //}
                        if (applicationDbContext.ZIzdatnica.Any(x => x.BR_DOC == zIzdatnicaVM.Br_dok))
                        {
                            throw new Exception("Izdatnica sa tim Brojem Dokumenta vec postoji.");
                        }
                        var entity = applicationDbContext.ZIzdatnica.Add(
                            new ZIzdatnica()
                            {
                                BR_DOC = zIzdatnicaVM.Br_dok,
                                Datum = zIzdatnicaVM.Datum,
                                DatumTrebovanja = zIzdatnicaVM.DatumTrebovanja,
                                BR_TREB = zIzdatnicaVM.BR_TREB,
                                //VeterinarskiTehnicarId = vetTehnicar.Id
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        var entity = applicationDbContext.ZIzdatnica.FirstOrDefault(x => x.Id == zIzdatnicaVM.Id);
                        if (entity != null)
                        {
                            if (applicationDbContext.ZIzdatnica.Any(x => x.BR_DOC == zIzdatnicaVM.Br_dok && x.Id != entity.Id))
                            {
                                throw new Exception("Izdatnica sa tim Brojem Dokumenta vec postoji.");
                            }

                            if (entity.BR_DOC != zIzdatnicaVM.Br_dok)
                            {
                                entity.BR_DOC = zIzdatnicaVM.Br_dok;

                                var lekovi = applicationDbContext.SIzdatnica.Where(x => x.ZIzdatnicaId == entity.Id).ToList();
                                foreach (var item in lekovi)
                                {
                                    item.BR_DOC = zIzdatnicaVM.Br_dok;
                                }
                            }

                            entity.Datum = zIzdatnicaVM.Datum;
                            entity.DatumTrebovanja = zIzdatnicaVM.DatumTrebovanja;

                            //var vetTehnicar = applicationDbContext.VeterinarskiTehnicar.FirstOrDefault(x => x.VET == zIzdatnicaVM.Vet);
                            //if (vetTehnicar == null)
                            //{
                            //    throw new Exception("Tehnicar sa tim Vet ne postoji.");
                            //}
                            //entity.VeterinarskiTehnicarId = vetTehnicar.Id;
                            entity.BR_TREB = zIzdatnicaVM.BR_TREB;
                        }
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Success = false;
                returnObject.Message = ex.Message;
                return returnObject;
            }
        }
        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.ZIzdatnica.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        if (applicationDbContext.SIzdatnica.Any(x => x.BR_DOC == entity.BR_DOC))
                        {
                            throw new Exception("Morate prvo izbrisati sve artikle iz ove izdatnice.");
                        }
                        else
                        {
                            applicationDbContext.ZIzdatnica.Remove(entity);
                            applicationDbContext.SaveChanges();
                            return new ReturnObject() { Success = true };
                        }
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen.");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject() { Success = false, Message = ex.Message };
            }
        }
    }
}
