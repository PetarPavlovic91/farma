﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class SPrijemnicaService
    {
        public List<SPrijemnicaVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.SPrijemnica.Select(x =>
                new SPrijemnicaVM()
                {
                    Id = x.Id,
                    BR_DOC = x.BR_DOC,
                    RB = x.RB,
                    Cena = x.Cena,
                    Kolicina = x.Kolicina,
                    ArtiklId = x.ArtiklId
                }).ToList();
            }
        }

        public List<PrijemnicaReportVM> PrijemnicaReport(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from si in applicationDbContext.SPrijemnica.Where(x => x.BR_DOC == brDok)
                        join l in applicationDbContext.Artikl on si.ArtiklId equals l.Id
                        join jm in applicationDbContext.JedinicaMere on l.JedinicaMereId equals jm.Id
                        select new PrijemnicaReportVM()
                        {
                            RB = si.RB,
                            LEK = l.SifraArtikla,
                            NAZIV_LEKA = l.Naziv,
                            JM = jm.Naziv,
                            KOLICINA = si.Kolicina,
                            CENA = si.Cena
                        }).ToList();
            }
        }

        public List<SPrijemnicaLekovaPregledVM> GetAllByBrDok(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from sp in applicationDbContext.SPrijemnica.Where(x => x.BR_DOC == brDok)
                        join a in applicationDbContext.Artikl on sp.ArtiklId equals a.Id
                        join j in applicationDbContext.JedinicaMere on a.JedinicaMereId equals j.Id
                        select new SPrijemnicaLekovaPregledVM()
                        {
                            Id = sp.Id,
                            Naziv_Leka = a.Naziv,
                            Lek = a.SifraArtikla,
                            RB = sp.RB,
                            Cena = sp.Cena,
                            Kolicina = sp.Kolicina,
                            Jm = j.Naziv,
                            Br_dok = brDok
                        }).ToList();
            }
        }

        public SPrijemnicaVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.SPrijemnica.Where(x => x.Id == id).Select(x =>
                new SPrijemnicaVM()
                {
                    Id = x.Id,
                    BR_DOC = x.BR_DOC,
                    RB = x.RB,
                    Cena = x.Cena,
                    Kolicina = x.Kolicina,
                    ArtiklId = x.ArtiklId
                }).FirstOrDefault();
            }
        }

        public ReturnObject SaveWithLek(SPrijemnicaLekovaAzuriranjeVM sPrijemnicaVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.SifraArtikla == sPrijemnicaVM.Lek);
                    if (artikl == null)
                    {
                        throw new Exception("Sifra leka je pogresna");
                    }
                    var vrstaDokumenta = applicationDbContext.VrstaDokumenta.FirstOrDefault(x => x.Naziv == "Prijemnica");
                    if (vrstaDokumenta == null)
                    {
                        throw new Exception("Ne postoji vrsta dokumenta.");
                    }
                    var partner = applicationDbContext.Partner.FirstOrDefault(x => x.SIFPP == sPrijemnicaVM.Sifpp);
                    if (partner == null)
                    {
                        throw new Exception("Ne postoji partner.");
                    }
                    if (applicationDbContext.SPrijemnica.Any(x => x.BR_DOC == sPrijemnicaVM.Br_dok && x.RB == sPrijemnicaVM.RB))
                    {
                        throw new Exception("Isti RB vec postoji na ovoj prijemnici, molim promenite RB.");
                    }
                    artikl.Ulaz = artikl.Ulaz + sPrijemnicaVM.Kolicina;
                    if (artikl.PR_CENA == 0)
                    {
                        artikl.PR_CENA = sPrijemnicaVM.Cena;
                    }
                    else
                    {
                        var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artikl.Id).ToList();
                        var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artikl.Id).ToList();
                        var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == artikl.Id);
                        if (pocetnoStanje != null)
                        {
                            artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina + sPrijemnicaVM.Cena * sPrijemnicaVM.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina + sPrijemnicaVM.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                        }
                        else
                        {
                            if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                            {
                                artikl.PR_CENA = sPrijemnicaVM.Cena;
                            }
                            else
                            {
                                artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + sPrijemnicaVM.Cena * sPrijemnicaVM.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + sPrijemnicaVM.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                            }
                        }
                    }

                    var entity = applicationDbContext.SPrijemnica.Add(
                        new SPrijemnica()
                        {
                            BR_DOC = sPrijemnicaVM.Br_dok,
                            RB = sPrijemnicaVM.RB,
                            Cena = sPrijemnicaVM.Cena,
                            Kolicina = sPrijemnicaVM.Kolicina,
                            ArtiklId = artikl.Id,
                            ZPrijemnicaId = sPrijemnicaVM.ZPrijemnicaId
                        });

                    //entity.Kartice.Add(new Kartica()
                    //{
                    //    Datum = sPrijemnicaVM.Datum,
                    //    BR_DOK = sPrijemnicaVM.Br_dok,
                    //    U_I = "U",
                    //    Cena = sPrijemnicaVM.Cena,
                    //    Kolicina = sPrijemnicaVM.Kolicina,
                    //    VrstaDokumentaId = vrstaDokumenta.Id,
                    //    ArtiklId = artikl.Id,
                    //    PartnerId = partner.Id
                    //});
                    applicationDbContext.SaveChanges();

                    SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                    var izdatnicaDatum = applicationDbContext.ZPrijemnica.FirstOrDefault(x => x.Id == entity.ZPrijemnicaId);
                    sIzdatnicaService.AzurirajProsecneCene(izdatnicaDatum.Datum, artikl.Id);
                    returnObject.Id = entity.Id;
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Success = false;
                returnObject.Message = ex.Message;
                return returnObject;
            }
        }

        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.SPrijemnica.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.Id == entity.ArtiklId);
                        if (artikl == null)
                        {
                            throw new Exception("Ne postoji taj artikl. Proverite sifre artikl.");
                        }
                        artikl.Ulaz = artikl.Ulaz - entity.Kolicina;
                        applicationDbContext.SPrijemnica.Remove(entity);
                        applicationDbContext.SaveChanges();

                        var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == entity.ArtiklId);
                        if (pocetnoStanje != null)
                        {
                            artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                        }
                        else
                        {
                            if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                            {
                                artikl.PR_CENA = 0;
                            }
                            else
                            {
                                artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) - sIzdatnice.Sum(x => x.Kolicina));
                            }
                        }
                        applicationDbContext.SaveChanges();
                        SIzdatnicaService sIzdatnicaService = new SIzdatnicaService();
                        var izdatnicaDatum = applicationDbContext.ZPrijemnica.FirstOrDefault(x => x.Id == entity.ZPrijemnicaId);
                        sIzdatnicaService.AzurirajProsecneCene(izdatnicaDatum.Datum, artikl.Id);
                        return new ReturnObject() { Success = true };
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject()
                {
                    Success = false,
                    Message = ex.Message

                };
            }
        }

        public SPrijemnicaLekovaPregledVM GetByIdWithLek(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from sp in applicationDbContext.SPrijemnica.Where(x => x.Id == id)
                        join a in applicationDbContext.Artikl on sp.ArtiklId equals a.Id
                        join j in applicationDbContext.JedinicaMere on a.JedinicaMereId equals j.Id
                        select new SPrijemnicaLekovaPregledVM()
                        {
                            Id = sp.Id,
                            Naziv_Leka = a.Naziv,
                            Lek = a.SifraArtikla,
                            RB = sp.RB,
                            Cena = sp.Cena,
                            Kolicina = sp.Kolicina,
                            Jm = j.Naziv
                        }).FirstOrDefault();
            }
        }

        public double GetNewRB(string id)
        {
            var prijemnicaId = int.Parse(id);
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rbs = applicationDbContext.SPrijemnica.Where(x => x.ZPrijemnicaId == prijemnicaId).OrderByDescending(x => x.RB).Select(x => x.RB).FirstOrDefault();
                if (rbs == null)
                {
                    rbs = 0;
                }
                return (double)rbs + 1;
            }
        }

        public void CheckIfKolicinaIsValid(int artiklId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var sPrijemniceKolicina = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artiklId).Sum(x => x.Kolicina);
                var sIzdatniceKolicina = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artiklId).Sum(x => x.Kolicina);
                var pocetnoStanjeKolicina = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == artiklId);
                if ((sPrijemniceKolicina ?? 0 + pocetnoStanjeKolicina?.Kolicina ?? 0 - sIzdatniceKolicina ?? 0) < 0)
                    throw new Exception("Neuspesna operacija. Kolicina artikla bi ovim otisla u minus.");
            }
        }
    }
}
