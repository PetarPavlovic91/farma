﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class VrstaDokumentaService
    {
        public List<VrstaDokumentaVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaDokumenta.Select(x =>
                new VrstaDokumentaVM()
                {
                    Id = x.Id,
                    VD = x.VD,
                    Naziv = x.Naziv
                }).ToList();
            }
        }
        public VrstaDokumentaVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaDokumenta.Where(x => x.Id == id).Select(x =>
                new VrstaDokumentaVM()
                {
                    Id = x.Id,
                    VD = x.VD,
                    Naziv = x.Naziv
                }).FirstOrDefault();
            }
        }
        public ReturnObject Save(VrstaDokumentaVM vrstaDokumentaVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (vrstaDokumentaVM.Id == 0)
                    {
                        var entity = applicationDbContext.VrstaDokumenta.Add(
                            new VrstaDokumenta()
                            {
                                VD = vrstaDokumentaVM.VD,
                                Naziv = vrstaDokumentaVM.Naziv
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        var entity = applicationDbContext.VrstaDokumenta.FirstOrDefault(x => x.Id == vrstaDokumentaVM.Id);
                        if (entity != null)
                        {
                            entity.VD = vrstaDokumentaVM.VD;
                            entity.Naziv = vrstaDokumentaVM.Naziv;
                        }
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception)
            {
                returnObject.Success = false;
                return returnObject;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.VrstaDokumenta.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.VrstaDokumenta.Remove(entity);
                        applicationDbContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
