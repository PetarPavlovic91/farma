﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class ZPrijemnicaService
    {

        public DateTime GetDatumByBrDok(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zi in applicationDbContext.ZPrijemnica.Where(x => x.BR_DOC == brDok)
                        select zi.Datum).FirstOrDefault();
            }
        }

        public List<ZPrijemnicaLekovaPregledVM> GetAllWithPartner()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zp in applicationDbContext.ZPrijemnica
                        join p in applicationDbContext.Partner on zp.PartnerId equals p.Id
                        select new ZPrijemnicaLekovaPregledVM()
                        {
                            Id = zp.Id,
                            Br_dok = zp.BR_DOC,
                            Datum = zp.Datum,
                            Naziv = p.Naziv,
                            Sifpp = p.SIFPP,
                            Dok_dobav = zp.DokumentDobavljaca
                        }).OrderBy(x => x.Br_dok).ToList();
            }
        }

        public ZPrijemnicaLekovaPregledVM GetPartnerInfoByDoc(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zp in applicationDbContext.ZPrijemnica
                        join p in applicationDbContext.Partner on zp.PartnerId equals p.Id
                        where zp.BR_DOC == brDok
                        select new ZPrijemnicaLekovaPregledVM()
                        {
                            Id = zp.Id,
                            Br_dok = zp.BR_DOC,
                            Datum = zp.Datum,
                            Naziv = p.Naziv,
                            Sifpp = p.SIFPP,
                            Dok_dobav = zp.DokumentDobavljaca
                        }).FirstOrDefault();
            }
        }

        public List<PregledPoPoslovnimPartnerimaVM> PregledPoPoslovnimPartnerima(DateTime FromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zPrijemnice = (from zp in applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= FromDate.Date && DbFunctions.TruncateTime(x.Datum) <= toDate.Date)
                                   join p in applicationDbContext.Partner on zp.PartnerId equals p.Id
                                   select new PregledPoPoslovnimPartnerimaVM()
                                   {
                                       Vd = "Prijem",
                                       Br_dok = zp.BR_DOC,
                                       Datum = zp.Datum,
                                       Naziv = p.Naziv,
                                       Sifpp = p.SIFPP,
                                       Vrednost = applicationDbContext.SPrijemnica.Where(x => x.ZPrijemnicaId == zp.Id).GroupBy(x => x.ZPrijemnicaId)
                                                                                  .Select(g => new
                                                                                  {
                                                                                      g.Key,
                                                                                      SUM = g.Sum(s => s.Cena * s.Kolicina)
                                                                                  })
                                                                                  .FirstOrDefault()
                                                                                  .SUM

                                   }).ToList();

                if (zPrijemnice.Count() > 0)
                {
                    var ukupno = new PregledPoPoslovnimPartnerimaVM()
                    {
                        Vd = "UKUPNO",
                        Datum = toDate,
                        Vrednost = zPrijemnice.GroupBy(x => x.Vd).Select(g => new
                        {
                            g.Key,
                            SUM = g.Sum(s => s.Vrednost)
                        }).FirstOrDefault().SUM
                    };
                    zPrijemnice.Add(ukupno);
                }
                return zPrijemnice;
            }
        }

        public List<PregledPoVrstiDokumentaVM> PregledPoVrstiDokumenta(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zPrijemnice = (from zp in applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= toDate.Date)
                                   join p in applicationDbContext.Partner on zp.PartnerId equals p.Id
                                   join sp in applicationDbContext.SPrijemnica on zp.Id equals sp.ZPrijemnicaId
                                   join a in applicationDbContext.Artikl on sp.ArtiklId equals a.Id
                                   join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                                   select new PregledPoVrstiDokumentaVM()
                                   {
                                       Ui = "U",
                                       Br_dok = zp.BR_DOC,
                                       Datum = zp.Datum,
                                       Lek = a.SifraArtikla,
                                       Naziv = a.Naziv,
                                       Jm = jm.Naziv,
                                       Kolicina = sp.Kolicina,
                                       Cena = sp.Cena,
                                       Vrednost = sp.Kolicina * sp.Cena,
                                       VEZA = p.SIFPP
                                   }).ToList();

                return zPrijemnice;
            }
        }

        public List<KarticaPoslovnogPartneraVM> KarticaPoslovnogPartnera(DateTime fromDate, DateTime toDate, string sifraPartnera)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zPrijemnice = (from zp in applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= toDate.Date)
                                   join p in applicationDbContext.Partner.Where(x => x.SIFPP == sifraPartnera) on zp.PartnerId equals p.Id
                                   select new KarticaPoslovnogPartneraVM()
                                   {
                                       Vd = "Prijem",
                                       Br_dok = zp.BR_DOC,
                                       Datum = zp.Datum,
                                       Vrednost = applicationDbContext.SPrijemnica.Where(x => x.ZPrijemnicaId == zp.Id).GroupBy(x => x.ZPrijemnicaId)
                                                                                  .Select(g => new
                                                                                  {
                                                                                      g.Key,
                                                                                      SUM = g.Sum(s => s.Cena * s.Kolicina)
                                                                                  })
                                                                                  .FirstOrDefault()
                                                                                  .SUM

                                   }).ToList();

                if (zPrijemnice.Count() > 0)
                {
                    var ukupno = new KarticaPoslovnogPartneraVM()
                    {
                        Vd = "UKUPNO",
                        Datum = toDate,
                        Vrednost = zPrijemnice.GroupBy(x => x.Vd).Select(g => new
                        {
                            g.Key,
                            SUM = g.Sum(s => s.Vrednost)
                        }).FirstOrDefault().SUM
                    };
                    zPrijemnice.Add(ukupno);
                }

                return zPrijemnice;
            }
        }

        public ZPrijemnicaLekovaPregledVM GetByIdWithPartner(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from zp in applicationDbContext.ZPrijemnica.Where(x => x.Id == id)
                        join p in applicationDbContext.Partner on zp.PartnerId equals p.Id
                        select new ZPrijemnicaLekovaPregledVM()
                        {
                            Id = zp.Id,
                            Br_dok = zp.BR_DOC,
                            Datum = zp.Datum,
                            Naziv = p.Naziv,
                            Sifpp = p.SIFPP,
                            Dok_dobav = zp.DokumentDobavljaca
                        }).FirstOrDefault();
            }
        }

        public ReturnObject SaveWithPartnerSifpp(ZPrijemnicaLekovaPregledVM zPrijemnica)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var partner = applicationDbContext.Partner.FirstOrDefault(x => x.SIFPP == zPrijemnica.Sifpp);
                    if (partner == null)
                    {
                        throw new Exception("Partner not found");
                    }
                    if (zPrijemnica.Id == 0)
                    {
                        if (applicationDbContext.ZPrijemnica.Any(x => x.BR_DOC == zPrijemnica.Br_dok))
                        {
                            throw new Exception("Prijemnica sa tim Brojem Dokumenta vec postoji.");
                        }
                        var entity = applicationDbContext.ZPrijemnica.Add(
                            new ZPrijemnica()
                            {
                                BR_DOC = zPrijemnica.Br_dok,
                                Datum = zPrijemnica.Datum,
                                DokumentDobavljaca = zPrijemnica.Dok_dobav,
                                PartnerId = partner.Id
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        var entity = applicationDbContext.ZPrijemnica.FirstOrDefault(x => x.Id == zPrijemnica.Id);
                        if (entity != null)
                        {
                            if (applicationDbContext.ZPrijemnica.Any(x => x.BR_DOC == zPrijemnica.Br_dok && x.Id != entity.Id))
                            {
                                throw new Exception("Prijemnica sa tim Brojem Dokumenta vec postoji.");
                            }

                            if (entity.BR_DOC != zPrijemnica.Br_dok)
                            {
                                entity.BR_DOC = zPrijemnica.Br_dok;

                                var lekovi = applicationDbContext.SPrijemnica.Where(x => x.ZPrijemnicaId == entity.Id).ToList();
                                foreach (var item in lekovi)
                                {
                                    item.BR_DOC = zPrijemnica.Br_dok;
                                }
                            }

                            entity.Datum = zPrijemnica.Datum;
                            entity.DokumentDobavljaca = zPrijemnica.Dok_dobav;
                            entity.PartnerId = partner.Id;
                        }
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Success = false;
                returnObject.Message = ex.Message;
                return returnObject;
            }
        }

        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.ZPrijemnica.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        if (applicationDbContext.SPrijemnica.Any(x => x.ZPrijemnicaId == entity.Id))
                        {
                            throw new Exception("Morate prvo izbrisati sve artikle iz ove prijemnice.");
                        }
                        else
                        {
                            applicationDbContext.ZPrijemnica.Remove(entity);
                            applicationDbContext.SaveChanges();
                            return new ReturnObject() { Success = true };
                        }
                    }
                    else
                    {
                        throw new Exception("Prijemnica pod ovim Id-em nije nadjena.");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject() { Success = false, Message = ex.Message };
            }
        }
    }
}
