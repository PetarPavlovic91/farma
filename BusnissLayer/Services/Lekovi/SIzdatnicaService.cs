﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class SIzdatnicaService
    {
        public List<SIzdatnicaPregledVM> GetAll(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from si in applicationDbContext.SIzdatnica.Where(x => x.BR_DOC == brDok)
                        join l in applicationDbContext.Artikl on si.ArtiklId equals l.Id
                        join jm in applicationDbContext.JedinicaMere on l.JedinicaMereId equals jm.Id
                        join m in applicationDbContext.VrstaTroska on si.VrstaTroskaId equals m.Id
                        join v in applicationDbContext.VeterinarskiTehnicar on si.VeterinarskiTehnicarId equals v.Id
                        select new SIzdatnicaPregledVM()
                        {
                            Id = si.Id,
                            Rb = si.RB,
                            Lek = l.SifraArtikla,
                            NazivLeka = l.Naziv,
                            Jm = jm.Naziv,
                            Kolicina = si.Kolicina,
                            Cena = si.Cena,
                            Vt = m.VT,
                            MestoTroska = m.Naziv,
                            Br_dok = si.BR_DOC,
                            VeterinarskiTehnicar = v.VET
                        }).ToList();
            }
        }
        public SIzdatnicaPregledVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from si in applicationDbContext.SIzdatnica.Where(x => x.Id == id)
                        join l in applicationDbContext.Artikl on si.ArtiklId equals l.Id
                        join jm in applicationDbContext.JedinicaMere on l.JedinicaMereId equals jm.Id
                        join m in applicationDbContext.VrstaTroska on si.VrstaTroskaId equals m.Id
                        join v in applicationDbContext.VeterinarskiTehnicar on si.VeterinarskiTehnicarId equals v.Id
                        select new SIzdatnicaPregledVM()
                        {
                            Id = si.Id,
                            Rb = si.RB,
                            Lek = l.SifraArtikla,
                            NazivLeka = l.Naziv,
                            Jm = jm.Naziv,
                            Kolicina = si.Kolicina,
                            Cena = si.Cena,
                            Vt = m.VT,
                            MestoTroska = m.Naziv,
                            VeterinarskiTehnicar = v.VET
                        }).FirstOrDefault();
            }
        }

        public ReturnObject Save(SIzdatnicaAzuriranjeVM sIzdatnicaVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var vetTehnicar = applicationDbContext.VeterinarskiTehnicar.FirstOrDefault(x => x.VET == sIzdatnicaVM.VeterinarskiTehnicar);
                    if (vetTehnicar == null)
                    {
                        throw new Exception("Tehnicar sa tim Vet ne postoji.");
                    }
                    var vrstaTroska = applicationDbContext.VrstaTroska.FirstOrDefault(x => x.VT == sIzdatnicaVM.Vt);
                    if (vrstaTroska == null)
                    {
                        throw new Exception("Ne postoji ta vrsta troska. Proverite sifre vrste troska.");
                    }
                    var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.SifraArtikla == sIzdatnicaVM.Lek);
                    if (artikl == null)
                    {
                        throw new Exception("Ne postoji taj artikl. Proverite sifre artikl.");
                    }
                    var vrstaDokumenta = applicationDbContext.VrstaDokumenta.FirstOrDefault(x => x.Naziv == "Izdatnica");
                    if (vrstaDokumenta == null)
                    {
                        throw new Exception("Ne postoji vrsta dokumenta.");
                    }
                    var zIzdatnica = applicationDbContext.ZIzdatnica.FirstOrDefault(x => x.Id == sIzdatnicaVM.ZIzdatnicaId);
                    if (zIzdatnica == null)
                    {
                        throw new Exception("Ne postoji zizdatnica.");
                    }

                    decimal kolicinaPocetnogStanja = 0;
                    var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == artikl.Id);
                    if (pocetnoStanje != null)
                    {
                        kolicinaPocetnogStanja = pocetnoStanje.Kolicina ?? 0;
                    }
                    artikl.Izlaz = artikl.Izlaz + sIzdatnicaVM.Kolicina;
                    if (kolicinaPocetnogStanja + artikl.Ulaz - artikl.Izlaz < 0)
                    {
                        returnObject.Message = "Stanje leka je u minusu!";
                    }
                    if (applicationDbContext.SIzdatnica.Any(x => x.BR_DOC == sIzdatnicaVM.Br_dok && x.RB == sIzdatnicaVM.Rb))
                    {
                        throw new Exception("Isti RB vec postoji na ovoj izdatnici, molim promenite RB.");
                    }

                    decimal? prosecnaCena = 0.00M;
                    var zPrijemniceIds = applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) <= zIzdatnica.Datum.Date).Select(x => x.Id).ToList();
                    var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artikl.Id && zPrijemniceIds.Contains(x.ZPrijemnicaId)).ToList();

                    var zIzdatniceIds = applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) <= zIzdatnica.Datum.Date).Select(x => x.Id).ToList();
                    var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artikl.Id && zIzdatniceIds.Contains(x.ZIzdatnicaId)).ToList();

                    if (pocetnoStanje != null)
                    {
                        prosecnaCena = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                    }
                    else
                    {
                        if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                        {
                            prosecnaCena = 0.00M;
                        }
                        else
                        {
                            prosecnaCena = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) - sIzdatnice.Sum(x => x.Kolicina));
                        }
                    }

                    var entity = applicationDbContext.SIzdatnica.Add(
                        new SIzdatnica()
                        {
                            BR_DOC = sIzdatnicaVM.Br_dok,
                            RB = sIzdatnicaVM.Rb,
                            Cena = prosecnaCena,
                            Kolicina = sIzdatnicaVM.Kolicina,
                            ArtiklId = artikl.Id,
                            VrstaTroskaId = vrstaTroska.Id,
                            ZIzdatnicaId = sIzdatnicaVM.ZIzdatnicaId,
                            VeterinarskiTehnicarId = vetTehnicar.Id
                        });

                    //entity.Kartice.Add(new Kartica()
                    //{
                    //    Datum = sIzdatnicaVM.Datum,
                    //    BR_DOK = sIzdatnicaVM.Br_dok,
                    //    U_I = "I",
                    //    Cena = prosecnaCena,
                    //    Kolicina = sIzdatnicaVM.Kolicina,
                    //    VrstaDokumentaId = vrstaDokumenta.Id,
                    //    ArtiklId = artikl.Id,
                    //    VeterinarskiTehnicarId = zIzdatnica.VeterinarskiTehnicarId,
                    //    VrstaTroskaId = vrstaTroska.Id
                    //});

                    applicationDbContext.SaveChanges();

                    var izdatnicaDatum = applicationDbContext.ZIzdatnica.FirstOrDefault(x => x.Id == entity.ZIzdatnicaId);
                    AzurirajProsecneCene(izdatnicaDatum.Datum, artikl.Id);

                    returnObject.Id = entity.Id;
                    if (prosecnaCena == 0)
                    {
                        returnObject.Message = "Uspesno sacuvano. Prosecna cena ovog artikla je 0.";
                    }
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Success = false;
                returnObject.Message = ex.Message;
                return returnObject;
            }
        }

        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.SIzdatnica.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.Id == entity.ArtiklId);
                        if (artikl == null)
                        {
                            throw new Exception("Ne postoji taj artikl. Proverite sifre artikl.");
                        }
                        artikl.Izlaz = artikl.Izlaz - entity.Kolicina;

                        applicationDbContext.SIzdatnica.Remove(entity);
                        applicationDbContext.SaveChanges();

                        var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == entity.ArtiklId).ToList();
                        var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == entity.ArtiklId);
                        if (pocetnoStanje != null)
                        {
                            artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina - sIzdatnice.Sum(x => x.Kolicina));
                        }
                        else
                        {
                            if (sPrijemnice.Count == 0 && sIzdatnice.Count == 0)
                            {
                                artikl.PR_CENA = 0;
                            }
                            else
                            {
                                artikl.PR_CENA = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) - sIzdatnice.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) - sIzdatnice.Sum(x => x.Kolicina));
                            }
                        }
                        applicationDbContext.SaveChanges();

                        var izdatnicaDatum = applicationDbContext.ZIzdatnica.FirstOrDefault(x => x.Id == entity.ZIzdatnicaId);
                        AzurirajProsecneCene(izdatnicaDatum.Datum, artikl.Id);
                        return new ReturnObject() { Success = true };
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }

        public double GetNewRB(string id)
        {
            var izdatnicaId = int.Parse(id);
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rbs = applicationDbContext.SIzdatnica.Where(x => x.ZIzdatnicaId == izdatnicaId).OrderByDescending(x => x.RB).Select(x => x.RB).FirstOrDefault();
                if (rbs == null)
                {
                    rbs = 0;
                }
                return (double)rbs + 1;
            }
        }

        public List<TrebovanjeVM> TrebovanjeReport(string brDok)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from si in applicationDbContext.SIzdatnica.Where(x => x.BR_DOC == brDok)
                        join l in applicationDbContext.Artikl on si.ArtiklId equals l.Id
                        join jm in applicationDbContext.JedinicaMere on l.JedinicaMereId equals jm.Id
                        join m in applicationDbContext.VrstaTroska on si.VrstaTroskaId equals m.Id
                        join v in applicationDbContext.VeterinarskiTehnicar on si.VeterinarskiTehnicarId equals v.Id
                        select new TrebovanjeVM()
                        {
                            RB = si.RB,
                            LEK = l.SifraArtikla,
                            NAZIV_LEKA = l.Naziv,
                            JM = jm.Naziv,
                            KOLICINA = si.Kolicina,
                            CENA = si.Cena,
                            VT = m.VT,
                            VR_TROSKA = m.Naziv,
                            VET = v.VET
                        }).ToList();
            }
        }


        public void AzurirajProsecneCene(DateTime datum, int artiklId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zIzdatniceIds = applicationDbContext.ZIzdatnica.Where(x => x.Datum > datum).Select(x => x.Id).ToList();
                var sIzdatnice = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artiklId && zIzdatniceIds.Contains(x.ZIzdatnicaId)).ToList();


                foreach (var item in sIzdatnice)
                {
                    var zIzdatnica = applicationDbContext.ZIzdatnica.FirstOrDefault(x => x.Id == item.ZIzdatnicaId);

                    var zPrijemniceIds = applicationDbContext.ZPrijemnica.Where(x => x.Datum < zIzdatnica.Datum).Select(x => x.Id).ToList();
                    var sPrijemnice = applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artiklId && zPrijemniceIds.Contains(x.ZPrijemnicaId)).ToList();

                    var zIzdatniceIdsU = applicationDbContext.ZIzdatnica.Where(x => x.Datum < datum).Select(x => x.Id).ToList();
                    var sIzdatniceU = applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artiklId && zIzdatniceIdsU.Contains(x.ZIzdatnicaId)).ToList();

                    var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == artiklId);
                    if (pocetnoStanje != null)
                    {
                        item.Cena = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina - sIzdatniceU.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina - sIzdatniceU.Sum(x => x.Kolicina));
                    }
                    else
                    {
                        if (sPrijemnice.Count == 0 && sIzdatniceU.Count == 0)
                        {
                            item.Cena = 0;
                        }
                        else
                        {
                            item.Cena = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) - sIzdatniceU.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) - sIzdatniceU.Sum(x => x.Kolicina));
                        }
                    }
                    applicationDbContext.SaveChanges();
                }
            }
        }

    }
}
