﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class KarticaService
    {
        public List<KarticaPregledVM> GetByDateAndSifraArtikla(DateTime fromDate, DateTime to, string sifraArtikla)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var artikl = applicationDbContext.Artikl.FirstOrDefault(x => x.SifraArtikla == sifraArtikla);

                if (artikl == null)
                {
                    throw new Exception("Pogresna sifra artikla");
                }
                var poctnoStanjeLeka = applicationDbContext.PocetnoStanje.Where(x => x.ArtiklId == artikl.Id).Select(x => x.Kolicina).FirstOrDefault();


                var poctnoStanjeUlaz = (from s in applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artikl.Id)
                                        join z in applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) <= fromDate.Date) on s.ZPrijemnicaId equals z.Id
                                        select s.Kolicina).DefaultIfEmpty().Sum();


                var poctnoStanjeIzlaz = (from s in applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artikl.Id)
                                         join z in applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) <= fromDate.Date) on s.ZIzdatnicaId equals z.Id
                                         select s.Kolicina).DefaultIfEmpty().Sum();


                var sPrijemnice = (from s in applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artikl.Id)
                                   join z in applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) <= fromDate.Date) on s.ZPrijemnicaId equals z.Id
                                   select new { s.Kolicina, s.Cena }).ToList();


                var sIzdatniceU = (from s in applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artikl.Id)
                                   join z in applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) <= fromDate.Date) on s.ZIzdatnicaId equals z.Id
                                   select new { s.Kolicina, s.Cena }).ToList();

                decimal? pocetnoStanjeCena = 0;
                var pocetnoStanjeKolicina = (poctnoStanjeLeka != null ? poctnoStanjeLeka : 0) + (poctnoStanjeUlaz != null ? poctnoStanjeUlaz : 0) - (poctnoStanjeIzlaz != null ? poctnoStanjeIzlaz : 0);
                if (pocetnoStanjeKolicina > 0)
                {
                    var pocetnoStanje = applicationDbContext.PocetnoStanje.FirstOrDefault(x => x.ArtiklId == artikl.Id);
                    if (pocetnoStanje != null)
                    {
                        pocetnoStanjeCena = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) + pocetnoStanje.Cena * pocetnoStanje.Kolicina - sIzdatniceU.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) + pocetnoStanje.Kolicina - sIzdatniceU.Sum(x => x.Kolicina));
                    }
                    else
                    {
                        if (sPrijemnice.Count == 0 && sIzdatniceU.Count == 0)
                        {
                            pocetnoStanjeCena = 0;
                        }
                        else
                        {
                            pocetnoStanjeCena = (sPrijemnice.Sum(x => x.Cena * x.Kolicina) - sIzdatniceU.Sum(x => x.Cena * x.Kolicina)) / (sPrijemnice.Sum(x => x.Kolicina) - sIzdatniceU.Sum(x => x.Kolicina));
                        }
                    }
                }

                List<KarticaPregledVM> karticaPregledVMs = new List<KarticaPregledVM>();
                karticaPregledVMs.Add(new KarticaPregledVM()
                {
                    Datum = fromDate,
                    BR_DOK = "",
                    U_I = "U",
                    Kolicina = pocetnoStanjeKolicina,
                    Cena = pocetnoStanjeCena,
                    Stanje = pocetnoStanjeKolicina,
                    VEZA = "",
                    Naziv = "Pocetno"
                });

                var karticaPregledVM = (from s in applicationDbContext.SPrijemnica.Where(x => x.ArtiklId == artikl.Id)
                                        join z in applicationDbContext.ZPrijemnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= to.Date) on s.ZPrijemnicaId equals z.Id
                                        join p in applicationDbContext.Partner on z.PartnerId equals p.Id into selectedP
                                        from p in selectedP.DefaultIfEmpty()
                                        select new KarticaPregledVM()
                                        {
                                            Datum = z.Datum,
                                            BR_DOK = z.BR_DOC,
                                            U_I = "U",
                                            Kolicina = s.Kolicina,
                                            Cena = s.Cena,
                                            Stanje = 0,
                                            VEZA = p.SIFPP,
                                            Naziv = p.Naziv
                                        }).ToList();

                karticaPregledVM.AddRange((from s in applicationDbContext.SIzdatnica.Where(x => x.ArtiklId == artikl.Id)
                                           join z in applicationDbContext.ZIzdatnica.Where(x => DbFunctions.TruncateTime(x.Datum) >= fromDate.Date && DbFunctions.TruncateTime(x.Datum) <= to.Date) on s.ZIzdatnicaId equals z.Id
                                           select new KarticaPregledVM()
                                           {
                                               Datum = z.Datum,
                                               BR_DOK = z.BR_DOC,
                                               U_I = "I",
                                               Kolicina = s.Kolicina,
                                               Cena = s.Cena,
                                               Stanje = 0,
                                               VEZA = null,
                                               Naziv = null
                                           }).ToList());

                karticaPregledVM = karticaPregledVM.OrderBy(x => x.Datum).ToList();

                for (int i = 0; i < karticaPregledVM.Count(); i++)
                {
                    if (karticaPregledVM[i].U_I == "U")
                    {
                        karticaPregledVM[i].Stanje = pocetnoStanjeKolicina + (karticaPregledVM[i].Kolicina != null ? karticaPregledVM[i].Kolicina : 0);
                    }
                    else
                    {
                        karticaPregledVM[i].Stanje = pocetnoStanjeKolicina - (karticaPregledVM[i].Kolicina != null ? karticaPregledVM[i].Kolicina : 0);
                    }
                    pocetnoStanjeKolicina = karticaPregledVM[i].Stanje;
                }
                karticaPregledVMs.AddRange(karticaPregledVM);
                return karticaPregledVMs;
            }
        }

        public List<PromeneZaPeriodVM> PromeneZaPeriod(DateTime fromDate, DateTime to)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", to);

                return applicationDbContext.Database.SqlQuery<PromeneZaPeriodVM>("PromeneZaPeriod @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
            }
        }

        public List<PromeneZaPeriodVM> StanjeLageraNaDan(DateTime date, bool excludeZeros = false)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTime", date);

                var result = applicationDbContext.Database.SqlQuery<PromeneZaPeriodVM>("StanjeLageraNaDan @dateTime", fromParametar).ToList();
                if (excludeZeros)
                {
                    result = result.Where(x => x.Stanje != 0).ToList();
                }
                return result;
            }
        }
    }
}
