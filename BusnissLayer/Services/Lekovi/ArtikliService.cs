﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using BusnissLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class ArtikliService
    {
        public List<StanjeLageraLekovaVM> GetStanjeLageraLekova()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                KarticaService karticaService = new KarticaService();
                var upToCurrentDate = DateTime.Now.AddDays(1);
                upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 0, 0, 0);
                var stanjeLageraNaDan = karticaService.StanjeLageraNaDan(upToCurrentDate);


                return (from s in stanjeLageraNaDan
                        join a in applicationDbContext.Artikl on s.Lek equals a.SifraArtikla
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        select new StanjeLageraLekovaVM()
                        {
                            VGP = va.VGP,
                            Lek = a.SifraArtikla,
                            Naziv = a.Naziv,
                            Jm = s.Naziv,
                            Stanje = s.Stanje,
                            Cena = s.Stanje.HasValue && s.Stanje != 0 ? s.Vrednost / s.Stanje : null,
                            Vrednost = s.Vrednost,
                            Pocetno_Stanje = s.Pocst,
                            Ulaz = s.Ulaz,
                            Izlaz = s.Izlaz,
                        }).ToList();
            }
        }

        public List<JedinicaMereVM> GetJediniceMere()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.JedinicaMere.Select(x => new JedinicaMereVM() { Naziv = x.Naziv }).ToList();
            }
        }

        public List<ArtiklPregledVM> LekoviZaNabavku()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                KarticaService karticaService = new KarticaService();
                var upToCurrentDate = DateTime.Now.AddDays(1);
                upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 0, 0, 0);
                var stanjeLageraNaDan = karticaService.StanjeLageraNaDan(upToCurrentDate);

                var result = (from a in applicationDbContext.Artikl
                        join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        join p in applicationDbContext.PocetnoStanje on a.Id equals p.ArtiklId into sp
                        where ((sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0) + a.Ulaz - a.Izlaz) < a.GR_NAB
                        select new ArtiklPregledVM()
                        {
                            Id = a.Id,
                            Lek = a.SifraArtikla,
                            Naziv = a.Naziv,
                            GR_NAB = a.GR_NAB,
                            Pocetno_Stanje = sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0,
                            Ulaz = a.Ulaz,
                            Izlaz = a.Izlaz,
                            Stanje = (sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0) + (a.Ulaz != null ? a.Ulaz : 0) - (a.Izlaz != null ? a.Izlaz : 0),
                            PR_CENA = a.PR_CENA,
                            Jm = jm.Naziv,
                            Grupa = va.VGP,
                            Naziv_Grupe = va.Naziv
                        }).ToList();

                return (from r in result
                       join s in stanjeLageraNaDan on r.Lek equals s.Lek
                       select new ArtiklPregledVM()
                       {
                           Id = r.Id,
                           Lek = r.Lek,
                           Naziv = r.Naziv,
                           GR_NAB = r.GR_NAB,
                           Pocetno_Stanje = r.Pocetno_Stanje,
                           Ulaz = r.Ulaz,
                           Izlaz = r.Izlaz,
                           Stanje = r.Stanje,
                           PR_CENA = s.Vrednost / s.Stanje,
                           Jm = r.Naziv,
                           Grupa = r.Grupa,
                           Naziv_Grupe = r.Naziv_Grupe
                       }).ToList();
            }
        }


        public List<ArtiklPregledVM> LogickaKontrolaPodataka()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                KarticaService karticaService = new KarticaService();
                var upToCurrentDate = DateTime.Now.AddDays(1);
                upToCurrentDate = new DateTime(upToCurrentDate.Year, upToCurrentDate.Month, upToCurrentDate.Day, 0, 0, 0);
                var stanjeLageraNaDan = karticaService.StanjeLageraNaDan(upToCurrentDate);

                var result = (from a in applicationDbContext.Artikl
                        join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        join p in applicationDbContext.PocetnoStanje on a.Id equals p.ArtiklId into sp
                        where ((sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0) + a.Ulaz - a.Izlaz) < 0
                        select new ArtiklPregledVM()
                        {
                            Id = a.Id,
                            Lek = a.SifraArtikla,
                            Naziv = a.Naziv,
                            GR_NAB = a.GR_NAB,
                            Pocetno_Stanje = sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0,
                            Ulaz = a.Ulaz,
                            Izlaz = a.Izlaz,
                            Stanje = (sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0) + (a.Ulaz != null ? a.Ulaz : 0) - (a.Izlaz != null ? a.Izlaz : 0),
                            PR_CENA = a.PR_CENA,
                            Jm = jm.Naziv,
                            Grupa = va.VGP,
                            Naziv_Grupe = va.Naziv
                        }).ToList();

                return (from r in result
                        join s in stanjeLageraNaDan on r.Lek equals s.Lek
                        select new ArtiklPregledVM()
                        {
                            Id = r.Id,
                            Lek = r.Lek,
                            Naziv = r.Naziv,
                            GR_NAB = r.GR_NAB,
                            Pocetno_Stanje = r.Pocetno_Stanje,
                            Ulaz = r.Ulaz,
                            Izlaz = r.Izlaz,
                            Stanje = r.Stanje,
                            PR_CENA = s.Vrednost / s.Stanje,
                            Jm = r.Naziv,
                            Grupa = r.Grupa,
                            Naziv_Grupe = r.Naziv_Grupe
                        }).ToList();
            }
        }

        public List<StanjeLageraNaDanVM> GetStanjeLageraLekovaNaDan(DateTime dan)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from a in applicationDbContext.Artikl
                        join sp in applicationDbContext.SPrijemnica on a.Id equals sp.ArtiklId into sps
                        from sp in sps.DefaultIfEmpty()
                        join zp in applicationDbContext.ZPrijemnica on sp.ZPrijemnicaId equals zp.Id into zps
                        from zp in zps.DefaultIfEmpty()
                        join si in applicationDbContext.SIzdatnica on a.Id equals si.ArtiklId into sis
                        from si in sis.DefaultIfEmpty()
                        join zi in applicationDbContext.ZIzdatnica on si.ZIzdatnicaId equals zi.Id into zis
                        from zi in zis.DefaultIfEmpty()
                        join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        select new StanjeLageraNaDanVM()
                        {
                            //VGP = va.VGP,
                            //Lek = a.SifraArtikla,
                            //Naziv = a.Naziv,
                            //Jm = jm.Naziv,
                            //Stanje = a.Ulaz - a.Izlaz,
                            //Cena = a.PR_CENA,//da li je to to?
                            //Vrednost = a.PR_CENA * (a.Ulaz - a.Izlaz),
                            //Pocetno_Stanje = a.PocetnoStanje,
                            //Ulaz = a.Ulaz,
                            //Izlaz = a.Izlaz,
                        }).ToList();
            }
        }

        public List<ArtiklPregledVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from a in applicationDbContext.Artikl
                        join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        join p in applicationDbContext.PocetnoStanje on a.Id equals p.ArtiklId into sp
                        select new ArtiklPregledVM()
                        {
                            Id = a.Id,
                            Lek = a.SifraArtikla,
                            Naziv = a.Naziv,
                            GR_NAB = a.GR_NAB,
                            Pocetno_Stanje = sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0,
                            Ulaz = a.Ulaz,
                            Izlaz = a.Izlaz,
                            Stanje = (sp.Count() > 0 ? sp.FirstOrDefault().Kolicina : 0) + (a.Ulaz != null ? a.Ulaz : 0) - (a.Izlaz != null ? a.Izlaz : 0),
                            PR_CENA = a.PR_CENA,
                            Jm = jm.Naziv,
                            Grupa = va.VGP,
                            Naziv_Grupe = va.Naziv
                        }).ToList();
            }
        }

        public List<ArtiklAzuriranjeVM> GetAllAzuriranjeVM()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from a in applicationDbContext.Artikl
                        join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        select new ArtiklAzuriranjeVM()
                        {
                            Id = a.Id,
                            Lek = a.SifraArtikla,
                            Naziv = a.Naziv,
                            GR_NAB = a.GR_NAB,
                            Jm = jm.Naziv,
                            Grupa = va.VGP,
                            Naziv_Grupe = va.Naziv
                        }).ToList();
            }
        }

        public List<ArtikliShortVM> GetAllShortVM()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Artikl.Select(x =>
                new ArtikliShortVM()
                {
                    SifraArtikla = x.SifraArtikla,
                    Naziv = x.Naziv
                }).OrderBy(x => x.Naziv).ToList();
            }
        }

        public List<ArtikliShortVM> GetAllShortVMOrderedBySifra()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Artikl.Select(x =>
                new ArtikliShortVM()
                {
                    SifraArtikla = x.SifraArtikla,
                    Naziv = x.Naziv
                }).OrderBy(x => x.SifraArtikla).ToList();
            }
        }


        public List<ArtikliShortVM> GetAllShortVM(string partOfName)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Artikl.Where(x => x.Naziv.Contains(partOfName)).Select(x =>
                new ArtikliShortVM()
                {
                    SifraArtikla = x.SifraArtikla,
                    Naziv = x.Naziv
                }).OrderBy(x => x.Naziv).ToList();
            }
        }

        public ArtiklAzuriranjeVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from a in applicationDbContext.Artikl.Where(x => x.Id == id)
                        join va in applicationDbContext.VrstaArtikla on a.VrstaArtiklaId equals va.Id
                        join jm in applicationDbContext.JedinicaMere on a.JedinicaMereId equals jm.Id
                        select new ArtiklAzuriranjeVM()
                        {
                            Id = a.Id,
                            Lek = a.SifraArtikla,
                            Naziv = a.Naziv,
                            GR_NAB = a.GR_NAB,
                            Grupa = va.VGP,
                            Jm = jm.Naziv
                        }).FirstOrDefault();
            }
        }
        public ReturnObject Save(ArtiklPregledVM artiklVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (artiklVM.Id == 0)
                    {
                        var jedinicaMere = applicationDbContext.JedinicaMere.FirstOrDefault(x => x.Naziv == artiklVM.Jm);
                        if (jedinicaMere == null)
                        {
                            returnObject.Success = false;
                            returnObject.Message = "Jedinica mere nije validna";
                            return returnObject;
                        }
                        var vrstaArtikla = applicationDbContext.VrstaArtikla.FirstOrDefault(x => x.VGP == artiklVM.Grupa);
                        if (vrstaArtikla == null)
                        {
                            returnObject.Success = false;
                            returnObject.Message = "Ova grupa lekova nije validna";
                            return returnObject;
                        }
                        var entity = applicationDbContext.Artikl.Add(
                            new Artikl()
                            {
                                SifraArtikla = artiklVM.Lek,
                                Naziv = artiklVM.Naziv,
                                GR_NAB = artiklVM.GR_NAB,
                                JedinicaMereId = jedinicaMere.Id,
                                VrstaArtiklaId = vrstaArtikla.Id
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {

                    }
                }
                return returnObject;
            }
            catch (Exception)
            {
                returnObject.Success = false;
                return returnObject;
            }
        }
        public ReturnObject Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.Artikl.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.Artikl.Remove(entity);
                        applicationDbContext.SaveChanges();
                        return new ReturnObject() { Success = true };
                    }
                    else
                    {
                        throw new Exception("Podatak nije pronadjen");
                    }
                }
            }
            catch (Exception ex)
            {
                return new ReturnObject()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }
    }
}
