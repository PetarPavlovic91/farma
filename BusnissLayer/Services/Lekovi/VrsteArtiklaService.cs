﻿using BusinessLayer.ViewModels;
using BusinessLayer.ViewModels.Lekovi;
using DataAccessLayer;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class VrsteArtiklaService
    {
        public List<VrsteArtiklaVM> GetAll()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaArtikla.Select(x =>
                new VrsteArtiklaVM()
                {
                    Id = x.Id,
                    VGP = x.VGP,
                    Naziv = x.Naziv
                }).ToList();
            }
        }

        public List<VrsteArtiklaShortVM> GetAllShortVM()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaArtikla.Select(x =>
                new VrsteArtiklaShortVM()
                {
                    VGP = x.VGP,
                    Naziv = x.Naziv
                }).ToList();
            }
        }

        public List<VrsteArtiklaVM> GetFiltered(string filterString)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaArtikla.Where(x => x.Naziv.Contains(filterString) || x.VGP.Contains(filterString)).Select(x =>
                new VrsteArtiklaVM()
                {
                    Id = x.Id,
                    VGP = x.VGP,
                    Naziv = x.Naziv
                }).ToList();
            }
        }

        public VrsteArtiklaVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.VrstaArtikla.Where(x => x.Id == id).Select(x =>
                new VrsteArtiklaVM()
                {
                    Id = x.Id,
                    VGP = x.VGP,
                    Naziv = x.Naziv
                }).FirstOrDefault();
            }
        }

        public ReturnObject Save(VrsteArtiklaVM vrstaArtiklaVM)
        {
            var returnObject = new ReturnObject() { Success = true };
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    if (vrstaArtiklaVM.Id == 0)
                    {
                        var entity = applicationDbContext.VrstaArtikla.Add(
                            new VrstaArtikla()
                            {
                                VGP = vrstaArtiklaVM.VGP,
                                Naziv = vrstaArtiklaVM.Naziv
                            });
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                    else
                    {
                        var entity = applicationDbContext.VrstaArtikla.FirstOrDefault(x => x.Id == vrstaArtiklaVM.Id);
                        if (entity != null)
                        {
                            entity.VGP = vrstaArtiklaVM.VGP;
                            entity.Naziv = vrstaArtiklaVM.Naziv;
                        }
                        applicationDbContext.SaveChanges();
                        returnObject.Id = entity.Id;
                    }
                }
                return returnObject;
            }
            catch (Exception ex)
            {
                returnObject.Success = false;
                returnObject.Message = ex.Message;
                return returnObject;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var entity = applicationDbContext.VrstaArtikla.FirstOrDefault(x => x.Id == id);
                    if (entity != null)
                    {
                        applicationDbContext.VrstaArtikla.Remove(entity);
                        applicationDbContext.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
