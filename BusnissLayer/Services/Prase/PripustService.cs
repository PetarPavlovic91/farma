﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase;
using BusnissLayer.ViewModels.Prase._01Pripust.Nerastovi;
using BusnissLayer.ViewModels.Prase._01Pripust.ReportModels;
using BusnissLayer.ViewModels.Prase.Pripust;
using BusnissLayer.ViewModels.Prase.Pripust.KontrolaProduktivnostiNerastova;
using BusnissLayer.ViewModels.Prase.Pripust.ProblemiUreprodukciji;
using BusnissLayer.ViewModels.Prase.Pripust.ReproduktivniPokazateljKrmaca;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class PripustService
    {

        public bool IsLatestPripust(int pripustId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var pripust = applicationDbContext.Pripust.First(x => x.Id == pripustId);

                var latestPripust = applicationDbContext.Pripust.Where(x => x.KrmacaId == pripust.KrmacaId).OrderByDescending(x => x.DAT_PRIP).First();

                if (pripust.DAT_PRIP < latestPripust.DAT_PRIP)
                {
                    return false;
                }
                return true;
            }
        }

        public List<AnalizaAnestrijeVM> AnalizaAnestrije(DateTime danOd, DateTime danDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@DanOd", danOd);
                var toParametar = new SqlParameter("@DanDo", danDo);
                return applicationDbContext.Database.SqlQuery<AnalizaAnestrijeVM>("Pripust.AnalizaPovadjanja @DanOd, @DanDo", fromParametar, toParametar).ToList();
            }
        }

        public string CheckIskljucenje(string krmaca)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var res = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == krmaca);
                if (res == null)
                    return "Ova krmaca ne postoji";
                if (applicationDbContext.Skart.Any(x => x.KrmacaId == res.Id))
                    return "Ova krmaca je iskljucena";
                if (res.FAZA == 2 || res.FAZA == 4)
                    return "Ova krmaca nije u odgovarajucoj predfazi.";
                return null;
            }
        }

        public DataTable AnalizaPripusta(DateTime danOd, DateTime danDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@DanOd", danOd);
                var toParametar = new SqlParameter("@DanDo", danDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Pripust.AnalizaPripusta_Krmace", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable AnalizaPripusta_Nedelje(DateTime danOd, DateTime danDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@DanOd", danOd);
                var toParametar = new SqlParameter("@DanDo", danDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Pripust.AnalizaPripusta_Nedelje", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public List<PeriodSuprasnostiVM> NedeljaPripusta(int nedelja, int godina, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var result = (from p in applicationDbContext.Pripust.Where(x => DbFunctions.TruncateTime(x.DAT_PRIP) >= (DateTime)DbFunctions.AddDays(new DateTime(godina, 1, 1, 0, 0, 0), 7 * (nedelja - 1)) 
                                                                                && DbFunctions.TruncateTime(x.DAT_PRIP) < (DateTime)DbFunctions.AddDays(new DateTime(godina, 1, 1, 23, 59, 59), 7 * (nedelja)))
                              join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                              select new PeriodSuprasnostiVM()
                              {
                                  Krmaca = k.T_KRM,
                                  Rasa = k.RASA,
                                  Ciklus = p.CIKLUS,
                                  Pripustena = p.DAT_PRIP,
                                  Rbp = p.RBP,
                                  Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                                  Ocek_Pras = (DateTime)DbFunctions.AddDays(p.DAT_PRIP, 115),
                                  Obj = p.OBJ,
                                  Box = p.BOX,
                                  Napomena = k.NAPOMENA
                              }).ToList();

                if (sortOrder == "Dani")
                {
                    return result.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return result.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }

        public DataTable SP_GetAllByDateReport(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danOdParametar = new SqlParameter("@danOd", new DateTime(datumOd.Year, datumOd.Month, datumOd.Day, 0, 0, 0));
                var danDoParametar = new SqlParameter("@danDo", new DateTime(datumDo.Year, datumDo.Month, datumDo.Day, 23, 59, 59));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Pripust.GetAllByDateReport", con))
                    {
                        cmd.Parameters.Add(danOdParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        #region EvidencijaPripusta
        //public ReturnObject Save(PripustAzuriranjeVM pripust)
        //{
        //    using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        //    {

        //        if (pripust.Id == 0)
        //        {
        //            var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == pripust.KRMACA);
        //            if (krmaca == null)
        //            {
        //                return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
        //            }
        //            int? nerastId = null;
        //            if (!string.IsNullOrEmpty(pripust.NERAST))
        //            {
        //                var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == pripust.NERAST);
        //                if (nerast == null)
        //                {
        //                    return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
        //                }
        //                nerastId = nerast.Id;
        //                if (!applicationDbContext.Pripust.Any(x => x.NerastId == nerast.Id))
        //                {
        //                    nerast.D_P_SKOKA = pripust.DatumPripusta;
        //                }
        //            }

        //            int? nerastId2 = null;
        //            if (!string.IsNullOrEmpty(pripust.NERAST2))
        //            {
        //                var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == pripust.NERAST2);
        //                if (nerast == null)
        //                {
        //                    return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
        //                }
        //                nerastId2 = nerast.Id;
        //            }
        //            var entity = applicationDbContext.Pripust.Add(new Pripust()
        //            {
        //                DAT_PRIP = pripust.DatumPripusta,
        //                BOX = pripust.BOX,
        //                CIKLUS = krmaca.CIKLUS + 1,
        //                OBJ = pripust.OBJ,
        //                OCENA = pripust.OCENA,
        //                OCENA2 = pripust.OCENA2,
        //                OSEM = pripust.OSEM,
        //                OSEM2 = pripust.OSEM2,
        //                PRIMEDBA = pripust.PRIMEDBA,
        //                PRIPUSNICA = pripust.PRIPUSNICA,
        //                PV = pripust.PV,
        //                PV2 = pripust.PV2,
        //                RAZL_P = pripust.RAZL_P,
        //                RB = pripust.Rb,
        //                RBP = pripust.RBP,
        //                KrmacaId = krmaca.Id,
        //                NerastId = nerastId,
        //                Nerast2Id = nerastId2
        //            });
        //            krmaca.CIKLUS = krmaca.CIKLUS + 1;
        //            applicationDbContext.SaveChanges();
        //            return new ReturnObject() { Success = true, Id = entity.Id };
        //        }
        //        else
        //        {
        //            var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == pripust.KRMACA);
        //            if (krmaca == null)
        //            {
        //                return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
        //            }
        //            int? nerastId = null;
        //            if (!string.IsNullOrEmpty(pripust.NERAST))
        //            {
        //                var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == pripust.NERAST);
        //                if (nerast == null)
        //                {
        //                    return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
        //                }
        //                nerastId = nerast.Id;
        //            }

        //            int? nerastId2 = null;
        //            if (!string.IsNullOrEmpty(pripust.NERAST2))
        //            {
        //                var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == pripust.NERAST2);
        //                if (nerast == null)
        //                {
        //                    return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
        //                }
        //                nerastId2 = nerast.Id;
        //            }
        //            var entity = applicationDbContext.Pripust.FirstOrDefault(x => x.Id == pripust.Id);
        //            if (entity == null)
        //            {
        //                return new ReturnObject() { Success = false, Message = "Pripust nije pronadjen" };
        //            }
        //            entity.DAT_PRIP = pripust.DatumPripusta;
        //            entity.BOX = pripust.BOX;
        //            //entity.CIKLUS = pripust.CIKLUS;
        //            entity.OBJ = pripust.OBJ;
        //            entity.OCENA = pripust.OCENA;
        //            entity.OCENA2 = pripust.OCENA2;
        //            entity.OSEM = pripust.OSEM;
        //            entity.OSEM2 = pripust.OSEM2;
        //            entity.PRIMEDBA = pripust.PRIMEDBA;
        //            entity.PRIPUSNICA = pripust.PRIPUSNICA;
        //            entity.PV = pripust.PV;
        //            entity.PV2 = pripust.PV2;
        //            entity.RAZL_P = pripust.RAZL_P;
        //            entity.RB = pripust.Rb;
        //            entity.RBP = pripust.RBP;
        //            entity.KrmacaId = krmaca.Id;
        //            entity.NerastId = nerastId;
        //            entity.Nerast2Id = nerastId2;
        //            applicationDbContext.SaveChanges();
        //            return new ReturnObject() { Success = true, Id = entity.Id };
        //        }
        //    }
        //}
        public bool ProveriDaLiJeTest(string tetovir)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (applicationDbContext.Krmaca.Any(x => x.T_KRM == tetovir))
                {
                    return false;
                }
                var test = applicationDbContext.Test.Include(x => x.Pedigre).Include(x => x.Poreklo).FirstOrDefault(x => x.T_TEST == tetovir);
                if (test == null)
                {
                    return false;
                }
                if (test.POL == "M")
                {
                    return false;
                }
                if (applicationDbContext.Skart.Any(x => x.TestId == test.Id))
                    return false;
                return true;
            }
        }

        public ReturnObject PrenesiPodatkeZaNovuKrmacuIzTesta(string tetovir)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (applicationDbContext.Krmaca.Any(x => x.T_KRM == tetovir))
                {
                    return new ReturnObject() { Success = false, Message = "Krmaca sa tim tetovirom vec postoji." };
                }
                var test = applicationDbContext.Test.Include(x => x.Pedigre).Include(x => x.Poreklo).FirstOrDefault(x => x.T_TEST == tetovir);
                if (test == null)
                {
                    return new ReturnObject() { Success = false, Message = "Test nije pronadjen." };
                }
                if (test.POL == "M")
                {
                    return new ReturnObject() { Success = false, Message = "Pol ovog grla je MUski, ne moze se preneti u krmace." };
                }
                applicationDbContext.Krmaca.Add(new Krmaca()
                {
                    BR_SISA = test.BR_SISA,
                    CIKLUS = 0,
                    DAT_ROD = test.DAT_ROD,
                    EKST = test.EKST,
                    FAZA = 0,
                    GEN_MAR = test.GEN_MAR,
                    GN = test.GN,
                    GNM = test.GNM,
                    GNO = test.GNO,
                    HALOTAN = test.HALOTAN,
                    IZ_LEGLA = test.IZ_LEGLA,
                    MARKICA = test.MARKICA,
                    MBR_KRM = test.MBR_TEST,
                    MBR_MAJKE = test.MBR_MAJKE,
                    MBR_OCA = test.MBR_OCA,
                    NAPOMENA = test.NAPOMENA,
                    ODG = test.ODG,
                    RASA = test.RASA,
                    SI1 = test.SI1,
                    SI1F = test.SI1F,
                    TETOVIRK10 = test.TETOVIRT10,
                    T_KRM = test.T_TEST,
                    T_MAJKE10 = test.T_MAJKE10,
                    T_OCA10 = test.T_OCA10,
                    VLA = test.VLA,
                    Poreklo = test.Poreklo,
                    Pedigre = test.Pedigre
                });
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }


        //public ReturnObject Delete(int id)
        //{
        //    using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        //    {
        //        var entity = applicationDbContext.Pripust.FirstOrDefault(x => x.Id == id);
        //        if (entity == null)
        //        {
        //            return new ReturnObject() { Success = false, Message = "Pripust nije pronadjen" };
        //        }
        //        applicationDbContext.Pripust.Remove(entity);
        //        applicationDbContext.SaveChanges();
        //        return new ReturnObject() { Success = true };
        //    }
        //}
        public List<PripustVM> GetAllByDate(DateTime datumPripusta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => DbFunctions.TruncateTime(x.DAT_PRIP) == datumPripusta.Date)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join n1 in applicationDbContext.Nerast on p.NerastId equals n1.Id into n1s
                        from n1 in n1s.DefaultIfEmpty()
                        join n2 in applicationDbContext.Nerast on p.Nerast2Id equals n2.Id into n2s
                        from n2 in n2s.DefaultIfEmpty()
                        select new PripustVM()
                        {
                            Id = p.Id,
                            Rb = p.RB,
                            BOX = p.BOX,
                            CIKLUS = p.CIKLUS,
                            KRMACA = k.T_KRM,
                            MATICNI_BROJ = k.MBR_KRM,
                            NERAST = n1 != null ? n1.T_NER : null,
                            NERAST2 = n2 != null ? n2.T_NER : null,
                            OBJ = p.OBJ,
                            OCENA = p.OCENA,
                            OCENA2 = p.OCENA2,
                            OSEM = p.OSEM,
                            OSEM2 = p.OSEM2,
                            PRIMEDBA = p.PRIMEDBA,
                            PRIPUSNICA = p.PRIPUSNICA,
                            PV = p.PV,
                            PV2 = p.PV2,
                            RAZL_P = p.RAZL_P,
                            RBP = p.RBP
                        }).ToList();
            }
        }
        #endregion

        #region 02PregledPripusta
        public List<PregledPodatakaVM> PregledPodatakaPripusti(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => x.DAT_PRIP >= fromDate && x.DAT_PRIP <= toDate)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join n1 in applicationDbContext.Nerast on p.NerastId equals n1.Id into n1s
                        from n1 in n1s.DefaultIfEmpty()
                        join n2 in applicationDbContext.Nerast on p.Nerast2Id equals n2.Id into n2s
                        from n2 in n2s.DefaultIfEmpty()
                        join pob in applicationDbContext.Pobacaj.Where(x => x.DAT_POBAC >= fromDate && x.DAT_POBAC <= toDate) on k.Id equals pob.KrmacaId into pobs
                        from pob in pobs.DefaultIfEmpty()
                        join z in applicationDbContext.Zaluc on k.Id equals z.KrmacaId into zs
                        join pr in applicationDbContext.Prasenje.Where(x => x.DAT_PRAS > fromDate && x.DAT_PRAS < toDate) on k.Id equals pr.KrmacaId into prs
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new PregledPodatakaVM()
                        {
                            BOX = p.BOX,
                            CIKLUS = p.CIKLUS,
                            KRMACA = k.T_KRM,
                            NERAST = n1 != null ? n1.T_NER : null,
                            OBJ = p.OBJ,
                            OCENA = p.OCENA,
                            OSEM = p.OSEM,
                            PV = p.PV,
                            RAZ = p.RAZL_P,
                            RBP = p.RBP,
                            Pripustena = p.DAT_PRIP,
                            RASA = k.RASA,
                            Pobacaj = pob.DAT_POBAC,
                            Pr_dana = p.CIKLUS > 1 ? zs.Count() > 0 ? DbFunctions.DiffDays(zs.Where(x => x.DAT_ZAL < p.DAT_PRIP).OrderByDescending(x => x.DAT_ZAL).FirstOrDefault().DAT_ZAL, p.DAT_PRIP).ToString() : null : null,
                            Opras = prs.Count() > 0 ? "DA" : null,
                            SK = s.RAZL_SK
                        }).ToList();
            }
        }

        public DataTable SP_GetAllByDate(DateTime datumPripusta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumPripusta.Year, datumPripusta.Month, datumPripusta.Day, 0,0,0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Pripust.GetAllByDate", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public void SP_Save(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Pripust.Save", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SP_Delete(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Pripust.Delete", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        public DataTable OprasivostPoOsemeniteljima(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KontrolaProduktivnostiOsemenitelja", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public List<OprasivostPoNerastovimaReportVM> OprasivostPoNerastovima(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                return applicationDbContext.Database.SqlQuery<OprasivostPoNerastovimaReportVM>("OprasivostPoNerastovima @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
            }
        }
        public DataTable OprasivostPoRasamaNerastovima(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OprasivostPoRasamaNerastova", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable OprasivostPoRasamaKrmaca(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OprasivostPoRasamaKrmaca", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable OprasivostPoCiklusima(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OprasivostPoCiklusima", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable KombinacijaPripusta(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@datumPripustaOd", fromDate);
                var toParametar = new SqlParameter("@datumPripustaDo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KombinacijePripusta", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        
        public DataTable KombinacijePripustaReport(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@datumPripustaOd", fromDate);
                var toParametar = new SqlParameter("@datumPripustaDo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KombinacijePripustaReport", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        #endregion

        #region 03KalendarPripusta
        public List<KrmaceZaPripustVM> KrmaceZaPripust(int brDana, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var krmaceZaPripust = (from k in applicationDbContext.Krmaca
                                       join z in applicationDbContext.Zaluc.Where(x => DbFunctions.DiffDays(x.DAT_ZAL, DateTime.Now) > brDana) on k.Id equals z.KrmacaId
                                       where k.CIKLUS == z.PARIT 
                                       select new KrmaceZaPripustVM()
                                       {
                                           Krmaca = k.T_KRM,
                                           Rasa = k.RASA,
                                           Parit = k.PARIT,
                                           Zalucena = z.DAT_ZAL,
                                           Dana = DbFunctions.DiffDays(z.DAT_ZAL, DateTime.Now),
                                           Faza = k.FAZA,
                                           Zdr_st = z.ZDR_ST,
                                           Vr_hor = "",
                                           Obj = z.OBJ,
                                           Box = z.BOX,
                                           Napomena = k.NAPOMENA
                                       }).ToList();
                if (sortOrder == "brDana")
                {
                    return krmaceZaPripust.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return krmaceZaPripust.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }
        public List<PeriodSuprasnostiVM> PeriodSuprasnosti(int brDanaOd, int brDanaDo, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var periodSuprasnosti = (from p in applicationDbContext.Pripust.Where(x => DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) >= brDanaOd && DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) <= brDanaDo)
                                         join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                                         select new PeriodSuprasnostiVM()
                                         {
                                             Krmaca = k.T_KRM,
                                             Rasa = k.RASA,
                                             Ciklus = p.CIKLUS,
                                             Pripustena = p.DAT_PRIP,
                                             Rbp = p.RBP,
                                             Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                                             Ocek_Pras = (DateTime)DbFunctions.AddDays(p.DAT_PRIP, 115),
                                             Obj = p.OBJ,
                                             Box = p.BOX,
                                             Napomena = k.NAPOMENA
                                         }).ToList();

                if (sortOrder == "brDana")
                {
                    return periodSuprasnosti.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return periodSuprasnosti.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }

        public List<PrevodUCekalisteVM> PrevodUCekaliste(int brDanaOdPripusta, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var periodSuprasnosti = (from p in applicationDbContext.Pripust.Where(x => DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) >= brDanaOdPripusta)
                                         join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                                         join kl in applicationDbContext.Klasa on k.MBR_KRM equals kl.MBR_GRLA into kls
                                         from kl in kls.DefaultIfEmpty()
                                         select new PrevodUCekalisteVM()
                                         {
                                             Krmaca = k.T_KRM,
                                             Rasa = k.RASA,
                                             Ciklus = p.CIKLUS,
                                             Pripustena = p.DAT_PRIP,
                                             Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                                             Klasa = kl.KLASA,
                                             Obj = p.OBJ,
                                             Box = p.BOX,
                                             Napomena = k.NAPOMENA
                                         }).ToList();

                if (sortOrder == "brDana")
                {
                    return periodSuprasnosti.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return periodSuprasnosti.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }
        #endregion

        #region 04PlanPripusta

        public DataTable PlanPripustaZaGrupuZalucenihKrmaca(DateTime datumZalucenja, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumZalucenjaParam = new SqlParameter("@datumZalucenja", datumZalucenja);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("PlanPripustaZaGrupuZalucenihKrmaca", con))
                    {
                        cmd.Parameters.Add(datumZalucenjaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable PlanPripustaZaGrupuNazimnica(DateTime datumTesta, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumTestaParam = new SqlParameter("@datumTesta", datumTesta);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("PlanPripustaZaGrupuNazimnica", con))
                    {
                        cmd.Parameters.Add(datumTestaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable PlanPripustaZaGrupuMerenihNazimnica(DateTime datumTesta, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumTestaParam = new SqlParameter("@datumTesta", datumTesta);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("PlanPripustaZaGrupuMerenihNazimnica", con))
                    {
                        cmd.Parameters.Add(datumTestaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable DnevniPlanPripusta(DateTime datumZalucenja, string T_Krm, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumZalucenjaParam = new SqlParameter("@datumZalucenja", datumZalucenja);
                var t_KrmParam = new SqlParameter("@t_KrmParam", T_Krm);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DevniPlanPripusta", con))
                    {
                        cmd.Parameters.Add(datumZalucenjaParam);
                        cmd.Parameters.Add(t_KrmParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public List<DnevniPlanPripustaVM> DnevniPlanPripustaReport(DateTime datumZalucenja, string T_Krm)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumZalucenjaParam = new SqlParameter("@datumZalucenja", datumZalucenja);
                var t_KrmParam = new SqlParameter("@t_KrmParam", T_Krm);

                return applicationDbContext.Database.SqlQuery<DnevniPlanPripustaVM>("DnevniPlanPripustaReport @datumZalucenja, @t_KrmParam", datumZalucenjaParam, t_KrmParam).ToList();
            }
        }
        public List<DnevniPlanPripustaVM> PlanPripustaZaGrupuZalucenihKrmacaReport(DateTime datumZalucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumZalucenjaParam = new SqlParameter("@datumZalucenja", datumZalucenja);

                return applicationDbContext.Database.SqlQuery<DnevniPlanPripustaVM>("PlanPripustaZaGrupuZalucenihKrmacaReport @datumZalucenja", datumZalucenjaParam).ToList();
            }
        }
        public List<DnevniPlanPripustaVM> PlanPripustaZaGrupuNazimnicaReport(DateTime datumTesta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumTestaParam = new SqlParameter("@datumTesta", datumTesta);

                return applicationDbContext.Database.SqlQuery<DnevniPlanPripustaVM>("PlanPripustaZaGrupuNazimnicaReport @datumTesta", datumTestaParam).ToList();
            }
        }
        public List<DnevniPlanPripustaVM> PlanPripustaZaGrupuMerenihNazimnicaReport(DateTime datumTesta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumTestaParam = new SqlParameter("@datumTesta", datumTesta);

                return applicationDbContext.Database.SqlQuery<DnevniPlanPripustaVM>("PlanPripustaZaGrupuMerenihNazimnicaReport @datumTesta", datumTestaParam).ToList();
            }
        }
        #endregion

        #region 05ReproduktivniPokazateljKrmaca
        public List<ReproduktivniPokazateljiZalucenihKrmacaVM> ReproduktivniPokazateljiZalucenihKrmaca(DateTime fromDate, DateTime toDate)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);

                var spResult = applicationDbContext.Database.SqlQuery<ReproduktivniPokazateljiZalucenihKrmacaVMProcedura>("ReproduktivniPokazateljiZalucenihKrmaca @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).FirstOrDefault();

                List<ReproduktivniPokazateljiZalucenihKrmacaVM> result = new List<ReproduktivniPokazateljiZalucenihKrmacaVM>();
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 1,
                    Opis = "ZALUCENO",
                    Prvopr = spResult.ZalPrvoPr,
                    Pr_prv = 100.00M,
                    Ostale = spResult.ZalOstale,
                    Prv_ost = 100.00M,
                    Ukupno = spResult.ZalPrvoPr + spResult.ZalOstale,
                    Pr_uk = 100.00M
                });
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 2,
                    Opis = "ISKLJUCENO NA ZALUCENJU",
                    Prvopr = spResult.IskljucenePosleZalucenjaPrvoPr,
                    Pr_prv = spResult.ZalPrvoPr != 0 ? spResult.IskljucenePosleZalucenjaPrvoPr / spResult.ZalPrvoPr * 100.00M : 0,
                    Ostale = spResult.IskljucenePosleZalucenjaOstale,
                    Prv_ost = spResult.ZalOstale != 0 ? spResult.IskljucenePosleZalucenjaOstale / spResult.ZalOstale * 100.00M : 0,
                    Ukupno = spResult.IskljucenePosleZalucenjaPrvoPr + spResult.IskljucenePosleZalucenjaOstale,
                    Pr_uk = (spResult.ZalPrvoPr + spResult.ZalOstale) != 0 ? (spResult.IskljucenePosleZalucenjaPrvoPr + spResult.IskljucenePosleZalucenjaOstale) / (spResult.ZalPrvoPr + spResult.ZalOstale) * 100.00M : 0
                });

                var neiskljucene = new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 3,
                    Opis = "NEISKLJUCENE POSLE ZALUCENJA",
                    Prvopr = spResult.ZalPrvoPr - spResult.IskljucenePosleZalucenjaPrvoPr,
                    Pr_prv = spResult.ZalPrvoPr != 0 ? (spResult.ZalPrvoPr - spResult.IskljucenePosleZalucenjaPrvoPr) / spResult.ZalPrvoPr * 100.00M : 0,
                    Ostale = spResult.ZalOstale - spResult.IskljucenePosleZalucenjaOstale,
                    Prv_ost = spResult.ZalOstale != 0 ? (spResult.ZalOstale - spResult.IskljucenePosleZalucenjaOstale) / spResult.ZalOstale * 100.00M : 0,
                    Ukupno = spResult.ZalPrvoPr - spResult.IskljucenePosleZalucenjaPrvoPr + spResult.ZalOstale - spResult.IskljucenePosleZalucenjaOstale,
                    Pr_uk = (spResult.ZalPrvoPr + spResult.ZalOstale) != 0 ? ((spResult.ZalPrvoPr + spResult.ZalOstale) - (spResult.IskljucenePosleZalucenjaPrvoPr + spResult.IskljucenePosleZalucenjaOstale)) / (spResult.ZalPrvoPr + spResult.ZalOstale) * 100.00M : 0
                };
                result.Add(neiskljucene);
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 4,
                    Opis = "PRIPUSTENO DO 7 DANA",
                    Prvopr = spResult.PripustDo7DanaPrvoPr,
                    Pr_prv = neiskljucene.Prvopr != 0 ? spResult.PripustDo7DanaPrvoPr / neiskljucene.Prvopr * 100.00M : 0,
                    Ostale = spResult.PripustDo7DanaOstale,
                    Prv_ost = neiskljucene.Ostale != 0 ? spResult.PripustDo7DanaOstale / neiskljucene.Ostale * 100.00M : 0,
                    Ukupno = spResult.PripustDo7DanaPrvoPr + spResult.PripustDo7DanaOstale,
                    Pr_uk = (neiskljucene.Prvopr + neiskljucene.Ostale) != 0 ? (spResult.PripustDo7DanaPrvoPr + spResult.PripustDo7DanaOstale) / (neiskljucene.Prvopr + neiskljucene.Ostale) * 100.00M : 0
                });
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 5,
                    Opis = "TRETIRANO HORMONOM",
                    Prvopr = 0,
                    Pr_prv = 0.00M,
                    Ostale = 0,
                    Prv_ost = 0.00M,
                    Ukupno = 0,
                    Pr_uk = 0.00M
                });
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 6,
                    Opis = "OD TOGA PRIPUSTENO",
                    Prvopr = 0,
                    Pr_prv = 0.00M,
                    Ostale = 0,
                    Prv_ost = 0.00M,
                    Ukupno = 0,
                    Pr_uk = 0.00M
                });
                var osemenjeno = new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 7,
                    Opis = "UKUPNO OSEMENJENO",
                    Prvopr = spResult.OsemenjenoPrvoPr,
                    Pr_prv = neiskljucene.Prvopr != 0 ?  spResult.OsemenjenoPrvoPr / neiskljucene.Prvopr * 100.00M : 0,
                    Ostale = spResult.OsemenjenoOstale,
                    Prv_ost = neiskljucene.Ostale != 0 ? spResult.OsemenjenoOstale / neiskljucene.Ostale * 100.00M : 0,
                    Ukupno = spResult.OsemenjenoPrvoPr + spResult.OsemenjenoOstale,
                    Pr_uk = (neiskljucene.Prvopr + neiskljucene.Ostale) != 0 ? (spResult.OsemenjenoPrvoPr + spResult.OsemenjenoOstale) / (neiskljucene.Prvopr + neiskljucene.Ostale) * 100.00M : 0
                };
                var povadjalo = new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 8,
                    Opis = "POVADJALO UKUPNO",
                    Prvopr = spResult.OsemenjenoPrvoPr,
                    Pr_prv = osemenjeno.Prvopr != 0 ? spResult.OsemenjenoPrvoPr / osemenjeno.Prvopr * 100.00M : 0,
                    Ostale = spResult.OsemenjenoOstale,
                    Prv_ost = osemenjeno.Ostale != 0 ? spResult.OsemenjenoOstale / osemenjeno.Ostale * 100.00M : 0,
                    Ukupno = spResult.OsemenjenoPrvoPr + spResult.OsemenjenoOstale,
                    Pr_uk = (osemenjeno.Prvopr + osemenjeno.Ostale) != 0 ? (spResult.OsemenjenoPrvoPr + spResult.OsemenjenoOstale) / (osemenjeno.Prvopr + osemenjeno.Ostale) * 100.00M : 0
                };
                result.Add(povadjalo);
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 9,
                    Opis = "POVADJALO DO 25 DANA",
                    Prvopr = spResult.PovadjaloDo25PrvoPr,
                    Pr_prv = povadjalo.Prvopr != 0 ? spResult.PovadjaloDo25PrvoPr / povadjalo.Prvopr * 100.00M : 0,
                    Ostale = spResult.PovadjaloDo25Ostale,
                    Prv_ost = povadjalo.Ostale != 0 ? spResult.PovadjaloDo25Ostale / povadjalo.Ostale * 100.00M : 0,
                    Ukupno = spResult.PovadjaloDo25PrvoPr + spResult.PovadjaloDo25Ostale,
                    Pr_uk = (povadjalo.Prvopr + povadjalo.Ostale) != 0 ? (spResult.PovadjaloDo25PrvoPr + spResult.PovadjaloDo25Ostale) / (povadjalo.Prvopr + povadjalo.Ostale) * 100.00M : 0
                });
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 10,
                    Opis = "POVADJALO PREKO 25 DANA",
                    Prvopr = spResult.PovadjaloPreko25PrvoPr,
                    Pr_prv = povadjalo.Prvopr != 0 ? spResult.PovadjaloPreko25PrvoPr / povadjalo.Prvopr * 100.00M : 0,
                    Ostale = spResult.PovadjaloPreko25Ostale,
                    Prv_ost = povadjalo.Ostale != 0 ?  spResult.PovadjaloPreko25Ostale / povadjalo.Ostale * 100.00M : 0,
                    Ukupno = spResult.PovadjaloPreko25PrvoPr + spResult.PovadjaloPreko25Ostale,
                    Pr_uk = (povadjalo.Prvopr + povadjalo.Ostale) != 0 ? (spResult.PovadjaloPreko25PrvoPr + spResult.PovadjaloPreko25Ostale) / (povadjalo.Prvopr + povadjalo.Ostale) * 100.00M : 0
                });
                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 11,
                    Opis = "UKUPNO OPRASENO",
                    Prvopr = spResult.UkupnoOprasenoPrvoPr,
                    Pr_prv = osemenjeno.Prvopr != 0 ? spResult.UkupnoOprasenoPrvoPr / osemenjeno.Prvopr * 100.00M : 0,
                    Ostale = spResult.UkupnoOprasenoOstale,
                    Prv_ost = osemenjeno.Ostale != 0 ? spResult.PovadjaloPreko25Ostale / osemenjeno.Ostale * 100.00M : 0,
                    Ukupno = spResult.UkupnoOprasenoPrvoPr + spResult.UkupnoOprasenoOstale,
                    Pr_uk = (osemenjeno.Prvopr + osemenjeno.Ostale) != 0 ? (spResult.PovadjaloPreko25PrvoPr + spResult.PovadjaloPreko25Ostale) / (osemenjeno.Prvopr + osemenjeno.Ostale) * 100.00M : 0
                });

                result.Add(new ReproduktivniPokazateljiZalucenihKrmacaVM()
                {
                    Rb = 12,
                    Opis = "OPRASENO OD PRVOG PRIPUSTA",
                    Prvopr = spResult.OprasenoOdPrvogPripustaPrvoPr,
                    Pr_prv = osemenjeno.Prvopr != 0 ? spResult.OprasenoOdPrvogPripustaPrvoPr / osemenjeno.Prvopr * 100.00M : 0,
                    Ostale = spResult.OprasenoOdPrvogPripustaOstale,
                    Prv_ost = osemenjeno.Ostale != 0 ? spResult.OprasenoOdPrvogPripustaOstale / osemenjeno.Ostale * 100.00M : 0,
                    Ukupno = spResult.OprasenoOdPrvogPripustaPrvoPr + spResult.OprasenoOdPrvogPripustaOstale,
                    Pr_uk = (osemenjeno.Prvopr + osemenjeno.Ostale) != 0 ? (spResult.OprasenoOdPrvogPripustaPrvoPr + spResult.OprasenoOdPrvogPripustaOstale) / (osemenjeno.Prvopr + osemenjeno.Ostale) * 100.00M : 0
                });

                //jos tri itema treba da dodam u listu, ali prvo da vidim koj su iskljucene a da nisu jalove, kako to da fltriram
                return result;
            }
        }
        public List<ReproduktivniPokazateljiZalucenihKrmacaPoMesecimaVM> ReproduktivniPokazateljiZalucenihKrmacaPoMesecima(DateTime periodOd, DateTime periodDo)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region 06KontrolaProduktivnostiNerastova
        public List<OprasivostPoNerastovimaReportVM> ProduktivnostNerastova(DateTime periodOd, DateTime periodDo)
        {
            return OprasivostPoNerastovima(periodOd, periodDo);
        }
        public List<BioloskiTestNerastovaVM> BioloskiTestNerastova(DateTime periodOd, DateTime periodDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@DanOd", periodOd);
                var toParametar = new SqlParameter("@DanDo", periodDo); 
                return applicationDbContext.Database.SqlQuery<BioloskiTestNerastovaVM>("Pripust.BioloskiTestNerastova @DanOd, @DanDo", fromParametar, toParametar).ToList();
            }
        }
        public List<BioloskiTestMladihNerastovaVM> BioloskiTestMladihNerastova(DateTime periodOd, DateTime periodDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@DanOd", periodOd);
                var toParametar = new SqlParameter("@DanDo", periodDo);
                return applicationDbContext.Database.SqlQuery<BioloskiTestMladihNerastovaVM>("Pripust.BioloskiTestMladihNerastova @DanOd, @DanDo", fromParametar, toParametar).ToList();
            }
        }
        public DataTable KalendarKoriscenjaNerastova(DateTime periodOd, DateTime periodDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", periodOd);
                var toParametar = new SqlParameter("@dateTimeTo", periodDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KalendarKoriscenjaNerastova", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public List<KalendarKoriscenjaNerastovaReportVM> KalendarKoriscenjaNerastovaReport(DateTime periodOd, DateTime periodDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", periodOd);
                var toParametar = new SqlParameter("@dateTimeTo", periodDo);
                return applicationDbContext.Database.SqlQuery<KalendarKoriscenjaNerastovaReportVM>("KalendarKoriscenjaNerastovaReport @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
            }
        }
        #endregion

        #region 07KontrolaProduktivnostiOsemenitelja
        public List<KontrolaProduktivnostiOsemeniteljaReportVM> KontrolaProduktivnostiOsemenitelja(DateTime periodOd, DateTime periodDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", periodOd);
                var toParametar = new SqlParameter("@dateTimeTo", periodDo);
                return applicationDbContext.Database.SqlQuery<KontrolaProduktivnostiOsemeniteljaReportVM>("KontrolaProduktivnostiOsemenitelja @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
            }
        }
        #endregion

        #region 08ProblemiUReprodukciji
        public List<ProblemiUReprodukcijiVM> ProblemiUReprodukciji(string osemenjavanjePovadjanje, DateTime periodOd, DateTime periodDo)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region 09Analize
        //public List<ProblemiUReprodukcijiVM> OprasivostPoRasamaNerastova(DateTime periodOd, DateTime periodDo)
        //{
        //    throw new NotImplementedException();
        //}
        #endregion

        #region CRUD
        #endregion
    }
}
