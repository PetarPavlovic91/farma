﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class IzvestajiService
    {

        public DataTable IzvodIzGlavneMaticneEvidencije(DateTime dan, int paritet, bool isHBRB, bool isNB, bool isNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParam = new SqlParameter("@Dan", dan);
                var paritetParam = new SqlParameter("@Paritet", paritet);
                var isHBRBParam = new SqlParameter("@IsHBRB", isHBRB);
                var isNBParam = new SqlParameter("@IsNB", isNB);
                var iisNerastoviParam = new SqlParameter("@IsNerastovi", isNerastovi);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.IzvodIzGlavneMaticneEvidencije", con))
                    {
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.Parameters.Add(isHBRBParam);
                        cmd.Parameters.Add(isNBParam);
                        cmd.Parameters.Add(iisNerastoviParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable MUSKontrolaProduktivnostiUmaticenih(DateTime datumOdPrasenje, DateTime datumDoPrasenje, string procedura)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdPrasenjeParam = new SqlParameter("@DatumOdPrasenje", datumOdPrasenje);
                var datumDoPrasenjeParam = new SqlParameter("@DatumDoPrasenje", datumDoPrasenje);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(procedura, con))
                    {
                        cmd.Parameters.Add(datumOdPrasenjeParam);
                        cmd.Parameters.Add(datumDoPrasenjeParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable KomisijskiSpisakZaNazimice(DateTime datumOd, string rasa, bool isGrlaUTestu)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var isGrlaUTestuParam = new SqlParameter("@IsGrlaUTestu", isGrlaUTestu);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.KomisijskiSpisakZaNazimice", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(isGrlaUTestuParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable KomisijskiSpisakZaNerastove(DateTime datumOd, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.KomisijskiZapisnikZaNerastove", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable KomisijskiZapisnikZaKrmaceDopuna(DateTime datumOd, DateTime datumOdPrasenje, DateTime datumDoPrasenje, int paritet, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumOdPrasenjeParam = new SqlParameter("@DatumOdPrasenje", datumOdPrasenje);
                var datumDoPrasenjeParam = new SqlParameter("@DatumDoPrasenje", datumDoPrasenje);
                var paritetParam = new SqlParameter("@Paritet", paritet);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.KomisijskiZapisnikZaKrmaceDopuna", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumOdPrasenjeParam);
                        cmd.Parameters.Add(datumDoPrasenjeParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable KomisijskiZapisnikZaKrmace(DateTime datumOd, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.KomisijskiZapisnikZaKrmace", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable UltrazvucnoMerenje(DateTime datumOd, bool test)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var isTestParam = new SqlParameter("@IsTest", test);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.UltrazvucnoMerenje", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(isTestParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable ZahtevZaIzdavanjeDozvoleOKoriscenjuNerastovaUPriplodu(DateTime datumOd)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.ZahtevZaIzdavanjeDozvoleOKoriscenjuNerastovaUPriplodu", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable PrijavaPocetkaBioloskogTesta(DateTime datumOd)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.PrijavaPocetkaBioloskogTesta", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable ZahtevZaRegistracijuZenskaGrla(DateTime datumOd, DateTime datumDo, string rasa, int paritet)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                var paritetParam = new SqlParameter("@Paritet", paritet);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.ZahtevZaRegistracijuZenskaGrla", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable ZahtevZaRegistracijuMuskaGrla(DateTime datumOd)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.ZahtevZaRegistracijuMuskaGrla", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable Registri_SkartGrla(DateTime datumOd, DateTime datumDo, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.Registri_SkartGrla", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable Registar_Zalucenja(DateTime datumOd, DateTime datumDo, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.Registar_Zalucenja", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable Registri_RegistarPrasadi(DateTime datumOd, DateTime datumDo, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.Registri_RegistarPrasadi", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable RegistarPriplodnogPodmladka(DateTime datumOd, DateTime datumDo, string rasa, bool muski, bool zenski)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                var muskiParam = new SqlParameter("@Muski", muski);
                var zenskiParam = new SqlParameter("@Zenski", zenski);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.RegistarPriplodnogPodmladka", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(muskiParam);
                        cmd.Parameters.Add(zenskiParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable Registri_Prasenja(DateTime datumOd, DateTime datumDo, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.Registri_Prasenja", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable Registri_Pripusti(DateTime datumOd, DateTime datumDo, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var datumOdParam = new SqlParameter("@DatumOd", datumOd);
                var datumDoParam = new SqlParameter("@DatumDo", datumDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.Registri_Pripusti", con))
                    {
                        cmd.Parameters.Add(datumOdParam);
                        cmd.Parameters.Add(datumDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable GrlaNaGazdinstvu(DateTime dan, int danaOdTesta, int starostPrasadiOd, int starostPrasadiDo, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParam = new SqlParameter("@Dan", dan);
                var danaOdTestaParam = new SqlParameter("@DanaOdTesta", danaOdTesta);
                var starostPrasadiOdParam = new SqlParameter("@StarostPrasadiOd", starostPrasadiOd);
                var starostPrasadiDoParam = new SqlParameter("@StarostPrasadiDo", starostPrasadiDo);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.GrlaNaGazdinstvu", con))
                    {
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(danaOdTestaParam);
                        cmd.Parameters.Add(starostPrasadiOdParam);
                        cmd.Parameters.Add(starostPrasadiDoParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable IzvodIzGlavneMaticneEvidencijeDopuna(DateTime dan, int paritet, bool isHBRB, bool isNB, DateTime prasenjeOd, DateTime prasenjeDo, string procedura)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParam = new SqlParameter("@Dan", dan);
                var paritetParam = new SqlParameter("@Paritet", paritet);
                var isHBRBParam = new SqlParameter("@IsHBRB", isHBRB);
                var isNBParam = new SqlParameter("@IsNB", isNB);
                var prasenjeOdParam = new SqlParameter("@PrasenjeOd", prasenjeOd);
                var prasenjeDoParam = new SqlParameter("@PrasenjeDo", prasenjeDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(procedura, con))
                    {
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.Parameters.Add(isHBRBParam);
                        cmd.Parameters.Add(isNBParam);
                        cmd.Parameters.Add(prasenjeOdParam);
                        cmd.Parameters.Add(prasenjeDoParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable IzvodIzGlavneMaticneEvidencijeAnaliza(DateTime dan, int paritet, bool isHBRB, bool isNB, bool isNerastovi, string procedura)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParam = new SqlParameter("@Dan", dan);
                var paritetParam = new SqlParameter("@Paritet", paritet);
                var isHBRBParam = new SqlParameter("@IsHBRB", isHBRB);
                var isNBParam = new SqlParameter("@IsNB", isNB);
                var iisNerastoviParam = new SqlParameter("@IsNerastovi", isNerastovi);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(procedura, con))
                    {
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.Parameters.Add(isHBRBParam);
                        cmd.Parameters.Add(isNBParam);
                        cmd.Parameters.Add(iisNerastoviParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

    }
}
