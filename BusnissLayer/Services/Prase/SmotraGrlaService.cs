﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class SmotraGrlaService
    {
        public DataTable RezimeSmotreTrenutnoStanjeGrla(string rasa, string procedura)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(procedura, con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable RezimeSmotreTrenutnoStanjeGrlaSamoKrmace(string rasa, string procedura, DateTime dan)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                var danParam = new SqlParameter("@dan", dan);
                var paritetParam = new SqlParameter("@paritet", 100);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(procedura, con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable RezimeSmotreTrenutnoStanjeGrlaSamoKrmaceDopuna(string rasa, string procedura, DateTime dan, DateTime danDo, int paritet)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                var danParam = new SqlParameter("@dan", dan);
                var danDoParam = new SqlParameter("@danDo", danDo);
                var paritetParam = new SqlParameter("@paritet", paritet);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(procedura, con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(danDoParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public bool BackupBaze()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var backupLocation = "C:\\ProjectFarma2021\\Backups\\";
                Directory.CreateDirectory(backupLocation);
                var backupLocationParam = new SqlParameter("@backupLocation", backupLocation);
                var res = applicationDbContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "DatabaseBackup @backupLocation", backupLocationParam);

                var backupLocationUSB = "";
                if (Directory.Exists("E:\\Backups\\"))
                {
                    backupLocationUSB = "E:\\Backups\\";
                }
                else if (Directory.Exists("F:\\Backups\\"))
                {
                    backupLocationUSB = "F:\\Backups\\";
                }
                else if (Directory.Exists("G:\\Backups\\"))
                {
                    backupLocationUSB = "G:\\Backups\\";
                }
                else if (Directory.Exists("D:\\Backups\\"))
                {
                    backupLocationUSB = "D:\\Backups\\";
                }
                if (!string.IsNullOrEmpty(backupLocationUSB))
                {
                    var backupLocationUSBParam = new SqlParameter("@backupLocation", backupLocationUSB);
                    var resUSB = applicationDbContext.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "DatabaseBackup @backupLocation", backupLocationUSBParam);
                }
                return true;
            }
        }

        public DataTable KomisijskiZapisnikZaNerastove(string rasa, DateTime dan, DateTime dateOd, DateTime dateDo, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@Rasa", rasa);
                var danParam = new SqlParameter("@DatumOd", dan);
                var dateOdParam = new SqlParameter("@DatumOdPrasenje", dateOd);
                var dateDoParam = new SqlParameter("@DatumDoPrasenje", dateDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Izvestaji.KomisijskiZapisnikZaNerastove", con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(dateOdParam);
                        cmd.Parameters.Add(dateDoParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable SmotraNerastovaRadniZapisnik(DateTime pripustOd, DateTime pripustDo, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var pripustOdParam = new SqlParameter("@pripustOd", pripustOd);
                var pripustDoParam = new SqlParameter("@pripustDo", pripustDo);
                var sviNerastoviParametar = new SqlParameter("@sviNerastovi", sviNerastovi);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SmotraNerastovaRadniZapisnik", con))
                    {
                        cmd.Parameters.Add(pripustOdParam);
                        cmd.Parameters.Add(pripustDoParam);
                        cmd.Parameters.Add(sviNerastoviParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable KomisijskiSpisakZaKrmace(DateTime dan, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                var danParam = new SqlParameter("@dan", dan);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KomisijskiSpisakZaKrmace", con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable KomisijskiSpisakZaNazimice(DateTime dan, string rasa, bool isTest)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                var danParam = new SqlParameter("@dan", dan);
                var isTestParam = new SqlParameter("@isTest", isTest);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KomisijskiSpisakZaNazimice", con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(isTestParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable SmotraMladihNerastova()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SmotraMladihNerastovaUTestuRadniZapisnik", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable SmotraProizvodnjaKrmacaPoParitetima()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SmotraProizvodnjaKrmacaPoParitetima", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable SmotraProizvodnjaKrmacaPoKlasama()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SmotraProizvodnjaKrmacaPoKlasama", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable SmotraProizvodnjaKrmacaPoRasama()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SmotraProizvodnjaKrmacaPoRasama", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable RezimeSMotreRadniSpisakZaKrmace(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("RezimeSmotreZaKrmaceRadniSpisak", con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable RezimeSmotreZaKrmaceRadniSpisakDopuna(string rasa, DateTime danOd, DateTime danDo, int paritet)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                var danOdParam = new SqlParameter("@danOd", danOd);
                var danDoParam = new SqlParameter("@danDo", danDo);
                var paritetParam = new SqlParameter("@paritet", paritet);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("RezimeSmotreZaKrmaceRadniSpisakDopuna", con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danOdParam);
                        cmd.Parameters.Add(danDoParam);
                        cmd.Parameters.Add(paritetParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable KomisijskiSpisakZaKrmaceDopuna(string rasa, DateTime dan, DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParam = new SqlParameter("@rasa", rasa);
                var danParam = new SqlParameter("@dan", dan);
                var dateOdParam = new SqlParameter("@prasenjeOd", dateOd);
                var dateDoParam = new SqlParameter("@prasenjeDo", dateDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KomisijskiSpisakZaKrmaceDopuna", con))
                    {
                        cmd.Parameters.Add(rasaParam);
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(dateOdParam);
                        cmd.Parameters.Add(dateDoParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public int GetCountKrmacaByRasa(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        where ss.Count() == 0 && k.RASA.Contains(rasa)
                        select k.Id).Count();
            }
        }

        public int GetCountKrmacaByRasaDopuna(string rasa, DateTime odDate, DateTime doDate, int paritet = 100)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca
                        join s in applicationDbContext.Prasenje on k.Id equals s.KrmacaId
                        where s.DAT_PRAS >= odDate && s.DAT_PRAS <= doDate && k.RASA.Contains(rasa) && k.PARIT <= paritet
                        select k.Id).Count();
            }
        }
        public int GetCountNazimiceByRasa(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        where ss.Count() == 0 && k.RASA.Contains(rasa) && k.CIKLUS == 1
                        select k.Id).Count();
            }
        }


        public int GetCountNerastoviByRasa(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Nerast
                        join s in applicationDbContext.Skart on k.Id equals s.NerastId into ss
                        where ss.Count() == 0 && k.RASA.Contains(rasa)
                        select k.Id).Count();
            }
        }
    }
}
