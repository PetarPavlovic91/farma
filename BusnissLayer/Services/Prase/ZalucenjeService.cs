﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase._03Zalucenje;
using BusnissLayer.ViewModels.Prase.Zalucenje;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class ZalucenjeService
    {
        #region 01EvidencijaZalucenja
        //public ReturnObject Save(EvidencijaZalucenjaAzuriranjeVM zalucenje)
        //{
        //    using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        //    {

        //        if (zalucenje.Id == 0)
        //        {
        //            var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == zalucenje.Krmaca);
        //            if (krmaca == null)
        //            {
        //                return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
        //            }
        //            var entity = applicationDbContext.Zaluc.Add(new Zaluc()
        //            {
        //                DAT_ZAL = zalucenje.DatumZalucenja,
        //                ZDR_ST = zalucenje.ZDR_ST,
        //                ZALUC = zalucenje.ZALUC,
        //                TEZ_ZAL = zalucenje.TEZ_ZAL,
        //                DOJARA = zalucenje.DOJARA,
        //                BOX = zalucenje.BOX,
        //                OBJ = zalucenje.OBJ,
        //                OL_Z = zalucenje.OL_Z,
        //                PZ = zalucenje.PZ,
        //                RAZ_PZ = zalucenje.RAZ_PZ,
        //                PARIT = zalucenje.PARIT,
        //                RBZ = zalucenje.RBZ,
        //                KrmacaId = krmaca.Id
        //            });
        //            applicationDbContext.SaveChanges();
        //            return new ReturnObject() { Success = true, Id = entity.Id };
        //        }
        //        else
        //        {
        //            var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == zalucenje.Krmaca);
        //            if (krmaca == null)
        //            {
        //                return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
        //            }
        //            var entity = applicationDbContext.Zaluc.FirstOrDefault(x => x.Id == zalucenje.Id);
        //            if (entity == null)
        //            {
        //                return new ReturnObject() { Success = false, Message = "Zalucenje nije pronadjeno" };
        //            }
        //            entity.DAT_ZAL = zalucenje.DatumZalucenja;
        //            entity.ZDR_ST = zalucenje.ZDR_ST;
        //            entity.ZALUC = zalucenje.ZALUC;
        //            entity.TEZ_ZAL = zalucenje.TEZ_ZAL;
        //            entity.DOJARA = zalucenje.DOJARA;
        //            entity.BOX = zalucenje.BOX;
        //            entity.OBJ = zalucenje.OBJ;
        //            entity.OL_Z = zalucenje.OL_Z;
        //            entity.PZ = zalucenje.PZ;
        //            entity.RAZ_PZ = zalucenje.RAZ_PZ;
        //            entity.PARIT = zalucenje.PARIT;
        //            entity.RBZ = zalucenje.RB;
        //            applicationDbContext.SaveChanges();
        //            return new ReturnObject() { Success = true, Id = entity.Id };
        //        }
        //    }
        //}

        public string CheckIskljucenje(string krmaca)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var res = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == krmaca);
                if (res == null)
                    return "Ova krmaca ne postoji";
                if (applicationDbContext.Skart.Any(x => x.KrmacaId == res.Id))
                    return "Ova krmaca je iskljucena";
                if (res.FAZA != 2)
                    return "Ova krmaca nije u odgovarajucoj predfazi.";
                return null;
            }
        }
        public List<EvidencijaZalucenjaPregledVM> EvidencijaPrasenjaPregled(DateTime datumZalucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Zaluc.Where(x => DbFunctions.TruncateTime(x.DAT_ZAL) == datumZalucenja.Date)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new EvidencijaZalucenjaPregledVM()
                        {
                            Krmaca = k.T_KRM,
                            RB = p.RB,
                            PARIT = p.PARIT,
                            OBJ = p.OBJ,
                            BOX = p.BOX,
                            REFK = p.REFK,
                            DOJARA = p.DOJARA,
                            OL_Z = p.OL_Z,
                            PZ = p.PZ,
                            RAZ_PZ = p.RAZ_PZ,
                            RBZ = p.RBZ,
                            TEZ_ZAL = p.TEZ_ZAL,
                            ZALUC = p.ZALUC,
                            ZDR_ST = p.ZDR_ST
                        }).ToList();
            }
        }

        public List<EvidencijaZalucenjaAzuriranjeVM> GetEvidencijaZalucenjaAzuriranjeVM(DateTime datumZalucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Zaluc.Where(x => DbFunctions.TruncateTime(x.DAT_ZAL) == datumZalucenja.Date)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new EvidencijaZalucenjaAzuriranjeVM()
                        {
                            Id = p.Id,
                            Krmaca = k.T_KRM,
                            RB = p.RB,
                            PARIT = p.PARIT,
                            OBJ = p.OBJ,
                            BOX = p.BOX,
                            REFK = p.REFK,
                            DOJARA = p.DOJARA,
                            OL_Z = p.OL_Z,
                            PZ = p.PZ,
                            RAZ_PZ = p.RAZ_PZ,
                            RBZ = p.RBZ,
                            TEZ_ZAL = p.TEZ_ZAL,
                            ZALUC = p.ZALUC,
                            ZDR_ST = p.ZDR_ST
                        }).ToList();
            }
        }

        //public ReturnObject Delete(int id)
        //{
        //    using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
        //    {
        //        var entity = applicationDbContext.Zaluc.FirstOrDefault(x => x.Id == id);
        //        if (entity == null)
        //        {
        //            return new ReturnObject() { Success = false, Message = "Prasenje nije pronadjeno" };
        //        }
        //        applicationDbContext.Zaluc.Remove(entity);
        //        applicationDbContext.SaveChanges();
        //        return new ReturnObject() { Success = true };
        //    }
        //}

        public object SP_GetAllByDate(DateTime datumZalucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumZalucenja.Year, datumZalucenja.Month, datumZalucenja.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Zalucenje.GetAllByDate", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public void SP_Save(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Zalucenje.Save", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SP_Save_KlasaKrmace(string tetovir)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == tetovir);
                if (krmaca != null)
                {
                    var prosecnoZalucenihPrasadi = applicationDbContext.Zaluc.Where(x => x.KrmacaId == krmaca.Id).Average(x => x.ZALUC);
                    if (prosecnoZalucenihPrasadi != null)
                    {
                        if (prosecnoZalucenihPrasadi >= 8 && prosecnoZalucenihPrasadi < 9)
                        {
                            krmaca.KLASA = "II";
                        }
                        else if (prosecnoZalucenihPrasadi >= 9 && prosecnoZalucenihPrasadi < 9.5)
                        {
                            krmaca.KLASA = "I";
                        }
                        else if (prosecnoZalucenihPrasadi >= 9.5 && prosecnoZalucenihPrasadi < 10)
                        {
                            krmaca.KLASA = "I";
                            if (krmaca.CIKLUS >= 2)
                                krmaca.KLASA = "IA";
                        }
                        else if (prosecnoZalucenihPrasadi >= 10)
                        {
                            krmaca.KLASA = "I";
                            if (krmaca.CIKLUS >= 3)
                                krmaca.KLASA = "E";
                        }
                        applicationDbContext.SaveChanges();
                    }
                }
            }
        }
        #endregion
        #region PregledPodataka

        public List<PregledPodatakaZalucenjeVM> PregledPodatakaZalucenja(DateTime dateFrom, DateTime dateTo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from z in applicationDbContext.Zaluc.Where(x => DbFunctions.TruncateTime(x.DAT_ZAL) >= dateFrom.Date && DbFunctions.TruncateTime(x.DAT_ZAL) <= dateTo.Date)
                        join k in applicationDbContext.Krmaca on z.KrmacaId equals k.Id
                        join p in applicationDbContext.Prasenje on k.Id equals p.KrmacaId
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        where p.PARIT == z.PARIT
                        select new PregledPodatakaZalucenjeVM()
                        {
                            Krmaca = k.T_KRM,
                            Box = z.BOX,
                            Gaji = p.GAJI,
                            Zaluc = z.ZALUC,
                            Obj = z.OBJ,
                            Ol_z = z.OL_Z,
                            Parit = z.PARIT,
                            Pz = z.PZ,
                            RazPz = z.RAZ_PZ,
                            Rasa = k.RASA,
                            Sk = s.RAZL_SK,
                            Tezina = z.TEZ_ZAL,
                            Zalucena = z.DAT_ZAL,
                            Zdr_st = z.ZDR_ST,
                            Zivo = p.ZIVO,
                            Lakt = (int)DbFunctions.DiffDays(p.DAT_PRAS, z.DAT_ZAL)
                            //leo ne zanm sta je
                        }).ToList();
            }
        }

        public List<PregledPodatakaZalucenjeVM> AnalizaPrinudnoZalucenihKrmaca(DateTime dateFrom, DateTime dateTo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from z in applicationDbContext.Zaluc.Where(x => DbFunctions.TruncateTime(x.DAT_ZAL) >= dateFrom.Date && DbFunctions.TruncateTime(x.DAT_ZAL) <= dateTo.Date)
                        join k in applicationDbContext.Krmaca on z.KrmacaId equals k.Id
                        join p in applicationDbContext.Prasenje on k.Id equals p.KrmacaId
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        where !string.IsNullOrEmpty(z.PZ) && p.PARIT == z.PARIT
                        select new PregledPodatakaZalucenjeVM()
                        {
                            Krmaca = k.T_KRM,
                            Box = z.BOX,
                            Gaji = p.GAJI,
                            Zaluc = z.ZALUC,
                            Obj = z.OBJ,
                            Ol_z = z.OL_Z,
                            Parit = z.PARIT,
                            Pz = z.PZ,
                            RazPz = z.RAZ_PZ,
                            Rasa = k.RASA,
                            Sk = s.RAZL_SK,
                            Tezina = z.TEZ_ZAL,
                            Zalucena = z.DAT_ZAL,
                            Zdr_st = z.ZDR_ST,
                            Zivo = p.ZIVO,
                            Lakt = (int)DbFunctions.DiffDays(p.DAT_PRAS, z.DAT_ZAL)
                            //leo ne zanm sta je
                        }).ToList();
            }
        }

        public void SP_Delete(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Zalucenje.Delete", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public bool IsLatestZalucenje(int prasenjeId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var zalucenje = applicationDbContext.Zaluc.First(x => x.Id == prasenjeId);

                var latestZalucenje = applicationDbContext.Zaluc.Where(x => x.KrmacaId == zalucenje.KrmacaId).OrderByDescending(x => x.DAT_ZAL).First();

                if (zalucenje.DAT_ZAL < latestZalucenje.DAT_ZAL)
                {
                    return false;
                }
                return true;
            }
        }

        #endregion
        #region 03KalendarZalucenja
        public List<KrmaceZaZalucenjeVM> KrmaceZaZalucenje(int brDana, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var result = (from p in applicationDbContext.Prasenje.Include(x => x.Krmaca).Where(x => DbFunctions.AddDays(x.DAT_PRAS, brDana) <= DateTime.Now)
                              join z in applicationDbContext.Zaluc on new { PropertyName1 = p.KrmacaId, PropertyName2 = p.PARIT } equals new { PropertyName1 = z.KrmacaId, PropertyName2 = (int)z.PARIT } into zz
                              join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                              join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                              where zz.Count() == 0 && ss.Count() == 0
                                select new KrmaceZaZalucenjeVM()
                                {
                                    Krmaca = k.T_KRM,
                                    Box = p.BOX,
                                    Markica =k.MARKICA,
                                    Obj = p.OBJ,
                                    Oprasena = p.DAT_PRAS,
                                    Parit = p.PARIT,
                                    Prasadi = p.ZIVO,
                                    Rasa =k.RASA,
                                    Rbz = p.RB,
                                    Dana = DbFunctions.DiffDays(p.DAT_PRAS, DateTime.Now)
                                }).ToList();

                if (sortOrder == "Dani")
                {
                    return result.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return result.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }


        public DataTable SP_GetAllByDateReport(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danOdParametar = new SqlParameter("@danOd", new DateTime(datumOd.Year, datumOd.Month, datumOd.Day, 0, 0, 0));
                var danDoParametar = new SqlParameter("@danDo", new DateTime(datumDo.Year, datumDo.Month, datumDo.Day, 23, 59, 59));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Zalucenje.GetAllByDateReport", con))
                    {
                        cmd.Parameters.Add(danOdParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public object LeglaZaTetoviranjePrasadi(DateTime dateTimeFrom, DateTime dateTimeTo, string rasa, string sort)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danOdParametar = new SqlParameter("@dateTimeFrom", new DateTime(dateTimeFrom.Year, dateTimeFrom.Month, dateTimeFrom.Day, 0, 0, 0));
                var danDoParametar = new SqlParameter("@dateTimeTo", new DateTime(dateTimeTo.Year, dateTimeTo.Month, dateTimeTo.Day, 23, 59, 59));
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Zalucenje.LeglaZaTetoviranjePrasadi", con))
                    {
                        cmd.Parameters.Add(danOdParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.Parameters.Add(rasaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        #endregion
    }
}
