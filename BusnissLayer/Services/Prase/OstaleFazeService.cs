﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase._05OstaleFaze;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class OstaleFazeService
    {
        #region 02DetekcijaSuprasnosti
        public List<PrvaDetekcijaSuprasnostiVM> PrvaDetekcijaSuprasnosti(int brDanaOd, int brDanaDo, string sort)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) >= brDanaOd && DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) <= brDanaDo)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join s in applicationDbContext.Suprasnost on p.Id equals s.PripustId into ss
                        where ss.Count() == 0 || ss.All(x => !x.DrugaSuprasnost && !x.PrvaSuprasnost && !x.NijeSuprasna)
                        select new PrvaDetekcijaSuprasnostiVM()
                        {
                            Box = p.BOX,
                            Ciklus = k.CIKLUS,
                            Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                            Krmaca = k.T_KRM,
                            Obj = p.OBJ,
                            Pripustena = p.DAT_PRIP,
                            Rasa = k.RASA
                        }).ToList();
            }
        }

        public List<PrvaDetekcijaSuprasnostiVM> DrugaDetekcijaSuprasnosti(int brDanaOd, int brDanaDo, string sort)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) >= brDanaOd && DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) <= brDanaDo)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join s in applicationDbContext.Suprasnost on p.Id equals s.PripustId
                        where s.PrvaSuprasnost && !s.DrugaSuprasnost && !s.NijeSuprasna
                        select new PrvaDetekcijaSuprasnostiVM()
                        {
                            Box = p.BOX,
                            Ciklus = k.CIKLUS,
                            Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                            Krmaca = k.T_KRM,
                            Obj = p.OBJ,
                            Pripustena = p.DAT_PRIP,
                            Rasa = k.RASA
                        }).ToList();
            }
        }

        public List<EvidencijaOPrasenjuILaktacijiVM> EvidencijaOPrasenjuILaktaciji(int brDanaOd, int brDanaDo, string sort)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => DbFunctions.DiffDays(x.DAT_PRAS, DateTime.Now) >= brDanaOd && DbFunctions.DiffDays(x.DAT_PRAS, DateTime.Now) <= brDanaDo)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join z in applicationDbContext.Zaluc on k.Id equals z.KrmacaId into zz
                        where p.PARIT == k.CIKLUS && !zz.Any(x => x.PARIT == k.CIKLUS)
                        select new EvidencijaOPrasenjuILaktacijiVM()
                        {
                            Box = p.BOX,
                            Ciklus = k.CIKLUS,
                            Dana = DbFunctions.DiffDays(p.DAT_PRAS, DateTime.Now),
                            Krmaca = k.T_KRM,
                            Obj = p.OBJ,
                            Oprasena = p.DAT_PRAS,
                            Rasa = k.RASA
                        }).ToList();
            }
        }

        public void SP_Save_Pobacaj(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.SP_Save_Pobacaj", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public List<SemeVM> EvidencijaPregledaSemena(int brDanaOd, int brDanaDo, string sort)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from x in applicationDbContext.Seme.Where(x => DbFunctions.DiffDays(x.DAT_SEME, DateTime.Now) >= brDanaOd && DbFunctions.DiffDays(x.DAT_SEME, DateTime.Now) <= brDanaDo)
                        select new SemeVM()
                        {
                            DAT_SEME = x.DAT_SEME,
                            RB = x.RB,
                            MARKICA = x.MARKICA,
                            RB_UZ = x.RB_UZ,
                            LIBIDO = x.LIBIDO,
                            KOLIC = x.KOLIC,
                            GUST = x.GUST,
                            BR_U_ML = x.BR_U_ML,
                            VALO = x.VALO,
                            POKR = x.POKR,
                            P_MRT = x.P_MRT,
                            P_MORF = x.P_MORF,
                            UPOT = x.UPOT,
                            RAZR = x.RAZR,
                            PRIP = x.PRIP,
                            KOL_RS = x.KOL_RS,
                            DOZA = x.DOZA,
                            POK_RS = x.POK_RS,
                            DAT_PREG = x.DAT_PREG,
                            POK_PS = x.POK_PS,
                            NAPOMENA = x.NAPOMENA,
                            REFK = x.REFK
                        }).ToList();
            }
        }
        public List<SemeVM> PregledPodatakaSemena(DateTime datumOd, DateTime datumDo)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from x in applicationDbContext.Seme.Where(x => x.DAT_SEME >= datumOd && x.DAT_SEME <= datumDo)
                        select new SemeVM()
                        {
                            DAT_SEME = x.DAT_SEME,
                            RB = x.RB,
                            MARKICA = x.MARKICA,
                            RB_UZ = x.RB_UZ,
                            LIBIDO = x.LIBIDO,
                            KOLIC = x.KOLIC,
                            GUST = x.GUST,
                            BR_U_ML = x.BR_U_ML,
                            VALO = x.VALO,
                            POKR = x.POKR,
                            P_MRT = x.P_MRT,
                            P_MORF = x.P_MORF,
                            UPOT = x.UPOT,
                            RAZR = x.RAZR,
                            PRIP = x.PRIP,
                            KOL_RS = x.KOL_RS,
                            DOZA = x.DOZA,
                            POK_RS = x.POK_RS,
                            DAT_PREG = x.DAT_PREG,
                            POK_PS = x.POK_PS,
                            NAPOMENA = x.NAPOMENA,
                            REFK = x.REFK
                        }).ToList();
            }
        }

        public List<EvidencijaOPrasenjuILaktacijiVM> EvidencijaOPrasenjuILaktacijiPregledPodataka(DateTime datumOd, DateTime datumDo)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => x.DAT_PRAS >= datumOd && x.DAT_PRAS <= datumDo)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join z in applicationDbContext.Zaluc on k.Id equals z.KrmacaId into zz
                        where p.PARIT == k.CIKLUS && !zz.Any(x => x.PARIT == k.CIKLUS)
                        select new EvidencijaOPrasenjuILaktacijiVM()
                        {
                            Box = p.BOX,
                            Ciklus = k.CIKLUS,
                            Dana = DbFunctions.DiffDays(p.DAT_PRAS, DateTime.Now),
                            Krmaca = k.T_KRM,
                            Obj = p.OBJ,
                            Oprasena = p.DAT_PRAS,
                            Rasa = k.RASA
                        }).ToList();
            }
        }

        public List<HormonVM> PregledPodatakaDavanjaHormona(DateTime datumOd, DateTime datumDo)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Hormon.Where(x => x.DatumDavanjaHormona >= datumOd && x.DatumDavanjaHormona <= datumDo)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new HormonVM()
                        {
                            DatumDavanjaHormona = p.DatumDavanjaHormona,
                            Krmaca = k.T_KRM,
                            TipHormona = p.TipHormona
                        }).ToList();
            }
        }


        public List<UginucaVM> PregledPodatakaUginuca(DateTime datumOd, DateTime datumDo)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Uginuca.Where(x => x.DatumUginuca >= datumOd && x.DatumUginuca <= datumDo)
                        join n in applicationDbContext.Nerast on p.NerastId equals n.Id
                        select new UginucaVM()
                        {
                            DatumUginuca = p.DatumUginuca,
                            Nerast = n.T_NER,
                            Kategorija = p.Kategorija,
                            Komada = p.Komada,
                            MestoRodjenja = p.MestoRodjenja,
                            Razlog = p.Razlog
                        }).ToList();
            }
        }

        public DataTable AnalizaUginucaPoNerastovima(DateTime datumOd, DateTime datumDo)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@danOd", datumOd);
                var danDoParametar = new SqlParameter("@danDo", datumDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.Uginuca_GetAllByDateOdDo", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable PregledPodatakaSuprasnosti(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@danOd", datumOd);
                var danDoParametar = new SqlParameter("@danDo", datumDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.Suprasnost_GetAllByDateOdDo", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable PregledPodatakaPrevodaKrmca(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@danOd", datumOd);
                var danDoParametar = new SqlParameter("@danDo", datumDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.PrevodKrmaca_GetAllByDateOdDo", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable SP_GetAllByDate(DateTime datumPripusta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumPripusta.Year, datumPripusta.Month, datumPripusta.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.Suprasnost_GetAllByDate", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable SP_GetAllByDate_Hormon(DateTime datumDavanja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumDavanja.Year, datumDavanja.Month, datumDavanja.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.SP_GetAllByDate_Hormon", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable SP_GetAllByDate_Uginuca(DateTime datumDavanja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumDavanja.Year, datumDavanja.Month, datumDavanja.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.SP_GetAllByDate_Uginuca", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable SP_GetAllByDate_PrevodKrmaca(DateTime datumPripusta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumPripusta.Year, datumPripusta.Month, datumPripusta.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.PrevodKrmaca_GetAllByDate", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public void SP_Save(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.Suprasnost_Save", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SP_Save_Hormon(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.SP_Save_Hormon", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SP_Save_Uginuce(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.SP_Save_Uginuce", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        public void SP_Save_PrevodKrmaca(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("OstaleFaze.PrevodKrmaca_Save", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        #endregion
        #region 06OstaleFaze
        public List<EvidencijaPobacajaKrmacaAzuriranjeVM> EvidencijaPobacajaKrmacaAzuriranje(DateTime datumPobacaja)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pobacaj.Where(x => DbFunctions.TruncateTime(datumPobacaja) == DbFunctions.TruncateTime(x.DAT_POBAC))
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new EvidencijaPobacajaKrmacaAzuriranjeVM()
                        {
                            Ciklus = k.CIKLUS,
                            Krmaca = k.T_KRM,
                            Id = p.Id,
                            MaticniBroj = k.MBR_KRM,
                            Rb = p.RB,
                            DatumPobacaja = p.DAT_POBAC
                        }).ToList();
            }
        }

        public ReturnObject DeletePobacaj(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var entity = applicationDbContext.Pobacaj.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return new ReturnObject() { Success = false, Message = "Pobacaj nije pronadjen" };
                }
                applicationDbContext.Pobacaj.Remove(entity);
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }

        public ReturnObject Save(EvidencijaPobacajaKrmacaAzuriranjeVM pobacaj)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                if (pobacaj.Id == 0)
                {
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == pobacaj.Krmaca);
                    if (krmaca == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
                    }
                    var entity = applicationDbContext.Pobacaj.Add(new Pobacaj()
                    {
                        DAT_POBAC = (DateTime)pobacaj.DatumPobacaja,
                        KrmacaId = krmaca.Id,
                        CIKLUS = krmaca.CIKLUS,
                        RB = pobacaj.Rb
                    });
                    applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = true, Id = entity.Id };
                }
                else
                {
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == pobacaj.Krmaca);
                    if (krmaca == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
                    }
                    var entity = applicationDbContext.Pobacaj.FirstOrDefault(x => x.Id == pobacaj.Id);
                    if (entity == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Pobacaj nije pronadjen" };
                    }
                    entity.DAT_POBAC = (DateTime)pobacaj.DatumPobacaja;
                    entity.RB = pobacaj.Rb;

                    applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = true, Id = entity.Id };
                }
            }
        }

        public List<PobacajiKrmacaINazimicaVM> PobacajiKrmacaINazimica(DateTime datumPobacajaOd, DateTime datumPobacajaDo)
        {

            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pobacaj.Where(x => DbFunctions.TruncateTime(datumPobacajaOd) <= DbFunctions.TruncateTime(x.DAT_POBAC) && DbFunctions.TruncateTime(datumPobacajaDo) >= DbFunctions.TruncateTime(x.DAT_POBAC))
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new PobacajiKrmacaINazimicaVM()
                        {
                            Ciklus = k.CIKLUS,
                            Krmaca = k.T_KRM,
                            Rasa = k.RASA,
                            Pobacaj = p.DAT_POBAC,
                            Sk = s.RAZL_SK
                        }).ToList();
            }
        }
        #endregion
    }
}
