﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase._06Selekcija;
using BusnissLayer.ViewModels.Prase._07Kartoteka;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class SelekcijaService
    {
        public List<EvidencijaPodatakaOOdabiruGrlaPregledVM> EvidencijaPodatakaOOdabiruGrlaPregled(DateTime datumOdabira)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Odabir.Where(x => DbFunctions.TruncateTime(x.D_ODABIRA) == datumOdabira.Date).Select(x => new EvidencijaPodatakaOOdabiruGrlaPregledVM()
                {
                    Id = x.Id,
                    Ekst = x.EKST,
                    IzLegla = x.IZ_LEGLA,
                    Markica = x.MARKICA,
                    MaticniBrojMajke = x.MBR_MAJKE,
                    MaticniBrojOca = x.MBR_OCA,
                    Pol = x.POL,
                    Prirast = x.PRIRAST,
                    Rasa = x.RASA,
                    Rodjeno = x.DAT_ROD,
                    Sisa = x.BR_SISA,
                    Tetovir = x.T_TEST,
                    Tezina = x.TEZINA
                }).ToList();
            }
        }

        public DataTable AnalizaPerformansTestaPoOcu(DateTime dateFrom, DateTime dateTo, bool kramca, string otac, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var dateTimeFrom = new SqlParameter("@dateTimeFrom", dateFrom);
                var dateTimeTo = new SqlParameter("@dateTimeTo", dateTo);
                var kramcaParam = new SqlParameter("@IsKrmaca", kramca);
                var otacParam = new SqlParameter("@Otac", otac);
                var rasaParam = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.AnalizaPerformansTestaPoOcu", con))
                    {
                        cmd.Parameters.Add(dateTimeFrom);
                        cmd.Parameters.Add(dateTimeTo);
                        cmd.Parameters.Add(kramcaParam);
                        cmd.Parameters.Add(otacParam);
                        cmd.Parameters.Add(rasaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable AnalizaPerformansTestaPoOcevima(DateTime dateFrom, DateTime dateTo, bool kramca)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var dateTimeFrom = new SqlParameter("@dateTimeFrom", dateFrom);
                var dateTimeTo = new SqlParameter("@dateTimeTo", dateTo);
                var kramcaParam = new SqlParameter("@IsKrmaca", kramca);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.AnalizaPerformansTestaPoOcevima", con))
                    {
                        cmd.Parameters.Add(dateTimeFrom);
                        cmd.Parameters.Add(dateTimeTo);
                        cmd.Parameters.Add(kramcaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable AnalizaPerformansTestaPoRasama(DateTime dateFrom, DateTime dateTo, bool kramca)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var dateTimeFrom = new SqlParameter("@dateTimeFrom", dateFrom);
                var dateTimeTo = new SqlParameter("@dateTimeTo", dateTo);
                var kramcaParam = new SqlParameter("@IsKrmaca", kramca);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.AnalizaPerformansTestaPoRasama", con))
                    {
                        cmd.Parameters.Add(dateTimeFrom);
                        cmd.Parameters.Add(dateTimeTo);
                        cmd.Parameters.Add(kramcaParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable AnalizaPripustaTestiranihGrla(DateTime dateFrom, DateTime dateTo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var dateTimeFrom = new SqlParameter("@dateTimeFrom", dateFrom);
                var dateTimeTo = new SqlParameter("@dateTimeTo", dateTo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.AnalizaPripustaTestiranihGrla", con))
                    {
                        cmd.Parameters.Add(dateTimeFrom);
                        cmd.Parameters.Add(dateTimeTo);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public object TestNerastovaNaPlodnost_SI2(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.TestNerastovaNaPlodnost_SI2", con))
                    {
                        cmd.Parameters.Add(rasaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable ProgeniTestKrmacaINerastovaSI3(string rasa, bool krmace)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                var krmaceParametar = new SqlParameter("@Krmace", krmace);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.ProgeniTestKrmacaINerastovaSI3", con))
                    {
                        cmd.Parameters.Add(rasaParametar);
                        cmd.Parameters.Add(krmaceParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable TestKrmacaNaPlodnost_SI2(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Selekcija.TestKrmacaNaPlodnost_SI2", con))
                    {
                        cmd.Parameters.Add(rasaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public List<PregledPodatakaTestiranjeNazimicaVM> PregledPodatakaTestiranjeNazimica(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                return (from x in applicationDbContext.Test.Where(x => DbFunctions.TruncateTime(x.D_TESTA) >= datumOd.Date && DbFunctions.TruncateTime(x.D_TESTA) <= datumDo.Date && x.POL == "Z")
                        join k in applicationDbContext.Krmaca on x.MBR_TEST equals k.MBR_KRM into ks
                        from k in ks.DefaultIfEmpty()
                        join p in applicationDbContext.Pripust on k.Id equals p.KrmacaId into ps
                        from p in ps.DefaultIfEmpty()
                        select new PregledPodatakaTestiranjeNazimicaVM()
                        {
                            Markica = x.MARKICA,
                            Mat_br_majke = x.MBR_MAJKE,
                            Mat_br_oca = x.MBR_OCA,
                            Rasa = x.RASA,
                            Rb = x.RB,
                            Starost = DbFunctions.DiffDays(x.DAT_ROD, DateTime.Now),
                            Tetovir = x.T_TEST,
                            Tezina = x.TEZINA,
                            Iz_legla = x.IZ_LEGLA,
                            Dat_rod = x.DAT_ROD,
                            Dat_testa = x.D_TESTA,
                            Prirast = x.PRIRAST,
                            U_priplod = p != null ? (DateTime?)p.DAT_PRIP : null
                        }).ToList();
            }
        }

        public void SP_Save_Test(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Selekcija.SP_Save_Test", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SP_Save_Odabir(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Selekcija.SP_Save_Odabir", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public List<PregledPodatakaTestiranjeMladihNerastovaVM> PregledPodatakaTestiranjeMladihNerastova(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                return (from x in applicationDbContext.Test.Where(x => DbFunctions.TruncateTime(x.D_TESTA) >= datumOd.Date && DbFunctions.TruncateTime(x.D_TESTA) <= datumDo.Date && x.POL == "Z")
                        join n in applicationDbContext.Nerast on x.MBR_TEST equals n.MBR_NER into ns
                        from n in ns.DefaultIfEmpty()
                        join p in applicationDbContext.Pripust on n.Id equals p.NerastId into ps
                        from p in ps.DefaultIfEmpty()
                        select new PregledPodatakaTestiranjeMladihNerastovaVM()
                        {
                            Markica = x.MARKICA,
                            Mat_br_majke = x.MBR_MAJKE,
                            Mat_br_oca = x.MBR_OCA,
                            Rasa = x.RASA,
                            Rb = x.RB,
                            Starost = DbFunctions.DiffDays(x.DAT_ROD, DateTime.Now),
                            Tetovir = x.T_TEST,
                            Tezina = x.TEZINA,
                            Iz_legla = x.IZ_LEGLA,
                            Dat_rod = x.DAT_ROD,
                            Dat_testa = x.D_TESTA,
                            Prirast = x.PRIRAST,
                            U_priplod = p != null ? (DateTime?)p.DAT_PRIP : null,
                            Ekst = x.EKST,
                            Sisa = x.BR_SISA
                        }).ToList();
            }
        }

        public List<PregledPodatakaOdabirGrlaVM> PregledPodatakaOdabirGrla(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from x in applicationDbContext.Odabir.Where(x => DbFunctions.TruncateTime(x.D_ODABIRA) >= datumOd.Date && DbFunctions.TruncateTime(x.D_ODABIRA) <= datumDo.Date)
                        join t in applicationDbContext.Test on x.MBR_TEST equals t.MBR_TEST into ts
                        from t in ts.DefaultIfEmpty()
                        select new PregledPodatakaOdabirGrlaVM()
                        {
                            Pol = x.POL,
                            Rasa = x.RASA,
                            Tezina = x.TEZINA,
                            Iz_legla = x.IZ_LEGLA,
                            Dat_rod = x.DAT_ROD,
                            Iskljucena = null,
                            Majka = x.MBR_MAJKE,
                            Otac = x.MBR_OCA,
                            Markica = x.MARKICA,
                            Odabrana = x.D_ODABIRA,
                            Prirast = x.PRIRAST,
                            Tetov = x.T_TEST,
                            U_test = t != null ? (DateTime?)t.D_TESTA : null
                        }).ToList();
            }
        }

        public List<EvidencijaPodatakaSaTestaGrlaZaPriplodVM> EvidencijaPodatakaSaTestaGrlaZaPriplod(DateTime datumTesta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                return applicationDbContext.Test.Include(x => x.Skartovi).Where(x => DbFunctions.TruncateTime(x.D_TESTA) == datumTesta.Date).Select(x => new EvidencijaPodatakaSaTestaGrlaZaPriplodVM()
                {
                    Id = x.Id,
                    Ekst = x.EKST,
                    Markica = x.MARKICA,
                    Mat_br_majke = x.MBR_MAJKE,
                    Mat_br_oca = x.MBR_OCA,
                    Pol = x.POL,
                    Rasa = x.RASA,
                    Rodjeno = x.DAT_ROD,
                    Sisa = x.BR_SISA,
                    Tetovir = x.T_TEST,
                    Tezina = x.TEZINA,
                    Iz_legla = x.IZ_LEGLA,
                    Poc_tez = x.POC_TEZ,
                    Ulaz_u_test = x.U_TEST,
                    Hrana = x.HRANA,
                    L_sl = x.L_SL,
                    B_sl = x.B_SL,
                    D_sl = x.DUB_S,
                    D_mis = x.DUB_M,
                    Procenat_Mesa = x.PROC_M,
                    U_priplodu_od = x.U_PRIPLOD,
                    Si1 = x.SI1,
                    Napomena = x.NAPOMENA,
                    Iskljuceno = x.Skartovi.FirstOrDefault() == null ? null : (DateTime?)x.Skartovi.FirstOrDefault().D_SKART,
                    Razl = x.Skartovi.FirstOrDefault() == null ? null : x.Skartovi.FirstOrDefault().RAZL_SK,
                }).ToList();
            }
        }

        public ReturnObject DeleteOdabira(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var entity = applicationDbContext.Odabir.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return new ReturnObject() { Success = false, Message = "Odabir nije pronadjen" };
                }
                applicationDbContext.Odabir.Remove(entity);
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }

        public bool EvidencijaPodatakaSaSmotreGrla(string tetovir, string markica,int? ekst, string brSisa, int? si, string napomena)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var test = applicationDbContext.Test.FirstOrDefault(x => x.T_TEST == tetovir);
                if (test != null)
                {
                    test.MARKICA = markica;
                    test.EKST = ekst;
                    test.BR_SISA = brSisa;
                    test.SI1 = si;
                    test.NAPOMENA = napomena;
                    applicationDbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<PrenosPodatakaIzRegistraPrasadiUOdabirVM> GetPrenosPodatakaIzRegistraPrasadiUOdabir(DateTime fromDate, DateTime toDate, DateTime datumOdabira)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                var datumOdabiraParametar = new SqlParameter("@datumOdabira", datumOdabira);

                return applicationDbContext.Database.SqlQuery<PrenosPodatakaIzRegistraPrasadiUOdabirVM>("PrenosPodatakaIzRegistraPrasadiUOdabir @dateTimeFrom, @dateTimeTo, @datumOdabira", fromParametar, toParametar, datumOdabiraParametar).ToList();
            }
        }

        public ReturnObject PrenesiPodatkeIzRegistraPrasadiUOdabir(List<PrenosPodatakaIzRegistraPrasadiUOdabirVM> prenosPodatakaIzRegistraPrasadiUOdabirVMs)
        {
            if (prenosPodatakaIzRegistraPrasadiUOdabirVMs != null)
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var datumOdabira = prenosPodatakaIzRegistraPrasadiUOdabirVMs.FirstOrDefault().D_odabira;
                    var idsForPrenos = prenosPodatakaIzRegistraPrasadiUOdabirVMs.Select(x => x.Id);
                    var prasadZaPrenos = applicationDbContext.Prase.Include(x => x.Krmaca).Include(x => x.Nerast).Where(x => idsForPrenos.Contains(x.Id)).ToList();
                    foreach (var item in prasadZaPrenos)
                    {

                        var majcinPedigre = applicationDbContext.Pedigre.FirstOrDefault(x => x.KrmacaId == item.Krmaca.Id || x.MBR_GRLA == item.MBR_MAJKE);
                        var ocevPedigre = applicationDbContext.Pedigre.FirstOrDefault(x => x.NerastId == item.Nerast.Id || x.MBR_GRLA == item.MBR_OCA);
                        var majcinoPoreklo = applicationDbContext.Poreklo.FirstOrDefault(x => x.KrmacaId == item.Krmaca.Id || x.MBR_GRLA == item.MBR_MAJKE);
                        var ocevoPoreklo = applicationDbContext.Poreklo.FirstOrDefault(x => x.NerastId == item.Nerast.Id || x.MBR_GRLA == item.MBR_MAJKE);
                        item.IsPrenesenaUOdabir = true;
                        applicationDbContext.Odabir.Add(new Odabir()
                        {
                            DAT_ROD = item.DAT_ROD,
                            BR_SISA = "",
                            D_ODABIRA = datumOdabira,
                            GN = item.GN,
                            IZ_LEGLA = item.IZ_LEGLA,
                            MBR_MAJKE = item.Krmaca != null ? item.Krmaca.MBR_KRM : item.MBR_MAJKE,
                            MBR_OCA = item.Nerast != null ? item.Nerast.MBR_NER : item.MBR_OCA,
                            MBR_TEST = item.MBR_PRAS,
                            VLA = item.MBR_PRAS.Substring(0, 3),
                            POL = item.POL,
                            ODG = item.MBR_PRAS.Substring(3, 3),
                            GNM = item.Krmaca != null ? item.Krmaca.GN : "",
                            GNO = item.Nerast != null ? item.Nerast.GN : "",
                            T_MAJKE10 = item.Krmaca != null ? item.Krmaca.TETOVIRK10 : "",
                            T_OCA10 = item.Nerast != null ? item.Nerast.TETOVIRN10 : "",
                            T_TEST = item.T_PRAS,
                            RASA = item.MBR_PRAS.Substring(6, 4),
                            Pedigre = new List<Pedigre>() { GeneratePedigre(applicationDbContext, majcinPedigre, ocevPedigre, item.T_PRAS, item.MBR_PRAS, item.POL, "") },
                            Poreklo = new List<Poreklo>() { GeneratePoreklo(applicationDbContext, majcinoPoreklo, ocevoPoreklo, item.T_PRAS, item.MBR_PRAS, item.POL, "", item.GN) }
                            //PRIRAST = item.pri
                            //TEZINA = item.tez
                        });
                    }
                    applicationDbContext.SaveChanges();
                    return new ReturnObject()
                    {
                        Success = true
                    };
                }
            }
            return new ReturnObject()
            {
                Success = false
            };
        }

        private Pedigre GeneratePedigre(ApplicationDbContext applicationDbContext, Pedigre majcinPedigre, Pedigre ocevPedigre, string t_GRLA, string mbr_GRLA, string pol, string hr_GRLA)
        {
            var pedigrePraseta = new Pedigre();
            pedigrePraseta.T_GRLA = t_GRLA;
            pedigrePraseta.MBR_GRLA = mbr_GRLA;
            pedigrePraseta.POL = pol;
            pedigrePraseta.HR_GRLA = hr_GRLA;
            if (majcinPedigre != null)
            {
                pedigrePraseta.MB_MAJKE = majcinPedigre.MBR_GRLA;
                pedigrePraseta.MBM_DEDE = majcinPedigre.MB_OCA;
                pedigrePraseta.MBM_BABE = majcinPedigre.MB_MAJKE;
                pedigrePraseta.MBMD_PRAD = majcinPedigre.MBO_DEDE;
                pedigrePraseta.MBMD_PRAB = majcinPedigre.MBO_BABE;
                pedigrePraseta.MBMB_PRAD = majcinPedigre.MBM_DEDE;
                pedigrePraseta.MBMB_PRAB = majcinPedigre.MBM_BABE;
                pedigrePraseta.HR_MAJKE = majcinPedigre.HR_GRLA;
                pedigrePraseta.HRM_DEDE = majcinPedigre.HR_OCA;
                pedigrePraseta.HRM_BABE = majcinPedigre.HR_MAJKE;
                pedigrePraseta.HRMD_PRAD = majcinPedigre.HRO_DEDE;
                pedigrePraseta.HRMD_PRAB = majcinPedigre.HRO_BABE;
                pedigrePraseta.HRMB_PRAD = majcinPedigre.HRM_DEDE;
                pedigrePraseta.HRMB_PRAB = majcinPedigre.HRM_BABE;
            }
            if (ocevPedigre != null)
            {
                pedigrePraseta.MB_OCA = ocevPedigre.MBR_GRLA;
                pedigrePraseta.MBO_DEDE = ocevPedigre.MB_OCA;
                pedigrePraseta.MBO_BABE = ocevPedigre.MB_MAJKE;
                pedigrePraseta.MBOD_PRAD = ocevPedigre.MBO_DEDE;
                pedigrePraseta.MBOD_PRAB = ocevPedigre.MBO_BABE;
                pedigrePraseta.MBOB_PRAD = ocevPedigre.MBM_DEDE;
                pedigrePraseta.MBOB_PRAB = ocevPedigre.MBM_BABE;
                pedigrePraseta.HR_OCA = ocevPedigre.HR_GRLA;
                pedigrePraseta.HRO_DEDE = ocevPedigre.HR_OCA;
                pedigrePraseta.HRO_BABE = ocevPedigre.HR_MAJKE;
                pedigrePraseta.HROD_PRAD = ocevPedigre.HRO_DEDE;
                pedigrePraseta.HROD_PRAB = ocevPedigre.HRO_BABE;
                pedigrePraseta.HROB_PRAD = ocevPedigre.HRM_DEDE;
                pedigrePraseta.HROB_PRAB = ocevPedigre.HRM_BABE;
            }
            return pedigrePraseta;
        }


        private Poreklo GeneratePoreklo(ApplicationDbContext applicationDbContext, Poreklo majcinoPoreklo, Poreklo ocevoPoreklo, string t_GRLA, string mbr_GRLA, string pol, string hr_GRLA, string gn)
        {
            var porekloPraseta = new Poreklo();
            porekloPraseta.T_GRLA = t_GRLA;
            porekloPraseta.T_GRLA10 = t_GRLA;
            porekloPraseta.MBR_GRLA = mbr_GRLA;
            porekloPraseta.MB_GRLA = "092-092-" + gn + "-" + t_GRLA;
            porekloPraseta.POL = pol;
            porekloPraseta.HR_GRLA = hr_GRLA;
            porekloPraseta.GN = gn;
            if (majcinoPoreklo != null)
            {
                porekloPraseta.MB_MAJKE = majcinoPoreklo.MB_GRLA;
                porekloPraseta.MBM_DEDE = majcinoPoreklo.MB_OCA;
                porekloPraseta.MBM_BABE = majcinoPoreklo.MB_MAJKE;
                porekloPraseta.MBMD_PRAD = majcinoPoreklo.MBO_DEDE;
                porekloPraseta.MBMD_PRAB = majcinoPoreklo.MBO_BABE;
                porekloPraseta.MBMB_PRAD = majcinoPoreklo.MBM_DEDE;
                porekloPraseta.MBMB_PRAB = majcinoPoreklo.MBM_BABE;
                porekloPraseta.HR_MAJKE = majcinoPoreklo.HR_GRLA;
                porekloPraseta.HRM_DEDE = majcinoPoreklo.HR_OCA;
                porekloPraseta.HRM_BABE = majcinoPoreklo.HR_MAJKE;
                porekloPraseta.HRMD_PRAD = majcinoPoreklo.HRO_DEDE;
                porekloPraseta.HRMD_PRAB = majcinoPoreklo.HRO_BABE;
                porekloPraseta.HRMB_PRAD = majcinoPoreklo.HRM_DEDE;
                porekloPraseta.HRMB_PRAB = majcinoPoreklo.HRM_BABE;
            }
            if (ocevoPoreklo != null)
            {
                porekloPraseta.MB_OCA = ocevoPoreklo.MB_GRLA;
                porekloPraseta.MBO_DEDE = ocevoPoreklo.MB_OCA;
                porekloPraseta.MBO_BABE = ocevoPoreklo.MB_MAJKE;
                porekloPraseta.MBOD_PRAD = ocevoPoreklo.MBO_DEDE;
                porekloPraseta.MBOD_PRAB = ocevoPoreklo.MBO_BABE;
                porekloPraseta.MBOB_PRAD = ocevoPoreklo.MBM_DEDE;
                porekloPraseta.MBOB_PRAB = ocevoPoreklo.MBM_BABE;
                porekloPraseta.HR_OCA = ocevoPoreklo.HR_GRLA;
                porekloPraseta.HRO_DEDE = ocevoPoreklo.HR_OCA;
                porekloPraseta.HRO_BABE = ocevoPoreklo.HR_MAJKE;
                porekloPraseta.HROD_PRAD = ocevoPoreklo.HRO_DEDE;
                porekloPraseta.HROD_PRAB = ocevoPoreklo.HRO_BABE;
                porekloPraseta.HROB_PRAD = ocevoPoreklo.HRM_DEDE;
                porekloPraseta.HROB_PRAB = ocevoPoreklo.HRM_BABE;
            }
            return porekloPraseta;
        }

        public List<PrenosPodatakaIzOdabiraUtestVM> GetPrenosPodatakaIzOdabiraUtest(DateTime fromDate, DateTime toDate, DateTime datumTesta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                var datumTestaParametar = new SqlParameter("@datumTesta", datumTesta);

                return applicationDbContext.Database.SqlQuery<PrenosPodatakaIzOdabiraUtestVM>("PrenosPodatakaIzOdabiraUtest @dateTimeFrom, @dateTimeTo, @datumTesta", fromParametar, toParametar, datumTestaParametar).ToList();
            }
        }

        public KartotekaNerastovaPregledVM GetTestByT_Kod(string t_Kod)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from n in applicationDbContext.Test.Where(x => x.T_TEST == t_Kod)
                        join s in applicationDbContext.Skart on n.Id equals s.NerastId into ss
                        from s in ss.DefaultIfEmpty()
                        select new KartotekaNerastovaPregledVM()
                        {
                            Nerast = n.T_TEST,
                            Id = n.Id,
                            Ekst = n.EKST,
                            GnM = n.GNM,
                            GnO = n.GNO,
                            Iskljucen = s.D_SKART,
                            Iz_legla = n.IZ_LEGLA,
                            Markica = n.MARKICA,
                            Mat_broj_nerasta = n.MBR_TEST,
                            Mat_broj_majke = n.MBR_MAJKE,
                            Mat_broj_oca = n.MBR_OCA,
                            Napomena = n.NAPOMENA,
                            Odg = n.ODG,
                            Rasa = n.RASA,
                            Razl = s.RAZL_SK,
                            Rodjen = n.DAT_ROD,
                            Si1 = n.SI1,
                            Sisa = n.BR_SISA,
                            Tetovir = n.T_TEST,
                            Tet_majke = n.T_MAJKE10,
                            Tet_oca = n.T_OCA10,
                            Vla = n.VLA
                        }).FirstOrDefault();
            }
        }

        public ReturnObject PrenosPodatakaIzOdabiraUtest(List<PrenosPodatakaIzOdabiraUtestVM> prenosPodatakaIzOdabiraUtestVMs)
        {
            if (prenosPodatakaIzOdabiraUtestVMs != null)
            {
                using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
                {
                    var datumTesta = prenosPodatakaIzOdabiraUtestVMs.FirstOrDefault().D_testa;
                    var idsForPrenos = prenosPodatakaIzOdabiraUtestVMs.Select(x => x.Id);
                    var prasadZaPrenos = applicationDbContext.Odabir.Include(x => x.Pedigre).Include(x => x.Poreklo).Where(x => idsForPrenos.Contains(x.Id)).ToList();
                    foreach (var item in prasadZaPrenos)
                    {
                        var poreklo = item.Poreklo ?? applicationDbContext.Poreklo.Where(x => x.MBR_GRLA == item.MBR_TEST).ToList();
                        var pedigre = item.Pedigre ?? applicationDbContext.Pedigre.Where(x => x.MBR_GRLA == item.MBR_TEST).ToList();
                        item.IsPrenesenUTest = true;
                        applicationDbContext.Test.Add(new Test()
                        {
                            DAT_ROD = item.DAT_ROD.Value,
                            BR_SISA = item.BR_SISA,
                            D_TESTA = datumTesta,
                            GN = item.GN,
                            IZ_LEGLA = item.IZ_LEGLA,
                            MBR_MAJKE = item.MBR_MAJKE,
                            MBR_OCA = item.MBR_OCA,
                            MBR_TEST = item.MBR_TEST,
                            VLA = item.VLA,
                            POL = item.POL,
                            ODG = item.ODG,
                            GNM = item.GNM,
                            GNO = item.GNO,
                            T_MAJKE10 = item.T_MAJKE10,
                            T_OCA10 = item.T_OCA10,
                            T_TEST = item.T_TEST,
                            TETOVIRT10 = item.T_TEST,
                            RASA = item.RASA,
                            PRIRAST = item.PRIRAST,
                            TEZINA = item.TEZINA,
                            EKST = item.EKST,
                            U_PRIPLOD = item.U_PRIPLOD,
                            POC_TEZ = item.TEZINA,
                            Poreklo = poreklo,
                            Pedigre = pedigre
                        });
                    }
                    applicationDbContext.SaveChanges();
                    return new ReturnObject()
                    {
                        Success = true
                    };
                }
            }
            return new ReturnObject()
            {
                Success = false
            };
        }
    }
}
