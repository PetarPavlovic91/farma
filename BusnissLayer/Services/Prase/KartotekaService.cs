﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase._07Kartoteka;
using BusnissLayer.ViewModels.Prase.Krmaca;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class KartotekaService
    {
        #region 03KartotekaKrmaca
        public List<KartotekaKrmacaPregledVM> KartotekaKrmacaPregled(bool iskljucena)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (iskljucena)
                {
                    return (from k in applicationDbContext.Krmaca
                            join s in applicationDbContext.Skart on k.Id equals s.KrmacaId
                            select new KartotekaKrmacaPregledVM()
                            {
                                Krmaca = k.TETOVIRK10,
                                Id = k.Id,
                                Cliklus = k.CIKLUS,
                                Ekst = k.EKST,
                                Faza = k.FAZA,
                                Na_farmi_od = k.NA_FARM_OD,
                                Gn = k.GN,
                                GnM = k.GNM,
                                GnO = k.GNO,
                                Grupa = k.GRUPA,
                                HB_Broj = k.HBBROJ,
                                Iskljucena = s.D_SKART,
                                Iz_legla = k.IZ_LEGLA,
                                Klasa = k.KLASA,
                                Lakt = k.UK_LAKT,
                                Markica = k.MARKICA,
                                Mat_broj_majke = k.MBR_MAJKE,
                                Mat_broj_oca = k.MBR_OCA,
                                Mrtvo = k.UK_MRTVO,
                                Napomena = k.NAPOMENA,
                                Odg = k.ODG,
                                Opis = k.OPIS,
                                Paritet = k.PARIT,
                                Prazni_dani = k.UK_PRAZNI,
                                Proizv_d = k.UK_PROIZ_D,
                                Rasa = k.RASA,
                                Razl = s.RAZL_SK,
                                RB_Broj = k.RBBROJ,
                                Rodjena = k.DAT_ROD,
                                Si1 = k.SI1,
                                Si2 = k.SI2,
                                Si3 = k.SI3,
                                Sisa = k.BR_SISA,
                                Tetovir = k.TETOVIRK10,
                                Tet_majke = k.T_MAJKE10,
                                Tet_oca = k.T_OCA10,
                                Vla = k.VLA,
                                Zaluc = k.UK_ZAL,
                                Zenski = k.UK_ZIVO_Z,
                                Zivo = k.UK_ZIVO
                            }).ToList();
                }
                else
                {
                    return (from k in applicationDbContext.Krmaca
                            join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                            where ss.Count() == 0
                            select new KartotekaKrmacaPregledVM()
                            {
                                Krmaca = k.TETOVIRK10,
                                Id = k.Id,
                                Cliklus = k.CIKLUS,
                                Ekst = k.EKST,
                                Faza = k.FAZA,
                                Na_farmi_od = k.NA_FARM_OD,
                                Gn = k.GN,
                                GnM = k.GNM,
                                GnO = k.GNO,
                                Grupa = k.GRUPA,
                                HB_Broj = k.HBBROJ,
                                Iskljucena = null,
                                Iz_legla = k.IZ_LEGLA,
                                Klasa = k.KLASA,
                                Lakt = k.UK_LAKT,
                                Markica = k.MARKICA,
                                Mat_broj_majke = k.MBR_MAJKE,
                                Mat_broj_oca = k.MBR_OCA,
                                Mrtvo = k.UK_MRTVO,
                                Napomena = k.NAPOMENA,
                                Odg = k.ODG,
                                Opis = k.OPIS,
                                Paritet = k.PARIT,
                                Prazni_dani = k.UK_PRAZNI,
                                Proizv_d = k.UK_PROIZ_D,
                                Rasa = k.RASA,
                                Razl = "",
                                RB_Broj = k.RBBROJ,
                                Rodjena = k.DAT_ROD,
                                Si1 = k.SI1,
                                Si2 = k.SI2,
                                Si3 = k.SI3,
                                Sisa = k.BR_SISA,
                                Tetovir = k.TETOVIRK10,
                                Tet_majke = k.T_MAJKE10,
                                Tet_oca = k.T_OCA10,
                                Vla = k.VLA,
                                Zaluc = k.UK_ZAL,
                                Zenski = k.UK_ZIVO_Z,
                                Zivo = k.UK_ZIVO
                            }).ToList();
                }
            }
        }

        public DataTable Sifrarnik(string grupa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var idParametar = new SqlParameter("@Grupa", grupa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("proc_Sifrarnik", con))
                    {
                        cmd.Parameters.Add(idParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable GetAllForKrmacaReport(int krmacaId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var idParametar = new SqlParameter("@Id", krmacaId);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetAllForKrmacaReport", con))
                    {
                        cmd.Parameters.Add(idParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public List<AktivneKrmaceNaDanVM> AktivneKrmaceNaDan(DateTime dan, string rasa, bool isNazimica = false)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", dan);
                var rasaParametar = new SqlParameter("@rasa", rasa);
                var isNazimicaParametar = new SqlParameter("@IsNazimica", isNazimica);

                return applicationDbContext.Database.SqlQuery<AktivneKrmaceNaDanVM>("AktivneKrmaceNaDan @dan, @rasa, @IsNazimica", danParametar, rasaParametar, isNazimicaParametar).ToList();
            }
        }

        public List<AktivneKrmaceNaDanParitetnaStrukturaVM> AktivneKrmaceNaDanParitetnaStruktura(DateTime dan, string rasa, bool isNazimica = false)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", dan);
                var rasaParametar = new SqlParameter("@rasa", rasa);
                var isNazimicaParametar = new SqlParameter("@IsNazimica", isNazimica);

                return applicationDbContext.Database.SqlQuery<AktivneKrmaceNaDanParitetnaStrukturaVM>("AktivneKrmaceNaDanParitetnaStruktura @dan, @rasa, @IsNazimica", danParametar, rasaParametar, isNazimicaParametar).ToList();
            }
        }

        public List<PregledAktivnihNerastovaNaDanRasnaStrukturaVM> AktivneKrmaceNaDanRasnaStruktura(DateTime dan, string rasa, bool isNazimica = false)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", dan);
                var rasaParametar = new SqlParameter("@rasa", rasa);
                var isNazimicaParametar = new SqlParameter("@IsNazimica", isNazimica);

                return applicationDbContext.Database.SqlQuery<PregledAktivnihNerastovaNaDanRasnaStrukturaVM>("AktivneKrmaceNaDanRasnaStruktura @dan, @rasa, @IsNazimica", danParametar, rasaParametar, isNazimicaParametar).ToList();
            }
        }

        public List<PregledAktivnihNerastovaNaDanKlasnaStrukturaVM> AktivneKrmaceNaDanKlasnaStruktura(DateTime dan, string rasa, bool isNazimica = false)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", dan);
                var rasaParametar = new SqlParameter("@rasa", rasa);
                var isNazimicaParametar = new SqlParameter("@IsNazimica", isNazimica);

                return applicationDbContext.Database.SqlQuery<PregledAktivnihNerastovaNaDanKlasnaStrukturaVM>("AktivneKrmaceNaDanKlasnaStruktura @dan, @rasa, @IsNazimica", danParametar, rasaParametar, isNazimicaParametar).ToList();
            }
        }

        public bool EvidencijaPodatakaSaSmotreGrla(string tetovir, string klasa, string klP, string markica, DateTime naFarmiOd, int? ekst, string brSisa, int? si, string napomena, string hbBroj, string rbBroj)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == tetovir);
                if (nerast != null)
                {
                    nerast.KLASA = klasa;
                    nerast.MARKICA = markica;
                    nerast.NA_FARM_OD = naFarmiOd;
                    nerast.EKST = ekst;
                    nerast.BR_SISA = brSisa;
                    nerast.SI1 = si;
                    nerast.NAPOMENA = napomena;
                    nerast.HBBROJ = hbBroj;
                    nerast.RBBROJ = rbBroj;

                    var existingKlasa = applicationDbContext.Klasa.FirstOrDefault(k => k.DAT_SMOTRE.HasValue && DbFunctions.TruncateTime((DateTime)k.DAT_SMOTRE) == DbFunctions.TruncateTime(DateTime.Now));
                    if (existingKlasa != null)
                    {
                        existingKlasa.KLASA = klasa;
                        existingKlasa.DAT_SMOTRE = DateTime.Now;
                        existingKlasa.MBR_GRLA = nerast.MBR_NER;
                        existingKlasa.T_GRLA = nerast.T_NER;
                        existingKlasa.POL = "M";
                        existingKlasa.KL_P = null;
                        existingKlasa.GODINA = DateTime.Now.Year;
                        existingKlasa.EKST = ekst;
                    }
                    else
                    {
                        applicationDbContext.Klasa.Add(new Klasa()
                        {
                            GODINA = DateTime.Now.Year,
                            KL_P = null,
                            DAT_SMOTRE = DateTime.Now,
                            KLASA = klasa,
                            MBR_GRLA = nerast.MBR_NER,
                            T_GRLA = nerast.T_NER,
                            POL = "M",
                            EKST = ekst
                    });
                    }
                    applicationDbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public List<AktivneKrmaceNaDanHBRBNStrukturaVM> AktivneKrmaceNaDanHBRBNStruktura(DateTime dan, string rasa, bool isNazimica = false)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", dan);
                var rasaParametar = new SqlParameter("@rasa", rasa);
                var isNazimicaParametar = new SqlParameter("@IsNazimica", isNazimica);

                return applicationDbContext.Database.SqlQuery<AktivneKrmaceNaDanHBRBNStrukturaVM>("AktivneKrmaceNaDanHBRBNStruktura @dan, @rasa, @IsNazimica", danParametar, rasaParametar, isNazimicaParametar).ToList();
            }
        }

        public DataTable GrlaBezOtvorenogPorekla(bool isPoreklo, string vrstaGrla)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var isPorekloParametar = new SqlParameter("@isPoreklo", isPoreklo);
                var vrstaGrlaParametar = new SqlParameter("@vrstaGrla", vrstaGrla);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GrlaBezOtvorenogPorekla", con))
                    {
                        cmd.Parameters.Add(isPorekloParametar);
                        cmd.Parameters.Add(vrstaGrlaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable GrlaBezPotpunogPorekla(bool isPoreklo, string vrstaGrla, bool aktivan)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var isPorekloParametar = new SqlParameter("@isPoreklo", isPoreklo);
                var vrstaGrlaParametar = new SqlParameter("@vrstaGrla", vrstaGrla);
                var aktivanParametar = new SqlParameter("@aktivan", aktivan);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GrlaBezPotpunogPorekla", con))
                    {
                        cmd.Parameters.Add(isPorekloParametar);
                        cmd.Parameters.Add(vrstaGrlaParametar);
                        cmd.Parameters.Add(aktivanParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public List<PregledHBBrojevaKrmacaVM> PregledHBBrojevaKrmaca()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => !string.IsNullOrEmpty(x.HBBROJ))
                        select new PregledHBBrojevaKrmacaVM()
                        {
                            Krmaca = k.TETOVIRK10,
                            Ekst = k.EKST,
                            Faza = k.FAZA,
                            Klasa = k.KLASA,
                            Markica = k.MARKICA,
                            Napomena = k.NAPOMENA,
                            Rasa = k.RASA,
                            Dat_rod = k.DAT_ROD,
                            Sisa = k.BR_SISA,
                            Uk_zivo = k.UK_ZIVO,
                            Box = "",
                            Ciklus = k.CIKLUS,
                            HBBroj = k.HBBROJ,
                            Mbr_krm = k.MBR_KRM,
                            Obj = "",
                            Parit = k.PARIT,
                            Uk_mrtvo = k.UK_MRTVO
                        }).ToList();
            }
        }

        public List<PregledRBBrojevaKrmacaVM> PregledRBBrojevaKrmaca()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => !string.IsNullOrEmpty(x.RBBROJ))
                        select new PregledRBBrojevaKrmacaVM()
                        {
                            Krmaca = k.TETOVIRK10,
                            Ekst = k.EKST,
                            Faza = k.FAZA,
                            Klasa = k.KLASA,
                            Markica = k.MARKICA,
                            Napomena = k.NAPOMENA,
                            Rasa = k.RASA,
                            Dat_rod = k.DAT_ROD,
                            Sisa = k.BR_SISA,
                            Uk_zivo = k.UK_ZIVO,
                            Box = "",
                            Ciklus = k.CIKLUS,
                            RBBroj = k.RBBROJ,
                            Mbr_krm = k.MBR_KRM,
                            Obj = "",
                            Parit = k.PARIT,
                            Uk_mrtvo = k.UK_MRTVO
                        }).ToList();
            }
        }

        public MaticniBrojIFazaVM PregledRBBrojevaKrmaca(string tetovir, string markica)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var result = new MaticniBrojIFazaVM();
                if (!string.IsNullOrEmpty(markica))
                {
                    var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.MARKICA == markica);
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.MARKICA == markica);
                    var odabir = applicationDbContext.Odabir.FirstOrDefault(x => x.MARKICA == markica);
                    var test = applicationDbContext.Test.FirstOrDefault(x => x.MARKICA == markica);
                    if (nerast != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: M";
                        result.GrloJe = "Nerast";
                        result.Mat_br = nerast.MBR_NER;
                        result.Mbr_majke = nerast.MBR_MAJKE;
                        result.Mbr_oca = nerast.MBR_OCA;
                        result.DatumRodjenja = nerast.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (krmaca != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: Z";
                        result.GrloJe = "Krmaca";
                        result.Mat_br = krmaca.MBR_KRM;
                        result.Mbr_majke = krmaca.MBR_MAJKE;
                        result.Mbr_oca = krmaca.MBR_OCA;
                        result.DatumRodjenja = krmaca.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (test != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: " + test.POL;
                        result.GrloJe = "Test";
                        result.Mat_br = test.MBR_TEST;
                        result.Mbr_majke = test.MBR_MAJKE;
                        result.Mbr_oca = test.MBR_OCA;
                        result.DatumRodjenja = test.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (odabir != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: " + odabir.POL;
                        result.GrloJe = "Odabir";
                        result.Mat_br = odabir.MBR_TEST;
                        result.Mbr_majke = odabir.MBR_MAJKE;
                        result.Mbr_oca = odabir.MBR_OCA;
                        result.DatumRodjenja = odabir.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                }
                else if (!string.IsNullOrEmpty(tetovir))
                {
                    var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == tetovir);
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == tetovir);
                    var odabir = applicationDbContext.Odabir.FirstOrDefault(x => x.T_TEST == tetovir);
                    var test = applicationDbContext.Test.FirstOrDefault(x => x.T_TEST == tetovir);
                    var prase = applicationDbContext.Prase.FirstOrDefault(x => x.T_PRAS == tetovir);
                    if (nerast != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: M";
                        result.GrloJe = "Nerast";
                        result.Mat_br = nerast.MBR_NER;
                        result.Mbr_majke = nerast.MBR_MAJKE;
                        result.Mbr_oca = nerast.MBR_OCA;
                        result.DatumRodjenja = nerast.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (krmaca != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: Z";
                        result.GrloJe = "Krmaca";
                        result.Mat_br = krmaca.MBR_KRM;
                        result.Mbr_majke = krmaca.MBR_MAJKE;
                        result.Mbr_oca = krmaca.MBR_OCA;
                        result.DatumRodjenja = krmaca.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (test != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: " + test.POL;
                        result.GrloJe = "Test";
                        result.Mat_br = test.MBR_TEST;
                        result.Mbr_majke = test.MBR_MAJKE;
                        result.Mbr_oca = test.MBR_OCA;
                        result.DatumRodjenja = test.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (odabir != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: " + odabir.POL;
                        result.GrloJe = "Odabir";
                        result.Mat_br = odabir.MBR_TEST;
                        result.Mbr_majke = odabir.MBR_MAJKE;
                        result.Mbr_oca = odabir.MBR_OCA;
                        result.DatumRodjenja = odabir.DAT_ROD?.ToString("yyyy/MM/dd");
                    }
                    else if (prase != null)
                    {
                        result.Ciklus_Parit_Faza = "Pol: " + prase.POL;
                        result.GrloJe = "Prase";
                        result.Mat_br = prase.MBR_PRAS;
                        result.Mbr_majke = prase.MBR_MAJKE;
                        result.Mbr_oca = prase.MBR_OCA;
                        result.DatumRodjenja = prase.DAT_ROD.ToString("yyyy/MM/dd");
                    }
                }
                return result;
            }
        }


        public List<PregledNBBrojevaKrmacaVM> PregledNBBrojevaKrmaca()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => !string.IsNullOrEmpty(x.NBBROJ))
                        select new PregledNBBrojevaKrmacaVM()
                        {
                            Krmaca = k.TETOVIRK10,
                            Ekst = k.EKST,
                            Faza = k.FAZA,
                            Klasa = k.KLASA,
                            Markica = k.MARKICA,
                            Napomena = k.NAPOMENA,
                            Rasa = k.RASA,
                            Dat_rod = k.DAT_ROD,
                            Sisa = k.BR_SISA,
                            Uk_zivo = k.UK_ZIVO,
                            Box = "",
                            Ciklus = k.CIKLUS,
                            NBBroj = k.NBBROJ,
                            Mbr_krm = k.MBR_KRM,
                            Obj = "",
                            Parit = k.PARIT,
                            Uk_mrtvo = k.UK_MRTVO
                        }).ToList();
            }
        }

        public List<PregledMarkicaKrmacaVM> PregledMarkicaKrmaca()
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => !string.IsNullOrEmpty(x.MARKICA))
                        select new PregledMarkicaKrmacaVM()
                        {
                            Krmaca = k.TETOVIRK10,
                            Ekst = k.EKST,
                            Faza = k.FAZA,
                            Klasa = k.KLASA,
                            Markica = k.MARKICA,
                            Napomena = k.NAPOMENA,
                            Rasa = k.RASA,
                            Dat_rod = k.DAT_ROD,
                            Sisa = k.BR_SISA,
                            Uk_zivo = k.UK_ZIVO,
                            Box = "",
                            Ciklus = k.CIKLUS,
                            HBBroj = k.HBBROJ,
                            Mbr_krm = k.MBR_KRM,
                            Obj = "",
                            Parit = k.PARIT,
                            Uk_mrtvo = k.UK_MRTVO,
                            NBBroj = k.NBBROJ,
                            RBBroj = k.RBBROJ
                        }).ToList();
            }
        }

        public List<AnalizaPotomstvaPoOcuVM> AnalizaPotomstvaPoMajci(string mbrKrmace)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var mbrKrmaceParametar = new SqlParameter("@mbr_majke", mbrKrmace);

                return applicationDbContext.Database.SqlQuery<AnalizaPotomstvaPoOcuVM>("AnalizaPotomstvaPoMajci @mbr_majke", mbrKrmaceParametar).ToList();
            }
        }

        #endregion
        #region 04KartotekaNerastova
        public ReturnObject PrenesiPodatkeZaNovogNerastaIzTesta(string tetovir)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var test = applicationDbContext.Test.Include(x => x.Pedigre).Include(x => x.Poreklo).FirstOrDefault(x => x.T_TEST == tetovir);
                if (test == null)
                {
                    return new ReturnObject() { Success = false, Message = "Test nije pronadjen." };
                }
                if (applicationDbContext.Nerast.Any(x => x.T_NER == tetovir))
                {
                    return new ReturnObject() { Success = false, Message = "Nerast sa ovim tetovir brojem vec postoji." };
                }
                applicationDbContext.Nerast.Add(new Nerast()
                {
                    BR_SISA = test.BR_SISA,
                    DAT_ROD = test.DAT_ROD,
                    EKST = test.EKST,
                    GEN_MAR = test.GEN_MAR,
                    GN = test.GN,
                    GNM = test.GNM,
                    GNO = test.GNO,
                    HALOTAN = test.HALOTAN,
                    IZ_LEGLA = test.IZ_LEGLA,
                    MARKICA = test.MARKICA,
                    MBR_NER = test.MBR_TEST,
                    MBR_MAJKE = test.MBR_MAJKE,
                    MBR_OCA = test.MBR_OCA,
                    NAPOMENA = test.NAPOMENA,
                    ODG = test.ODG,
                    RASA = test.RASA,
                    SI1 = test.SI1,
                    SI1F = test.SI1F,
                    TETOVIRN10 = test.TETOVIRT10,
                    T_NER = test.T_TEST,
                    T_MAJKE10 = test.T_MAJKE10,
                    T_OCA10 = test.T_OCA10,
                    VLA = test.VLA,
                    Poreklo = test.Poreklo,
                    Pedigre = test.Pedigre
                });
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }
        public List<KartotekaNerastovaPregledVM> KatalogNerastovaPregled(bool iskljucena)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (iskljucena)
                {
                    return (from n in applicationDbContext.Nerast
                            join s in applicationDbContext.Skart on n.Id equals s.NerastId
                            select new KartotekaNerastovaPregledVM()
                            {
                                Nerast = n.TETOVIRN10,
                                Id = n.Id,
                                Ekst = n.EKST,
                                Na_farmi_od = n.NA_FARM_OD,
                                GnM = n.GNM,
                                GnO = n.GNO,
                                HB_Broj = n.HBBROJ,
                                Iskljucen = s.D_SKART,
                                Iz_legla = n.IZ_LEGLA,
                                Klasa = n.KLASA,
                                Markica = n.MARKICA,
                                Mat_broj_majke = n.MBR_MAJKE,
                                Mat_broj_oca = n.MBR_OCA,
                                Mrtvo = n.UK_MRTVO,
                                Napomena = n.NAPOMENA,
                                Odg = n.ODG,
                                Opis = n.OPIS,
                                Rasa = n.RASA,
                                Razl = s.RAZL_SK,
                                RB_Broj = n.RBBROJ,
                                Rodjen = n.DAT_ROD,
                                Si1 = n.SI1,
                                Si2 = n.SI2,
                                Si3 = n.SI3,
                                Sisa = n.BR_SISA,
                                Tetovir = n.TETOVIRN10,
                                Tet_majke = n.T_MAJKE10,
                                Tet_oca = n.T_OCA10,
                                Vla = n.VLA,
                                Zivo = n.UK_ZIVO,
                                Broj_dozvole = n.BR_DOZVOLE,
                                Br_op = n.BR_OP,
                                Br_os = n.BR_OS,
                                Prvi_skok = n.D_P_SKOKA,
                                Pv = n.PV
                            }).ToList();
                }
                else
                {
                    return (from n in applicationDbContext.Nerast
                            join s in applicationDbContext.Skart on n.Id equals s.NerastId into ss
                            where ss.Count() == 0
                            select new KartotekaNerastovaPregledVM()
                            {
                                Nerast = n.TETOVIRN10,
                                Id = n.Id,
                                Ekst = n.EKST,
                                Na_farmi_od = n.NA_FARM_OD,
                                GnM = n.GNM,
                                GnO = n.GNO,
                                HB_Broj = n.HBBROJ,
                                Iskljucen = null,
                                Iz_legla = n.IZ_LEGLA,
                                Klasa = n.KLASA,
                                Markica = n.MARKICA,
                                Mat_broj_majke = n.MBR_MAJKE,
                                Mat_broj_oca = n.MBR_OCA,
                                Mrtvo = n.UK_MRTVO,
                                Napomena = n.NAPOMENA,
                                Odg = n.ODG,
                                Opis = n.OPIS,
                                Rasa = n.RASA,
                                Razl = "",
                                RB_Broj = n.RBBROJ,
                                Rodjen = n.DAT_ROD,
                                Si1 = n.SI1,
                                Si2 = n.SI2,
                                Si3 = n.SI3,
                                Sisa = n.BR_SISA,
                                Tetovir = n.TETOVIRN10,
                                Tet_majke = n.T_MAJKE10,
                                Tet_oca = n.T_OCA10,
                                Vla = n.VLA,
                                Zivo = n.UK_ZIVO,
                                Broj_dozvole = n.BR_DOZVOLE,
                                Br_op = n.BR_OP,
                                Br_os = n.BR_OS,
                                Prvi_skok = n.D_P_SKOKA,
                                Pv = n.PV
                            }).ToList();
                }
            }
        }

        public ReturnObject SaveNerasta(KartotekaNerastovaPregledVM nerast)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (nerast.Id == 0)
                {
                    //dodajemo novi
                    if (applicationDbContext.Nerast.Any(x => x.T_NER == nerast.Nerast))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast sa tim tetovir brojem vec postoji" };
                    }
                    if (string.IsNullOrEmpty(nerast.Rasa))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast mora imati Rasu" };
                    }
                    if (string.IsNullOrEmpty(nerast.Odg))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast mora imati popunjen Odg" };
                    }
                    if (string.IsNullOrEmpty(nerast.Vla))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast mora imati popunjen Vla" };
                    }
                    if (string.IsNullOrEmpty(nerast.Odg))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast mora imati popunjen Odg" };
                    }
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.MBR_KRM == nerast.Mat_broj_majke);
                    var otac = applicationDbContext.Nerast.FirstOrDefault(x => x.MBR_NER == nerast.Mat_broj_oca);
                    applicationDbContext.Nerast.Add(new Nerast()
                    {
                        T_NER = nerast.Nerast,
                        BR_DOZVOLE = nerast.Broj_dozvole,
                        BR_SISA = nerast.Sisa,
                        DAT_ROD = nerast.Rodjen,
                        EKST = nerast.Ekst,
                        GNM = krmaca != null ? krmaca.GN : "",
                        MBR_MAJKE = krmaca != null ? krmaca.MBR_KRM : "",
                        T_MAJKE10 = krmaca != null ? krmaca.T_KRM : "",
                        GNO = otac != null ? otac.GN : "",
                        MBR_OCA = otac != null ? otac.MBR_NER : "",
                        T_OCA10 = otac != null ? otac.T_NER : "",
                        MBR_NER = nerast.Vla + nerast.Odg + nerast.Rasa + nerast.Tetovir,
                        HBBROJ = nerast.HB_Broj,
                        RBBROJ = nerast.RB_Broj,
                        IZ_LEGLA = nerast.Iz_legla,
                        KLASA = nerast.Klasa,
                        MARKICA = nerast.Markica,
                        NA_FARM_OD = nerast.Na_farmi_od,
                        NAPOMENA = nerast.Napomena,
                        ODG = nerast.Odg,
                        VLA = nerast.Vla,
                        RASA = nerast.Rasa,
                        OPIS = nerast.Opis,
                        TETOVIRN10 = nerast.Tetovir,
                        PV = nerast.Pv,
                        SI1 = nerast.Si1
                    });
                }
                else
                {
                    //azuriramo postojeci
                    var entity = applicationDbContext.Nerast.FirstOrDefault(x => x.Id == nerast.Id);
                    if (entity == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
                    }
                    entity.BR_DOZVOLE = nerast.Broj_dozvole;
                    entity.BR_SISA = nerast.Sisa;
                    entity.EKST = nerast.Ekst;
                    entity.HBBROJ = nerast.HB_Broj;
                    entity.RBBROJ = nerast.RB_Broj;
                    entity.KLASA = nerast.Klasa;
                    entity.MARKICA = nerast.Markica;
                    entity.NAPOMENA = nerast.Napomena;
                    entity.OPIS = nerast.Opis;
                    entity.PV = nerast.Pv;
                    entity.SI1 = nerast.Si1;
                }
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }

        public ReturnObject DeleteNerasta(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var entity = applicationDbContext.Nerast.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
                }
                applicationDbContext.Nerast.Remove(entity);
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }

        public ReturnObject DeleteNazimice(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var entity = applicationDbContext.Krmaca.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return new ReturnObject() { Success = false, Message = "Nerast nije pronadjen" };
                }
                applicationDbContext.Krmaca.Remove(entity);
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }
        public KartotekaNerastovaPregledVM GetByT_Kod(string t_Kod)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from n in applicationDbContext.Nerast.Where(x => x.T_NER == t_Kod)
                        join s in applicationDbContext.Skart on n.Id equals s.NerastId into ss
                        from s in ss.DefaultIfEmpty()
                        select new KartotekaNerastovaPregledVM()
                        {
                            Nerast = n.TETOVIRN10,
                            Id = n.Id,
                            Ekst = n.EKST,
                            Na_farmi_od = n.NA_FARM_OD,
                            GnM = n.GNM,
                            GnO = n.GNO,
                            HB_Broj = n.HBBROJ,
                            Iskljucen = s.D_SKART,
                            Iz_legla = n.IZ_LEGLA,
                            Klasa = n.KLASA,
                            Markica = n.MARKICA,
                            Mat_broj_nerasta = n.MBR_NER,
                            Mat_broj_majke = n.MBR_MAJKE,
                            Mat_broj_oca = n.MBR_OCA,
                            Mrtvo = n.UK_MRTVO,
                            Napomena = n.NAPOMENA,
                            Odg = n.ODG,
                            Opis = n.OPIS,
                            Rasa = n.RASA,
                            Razl = s.RAZL_SK,
                            RB_Broj = n.RBBROJ,
                            Rodjen = n.DAT_ROD,
                            Si1 = n.SI1,
                            Si2 = n.SI2,
                            Si3 = n.SI3,
                            Sisa = n.BR_SISA,
                            Tetovir = n.TETOVIRN10,
                            Tet_majke = n.T_MAJKE10,
                            Tet_oca = n.T_OCA10,
                            Vla = n.VLA,
                            Zivo = n.UK_ZIVO,
                            Broj_dozvole = n.BR_DOZVOLE,
                            Br_op = n.BR_OP,
                            Br_os = n.BR_OS,
                            Prvi_skok = n.D_P_SKOKA,
                            Pv = n.PV
                        }).FirstOrDefault();
            }
        }

        public KartotekaNerastovaPregledVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from n in applicationDbContext.Nerast.Where(x => x.Id == id)
                        join s in applicationDbContext.Skart on n.Id equals s.NerastId into ss
                        from s in ss.DefaultIfEmpty()
                        select new KartotekaNerastovaPregledVM()
                        {
                            Nerast = n.TETOVIRN10,
                            Id = n.Id,
                            Ekst = n.EKST,
                            Na_farmi_od = n.NA_FARM_OD,
                            GnM = n.GNM,
                            GnO = n.GNO,
                            HB_Broj = n.HBBROJ,
                            Iskljucen = s.D_SKART,
                            Iz_legla = n.IZ_LEGLA,
                            Klasa = n.KLASA,
                            Markica = n.MARKICA,
                            Mat_broj_nerasta = n.MBR_NER,
                            Mat_broj_majke = n.MBR_MAJKE,
                            Mat_broj_oca = n.MBR_OCA,
                            Mrtvo = n.UK_MRTVO,
                            Napomena = n.NAPOMENA,
                            Odg = n.ODG,
                            Opis = n.OPIS,
                            Rasa = n.RASA,
                            Razl = s.RAZL_SK,
                            RB_Broj = n.RBBROJ,
                            Rodjen = n.DAT_ROD,
                            Si1 = n.SI1,
                            Si2 = n.SI2,
                            Si3 = n.SI3,
                            Sisa = n.BR_SISA,
                            Tetovir = n.TETOVIRN10,
                            Tet_majke = n.T_MAJKE10,
                            Tet_oca = n.T_OCA10,
                            Vla = n.VLA,
                            Zivo = n.UK_ZIVO,
                            Broj_dozvole = n.BR_DOZVOLE,
                            Br_op = n.BR_OP,
                            Br_os = n.BR_OS,
                            Prvi_skok = n.D_P_SKOKA,
                            Pv = n.PV
                        }).FirstOrDefault();
            }
        }

        public List<KartotekaNerastovaPregled_GodineVM> KartotekaNerastovaPregled_Godine(string mbrNerasta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var mbrNerastaParametar = new SqlParameter("@maticniBrojNerasta", mbrNerasta);

                return applicationDbContext.Database.SqlQuery<KartotekaNerastovaPregled_GodineVM>("KartotekaNerastovaPregled_Godine @maticniBrojNerasta", mbrNerastaParametar).ToList();
            }
        }


        public List<AnalizaPotomstvaPoOcuVM> AnalizaPotomstvaPoOcu(string mbrNerasta)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var mbrNerastaParametar = new SqlParameter("@mbr_oca", mbrNerasta);

                return applicationDbContext.Database.SqlQuery<AnalizaPotomstvaPoOcuVM>("AnalizaPotomstvaPoOcu @mbr_oca", mbrNerastaParametar).ToList();
            }
        }


        public List<PregledAktivnihNerastovaNaDanVM> PregledAktivnihNerastovaNaDan(DateTime aktivniNaDan, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", aktivniNaDan);
                var sviNerastoviParametar = new SqlParameter("@sviNerastovi", sviNerastovi);

                return applicationDbContext.Database.SqlQuery<PregledAktivnihNerastovaNaDanVM>("PregledAktivnihNerastovaNaDan @dan, @sviNerastovi", danParametar, sviNerastoviParametar).ToList();
            }
        }

        public List<PregledAktivnihNerastovaNaDanRasnaStrukturaVM> PregledAktivnihNerastovaNaDanRasnaStruktura(DateTime aktivniNaDan, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", aktivniNaDan);
                var sviNerastoviParametar = new SqlParameter("@sviNerastovi", sviNerastovi);

                return applicationDbContext.Database.SqlQuery<PregledAktivnihNerastovaNaDanRasnaStrukturaVM>("PregledAktivnihNerastovaNaDanRasnaStruktura @dan, @sviNerastovi", danParametar, sviNerastoviParametar).ToList();
            }
        }
        public List<PregledAktivnihNerastovaNaDanKlasnaStrukturaVM> PregledAktivnihNerastovaNaDanKlasnaStruktura(DateTime aktivniNaDan, bool sviNerastovi)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", aktivniNaDan);
                var sviNerastoviParametar = new SqlParameter("@sviNerastovi", sviNerastovi);

                return applicationDbContext.Database.SqlQuery<PregledAktivnihNerastovaNaDanKlasnaStrukturaVM>("PregledAktivnihNerastovaNaDanKlasnaStruktura @dan, @sviNerastovi", danParametar, sviNerastoviParametar).ToList();
            }
        }
        #endregion
        #region 05KartotekaNazimica
        public List<KartotekaKrmacaPregledVM> KartotekaNazimicaPregled(bool iskljucena)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (iskljucena)
                {
                    return (from k in applicationDbContext.Krmaca.Where(x => x.CIKLUS == 1)
                            join s in applicationDbContext.Skart on k.Id equals s.KrmacaId
                            select new KartotekaKrmacaPregledVM()
                            {
                                Krmaca = k.TETOVIRK10,
                                Id = k.Id,
                                Cliklus = k.CIKLUS,
                                Ekst = k.EKST,
                                Faza = k.FAZA,
                                Na_farmi_od = k.NA_FARM_OD,
                                Gn = k.GN,
                                GnM = k.GNM,
                                GnO = k.GNO,
                                Grupa = k.GRUPA,
                                HB_Broj = k.HBBROJ,
                                Iskljucena = s.D_SKART,
                                Iz_legla = k.IZ_LEGLA,
                                Klasa = k.KLASA,
                                Lakt = k.UK_LAKT,
                                Markica = k.MARKICA,
                                Mat_broj_majke = k.MBR_MAJKE,
                                Mat_broj_oca = k.MBR_OCA,
                                Mrtvo = k.UK_MRTVO,
                                Napomena = k.NAPOMENA,
                                Odg = k.ODG,
                                Opis = k.OPIS,
                                Paritet = k.PARIT,
                                Prazni_dani = k.UK_PRAZNI,
                                Proizv_d = k.UK_PROIZ_D,
                                Rasa = k.RASA,
                                Razl = s.RAZL_SK,
                                RB_Broj = k.RBBROJ,
                                Rodjena = k.DAT_ROD,
                                Si1 = k.SI1,
                                Si2 = k.SI2,
                                Si3 = k.SI3,
                                Sisa = k.BR_SISA,
                                Tetovir = k.TETOVIRK10,
                                Tet_majke = k.T_MAJKE10,
                                Tet_oca = k.T_OCA10,
                                Vla = k.VLA,
                                Zaluc = k.UK_ZAL,
                                Zenski = k.UK_ZIVO_Z,
                                Zivo = k.UK_ZIVO
                            }).ToList();
                }
                else
                {
                    return (from k in applicationDbContext.Krmaca.Where(x => x.CIKLUS == 1)
                            join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                            where ss.Count() == 0
                            select new KartotekaKrmacaPregledVM()
                            {
                                Krmaca = k.TETOVIRK10,
                                Id = k.Id,
                                Cliklus = k.CIKLUS,
                                Ekst = k.EKST,
                                Faza = k.FAZA,
                                Na_farmi_od = k.NA_FARM_OD,
                                Gn = k.GN,
                                GnM = k.GNM,
                                GnO = k.GNO,
                                Grupa = k.GRUPA,
                                HB_Broj = k.HBBROJ,
                                Iskljucena = null,
                                Iz_legla = k.IZ_LEGLA,
                                Klasa = k.KLASA,
                                Lakt = k.UK_LAKT,
                                Markica = k.MARKICA,
                                Mat_broj_majke = k.MBR_MAJKE,
                                Mat_broj_oca = k.MBR_OCA,
                                Mrtvo = k.UK_MRTVO,
                                Napomena = k.NAPOMENA,
                                Odg = k.ODG,
                                Opis = k.OPIS,
                                Paritet = k.PARIT,
                                Prazni_dani = k.UK_PRAZNI,
                                Proizv_d = k.UK_PROIZ_D,
                                Rasa = k.RASA,
                                Razl = "",
                                RB_Broj = k.RBBROJ,
                                Rodjena = k.DAT_ROD,
                                Si1 = k.SI1,
                                Si2 = k.SI2,
                                Si3 = k.SI3,
                                Sisa = k.BR_SISA,
                                Tetovir = k.TETOVIRK10,
                                Tet_majke = k.T_MAJKE10,
                                Tet_oca = k.T_OCA10,
                                Vla = k.VLA,
                                Zaluc = k.UK_ZAL,
                                Zenski = k.UK_ZIVO_Z,
                                Zivo = k.UK_ZIVO
                            }).ToList();
                }
            }
        }

        public List<PripustiNazimicaVM> PripustiNazimica(DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => DbFunctions.TruncateTime(x.DAT_PRIP) >= dateOd.Date && DbFunctions.TruncateTime(x.DAT_PRIP) <= dateDo.Date && x.CIKLUS == 1)
                        join k in applicationDbContext.Krmaca.Where(x => x.CIKLUS == 1) on p.KrmacaId equals k.Id
                        join n in applicationDbContext.Nerast on p.NerastId equals n.Id into ns
                        from n in ns.DefaultIfEmpty()
                        join n2 in applicationDbContext.Nerast on p.NerastId equals n2.Id into n2s
                        from n2 in n2s.DefaultIfEmpty()
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new PripustiNazimicaVM
                        {
                            Box = p.BOX,
                            Ciklus = 1,
                            Nazimica = k.TETOVIRK10,
                            Nerast = n.TETOVIRN10 != null ? n.TETOVIRN10 : n2.TETOVIRN10,
                            Obj = p.OBJ,
                            Ocena = p.OCENA,
                            Osem = p.OSEM,
                            Pripustena = p.DAT_PRIP,
                            Pv = p.PV,
                            Rasa = k.RASA,
                            Raz = p.RAZL_P,
                            Rbp = p.RBP,
                            Sk = s.RAZL_SK
                        }).ToList();
            }
        }

        public List<PripustiNazimicaOcekivaniDatumPrasenjaVM> PripustiNazimicaOcekivaniDatumPrasenja(DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => DbFunctions.TruncateTime(DbFunctions.AddDays(x.DAT_PRIP, 115)) >= dateOd.Date && DbFunctions.TruncateTime(DbFunctions.AddDays(x.DAT_PRIP, 115)) <= dateDo.Date && x.CIKLUS == 1)
                        join k in applicationDbContext.Krmaca.Where(x => x.CIKLUS == 1) on p.KrmacaId equals k.Id
                        join n in applicationDbContext.Nerast on p.NerastId equals n.Id into ns
                        from n in ns.DefaultIfEmpty()
                        join n2 in applicationDbContext.Nerast on p.NerastId equals n2.Id into n2s
                        from n2 in n2s.DefaultIfEmpty()
                        select new PripustiNazimicaOcekivaniDatumPrasenjaVM
                        {
                            Box = p.BOX,
                            Ciklus = 1,
                            Nazimica = k.TETOVIRK10,
                            Nerast = n.TETOVIRN10 != null ? n.TETOVIRN10 : n2.TETOVIRN10,
                            Obj = p.OBJ,
                            Osem = p.OSEM,
                            Pripustena = p.DAT_PRIP,
                            Pv = p.PV,
                            Rasa = k.RASA,
                            Raz = p.RAZL_P,
                            Rbp = p.RBP,
                            Mbr_krm = k.MBR_KRM,
                            Ocek_pras = DbFunctions.AddDays(p.DAT_PRIP, 115)
                        }).ToList();
            }
        }

        public List<OpraseneNazimiceVM> OpraseneNazimice(DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => DbFunctions.TruncateTime(x.DAT_PRAS) >= dateOd.Date && DbFunctions.TruncateTime(x.DAT_PRAS) <= dateDo.Date && x.PARIT == 1)
                        join k in applicationDbContext.Krmaca.Where(x => x.CIKLUS == 1) on p.KrmacaId equals k.Id
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new OpraseneNazimiceVM
                        {
                            Box = p.BOX,
                            Parit = 1,
                            Nazimica = k.TETOVIRK10,
                            Obj = p.OBJ,
                            Oprasena = p.DAT_PRAS,
                            Rasa = k.RASA,
                            Alvaarhho = p.ANOMALIJE,
                            Dod = p.DODATO,
                            Gaji = p.GAJI,
                            Mrtvo = p.MRTVO,
                            Odu = p.ODUZETO,
                            Tez = p.TEZ_LEG,
                            Ukupno = p.ZIVO + p.MRTVO,
                            Zivo = p.ZIVO,
                            Sk = s.RAZL_SK
                        }).ToList();
            }
        }

        //
        public List<ZaluceneNazimiceVM> ZaluceneNazimice(DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from z in applicationDbContext.Zaluc.Where(x => DbFunctions.TruncateTime(x.DAT_ZAL) >= dateOd.Date && DbFunctions.TruncateTime(x.DAT_ZAL) <= dateDo.Date && x.PARIT == 1)
                        join k in applicationDbContext.Krmaca.Where(x => x.CIKLUS == 1) on z.KrmacaId equals k.Id
                        join p in applicationDbContext.Prasenje.Where(x => x.PARIT == 1) on k.Id equals p.KrmacaId
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new ZaluceneNazimiceVM
                        {
                            Box = z.BOX,
                            Parit = 1,
                            Nazimica = k.TETOVIRK10,
                            Obj = z.OBJ,
                            Zalucena = z.DAT_ZAL,
                            Rasa = k.RASA,
                            Sk = s.RAZL_SK,
                            Zaluc = z.ZALUC,
                            Lakt = DbFunctions.DiffDays(p.DAT_PRAS, z.DAT_ZAL),
                            Gaji = p.GAJI,
                            Tez = z.TEZ_ZAL,
                            Zdr_st = z.ZDR_ST,
                            Zivo = p.ZIVO,
                            Pz = z.PZ
                        }).ToList();
            }
        }

        public List<PregledNazimicaMaticniPodaciVM> PregledNazimicaMaticniPodaci(DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => DbFunctions.TruncateTime(x.DAT_ROD) >= dateOd.Date && DbFunctions.TruncateTime(x.DAT_ROD) <= dateDo.Date && x.CIKLUS == 1)
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new PregledNazimicaMaticniPodaciVM
                        {
                            Nazimica = k.TETOVIRK10,
                            Rasa = k.RASA,
                            Razl_sk = s.RAZL_SK,
                            Br_sisa = k.BR_SISA,
                            Dat_rod = k.DAT_ROD,
                            D_skart = s.D_SKART,
                            Ekst = k.EKST,
                            Mbr_majke = k.MBR_MAJKE,
                            Mbr_oca = k.MBR_OCA,
                            Odg = k.ODG,
                            Si1 = k.SI1,
                            Vla = k.VLA
                        }).ToList();
            }
        }

        public List<PregledNazimicaGrlaZaOsiguranjeVM> PregledNazimicaGrlaZaOsiguranje(DateTime dateOd, DateTime dateDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var dateOdParametar = new SqlParameter("@dateOd", dateOd);
                var dateDoParametar = new SqlParameter("@dateDo", dateDo);

                return applicationDbContext.Database.SqlQuery<PregledNazimicaGrlaZaOsiguranjeVM>("PregledNazimicaGrlaZaOsiguranje @dateOd, @dateDo", dateOdParametar, dateDoParametar).ToList();
            }
        }
        #endregion

        #region OdabirITest

        public DataTable GrlaUTestuIOdabiruNaDan(DateTime dan, int brDana, bool nazimice, bool testirane)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParam = new SqlParameter("@dan", dan);
                var brDanaParam = new SqlParameter("@brDana", brDana);
                var nazimiceParam = new SqlParameter("@nazimice", nazimice);
                var testiraneParam = new SqlParameter("@testirane", testirane);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GrlaUTestuIOdabiruNaDan", con))
                    {
                        cmd.Parameters.Add(danParam);
                        cmd.Parameters.Add(brDanaParam);
                        cmd.Parameters.Add(nazimiceParam);
                        cmd.Parameters.Add(testiraneParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable RegistarPrasadi(DateTime danRodjenjaOd, DateTime danRodjenjaDo, string pol)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danRodjenjaOdParam = new SqlParameter("@danRodjenjaOd", danRodjenjaOd);
                var danRodjenjaDoParam = new SqlParameter("@danRodjenjaDo", danRodjenjaDo);
                var polParam = new SqlParameter("@pol", pol);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("RegistarPrasadi", con))
                    {
                        cmd.Parameters.Add(danRodjenjaOdParam);
                        cmd.Parameters.Add(danRodjenjaDoParam);
                        cmd.Parameters.Add(polParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable PromenaPolaPrasadiGet(DateTime danRodjenjaOd, DateTime danRodjenjaDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danRodjenjaOdParam = new SqlParameter("@danRodjenjaOd", danRodjenjaOd);
                var danRodjenjaDoParam = new SqlParameter("@danRodjenjaDo", danRodjenjaDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("PromenaPolaPrasadiGet", con))
                    {
                        cmd.Parameters.Add(danRodjenjaOdParam);
                        cmd.Parameters.Add(danRodjenjaDoParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable DopunaBrojaSisaKodPrasadiGet(DateTime danRodjenjaOd, DateTime danRodjenjaDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danRodjenjaOdParam = new SqlParameter("@danRodjenjaOd", danRodjenjaOd);
                var danRodjenjaDoParam = new SqlParameter("@danRodjenjaDo", danRodjenjaDo);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DopunaBrojaSisaKodPrasadiGet", con))
                    {
                        cmd.Parameters.Add(danRodjenjaOdParam);
                        cmd.Parameters.Add(danRodjenjaDoParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public void PromenaPolaPrasadiSave(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("PromenaPolaPrasadiSave", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }
        public void DopunaBrojaSisaKodPrasadiSave(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("DopunaBrojaSisaKodPrasadiSave", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public List<DopunaRegistraTetoviranihPrasadiVM> DopunaRegistraTetoviranihPrasadi(DateTime datePrasenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => DbFunctions.TruncateTime(x.DAT_PRAS) == datePrasenja.Date)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new DopunaRegistraTetoviranihPrasadiVM
                        {
                            Id = p.Id,
                            Krmaca = k.TETOVIRK10,
                            Broj_registra = p.REG_PRAS,
                            MaticniBroj = k.MBR_KRM,
                            Parit = p.PARIT,
                            Tmp_do = p.TMP_DO,
                            Tmp_od = p.TMP_OD,
                            Tov_do = p.TOV_DO,
                            Tov_od = p.TOV_OD,
                            Tzp_do = p.TZP_DO,
                            Tzp_od = p.TZP_OD,
                            Uginulo = p.UGIN,
                            Zivo = p.ZIVO
                        }).ToList();
            }
        }

        public DataTable TetoviranaPrasadNaStanjuNaDan(DateTime prasadNaStanjuNaDan, int starostiDoDana, string pol)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var prasadNaStanjuNaDanParam = new SqlParameter("@prasadNaStanjuNaDan", prasadNaStanjuNaDan);
                var starostiDoDanaParam = new SqlParameter("@starostiDoDana", starostiDoDana);
                var polParam = new SqlParameter("@pol", pol);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("TetoviranaPrasadNaStanjuNaDan", con))
                    {
                        cmd.Parameters.Add(prasadNaStanjuNaDanParam);
                        cmd.Parameters.Add(starostiDoDanaParam);
                        cmd.Parameters.Add(polParam);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            // Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        #endregion
    }
}
