﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase._02Prasenje;
using BusnissLayer.ViewModels.Prase._07Kartoteka;
using BusnissLayer.ViewModels.Prase.Prasenje;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class PrasenjeService
    {
        #region 01EvidencijaPrasenja

        public ReturnObject Save(EvidencijaPrasenjaAzuriranjeVM prasenje)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                if (prasenje.Id == 0)
                {
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == prasenje.Krmaca);
                    if (krmaca == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena. Ukoliko je nazimica, prvo prenesite podatke iz testa." };
                    }
                    var entity = applicationDbContext.Prasenje.Add(new Prasenje()
                    {
                        DAT_PRAS = prasenje.DatumPrasenja,
                        DODATO = prasenje.DODATO,
                        ODUZETO = prasenje.ODUZETO,
                        OBJ = prasenje.OBJ,
                        BAB = prasenje.BAB,
                        BOX = prasenje.BOX,
                        UBIJ = prasenje.UBIJ,
                        ANOMALIJE = prasenje.ANOMALIJE,
                        GAJI = prasenje.GAJI,
                        MAT = prasenje.MAT,
                        PARIT = prasenje.PARIT,
                        RAD = prasenje.RAD,
                        REG_PRAS = prasenje.REG_PRAS,
                        RB = prasenje.RB,
                        ZAPRIM = prasenje.ZAPRIM,
                        KrmacaId = krmaca.Id,
                        MRTVO = prasenje.MRTVO,
                        OL_P = prasenje.OL_P,
                        REFK = prasenje.REFK,
                        TEZ_LEG = prasenje.TEZ_LEG,
                        TMP_DO = prasenje.TMP_DO,
                        TMP_OD = prasenje.TMP_OD,
                        TOV_DO = prasenje.TOV_DO,
                        TOV_OD = prasenje.TOV_OD,
                        TZP_DO = prasenje.TZP_DO,
                        TZP_OD = prasenje.TZP_OD,
                        UGIN = prasenje.UGIN,
                        UGNJ = prasenje.UGNJ,
                        ZIVO = prasenje.ZIVO,
                        ZIVOZ = prasenje.ZIVOZ
                    });
                    var result = DodajNovuPrasadNakonPrasenja(applicationDbContext, prasenje, krmaca, entity);
                    applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = true, Id = entity.Id };
                }
                else
                {
                    //var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == prasenje.Krmaca);
                    //if (krmaca == null)
                    //{
                    //    return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
                    //}
                    //var entity = applicationDbContext.Prasenje.FirstOrDefault(x => x.Id == prasenje.Id);
                    //if (entity == null)
                    //{
                    //    return new ReturnObject() { Success = false, Message = "Prasenje nije pronadjeno" };
                    //}
                    //entity.DAT_PRAS = prasenje.DatumPrasenja;
                    //entity.DODATO = prasenje.DODATO;
                    //entity.ODUZETO = prasenje.ODUZETO;
                    //entity.OBJ = prasenje.OBJ;
                    //entity.BAB = prasenje.BAB;
                    //entity.BOX = prasenje.BOX;
                    //entity.UBIJ = prasenje.UBIJ;
                    //entity.ANOMALIJE = prasenje.ANOMALIJE;
                    //entity.GAJI = prasenje.GAJI;
                    //entity.MAT = prasenje.MAT;
                    //entity.PARIT = prasenje.PARIT;
                    //entity.RAD = prasenje.RAD;
                    //entity.REG_PRAS = prasenje.REG_PRAS;
                    //entity.RB = prasenje.RB;
                    //entity.ZAPRIM = prasenje.ZAPRIM;
                    //entity.KrmacaId = krmaca.Id;
                    //entity.MRTVO = prasenje.MRTVO;
                    //entity.OL_P = prasenje.OL_P;
                    //entity.REFK = prasenje.REFK;
                    //entity.TEZ_LEG = prasenje.TEZ_LEG;
                    //entity.TMP_DO = prasenje.TMP_DO;
                    //entity.TMP_OD = prasenje.TMP_OD;
                    //entity.TOV_DO = prasenje.TOV_DO;
                    //entity.TOV_OD = prasenje.TOV_OD;
                    //entity.TZP_DO = prasenje.TZP_DO;
                    //entity.TZP_OD = prasenje.TZP_OD;
                    //entity.UGIN = prasenje.UGIN;
                    //entity.UGNJ = prasenje.UGNJ;
                    //entity.ZIVO = prasenje.ZIVO;
                    //entity.ZIVOZ = prasenje.ZIVOZ;
                    //applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = false, Message = "Izmene nisu dozvoljene, izbrisite pa dodajte ponovo" };
                }
            }
        }

        public DataTable NIIManjePrasadiUMPrasenja(int paritet, int zivo, string rasa, bool prvih)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var paritetParametar = new SqlParameter("@Paritet", paritet);
                var zivoParametar = new SqlParameter("@Zivo", zivo);
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                var prvihParametar = new SqlParameter("@Prvih", prvih);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.KrmaceKojePraseNIMANJEPrasadiUMPrasenja", con))
                    {
                        cmd.Parameters.Add(paritetParametar);
                        cmd.Parameters.Add(zivoParametar);
                        cmd.Parameters.Add(rasaParametar);
                        cmd.Parameters.Add(prvihParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable NIIVIsePrasadiUMPrasenja(int paritet, int zivo, string rasa, bool prvih)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var paritetParametar = new SqlParameter("@Paritet", paritet);
                var zivoParametar = new SqlParameter("@Zivo", zivo);
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                var prvihParametar = new SqlParameter("@Prvih", prvih);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.KrmaceKojePraseNPrasadiUMPrasenja", con))
                    {
                        cmd.Parameters.Add(paritetParametar);
                        cmd.Parameters.Add(zivoParametar);
                        cmd.Parameters.Add(rasaParametar);
                        cmd.Parameters.Add(prvihParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable DistribucijaZivoRodjenePrasadi(DateTime fromDate, DateTime toDate, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.DistrobucijaZivoRodjenePrasadiPoPrasenjima", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.Parameters.Add(rasaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        public DataTable DistribucijaZivoRodjenePrasadiReport(DateTime fromDate, DateTime toDate, string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.DistrobucijaZivoRodjenePrasadiPoPrasenjimaReport", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.Parameters.Add(rasaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable ProizvodniRezultatiKrmacaPoParitima(string rasa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var rasaParametar = new SqlParameter("@Rasa", rasa);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.ProizvodniRezultatiKrmacaPoParitima", con))
                    {
                        cmd.Parameters.Add(rasaParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable ProizvodniRezultatiKrmaca(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("ProizvodniRezultatiKrmaca", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public DataTable KontrolaProduktivnostiNerastovskihMajki(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KontrolaProduktivnostiNerastovskihMajki", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public ReturnObject SaveOdDoMuskuPrasad(int prasenjeId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var prasenje = applicationDbContext.Prasenje.FirstOrDefault(x => x.Id == prasenjeId);
                if (prasenje == null)
                    return new ReturnObject() { Success = false, Message = "Prasenje nije pronadjeno." };


                if (prasenje.TMP_OD == null || prasenje.TMP_DO == null)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Polja TMP_OD i TMP_DO moraju biti uneseni." };


                if (prasenje.TMP_DO - prasenje.TMP_OD > prasenje.ZIVO - prasenje.ZIVOZ)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Razlika TMP_DO - TMP_OD mora biti manja od razlike ZIVO - ZIVOZ." };

                if (prasenje.TMP_DO - prasenje.TMP_OD <= 0)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Razlika TMP_DO - TMP_OD mora biti veca od 0." };

                var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.Id == prasenje.KrmacaId);
                if (krmaca == null)
                {
                    return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena. Ukoliko je nazimica, prvo prenesite podatke iz testa." };
                }

                var pripust = applicationDbContext.Pripust.Include(x => x.Nerast).Where(x => x.KrmacaId == krmaca.Id).OrderByDescending(x => x.DAT_PRIP).First();
                if (pripust == null)
                {
                    throw new Exception("Pripust nije pronadjen stoga ne mozemo pronaci Nerasta");
                }
                Nerast nerast = null;
                if (pripust != null && pripust.Nerast != null)
                {
                    nerast = pripust.Nerast;
                }
                if (nerast == null)
                {
                    throw new Exception("Nerast nije pronadjen");
                }

                if (prasenje.TMP_OD.HasValue && prasenje.TMP_DO.HasValue)
                {
                    for (int i = prasenje.TMP_OD.Value; i <= prasenje.TMP_DO.Value; i++)
                    {
                        var prasBroj = GenerateTetovirBroj(i);
                        var mbrBroj = "092092" + nerast.RASA.Substring(0, 2) + krmaca.RASA.Substring(0, 2) + prasBroj;
                        var gn = (prasenje.DAT_PRAS.Year - 2000).ToString();
                        if (!applicationDbContext.Prase.Any(x => x.T_PRAS == prasBroj))
                        {
                            prasenje.Prasad.Add(new DataAccessLayer.Entities.PIGS.Prase()
                            {
                                DAT_ROD = prasenje.DAT_PRAS,
                                IZ_LEGLA = prasenje.PARIT.ToString() + " " + prasenje.ZIVO != null ? prasenje.ZIVO.ToString() : "" + " " + prasenje.MRTVO != null ? prasenje.MRTVO.ToString() : "",
                                GN = gn,
                                KrmacaId = krmaca.Id,
                                MBR_PRAS = mbrBroj,
                                POL = "M",
                                MBR_PRAS1 = mbrBroj,
                                NerastId = nerast.Id,
                                PRAS_BROJ = prasBroj,
                                T_PRAS = prasBroj,
                                MBR_MAJKE = krmaca.MBR_KRM,
                                MBR_OCA = nerast.MBR_NER
                            });
                        }
                    }
                }
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true, Id = prasenje.Id };
            }
        }

        public ReturnObject SaveOdDoZenskuPrasad(int prasenjeId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var prasenje = applicationDbContext.Prasenje.FirstOrDefault(x => x.Id == prasenjeId);
                if (prasenje == null)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Prasenje nije pronadjeno." };

                if (prasenje.TZP_OD == null || prasenje.TZP_DO == null)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Polja TZP_OD i TZP_OD moraju biti uneseni." };


                if (prasenje.TZP_DO - prasenje.TZP_OD > prasenje.ZIVOZ)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Razlika TZP_DO - TZP_OD mora biti manja od ZIVOZ." };

                if (prasenje.TZP_DO - prasenje.TZP_OD <= 0)
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Razlika TZP_DO - TZP_OD mora biti manja od ZIVOZ." };

                var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.Id == prasenje.KrmacaId);
                if (krmaca == null)
                {
                    return new ReturnObject() { Success = false, Message = "Neuspesno. Krmaca nije pronadjena. Ukoliko je nazimica, prvo prenesite podatke iz testa." };
                }

                var pripust = applicationDbContext.Pripust.Include(x => x.Nerast).Where(x => x.KrmacaId == krmaca.Id).OrderByDescending(x => x.DAT_PRIP).First();
                if (pripust == null)
                {
                    throw new Exception("Pripust nije pronadjen stoga ne mozemo pronaci Nerasta");
                }
                Nerast nerast = null;
                if (pripust != null && pripust.Nerast != null)
                {
                    nerast = pripust.Nerast;
                }
                if (nerast == null)
                {
                    throw new Exception("Nerast nije pronadjen");
                }


                if (prasenje.TZP_OD.HasValue && prasenje.TZP_DO.HasValue)
                {
                    for (int i = prasenje.TZP_OD.Value; i <= prasenje.TZP_DO.Value; i++)
                    {
                        var prasBroj = GenerateTetovirBroj(i);
                        var mbrBroj = "092092" + nerast.RASA.Substring(0, 2) + krmaca.RASA.Substring(0, 2) + prasBroj;
                        var gn = (prasenje.DAT_PRAS.Year - 2000).ToString();
                        if (!applicationDbContext.Prase.Any(x => x.T_PRAS == prasBroj))
                        {
                            prasenje.Prasad.Add(new DataAccessLayer.Entities.PIGS.Prase()
                            {
                                DAT_ROD = prasenje.DAT_PRAS,
                                IZ_LEGLA = prasenje.PARIT.ToString() + " " + prasenje.ZIVO != null ? prasenje.ZIVO.ToString() : "" + " " + prasenje.MRTVO != null ? prasenje.MRTVO.ToString() : "",
                                GN = gn,
                                KrmacaId = krmaca.Id,
                                MBR_PRAS = mbrBroj,
                                POL = "Z",
                                MBR_PRAS1 = mbrBroj,
                                NerastId = nerast.Id,
                                PRAS_BROJ = prasBroj,
                                T_PRAS = prasBroj,
                                MBR_MAJKE = krmaca.MBR_KRM,
                                MBR_OCA = nerast.MBR_NER
                            });
                        }
                    }
                }

                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true, Id = prasenje.Id };
            }
        }

        public ReturnObject SaveRegistraPrasadi(DopunaRegistraTetoviranihPrasadiVM prasenje)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                if (prasenje.Id != 0)
                {
                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == prasenje.Krmaca);
                    if (krmaca == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Krmaca nije pronadjena" };
                    }
                    var entity = applicationDbContext.Prasenje.FirstOrDefault(x => x.Id == prasenje.Id);
                    if (entity == null)
                    {
                        return new ReturnObject() { Success = false, Message = "Prasenje nije pronadjeno" };
                    }
                    entity.PARIT = prasenje.Parit;
                    entity.REG_PRAS = prasenje.Broj_registra;
                    entity.KrmacaId = krmaca.Id;
                    entity.TMP_DO = prasenje.Tmp_do;
                    entity.TMP_OD = prasenje.Tmp_od;
                    entity.TOV_DO = prasenje.Tov_do;
                    entity.TOV_OD = prasenje.Tov_od;
                    entity.TZP_DO = prasenje.Tzp_do;
                    entity.TZP_OD = prasenje.Tzp_od;
                    entity.UGIN = prasenje.Uginulo;
                    entity.ZIVO = prasenje.Zivo;
                    EvidencijaPrasenjaAzuriranjeVM evidencijaPrasenjaAzuriranjeVM = new EvidencijaPrasenjaAzuriranjeVM()
                    {
                        PARIT = prasenje.Parit,
                        DatumPrasenja = entity.DAT_PRAS,
                        TMP_DO = entity.TMP_DO,
                        TMP_OD = entity.TMP_OD,
                        TOV_DO = entity.TOV_DO,
                        TOV_OD = entity.TOV_OD,
                        TZP_DO = entity.TZP_DO,
                        TZP_OD = entity.TZP_OD,
                        UGIN = entity.UGIN,
                        ZIVO = entity.ZIVO
                    };
                    var result = DodajNovuPrasadNakonPrasenja(applicationDbContext, evidencijaPrasenjaAzuriranjeVM, krmaca, entity);
                    applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = true };
                }
                else
                {
                    return new ReturnObject() { Success = false, Message = "Id mora biti popunjeno polje" };
                }
            }
        }


        public ReturnObject DodajNovuPrasadNakonPrasenja(ApplicationDbContext applicationDbContext, EvidencijaPrasenjaAzuriranjeVM prasenje, Krmaca krmaca, Prasenje prasenjeEntity)
        {
            var pripust = applicationDbContext.Pripust.Include(x => x.Nerast).Where(x => x.KrmacaId == krmaca.Id).OrderByDescending(x => x.DAT_PRIP).First();
            if (pripust == null)
            {
                throw new Exception("Pripust nije pronadjen stoga ne mozemo pronaci Nerasta");
            }
            Nerast nerast = null;
            if (pripust != null && pripust.Nerast != null)
            {
                nerast = pripust.Nerast;
            }
            if (nerast == null)
            {
                throw new Exception("Nerast nije pronadjen");
            }
            //var majcinPedigre = applicationDbContext.Pedigre.FirstOrDefault(x => x.KrmacaId == krmaca.Id);
            //var ocevPedigre = applicationDbContext.Pedigre.FirstOrDefault(x => x.NerastId == nerast.Id);
            //var majcinoPoreklo = applicationDbContext.Poreklo.FirstOrDefault(x => x.KrmacaId == krmaca.Id);
            //var ocevoPoreklo = applicationDbContext.Poreklo.FirstOrDefault(x => x.NerastId == nerast.Id);
            if (prasenje.TZP_DO.HasValue && prasenje.TZP_OD.HasValue)
            {
                DodajZenskuPrasad(applicationDbContext, prasenje, krmaca, nerast, nerast.RASA.Substring(0, 2) + krmaca.RASA.Substring(0, 2), prasenjeEntity);
            }
            if (prasenje.TMP_DO.HasValue && prasenje.TMP_OD.HasValue)
            {
                DodajMuskuPrasad(applicationDbContext, prasenje, krmaca, nerast, nerast.RASA.Substring(0, 2) + krmaca.RASA.Substring(0, 2), prasenjeEntity);
            }
            return new ReturnObject()
            {
                Success = true
            };
        }

        public bool IsLatestPrasenje(int pripustId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var prasenje = applicationDbContext.Prasenje.First(x => x.Id == pripustId);

                var latestPrasenje = applicationDbContext.Prasenje.Where(x => x.KrmacaId == prasenje.KrmacaId).OrderByDescending(x => x.DAT_PRAS).First();

                if (prasenje.DAT_PRAS < latestPrasenje.DAT_PRAS)
                {
                    return false;
                }
                return true;
            }
        }

        public string CheckIskljucenje(string krmaca)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var res = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == krmaca);
                if (res == null)
                    return "Ova krmaca ne postoji";
                if (applicationDbContext.Skart.Any(x => x.KrmacaId == res.Id))
                    return "Ova krmaca je iskljucena";
                if (res.FAZA != 1)
                    return "Ova krmaca nije u odgovarajucoj predfazi.";
                return null;
            }
        }

        public DataTable SP_GetAllByDate(DateTime datumPrasenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumPrasenja.Year, datumPrasenja.Month, datumPrasenja.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.GetAllByDate", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }


        public DataTable SP_GetAllByDateReport(DateTime datumOd,DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danOdParametar = new SqlParameter("@danOd", new DateTime(datumOd.Year, datumOd.Month, datumOd.Day, 0, 0, 0));
                var danDoParametar = new SqlParameter("@danDo", new DateTime(datumDo.Year, datumDo.Month, datumDo.Day, 23, 59, 59));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Prasenje.GetAllByDateReport", con))
                    {
                        cmd.Parameters.Add(danOdParametar);
                        cmd.Parameters.Add(danDoParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public void SP_Delete(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Prasenje.Delete", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void SP_Save(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Prasenje.Save", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        private void DodajMuskuPrasad(ApplicationDbContext applicationDbContext, EvidencijaPrasenjaAzuriranjeVM prasenje, Krmaca majka, Nerast otac, string rasa, Prasenje prasenjeEntity)
        {
            for (int i = prasenje.TMP_OD.Value; i <= prasenje.TMP_DO.Value; i++)
            {
                var prasBroj = GenerateTetovirBroj(i);
                var mbrBroj = "092092" + rasa + prasBroj;
                var gn = (prasenje.DatumPrasenja.Year - 2000).ToString();
                if (!applicationDbContext.Prase.Any(x => x.T_PRAS == prasBroj))
                {
                    prasenjeEntity.Prasad.Add(new DataAccessLayer.Entities.PIGS.Prase()
                    {
                        DAT_ROD = prasenje.DatumPrasenja,
                        IZ_LEGLA = prasenje.PARIT.ToString() + " " + prasenje.ZIVO != null ? prasenje.ZIVO.ToString() : "" + " " + prasenje.MRTVO != null ? prasenje.MRTVO.ToString() : "",
                        GN = gn,
                        KrmacaId = majka.Id,
                        MBR_PRAS = mbrBroj,
                        POL = "M",
                        MBR_PRAS1 = mbrBroj,
                        NerastId = otac.Id,
                        PRAS_BROJ = prasBroj,
                        T_PRAS = prasBroj,
                        MBR_MAJKE = majka.MBR_KRM,
                        MBR_OCA = otac.MBR_NER
                        //Pedigre = new List<Pedigre>() { GeneratePedigre(applicationDbContext, majcinPedigre, ocevPedigre, prasBroj, mbrBroj, "M", "") },
                        //Poreklo = new List<Poreklo>() { GeneratePoreklo(applicationDbContext, majcinoPoreklo, ocevoPoreklo, prasBroj, mbrBroj, "M", "", gn.ToString()) }
                        //PRIPLOD = prasenje.ZAPRIM
                    });
                }
                else
                {
                    throw new Exception("Ovaj tetovir vec postoji za neko prase.");
                }
            }
        }

        private void DodajZenskuPrasad(ApplicationDbContext applicationDbContext, EvidencijaPrasenjaAzuriranjeVM prasenje, Krmaca majka, Nerast otac, string rasa, Prasenje prasenjeEntity)
        {
            for (int i = prasenje.TZP_OD.Value; i <= prasenje.TZP_DO.Value; i++)
            {
                var prasBroj = GenerateTetovirBroj(i);
                var mbrBroj = "092092" + rasa + prasBroj;
                var gn = (prasenje.DatumPrasenja.Year - 2000).ToString();
                if (!applicationDbContext.Prase.Any(x => x.T_PRAS == prasBroj))
                {
                    prasenjeEntity.Prasad.Add(new DataAccessLayer.Entities.PIGS.Prase()
                    {
                        DAT_ROD = prasenje.DatumPrasenja,
                        IZ_LEGLA = prasenje.PARIT.ToString() + " " + prasenje.ZIVO != null ? prasenje.ZIVO.ToString() : "" + " " + prasenje.MRTVO != null ? prasenje.MRTVO.ToString() : "",
                        GN = gn,
                        KrmacaId = majka.Id,
                        MBR_PRAS = mbrBroj,
                        POL = "Z",
                        MBR_PRAS1 = mbrBroj,
                        NerastId = otac.Id,
                        PRAS_BROJ = prasBroj,
                        T_PRAS = prasBroj,
                        MBR_MAJKE = majka.MBR_KRM,
                        MBR_OCA = otac.MBR_NER
                        //Pedigre = new List<Pedigre>() { GeneratePedigre(applicationDbContext, majcinPedigre, ocevPedigre, prasBroj, mbrBroj, "Z", "") },
                        //Poreklo = new List<Poreklo>() { GeneratePoreklo(applicationDbContext, majcinoPoreklo, ocevoPoreklo, prasBroj, mbrBroj, "Z", "", gn.ToString()) }
                        //PRIPLOD = prasenje.ZAPRIM
                    });
                }
                else
                {
                    throw new Exception("Ovaj tetovir vec postoji za neko prase.");
                }
            }
        }

        public string GenerateTetovirBroj(int broj)
        {
            var prasBroj = broj.ToString();
            if (broj < 10)
            {
                prasBroj = "0000" + broj;
            }
            else if (broj < 100)
            {
                prasBroj = "000" + broj;
            }
            else if (broj < 1000)
            {
                prasBroj = "00" + broj;
            }
            else if (broj < 10000)
            {
                prasBroj = "0" + broj;
            }
            return prasBroj;
        }

        public ReturnObject Delete(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var entity = applicationDbContext.Prasenje.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return new ReturnObject() { Success = false, Message = "Prasenje nije pronadjeno" };
                }
                applicationDbContext.Prasenje.Remove(entity);
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }
        public List<EvidencijaPrasenjaPregledVM> EvidencijaPrasenjaPregled(DateTime datumPrasenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => DbFunctions.TruncateTime(x.DAT_PRAS) == datumPrasenja.Date)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new EvidencijaPrasenjaPregledVM()
                        {
                            Krmaca = k.T_KRM,
                            MaticniBroj = k.MBR_KRM,
                            RB = p.RB,
                            PARIT = p.PARIT,
                            OBJ = p.OBJ,
                            BOX = p.BOX,
                            BAB = p.BAB,
                            RAD = p.RAD,
                            MAT = p.MAT,
                            ZIVO = p.ZIVO,
                            MRTVO = p.MRTVO,
                            ZIVOZ = p.ZIVOZ,
                            UBIJ = p.UBIJ,
                            UGNJ = p.UGNJ,
                            ZAPRIM = p.ZAPRIM,
                            UGIN = p.UGIN,
                            DODATO = p.DODATO,
                            ODUZETO = p.ODUZETO,
                            GAJI = p.GAJI,
                            TEZ_LEG = p.TEZ_LEG,
                            ANOMALIJE = p.ANOMALIJE,
                            TMP_OD = p.TMP_OD,
                            TMP_DO = p.TMP_DO,
                            TZP_OD = p.TZP_OD,
                            TZP_DO = p.TZP_DO,
                            TOV_OD = p.TOV_OD,
                            TOV_DO = p.TOV_DO,
                            REG_PRAS = p.REG_PRAS,
                            OL_P = p.OL_P,
                            REFK = p.REFK
                        }).ToList();
            }
        }

        public List<EvidencijaPrasenjaAzuriranjeVM> GetEvidencijaPrasenjaAzuriranjeVM(DateTime datumPrasenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => DbFunctions.TruncateTime(x.DAT_PRAS) == datumPrasenja.Date)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        select new EvidencijaPrasenjaAzuriranjeVM()
                        {
                            Id = p.Id,
                            DatumPrasenja = p.DAT_PRAS,
                            Krmaca = k.T_KRM,
                            RB = p.RB,
                            PARIT = p.PARIT,
                            OBJ = p.OBJ,
                            BOX = p.BOX,
                            BAB = p.BAB,
                            RAD = p.RAD,
                            MAT = p.MAT,
                            ZIVO = p.ZIVO,
                            MRTVO = p.MRTVO,
                            ZIVOZ = p.ZIVOZ,
                            UBIJ = p.UBIJ,
                            UGNJ = p.UGNJ,
                            ZAPRIM = p.ZAPRIM,
                            UGIN = p.UGIN,
                            DODATO = p.DODATO,
                            ODUZETO = p.ODUZETO,
                            GAJI = p.GAJI,
                            TEZ_LEG = p.TEZ_LEG,
                            ANOMALIJE = p.ANOMALIJE,
                            TMP_OD = p.TMP_OD,
                            TMP_DO = p.TMP_DO,
                            TZP_OD = p.TZP_OD,
                            TZP_DO = p.TZP_DO,
                            TOV_OD = p.TOV_OD,
                            TOV_DO = p.TOV_DO,
                            REG_PRAS = p.REG_PRAS,
                            OL_P = p.OL_P,
                            REFK = p.REFK
                        }).ToList();
            }
        }
        #endregion

        #region 02PregledPodataka
        public List<PregledPodatakaPrasenjaVM> PregledPodatakaPrasenja(DateTime datumOd, DateTime datumDo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => x.DAT_PRAS >= datumOd && x.DAT_PRAS < datumDo)
                        join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new PregledPodatakaPrasenjaVM()
                        {
                            Krmaca = k.T_KRM,
                            MaticniBroj = k.MBR_KRM,
                            RB = p.RB,
                            PARIT = p.PARIT,
                            OBJ = p.OBJ,
                            BOX = p.BOX,
                            BAB = p.BAB,
                            RAD = p.RAD,
                            MAT = p.MAT,
                            ZIVO = p.ZIVO,
                            MRTVO = p.MRTVO,
                            ZIVOZ = p.ZIVOZ,
                            UBIJ = p.UBIJ,
                            UGNJ = p.UGNJ,
                            ZAPRIM = p.ZAPRIM,
                            UGIN = p.UGIN,
                            DODATO = p.DODATO,
                            ODUZETO = p.ODUZETO,
                            GAJI = p.GAJI,
                            TEZ_LEG = p.TEZ_LEG,
                            ANOMALIJE = p.ANOMALIJE,
                            TMP_OD = p.TMP_OD,
                            TMP_DO = p.TMP_DO,
                            TZP_OD = p.TZP_OD,
                            TZP_DO = p.TZP_DO,
                            TOV_OD = p.TOV_OD,
                            TOV_DO = p.TOV_DO,
                            REG_PRAS = p.REG_PRAS,
                            OL_P = p.OL_P,
                            REFK = p.REFK,
                            Sk = s.RAZL_SK,
                            Oprasena = p.DAT_PRAS,
                            //Otac
                        }).ToList();
            }
        }

        public DataTable KontrolaProduktivnostiKrmaca(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KontrolaProduktivnostiKrmaca", con))
                    {
                        cmd.Parameters.Add(fromParametar);
                        cmd.Parameters.Add(toParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }
        #endregion

        #region 03KalendarPrasenja
        public List<NeopraseneKrmaceINazimiceVM> NeopraseneKrmaceINazimice(int brDana, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var result = (from p in applicationDbContext.Pripust.Where(x => DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) > brDana)
                                join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                                join n in applicationDbContext.Nerast on p.NerastId equals n.Id
                                join s in applicationDbContext.Skart on p.KrmacaId equals s.KrmacaId into ss
                              join p1 in applicationDbContext.Pripust on p.KrmacaId equals p1.KrmacaId into pp
                              where ss.Count() == 0 && k.FAZA == 1 && p.CIKLUS == k.CIKLUS && !pp.Any(x => x.DAT_PRIP > p.DAT_PRIP)
                              select new NeopraseneKrmaceINazimiceVM()
                                {
                                    Nerast = n.T_NER,
                                    Box = p.BOX,
                                    Ciklus = p.CIKLUS,
                                    Krmaca = k.T_KRM,
                                    Obj = p.OBJ,
                                    Pripustena = p.DAT_PRIP,
                                    Rasa = k.RASA,
                                    Ocek_pras = DbFunctions.AddDays(p.DAT_PRIP, 115),
                                    Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                                    Napomena = k.NAPOMENA
                                }).ToList();

                if (sortOrder == "Dani")
                {
                    return result.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return result.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }
        public List<KrmaceINazimiceZaPrevodUPrasilisteVM> KrmaceINazimiceZaPrevodUPrasiliste(int brDana, string sortOrder)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var result = (from p in applicationDbContext.Pripust.Where(x => DbFunctions.DiffDays(x.DAT_PRIP, DateTime.Now) > brDana)
                              join k in applicationDbContext.Krmaca on p.KrmacaId equals k.Id
                              join n in applicationDbContext.Nerast on p.NerastId equals n.Id
                              join s in applicationDbContext.Skart on p.KrmacaId equals s.KrmacaId into ss
                              join p1 in applicationDbContext.Pripust on p.KrmacaId equals p1.KrmacaId into pp
                              where ss.Count() == 0 && k.FAZA == 1 && p.CIKLUS == k.CIKLUS && !pp.Any(x => x.DAT_PRIP > p.DAT_PRIP)
                              select new KrmaceINazimiceZaPrevodUPrasilisteVM()
                                {
                                  Nerast = n.T_NER,
                                  Box = p.BOX,
                                  Ciklus = p.CIKLUS,
                                  Krmaca = k.T_KRM,
                                  Obj = p.OBJ,
                                  Pripustena = p.DAT_PRIP,
                                  Rasa = k.RASA,
                                  Ocek_pras = DbFunctions.AddDays(p.DAT_PRIP, 115),
                                  Dana = DbFunctions.DiffDays(p.DAT_PRIP, DateTime.Now),
                                  Rbp = p.RBP
                              }).ToList();

                if (sortOrder == "Dani")
                {
                    return result.OrderBy(x => x.Dana).ToList();
                }
                else
                {
                    return result.OrderBy(x => x.Obj).ThenBy(x => x.Box).ToList();
                }
            }
        }
        #endregion
    }
}
