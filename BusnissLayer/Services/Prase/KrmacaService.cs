﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase.Krmaca;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class KrmacaService
    {
        public KrmacaAzuriranjeVM GetByT_Kod(string t_Kod)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => x.T_KRM == t_Kod)
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new KrmacaAzuriranjeVM()
                        {
                            Id = k.Id,
                            BR_SISA = k.BR_SISA,
                            ODG = k.ODG,
                            VLA = k.VLA,
                            CIKLUS = k.PARIT,
                            DAT_ROD = k.DAT_ROD,
                            D_SKART = k.D_SKART,
                            EKST = k.EKST,
                            FAZA = k.FAZA,
                            PREDFAZA = k.PRED_FAZA,
                            KL_P = k.KL_P,
                            GN = k.GN,
                            GNM = k.GNM,
                            GNO = k.GNO,
                            GRUPA = k.GRUPA,
                            HBBROJ = k.HBBROJ,
                            IEZ = k.IEZ,
                            KLASA = k.KLASA,
                            MARKICA = k.MARKICA,
                            MBR_KRM = k.MBR_KRM,
                            MBR_MAJKE = k.MBR_MAJKE,
                            MBR_OCA = k.MBR_OCA,
                            NAPOMENA = k.NAPOMENA,
                            NBBROJ = k.NBBROJ,
                            OPIS = k.OPIS,
                            PARIT = k.PARIT,
                            RASA = k.RASA,
                            RAZL_SK = k.RAZL_SK,
                            RBBROJ = k.RBBROJ,
                            SI1 = k.SI1,
                            SI2 = k.SI2,
                            SI3 = k.SI3,
                            TETOVIRK10 = k.TETOVIRK10,
                            T_KRM = k.T_KRM,
                            T_MAJKE10 = k.T_MAJKE10,
                            T_OCA10 = k.T_OCA10,
                            UK_LAKT = k.UK_LAKT,
                            UK_MRTVO = k.UK_MRTVO,
                            UK_PRAZNI = k.UK_PRAZNI,
                            UK_PROIZ_D = k.UK_PROIZ_D,
                            UK_ZAL = k.UK_ZAL,
                            UK_ZIVO = k.UK_ZIVO
                        }).FirstOrDefault();
            }
        }


        public KrmacaAzuriranjeVM GetById(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => x.Id == id)
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new KrmacaAzuriranjeVM()
                        {
                            Id = k.Id,
                            BR_SISA = k.BR_SISA,
                            CIKLUS = k.PARIT,
                            ODG = k.ODG,
                            VLA = k.VLA,
                            DAT_ROD = k.DAT_ROD,
                            D_SKART = k.D_SKART,
                            EKST = k.EKST,
                            FAZA = k.FAZA,
                            GN = k.GN,
                            GNM = k.GNM,
                            GNO = k.GNO,
                            GRUPA = k.GRUPA,
                            HBBROJ = k.HBBROJ,
                            IEZ = k.IEZ,
                            KLASA = k.KLASA,
                            KL_P = k.KL_P,
                            MARKICA = k.MARKICA,
                            MBR_KRM = k.MBR_KRM,
                            MBR_MAJKE = k.MBR_MAJKE,
                            MBR_OCA = k.MBR_OCA,
                            NAPOMENA = k.NAPOMENA,
                            NBBROJ = k.NBBROJ,
                            OPIS = k.OPIS,
                            PARIT = k.PARIT,
                            RASA = k.RASA,
                            RAZL_SK = k.RAZL_SK,
                            RBBROJ = k.RBBROJ,
                            SI1 = k.SI1,
                            SI2 = k.SI2,
                            SI3 = k.SI3,
                            TETOVIRK10 = k.TETOVIRK10,
                            T_KRM = k.T_KRM,
                            T_MAJKE10 = k.T_MAJKE10,
                            T_OCA10 = k.T_OCA10,
                            UK_LAKT = k.UK_LAKT,
                            UK_MRTVO = k.UK_MRTVO,
                            UK_PRAZNI = k.UK_PRAZNI,
                            UK_PROIZ_D = k.UK_PROIZ_D,
                            UK_ZAL = k.UK_ZAL,
                            UK_ZIVO = k.UK_ZIVO
                        }).FirstOrDefault();
            }
        }

        public ReturnObject Save(KrmacaAzuriranjeVM krmaca)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                if (krmaca.Id == 0)
                {
                    //dodajemo novi
                    if (applicationDbContext.Krmaca.Any(x => x.T_KRM == krmaca.T_KRM))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast sa tim tetovir brojem vec postoji" };
                    }
                    if (string.IsNullOrEmpty(krmaca.RASA))
                    {
                        return new ReturnObject() { Success = false, Message = "Nerast mora imati Rasu" };
                    }
                    var majka = applicationDbContext.Krmaca.FirstOrDefault(x => x.MBR_KRM == krmaca.MBR_MAJKE);
                    var otac = applicationDbContext.Nerast.FirstOrDefault(x => x.MBR_NER == krmaca.MBR_OCA);
                    applicationDbContext.Krmaca.Add(new Krmaca()
                    {
                        T_KRM = krmaca.T_KRM,
                        BR_SISA = krmaca.BR_SISA,
                        DAT_ROD = krmaca.DAT_ROD,
                        EKST = krmaca.EKST,
                        GNM = majka != null ? majka.GN : "",
                        MBR_MAJKE = majka != null ? majka.MBR_KRM : "",
                        T_MAJKE10 = majka != null ? majka.T_KRM : "",
                        GNO = otac != null ? otac.GN : "",
                        MBR_OCA = otac != null ? otac.MBR_NER : "",
                        T_OCA10 = otac != null ? otac.T_NER : "",
                        MBR_KRM = krmaca.VLA + krmaca.ODG + krmaca.RASA + krmaca.T_KRM,
                        HBBROJ = krmaca.HBBROJ,
                        RBBROJ = krmaca.RBBROJ,
                        KLASA = krmaca.KLASA,
                        MARKICA = krmaca.MARKICA,
                        NAPOMENA = krmaca.NAPOMENA,
                        ODG = krmaca.ODG,
                        VLA = krmaca.VLA,
                        RASA = krmaca.RASA,
                        OPIS = krmaca.OPIS,
                        TETOVIRK10 = krmaca.T_KRM,
                        SI1 = krmaca.SI1
                    });
                }
                else
                {
                    //azuriramo postojeci
                    return new ReturnObject() { Success = false, Message = "Trenutno ne podrzavamo azuriranje" };
                }
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }

        public bool EvidencijaPodatakaSaSmotreGrla(string tetovir, string klasa, string klP, string markica, DateTime naFarmiOd, int? ekst, string brSisa, int? si, string napomena, string hbBroj, string rbBroj, string nbBroj, string grupa, string obj, string box, int? faza, int? fazaX, int? predFaza)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == tetovir);
                if (krmaca != null)
                {
                    krmaca.KLASA = klasa;
                    krmaca.KL_P = klP;
                    krmaca.MARKICA = markica;
                    krmaca.NA_FARM_OD = naFarmiOd;
                    krmaca.BR_SISA = brSisa;
                    krmaca.SI1 = si;
                    krmaca.NAPOMENA = napomena;
                    krmaca.HBBROJ = hbBroj;
                    krmaca.RBBROJ = rbBroj;
                    krmaca.NBBROJ = nbBroj;
                    krmaca.GRUPA = grupa;
                    krmaca.FAZA = faza;
                    krmaca.PRED_FAZA = predFaza;
                    krmaca.EKST = ekst;

                    var existingKlasa = applicationDbContext.Klasa.FirstOrDefault(k => k.DAT_SMOTRE.HasValue && DbFunctions.TruncateTime((DateTime)k.DAT_SMOTRE) == DbFunctions.TruncateTime(DateTime.Now));
                    if (existingKlasa != null)
                    {
                        existingKlasa.KLASA = klasa;
                        existingKlasa.DAT_SMOTRE = DateTime.Now;
                        existingKlasa.MBR_GRLA = krmaca.MBR_KRM;
                        existingKlasa.T_GRLA = krmaca.T_KRM;
                        existingKlasa.POL = "Z";
                        existingKlasa.KL_P = klP;
                        existingKlasa.GODINA = DateTime.Now.Year;
                        existingKlasa.EKST = ekst;
                    }
                    else
                    {
                        applicationDbContext.Klasa.Add(new Klasa()
                        {
                            GODINA = DateTime.Now.Year,
                            KL_P = klP,
                            DAT_SMOTRE = DateTime.Now,
                            KLASA = klasa,
                            MBR_GRLA = krmaca.MBR_KRM,
                            T_GRLA = krmaca.T_KRM,
                            POL = "Z",
                            EKST = ekst
                        });
                    }
                    applicationDbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public KrmacaAzuriranjeVM GetByMarkica(string markica)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from k in applicationDbContext.Krmaca.Where(x => x.MARKICA == markica)
                        join s in applicationDbContext.Skart on k.Id equals s.KrmacaId into ss
                        from s in ss.DefaultIfEmpty()
                        select new KrmacaAzuriranjeVM()
                        {
                            Id = k.Id,
                            BR_SISA = k.BR_SISA,
                            CIKLUS = k.PARIT,
                            DAT_ROD = k.DAT_ROD,
                            D_SKART = k.D_SKART,
                            EKST = k.EKST,
                            FAZA = k.FAZA,
                            GN = k.GN,
                            ODG = k.ODG,
                            VLA = k.VLA,
                            GNM = k.GNM,
                            GNO = k.GNO,
                            GRUPA = k.GRUPA,
                            HBBROJ = k.HBBROJ,
                            IEZ = k.IEZ,
                            KLASA = k.KLASA,
                            KL_P = k.KL_P,
                            MARKICA = k.MARKICA,
                            MBR_KRM = k.MBR_KRM,
                            MBR_MAJKE = k.MBR_MAJKE,
                            MBR_OCA = k.MBR_OCA,
                            NAPOMENA = k.NAPOMENA,
                            NBBROJ = k.NBBROJ,
                            OPIS = k.OPIS,
                            PARIT = k.PARIT,
                            RASA = k.RASA,
                            RAZL_SK = k.RAZL_SK,
                            RBBROJ = k.RBBROJ,
                            SI1 = k.SI1,
                            SI2 = k.SI2,
                            SI3 = k.SI3,
                            TETOVIRK10 = k.TETOVIRK10,
                            T_KRM = k.T_KRM,
                            T_MAJKE10 = k.T_MAJKE10,
                            T_OCA10 = k.T_OCA10,
                            UK_LAKT = k.UK_LAKT,
                            UK_MRTVO = k.UK_MRTVO,
                            UK_PRAZNI = k.UK_PRAZNI,
                            UK_PROIZ_D = k.UK_PROIZ_D,
                            UK_ZAL = k.UK_ZAL,
                            UK_ZIVO = k.UK_ZIVO
                        }).FirstOrDefault();
            }
        }

        public List<KrmacineKlaseVM> KrmacineKlaseByMaticniBroj(string maticniBroj)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return applicationDbContext.Klasa.Where(x => x.MBR_GRLA == maticniBroj).Select(x => new KrmacineKlaseVM()
                {
                    God = x.GODINA,
                    Klasa = x.KLASA,
                    Ekst = x.EKST
                }).ToList();
            }
        }

        public List<KrmacaPripustVM> GetPripusti(int krmacaId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Pripust.Where(x => x.KrmacaId == krmacaId)
                        join n in applicationDbContext.Nerast on p.NerastId equals n.Id into ns
                        from n in ns.DefaultIfEmpty()
                        select new KrmacaPripustVM()
                        {
                            Dat_Prip = p.DAT_PRIP,
                            Nerast = n.T_NER,
                            Rasa = n.RASA,
                            Rb = p.RB != null ? p.RB.ToString() : "",
                            Rbp = p.RBP != null ? p.RBP.ToString() : ""
                        }).ToList();
            }
        }

        public List<GetAllForKrmacaVM> SP_GetAllKrmacaData(int krmacaId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var idParametar = new SqlParameter("@Id", krmacaId);

                var krmacaKarton = applicationDbContext.Database.SqlQuery<GetAllForKrmacaVM>("GetAllForKrmaca @Id", idParametar).ToList();
                GetAllForKrmacaVM previous = null;
                for (int i = krmacaKarton.Count - 1; i >= 0; i--)
                {
                    var current = krmacaKarton.ElementAt(i);
                    if (previous != null && previous.Pr == current.Pr)
                    {
                        current.DAT_PRAS = null;
                        current.Zivo = null;
                        current.Zen = null;
                        current.Mrt = null;
                        current.Ukup = null;
                        current.Anomalije = null;
                        current.Dod = null;
                        current.Odu = null;
                        current.Gaji = null;
                        current.Dat_zal = null;
                        current.Zal = null;
                        current.Lakt = null;
                        current.Pr_d = null;
                    }
                    previous = current;
                }
                return krmacaKarton;
            }
        }

        public List<KrmacaPrasenjeVM> GetPrasenje(int krmacaId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from p in applicationDbContext.Prasenje.Where(x => x.KrmacaId == krmacaId)
                        select new KrmacaPrasenjeVM()
                        {
                            Anomalije = p.ANOMALIJE,
                            Dat_pras = p.DAT_PRAS,
                            Dodato = p.DODATO,
                            Gaji = p.GAJI,
                            Mrtvo = p.MRTVO,
                            Oduzeto = p.ODUZETO,
                            Pr = p.PARIT,
                            Zivo = p.ZIVO,
                            Ukupno = p.ZIVO != null && p.MRTVO != null ? (int)p.ZIVO + (int)p.MRTVO : p.ZIVO != null && p.MRTVO == null ? (int)p.ZIVO : p.ZIVO == null && p.MRTVO != null ? (int)p.MRTVO : 0,
                            Zensko = p.ZIVOZ
                        }).ToList();
            }
        }
        public List<KrmacaZalucenjeVM> GetZalucenje(int krmacaId)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from z in applicationDbContext.Zaluc.Where(x => x.KrmacaId == krmacaId)
                        join p in applicationDbContext.Prasenje.Where(x => x.KrmacaId == krmacaId) on z.PARIT equals p.PARIT into ps
                        from p in ps.DefaultIfEmpty()
                        join sledeciP in applicationDbContext.Pripust.Where(x => x.KrmacaId == krmacaId) on z.PARIT + 1 equals sledeciP.CIKLUS into sledeciPs
                        from sledeciP in sledeciPs.DefaultIfEmpty()
                        select new KrmacaZalucenjeVM()
                        {
                            Dat_zal = z.DAT_ZAL,
                            Zal = z.ZALUC,
                            Lakt = p != null && p.DAT_PRAS != null ? DbFunctions.DiffDays(p.DAT_PRAS, z.DAT_ZAL) : 0,
                            Prazni_dani = sledeciP != null && sledeciP.DAT_PRIP != null ? DbFunctions.DiffDays(z.DAT_ZAL, sledeciP.DAT_PRIP) : DbFunctions.DiffDays(z.DAT_ZAL, DateTime.Now),
                        }).ToList();
            }
        }

        public decimal GetAvgZivo(List<KrmacaPrasenjeVM> krmacaPrasenjes)
        {
            var zivo = krmacaPrasenjes.Where(x => x.Zivo != null).Select(x => x.Zivo).ToList();
            var ukupnoZivo = 0;
            if (zivo.Count > 0)
            {
                foreach (var item in zivo)
                {
                    ukupnoZivo += (int)item;
                }
                return (decimal)ukupnoZivo / (decimal)zivo.Count;
            }
            return 0.0M;
        }

        public decimal GetAvgMrtvo(List<KrmacaPrasenjeVM> krmacaPrasenjes)
        {
            var result = krmacaPrasenjes.Where(x => x.Mrtvo != null).Select(x => x.Mrtvo).ToList();
            var ukupno = 0;
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    ukupno += (int)item;
                }
                return (decimal)ukupno / (decimal)result.Count;
            }
            return 0.0M;
        }

        public decimal GetAvgZaluceno(List<KrmacaZalucenjeVM> krmacaZalucenja)
        {
            var result = krmacaZalucenja.Where(x => x.Zal != null).Select(x => x.Zal).ToList();
            var ukupno = 0;
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    ukupno += (int)item;
                }
                return (decimal)ukupno / (decimal)result.Count;
            }
            return 0.0M;
        }

        public decimal GetAvgLakt(List<KrmacaZalucenjeVM> krmacaZalucenja)
        {
            var result = krmacaZalucenja.Where(x => x.Lakt != null).Select(x => x.Lakt).ToList();
            var ukupno = 0;
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    ukupno += (int)item;
                }
                return (decimal)ukupno / (decimal)result.Count;
            }
            return 0.0M;
        }

        public decimal GetAvgPrazniDani(List<KrmacaZalucenjeVM> krmacaZalucenja)
        {
            var result = krmacaZalucenja.Where(x => x.Prazni_dani != null).Select(x => x.Prazni_dani).ToList();
            var ukupno = 0;
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    ukupno += (int)item;
                }
                return (decimal)ukupno / (decimal)result.Count;
            }
            return 0.0M;
        }

        public decimal GetAvgM_I(List<KrmacaZalucenjeVM> krmacaZalucenja)
        {
            var result = krmacaZalucenja.Where(x => x.M_I != null).Select(x => x.M_I).ToList();
            var ukupno = 0;
            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    ukupno += (int)item;
                }
                return (decimal)ukupno / (decimal)result.Count;
            }
            return 0.0M;
        }

        public PedigreExtendedVM GetPedigreByMaticniBroj(string maticniBroj)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var pedigre = applicationDbContext.Pedigre.FirstOrDefault(x => x.MBR_GRLA == maticniBroj);
                if (pedigre == null)
                {
                    return null;
                }
                var pedigreExtended = new PedigreExtendedVM()
                {
                    POL = pedigre.POL,
                    HRMB_PRAB = pedigre.HRMB_PRAB,
                    HRMB_PRAD = pedigre.HRMB_PRAD,
                    HRMD_PRAB = pedigre.HRMD_PRAB,
                    HRMD_PRAD = pedigre.HRMD_PRAD,
                    HRM_BABE = pedigre.HRM_BABE,
                    HRM_DEDE = pedigre.HRM_DEDE,
                    HROB_PRAB = pedigre.HROB_PRAB,
                    HROB_PRAD = pedigre.HROB_PRAD,
                    HROD_PRAB = pedigre.HROD_PRAB,
                    HROD_PRAD = pedigre.HROD_PRAD,
                    HRO_BABE = pedigre.HRO_BABE,
                    HRO_DEDE = pedigre.HRO_DEDE,
                    HR_GRLA = pedigre.HR_GRLA,
                    HR_MAJKE = pedigre.HR_MAJKE,
                    HR_OCA = pedigre.HR_OCA,
                    MBMB_PRAB = pedigre.MBMB_PRAB,
                    MBMB_PRAD = pedigre.MBMB_PRAD,
                    MBMD_PRAB = pedigre.MBMD_PRAB,
                    MBMD_PRAD = pedigre.MBMD_PRAD,
                    MBM_BABE = pedigre.MBM_BABE,
                    MBM_DEDE = pedigre.MBM_DEDE,
                    MBOB_PRAB = pedigre.MBOB_PRAB,
                    MBOB_PRAD = pedigre.MBOB_PRAD,
                    MBOD_PRAB = pedigre.MBOD_PRAB,
                    MBOD_PRAD = pedigre.MBOD_PRAD,
                    MBO_BABE = pedigre.MBO_BABE,
                    MBO_DEDE = pedigre.MBO_DEDE,
                    MBR_GRLA = pedigre.MBR_GRLA,
                    MB_MAJKE = pedigre.MB_MAJKE,
                    MB_OCA = pedigre.MB_OCA,
                    T_GRLA = pedigre.T_GRLA
                };
                return pedigreExtended;
            }
        }

        public PedigreExtendedVM GetPedigreByTetovir(string t_GRLA)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var pedigre = applicationDbContext.Pedigre.FirstOrDefault(x => x.T_GRLA == t_GRLA);
                if (pedigre == null)
                {
                    return null;
                }
                var pedigreExtended = new PedigreExtendedVM()
                {
                    POL = pedigre.POL,
                    HRMB_PRAB = pedigre.HRMB_PRAB,
                    HRMB_PRAD = pedigre.HRMB_PRAD,
                    HRMD_PRAB = pedigre.HRMD_PRAB,
                    HRMD_PRAD = pedigre.HRMD_PRAD,
                    HRM_BABE = pedigre.HRM_BABE,
                    HRM_DEDE = pedigre.HRM_DEDE,
                    HROB_PRAB = pedigre.HROB_PRAB,
                    HROB_PRAD = pedigre.HROB_PRAD,
                    HROD_PRAB = pedigre.HROD_PRAB,
                    HROD_PRAD = pedigre.HROD_PRAD,
                    HRO_BABE = pedigre.HRO_BABE,
                    HRO_DEDE = pedigre.HRO_DEDE,
                    HR_GRLA = pedigre.HR_GRLA,
                    HR_MAJKE = pedigre.HR_MAJKE,
                    HR_OCA = pedigre.HR_OCA,
                    MBMB_PRAB = pedigre.MBMB_PRAB,
                    MBMB_PRAD = pedigre.MBMB_PRAD,
                    MBMD_PRAB = pedigre.MBMD_PRAB,
                    MBMD_PRAD = pedigre.MBMD_PRAD,
                    MBM_BABE = pedigre.MBM_BABE,
                    MBM_DEDE = pedigre.MBM_DEDE,
                    MBOB_PRAB = pedigre.MBOB_PRAB,
                    MBOB_PRAD = pedigre.MBOB_PRAD,
                    MBOD_PRAB = pedigre.MBOD_PRAB,
                    MBOD_PRAD = pedigre.MBOD_PRAD,
                    MBO_BABE = pedigre.MBO_BABE,
                    MBO_DEDE = pedigre.MBO_DEDE,
                    MBR_GRLA = pedigre.MBR_GRLA,
                    MB_MAJKE = pedigre.MB_MAJKE,
                    MB_OCA = pedigre.MB_OCA,
                    T_GRLA = pedigre.T_GRLA
                };
                return pedigreExtended;
            }
        }


        public PorekloExtendedVM GetPorekloByTetovir(string t_GRLA)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var poreklo = applicationDbContext.Poreklo.FirstOrDefault(x => x.T_GRLA == t_GRLA);
                if (poreklo == null)
                {
                    return null;
                }
                var porekloExtended = new PorekloExtendedVM()
                {
                    POL = poreklo.POL,
                    HRMB_PRAB = poreklo.HRMB_PRAB,
                    HRMB_PRAD = poreklo.HRMB_PRAD,
                    HRMD_PRAB = poreklo.HRMD_PRAB,
                    HRMD_PRAD = poreklo.HRMD_PRAD,
                    HRM_BABE = poreklo.HRM_BABE,
                    HRM_DEDE = poreklo.HRM_DEDE,
                    HROB_PRAB = poreklo.HROB_PRAB,
                    HROB_PRAD = poreklo.HROB_PRAD,
                    HROD_PRAB = poreklo.HROD_PRAB,
                    HROD_PRAD = poreklo.HROD_PRAD,
                    HRO_BABE = poreklo.HRO_BABE,
                    HRO_DEDE = poreklo.HRO_DEDE,
                    HR_GRLA = poreklo.HR_GRLA,
                    HR_MAJKE = poreklo.HR_MAJKE,
                    HR_OCA = poreklo.HR_OCA,
                    MBMB_PRAB = poreklo.MBMB_PRAB,
                    MBMB_PRAD = poreklo.MBMB_PRAD,
                    MBMD_PRAB = poreklo.MBMD_PRAB,
                    MBMD_PRAD = poreklo.MBMD_PRAD,
                    MBM_BABE = poreklo.MBM_BABE,
                    MBM_DEDE = poreklo.MBM_DEDE,
                    MBOB_PRAB = poreklo.MBOB_PRAB,
                    MBOB_PRAD = poreklo.MBOB_PRAD,
                    MBOD_PRAB = poreklo.MBOD_PRAB,
                    MBOD_PRAD = poreklo.MBOD_PRAD,
                    MBO_BABE = poreklo.MBO_BABE,
                    MBO_DEDE = poreklo.MBO_DEDE,
                    MBR_GRLA = poreklo.MBR_GRLA,
                    MB_MAJKE = poreklo.MB_MAJKE,
                    MB_OCA = poreklo.MB_OCA,
                    T_GRLA = poreklo.T_GRLA,
                    GN = poreklo.GN,
                    MB_GRLA = poreklo.MB_GRLA
                };
                return porekloExtended;
            }
        }
    }
}
