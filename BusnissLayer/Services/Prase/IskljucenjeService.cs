﻿using BusinessLayer.ViewModels;
using BusnissLayer.ViewModels.Prase._04Iskljucenje;
using DataAccessLayer;
using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.Services.Prase
{
    public class IskljucenjeService
    {
        #region Evidencija
        public ReturnObject Save(EvidencijaIskljucenjaAzuriranjeVM iskljucenje)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {

                if (iskljucenje.Id == 0)
                {
                    var skart = new Skart()
                    {
                        D_SKART = iskljucenje.D_SKART,
                        RAZL_SK = iskljucenje.RAZL_SK,
                        RB = iskljucenje.RB,
                        REFK = iskljucenje.REFK
                    };

                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == iskljucenje.T_GRLA);
                    if (krmaca != null)
                        skart.KrmacaId = krmaca.Id;

                    var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == iskljucenje.T_GRLA);
                    if (nerast != null)
                        skart.NerastId = nerast.Id;

                    var test = applicationDbContext.Test.FirstOrDefault(x => x.T_TEST == iskljucenje.T_GRLA);
                    if (test != null)
                        skart.TestId = test.Id;

                    var odabir = applicationDbContext.Odabir.FirstOrDefault(x => x.T_TEST == iskljucenje.T_GRLA);
                    if (odabir != null)
                        skart.OdabirId = odabir.Id;

                    var prase = applicationDbContext.Prase.FirstOrDefault(x => x.T_PRAS == iskljucenje.T_GRLA);
                    if (prase != null)
                        skart.PraseId = prase.Id;

                    var entity = applicationDbContext.Skart.Add(skart);
                    applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = true, Id = entity.Id };
                }
                else
                {
                    var entity = applicationDbContext.Skart.FirstOrDefault(x => x.Id == iskljucenje.Id);
                    if (entity == null)
                        return new ReturnObject() { Success = false, Message = "Iskljucenje nije pronadjeno" };

                    entity.D_SKART = iskljucenje.D_SKART;
                    entity.RAZL_SK = iskljucenje.RAZL_SK;
                    entity.RB = iskljucenje.RB;
                    entity.REFK = iskljucenje.REFK;

                    var krmaca = applicationDbContext.Krmaca.FirstOrDefault(x => x.T_KRM == iskljucenje.T_GRLA);
                    if (krmaca != null)
                        entity.KrmacaId = krmaca.Id;

                    var nerast = applicationDbContext.Nerast.FirstOrDefault(x => x.T_NER == iskljucenje.T_GRLA);
                    if (nerast != null)
                        entity.NerastId = nerast.Id;

                    var test = applicationDbContext.Test.FirstOrDefault(x => x.T_TEST == iskljucenje.T_GRLA);
                    if (test != null)
                        entity.TestId = test.Id;

                    var odabir = applicationDbContext.Odabir.FirstOrDefault(x => x.T_TEST == iskljucenje.T_GRLA);
                    if (odabir != null)
                        entity.OdabirId = odabir.Id;

                    var prase = applicationDbContext.Prase.FirstOrDefault(x => x.T_PRAS == iskljucenje.T_GRLA);
                    if (prase != null)
                        entity.PraseId = prase.Id;
                    applicationDbContext.SaveChanges();
                    return new ReturnObject() { Success = true, Id = entity.Id };
                }
            }
        }


        public void SP_Delete(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Iskljucenje.Delete", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public DataTable SP_GetAllByDate(DateTime datumIskljucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var danParametar = new SqlParameter("@dan", new DateTime(datumIskljucenja.Year, datumIskljucenja.Month, datumIskljucenja.Day, 0, 0, 0));
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("Iskljucenje.GetAllByDate", con))
                    {
                        cmd.Parameters.Add(danParametar);
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(sda);

                            //// Populate a new data table and bind it to the BindingSource.
                            DataTable table = new DataTable();
                            table.Locale = System.Globalization.CultureInfo.InvariantCulture;
                            sda.Fill(table);
                            return table;
                        }
                    }
                }
            }
        }

        public void SP_Save(List<SqlParameter> sqlParameters)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                using (SqlConnection con = new SqlConnection(applicationDbContext.Database.Connection.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Iskljucenje.Save", con))
                    {
                        cmd.Parameters.AddRange(sqlParameters.ToArray());
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public List<EvidencijaIskljucenjaPregledVM> EvidencijaIskljucenjaPregledVM(DateTime datumIskljucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from s in applicationDbContext.Skart.Where(x => DbFunctions.TruncateTime(x.D_SKART) == datumIskljucenja.Date)
                        join k in applicationDbContext.Krmaca on s.KrmacaId equals k.Id into ks
                        from k in ks.DefaultIfEmpty()
                        join n in applicationDbContext.Nerast on s.NerastId equals n.Id into ns
                        from n in ns.DefaultIfEmpty()
                        join p in applicationDbContext.Prase on s.PraseId equals p.Id into ps
                        from p in ps.DefaultIfEmpty()
                        join o in applicationDbContext.Odabir on s.OdabirId equals o.Id into os
                        from o in os.DefaultIfEmpty()
                        join t in applicationDbContext.Test on s.TestId equals t.Id into ts
                        from t in ts.DefaultIfEmpty()
                        select new EvidencijaIskljucenjaPregledVM()
                        {
                            MBR_GRLA = n != null ? n.MBR_NER : t != null ? t.MBR_TEST : o != null ? o.MBR_TEST : p != null ? p.MBR_PRAS : k.MBR_KRM,
                            POL = n != null ? "M" : t != null ? t.POL : o != null ? o.POL : p != null ? p.POL : "Z",
                            RAZL_SK = s.RAZL_SK,
                            RB = s.RB,
                            REFK = s.REFK,
                            T_GRLA = n != null ? n.T_NER : t != null ? t.T_TEST : o != null ? o.T_TEST : p != null ? p.T_PRAS : k.T_KRM
                        }).ToList();
            }
        }
        public List<EvidencijaIskljucenjaAzuriranjeVM> GetEvidencijaIskljucenjaAzuriranjeVM(DateTime datumIskljucenja)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from s in applicationDbContext.Skart.Where(x => DbFunctions.TruncateTime(x.D_SKART) == datumIskljucenja.Date)
                        join k in applicationDbContext.Krmaca on s.KrmacaId equals k.Id into ks
                        from k in ks.DefaultIfEmpty()
                        join n in applicationDbContext.Nerast on s.NerastId equals n.Id into ns
                        from n in ns.DefaultIfEmpty()
                        join p in applicationDbContext.Prase on s.PraseId equals p.Id into ps
                        from p in ps.DefaultIfEmpty()
                        join o in applicationDbContext.Odabir on s.OdabirId equals o.Id into os
                        from o in os.DefaultIfEmpty()
                        join t in applicationDbContext.Test on s.TestId equals t.Id into ts
                        from t in ts.DefaultIfEmpty()
                        select new EvidencijaIskljucenjaAzuriranjeVM()
                        {
                            Id = s.Id,
                            D_SKART = s.D_SKART,
                            MBR_GRLA = n != null ? n.MBR_NER : t != null ? t.MBR_TEST : o != null ? o.MBR_TEST : p != null ? p.MBR_PRAS : k.MBR_KRM,
                            POL = n != null ? "M" : t != null ? t.POL : o != null ? o.POL : p != null ? p.POL : "Z",
                            RAZL_SK = s.RAZL_SK,
                            RB = s.RB,
                            REFK = s.REFK,
                            T_GRLA = n != null ? n.T_NER : t != null ? t.T_TEST : o != null ? o.T_TEST : p != null ? p.T_PRAS : k.T_KRM
                        }).ToList();
            }
        }

        public ReturnObject Delete(int id)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var entity = applicationDbContext.Skart.FirstOrDefault(x => x.Id == id);
                if (entity == null)
                {
                    return new ReturnObject() { Success = false, Message = "Iskljucenje nije pronadjeno" };
                }
                applicationDbContext.Skart.Remove(entity);
                applicationDbContext.SaveChanges();
                return new ReturnObject() { Success = true };
            }
        }
        #endregion

        #region PregledPodataka
        public List<PregledPodatakaIskljucenjeVM> PregledPodatakaIskljucenje(DateTime dateFrom, DateTime dateTo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from s in applicationDbContext.Skart.Where(x => DbFunctions.TruncateTime(x.D_SKART) >= dateFrom.Date && DbFunctions.TruncateTime(x.D_SKART) <= dateTo.Date)
                        join k in applicationDbContext.Krmaca on s.KrmacaId equals k.Id into ks
                        from k in ks.DefaultIfEmpty()
                        join n in applicationDbContext.Nerast on s.NerastId equals n.Id into ns
                        from n in ns.DefaultIfEmpty()
                        join p in applicationDbContext.Prase on s.PraseId equals p.Id into ps
                        from p in ps.DefaultIfEmpty()
                        join o in applicationDbContext.Odabir on s.OdabirId equals o.Id into os
                        from o in os.DefaultIfEmpty()
                        join t in applicationDbContext.Test on s.TestId equals t.Id into ts
                        from t in ts.DefaultIfEmpty()
                        select new PregledPodatakaIskljucenjeVM()
                        {
                            Ciklus = k.CIKLUS,
                            Faza = k.FAZA,
                            Grlo = n != null ? n.MBR_NER : t != null ? t.MBR_TEST : o != null ? o.MBR_TEST : p != null ? p.MBR_PRAS : k.MBR_KRM,
                            Iskljuceno = s.D_SKART,
                            Pol = n != null ? "M" : t != null ? t.POL : o != null ? o.POL : p != null ? p.POL : "Z",
                            Razlog = s.RAZL_SK,
                            Rodjeno = n != null ? n.DAT_ROD : t != null ? t.DAT_ROD : o != null ? o.DAT_ROD : p != null ? p.DAT_ROD : k.DAT_ROD
                        }).ToList();
            }
        }


        public List<PregledPodatakaIskljucenjeVM> AnalizaIskljucenjaGrlaIzTesta(DateTime dateFrom, DateTime dateTo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from s in applicationDbContext.Skart.Where(x => DbFunctions.TruncateTime(x.D_SKART) >= dateFrom.Date && DbFunctions.TruncateTime(x.D_SKART) <= dateTo.Date)
                        join o in applicationDbContext.Odabir on s.OdabirId equals o.Id into os
                        from o in os.DefaultIfEmpty()
                        join t in applicationDbContext.Test on s.TestId equals t.Id into ts
                        from t in ts.DefaultIfEmpty()
                        select new PregledPodatakaIskljucenjeVM()
                        {
                            Ciklus = null,
                            Faza = null,
                            Grlo = t != null ? t.MBR_TEST :o.MBR_TEST,
                            Iskljuceno = s.D_SKART,
                            Pol = t != null ? t.POL : o.POL ,
                            Razlog = s.RAZL_SK,
                            Rodjeno = t != null ? t.DAT_ROD : o.DAT_ROD
                        }).ToList();
            }
        }


        public List<PregledPodatakaIskljucenjeVM> AnalizaIskljucenjaGrlaIzRegistraPrasadi(DateTime dateFrom, DateTime dateTo)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                return (from s in applicationDbContext.Skart.Where(x => DbFunctions.TruncateTime(x.D_SKART) >= dateFrom.Date && DbFunctions.TruncateTime(x.D_SKART) <= dateTo.Date)
                        join p in applicationDbContext.Prase on s.PraseId equals p.Id
                        select new PregledPodatakaIskljucenjeVM()
                        {
                            Ciklus = null,
                            Faza = null,
                            Grlo = p.MBR_PRAS,
                            Iskljuceno = s.D_SKART,
                            Pol = p.POL,
                            Razlog = s.RAZL_SK,
                            Rodjeno = p.DAT_ROD
                        }).ToList();
            }
        }


        public List<UkupnaIskljucenjaKrmacaVM> UkupnaIskljucenjaKrmaca(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);

                var res =  applicationDbContext.Database.SqlQuery<UkupnaIskljucenjaKrmacaVM>("UkupnaIskljucenjaKrmaca @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
                if (res.Count > 0)
                {
                    res.Add(new UkupnaIskljucenjaKrmacaVM()
                    {
                        Razlog = "UKUPNO",
                        Pr_01 = res.Select(x => x.Pr_01).Sum(),
                        Pr_02 = res.Select(x => x.Pr_02).Sum(),
                        Pr_03 = res.Select(x => x.Pr_03).Sum(),
                        Pr_04 = res.Select(x => x.Pr_04).Sum(),
                        Pr_05 = res.Select(x => x.Pr_05).Sum(),
                        Pr_06 = res.Select(x => x.Pr_06).Sum(),
                        Pr_07 = res.Select(x => x.Pr_07).Sum(),
                        Pr_08 = res.Select(x => x.Pr_08).Sum(),
                        Pr_09 = res.Select(x => x.Pr_09).Sum(),
                        Pr_10 = res.Select(x => x.Pr_10).Sum(),
                        Pr_11 = res.Select(x => x.Pr_11).Sum(),
                        Ukupno = res.Select(x => x.Ukupno).Sum(),
                    });
                }
                return res;
            }
        }
        public List<UkupnaIskljucenjaKrmacaVM> UkupnaIskljucenjaKrmacaPoGrupamaRazloga(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);

                var res = applicationDbContext.Database.SqlQuery<UkupnaIskljucenjaKrmacaVM>("UkupnaIskljucenjaKrmacaPoGrupamaRazloga @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
                if (res.Count > 0)
                {
                    res.Add(new UkupnaIskljucenjaKrmacaVM()
                    {
                        Razlog = "UKUPNO",
                        Pr_01 = res.Select(x => x.Pr_01).Sum(),
                        Pr_02 = res.Select(x => x.Pr_02).Sum(),
                        Pr_03 = res.Select(x => x.Pr_03).Sum(),
                        Pr_04 = res.Select(x => x.Pr_04).Sum(),
                        Pr_05 = res.Select(x => x.Pr_05).Sum(),
                        Pr_06 = res.Select(x => x.Pr_06).Sum(),
                        Pr_07 = res.Select(x => x.Pr_07).Sum(),
                        Pr_08 = res.Select(x => x.Pr_08).Sum(),
                        Pr_09 = res.Select(x => x.Pr_09).Sum(),
                        Pr_10 = res.Select(x => x.Pr_10).Sum(),
                        Pr_11 = res.Select(x => x.Pr_11).Sum(),
                        Ukupno = res.Select(x => x.Ukupno).Sum(),
                    });
                }
                return res;
            }
        }
        public List<UkupnaIskljucenjaNazimicaVM> UkupnaIskljucenjaNazimica(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);

                var res = applicationDbContext.Database.SqlQuery<UkupnaIskljucenjaNazimicaVM>("UkupnaIskljucenjaNazimica @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
                return res;
            }
        }

        public List<UkupnaIskljucenjaNazimicaVM> UkupnaIskljucenjaNerastova(DateTime fromDate, DateTime toDate)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);

                var res = applicationDbContext.Database.SqlQuery<UkupnaIskljucenjaNazimicaVM>("UkupnaIskljucenjaNerastova @dateTimeFrom, @dateTimeTo", fromParametar, toParametar).ToList();
                return res;
            }
        }
        #endregion

        #region 03AnalizaIskljucenjaKrmaca

        public List<UkupnaIskljucenjaKrmacaVM> UkupnaIskljucenjaKrmacaZaOdredjenuGrupu(DateTime fromDate, DateTime toDate, string grupa)
        {
            using (ApplicationDbContext applicationDbContext = new ApplicationDbContext())
            {
                var fromParametar = new SqlParameter("@dateTimeFrom", fromDate);
                var toParametar = new SqlParameter("@dateTimeTo", toDate);
                var grupaParametar = new SqlParameter("@group", grupa);

                var res = applicationDbContext.Database
                    .SqlQuery<UkupnaIskljucenjaKrmacaVM>("UkupnaIskljucenjaKrmacaZaOdredjenuGrupu @dateTimeFrom, @dateTimeTo, @group", fromParametar, toParametar, grupaParametar).ToList();

                if (res.Count > 0)
                {
                    res.Add(new UkupnaIskljucenjaKrmacaVM()
                    {
                         Razlog = "UKUPNO",
                          Pr_01 = res.Select(x => x.Pr_01).Sum(),
                          Pr_02 = res.Select(x => x.Pr_02).Sum(),
                          Pr_03 = res.Select(x => x.Pr_03).Sum(),
                          Pr_04 = res.Select(x => x.Pr_04).Sum(),
                          Pr_05 = res.Select(x => x.Pr_05).Sum(),
                          Pr_06 = res.Select(x => x.Pr_06).Sum(),
                          Pr_07 = res.Select(x => x.Pr_07).Sum(),
                          Pr_08 = res.Select(x => x.Pr_08).Sum(),
                          Pr_09 = res.Select(x => x.Pr_09).Sum(),
                          Pr_10 = res.Select(x => x.Pr_10).Sum(),
                          Pr_11 = res.Select(x => x.Pr_11).Sum(),
                          Ukupno = res.Select(x => x.Ukupno).Sum(),
                    });
                }
                return res;
            }
        }
        #endregion
    }
}
