﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels
{
    public class ReturnObject
    {
        public int Id { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
