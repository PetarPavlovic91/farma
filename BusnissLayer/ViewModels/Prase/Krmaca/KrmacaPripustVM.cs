﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class KrmacaPripustVM
    {
        public string Rb { get; set; }
        public DateTime? Dat_Prip { get; set; }
        public string Rbp { get; set; }
        public string Nerast { get; set; }
        public string Rasa { get; set; }

    }
}
