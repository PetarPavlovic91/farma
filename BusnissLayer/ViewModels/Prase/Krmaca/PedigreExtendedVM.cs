﻿using DataAccessLayer.Entities.PIGS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class PedigreExtendedVM
    {
        public string T_GRLA { get; set; }
        public string MBR_GRLA { get; set; }
        public string POL { get; set; }
        public string MB_OCA { get; set; }
        public string MB_MAJKE { get; set; }
        public string MBO_DEDE { get; set; }
        public string MBO_BABE { get; set; }
        public string MBM_DEDE { get; set; }
        public string MBM_BABE { get; set; }
        public string MBOD_PRAD { get; set; }
        public string MBOD_PRAB { get; set; }
        public string MBOB_PRAD { get; set; }
        public string MBOB_PRAB { get; set; }
        public string MBMD_PRAD { get; set; }
        public string MBMD_PRAB { get; set; }
        public string MBMB_PRAD { get; set; }
        public string MBMB_PRAB { get; set; }
        public string HR_GRLA { get; set; }
        public string HR_OCA { get; set; }
        public string HR_MAJKE { get; set; }
        public string HRO_DEDE { get; set; }
        public string HRO_BABE { get; set; }
        public string HRM_DEDE { get; set; }
        public string HRM_BABE { get; set; }
        public string HROD_PRAD { get; set; }
        public string HROD_PRAB { get; set; }
        public string HROB_PRAD { get; set; }
        public string HROB_PRAB { get; set; }
        public string HRMD_PRAD { get; set; }
        public string HRMD_PRAB { get; set; }
        public string HRMB_PRAD { get; set; }
        public string HRMB_PRAB { get; set; }
    }
}
