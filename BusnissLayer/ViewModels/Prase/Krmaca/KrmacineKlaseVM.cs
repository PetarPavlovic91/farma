﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class KrmacineKlaseVM
    {
        public int? God { get; set; }
        public string Klasa { get; set; }
        public int? Ekst { get; set; }
    }
}
