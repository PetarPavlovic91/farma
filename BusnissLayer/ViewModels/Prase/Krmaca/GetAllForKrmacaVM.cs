﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class GetAllForKrmacaVM
    {
        public long RB { get; set; }
        public DateTime Dat_prip { get; set; }
        public int? Rbp { get; set; }
        public string Nerast { get; set; }
        public string Rasa { get; set; }
        public int? Pr { get; set; }
        public DateTime? DAT_PRAS { get; set; }
        public int? Zivo { get; set; }
        public int? Zen { get; set; }
        public int? Mrt { get; set; }
        public int? Ukup { get; set; }
        public string Anomalije { get; set; }
        public int? Dod { get; set; }
        public int? Odu { get; set; }
        public int? Gaji { get; set; }
        public DateTime? Dat_zal { get; set; }
        public int? Zal { get; set; }
        public int? Lakt { get; set; }
        public int? Pr_d { get; set; }
    }
}
