﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class KrmacaAzuriranjeVM
    {
        public int Id { get; set; }
        public string MBR_KRM { get; set; }
        public string RASA { get; set; }
        public string VLA { get; set; }
        public string ODG { get; set; }
        public string T_KRM { get; set; }
        public string GN { get; set; }
        public int? PARIT { get; set; }
        public int? CIKLUS { get; set; }
        public int? FAZA { get; set; }
        public DateTime? DAT_ROD { get; set; }
        public string MBR_OCA { get; set; }
        public string MBR_MAJKE { get; set; }
        public string KLASA { get; set; }
        public int? EKST { get; set; }
        public string BR_SISA { get; set; }
        public int? SI1 { get; set; }
        public int? SI2 { get; set; }
        public int? SI3 { get; set; }
        public int? IEZ { get; set; }
        public int? UK_ZIVO { get; set; }
        public int? UK_MRTVO { get; set; }
        public int? UK_ZIVO_Z { get; set; }
        public int? UK_ZAL { get; set; }
        public int? UK_LAKT { get; set; }
        public int? UK_PRAZNI { get; set; }
        public int? UK_PROIZ_D { get; set; }
        public DateTime? D_SKART { get; set; }
        public int? RAZL_SK { get; set; }
        public string HBBROJ { get; set; }
        public string RBBROJ { get; set; }
        public string NBBROJ { get; set; }
        public string GRUPA { get; set; }
        public string MARKICA { get; set; }
        public string NAPOMENA { get; set; }
        public string KL_P { get; set; }
        public string TETOVIRK10 { get; set; }
        public string GNO { get; set; }
        public string T_OCA10 { get; set; }
        public string GNM { get; set; }
        public string T_MAJKE10 { get; set; }
        public string OPIS { get; set; }
        public int? PREDFAZA { get; set; }
    }
}
