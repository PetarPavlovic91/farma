﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class MaticniBrojKrmaceReportVM
    {
        public int RB { get; set; }
        public int Pr { get; set; }
        public DateTime Dat_prip1 { get; set; }
        public DateTime Dat_prip2 { get; set; }
        public DateTime Dat_prip3 { get; set; }
        public string Nerast { get; set; }
        public string Nerast2 { get; set; }
        public string Nerast3 { get; set; }
        public DateTime DAT_PRAS { get; set; }
        public string Musko { get; set; }
        public string Zensko { get; set; }
        public int MRTVO { get; set; }
        public int Mumificirano { get; set; }
        public int Ukup { get; set; }
        public double TEZ_LEG { get; set; }
        public string ANOMALIJE { get; set; }
        public DateTime DAT_ZAL { get; set; }
        public int ZALUC { get; set; }
        public string SaDana { get; set; }
        public double TEZ_ZAL { get; set; }
        public string OcenaZalucenja { get; set; }
        public string OcenaKrmace { get; set; }
        public string MuskihOdabrano { get; set; }
        public string ZenskihOdabrano { get; set; }
    }
}
