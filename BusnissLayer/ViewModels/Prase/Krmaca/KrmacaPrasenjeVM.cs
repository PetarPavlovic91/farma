﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class KrmacaPrasenjeVM
    {
        public int Pr { get; set; }
        public DateTime? Dat_pras { get; set; }
        public int? Zivo { get; set; }
        public int? Zensko { get; set; }
        public int? Mrtvo { get; set; }
        public int Ukupno { get; set; }
        public string Anomalije { get; set; }
        public string Tet { get; set; }
        public int? Dodato { get; set; }
        public int? Oduzeto { get; set; }
        public int? Gaji { get; set; }
    }
}
