﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Krmaca
{
    public class KrmacaZalucenjeVM
    {
        public DateTime? Dat_zal { get; set; }
        public string Z { get; set; }
        public string D { get; set; }
        public int? Zal { get; set; }
        public int? Lakt { get; set; }
        public int? M_I { get; set; }
        public int? Prazni_dani { get; set; }
        public int? B { get; set; }
        public int? P { get; set; }
    }
}
