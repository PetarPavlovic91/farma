﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._09RezimeSmotre
{
    public class KomisijskiSpisakZaNazimiceReportVM
    {
        public long Red { get; set; }
        public string Nazimica { get; set; }
        public string Odg { get; set; }
        public string Vla { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public DateTime Dat_rod { get; set; }
        public string Br_sisa { get; set; }
        public int? Ekst { get; set; }
        public string Klasa { get; set; }
        public int? Si1 { get; set; }
        public string Gn { get; set; }
        public string Mbr_krm { get; set; }
        public string Gno { get; set; }
        public string Mbr_oca { get; set; }
        public string Gnm { get; set; }
        public string Mbr_majke { get; set; }
        public DateTime Dat_prip { get; set; }
        public string Nerast { get; set; }
        public string Gnn    { get; set; }
        public string Mbr_ner    { get; set; }
        public string Otac { get; set; }
        public string Hr_o   { get; set; }
        public int? Si_o { get; set; }
        public string Kl_o { get; set; }
        public string Majka { get; set; }
        public string Hr_m { get; set; }
        public int? Si_m { get; set; }
        public string Kl_m { get; set; }
        public string Napomena { get; set; }
        public DateTime Dat_smotre { get; set; }
    }
}
