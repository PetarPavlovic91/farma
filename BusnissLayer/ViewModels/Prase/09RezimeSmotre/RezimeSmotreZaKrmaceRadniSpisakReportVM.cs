﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._09RezimeSmotre
{
    public class RezimeSmotreZaKrmaceRadniSpisakReportVM
    {
        public long Red { get; set; }
        public int Id  { get; set; }
        public string Krmaca { get; set; }
        public string Klasa { get; set; }
        public int? EKST { get; set; }
        public string VLA { get; set; }
        public string ODG { get; set; }
        public string MBR_KRM { get; set; }
        public int? PARIT { get; set; }
        public string GN { get; set; }
        public string RASA { get; set; }
        public string MARKICA { get; set; }
        public string HBBROJ { get; set; }
        public DateTime DAT_ROD { get; set; }
        public string GNO { get; set; }
        public string MBR_OCA { get; set; }
        public string GNM { get; set; }
        public string MBR_MAJKE { get; set; }
        public int? SI1 { get; set; }
        public int? SI2 { get; set; }
        public string KL_P { get; set; }
        public string BR_SISA { get; set; }
        public int? FAZA { get; set; }
        public int? PRED_FAZA { get; set; }
        public string NAPOMENA { get; set; }
        public int? Uk_zivo { get; set; }
        public int? Uk_mrtvo { get; set; }
        public int? Uk_zal { get; set; }
        public decimal? Pr_zi { get; set; }
        public decimal? Pr_mr { get; set; }
        public decimal? Pr_zal { get; set; }
    }
}
