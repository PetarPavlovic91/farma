﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._09RezimeSmotre
{
    public class SmotraMladihNerastovaReportVM
    {
        public long Rb { get; set; }
        public string Tetovir { get; set; }
        public string RASA { get; set; }
        public string ODG { get; set; }
        public DateTime DAT_ROD { get; set; }
        public DateTime D_TESTA { get; set; }
        public string BR_SISA { get; set; }
        public int? EKST { get; set; }
        public int? Starost { get; set; }
        public decimal? TEZINA { get; set; }
        public decimal? Prirast { get; set; }
        public string Konver { get; set; }
        public int? L_sl { get; set; }
        public int? B_sl { get; set; }
        public decimal? Proc_m { get; set; }
        public int? SI1 { get; set; }
    }
}
