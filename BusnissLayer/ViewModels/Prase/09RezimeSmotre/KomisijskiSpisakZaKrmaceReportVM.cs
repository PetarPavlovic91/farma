﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._09RezimeSmotre
{
    public class KomisijskiSpisakZaKrmaceReportVM
    {
        public long Rb { get; set; }
        public string Krmaca { get; set; }
        public string Klasa { get; set; }
        public int? EKST { get; set; }
        public string VLA { get; set; }
        public string ODG { get; set; }
        public string MBR_KRM { get; set; }
        public string GN { get; set; }
        public string RASA { get; set; }
        public string MARKICA { get; set; }
        public string Hr { get; set; }
        public string Broj { get; set; }
        public DateTime DAT_ROD { get; set; }
        public string GNO { get; set; }
        public string MBR_OCA { get; set; }
        public string GNM { get; set; }
        public string MBR_MAJKE { get; set; }
        public int? SI1 { get; set; }
        public int? SI2 { get; set; }
        public string KL_P { get; set; }
        public string Iez { get; set; }
        public string St_1_pr { get; set; }
        public string BR_SISA { get; set; }
        public int? FAZA { get; set; }
        public int? PRED_FAZA { get; set; }
        public string NAPOMENA { get; set; }
        public DateTime Dat_pras { get; set; }
        public int? Br_l { get; set; }
        public int? Uk_zivo { get; set; }
        public int? Uk_mrtvo { get; set; }
        public int? Uk_zaluc { get; set; }
        public decimal? Pr_zivo { get; set; }
        public decimal? Pr_mrtvo { get; set; }
        public decimal? Pr_zaluc { get; set; }
        public decimal? Masa_l { get; set; }
        public int? Uk_lakt { get; set; }
        public int? Uk_praz_d { get; set; }
        public int? Uk_proiz_d { get; set; }
        public string Otac { get; set; }
        public string Hr_o { get; set; }
        public int? Si_o { get; set; }
        public string Kl_o { get; set; }
        public string Majka { get; set; }
        public string Hr_m { get; set; }
        public int? Si_m { get; set; }
        public string Kl_m { get; set; }
        public DateTime DAT_SMOTRE { get; set; }
    }
}
