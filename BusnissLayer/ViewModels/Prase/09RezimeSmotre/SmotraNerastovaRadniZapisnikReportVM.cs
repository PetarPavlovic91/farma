﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.RezimeSmotre
{
    public class SmotraNerastovaRadniZapisnikReportVM
    {
    public long Rb { get; set; }
    public string Nerast { get; set; }
    public string RASA { get; set; }
    public string ODG { get; set; }
    public DateTime DAT_ROD { get; set; }
    public DateTime NA_FARM_OD { get; set; }
    public string BR_SISA { get; set; }
    public int? EKST { get; set; }
    public string Klasa { get; set; }
    public int? SI1 { get; set; }
    public string NAPOMENA { get; set; }
    public int? Osem { get; set; }
    public int? Opras { get; set; }
    public decimal? Pr_opr { get; set; }
    public decimal? Zivo { get; set; }
    public decimal? Mrtvo { get; set; }
    public decimal? Ukupno { get; set; }
}
}
