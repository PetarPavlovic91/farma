﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._09RezimeSmotre
{
    public class SmotraProizvodnjaKrmacaPoRasamaReportVM
    {
        public string Rasa { get; set; }
        public int? Br_krm { get; set; }
        public decimal? Procenat { get; set; }
        public int? Uk_zivo { get; set; }
        public int? Uk_mrtvo { get; set; }
        public int? Uk_opr { get; set; }
        public int? Uk_zal { get; set; }
        public decimal? Pr_zi { get; set; }
        public decimal? Pr_mr { get; set; }
        public decimal? Ukupno { get; set; }
        public decimal? Pr_zal { get; set; }
    }
}
