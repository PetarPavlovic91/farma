﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._04Iskljucenje
{
    public class EvidencijaIskljucenjaPregledVM
    {
        public int? RB { get; set; }
        public string POL { get; set; }
        public string T_GRLA { get; set; }
        public string MBR_GRLA { get; set; }
        public string RAZL_SK { get; set; }
        public string REFK { get; set; }
    }
}
