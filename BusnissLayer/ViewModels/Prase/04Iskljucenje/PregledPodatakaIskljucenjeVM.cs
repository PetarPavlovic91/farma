﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._04Iskljucenje
{
    public class PregledPodatakaIskljucenjeVM
    {
        public string Grlo { get; set; }
        public string Pol { get; set; }
        public DateTime? Rodjeno { get; set; }
        public DateTime? Iskljuceno { get; set; }
        public string Razlog { get; set; }
        public int? Ciklus { get; set; }
        public int? Faza { get; set; }
        public int? Pr_dana { get; set; }
    }
}
