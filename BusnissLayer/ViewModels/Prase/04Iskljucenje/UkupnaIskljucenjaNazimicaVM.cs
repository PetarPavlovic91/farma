﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._04Iskljucenje
{
    public class UkupnaIskljucenjaNazimicaVM
    {
        public string Sif { get; set; }
        public string Razlog { get; set; }
        public int Broj { get; set; }
    }
}
