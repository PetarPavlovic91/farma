﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._04Iskljucenje
{
    public class UkupnaIskljucenjaKrmacaVM
    {
        public string Sif { get; set; }
        public string Razlog { get; set; }
        public int Pr_01 { get; set; }
        public int Pr_02 { get; set; }
        public int Pr_03 { get; set; }
        public int Pr_04 { get; set; }
        public int Pr_05 { get; set; }
        public int Pr_06 { get; set; }
        public int Pr_07 { get; set; }
        public int Pr_08 { get; set; }
        public int Pr_09 { get; set; }
        public int Pr_10 { get; set; }
        public int Pr_11 { get; set; }
        public int Ukupno { get; set; }
    }
}
