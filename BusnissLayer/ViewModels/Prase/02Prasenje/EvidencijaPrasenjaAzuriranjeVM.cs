﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Prasenje
{
    public class EvidencijaPrasenjaAzuriranjeVM
    {
        public int Id { get; set; }
        public DateTime DatumPrasenja { get; set; }
        public string Krmaca { get; set; }
        public int? RB { get; set; }
        public int PARIT { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string BAB { get; set; }
        public string RAD { get; set; }
        public string MAT { get; set; }

        public int? ZIVO { get; set; }
        public int? MRTVO { get; set; }
        public int? ZIVOZ { get; set; }
        public int? UBIJ { get; set; }
        public int? UGNJ { get; set; }
        public int? ZAPRIM { get; set; }
        public int? UGIN { get; set; }
        public int? DODATO { get; set; }
        public int? ODUZETO { get; set; }
        public int? GAJI { get; set; }
        public double? TEZ_LEG { get; set; }
        public string ANOMALIJE { get; set; }
        public int? TMP_OD { get; set; }
        public int? TMP_DO { get; set; }
        public int? TZP_OD { get; set; }
        public int? TZP_DO { get; set; }
        public int? TOV_OD { get; set; }
        public int? TOV_DO { get; set; }
        public string REG_PRAS { get; set; }
        public string OL_P { get; set; }
        public string REFK { get; set; }
    }
}
