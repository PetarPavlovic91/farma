﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje.ReportModels
{
    public class KontrolaProduktivnostiKrmacaReportVM
    {
        public string Genotip { get; set; }
        public int? Br_krm { get; set; }
        public int? Br_leg { get; set; }
        public decimal? Supra { get; set; }
        public decimal? Zivo { get; set; }
        public decimal? Mrtvo { get; set; }
        public decimal? Ukupno { get; set; }
        public decimal? Zal_l { get; set; }
        public decimal? Lak { get; set; }
        public decimal? Zal_p { get; set; }
        public decimal? Tez_l { get; set; }
        public decimal? Tez_p { get; set; }
    }
}
