﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje.ReportModels
{
    public class ProizvodniRezultatiKrmacaReportVM
    {
        public string T_KRM { get; set; }
        public string RASA { get; set; }
        public DateTime? DAT_ROD { get; set; }
        public string   KLASA { get; set; }
        public int? Rb_pra { get; set; }
        public DateTime? DAT_PRIP { get; set; }
        public DateTime? DAT_PRAS { get; set; }
        public int? ZIVO { get; set; }
        public int? MRTVO { get; set; }
        public DateTime? DAT_ZAL { get; set; }
        public int? ZALUC { get; set; }
        public int? Supras { get; set; }
        public int? Lakt { get; set; }
    }
}
