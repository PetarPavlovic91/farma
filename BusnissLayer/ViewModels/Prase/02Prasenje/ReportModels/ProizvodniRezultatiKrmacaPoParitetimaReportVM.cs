﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje.ReportModels
{
    public class ProizvodniRezultatiKrmacaPoParitetimaReportVM
    {
        public int? PARIT { get; set; }
        public int? Br_krm { get; set; }
        public decimal? Procenat { get; set; }
        public int? Br_legla { get; set; }
        public decimal? SI1 { get; set; }
        public decimal? SI2 { get; set; }
        public decimal? SI3 { get; set; }
        public decimal? ZIVO { get; set; }
        public decimal? MRTVO { get; set; }
        public decimal? Ukupno { get; set; }
        public decimal? ZALUC { get; set; }
        public decimal? Lakt { get; set; }
        public decimal? Pazni { get; set; }
    }
}
