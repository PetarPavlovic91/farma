﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje.ReportModels
{
    public class KontrolaProduktivnostiNerastovskihMajkiReportVM
    {
        public string Genotip { get; set; }
        public int? Br_krm { get; set; }
        public int? Br_leg { get; set; }
        public decimal? Zivo { get; set; }
        public decimal? Lakt { get; set; }
        public decimal? Zal { get; set; }
        public decimal? Tez_l { get; set; }
    }
}
