﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje.ReportModels
{
    public class DistribucijaZivoRodjenePrasadiReportVM
    {
        public int? PARIT { get; set; }
        public int? ZIVO { get; set; }
        public int? Ukrstanje { get; set; }
    }
}
