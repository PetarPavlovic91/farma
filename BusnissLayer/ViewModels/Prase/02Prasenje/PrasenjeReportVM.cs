﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje
{
    public class PrasenjeReportVM
    {
        public int RB { get; set; }
        public string Tetovir { get; set; }
        public string K_RASA { get; set; }
        public string Slovo { get; set; }
        public string Broj { get; set; }
        public string K_HB_RB_N { get; set; }
        public DateTime DAT_PRIP { get; set; }
        public string T_NER { get; set; }
        public string N_Rasa { get; set; }
        public string N_HBRBN { get; set; }
        public DateTime DAT_PRAS { get; set; }
        public int PARIT { get; set; }
        public int Musko { get; set; }
        public int Zensko { get; set; }
        public double TEZ_LEG { get; set; }
        public int MRTVO { get; set; }
        public string ANOMALIJE { get; set; }
        public string Odgajivac { get; set; }
    }
}
