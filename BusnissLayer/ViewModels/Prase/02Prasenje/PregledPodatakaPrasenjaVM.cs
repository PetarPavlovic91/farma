﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Prasenje
{
    public class PregledPodatakaPrasenjaVM : EvidencijaPrasenjaPregledVM
    {
        public string Sk { get; set; }
        public string Otac { get; set; }
        public DateTime Oprasena { get; set; }
        
    }
}
