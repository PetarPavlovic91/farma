﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje
{
    public class NeopraseneKrmaceINazimiceVM
    {
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int? Ciklus { get; set; }
        public DateTime? Pripustena { get; set; }
        public int? Dana { get; set; }
        public DateTime? Ocek_pras { get; set; }
        public string Nerast { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public string Napomena { get; set; }
    }
}
