﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._02Prasenje
{
    public class KontrolaProduktivnostiKrmacaVM
    {
        public string Genotip { get; set; }
        public string Br_krm { get; set; }
        public string Br_leg { get; set; }
        public string Supra { get; set; }
        public string Zivo { get; set; }
        public string Mrtvo { get; set; }
        public string Ukupno { get; set; }
        public string Zal_l { get; set; }
        public string Lak { get; set; }
        public string Zal_p { get; set; }
        public string Tez_l { get; set; }
        public string Tez_p { get; set; }
    }
}
