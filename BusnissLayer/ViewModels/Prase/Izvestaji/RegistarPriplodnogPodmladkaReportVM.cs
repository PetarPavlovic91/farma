﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class RegistarPriplodnogPodmladkaReportVM
    {
        public long Red { get; set; }
        public string Tetov { get; set; }
        public DateTime Dat_rod { get; set; }
        public string Pol { get; set; }
        public string Rasa { get; set; }
        public int? Starost { get; set; }
        public string Iz_legla { get; set; }
        public string Rb_l { get; set; }
        public string Majka { get; set; }
        public string Hb_Majke { get; set; }
        public string Rasa_m { get; set; }
        public string Mbr_majke { get; set; }
        public string Otac { get; set; }
        public string Hb_Oca { get; set; }
        public string Rasa_o { get; set; }
        public string Mbr_oca { get; set; }
        public int? S_l { get; set; }
        public int? S_d { get; set; }
    }
}
