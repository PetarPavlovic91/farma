﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class MUS2KontrolaProduktivnostiUmaticenihNerastovaReportVM
    {
        public long Rb { get; set; }
        public string Rasan { get; set; }
        public int? Broj { get; set; }
        public int? Os_n { get; set; }
        public int? Os_k { get; set; }
        public int? Uk_os { get; set; }
        public int? Povad { get; set; }
        public int? Ne_op { get; set; }
        public int? Isklj { get; set; }
        public int? Pobac { get; set; }
        public int? Prod { get; set; }
        public int? Op_n { get; set; }
        public int? Op_k { get; set; }
        public int? Uk_opr { get; set; }
        public decimal? P_op_n { get; set; }
        public decimal? P_op_k { get; set; }
        public decimal? P_op_uk { get; set; }
        public decimal? Zivo { get; set; }
        public decimal? Mrtvo { get; set; }
        public decimal? Uk_pr { get; set; }
    }
}
