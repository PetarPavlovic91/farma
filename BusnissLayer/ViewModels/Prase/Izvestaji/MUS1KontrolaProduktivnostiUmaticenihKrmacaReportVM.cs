﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class MUS1KontrolaProduktivnostiUmaticenihKrmacaReportVM
    {
        public long Rb { get; set; }
        public string Genotip { get; set; }
        public int? Br_krm { get; set; }
        public decimal? Proc { get; set; }
        public int? Br_leg { get; set; }
        public decimal? Pr_zivo { get; set; }
        public decimal? Pr_mrtvo { get; set; }
        public decimal? Ukupno { get; set; }
        public int? Zal_lp { get; set; }
        public int? Zal_l { get; set; }
        public decimal? Lakt { get; set; }
        public decimal? Zal_opr { get; set; }
        public decimal? Zal_zal { get; set; }
        public decimal? Tez_l { get; set; }
        public decimal? Tez_p { get; set; }
    }
}
