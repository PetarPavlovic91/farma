﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class Registar_ZalucenjaReportVM
    {
        public long Red { get; set; }
        public DateTime Dat_zal { get; set; }
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public string Id_s { get; set; }
        public string Id_broj { get; set; }
        public int? Parit { get; set; }
        public string Hb_rb_n { get; set; }
        public int? Zaluc { get; set; }
        public decimal? Tez_zal { get; set; }
        public int? Rbz { get; set; }
        public string Dojara { get; set; }
        public string Odg { get; set; }
        public string Mbr_krm { get; set; }
    }
}
