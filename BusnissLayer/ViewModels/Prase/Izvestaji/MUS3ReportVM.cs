﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class MUS3ReportVM
    {
        public long Rb { get; set; }
        public string Genotip { get; set; }
        public int? Krm { get; set; }
        public int? Leg { get; set; }
        public decimal? Zivo { get; set; }
        public decimal? Lakt { get; set; }
        public decimal? Zal { get; set; }
        public decimal? Tez_l { get; set; }
    }
}
