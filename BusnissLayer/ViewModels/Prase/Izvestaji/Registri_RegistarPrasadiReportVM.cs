﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class Registri_RegistarPrasadiReportVM
    {
        public long Red { get; set; }
        public int? Tet_od { get; set; }
        public int? Tet_do { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Rasa { get; set; }
        public string Majka { get; set; }
        public string Rasa_m { get; set; }
        public string Markica { get; set; }
        public string Id_s { get; set; }
        public string Id_broj { get; set; }
        public string M_Hb_rb_n { get; set; }
        public string Otac { get; set; }
        public string Rasa_o { get; set; }
        public string O_Hb_rb { get; set; }
        public string Odg { get; set; }
        public string Mbr_majke { get; set; }
        public string Mbr_oca { get; set; }
    }
}
