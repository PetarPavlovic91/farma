﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class UltrazvucnoMerenjeReportVM
    {
        public DateTime D_TESTA { get; set; }
        public long Rb { get; set; }
        public string Pol { get; set; }
        public string Gn { get; set; }
        public string T_test { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public string Mbr_test { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Mbr_oca { get; set; }
        public string Mbr_majke { get; set; }
        public string Iz_legla { get; set; }
        public string Markica { get; set; }
        public int? Ekst { get; set; }
        public string Br_sisa { get; set; }
        public string Gen_mar { get; set; }
        public string Halotan { get; set; }
        public DateTime? U_test { get; set; }
        public decimal? Poc_tez { get; set; }
        public decimal? Tezina { get; set; }
        public decimal? Prirast { get; set; }
        public string Hrana { get; set; }
        public int? L_sl { get; set; }
        public int? B_sl { get; set; }
        public int? Dub_s { get; set; }
        public int? Dub_m { get; set; }
        public decimal? Proc_m { get; set; }
        public int? Si1f { get; set; }
        public int? Si1 { get; set; }
        public DateTime? D_skart { get; set; }
        public string Razl_sk { get; set; }
        public string Napomena { get; set; }
        public DateTime? U_priplod { get; set; }
        public string Tetovirt10 { get; set; }
        public string Gno { get; set; }
        public string T_oca10 { get; set; }
        public string Gnm { get; set; }
        public string T_majke10 { get; set; }
        public string Refk { get; set; }
        public int? Starost { get; set; }
        public string M_hbrb { get; set; }
        public int? M_Si { get; set; }
        public string M_kl { get; set; }
        public string O_hbrb { get; set; }
        public int? O_Si { get; set; }
        public string O_kl { get; set; }
        public string Odgo { get; set; }
        public string Odgm { get; set; }
    }
}
