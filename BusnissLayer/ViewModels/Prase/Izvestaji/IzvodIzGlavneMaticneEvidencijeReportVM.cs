﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class IzvodIzGlavneMaticneEvidencijeReportVM
    {
        public long Red { get; set; }
        public string Pol { get; set; }
        public string HbRbNb { get; set; }
        public string Broj { get; set; }
        public string Grlo { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public DateTime Dat_rod { get; set; }
        public int? Si1 { get; set; }
        public DateTime? Dat_pras { get; set; }
        public string Dat_pos_l { get; set; }
        public int? Br_l { get; set; }
        public string Markica { get; set; }
        public string Klasa { get; set; }
        public string Red_s { get; set; }
        public string Dat_z_skok { get; set; }
        public decimal? Zivo { get; set; }
        public string Kl_m { get; set; }
        public string Otac_l { get; set; }
        public string Hb_oca_l { get; set; }
        public string Rb_reg_p { get; set; }
        public string Id_oca { get; set; }
        public string Id_majke { get; set; }
        public string Gn { get; set; }
        public string Mbr_grla { get; set; }
        public string Gno { get; set; }
        public string Mbr_oca { get; set; }
        public string Gnm { get; set; }
        public string Mbr_majke { get; set; }
    }
}
