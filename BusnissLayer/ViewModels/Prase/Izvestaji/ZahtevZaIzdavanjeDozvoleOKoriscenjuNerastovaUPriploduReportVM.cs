﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class ZahtevZaIzdavanjeDozvoleOKoriscenjuNerastovaUPriploduReportVM
    {
        public long Red { get; set; }
        public string Nerast { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public string Hr { get; set; }
        public string Broj { get; set; }
        public string Dat_smotre { get; set; }
        public string Red_s { get; set; }
        public DateTime? Dat_rod { get; set; }
        public int? Si1 { get; set; }
        public string Napomena { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Mbr_ner { get; set; }
        public string Vlasnik { get; set; }
    }
}
