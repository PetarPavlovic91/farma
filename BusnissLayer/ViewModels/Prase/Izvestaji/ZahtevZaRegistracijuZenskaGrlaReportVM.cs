﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class ZahtevZaRegistracijuZenskaGrlaReportVM
    {
        public long Red { get; set; }
        public string Krmaca { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public int? Si1 { get; set; }
        public DateTime? Dat_pras { get; set; }
        public int? Br_l { get; set; }
        public string Markica { get; set; }
        public string Hr { get; set; }
        public string Broj { get; set; }
        public string Klasa { get; set; }
        public string Otac { get; set; }
        public string Hr_o { get; set; }
        public int? Si_o  { get; set; }
        public string Kl_o { get; set; }
        public string Id_o { get; set; }
        public string Majka { get; set; }
        public string Hr_m { get; set; }
        public int? Si_m { get; set; }
        public string Kl_m { get; set; }
        public string Id_m { get; set; }
        public string Otac_l { get; set; }
        public string Hb_oca_l { get; set; }
        public string Rb_reg_p { get; set; }
        public int? Tp_od { get; set; }
        public int? Tp_do { get; set; }
        public int? Tmp_od { get; set; }
        public int? Tmp_do { get; set; }
        public int? Tzp_od { get; set; }
        public int? Tzp_do { get; set; }
        public string Rasap { get; set; }
        public string Primedba { get; set; }
        public string Gn { get; set; }
        public string Mbr_krm { get; set; }
        public string Gno { get; set; }
        public string Mbr_oca { get; set; }
        public string Gnm { get; set; }
        public string Mbr_majke { get; set; }
        public string Gnol { get; set; }
        public string Mbr_oca_l { get; set; }
    }
}
