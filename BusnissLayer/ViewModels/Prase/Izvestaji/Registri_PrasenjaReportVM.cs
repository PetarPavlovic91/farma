﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class Registri_PrasenjaReportVM
    {
        public long Red { get; set; }
        public string Reg_pras { get; set; }
        public string Krmaca { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public string Id_s { get; set; }
        public string Id_broj { get; set; }
        public string HbBroj { get; set; }
        public string RbBroj { get; set; }
        public string NbBroj { get; set; }
        public string Mbr_ner { get; set; }
        public string Rasan { get; set; }
        public string Nerast { get; set; }
        public string HbRb_n { get; set; }
        public DateTime? Dat_prip { get; set; }
        public DateTime? Dat_pras { get; set; }
        public int? Br_l { get; set; }
        public int? Zivo { get; set; }
        public int? Zivom { get; set; }
        public int? Zivoz { get; set; }
        public decimal? Tez_leg { get; set; }
        public int? Mrtvo { get; set; }
        public int? Dodato { get; set; }
        public int? Oduzeto { get; set; }
        public string Anomalije { get; set; }
        public int? Ugin { get; set; }
        public DateTime? Dat_zal { get; set; }
        public int? Zaluc { get; set; }
        public int? Lakt { get; set; }
        public decimal? Tez { get; set; }
        public int? Tet_od { get; set; }
        public int? Tet_do { get; set; }
        public int? Cs_od { get; set; }
        public int? Cs_do { get; set; }
        public DateTime? D_prvi_pri { get; set; }
        public DateTime? D_fert_pri { get; set; }
        public DateTime? D_skart { get; set; }
        public string Razl_sk { get; set; }
        public DateTime? D_sl_pras { get; set; }
        public int? Praz_d { get; set; }
        public int? Z_s_lakt { get; set; }
        public int? Z_s_grav { get; set; }
        public int? M_prip { get; set; }
        public int? Z_prip { get; set; }
        public string Primedba { get; set; }
        public string Mbr_krm { get; set; }
    }
}
