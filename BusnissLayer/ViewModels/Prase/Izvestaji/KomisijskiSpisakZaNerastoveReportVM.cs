﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class KomisijskiSpisakZaNerastoveReportVM
    {
        public long Rb { get; set; }
        public string Klasa { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Gn { get; set; }
        public string Tet_ner { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public string HbBroj { get; set; }
        public string Br_dozvole { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Mbr_ner { get; set; }
        public int? Os_naz { get; set; }
        public int? Os_krm { get; set; }
        public int? Uk_osem { get; set; }
        public int? Op_naz { get; set; }
        public int? Op_krm { get; set; }
        public int? Uk_opr { get; set; }
        public decimal? Pr_op_n { get; set; }
        public decimal? Pr_op_k { get; set; }
        public decimal? Pr_op { get; set; }
        public int? Zivo_n { get; set; }
        public int? Zivo_k { get; set; }
        public int? Mrtvo_n { get; set; }
        public int? Mrtvo_k { get; set; }
        public int? Uk_pr_n { get; set; }
        public int? Uk_pr_k { get; set; }
        public decimal? Pr_zi_n { get; set; }
        public decimal? Pr_zi_k { get; set; }
        public decimal? Pr_zi { get; set; }
        public decimal? Pr_mr_n { get; set; }
        public decimal? Pr_mr_k { get; set; }
        public decimal? Pr_mr { get; set; }
        public decimal? Pr_uk_n { get; set; }
        public decimal? Pr_uk_k { get; set; }
        public decimal? Pr_uk { get; set; }
        public int? Prir { get; set; }
        public int? Konv { get; set; }
        public int? Si2 { get; set; }
        public int? Mld { get; set; }
        public int? Si1 { get; set; }
        public string T_ner { get; set; }
    }
}
