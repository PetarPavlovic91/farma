﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class Registri_PripustiReportVM
    {
        public long Red { get; set; }
        public DateTime DAT_PRIP { get; set; }
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public string Id_s { get; set; }
        public string Id_broj { get; set; }
        public int? Ciklus { get; set; }
        public string K_n { get; set; }
        public string Sk { get; set; }
        public int? Rbp { get; set; }
        public string Raz { get; set; }
        public string Nerast { get; set; }
        public string Rasan { get; set; }
        public string Pv { get; set; }
        public string Osem { get; set; }
        public string Pripusnica { get; set; }
        public string Primedba { get; set; }
        public string Vla { get; set; }
        public string Mbr_krm { get; set; }
    }
}
