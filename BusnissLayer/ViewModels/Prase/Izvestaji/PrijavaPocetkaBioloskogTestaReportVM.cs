﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class PrijavaPocetkaBioloskogTestaReportVM
    {
        public long Red { get; set; }
        public string Nerast { get; set; }
        public string Markica { get; set; }
        public string Br_dozvole { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Rasa { get; set; }
        public DateTime? D_p_skoka { get; set; }
        public string Vla { get; set; }
        public string Vlasnik { get; set; }
        public string Otac { get; set; }
        public string Hr_o { get; set; }
        public int? Si_o { get; set; }
        public string Kl_o { get; set; }
        public string Majka { get; set; }
        public string Hr_m { get; set; }
        public int? Si_m { get; set; }
        public string Kl_m { get; set; }
        public string Mbr_ner { get; set; }
        public string Mbr_oca { get; set; }
        public string Mbr_majke { get; set; }
    }
}
