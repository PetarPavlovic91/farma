﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class Registri_SkartGrlaReportVM
    {
        public long Red { get; set; }
        public DateTime? D_skart { get; set; }
        public string T_grla { get; set; }
        public string Rasa { get; set; }
        public string Pol { get; set; }
        public string Kategorija { get; set; }
        public string Markica { get; set; }
        public string Id_s { get; set; }
        public string Id_broj { get; set; }
        public string Hb_rb_n { get; set; }
        public string Razl_sk { get; set; }
        public string Razlog_sk { get; set; }
        public string Napomena { get; set; }
        public string Odg { get; set; }
        public string Mbr_grla { get; set; }
    }
}
