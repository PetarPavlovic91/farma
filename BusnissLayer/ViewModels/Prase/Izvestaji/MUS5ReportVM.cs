﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class MUS5ReportVM
    {
        public long Rb { get; set; }
        public string Rasa { get; set; }
        public int? Testirano { get; set; }
        public decimal? Starost { get; set; }
        public decimal? Tez_pt { get; set; }
        public decimal? Tezina { get; set; }
        public decimal? Dnev_pr { get; set; }
        public decimal? Prirast { get; set; }
        public decimal? Konv { get; set; }
        public decimal? L_sl { get; set; }
        public decimal? Dub_s { get; set; }
        public decimal? Mld { get; set; }
        public decimal? P_mesa { get; set; }
        public decimal? Si1 { get; set; }
    }
}
