﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class GrlaNaGazdinstvuReportVM
    {
        public long Red { get; set; }
        public string Grlo { get; set; }
        public string Kartegorija { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public string HbRbNb { get; set; }
        public string Broj { get; set; }
        public string Red_s { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Pol { get; set; }
        public string Mbr_grla { get; set; }
        public int? Kat { get; set; }
    }
}
