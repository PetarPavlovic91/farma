﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Izvestaji
{
    public class MUS6ReportVM
    {
        public long Rb { get; set; }
        public string Nerast { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public DateTime? D_p_skoka { get; set; }
        public int? Br_leg { get; set; }
        public int? Br_pras { get; set; }
        public int? Leg_mane { get; set; }
        public string Pr_mane { get; set; }
        public string Hc { get; set; }
        public string Aa { get; set; }
        public string Rp { get; set; }
        public string Lp { get; set; }
        public string Av { get; set; }
        public string He { get; set; }
    }
}
