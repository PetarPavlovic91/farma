﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze
{
    public class PrvaDetekcijaSuprasnostiVM
    {
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int? Ciklus { get; set; }
        public DateTime? Pripustena { get; set; }
        public int? Dana { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public string Napomena { get; set; }
    }
}
