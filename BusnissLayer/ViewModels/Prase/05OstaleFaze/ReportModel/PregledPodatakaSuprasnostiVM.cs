﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze.ReportModel
{
    public class PregledPodatakaSuprasnostiVM
    {
        public string Krmaca { get; set; }
        public bool PrvaSuprasnost { get; set; }
        public bool DrugaSuprasnost { get; set; }
        public bool NijeSuprasna { get; set; }
    }
}
