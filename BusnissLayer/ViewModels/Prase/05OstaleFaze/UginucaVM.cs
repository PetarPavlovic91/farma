﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze
{
    public class UginucaVM
    {
        public DateTime DatumUginuca { get; set; }
        public string Nerast { get; set; }
        public string Kategorija { get; set; }
        public string Razlog { get; set; }
        public int Komada { get; set; }
        public string MestoRodjenja { get; set; }
    }
}
