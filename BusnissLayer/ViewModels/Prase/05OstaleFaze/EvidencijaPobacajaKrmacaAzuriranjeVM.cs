﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze
{
    public class EvidencijaPobacajaKrmacaAzuriranjeVM
    {
        public int Id { get; set; }
        public int? Rb { get; set; }
        public string Krmaca { get; set; }
        public string MaticniBroj { get; set; }
        public int? Ciklus { get; set; }
        public DateTime? DatumPobacaja { get; set; }
    }
}
