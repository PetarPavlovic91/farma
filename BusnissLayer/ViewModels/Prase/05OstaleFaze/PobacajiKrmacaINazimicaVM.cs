﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze
{
    public class PobacajiKrmacaINazimicaVM
    {
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int? Ciklus { get; set; }
        public string Sk { get; set; }
        public DateTime? Pobacaj { get; set; }
    }
}
