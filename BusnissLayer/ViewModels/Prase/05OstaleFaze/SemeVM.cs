﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze
{
    public class SemeVM
    {
        public DateTime DAT_SEME { get; set; }
        public int? RB { get; set; }
        public string MARKICA { get; set; }
        public int? RB_UZ { get; set; }
        public int? LIBIDO { get; set; }
        public int? KOLIC { get; set; }
        public int? GUST { get; set; }
        public int? BR_U_ML { get; set; }
        public int? VALO { get; set; }
        public int? POKR { get; set; }
        public int? P_MRT { get; set; }
        public int? P_MORF { get; set; }
        public int? UPOT { get; set; }
        public int? RAZR { get; set; }
        public int? PRIP { get; set; }
        public int? KOL_RS { get; set; }
        public int? DOZA { get; set; }
        public int? POK_RS { get; set; }
        public DateTime? DAT_PREG { get; set; }
        public int? POK_PS { get; set; }
        public string NAPOMENA { get; set; }
        public string REFK { get; set; }
    }
}
