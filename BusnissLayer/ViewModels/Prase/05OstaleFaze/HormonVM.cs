﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._05OstaleFaze
{
    public class HormonVM
    {
        public DateTime DatumDavanjaHormona { get; set; }
        public string Krmaca { get; set; }
        public string TipHormona { get; set; }
    }
}
