﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._03Zalucenje
{
    public class KrmaceZaZalucenjeVM
    {
        public string Krmaca { get; set; }
        public string Markica { get; set; }
        public string Rasa { get; set; }
        public int? Parit { get; set; }
        public DateTime Oprasena { get; set; }
        public int? Dana { get; set; }
        public int? Prasadi { get; set; }
        public int? Rbz { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public string Napomena { get; set; }
    }
}
