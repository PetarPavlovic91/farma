﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Zalucenje
{
    public class EvidencijaZalucenjaAzuriranjeVM
    {
        public int Id { get; set; }
        public DateTime DatumZalucenja { get; set; }
        public string Krmaca { get; set; }
        public int? RB { get; set; }
        public int? PARIT { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string PZ { get; set; }
        public string RAZ_PZ { get; set; }
        public int? ZALUC { get; set; }
        public double? TEZ_ZAL { get; set; }
        public string ZDR_ST { get; set; }
        public string OL_Z { get; set; }
        public int? RBZ { get; set; }
        public string DOJARA { get; set; }
        public string REFK { get; set; }
    }
}
