﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._03Zalucenje
{
    public class PregledPodatakaZalucenjeVM
    {
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int? Parit { get; set; }
        public string Sk { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public DateTime Zalucena { get; set; }
        public string Pz { get; set; }
        public string RazPz { get; set; }
        public string Zdr_st { get; set; }
        public int? Zivo { get; set; }
        public int? Gaji { get; set; }
        public int? Zaluc { get; set; }
        public double? Tezina { get; set; }
        public int Lakt { get; set; }
        public string Ol_z { get; set; }
        public string leo { get; set; }
    }
}
