﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._03Zalucenje.ReportModel
{
    public class LeglaZaTetoviranjePrasadiReportVM
    {
        public string MBR_KRM { get; set; }
        public string Krmaca { get; set; }
        public string RASA { get; set; }
        public int? PARIT { get; set; }
        public DateTime? DAT_PRAS { get; set; }
        public int? Dana { get; set; }
        public int? Prasadi { get; set; }
        public int? Zens { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string Zenska_od { get; set; }
        public string Zenska_do { get; set; }
        public string Muski_od { get; set; }
        public string Muski_do { get; set; }
    }
}
