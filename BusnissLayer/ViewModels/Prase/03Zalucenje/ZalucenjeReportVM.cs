﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._03Zalucenje
{
    public class ZalucenjeReportVM
    {
        public string RB { get; set; }
        public string DAT_ZAL { get; set; }
        public string T_KRM { get; set; }
        public string Slovo { get; set; }
        public string Broj { get; set; }
        public string K_RASA { get; set; }
        public string K_HB_RB_N { get; set; }
        public int? ZALUC { get; set; }
        public double? TEZ_ZAL { get; set; }
        public string VLA { get; set; }
    }
}
