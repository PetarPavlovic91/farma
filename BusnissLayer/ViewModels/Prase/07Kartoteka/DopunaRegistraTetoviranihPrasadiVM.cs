﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class DopunaRegistraTetoviranihPrasadiVM
    {
        public int Id { get; set; }
        public string Krmaca { get; set; }
        public string MaticniBroj { get; set; }
        public int Parit { get; set; }
        public int? Zivo { get; set; }
        public int? Tmp_od { get; set; }
        public int? Tmp_do { get; set; }
        public int? Tzp_od { get; set; }
        public int? Tzp_do { get; set; }
        public int? Tov_od { get; set; }
        public int? Tov_do { get; set; }
        public string Broj_registra { get; set; }
        public string Ocena { get; set; }
        public int? Uginulo { get; set; }
    }
}
