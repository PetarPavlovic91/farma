﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka.ReportModels
{
    public class RegistarPrasadiReportVM
    {
        public string T_pras { get; set; }
        public string Pol { get; set; }
        public string Rasa { get; set; }
        public DateTime Dat_rod { get; set; }
        public string Mbr_oca { get; set; }
        public string Mbr_majke { get; set; }
        public string Iz_legla { get; set; }
        public string S_l { get; set; }
        public string S_d { get; set; }
        public bool Priplod { get; set; }
        public string Mbr_pras { get; set; }
    }
}
