﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka.ReportModels
{
    public class GrlaBezPotpunogPoreklaReportVM
    {
        public string Tetovir { get; set; }
        public string POL { get; set; }
        public string RASA { get; set; }
        public DateTime? DAT_ROD { get; set; }
        public string MB_MAJKE { get; set; }
        public string MB_OCA { get; set; }
        public string MBM_BABE { get; set; }
        public string MBM_DEDE { get; set; }
        public string MBMB_PRAB { get; set; }
        public string MBMB_PRAD { get; set; }
        public string MBMD_PRAB { get; set; }
        public string MBMD_PRAD { get; set; }
        public string MBO_BABE { get; set; }
        public string MBO_DEDE { get; set; }
        public string MBOB_PRAB { get; set; }
        public string MBOB_PRAD { get; set; }
        public string MBOD_PRAB { get; set; }
        public string MBOD_PRAD { get; set; }
    }
}
