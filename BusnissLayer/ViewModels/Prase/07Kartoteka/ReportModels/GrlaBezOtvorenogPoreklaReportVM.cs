﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka.ReportModels
{
    public class GrlaBezOtvorenogPoreklaReportVM
    {
        public string Tetovir { get; set; }
        public string POL { get; set; }
        public string RASA { get; set; }
        public string Mbr_Grla { get; set; }
        public DateTime? DAT_ROD { get; set; }
        public string Otac { get; set; }
        public string Ped_o { get; set; }
        public string Majka { get; set; }
        public string Ped_m { get; set; }
    }
}
