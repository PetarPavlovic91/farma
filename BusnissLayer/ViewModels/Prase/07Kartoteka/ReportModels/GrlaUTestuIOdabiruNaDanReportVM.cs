﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka.ReportModels
{
    public class GrlaUTestuIOdabiruNaDanReportVM
    {
        public long Red { get; set; }
        public string Grlo { get; set; }
        public string Rasa { get; set; }
        public DateTime Dat_rod { get; set; }
        public string Markica { get; set; }
        public string Otac { get; set; }
        public string Majka { get; set; }
        public int Starost { get; set; }
        public DateTime D_testa { get; set; }
        public int? Ekst { get; set; }
        public string Br_sisa { get; set; }
        public decimal? Tezina { get; set; }
        public decimal? Prirast { get; set; }
        public int? Si1 { get; set; }
    }
}
