﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka.ReportModels
{
    public class TetoviranaPrasadNaStanjuNaDanReportVM
    {
        public long Rb { get; set; }
        public string Tetov { get; set; }
        public string Pol { get; set; }
        public string Rasa { get; set; }
        public DateTime Dat_rod { get; set; }
        public int? Starost { get; set; }
        public string Otac { get; set; }
        public string Majka { get; set; }
    }
}
