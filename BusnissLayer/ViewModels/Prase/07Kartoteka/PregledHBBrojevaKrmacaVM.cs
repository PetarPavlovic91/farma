﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class PregledHBBrojevaKrmacaVM
    {
        public string Krmaca { get; set; }
        public string HBBroj { get; set; }
        public string Rasa { get; set; }
        public string Mbr_krm { get; set; }
        public DateTime? Dat_rod { get; set; }
        public int? Ekst { get; set; }
        public string Sisa { get; set; }
        public string Klasa { get; set; }
        public int? Ciklus { get; set; }
        public int? Parit { get; set; }
        public int? Faza { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public string Markica { get; set; }
        public string Napomena { get; set; }
        public int? Uk_zivo { get; set; }
        public int? Uk_mrtvo { get; set; }

    }
}
