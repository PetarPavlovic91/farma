﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class KartotekaNerastovaPregled_GodineVM
    {
        public string Godina { get; set; }
        public string Kat { get; set; }
        public int? Osemen { get; set; }
        public int? Povadj { get; set; }
        public int? Pobac { get; set; }
        public int? Opras { get; set; }
        //public float Proc_op { get; set; }
        public int? Zal_krm { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Zaluc { get; set; }
    }
}
