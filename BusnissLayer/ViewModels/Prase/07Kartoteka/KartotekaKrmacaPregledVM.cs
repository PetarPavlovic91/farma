﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class KartotekaKrmacaPregledVM
    {
        public int Id { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public string Krmaca { get; set; }
        public int? Cliklus { get; set; }
        public int? Paritet { get; set; }
        public int? Faza { get; set; }
        public DateTime? Rodjena { get; set; }
        public string Mat_broj_oca { get; set; }
        public string Mat_broj_majke { get; set; }
        public string Iz_legla { get; set; }
        public string Klasa { get; set; }
        public int? Ekst { get; set; }
        public string Sisa { get; set; }
        public DateTime? Na_farmi_od { get; set; }
        public int? Si1 { get; set; }
        public int? Si2 { get; set; }
        public int? Si3 { get; set; }
        public int? lez { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Zenski { get; set; }
        public int? Zaluc { get; set; }
        public int? Lakt { get; set; }
        public int? Prazni_dani { get; set; }
        public int? Proizv_d { get; set; }
        public DateTime? Iskljucena { get; set; }
        public string Razl { get; set; }
        public string HB_Broj { get; set; }
        public string RB_Broj { get; set; }
        public string Grupa { get; set; }
        public string Napomena { get; set; }
        public string Markica { get; set; }
        public string Opis { get; set; }
        public string Gn { get; set; }
        public string Tetovir { get; set; }
        public string GnO { get; set; }
        public string Tet_oca { get; set; }
        public string GnM { get; set; }
        public string Tet_majke { get; set; }
    }
}
