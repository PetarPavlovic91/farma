﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class PregledAktivnihNerastovaNaDanVM
    {
        public long Red { get; set; }
        public string T_ner { get; set; }
        public string Rasa { get; set; }
        public string Mbr_ner { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Gn { get; set; }
        public string Markica { get; set; }
        public string Otac { get; set; }
        public string Majka { get; set; }
        public string HB_Broj { get; set; }
        public string RB_Broj { get; set; }
        public string Red_s { get; set; }
        public string Klasa { get; set; }
        public int? Ekst { get; set; }
        public int? Si1 { get; set; }
        public DateTime? Na_farmi_od { get; set; }
        public DateTime? D_p_skoka { get; set; }
        public int? Br_os { get; set; }
        public int? Br_op { get; set; }
        public int? Uk_zivo { get; set; }
        public int? Uk_mrt { get; set; }
        public string Tetovirn10 { get; set; }
        public string Napomena { get; set; }
        public DateTime? D_skart { get; set; }
        public string Sk { get; set; }
    }
}
