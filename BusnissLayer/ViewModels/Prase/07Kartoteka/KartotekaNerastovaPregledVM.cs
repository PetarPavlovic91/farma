﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class KartotekaNerastovaPregledVM
    {
        public int Id { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public string Nerast { get; set; }
        public DateTime? Rodjen { get; set; }
        public string Mat_broj_nerasta { get; set; }
        public string Mat_broj_oca { get; set; }
        public string Mat_broj_majke { get; set; }
        public string Iz_legla { get; set; }
        public string Klasa { get; set; }
        public int? Ekst { get; set; }
        public string Sisa { get; set; }
        public DateTime? Na_farmi_od { get; set; }
        public int? Si1 { get; set; }
        public int? Si2 { get; set; }
        public int? Si3 { get; set; }
        public DateTime? Prvi_skok { get; set; }
        public string Pv { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Br_os { get; set; }
        public int? Br_op { get; set; }
        public DateTime? Iskljucen { get; set; }
        public string Razl { get; set; }
        public string HB_Broj { get; set; }
        public string RB_Broj { get; set; }
        public string Napomena { get; set; }
        public string Markica { get; set; }
        public string Opis { get; set; }
        public string Tetovir { get; set; }
        public string GnO { get; set; }
        public string Tet_oca { get; set; }
        public string GnM { get; set; }
        public string Tet_majke { get; set; }
        public string Broj_dozvole { get; set; }
    }
}
