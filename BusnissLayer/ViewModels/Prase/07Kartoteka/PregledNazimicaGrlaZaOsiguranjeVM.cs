﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class PregledNazimicaGrlaZaOsiguranjeVM
    {
        public long Rb { get; set; }
        public string Nazimica { get; set; }
        public string Rasa { get; set; }
        public string Mbr_krm { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Markica { get; set; }
        public string Hb_rb_broj { get; set; }
    }
}
