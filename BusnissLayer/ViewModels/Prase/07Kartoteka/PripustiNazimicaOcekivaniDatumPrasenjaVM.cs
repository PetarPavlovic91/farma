﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class PripustiNazimicaOcekivaniDatumPrasenjaVM
    {
        public string Mbr_krm { get; set; }
        public string Nazimica { get; set; }
        public string Rasa { get; set; }
        public int? Ciklus { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public DateTime? Pripustena { get; set; }
        public int? Rbp { get; set; }
        public string Raz { get; set; }
        public string Nerast { get; set; }
        public string Pv { get; set; }
        public string Osem { get; set; }
        public DateTime? Ocek_pras { get; set; }
    }
}
