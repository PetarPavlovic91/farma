﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class MaticniBrojIFazaVM
    {
        public string Mat_br { get; set; }
        public string Mbr_oca { get; set; }
        public string Mbr_majke { get; set; }
        public string GrloJe { get; set; }
        public string Ciklus_Parit_Faza { get; set; }
        public string DatumRodjenja { get; set; }
    }
}
