﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class AktivneKrmaceNaDanVM
    {
        public long Red { get; set; }
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Mbr_krm { get; set; }
        public string Markica { get; set; }
        public string HB_Broj { get; set; }
        public string RB_Broj { get; set; }
        public string Klasa { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public int? Parit { get; set; }
        public int? Uk_Zivo { get; set; }
        public int? Uk_Mrtvo { get; set; }
        public int? Uk_Zaluc { get; set; }
    }
}
