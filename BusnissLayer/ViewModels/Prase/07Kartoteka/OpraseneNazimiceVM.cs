﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class OpraseneNazimiceVM
    {
        public string Nazimica { get; set; }
        public string Rasa { get; set; }
        public int? Parit { get; set; }
        public string Sk { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public DateTime? Oprasena { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Ukupno { get; set; }
        public int? Dod { get; set; }
        public int? Odu { get; set; }
        public int? Gaji { get; set; }
        public double? Tez { get; set; }
        public string Alvaarhho { get; set; }
        public string Tm { get; set; }
        public string Tz { get; set; }
    }
}
