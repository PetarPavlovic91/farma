﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class AktivneKrmaceNaDanHBRBNStrukturaVM
    {
        public string Tip { get; set; }
        public int? Broj { get; set; }
        public decimal? Procenat { get; set; }
    }
}
