﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class AnalizaPotomstvaPoOcuVM
    {
        public string Rod { get; set; }
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int? Pr { get; set; }
        public string Sk { get; set; }
        public int? Zivo { get; set; }
        public decimal? P_ziv { get; set; }
        public int? Mrt { get; set; }
        public decimal? P_mr { get; set; }
        public int? Zal { get; set; }
        public decimal? P_zal { get; set; }
        public int? Lakt { get; set; }
        public decimal? P_lak { get; set; }
        public int? Prazn { get; set; }
        public decimal? P_praz { get; set; }
        public string Mbr_krm { get; set; }
    }
}
