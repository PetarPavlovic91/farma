﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class PripustiNazimicaVM
    {
        public string Nazimica { get; set; }
        public string Rasa { get; set; }
        public int? Ciklus { get; set; }
        public string Sk { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public DateTime? Pripustena { get; set; }
        public int? Rbp { get; set; }
        public string Raz { get; set; }
        public string Nerast { get; set; }
        public string Pv { get; set; }
        public string Osem { get; set; }
        public string Ocena { get; set; }
        public string Opras { get; set; }
    }
}
