﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class ZaluceneNazimiceVM
    {
        public string Nazimica { get; set; }
        public string Rasa { get; set; }
        public int? Parit { get; set; }
        public string Sk { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public DateTime? Zalucena { get; set; }
        public string Pz { get; set; }
        public string Zdr_st { get; set; }
        public int? Zivo { get; set; }
        public int? Gaji { get; set; }
        public int? Zaluc { get; set; }
        public double? Tez { get; set; }
        public int? Lakt { get; set; }
        public int? leo { get; set; }
    }
}
