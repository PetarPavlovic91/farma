﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._07Kartoteka
{
    public class PregledNazimicaMaticniPodaciVM
    {
        public string Nazimica { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Mbr_oca { get; set; }
        public string Mbr_majke { get; set; }
        public int? Ekst { get; set; }
        public string Br_sisa { get; set; }
        public int? Si1 { get; set; }
        public DateTime? D_skart { get; set; }
        public string Razl_sk { get; set; }
    }
}
