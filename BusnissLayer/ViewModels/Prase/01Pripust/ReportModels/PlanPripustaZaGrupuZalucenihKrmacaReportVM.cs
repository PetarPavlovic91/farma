﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class PlanPripustaZaGrupuZalucenihKrmacaReportVM
    {
        public string T_KRM { get; set; }
        public string RASA { get; set; }
        public int? SI1 { get; set; }
        public string KLASA { get; set; }
        public string T_NER { get; set; }
        public string Value { get; set; }
    }
}
