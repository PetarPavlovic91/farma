﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class DnevniPlanPripustaVM
    {
        public string T_KRM { get; set; }
        public string  Rasa { get; set; }
        public int? SI1 { get; set; }
        public string KLASA { get; set; }
        public decimal? Pr_oprasenih { get; set; }
        public int? STAROST { get; set; }
        public string  T_Ner { get; set; }
        public string Ukrstanje { get; set; }
    }
}
