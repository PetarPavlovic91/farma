﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class OprasivostPoCiklusimaReportVM
    {
        public int? Ciklus { get; set; }
        public int? Osemenjeno { get; set; }
        public int? Povad { get; set; }
        public int? Ne_op { get; set; }
        public int? Isklj { get; set; }
        public int? Pobac { get; set; }
        public int? Prod { get; set; }
        public int? Opraseno { get; set; }
        public int? Proc_opr { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Uk_pras { get; set; }
    }
}
