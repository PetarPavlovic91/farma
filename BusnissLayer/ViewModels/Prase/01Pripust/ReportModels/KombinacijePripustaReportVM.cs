﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class KombinacijePripustaReportVM
    {
        public string KRasa { get; set; }
        public string NRasa { get; set; }
        public int? BrojPripustaPoRasi { get; set; }
    }
}
