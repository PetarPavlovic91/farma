﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class KontrolaProduktivnostiOsemeniteljaReportVM
    {
        public string Osem { get; set; }
        public int? Os_k { get; set; }
        public int? Os_n { get; set; }
        public int? Uk_os { get; set; }
        public int? Povad { get; set; }
        public int? Ne_op { get; set; }
        public int? Isklj { get; set; }
        public int? Pobac { get; set; }
        public int? Prod { get; set; }
        public int? Op_n { get; set; }
        public int? Op_k { get; set; }
        public int? Uk_opr { get; set; }
        public int? P_op_n { get; set; }
        public int? P_op_k { get; set; }
        public int? P_op_uk { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Uk_p { get; set; }
    }
}
