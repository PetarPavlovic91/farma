﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class KalendarKoriscenjaNerastovaReportVM
    {
        public DateTime DAT_PRIP { get; set; }
        public string T_NER { get; set; }
        public int Pripust { get; set; }
    }
}
