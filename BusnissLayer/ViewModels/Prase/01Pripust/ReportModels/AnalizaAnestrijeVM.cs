﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.ReportModels
{
    public class AnalizaAnestrijeVM
    {
        public string Krmaca { get; set; }
        public string Rasak { get; set; }
        public int? CIKLUS { get; set; }
        public int? Rbp { get; set; }
        public DateTime? DAT_ZAL { get; set; }
        public string Vh { get; set; }
        public string Ps { get; set; }
        public string Vo { get; set; }
        public string T_NER { get; set; }
        public int? Ze1 { get; set; }
        public int? E1e2 { get; set; }
    }
}
