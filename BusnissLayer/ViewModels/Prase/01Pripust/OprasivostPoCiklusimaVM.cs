﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust
{
    public class OprasivostPoCiklusimaVM
    {
        public int Ciklus { get; set; }
        public int Osemenjeno { get; set; }
        public int Povad { get; set; }
        public int Ne_op { get; set; }
        public int Isklj { get; set; }
        public int Pobac { get; set; }
        public int Prod { get; set; }
        public int Opraseno { get; set; }
        public double Proc_opr { get; set; }
        public double Zivo { get; set; }
        public double Mrtvo { get; set; }
        public double Uk_pras { get; set; }
    }
}
