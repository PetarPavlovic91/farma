﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust.ReproduktivniPokazateljKrmaca
{
    public class ReproduktivniPokazateljiZalucenihKrmacaVM
    {
        public int Rb { get; set; }
        public string Opis { get; set; }
        public int? Prvopr { get; set; }
        public decimal? Pr_prv { get; set; }
        public int? Ostale { get; set; }
        public decimal? Prv_ost { get; set; }
        public int? Ukupno { get; set; }
        public decimal? Pr_uk { get; set; }
    }
}
