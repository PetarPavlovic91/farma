﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust.ReproduktivniPokazateljKrmaca
{
    public class ReproduktivniPokazateljiZalucenihKrmacaPoMesecimaVM
    {
        public string Mesec { get; set; }
        public int Zaluc { get; set; }
        public string Pr { get; set; }
        public double Sk_zal { get; set; }
        public double Pr_7_d { get; set; }
        public double Pripust { get; set; }
        public string Hormon { get; set; }
        public string Hor_prip { get; set; }
        public string Povadj { get; set; }
        public string Pov_25 { get; set; }
        public string Isklj { get; set; }
        public string Opr_1pr { get; set; }
        public string Opraseno { get; set; }
    }
}
