﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust.ReproduktivniPokazateljKrmaca
{
    public class ReproduktivniPokazateljiZalucenihKrmacaVMProcedura
    {
        public int ZalPrvoPr { get; set; }
        public int ZalOstale { get; set; }
        public int IskljucenePosleZalucenjaPrvoPr { get; set; }
        public int IskljucenePosleZalucenjaOstale { get; set; }
        public int PripustDo7DanaPrvoPr { get; set; }
        public int PripustDo7DanaOstale { get; set; }
        public int OsemenjenoPrvoPr { get; set; }
        public int OsemenjenoOstale { get; set; }
        public int PovadjaloUkupnoPrvoPr { get; set; }
        public int PovadjaloUkupnoOstale { get; set; }
        public int PovadjaloDo25PrvoPr { get; set; }
        public int PovadjaloDo25Ostale { get; set; }
        public int PovadjaloPreko25PrvoPr { get; set; }
        public int PovadjaloPreko25Ostale { get; set; }
        public int UkupnoOprasenoPrvoPr { get; set; }
        public int UkupnoOprasenoOstale { get; set; }
        public int OprasenoOdPrvogPripustaPrvoPr { get; set; }
        public int OprasenoOdPrvogPripustaOstale { get; set; }
        public int IskljucenoBezJalovihPrvoPr { get; set; }
        public int IskljucenoBezJalovihOstale { get; set; }
        public int OprasenoPlusIskljucenoPrvoPr { get; set; }
        public int OprasenoPlusIskljucenoOstale { get; set; }
        public int JaloveINeoprasenePrvoPr { get; set; }
        public int JaloveINeopraseneOstale { get; set; }
    }
}
