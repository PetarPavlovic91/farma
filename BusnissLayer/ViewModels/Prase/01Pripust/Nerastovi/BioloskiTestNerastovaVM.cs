﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust.Nerastovi
{
    public class BioloskiTestNerastovaVM
    {
        public string T_NER { get; set; }
        public string Rasa { get; set; }
        public DateTime Dat_rod { get; set; }
        public int? Br_leg { get; set; }
        public int? Br_pras { get; set; }
        public int? Leg_mane { get; set; }
        public int? Pr_mane { get; set; }
        public int? Hc { get; set; }
        public int? Aa { get; set; }
        public int? Lp { get; set; }
        public int? Rp { get; set; }
        public int? Av { get; set; }
        public int? He { get; set; }
    }
}
