﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust.ProblemiUreprodukciji
{
    public class ProblemiUReprodukcijiVM
    {
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int Ciklus { get; set; }
        public int Rbp { get; set; }
        public DateTime Dat_zal { get; set; }
        public string Vh { get; set; }
        public string Ps { get; set; }
        public int Vo { get; set; }
        public string Nerast { get; set; }
        public int Ze1 { get; set; }
        public int E1e2 { get; set; }
        public string P1 { get; set; }
        public string P2 { get; set; }
        public string P3 { get; set; }
        public string P4 { get; set; }
        public string P5 { get; set; }
        public string P6 { get; set; }
        public string P7 { get; set; }
        public string P8 { get; set; }
        public string E1 { get; set; }
        public string E2 { get; set; }
        public string E3 { get; set; }
        public string E4 { get; set; }
        public string E5 { get; set; }
        public string E6 { get; set; }
        public string E7 { get; set; }
        public string E8 { get; set; }
        public string H1 { get; set; }
        public string H2 { get; set; }
        public string H3 { get; set; }
        public string H4 { get; set; }
        public string H5 { get; set; }
        public string H6 { get; set; }
        public string H7 { get; set; }
        public string H8 { get; set; }
    }
}
