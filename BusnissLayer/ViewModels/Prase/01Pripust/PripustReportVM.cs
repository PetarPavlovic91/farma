﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust
{
    public class PripustReportVM
    {
        public int RB { get; set; }
        public DateTime DAT_PRIP { get; set; }
        public string T_KRM { get; set; }
        public string Slovo { get; set; }
        public string Broj { get; set; }
        public string K_RASA { get; set; }
        public string KN { get; set; }
        public string T_Ner { get; set; }
        public string N_RASA { get; set; }
        public string VO_PP { get; set; }
        public string VO_PP_PoRedu { get; set; }
        public string PRIPUSNICA { get; set; }
        public string PRIMEDBA { get; set; }
        public string VLA { get; set; }
    }
}
