﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust
{
    public class KrmaceZaPripustVM
    {
        public string Krmaca { get; set; }
        public string Rasa { get; set; }
        public int? Parit { get; set; }
        public DateTime Zalucena { get; set; }
        public int? Dana { get; set; }
        public int? Faza { get; set; }
        public string Zdr_st { get; set; }
        public string Vr_hor { get; set; }
        public string Obj { get; set; }
        public string Box { get; set; }
        public string Napomena { get; set; }
    }
}
