﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust
{
    public class OprasivostPoRasamaNerastovaVM
    {
        public string Rasan { get; set; }
        public int Os_n { get; set; }
        public int Os_k { get; set; }
        public int Uk_os { get; set; }
        public int Povad { get; set; }
        public int Isklj { get; set; }
        public int Prod { get; set; }
        public int Pobac { get; set; }
        public int Ne_op { get; set; }
        public int Op_n { get; set; }
        public int Op_k { get; set; }
        public int Uk_op { get; set; }
        public double P_op_n { get; set; }
        public double P_op_k { get; set; }
        public double P_op_uk { get; set; }
        public double Zivo { get; set; }
        public double Mrtvo { get; set; }
        public double Uk_pr { get; set; }
    }
}
