﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust.KontrolaProduktivnostiNerastova
{
    public class BioloskiTestMladihNerastovaVM
    {
        public long Rb { get; set; }
        public string Mbr_ner { get; set; }
        public string T_krm { get; set; }
        public string Rasak { get; set; }
        public int? Rpb { get; set; }
        public DateTime? Dat_pras { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Ukupno { get; set; }
        public int? Hc { get; set; }
        public int? Aa { get; set; }
        public int? Rn { get; set; }
        public int? Lp { get; set; }
        public int? Rp { get; set; }
        public int? Av { get; set; }
        public int? Hr { get; set; }
        public int? Cr { get; set; }
        public int? He { get; set; }
    }
}
