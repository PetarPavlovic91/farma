﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase
{
    public class PripustVM
    {
        public int? Id { get; set; }
        public int? Rb { get; set; }
        public string KRMACA { get; set; }
        public string MATICNI_BROJ { get; set; }
        public int? CIKLUS { get; set; }
        public int? RBP { get; set; }
        public string RAZL_P { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string PV { get; set; }
        public string NERAST { get; set; }
        public string OSEM { get; set; }
        public string OCENA { get; set; }
        public string NERAST2 { get; set; }
        public string PV2 { get; set; }
        public string OSEM2 { get; set; }
        public string OCENA2 { get; set; }
        public string PRIPUSNICA { get; set; }
        public string PRIMEDBA { get; set; }
    }
}
