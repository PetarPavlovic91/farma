﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase.Pripust
{
    public class PregledPodatakaVM
    {
        public string KRMACA { get; set; }
        public string RASA { get; set; }
        public int? CIKLUS { get; set; }
        public string SK { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public DateTime? Pripustena { get; set; }
        public int? RBP { get; set; }
        public string RAZ { get; set; }
        public string NERAST { get; set; }
        public string PV { get; set; }
        public string OSEM { get; set; }
        public string OCENA { get; set; }
        public DateTime? Pobacaj { get; set; }
        public string Pr_dana { get; set; }
        public string Opras { get; set; }
    }
}
