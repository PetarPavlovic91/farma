﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._01Pripust
{
    public class PripustSaveVM
    {
        public int? Id { get; set; }
        public string Krmaca { get; set; }
        public string MaticniBroj { get; set; }
        public int? CIKLUS { get; set; }
        public int? RBP { get; set; }
        public string RAZL_P { get; set; }
        public string OBJ { get; set; }
        public string BOX { get; set; }
        public string Nerast { get; set; }
        public string PV { get; set; }
        public string OSEM { get; set; }
        public string OCENA { get; set; }
        public string Nerast2 { get; set; }
        public string PV2 { get; set; }
        public string OSEM2 { get; set; }
        public string OCENA2 { get; set; }
        public string PRIPUSNICA { get; set; }
        public string PRIMEDBA { get; set; }
        public DateTime? DAT_PRIP { get; set; }
    }
}
