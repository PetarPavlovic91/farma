﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class TestKrmacaNaPlodnost_SI2VM
    {
        public int RB { get; set; }
        public string Krmaca { get; set; }
        public string Odg    { get; set; }
        public string Rasa { get; set; }
        public string Markica { get; set; }
        public int? Parit { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Zaluc { get; set; }
        public decimal? Pr_zivo { get; set; }
        public decimal? Pr_mrtvo { get; set; }
        public decimal? Pr_zal { get; set; }
        public int? lez { get; set; }
        public int? Si2 { get; set; }
    }
}
