﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class PrenosPodatakaIzRegistraPrasadiUOdabirVM
    {
        public int Id { get; set; }
        public DateTime D_odabira { get; set; }
        public long Rb { get; set; }
        public string Pol { get; set; }
        public string Gn { get; set; }
        public string T_test { get; set; }
        public string Vla { get; set; }
        public string Odg { get; set; }
        public string Rasa { get; set; }
        public string Mbr_test { get; set; }
        public DateTime Dat_rod { get; set; }
        public string Mbr_oca { get; set; }
        public string Mbr_majke { get; set; }
        public string Iz_legla { get; set; }
        public string Br_sisa { get; set; }
    }
}
