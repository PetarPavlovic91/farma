﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class ProgeniTestKrmacaINerastovaSI3VM
    {
        public int RB { get; set; }
        public string Rasa { get; set; }
        public string Odg { get; set; }
        public string Krmaca { get; set; }
        public string Nerast { get; set; }
        public int? Parit { get; set; }
        public int? Zenskih { get; set; }
        public int? Muskih { get; set; }
        public int? Ukupno { get; set; }
        public int? Testirano { get; set; }
        public decimal? Prirast { get; set; }
        public decimal? L_SL { get; set; }
        public decimal? B_SL { get; set; }
        public decimal? P_mesa { get; set; }
        public int? Si3 { get; set; }
    }
}
