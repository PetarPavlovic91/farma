﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class AnalizaPripustaTestiranihGrlaReportVM
    {
        public DateTime? D_testa { get; set; }
        public string Tetovir { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Otac { get; set; }
        public string Majka { get; set; }
        public int? Starost { get; set; }
        public decimal? Tezina { get; set; }
        public decimal? Prirast { get; set; }
        public int? Index { get; set; }
        public DateTime? D_skarta { get; set; }
        public string Razl { get; set; }
        public DateTime? D_hormon { get; set; }
        public string Vh { get; set; }
        public DateTime? Dat_prip { get; set; }
        public string Nerast { get; set; }
        public string Rasan { get; set; }
        public int? Rbp { get; set; }
        public string Pv { get; set; }
        public string Osem { get; set; }
        public DateTime? DAT_PRAS { get; set; }
        public int? Zivo { get; set; }
        public int? Mrtvo { get; set; }
        public int? Gaji { get; set; }
        public DateTime? Dat_zal { get; set; }
        public int? Zaluc { get; set; }
        public decimal? Tez_zal { get; set; }
        public string Mbr_testa { get; set; }
    }
}
