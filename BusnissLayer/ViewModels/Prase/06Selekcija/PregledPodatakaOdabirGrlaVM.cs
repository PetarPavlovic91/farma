﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class PregledPodatakaOdabirGrlaVM
    {
        public DateTime Odabrana { get; set; }
        public string Pol { get; set; }
        public string Tetov { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Iz_legla { get; set; }
        public string Otac { get; set; }
        public string Majka { get; set; }
        public string Markica { get; set; }
        public int? Tezina { get; set; }
        public int?  Prirast { get; set; }
        public DateTime? U_test { get; set; }
        public DateTime? Iskljucena { get; set; }
        public string Razl { get; set; }
    }
}
