﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class EvidencijaPodatakaSaTestaGrlaZaPriplodVM
    {
        public int Id { get; set; }
        public string Pol { get; set; }
        public string Tetovir { get; set; }
        public string Rasa { get; set; }
        public DateTime? Rodjeno { get; set; }
        public string Mat_br_oca { get; set; }
        public string Mat_br_majke { get; set; }
        public string Iz_legla { get; set; }
        public int? Ekst { get; set; }
        public string Sisa { get; set; }
        public DateTime? Ulaz_u_test { get; set; }
        public double? Poc_tez { get; set; }
        public double? Tezina { get; set; }
        public int? Hrana { get; set; }
        public int? L_sl { get; set; }
        public int? B_sl { get; set; }
        public int? D_sl { get; set; }
        public int? D_mis { get; set; }
        public double? Procenat_Mesa { get; set; }
        public int? Si1 { get; set; }
        public DateTime? Iskljuceno { get; set; }
        public string Razl { get; set; }
        public string Napomena { get; set; }
        public DateTime? U_priplodu_od { get; set; }
        public string Markica { get; set; }
    }
}
