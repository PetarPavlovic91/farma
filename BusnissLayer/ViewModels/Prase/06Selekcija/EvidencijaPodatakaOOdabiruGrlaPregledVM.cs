﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class EvidencijaPodatakaOOdabiruGrlaPregledVM
    {
        public int Id { get; set; }
        public string Pol { get; set; }
        public string Tetovir { get; set; }
        public string Rasa { get; set; }
        public DateTime? Rodjeno { get; set; }
        public string MaticniBrojOca { get; set; }
        public string MaticniBrojMajke { get; set; }
        public string IzLegla { get; set; }
        public int? Ekst { get; set; }
        public string Sisa { get; set; }
        public int? Tezina { get; set; }
        public int? Prirast { get; set; }
        public string Markica { get; set; }
    }
}
