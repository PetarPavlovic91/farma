﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class AnalizaPerformansTestaPoRasamaReportVM
    {
        public int? Testirano { get; set; }
        public decimal? Starost { get; set; }
        public decimal? Tezina { get; set; }
        public decimal? Prirast { get; set; }
        public decimal? L_sl { get; set; }
        public decimal? B_sl { get; set; }
        public decimal? Dub_s { get; set; }
        public decimal? Mld { get; set; }
        public decimal? P_mesa { get; set; }
        public decimal? Si1 { get; set; }
        public int? Osem { get; set; }
        public decimal? Pr_osem { get; set; }
    }
}
