﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class AnalizaPerformansTestaPoOcuReportVM
    {
        public string Tetovir { get; set; }
        public string Rasa { get; set; }
        public string Majka { get; set; }
        public DateTime? DatumTesta { get; set; }
        public int? Starost { get; set; }
        public decimal? Tezina { get; set; }
        public int? Prirast { get; set; }
        public int? L_sl { get; set; }
        public int? B_sl { get; set; }
        public int? Dub_s { get; set; }
        public int? Mld { get; set; }
        public decimal? P_mesa { get; set; }
        public int? Si1 { get; set; }
        public string Priplod { get; set; }
    }
}
