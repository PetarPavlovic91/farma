﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Prase._06Selekcija
{
    public class PregledPodatakaTestiranjeMladihNerastovaVM
    {
        public DateTime Dat_testa { get; set; }
        public int? Rb { get; set; }
        public string Tetovir { get; set; }
        public string Rasa { get; set; }
        public DateTime? Dat_rod { get; set; }
        public string Iz_legla { get; set; }
        public string Mat_br_oca { get; set; }
        public string Mat_br_majke { get; set; }
        public string Markica { get; set; }
        public int? Starost { get; set; }
        public int? Ekst { get; set; }
        public string Sisa { get; set; }
        public double? Tezina { get; set; }
        public double? Prirast { get; set; }
        public string Ledj { get; set; }
        public string Bocna { get; set; }
        public string Dub_s { get; set; }
        public string Dub_m { get; set; }
        public string Proc_m { get; set; }
        public string Index { get; set; }
        public DateTime? U_priplod { get; set; }
        public DateTime? Iskljucen { get; set; }
        public string Razl { get; set; }
    }
}
