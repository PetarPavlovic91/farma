﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class ZIzdatnicaVM
    {
        public int Id { get; set; }
        public string Br_dok { get; set; }
        public DateTime Datum { get; set; }
        //public string Vet { get; set; }
        //public string VeterinarskiTehnicar { get; set; }
        public DateTime DatumTrebovanja { get; set; }
        public string BR_TREB { get; set; }
    }
}
