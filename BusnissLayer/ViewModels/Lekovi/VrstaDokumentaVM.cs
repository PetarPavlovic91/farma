﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VrstaDokumentaVM
    {
        public int Id { get; set; }
        public string VD { get; set; }
        public string Naziv { get; set; }
    }
}
