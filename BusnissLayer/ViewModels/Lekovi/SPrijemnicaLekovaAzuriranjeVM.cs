﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class SPrijemnicaLekovaAzuriranjeVM
    {
        public int ZPrijemnicaId { get; set; }
        public double RB { get; set; }
        public string Lek { get; set; }
        public string Naziv_Leka { get; set; }
        public string Jm { get; set; }
        public decimal Kolicina { get; set; }
        public decimal Cena { get; set; }
        public string Br_dok { get; set; }
        public DateTime Datum { get; set; }
        public string Sifpp { get; set; }
    }
}
