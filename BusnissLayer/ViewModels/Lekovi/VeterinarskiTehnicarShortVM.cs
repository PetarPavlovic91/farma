﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VeterinarskiTehnicarShortVM
    {
        public string VET { get; set; }
        public string Naziv { get; set; }
    }
}
