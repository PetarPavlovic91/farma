﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class StanjeLageraLekovaVM
    {
        public string VGP { get; set; }
        public string Lek { get; set; }
        public string Naziv { get; set; }
        public string Jm { get; set; }
        public decimal? Stanje { get; set; }
        public decimal? Cena { get; set; }
        public decimal? Vrednost { get; set; }
        public decimal? Pocetno_Stanje { get; set; }
        public decimal? Ulaz { get; set; }
        public decimal? Izlaz { get; set; }
    }
}
