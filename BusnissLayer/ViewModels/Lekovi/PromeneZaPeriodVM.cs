﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class PromeneZaPeriodVM
    {
        public string Lek { get; set; }
        public string Naziv { get; set; }
        public string Jm { get; set; }
        public decimal? Pocst { get; set; }
        public decimal? Vred_pocst { get; set; }
        public decimal? Ulaz { get; set; }
        public decimal? Vred_ulaz { get; set; }
        public decimal? Izlaz { get; set; }
        public decimal? Vred_izlaz { get; set; }
        public decimal? Stanje { get; set; }
        public decimal? Vrednost { get; set; }
    }
}
