﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class PocetnoStanjeVM
    {
        public int Id { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Cena { get; set; }
        public string SifraArtikla { get; set; }
        public string NazivArtikla { get; set; }
        public DateTime Datum { get; set; }
    }
}
