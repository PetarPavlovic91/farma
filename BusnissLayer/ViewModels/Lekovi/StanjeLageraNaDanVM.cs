﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class StanjeLageraNaDanVM
    {
        public string Lek { get; set; }
        public string Naziv { get; set; }
        public string Jm { get; set; }
        public double Pocst { get; set; }
        public double Vred_pocst { get; set; }
        public double Ulaz { get; set; }
        public double Vred_ulaz { get; set; }
        public double Izlaz { get; set; }
        public double Vred_izlaz { get; set; }
        public double Stanje { get; set; }
        public double Vrednost { get; set; }
    }
}
