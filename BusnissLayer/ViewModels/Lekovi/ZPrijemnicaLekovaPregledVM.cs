﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class ZPrijemnicaLekovaPregledVM
    {
        public int Id { get; set; }
        public string Br_dok { get; set; }
        public DateTime Datum { get; set; }
        public string Sifpp { get; set; }
        public string Naziv { get; set; }
        public string Dok_dobav { get; set; }
    }
}
