﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class PregledPoPoslovnimPartnerimaVM
    {
        public string Vd { get; set; }
        public DateTime Datum { get; set; }
        public string Br_dok { get; set; }
        public decimal? Vrednost { get; set; }
        public string Sifpp { get; set; }
        public string Naziv { get; set; }
    }
}
