﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Services.Lekovi
{
    public class SIzdatnicaPregledVM
    {
        public int Id { get; set; }
        public double? Rb { get; set; }
        public string Lek { get; set; }
        public string NazivLeka { get; set; }
        public string Jm { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Cena { get; set; }
        public string Vt { get; set; }
        public string MestoTroska { get; set; }
        public string Br_dok { get; set; }
        public string VeterinarskiTehnicar { get; set; }
    }
}
