﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class KarticaVM
    {
        public int Id { get; set; }
        public DateTime Datum { get; set; }
        public string BR_DOK { get; set; }
        //ULAZ_IZLAZ
        public string U_I { get; set; }
        public double Kolicina { get; set; }
        public double Cena { get; set; }
        //public string Veza { get; set; }
        public int VrstaDokumentaId { get; set; }
        public int? VeterinarskiTehnicarId { get; set; }
        public int ArtiklId { get; set; }
        public int? VrstaTroskaId { get; set; }
    }
}
