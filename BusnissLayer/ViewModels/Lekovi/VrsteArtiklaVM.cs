﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VrsteArtiklaVM
    {
        public int Id { get; set; }
        public string VGP { get; set; }
        public string Naziv { get; set; }
    }
}
