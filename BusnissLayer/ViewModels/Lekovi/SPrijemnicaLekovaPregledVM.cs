﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class SPrijemnicaLekovaPregledVM
    {
        public int Id { get; set; }
        public double? RB { get; set; }
        public string Lek { get; set; }
        public string Naziv_Leka { get; set; }
        public string Jm { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Cena { get; set; }
        public string Br_dok { get; set; }
    }
}
