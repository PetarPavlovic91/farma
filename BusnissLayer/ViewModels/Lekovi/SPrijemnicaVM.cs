﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class SPrijemnicaVM
    {
        public int Id { get; set; }
        public string BR_DOC { get; set; }
        public double? RB { get; set; }
        public decimal? Cena { get; set; }
        public decimal? Kolicina { get; set; }
        public int ArtiklId { get; set; }
    }
}
