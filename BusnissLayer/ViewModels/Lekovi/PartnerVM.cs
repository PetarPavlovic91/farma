﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class PartnerVM
    {
        public int Id { get; set; }
        public string SIFPP { get; set; }
        public string Naziv { get; set; }
        public string Adresa { get; set; }
        public string Mesto { get; set; }
        public string ZiroRacun { get; set; }
        public string Telefon { get; set; }
        public string Saradnik { get; set; }
    }
}
