﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class ArtiklVM
    {
        public int Id { get; set; }
        public string SifraArtikla { get; set; }
        public string Naziv { get; set; }
        public double GR_NAB { get; set; }
        public double PocetnoStanje { get; set; }
        public double Ulaz { get; set; }
        public double Izlaz { get; set; }
        public double Stanje { get; set; }
        public double PR_CENA { get; set; }
        public int JedinicaMereId { get; set; }
        public int VrstaArtiklaId { get; set; }
    }
}
