﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VrsteArtiklaShortVM
    {
        public string VGP { get; set; }
        public string Naziv { get; set; }
    }
}
