﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class ZPrijemnicaVM
    {
        public int Id { get; set; }
        public string BR_DOC { get; set; }
        public DateTime Datum { get; set; }
        public string DokumentDobavljaca { get; set; }
        public int PartnerId { get; set; }
    }
}
