﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class TrebovanjeVM
    {
        public double? RB { get; set; }
        public string LEK { get; set; }
        public string NAZIV_LEKA { get; set; }
        public string JM { get; set; }
        public decimal? KOLICINA { get; set; }
        public decimal? CENA { get; set; }
        public string VT { get; set; }
        public string VR_TROSKA { get; set; }
        public string VET { get; set; }
    }
}
