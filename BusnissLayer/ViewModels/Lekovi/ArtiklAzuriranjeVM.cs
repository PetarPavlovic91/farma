﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class ArtiklAzuriranjeVM
    {
        public int Id { get; set; }
        public string Lek { get; set; }
        public string Naziv { get; set; }
        public string Jm { get; set; }
        public string Grupa { get; set; }
        public string Naziv_Grupe { get; set; }
        public decimal? GR_NAB { get; set; }
    }
}
