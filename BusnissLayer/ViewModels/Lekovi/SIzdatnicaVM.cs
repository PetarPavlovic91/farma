﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class SIzdatnicaVM
    {
        public int Id { get; set; }
        public string BR_DOC { get; set; }
        public double RB { get; set; }
        public double Cena { get; set; }
        public double Kolicina { get; set; }
        public int ArtiklId { get; set; }
        public int VrstaTroskaId { get; set; }
    }
}
