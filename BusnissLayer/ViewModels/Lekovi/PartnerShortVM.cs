﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class PartnerShortVM
    {
        public string SIFPP { get; set; }
        public string Naziv { get; set; }
    }
}
