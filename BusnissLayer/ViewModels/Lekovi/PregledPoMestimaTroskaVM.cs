﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class PregledPoMestimaTroskaVM
    {
        public string Vt { get; set; }
        public string Mesto_tr { get; set; }
        public string Vet { get; set; }
        public string Vet_tehn { get; set; }
        public string Lek { get; set; }
        public string Naziv { get; set; }
        public string Jm { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Vrednost { get; set; }
    }
}
