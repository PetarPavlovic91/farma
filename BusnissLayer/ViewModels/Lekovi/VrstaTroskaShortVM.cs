﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VrstaTroskaShortVM
    {
        public string VT { get; set; }
        public string Naziv { get; set; }
    }
}
