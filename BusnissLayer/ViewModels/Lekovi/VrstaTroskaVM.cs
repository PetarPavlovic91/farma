﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VrstaTroskaVM
    {
        public int Id { get; set; }
        public string VT { get; set; }
        public string Naziv { get; set; }
    }
}
