﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusnissLayer.ViewModels.Lekovi
{
    public class KarticaPregledVM
    {
        public DateTime Datum { get; set; }
        public string BR_DOK { get; set; }
        //ULAZ_IZLAZ
        public string U_I { get; set; }
        public decimal? Kolicina { get; set; }
        public decimal? Cena { get; set; }
        public decimal? Stanje { get; set; }
        public string VEZA { get; set; }
        public string Naziv { get; set; }
    }
}
