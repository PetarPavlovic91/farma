﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.ViewModels.Lekovi
{
    public class VeterinarskiTehnicarVM
    {
        public int Id { get; set; }
        public string VET { get; set; }
        public string Naziv { get; set; }
        public string Mesto { get; set; }
        public string Ulica { get; set; }
        public string Telefon { get; set; }
    }
}
