USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Izvestaji].[ZahtevZaRegistracijuZenskaGrla]    Script Date: 8/15/2022 7:15:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Izvestaji].[ZahtevZaRegistracijuZenskaGrla]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max),
	@Paritet as Int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, k.T_KRM as Krmaca
		, k.ODG as Odg
		, k.RASA as Rasa
		, k.DAT_ROD as Dat_rod
		, k.SI1 as Si1
		, p.DAT_PRAS as Dat_pras
		, p.PARIT as Br_l
		, k.MARKICA as Markica
		, CASE when ISNULL(k.HBBROJ, '') <> '' then 'HB' when ISNULL(k.RBBROJ, '') <> '' then 'RB' when ISNULL(k.NBBROJ, '') <> '' then 'NB' end as Hr
		, COALESCE(k.HBBROJ,k.RBBROJ,k.NBBROJ) as Broj
		, k.KLASA as Klasa
		, o.T_NER as Otac
		, COALESCE(o.HBBROJ,o.RBBROJ) as Hr_o
		, o.SI1 as Si_o
		, o.KLASA as Kl_o
		, o.MARKICA as Id_o
		, m.T_KRM as Majka
		, COALESCE(m.HBBROJ,m.RBBROJ,m.NBBROJ) as Hr_m
		, m.SI1 as Si_m
		, m.KLASA as Kl_m
		, m.MARKICA as Id_m
		, lo.T_NER as Otac_l
		, COALESCE(lo.HBBROJ,lo.RBBROJ) as Hb_oca_l
		, p.REG_PRAS as Rb_reg_p
		, case when IsNull(p.TMP_OD,0) <> 0 and IsNull(p.TMP_OD,0) < IsNull(p.TZP_OD,0) then IsNull(p.TMP_OD,0) else IsNull(p.TZP_OD,0) end as Tp_od
		, case when IsNull(p.TMP_DO,0) <> 0 and IsNull(p.TMP_DO,0) > IsNull(p.TZP_DO,0) then IsNull(p.TMP_DO,0) else IsNull(p.TZP_DO,0) end as Tp_do
		, p.TMP_OD as Tmp_od
		, p.TMP_DO as Tmp_do
		, p.TZP_OD as Tzp_od
		, p.TZP_DO as Tzp_do
		, SUBSTRING(k.RASA, 1, 2) + SUBSTRING(lo.RASA, 1, 2) as Rasap
		, pr.PRIMEDBA as Primedba
		, k.GN as Gn
		, k.MBR_KRM as Mbr_krm
		, o.GN as Gno
		, o.MBR_NER as Mbr_oca
		, m.GN as Gnm
		, m.MBR_KRM as Mbr_majke
		, lo.GN as Gnol
		, lo.MBR_NER as Mbr_oca_l
	from Prasenje p
	join Krmaca k on k.Id = p.KrmacaId
	join Pripust pr on pr.KrmacaId = k.Id and p.PARIT = pr.CIKLUS
	left join Nerast lo on lo.Id = pr.NerastId
	left join Nerast o on o.MBR_NER = k.MBR_OCA
	left join Krmaca m on m.MBR_KRM = k.MBR_MAJKE
	where p.DAT_PRAS >= @DatumOd and p.DAT_PRAS <= @DatumDo and k.RASA like '%' + @Rasa + '%' and p.PARIT <= @Paritet
END
