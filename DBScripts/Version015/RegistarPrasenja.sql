USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Izvestaji].[Registri_Prasenja]    Script Date: 8/14/2022 3:27:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Izvestaji].[Registri_Prasenja]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, MAX(p.REG_PRAS) as Reg_pras
		, MAX(k.T_KRM) as Krmaca
		, MAX(k.ODG) as Odg
		, MAX(k.RASA) as Rasa
		, MAX(k.MARKICA) as Markica
		, SUBSTRING(MAX(k.MARKICA), 1, 3) as Id_s
		, SUBSTRING(MAX(k.MARKICA), 4,100) as Id_broj
		, MAX(k.HBBROJ) as HbBroj
		, MAX(k.RBBROJ) as RbBroj
		, MAX(k.NBBROJ) as NbBroj
		, MAX(n.MBR_NER) as Mbr_ner
		, MAX(n.RASA) as Rasan
		, MAX(n.T_NER) as Nerast
		, COALESCE(MAX(n.HBBROJ), MAX(n.RBBROJ)) as HbRb_n
		, MAX(pr.DAT_PRIP) as Dat_prip
		, MAX(p.DAT_PRAS) as Dat_pras
		, MAX(p.PARIT) as Br_l
		, MAX(p.ZIVO) as Zivo
		, MAX(p.ZIVO) - MAX(p.ZIVOZ) as Zivom
		, MAX(p.ZIVOZ) as Zivoz
		, MAX(p.TEZ_LEG) as Tez_leg
		, MAX(p.MRTVO) as Mrtvo
		, MAX(p.DODATO) as Dodato
		, MAX(p.ODUZETO) as Oduzeto
		, MAX(p.ANOMALIJE) as Anomalije
		, MAX(p.UGIN) as Ugin
		, MAX(z.DAT_ZAL) as Dat_zal
		, MAX(z.ZALUC) as Zaluc
		, DATEDIFF(day,MAX(p.DAT_PRAS), MAX(z.DAT_ZAL)) as Lakt
		, MAX(z.TEZ_ZAL) as Tez
		, case when IsNull(MAX(p.TMP_OD),0) <> 0 and IsNull(MAX(p.TMP_OD),0) < IsNull(MAX(p.TZP_OD),0) then IsNull(MAX(p.TMP_OD),0) else IsNull(MAX(p.TZP_OD),0) end as Tet_od
		, case when IsNull(MAX(p.TMP_DO),0) <> 0 and IsNull(MAX(p.TMP_DO),0) > IsNull(MAX(p.TZP_DO),0) then IsNull(MAX(p.TMP_DO),0) else IsNull(MAX(p.TZP_DO),0) end as Tet_do
		, null as Cs_od
		, null as Cs_do
		, MAX(pr1.DAT_PRIP) as D_prvi_pri
		, ISNULL(MAX(pr2.DAT_PRIP),MAX(pr1.DAT_PRIP)) as D_fert_pri
		, MAX(s.D_SKART) as D_skart
		, MAX(s.RAZL_SK) as Razl_sk
		, MAX(p2.DAT_PRAS) as D_sl_pras
		, DATEDIFF(day, MAX(z.DAT_ZAL), MAX(pr1.DAT_PRIP)) as Praz_d
		, null Z_s_lakt
		, null Z_s_grav
		, null M_prip
		, null Z_prip
		, MAX(pr.PRIMEDBA) as Primedba
		, MAX(k.MBR_KRM) as Mbr_krm
	from Prasenje p
	join Krmaca k on k.Id = p.KrmacaId
	join Pripust pr on pr.KrmacaId = k.Id and pr.CIKLUS = p.PARIT
	left join Nerast n on pr.NerastId = n.Id
	left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
	left join Skart s on s.KrmacaId =k.Id
	left join Pripust pr1 on pr1.KrmacaId = k.Id and pr1.CIKLUS = p.PARIT + 1 and pr1.RBP = 1
	left join Pripust pr2 on pr2.KrmacaId = k.Id and pr2.CIKLUS = p.PARIT + 1 and pr2.RBP = 2
	left join Prasenje p2 on p2.KrmacaId = k.Id and p2.PARIT = p.PARIT + 1
	where p.DAT_PRAS >= @DatumOd and p.DAT_PRAS <= @DatumDo and k.RASA like '%' + @Rasa + '%'
	GROUP by p.Id

END
