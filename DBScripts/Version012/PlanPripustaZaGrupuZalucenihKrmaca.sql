USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[PlanPripustaZaGrupuZalucenihKrmaca]    Script Date: 6/24/2022 5:11:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PlanPripustaZaGrupuZalucenihKrmaca]
	-- Add the parameters for the stored procedure here
	@datumZalucenja as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + n.T_NER + ']',
    '[' + n.T_NER + ']'
  )
from (
select distinct n.RASA + ' ' + n.T_NER as T_NER
FROM Krmaca k
join Zaluc z on z.KrmacaId = k.Id
cross join 
(
select n1.*
from Nerast n1
left join Skart s on s.NerastId = n1.Id
where s.Id is null
) as n
where z.DAT_ZAL >= @datumZalucenja and z.DAT_ZAL <= DATEADD(day, 1, @datumZalucenja)) n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select 
	k.T_KRM, 
	k.RASA, 	
	k.SI1,
	k.KLASA,
	n.RASA + '' '' + n.T_GRLA as T_NER,
	CASE
		WHEN p.MBR_GRLA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MB_OCA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MB_MAJKE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBM_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBM_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBO_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBO_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		ELSE ''+''
	END as Ukrstanje
	from Krmaca k
	join Pedigre p on k.Id = p.KrmacaId
	join Zaluc z on z.KrmacaId = k.Id
	cross join 
	(
	select p1.*, n1.RASA
	from Nerast n1
	join Pedigre p1 on n1.Id = p1.NerastId
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where z.DAT_ZAL >= '''+ CONVERT(NVARCHAR(MAX), @datumZalucenja)  +''' 
			and z.DAT_ZAL <=  ''' +  CONVERT(NVARCHAR(MAX), DATEADD(day, 1, @datumZalucenja)) +''') as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'



EXECUTE(@PivotTableSQL)

END
