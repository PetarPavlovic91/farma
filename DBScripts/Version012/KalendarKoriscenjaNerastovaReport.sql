USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KalendarKoriscenjaNerastovaReport]    Script Date: 6/29/2022 3:41:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KalendarKoriscenjaNerastovaReport]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
select p.DAT_PRIP, n.T_NER, Count(*) as Pripust
	from  Pripust p 
	join Nerast n on p.NerastId = n.Id
	left join Skart s on s.NerastId = n.Id
	where s.Id is null and p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
	group by  p.DAT_PRIP, n.T_NER
END
GO


