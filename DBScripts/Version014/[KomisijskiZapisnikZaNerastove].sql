USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Izvestaji].[KomisijskiZapisnikZaNerastove]    Script Date: 7/29/2022 11:39:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO















-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Izvestaji].[KomisijskiZapisnikZaNerastove]
@DatumOd as Date,
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date,
@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,max(kl.KLASA) as Klasa
,max(n.VLA) as Vla
,max(n.ODG) as Odg
,max(n.GN) as Gn
,max(n.T_NER) as Tet_ner
,max(n.RASA) as Rasa
,max(n.MARKICA) as Markica
,COALESCE(max(n.HBBROJ),max(n.RBBROJ)) as HbBroj
,max(n.BR_DOZVOLE) as Br_dozvole
,max(n.DAT_ROD) as Dat_rod
,max(n.MBR_NER) as Mbr_ner
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) as Os_naz
,Sum(case when k.PARIT > 1 then 1 else 0 end) as Os_krm
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) + Sum(case when k.PARIT > 1 then 1 else 0 end) as Uk_osem
,Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) as Op_naz
,Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end) as Op_krm
,Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) + Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end) as Uk_opr
,case when Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) = 0 then 0 else Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) * 100.00 / Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) end as Pr_op_n
,case when Sum(case when k.PARIT > 1 then 1 else 0 end) = 0 then 0 else Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end) * 100.00 / Sum(case when k.PARIT > 1 then 1 else 0 end) end as Pr_op_k
,case when (Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) + Sum(case when k.PARIT > 1 then 1 else 0 end)) = 0 then 0 else (Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) + Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end)) * 100.00 / (Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) + Sum(case when k.PARIT > 1 then 1 else 0 end)) end as Pr_op
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.ZIVO,0) else 0 end) as Zivo_n
,Sum(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.ZIVO,0) else 0 end) as Zivo_k
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.MRTVO,0) else 0 end) as Mrtvo_n
,Sum(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.MRTVO,0) else 0 end) as Mrtvo_k
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.ZIVO,0) else 0 end) + Sum(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.MRTVO,0) else 0 end) as Uk_pr_n
,Sum(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.ZIVO,0) else 0 end) + Sum(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.MRTVO,0) else 0 end) as Uk_pr_k
,AVG(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.ZIVO,0) else 0 end) as Pr_zi_n
,AVG(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.ZIVO,0) else 0 end) as Pr_zi_k
,AVG(ISNULL(p.ZIVO,0)) as Pr_zi
,AVG(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.MRTVO,0) else 0 end) as Pr_mr_n
,AVG(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.MRTVO,0) else 0 end) as Pr_mr_k
,AVG(ISNULL(p.MRTVO,0)) as Pr_mr
,AVG(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.ZIVO,0) else 0 end) + AVG(case when k.PARIT = 0 or k.PARIT = 1 then ISNULL(p.MRTVO,0) else 0 end) as Pr_uk_n
,AVG(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.ZIVO,0) else 0 end) + AVG(case when k.PARIT > 1 and p.Id is not null then ISNULL(p.MRTVO,0) else 0 end) as Pr_uk_k
,AVG(ISNULL(p.ZIVO,0)) + AVG(ISNULL(p.MRTVO,0)) as Pr_uk
,0 as Prir
,0 as Konv
,max(n.SI2) as Si2
,0 as Mld
,max(n.SI1) as Si1
,max(n.T_NER) as T_ner
from Nerast n
join Pripust pr on pr.NerastId = n.Id
join Krmaca k on pr.KrmacaId = k.Id
left join Skart s on n.Id = s.NerastId
join Klasa kl on kl.MBR_GRLA = n.MBR_NER
left join Prasenje p on p.KrmacaId = k.Id and p.PARIT = pr.CIKLUS
where kl.DAT_SMOTRE = @DatumOd and n.RASA like '%' + @Rasa +'%' and p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje
group by n.Id
END
