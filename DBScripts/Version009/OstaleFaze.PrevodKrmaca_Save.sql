USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [OstaleFaze].[Suprasnost_Save]    Script Date: 4/1/2022 5:42:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [OstaleFaze].[PrevodKrmaca_Save]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@PrvaSuprasnost AS bit,
	@DrugaSuprasnost AS bit,
	@NijeSuprasna AS bit,
	@Prevedena as bit,
	@Dat_Pristupa AS DATE,
	@Krmaca as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @KrmacaId AS INT = (SELECT TOP 1 k.Id FROM Krmaca k WHERE k.T_KRM = @Krmaca);
	DECLARE @PripustId AS INT = (SELECT TOP 1 p.Id FROM Pripust p WHERE p.KrmacaId = @KrmacaId and p.DAT_PRIP = @Dat_Pristupa);

	
	
    INSERT INTO Suprasnosts
	(
	   PrvaSuprasnost
	   ,DrugaSuprasnost
	   ,NijeSuprasna
	   ,Prevedena
      ,[PripustId]
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@PrvaSuprasnost, @DrugaSuprasnost, @NijeSuprasna,@Prevedena, @PripustId, GETDATE(),GETDATE());

END
ELSE BEGIN
		UPDATE Suprasnosts
		SET
		PrvaSuprasnost = @PrvaSuprasnost
	   ,DrugaSuprasnost = @DrugaSuprasnost
	   ,NijeSuprasna = @NijeSuprasna
	   ,Prevedena = @Prevedena
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END

END
GO


