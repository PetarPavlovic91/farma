USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[AnalizaPovadjanja]    Script Date: 3/19/2022 7:31:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[AnalizaPovadjanja]
	-- Add the parameters for the stored procedure here
	@DanOd as Date,
	@DanDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
k.T_KRM as Krmaca,
k.RASA as Rasak,
p.CIKLUS,
coalesce(p4.rbp,p3.rbp,p2.rbp,p1.rbp) as Rbp,
z.DAT_ZAL,
NULL as Vh,
CASE when n.Id is not null then 'P' else 'S' end as Ps,
CASE when n.Id is not null then p.OSEM else null end as Vo,
n.T_NER,
DATEDIFF(day, z.DAT_ZAL, p.DAT_PRIP) as Ze1,
DATEDIFF(day, p.DAT_PRIP, p1.DAT_PRIP) as E1e2
from Krmaca k 
join Pripust p on p.KrmacaId = k.Id and p.rbp = 1
left join Nerast n on p.NerastId = n.Id
left join Zaluc z on z.KrmacaId = p.KrmacaId and p.CIKLUS - 1 = z.PARIT
join Pripust p1 on p1.KrmacaId = p.KrmacaId and p1.CIKLUS = p.CIKLUS and p1.rbp = 2 
left join Pripust p2 on p2.KrmacaId = p.KrmacaId and p2.CIKLUS = p.CIKLUS and p2.rbp = 3
left join Pripust p3 on p3.KrmacaId = p.KrmacaId and p3.CIKLUS = p.CIKLUS and p3.rbp = 4
left join Pripust p4 on p4.KrmacaId = p.KrmacaId and p4.CIKLUS = p.CIKLUS and p4.rbp = 5
where p.DAT_PRIP > @DanOd and p.DAT_PRIP < @DanDo



END
GO


