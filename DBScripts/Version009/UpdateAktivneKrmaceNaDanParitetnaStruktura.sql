USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[AktivneKrmaceNaDanParitetnaStruktura]    Script Date: 4/6/2022 9:01:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AktivneKrmaceNaDanParitetnaStruktura]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@rasa as Nvarchar(max),
	@IsNazimica as Bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @allRows as int;

		if	@IsNazimica = 0
	begin

	 set @allRows = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%');

select 
PARIT as Parit,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%'
 group by PARIT
 
END

 ELSE

 BEGIN
 
	 set @allRows = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and k.CIKLUS = 1);

select 
PARIT as Parit,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and k.CIKLUS = 1
 group by PARIT
 
END
END
