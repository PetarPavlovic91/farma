USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [OstaleFaze].[SP_Save_Hormon]    Script Date: 4/6/2022 10:09:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.PromenaPolaPrasadiSave
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@Pol AS nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


		UPDATE Prase
		SET
		Pol = @Pol
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END
GO


