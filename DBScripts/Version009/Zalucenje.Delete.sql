USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Zalucenje].[Delete]    Script Date: 3/14/2022 12:21:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Zalucenje].[Delete]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @KrmacaId AS INT = (SELECT TOP 1 z.KrmacaId FROM Zaluc z WHERE z.Id = @Id);
	


	DELETE FROM dbo.Zaluc
    WHERE Id = @Id
	
	DECLARE @LastPripustDate AS DATE = ISNULL((SELECT MAX(DAT_PRIP) FROM Pripust WHERE KrmacaId = @KrmacaId),'1980-01-01');
	DECLARE @LastPrasenjeDate AS DATE = ISNULL((SELECT MAX(P.DAT_PRAS) FROM Prasenje p WHERE P.KrmacaId = @KrmacaId),'1980-01-01');
	DECLARE @LastZalucDate AS DATE = ISNULL((SELECT MAX(z.DAT_ZAL) FROM Zaluc z WHERE z.KrmacaId = @KrmacaId),'1980-01-01');

	DECLARE @Faza AS INT = CASE 
							WHEN  @LastPripustDate > @LastPrasenjeDate AND @LastPripustDate > @LastZalucDate THEN  1
							WHEN @LastPrasenjeDate > @LastPripustDate AND @LastPrasenjeDate > @LastZalucDate THEN  2
							WHEN @LastZalucDate > @LastPrasenjeDate AND @LastZalucDate > @LastPripustDate THEN  3
							END
    UPDATE Krmaca SET FAZA = @Faza WHERE Id = @KrmacaId
END
