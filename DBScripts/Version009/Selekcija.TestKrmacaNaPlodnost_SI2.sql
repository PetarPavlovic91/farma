USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Selekcija].[TestKrmacaNaPlodnost_SI2]    Script Date: 4/11/2022 7:34:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create schema Selekcija 
go




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[TestKrmacaNaPlodnost_SI2]
	-- Add the parameters for the stored procedure here
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB,
MAX(k.T_KRM) as Krmaca,
MAX(k.ODG) as Odg,
MAX(k.RASA) as Rasa,
MAX(k.MARKICA) as Markica,
MAX(k.PARIT) as Parit,
SUM(ISNULL(p.ZIVO,0)) as Zivo,
SUM(ISNULL(p.MRTVO,0)) as Mrtvo,
SUM(ISNULL(z.ZALUC,0)) as Zaluc,
SUM(ISNULL(p.ZIVO,0))/ count(*) as Pr_zivo,
SUM(ISNULL(p.MRTVO,0))/ count(*) as Pr_mrtvo,
case when SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) = 0 then null else SUM(ISNULL(z.ZALUC,0))/SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) end as Pr_zal,
MAX(k.IEZ) as lez,
MAX(k.si2) as Si2
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Skart s on s.KrmacaId = k.Id
where s.Id is null and si2 > 0 and k.RASA like '%'+@Rasa+'%'
group by k.Id

END
GO


