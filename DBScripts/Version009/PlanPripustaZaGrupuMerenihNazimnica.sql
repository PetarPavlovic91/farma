USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PlanPripustaZaGrupuMerenihNazimnica]    Script Date: 3/20/2022 12:36:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PlanPripustaZaGrupuMerenihNazimnica]
	-- Add the parameters for the stored procedure here
	@datumTesta as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + n.T_NER + ']',
    '[' + n.T_NER + ']'
  )
from (
select distinct n.T_NER
FROM Odabir t 
cross join 
(
select n1.*
from Nerast n1
left join Skart s on s.NerastId = n1.Id
where s.Id is null
) as n
where t.D_ODABIRA >= @datumTesta and t.D_ODABIRA <= DATEADD(day, 1, @datumTesta) and t.POL = 'Z') n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select 
	t.T_TEST, 
	t.RASA, 
	n.T_NER,
	CASE
		WHEN p.MB_OCA = n.MBR_NER
		THEN ''-''
		WHEN p.MBM_DEDE = n.MBR_NER
		THEN ''-''
		WHEN p.MBMB_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBMD_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBO_DEDE = n.MBR_NER
		THEN ''-''
		WHEN p.MBOB_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBOD_PRAD = n.MBR_NER
		THEN ''-''
		ELSE ''+''
	END as Ukrstanje
	FROM Odabir t 
	join Pedigre p on t.MBR_TEST = p.MBR_GRLA
	cross join 
	(
	select n1.*
	from Nerast n1
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where t.D_ODABIRA >= '''+ CONVERT(NVARCHAR(MAX), @datumTesta)  +''' 
			and t.D_ODABIRA <=  ''' +  CONVERT(NVARCHAR(MAX), DATEADD(day, 1, @datumTesta)) +''' and t.POL = ''Z'') as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'


EXECUTE(@PivotTableSQL)

END
GO


