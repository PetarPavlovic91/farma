USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[GetAllByDate]    Script Date: 4/7/2022 11:10:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE proc_Sifrarnik
	-- Add the parameters for the stored procedure here
	@Grupa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
SIFRA,
NAZIV
from SifreF
where SIFARNIK = @Grupa
order by SIFRA
 


END
GO


