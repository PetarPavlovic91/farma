USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[DopunaBrojaSisaKodPrasadiSave]    Script Date: 4/6/2022 10:44:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DopunaBrojaSisaKodPrasadiSave]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@S_l AS nvarchar(max) = '',
	@S_d AS nvarchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


		UPDATE Prase
		SET
		S_L = @S_l,
		S_D = @S_d
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END
GO


