USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[AktivneKrmaceNaDan]    Script Date: 4/6/2022 8:52:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AktivneKrmaceNaDan]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@rasa as Nvarchar(max),
	@IsNazimica as Bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if	@IsNazimica = 0
	begin
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,k.TETOVIRK10 as Krmaca
,Rasa
,Dat_rod 
,Mbr_krm 
,Markica 
,k.HBBROJ as HB_Broj 
,k.RBBROJ as RB_Broj 
,Klasa 
,'' as Obj
,'' as Box
,Parit 
,Uk_Zivo 
,Uk_Mrtvo
,k.UK_ZAL Uk_Zaluc
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%'
 END

 ELSE

 BEGIN
 select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,k.TETOVIRK10 as Krmaca
,Rasa
,Dat_rod 
,Mbr_krm 
,Markica 
,k.HBBROJ as HB_Broj 
,k.RBBROJ as RB_Broj 
,Klasa 
,'' as Obj
,'' as Box
,Parit 
,Uk_Zivo 
,Uk_Mrtvo
,k.UK_ZAL Uk_Zaluc
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and k.CIKLUS = 1
 END


END
