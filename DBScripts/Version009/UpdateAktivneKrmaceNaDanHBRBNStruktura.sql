USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[AktivneKrmaceNaDanHBRBNStruktura]    Script Date: 4/6/2022 8:55:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AktivneKrmaceNaDanHBRBNStruktura]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@rasa as Nvarchar(max),
	@IsNazimica as Bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
 declare @allRows as int;
 
If(OBJECT_ID('tempdb..#TempAktivneKrmaceNaDanHBRBNStruktura') Is Not Null)
Begin
    Drop Table #TempAktivneKrmaceNaDanHBRBNStruktura
End

create table #TempAktivneKrmaceNaDanHBRBNStruktura
(
    Tip nvarchar(max),
	Broj int,
	Procenat decimal
)



		if	@IsNazimica = 0
	begin

	 set @allRows = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%');


insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'HB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and HBBROJ is not null

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'RB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and RBBROJ is not null

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'NB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and NBBROJ is not null

select *
from #TempAktivneKrmaceNaDanHBRBNStruktura

END

 ELSE

 BEGIN
	 set @allRows = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and k.CIKLUS = 1);


insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'HB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and HBBROJ is not null and k.CIKLUS = 1

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'RB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and RBBROJ is not null and k.CIKLUS = 1

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'NB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and NBBROJ is not null and k.CIKLUS = 1

select *
from #TempAktivneKrmaceNaDanHBRBNStruktura
 
END
END