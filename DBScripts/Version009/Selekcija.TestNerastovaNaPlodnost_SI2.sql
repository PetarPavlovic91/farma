USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Selekcija].[TestKrmacaNaPlodnost_SI2]    Script Date: 4/13/2022 9:56:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[TestNerastovaNaPlodnost_SI2]
	-- Add the parameters for the stored procedure here
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB,
MAX(n.T_NER) as Nerast,
MAX(n.ODG) as Odg,
MAX(n.RASA) as Rasa,
MAX(n.MARKICA) as Markica,
SUM(ISNULL(p.ZIVO,0)) as Zivo,
SUM(ISNULL(p.MRTVO,0)) as Mrtvo,
SUM(ISNULL(z.ZALUC,0)) as Zaluc,
SUM(ISNULL(p.ZIVO,0))/ count(*) as Pr_zivo,
SUM(ISNULL(p.MRTVO,0))/ count(*) as Pr_mrtvo,
case when SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) = 0 then null else SUM(ISNULL(z.ZALUC,0))/SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) end as Pr_zal,
MAX(n.si2) as Si2
from Krmaca k
join Pripust pr on pr.KrmacaId = k.Id
join Nerast n on n.Id = pr.NerastId
join Prasenje p on p.KrmacaId = k.Id and p.PARIT = pr.CIKLUS
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Skart s on s.NerastId = n.Id
where s.Id is null and n.SI2 > 0 and n.RASA like '%'+@Rasa+'%'
group by k.Id

END
GO


