USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[AnalizaPripusta_Nedelje]    Script Date: 4/12/2022 9:52:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[AnalizaPripusta_Nedelje]
	-- Add the parameters for the stored procedure here
	@DanOd as Date,
	@DanDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
DATEPART(week, p.DAT_PRIP) as Nedelja,
COUNT(p.Id) as Br_os,
sum(case when p.CIKLUS > 1 or pr.Id is not null then 1 else 0 end) as Os_k,
sum(case when p.CIKLUS < 2 and pr.Id is null then 1 else 0 end) as Os_n,
sum(case when p.RBP > 1 then 1 else 0 end) as Povadjala,
sum(case when isnull(pob.Id,0) <> 0 then 1 else 0 end) as Pobacaj,
sum(case when pr.Id is null then 1 else 0 end) as Neopraseno,
case when COUNT(p.Id) = 0 then 0 else 100 * sum(case when pr.Id is null then 1 else 0 end) / COUNT(p.Id) end Pr_neop,
sum(case when pr.Id is not null then 1 else 0 end) as Opraseno,
case when COUNT(p.Id) = 0 then 0 else 100 * sum(case when pr.Id is not null then 1 else 0 end) / COUNT(p.Id) end Pr_opr,
case when COUNT(p.Id) = 0 then 0 else sum(isnull(pr.Zivo,0)) / COUNT(p.Id) end Zivo,
case when COUNT(p.Id) = 0 then 0 else sum(isnull(pr.Mrtvo,0)) / COUNT(p.Id) end Mrtvo,
sum(case when z.Id is not null then 1 else 0 end) as Zaluceno,
case when sum(case when z.Id is not null then 1 else 0 end) = 0 then 0 else sum(isnull(z.ZALUC,0)) / sum(case when z.Id is not null then 1 else 0 end) end Zaluc,
(DATEPART(week, p.DAT_PRIP) - 1) * 7 as D1,
DATEPART(week, p.DAT_PRIP) * 7 as D2
from Pripust p
join Nerast n on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
left join Prasenje pr on pr.KrmacaId = p.KrmacaId and p.CIKLUS = pr.PARIT
left join Zaluc z on z.KrmacaId = p.KrmacaId and p.CIKLUS = z.PARIT
left join Pobacaj pob on pob.KrmacaId = p.KrmacaId and pob.CIKLUS = p.CIKLUS
where p.DAT_PRIP >= @DanOd and p.DAT_PRIP <= @DanDo
GROUP BY DATEPART(week, p.DAT_PRIP)
ORDER BY DATEPART(week, p.DAT_PRIP);



END
GO


