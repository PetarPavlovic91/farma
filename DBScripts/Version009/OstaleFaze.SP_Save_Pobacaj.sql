USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [OstaleFaze].[SP_Save_Hormon]    Script Date: 4/11/2022 8:01:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [OstaleFaze].[SP_Save_Pobacaj]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@DatumPobacaja AS DATE,
	@Krmaca as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @KrmacaId AS INT = (SELECT TOP 1 k.Id FROM Krmaca k WHERE k.T_KRM = @Krmaca);

	
	
    INSERT INTO Pobacaj
	(
	   DAT_POBAC
	   ,KrmacaId
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@DatumPobacaja, @KrmacaId, GETDATE(),GETDATE());

END

END
GO


