USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PromenaPolaPrasadiGet]    Script Date: 4/6/2022 10:21:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PromenaPolaPrasadiGet]
	-- Add the parameters for the stored procedure here
	@danRodjenjaOd as Date,
	@danRodjenjaDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
p.Id,
T_PRAS as T_pras,
POL as Pol,
p.Dat_rod,
n.MBR_NER as Mbr_oca,
k.MBR_KRM as Mbr_majke,
p.IZ_LEGLA as Iz_legla,
p.PRIPLOD as Priplod,
p.Mbr_pras
from Prase p
left join Nerast n on p.NerastId = n.Id
left join Krmaca k on p.KrmacaId = k.Id
where p.DAT_ROD >= @danRodjenjaOd and p.DAT_ROD <= @danRodjenjaDo 
end
GO


