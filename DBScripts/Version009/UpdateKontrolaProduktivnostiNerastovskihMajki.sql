USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[KontrolaProduktivnostiNerastovskihMajki]    Script Date: 4/4/2022 8:54:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[KontrolaProduktivnostiNerastovskihMajki]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
nerMajka.RASA as Genotip, 
count(distinct nerMajka.Id) Br_krm,
count(*) Br_leg,
SUM(p.ZIVO)/ count(*) as Zivo,
AVG(DATEDIFF(day, z1.DAT_ZAL, pr.DAT_PRIP)) as Lakt,
SUM(z.ZALUC)/SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) as Zal,
SUM(z.TEZ_ZAL)/SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) as Tez_l
from   (select k.Id, k.RASA
		from Krmaca k
		where k.KLASA = 'E') nerMajka
join Pripust pr on pr.KrmacaId = nerMajka.Id
join Prasenje p on p.KrmacaId = nerMajka.Id and p.DAT_PRAS >= @dateTimeFrom and p.DAT_PRAS <= @dateTimeTo and pr.CIKLUS = p.PARIT
left join Zaluc z on z.KrmacaId = nerMajka.Id and z.PARIT = p.PARIT
left join Zaluc z1 on z1.KrmacaId = nerMajka.Id and z1.PARIT = p.PARIT - 1
group by nerMajka.RASA

END
