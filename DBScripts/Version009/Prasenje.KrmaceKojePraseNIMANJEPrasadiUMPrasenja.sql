USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Prasenje].[KrmaceKojePraseNIMANJEPrasadiUMPrasenja]    Script Date: 3/20/2022 2:07:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Prasenje].[KrmaceKojePraseNIMANJEPrasadiUMPrasenja]
	-- Add the parameters for the stored procedure here
	@Paritet as Int, 
	@Zivo as Int,
	@Rasa as Nvarchar(MAX),
	@Prvih as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @PivotColumnHeaders NVARCHAR(MAX)
DECLARE @PivotTableSQL NVARCHAR(MAX)


If(OBJECT_ID('tempdb..#PARITETT') Is Not Null)
Begin
    Drop Table #PARITETT
End

create table #PARITETT
(
    PARITET int
)

If(OBJECT_ID('tempdb..#PARITETTable') Is Not Null)
Begin
    Drop Table #PARITETTable
End

create table #PARITETTable
(
    PARITET int
)


if @Prvih = 1

begin

insert into #PARITETT
select distinct p.PARIT
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where s.Id is null and p.PARIT <= @Paritet and p.ZIVO <= @Zivo and k.RASA like '%' + @Rasa +'%'
order by p.PARIT

insert into #PARITETTable
select TOP (@Paritet) *
FROM #PARITETT

SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + CONVERT(NVARCHAR(MAX), p.PARIT) + ']',
    '[' + CONVERT(NVARCHAR(MAX), p.PARIT) + ']'
  )
from (
select PARITET as PARIT
FROM #PARITETTable) p
order by p.PARIT


SET @PivotTableSQL = N'
select *
from
   (
select k.T_KRM as Krmaca, k.RASA, k.PARIT, p.PARIT as Prasenje, p.ZIVO
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
cross join (
select DISTINCT k.T_KRM as Krmaca
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where s.Id is null and p.PARIT <= ''' + CONVERT(NVARCHAR(MAX),  @Paritet) +''' and p.ZIVO  <= ''' + CONVERT(NVARCHAR(MAX), @Zivo) +''' and k.RASA like ''%'' + ''' + @Rasa +''' +''%''
) dt
where s.Id is null and p.PARIT <= ''' + CONVERT(NVARCHAR(MAX),  @Paritet) +''' and k.RASA like ''%'' + ''' + @Rasa +''' +''%'') as DataTable
PIVOT
(
MAX(ZIVO)
FOR Prasenje
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
order by Krmaca
'


EXECUTE(@PivotTableSQL)

END

ELSE
BEGIN

insert into #PARITETT
select distinct p.PARIT
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where s.Id is null and p.PARIT > k.PARIT - @Paritet and p.ZIVO <= @Zivo and k.RASA like '%' + @Rasa +'%'
order by p.PARIT

insert into #PARITETTable
select TOP (@Paritet) *
FROM #PARITETT

SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + CONVERT(NVARCHAR(MAX), p.PARIT) + ']',
    '[' + CONVERT(NVARCHAR(MAX), p.PARIT) + ']'
  )
from (
select PARITET as PARIT
FROM #PARITETTable) p
order by p.PARIT


SET @PivotTableSQL = N'
select *
from
   (
select k.T_KRM as Krmaca, k.RASA, k.PARIT, p.PARIT as Prasenje, p.ZIVO
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
cross join (
select DISTINCT k.T_KRM as Krmaca
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where s.Id is null and p.PARIT > k.PARIT - ''' + CONVERT(NVARCHAR(MAX),  @Paritet) +''' and p.ZIVO  <= ''' + CONVERT(NVARCHAR(MAX), @Zivo) +''' and k.RASA like ''%'' + ''' + @Rasa +''' +''%''
) dt
where s.Id is null and p.PARIT > k.PARIT - ''' + CONVERT(NVARCHAR(MAX),  @Paritet) +''' and k.RASA like ''%'' + ''' + @Rasa +''' +''%'') as DataTable
PIVOT
(
MAX(ZIVO)
FOR Prasenje
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
order by Krmaca
'


EXECUTE(@PivotTableSQL)

END

END
GO


