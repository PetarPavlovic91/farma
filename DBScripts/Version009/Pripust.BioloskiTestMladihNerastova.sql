USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[BioloskiTestMladihNerastova]    Script Date: 4/12/2022 9:51:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[BioloskiTestMladihNerastova]
	-- Add the parameters for the stored procedure here
	@DanOd as Date,
	@DanDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB,
n.MBR_NER, 
k.T_Krm,
k.RASA as Rasak, 
p.RBP,
pr.DAT_PRAS,
pr.Zivo,
pr.Mrtvo,
isnull(pr.ZIVO,0) + isnull(pr.MRTVO,0) as Ukupno,
0 as Hc,
0 as Aa,
0 as Rn,
0 as Lp,
0 as Rp,
0 as Av,
0 as Hr,
0 as Cr,
0 as He
from Pripust p
join Nerast n on p.NerastId = n.Id
left join Skart s on n.Id = s.NerastId
join Prasenje pr on pr.KrmacaId = p.KrmacaId and p.CIKLUS = pr.PARIT
join Krmaca k on pr.KrmacaId = k.Id
where s.Id is NULL and pr.DAT_PRAS >= @DanOd and pr.DAT_PRAS <= @DanDo



END
GO


