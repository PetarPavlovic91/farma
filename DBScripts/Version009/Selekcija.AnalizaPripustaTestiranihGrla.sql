USE [FarmaProduction]
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[AnalizaPripustaTestiranihGrla]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
max(t.D_TESTA) as D_testa,
max(t.T_TEST) as Tetovir,
max(t.RASA) as Rasa,
max(t.DAT_ROD) as Dat_rod,
max(t.T_OCA10) as Otac,
max(t.T_MAJKE10) as Majka,
datediff(day,max(t.DAT_ROD),getdate()) as Starost,
max(t.TEZINA) as Tezina,
max(t.PRIRAST) as Prirast,
max(t.si1) as [Index],
max(s.D_SKART) as D_skarta,
max(s.RAZL_SK) as Razl,
max(h.DatumDavanjaHormona) as D_hormon,
max(h.TipHormona) as Vh,
max(p.DAT_PRIP) as Dat_prip,
max(n.T_NER) as Nerast,
max(n.RASA) as Rasan,
max(p.RBP) as Rbp,
max(p.PV) as Pv,
max(p.OSEM) as Osem,
max(pr.DAT_PRAS) as DAT_PRAS,
max(pr.ZIVO) as Zivo,
max(pr.MRTVO) as Mrtvo,
max(pr.GAJI) as Gaji,
max(pr.ZIVO) as Zivo,
max(z.DAT_ZAL) as Dat_zal,
max(z.ZALUC) as Zaluc,
max(z.TEZ_ZAL) as Tez_zal,
max(t.MBR_TEST) as Mbr_testa
from Test t
left join Krmaca k on k.MBR_KRM = t.MBR_TEST
left join Skart s on s.TestId = t.Id
left join Hormons h on k.Id = h.KrmacaId
left join Pripust p on p.KrmacaId = k.Id
left join Nerast n on n.Id = p.NerastId
left join Prasenje pr on pr.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id
where (k.Id is null or k.CIKLUS = 1) and t.D_TESTA >= @dateTimeFrom and t.D_TESTA <= @dateTimeTo
group by t.Id
 
END
GO


