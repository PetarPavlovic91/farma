USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Selekcija].[AnalizaPripustaTestiranihGrla]    Script Date: 4/17/2022 8:57:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[AnalizaPerformansTestaPoRasama]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date,
	@IsKrmaca as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @IsKrmaca = 1

begin

select 
count(t.Id) as Testirano,
AVG(DATEDIFF(day, t.DAT_ROD,getdate())) as Starost,
AVG(t.TEZINA) as Tezina,
AVG(t.PRIRAST) as Prirast,
AVG(t.L_SL) as L_sl,
AVG(t.B_SL) as B_sl,
AVG(t.DUB_S) as Dub_s,
AVG(t.DUB_M) as Mld,
AVG(t.PROC_M) as P_mesa,
AVG(t.SI1) as Si1,
SUM(case when p.Id is not null then 1 else 0 end) as Osem,
case when count(t.Id) = 0 then 0 else SUM(case when p.Id is not null then 1 else 0 end) / count(t.Id) end as Pr_osem
from Test t
left join Krmaca k on k.MBR_KRM = t.MBR_TEST
left join Pripust p on p.KrmacaId = k.Id
where t.POL = 'Z' and (k.Id is null or k.CIKLUS = 1) and t.D_TESTA >= @dateTimeFrom and t.D_TESTA <= @dateTimeTo
group by t.RASA
 
 END
 ELSE
 BEGIN

 select 
count(t.Id) as Testirano,
AVG(DATEDIFF(day, t.DAT_ROD,getdate())) as Starost,
AVG(t.TEZINA) as Tezina,
AVG(t.PRIRAST) as Prirast,
AVG(t.L_SL) as L_sl,
AVG(t.B_SL) as B_sl,
AVG(t.DUB_S) as Dub_s,
AVG(t.DUB_M) as Mld,
AVG(t.PROC_M) as P_mesa,
AVG(t.SI1) as Si1,
NULL as Osem,
NULL as Pr_osem
from Test t
left join Krmaca k on k.MBR_KRM = t.MBR_TEST
left join Pripust p on p.KrmacaId = k.Id
where t.POL = 'M' and (k.Id is null or k.CIKLUS = 1) and t.D_TESTA >= @dateTimeFrom and t.D_TESTA <= @dateTimeTo
group by t.RASA

 END

END
GO


