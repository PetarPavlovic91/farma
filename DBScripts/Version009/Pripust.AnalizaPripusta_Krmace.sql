USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[AnalizaPripusta_Krmace]    Script Date: 4/12/2022 9:52:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[AnalizaPripusta_Krmace]
	-- Add the parameters for the stored procedure here
	@DanOd as Date,
	@DanDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red,
p.DAT_PRIP, 
0 as Nedelja,
p.RBP,
k.T_KRM,
COALESCE(k.HBBROJ,k.RBBROJ,k.NBBROJ) as Hb_Rb_N, 
k.MARKICA,
k.RASA as Rasak,
p.CIKLUS,
n.T_NER,
n.RASA as Rasan,
NULL as Dat_povad,
pob.DAT_POBAC,
NULL as Jalovost,
NULL as Uginuce,
NULL as Prodaja,
NULL as Ostalo,
pr.DAT_PRAS,
pr.ZIVO,
pr.MRTVO,
z.DAT_ZAL,
z.TEZ_ZAL,
p.PRIPUSNICA,
p.PRIMEDBA,
k.MBR_KRM
from Pripust p
join Nerast n on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
left join Prasenje pr on pr.KrmacaId = p.KrmacaId and p.CIKLUS = pr.PARIT
left join Zaluc z on z.KrmacaId = p.KrmacaId and p.CIKLUS = z.PARIT
left join Pobacaj pob on pob.KrmacaId = p.KrmacaId and pob.CIKLUS = p.CIKLUS
where p.DAT_PRIP >= @DanOd and p.DAT_PRIP <= @DanDo



END
GO


