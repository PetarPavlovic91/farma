USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Selekcija].[AnalizaPripustaTestiranihGrla]    Script Date: 4/17/2022 8:57:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[AnalizaPerformansTestaPoOcu]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date,
	@IsKrmaca as Bit,
	@Otac as nvarchar(max),
	@Rasa as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @IsKrmaca = 1

begin

select 
t.T_TEST as Tetovir,
t.RASA as Rasa,
t.T_MAJKE10 as Majka,
t.D_TESTA as DatumTesta,
DATEDIFF(day, t.DAT_ROD,getdate()) as Starost,
t.TEZINA as Tezina,
t.PRIRAST as Prirast,
t.L_SL as L_sl,
t.B_SL as B_sl,
t.DUB_S as Dub_s,
t.DUB_M as Mld,
t.PROC_M as P_mesa,
t.SI1 as Si1,
case when p.Id is not null then 'DA' else '' end as Priplod
from Test t
left join Krmaca k on k.MBR_KRM = t.MBR_TEST
left join Pripust p on p.KrmacaId = k.Id
where t.T_OCA10 like '%' + @Otac + '%' and t.RASA like '%' + @Rasa + '%' and  t.POL = 'Z' and (k.Id is null or k.CIKLUS = 1) and t.D_TESTA >= @dateTimeFrom and t.D_TESTA <= @dateTimeTo
 
 END
 ELSE
 BEGIN

 select 
t.T_TEST as Tetovir,
t.RASA as Rasa,
t.T_MAJKE10 as Majka,
t.D_TESTA as DatumTesta,
DATEDIFF(day, t.DAT_ROD,getdate()) as Starost,
t.TEZINA as Tezina,
t.PRIRAST as Prirast,
t.L_SL as L_sl,
t.B_SL as B_sl,
t.DUB_S as Dub_s,
t.DUB_M as Mld,
t.PROC_M as P_mesa,
t.SI1 as Si1,
case when p.Id is not null then 'DA' else '' end as Priplod
from Test t
left join Krmaca k on k.MBR_KRM = t.MBR_TEST
left join Pripust p on p.KrmacaId = k.Id
where t.T_OCA10 like '%' + @Otac + '%' and t.RASA like '%' + @Rasa + '%' and t.POL = 'M' and (k.Id is null or k.CIKLUS = 1) and t.D_TESTA >= @dateTimeFrom and t.D_TESTA <= @dateTimeTo

 END

END
GO


