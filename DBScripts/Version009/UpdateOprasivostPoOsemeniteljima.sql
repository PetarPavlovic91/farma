USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[OprasivostPoOsemeniteljima]    Script Date: 3/16/2022 5:07:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[OprasivostPoOsemeniteljima]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
If(OBJECT_ID('tempdb..#Os_nT') Is Not Null)
Begin
    Drop Table #Os_nT
End

If(OBJECT_ID('tempdb..#Os_kT') Is Not Null)
Begin
    Drop Table #Os_kT
End

If(OBJECT_ID('tempdb..#PobT') Is Not Null)
Begin
    Drop Table #PobT
End

If(OBJECT_ID('tempdb..#OprasenoT') Is Not Null)
Begin
    Drop Table #OprasenoT
End

If(OBJECT_ID('tempdb..#SkartT') Is Not Null)
Begin
    Drop Table #SkartT
End

If(OBJECT_ID('tempdb..#Op_nT') Is Not Null)
Begin
    Drop Table #Op_nT
End

If(OBJECT_ID('tempdb..#Op_kT') Is Not Null)
Begin
    Drop Table #Op_kT
End

If(OBJECT_ID('tempdb..#PovadT') Is Not Null)
Begin
    Drop Table #PovadT
End

create table #Os_nT
(
    OSEM int,
	Os_n int
)

insert into #Os_nT
select p.OSEM, count(*) as Os_n
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by p.OSEM


create table #Os_kT
(
    OSEM int,
	Os_k int
)

insert into #Os_kT
select p.OSEM, count(*) as Os_k
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by p.OSEM



create table #PobT
(
    OSEM int,
	Pob int
)

insert into #PobT
select p.OSEM, count(*) as Pob
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Pobacaj pob on pob.KrmacaId = k.Id and pob.CIKLUS = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #OprasenoT
(
    OSEM int,
	Opraseno int,
	Zivo int,
	Mrtvo int
)

insert into #OprasenoT
select p.OSEM, count(*) as Opraseno, SUM(pr.ZIVO) as Zivo, SUM(pr.MRTVO) as Mrtvo
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #SkartT
(
    OSEM int,
	Skart int
)

insert into #SkartT
select p.OSEM, count(*) as Skart
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #Op_nT
(
    OSEM int,
	Op_n int
)

insert into #Op_nT
select p.OSEM, count(*) as Op_n
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by p.OSEM


create table #Op_kT
(
    OSEM int,
	Op_k int
)

insert into #Op_kT
select p.OSEM, count(*) as Op_k
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by p.OSEM


create table #PovadT
(
    OSEM int,
	Povad int
)

insert into #PovadT
select p.OSEM, count(*) as Povad
from  Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.RAZL_P = '2'
group by p.OSEM

select 
COALESCE(osk.OSEM, osn.OSEM) as Osem,
osk.Os_k,
osn.Os_n,
isnull(osk.Os_k,0) + isnull(osn.Os_n,0) as Uk_os,
pov.Povad as Povad,
isnull(osk.Os_k,0) + isnull(osn.Os_n,0) - isnull(o.Opraseno,0) as Ne_op,
s.Skart as Isklj,
pob.Pob as Pobac,
0 as Prod,
opn.Op_n,
opk.Op_k,
isnull(opn.Op_n,0) + isnull(opk.Op_k,0) as Uk_opr,
case when isnull(osn.Os_n,0) = 0 then 0 else isnull(opn.Op_n,0) / isnull(osn.Os_n,0) * 100 end as P_op_n,
case when isnull(osk.Os_k,0) = 0 then 0 else isnull(opk.Op_k,0) / isnull(osk.Os_k,0) * 100 end as P_op_k,
case when isnull(osk.Os_k,0) + isnull(osn.Os_n,0) = 0 then 0 else (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) / (isnull(osk.Os_k,0) + isnull(osn.Os_n,0)) * 100 end as P_op_uk,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else isnull(o.Zivo,0) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Zivo,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else isnull(o.Mrtvo,0) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Mrtvo,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else (isnull(o.Zivo,0) + isnull(o.Mrtvo,0)) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Uk_p
from #Os_kT osk
full join #Os_nT osn on osn.OSEM = osk.OSEM
left join #PobT pob on pob.OSEM = osk.OSEM or pob.OSEM = osn.OSEM
left join #SkartT s on s.OSEM = osk.OSEM or s.OSEM = osn.OSEM
left join #OprasenoT o on o.OSEM = osk.OSEM or o.OSEM = osn.OSEM
left join #Op_nT opn on opn.OSEM = osk.OSEM or opn.OSEM = osn.OSEM
left join #Op_kT opk on opk.OSEM = osk.OSEM or opk.OSEM = osn.OSEM
left join #PovadT pov on pov.OSEM = osk.OSEM or pov.OSEM = osn.OSEM


END
