USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[Save]    Script Date: 4/12/2022 11:18:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[SP_Save_Odabir]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@Tezina AS INT,
	@Prirast AS INT,
	@IzLegla AS NVARCHAR(MAX) = NULL,
	@Ekst AS NVARCHAR(MAX) = NULL,
	@Markica AS NVARCHAR(MAX) = NULL,
	@Sisa AS NVARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		UPDATE Odabir
		SET
		TEZINA = @Tezina,
		PRIRAST = @Prirast,
		IZ_LEGLA = @IzLegla,
		EKST = @Ekst,
		MARKICA = @Markica,
		BR_SISA = @Sisa,
		DateModified = GETDATE()
		WHERE Id = @Id

END
GO


