USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[DevniPlanPripusta]    Script Date: 3/20/2022 12:32:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DevniPlanPripusta]
	-- Add the parameters for the stored procedure here
	@datumZalucenja as Date,
	@t_KrmParam as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + T_NER + ']',
    '[' + T_NER + ']'
  )
  FROM
  (
	select distinct n1.T_NER
	from Nerast n1
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select 
	k.T_KRM, 
	k.RASA, 
	n.T_NER,
	CASE
		WHEN p.MB_OCA = n.MBR_NER
		THEN ''-''
		WHEN p.MBM_DEDE = n.MBR_NER
		THEN ''-''
		WHEN p.MBMB_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBMD_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBO_DEDE = n.MBR_NER
		THEN ''-''
		WHEN p.MBOB_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBOD_PRAD = n.MBR_NER
		THEN ''-''
		ELSE ''+''
	END as Ukrstanje
	from Krmaca k
	join Pedigre p on k.Id = p.KrmacaId
	cross join 
	(
	select n1.*
	from Nerast n1
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where k.T_KRM = ''' + @t_KrmParam +''') as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'


EXECUTE(@PivotTableSQL)

END
