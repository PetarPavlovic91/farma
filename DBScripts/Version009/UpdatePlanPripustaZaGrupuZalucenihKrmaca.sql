USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[PlanPripustaZaGrupuZalucenihKrmaca]    Script Date: 3/20/2022 12:32:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PlanPripustaZaGrupuZalucenihKrmaca]
	-- Add the parameters for the stored procedure here
	@datumZalucenja as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + n.T_NER + ']',
    '[' + n.T_NER + ']'
  )
from (
select distinct n.T_NER
FROM Krmaca k
join Zaluc z on z.KrmacaId = k.Id
cross join 
(
select n1.*
from Nerast n1
left join Skart s on s.NerastId = n1.Id
where s.Id is null
) as n
where z.DAT_ZAL >= @datumZalucenja and z.DAT_ZAL <= DATEADD(day, 1, @datumZalucenja)) n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select 
	k.T_KRM, 
	k.RASA, 
	n.T_NER,
	CASE
		WHEN p.MB_OCA = n.MBR_NER
		THEN ''-''
		WHEN p.MBM_DEDE = n.MBR_NER
		THEN ''-''
		WHEN p.MBMB_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBMD_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBO_DEDE = n.MBR_NER
		THEN ''-''
		WHEN p.MBOB_PRAD = n.MBR_NER
		THEN ''-''
		WHEN p.MBOD_PRAD = n.MBR_NER
		THEN ''-''
		ELSE ''+''
	END as Ukrstanje
	from Krmaca k
	join Pedigre p on k.MBR_KRM = p.MBR_GRLA
	join Zaluc z on z.KrmacaId = k.Id
	cross join 
	(
	select n1.*
	from Nerast n1
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where z.DAT_ZAL >= '''+ CONVERT(NVARCHAR(MAX), @datumZalucenja)  +''' 
			and z.DAT_ZAL <=  ''' +  CONVERT(NVARCHAR(MAX), DATEADD(day, 1, @datumZalucenja)) +''') as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'



EXECUTE(@PivotTableSQL)

END
