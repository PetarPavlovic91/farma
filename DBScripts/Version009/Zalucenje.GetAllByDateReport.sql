USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[GetAllByDateReport]    Script Date: 3/7/2022 12:07:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Zalucenje].[GetAllByDateReport]
	-- Add the parameters for the stored procedure here
	@danOd as DATE,
	@danDo as DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,z.DAT_ZAL
,k.T_KRM
,NULL AS Slovo
,NULL AS Broj
,k.RASA AS K_RASA
,COALESCE(k.HBBROJ,k.RBBROJ,k.NBBROJ) AS K_HB_RB_N
,z.ZALUC
,z.TEZ_ZAL
,k.VLA
from Zaluc z
join Krmaca k on k.Id = z.KrmacaId
where z.DAT_ZAL >= @danOd and z.DAT_ZAL <= @danDo
 


END
GO


