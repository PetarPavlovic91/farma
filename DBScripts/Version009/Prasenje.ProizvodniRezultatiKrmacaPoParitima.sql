USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Prasenje].[ProizvodniRezultatiKrmacaPoParitima]    Script Date: 3/19/2022 11:24:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Prasenje].[ProizvodniRezultatiKrmacaPoParitima]
	-- Add the parameters for the stored procedure here
	@Rasa as Nvarchar(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
declare @BrKrmacaUkupno as Int = (select count(Id) from Krmaca where RASA LIKE '%' + @Rasa + '%');

SELECT 
pr.PARIT,
count(distinct k.Id) as Br_krm, 
(count(distinct k.Id) * 100.0) / @BrKrmacaUkupno as Procenat,
count(k.Id) as Br_legla,
avg(isnull(k.si1,0)) as SI1,
avg(isnull(k.si2,0)) as SI2,
avg(isnull(k.si3,0)) as SI3,
avg(isnull(pr.ZIVO,0)) as ZIVO,
avg(isnull(pr.MRTVO,0)) as MRTVO,
avg(isnull(pr.ZIVO,0)) + avg(isnull(pr.MRTVO,0)) as Ukupno,
avg(isnull(z.ZALUC,0)) as ZALUC,
avg(datediff(day, pr.DAT_PRAS, z.DAT_ZAL)) as Lakt,
avg(datediff(day, z1.DAT_ZAL, p.DAT_PRIP)) as Pazni
from Pripust p
join Nerast n on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = p.KrmacaId and p.CIKLUS = pr.PARIT
left join Zaluc z on z.KrmacaId = p.KrmacaId and p.CIKLUS = z.PARIT
left join Zaluc z1 on z1.KrmacaId = p.KrmacaId and p.CIKLUS - 1 = z1.PARIT
where k.RASA LIKE '%' + @Rasa + '%'
group by pr.PARIT
order by pr.PARIT


END
GO


