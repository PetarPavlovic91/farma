USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[GetAllForKrmaca]    Script Date: 3/9/2022 11:10:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetAllForKrmaca]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,pr.DAT_PRIP AS Dat_prip
,MAX(pr.RBP) AS Rbp
,MAX(n.T_NER) AS Nerast
,MAX(n.RASA) AS Rasa
,MAX(P.PARIT) AS Pr
,MAX(P.DAT_PRAS) AS DAT_PRAS
,MAX(P.ZIVO) AS Zivo
,MAX(P.ZIVOZ) AS Zen
,MAX(P.MRTVO) AS Mrt
,ISNULL(MAX(P.ZIVO),0) + ISNULL(MAX(P.MRTVO),0) AS Ukup
,MAX(P.ANOMALIJE) AS Anomalije
,MAX(P.DODATO) AS Dod
,MAX(P.ODUZETO) AS Odu
,MAX(P.GAJI) AS Gaji
,MAX(z.DAT_ZAL) AS Dat_zal
,MAX(z.ZALUC) AS Zal
,DATEDIFF(day, MAX(P.DAT_PRAS), MAX(z.DAT_ZAL)) AS Lakt
,DATEDIFF(day, MAX(z.DAT_ZAL), MAX(pr1.DAT_PRIP)) AS Pr_d
from Pripust pr 
join Krmaca k on k.Id = pr.KrmacaId
LEFT JOIN Prasenje p ON p.KrmacaId = pr.KrmacaId AND p.PARIT = pr.CIKLUS
LEFT JOIN Zaluc Z ON z.KrmacaId = p.KrmacaId AND z.PARIT = p.PARIT
LEFT JOIN Nerast n ON n.id = pr.NerastId
LEFT JOIN Pripust pr1 ON pr1.KrmacaId = pr.KrmacaId AND pr.CIKLUS + 1 = pr1.CIKLUS
where pr.KrmacaId = @Id
GROUP BY pr.DAT_PRIP
ORDER BY pr.DAT_PRIP
 
END
