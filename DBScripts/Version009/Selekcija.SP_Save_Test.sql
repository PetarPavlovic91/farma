USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Selekcija].[SP_Save_Odabir]    Script Date: 4/13/2022 12:29:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[SP_Save_Test]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@Tezina AS INT,
	@Poc_tez AS INT,
	@Hrana AS INT,
	@L_sl AS INT,
	@B_sl AS INT,
	@D_sl AS INT,
	@D_mis AS INT,
	@Si1 AS INT,
	@Procenat_Mesa AS FLOAT,
	@Iz_legla AS NVARCHAR(MAX) = NULL,
	@Ekst AS NVARCHAR(MAX) = NULL,
	@Markica AS NVARCHAR(MAX) = NULL,
	@Sisa AS NVARCHAR(MAX) = NULL,
	@Napomena AS NVARCHAR(MAX) = NULL,
	@Ulaz_u_test as date = null,
	@U_priplodu_od as date = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		UPDATE Test
		SET
		TEZINA = @Tezina,
		IZ_LEGLA = @Iz_legla,
		EKST = @Ekst,
		MARKICA = @Markica,
		BR_SISA = @Sisa,
		POC_TEZ = @Poc_tez,
		HRANA = @Hrana,
		L_SL = @L_sl,
		B_SL = @B_sl,
		DUB_S = @D_sl,
		DUB_M = @D_mis,
		SI1 = @Si1,
		PROC_M = @Procenat_Mesa,
		NAPOMENA = @Napomena,
		U_TEST = @Ulaz_u_test,
		U_PRIPLOD = @U_priplodu_od,
		DateModified = GETDATE()
		WHERE Id = @Id

END
GO


