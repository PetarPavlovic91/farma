USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [OstaleFaze].[Suprasnost_GetAllByDate]    Script Date: 4/1/2022 4:46:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [OstaleFaze].[Suprasnost_GetAllByDateOdDo]
	-- Add the parameters for the stored procedure here
	@danOd as Date,
	@danDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
s.Id
,k.T_KRM  AS Krmaca
,s.PrvaSuprasnost
,s.DrugaSuprasnost
,s.NijeSuprasna
from Pripust p 
JOIN Krmaca k on k.Id = p.KrmacaId
left join Suprasnosts s on p.Id = s.PripustId
where p.DAT_PRIP >= @danOd and p.DAT_PRIP <= @danDo
 


END
GO


