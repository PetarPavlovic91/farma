USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Prasenje].[DistrobucijaZivoRodjenePrasadiPoPrasenjima]    Script Date: 3/20/2022 12:12:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Prasenje].[DistrobucijaZivoRodjenePrasadiPoPrasenjima]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date,
	@Rasa as Nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + CONVERT(NVARCHAR(MAX), p.PARIT) + ']',
    '[' + CONVERT(NVARCHAR(MAX), p.PARIT) + ']'
  )
from (
select distinct p.PARIT
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
where p.DAT_PRAS >= @dateTimeFrom and p.DAT_PRAS  <= @dateTimeTo and k.RASA like '%' + @Rasa +'%') p
order by p.PARIT


DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (
select p.PARIT, ZIVO, case when p.parit = dt.Paritet then 1 else 0 end as Ukrstanje
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
cross join (
select DISTINCT p.parit as Paritet
FROM Krmaca k
join Prasenje p on p.KrmacaId = k.Id
where p.DAT_PRAS >= ''' + CONVERT(NVARCHAR(MAX), @dateTimeFrom) +''' and p.DAT_PRAS  <= ''' + CONVERT(NVARCHAR(MAX), @dateTimeTo) +''' and k.RASA like ''%'' + ''' + @Rasa +''' +''%''
) dt
where p.DAT_PRAS >= ''' + CONVERT(NVARCHAR(MAX), @dateTimeFrom) +''' and p.DAT_PRAS  <= ''' + CONVERT(NVARCHAR(MAX), @dateTimeTo) +''' and k.RASA like ''%'' + ''' + @Rasa +''' +''%'') as DataTable
PIVOT
(
SUM(Ukrstanje)
FOR PARIT
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
order by ZIVO
'


EXECUTE(@PivotTableSQL)

END
GO


