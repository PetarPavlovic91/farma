USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[OprasivostPoNerastovima]    Script Date: 3/16/2022 5:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[OprasivostPoNerastovima]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
If(OBJECT_ID('tempdb..#Os_nT') Is Not Null)
Begin
    Drop Table #Os_nT
End

If(OBJECT_ID('tempdb..#Os_kT') Is Not Null)
Begin
    Drop Table #Os_kT
End

If(OBJECT_ID('tempdb..#PobT') Is Not Null)
Begin
    Drop Table #PobT
End

If(OBJECT_ID('tempdb..#OprasenoT') Is Not Null)
Begin
    Drop Table #OprasenoT
End

If(OBJECT_ID('tempdb..#SkartT') Is Not Null)
Begin
    Drop Table #SkartT
End

If(OBJECT_ID('tempdb..#Op_nT') Is Not Null)
Begin
    Drop Table #Op_nT
End

If(OBJECT_ID('tempdb..#Op_kT') Is Not Null)
Begin
    Drop Table #Op_kT
End

If(OBJECT_ID('tempdb..#PovadT') Is Not Null)
Begin
    Drop Table #PovadT
End

create table #Os_nT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Os_n int
)

insert into #Os_nT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Os_n
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by n.Id, n.T_NER, n.MBR_NER


create table #Os_kT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Os_k int
)

insert into #Os_kT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Os_k
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by n.Id, n.T_NER, n.MBR_NER



create table #PobT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Pob int
)

insert into #PobT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Pob
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Pobacaj pob on pob.KrmacaId = k.Id and pob.CIKLUS = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.Id, n.T_NER, n.MBR_NER


create table #OprasenoT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Opraseno int,
	Zivo int,
	Mrtvo int
)

insert into #OprasenoT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Opraseno, SUM(pr.ZIVO) as Zivo, SUM(pr.MRTVO) as Mrtvo
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.Id, n.T_NER, n.MBR_NER


create table #SkartT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Skart int
)

insert into #SkartT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Skart
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.Id, n.T_NER, n.MBR_NER


create table #Op_nT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Op_n int
)

insert into #Op_nT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Op_n
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by n.Id, n.T_NER, n.MBR_NER


create table #Op_kT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Op_k int
)

insert into #Op_kT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Op_k
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by n.Id, n.T_NER, n.MBR_NER


create table #PovadT
(
    Id int, 
    T_NER nvarchar(max), 
    MBR_NER nvarchar(max),
	Povad int
)

insert into #PovadT
select n.Id, n.T_NER, n.MBR_NER, count(*) as Povad
from Nerast n
join Pripust p on p.NerastId = n.Id and p.RAZL_P = '2'
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by n.Id, n.T_NER, n.MBR_NER


select 
coalesce(osk.T_NER,osn.T_NER)  as Nerast, 
coalesce(osk.MBR_NER,osn.MBR_NER) as Mbr_ner,
osk.Os_k,
osn.Os_n,
isnull(osk.Os_k,0) + isnull(osn.Os_n,0) as Uk_os,
pov.Povad as Povad,
isnull(osk.Os_k,0) + isnull(osn.Os_n,0) - isnull(o.Opraseno,0) as Ne_op,
s.Skart as Isklj,
pob.Pob as Pobac,
0 as Prod,
opn.Op_n,
opk.Op_k,
isnull(opn.Op_n,0) + isnull(opk.Op_k,0) as Uk_opr,
case when isnull(osn.Os_n,0) = 0 then 0 else isnull(opn.Op_n,0) / isnull(osn.Os_n,0) * 100 end as P_op_n,
case when isnull(osk.Os_k,0) = 0 then 0 else isnull(opk.Op_k,0) / isnull(osk.Os_k,0) * 100 end as P_op_k,
case when isnull(osk.Os_k,0) + isnull(osn.Os_n,0) = 0 then 0 else (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) / (isnull(osk.Os_k,0) + isnull(osn.Os_n,0)) * 100 end as P_op_uk,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else isnull(o.Zivo,0) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Zivo,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else isnull(o.Mrtvo,0) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Mrtvo,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else (isnull(o.Zivo,0) + isnull(o.Mrtvo,0)) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Uk_p
from #Os_kT osk
full join #Os_nT osn on osn.Id = osk.Id 
left join #PobT pob on pob.Id = osk.Id or pob.Id = osn.Id
left join #SkartT s on s.Id = osk.Id or s.Id = osn.Id
left join #OprasenoT o on o.Id = osk.Id or o.Id = osn.Id
left join #Op_nT opn on opn.Id = osk.Id or opn.Id = osn.Id
left join #Op_kT opk on opk.Id = osk.Id or opk.Id = osn.Id
left join #PovadT pov on pov.Id = osk.Id or pov.Id = osn.Id



END
