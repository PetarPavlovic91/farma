USE [FarmaProduction]
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [OstaleFaze].[SP_Save_Uginuce]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@Kategorija AS nvarchar(max),
	@Razlog AS nvarchar(max),
	@Komada AS Int,
	@MestoRodjenja AS nvarchar(max),
	@DatumUginuca AS DATE,
	@Nerast as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @NerastId AS INT = (SELECT TOP 1 n.Id FROM Nerast n WHERE n.T_NER = @Nerast);

	
	
    INSERT INTO Uginucas
	(
	   DatumUginuca
	   ,Kategorija
	   ,Komada
	   ,MestoRodjenja
	   ,Razlog
	   ,NerastId
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@DatumUginuca,@Kategorija,@Komada,@MestoRodjenja, @Razlog, @NerastId, GETDATE(),GETDATE());

END
ELSE BEGIN
		UPDATE Uginucas
		SET
		DatumUginuca = @DatumUginuca
      ,Kategorija = @Kategorija
      ,Komada = @Komada
      ,MestoRodjenja = @MestoRodjenja
      ,Razlog = @Razlog
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END

END
GO


