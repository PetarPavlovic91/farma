USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [OstaleFaze].[Suprasnost_Save]    Script Date: 4/2/2022 7:48:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [OstaleFaze].[SP_Save_Hormon]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@TipHormona AS nvarchar(max),
	@DatumDavanjaHormona AS DATE,
	@Krmaca as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @KrmacaId AS INT = (SELECT TOP 1 k.Id FROM Krmaca k WHERE k.T_KRM = @Krmaca);

	
	
    INSERT INTO Hormons
	(
	   DatumDavanjaHormona
	   ,TipHormona
	   ,KrmacaId
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@DatumDavanjaHormona, @TipHormona, @KrmacaId, GETDATE(),GETDATE());

END
ELSE BEGIN
		UPDATE Hormons
		SET
		TipHormona = @TipHormona
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END

END
GO


