USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Prasenje].[GetAllByDateReport]    Script Date: 3/6/2022 11:26:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Prasenje].[GetAllByDateReport]
	-- Add the parameters for the stored procedure here
	@danOd as DATE,
	@danDo as DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,k.T_KRM AS Tetovir
,k.RASA AS K_RASA
,NULL AS Slovo
,NULL AS Broj
,COALESCE(k.HBBROJ,k.RBBROJ,k.NBBROJ) AS K_HB_RB_N
,p1.DAT_PRIP
,n.T_NER
,n.RASA AS N_Rasa
,COALESCE(n.HBBROJ,n.RBBROJ) as N_HBRBN
,P.DAT_PRAS
,P.PARIT
,P.ZIVO -P.ZIVOZ AS Musko
,P.zivoz AS Zensko
,P.TEZ_LEG
,P.MRTVO
,P.ANOMALIJE
,'092' AS Odgajivac
from Prasenje p
join Krmaca k on k.Id = p.KrmacaId
join Pripust p1 ON p1.KrmacaId = p.KrmacaId AND p.PARIT = p1.CIKLUS
LEFT JOIN Nerast n ON n.Id = p1.NerastId
where p.DAT_PRAS >= @danOd and p.DAT_PRAS <= @danDo
 


END
GO


