USE [FarmaProduction]
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [OstaleFaze].[Uginuca_GetAllByDateOdDo]
	-- Add the parameters for the stored procedure here
	@danOd as Date,
	@danDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
n.T_NER
,n.MBR_NER
,sum(pr.ZIVO) as Opraseno
,sum(pr.UGIN) as Uginulo
,100 * sum(isnull(pr.UGIN,0)) / sum(isnull(pr.ZIVO,0)) as Proc_ugin
from Pripust p 
join Prasenje pr on pr.KrmacaId = p.KrmacaId and p.CIKLUS = pr.PARIT
JOIN Nerast n on n.Id = p.NerastId
where pr.DAT_PRAS >= @danOd and pr.DAT_PRAS <= @danDo
group by n.T_NER, n.MBR_NER
 


END
GO


