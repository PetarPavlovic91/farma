USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Pripust].[Delete]    Script Date: 3/14/2022 12:06:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Pripust].[Delete]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Rbp INT = (SELECT top 1 p.RBP FROM Pripust p WHERE p.Id = @Id);
	DECLARE @KrmacaId INT = (SELECT top 1 p.KrmacaId FROM Pripust p WHERE p.Id = @Id);

	IF @Rbp = 1 BEGIN  
		
		UPDATE Krmaca
		SET CIKLUS = CIKLUS - 1,
		PARIT = (CASE when PARIT = 0 then 0 else PARIT - 1 end)
		WHERE Id = @KrmacaId;
    END
    

	DELETE FROM dbo.Pripust
    WHERE Id = @Id

	
	DECLARE @LastPripustDate AS DATE = ISNULL((SELECT MAX(DAT_PRIP) FROM Pripust WHERE KrmacaId = @KrmacaId),'1980-01-01');
	DECLARE @LastPrasenjeDate AS DATE = ISNULL((SELECT MAX(P.DAT_PRAS) FROM Prasenje p WHERE P.KrmacaId = @KrmacaId),'1980-01-01');
	DECLARE @LastZalucDate AS DATE = ISNULL((SELECT MAX(z.DAT_ZAL) FROM Zaluc z WHERE z.KrmacaId = @KrmacaId),'1980-01-01');

	DECLARE @Faza AS INT = CASE 
							WHEN  @LastPripustDate > @LastPrasenjeDate AND @LastPripustDate > @LastZalucDate THEN  1
							WHEN @LastPrasenjeDate > @LastPripustDate AND @LastPrasenjeDate > @LastZalucDate THEN  2
							WHEN @LastZalucDate > @LastPrasenjeDate AND @LastZalucDate > @LastPripustDate THEN  3
							END

	UPDATE Krmaca
	SET FAZA = @Faza
	WHERE Id = @KrmacaId;
END
