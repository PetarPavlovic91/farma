USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[OprasivostPoRasamaNerastova]    Script Date: 3/16/2022 5:11:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[OprasivostPoRasamaNerastova]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
If(OBJECT_ID('tempdb..#Os_nT') Is Not Null)
Begin
    Drop Table #Os_nT
End

If(OBJECT_ID('tempdb..#Os_kT') Is Not Null)
Begin
    Drop Table #Os_kT
End

If(OBJECT_ID('tempdb..#PobT') Is Not Null)
Begin
    Drop Table #PobT
End

If(OBJECT_ID('tempdb..#OprasenoT') Is Not Null)
Begin
    Drop Table #OprasenoT
End

If(OBJECT_ID('tempdb..#SkartT') Is Not Null)
Begin
    Drop Table #SkartT
End

If(OBJECT_ID('tempdb..#Op_nT') Is Not Null)
Begin
    Drop Table #Op_nT
End

If(OBJECT_ID('tempdb..#Op_kT') Is Not Null)
Begin
    Drop Table #Op_kT
End

If(OBJECT_ID('tempdb..#BrojPoRasi') Is Not Null)
Begin
    Drop Table #BrojPoRasi
End

If(OBJECT_ID('tempdb..#PovadT') Is Not Null)
Begin
    Drop Table #PovadT
End

create table #Os_nT
(
    RASA nvarchar(max),
	Os_n int
)

insert into #Os_nT
select n.RASA, count(*) as Os_n
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by n.RASA


create table #Os_kT
(
    RASA nvarchar(max),
	Os_k int
)

insert into #Os_kT
select n.RASA, count(*) as Os_k
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by n.RASA



create table #PobT
(
    RASA nvarchar(max),
	Pob int
)

insert into #PobT
select n.RASA, count(*) as Pob
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Pobacaj pob on pob.KrmacaId = k.Id  and pob.CIKLUS = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.RASA


create table #OprasenoT
(
    RASA nvarchar(max),
	Opraseno int,
	Zivo int,
	Mrtvo int
)

insert into #OprasenoT
select n.RASA, count(*) as Opraseno, SUM(pr.ZIVO) as Zivo, SUM(pr.MRTVO) as Mrtvo
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.RASA


create table #SkartT
(
    RASA nvarchar(max),
	Skart int
)

insert into #SkartT
select n.RASA, count(*) as Skart
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.RASA


create table #Op_nT
(
    RASA nvarchar(max),
	Op_n int
)

insert into #Op_nT
select n.RASA, count(*) as Op_n
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by n.RASA


create table #Op_kT
(
    RASA nvarchar(max),
	Op_k int
)

insert into #Op_kT
select n.RASA, count(*) as Op_k
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.PARIT = p.CIKLUS
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by n.RASA


create table #BrojPoRasi
(
    RASA nvarchar(max),
	Broj int
)

insert into #BrojPoRasi
select n.RASA, count(distinct n.Id) as Broj
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by n.RASA

create table #PovadT
(
    RASA nvarchar(max),
	Povad int
)

insert into #PovadT
select n.RASA, count(*) as Povad
from  Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.RAZL_P = '2'
group by n.RASA


select 
coalesce(osk.RASA,osn.RASA) as RASA,
br.Broj,
osk.Os_k,
osn.Os_n,
ISNULL(osk.Os_k,0) + ISNULL(osn.Os_n,0) as Uk_os,
pov.Povad as Povad,
ISNULL(osk.Os_k,0) + ISNULL(osn.Os_n,0) - ISNULL(o.Opraseno,0) as Ne_op,
s.Skart as Isklj,
pob.Pob as Pobac,
0 as Prod,
opn.Op_n,
opk.Op_k,
ISNULL(opn.Op_n,0) + ISNULL(opk.Op_k,0) as Uk_opr,
case when ISNULL(osn.Os_n,0) <> 0 then ISNULL(opn.Op_n,0) / ISNULL(osn.Os_n,0) * 100 else null end as P_op_n,
case when ISNULL(osk.Os_k,0) <> 0 then ISNULL(opk.Op_k,0) / ISNULL(osk.Os_k,0) * 100 else null end as P_op_k,
case when (ISNULL(osk.Os_k,0) + ISNULL(osn.Os_n,0)) <> 0 then (ISNULL(opk.Op_k,0) + ISNULL(opn.Op_n,0)) / (ISNULL(osk.Os_k,0) + ISNULL(osn.Os_n,0)) * 100 else null end  as P_op_uk,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else isnull(o.Zivo,0) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Zivo,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else isnull(o.Mrtvo,0) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Mrtvo,
case when isnull(opk.Op_k,0) + isnull(opn.Op_n,0) = 0 then 0 else (isnull(o.Zivo,0) + isnull(o.Mrtvo,0)) / (isnull(opk.Op_k,0) + isnull(opn.Op_n,0)) end as Uk_p
from #Os_kT osk
full join #Os_nT osn on osn.RASA = osk.RASA
left join #PobT pob on pob.RASA = osk.RASA or pob.RASA = osn.RASA
left join #SkartT s on s.RASA = osk.RASA or s.RASA = osn.RASA
left join #OprasenoT o on o.RASA = osk.RASA or o.RASA = osn.RASA
left join #Op_nT opn on opn.RASA = osk.RASA or opn.RASA = osn.RASA
left join #Op_kT opk on opk.RASA = osk.RASA or opk.RASA = osn.RASA
left join #BrojPoRasi br on br.RASA = osk.RASA or br.RASA = osn.RASA
left join #PovadT pov on pov.RASA = osk.RASA or pov.RASA = osn.RASA

END
