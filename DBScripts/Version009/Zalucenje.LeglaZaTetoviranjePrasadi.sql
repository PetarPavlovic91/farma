USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Prasenje].[ProizvodniRezultatiKrmacaPoParitima]    Script Date: 3/21/2022 10:48:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Zalucenje].[LeglaZaTetoviranjePrasadi]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date,
	@Rasa as Nvarchar(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
k.MBR_KRM,
k.T_KRM as Krmaca,
k.RASA,
pr.PARIT,
pr.DAT_PRAS,
datediff(day, pr.DAT_PRAS, GETDATE()) as Dana,
pr.ZIVO as Prasadi,
pr.ZIVOZ as Zens,
pr.OBJ,
pr.BOX,
pr.TZP_OD as Zenska_od,
pr.TZP_DO as Zenska_do,
pr.TMP_OD as Muski_od,
pr.TMP_DO as Muski_do
from Krmaca k
join Prasenje pr on pr.KrmacaId = k.Id
where  pr.DAT_PRAS >= @dateTimeFrom 
	and pr.DAT_PRAS <= @dateTimeTo 
	and k.RASA LIKE '%' + @Rasa + '%'
	and (pr.TMP_OD is null or pr.TMP_DO is null or pr.TZP_OD is null or pr.TZP_DO is null)
order by pr.DAT_PRAS


END
GO


