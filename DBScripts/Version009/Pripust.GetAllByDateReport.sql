USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[GetAllByDate]    Script Date: 3/6/2022 3:41:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[GetAllByDateReport]
	-- Add the parameters for the stored procedure here
	@danOd as DATE,
	@danDo as DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,P.DAT_PRIP
,k.T_KRM
,NULL AS Slovo
,NULL AS Broj
,k.RASA AS K_RASA
,NULL AS KN
,ISNULL(n.T_NER,n2.T_NER) AS T_Ner
,ISNULL(n.RASA,n2.RASA) AS N_RASA
,NULL AS VO_PP
,NULL AS VO_PP_PoRedu
,P.PRIPUSNICA
,P.PRIMEDBA
,k.VLA
from Pripust p
join Krmaca k on k.Id = p.KrmacaId
left join Nerast n on n.Id = p.NerastId
left join Nerast n2 on n2.Id = p.Nerast2Id
where p.DAT_PRIP >= @danOd and p.DAT_PRIP <= @danDo
 


END
GO


