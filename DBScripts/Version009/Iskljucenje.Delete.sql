USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[Delete]    Script Date: 2/27/2022 11:58:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Iskljucenje].[Delete]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	DELETE FROM dbo.Skart
    WHERE Id = @Id
END
GO


