USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[BioloskiTestNerastova]    Script Date: 4/12/2022 9:50:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[BioloskiTestNerastova]
	-- Add the parameters for the stored procedure here
	@DanOd as Date,
	@DanDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT 
n.T_NER, 
MAX(n.RASA) as Rasa, 
MAX(n.DAT_ROD) as Dat_rod,
count(pr.Id) as Br_leg,
sum(pr.ZIVO) as Br_pras,
sum(case when ISNULL(pr.ANOMALIJE, '') <> '' then 1 else 0 end) as Leg_mane,
0 as Pr_mane,
0 as Hc,
0 as Aa,
0 as Lp,
0 as Rp,
0 as Av,
0 as He
from Pripust p
join Nerast n on p.NerastId = n.Id
left join Skart s on n.Id = s.NerastId
join Prasenje pr on pr.KrmacaId = p.KrmacaId and p.CIKLUS = pr.PARIT
where s.Id is NULL and pr.DAT_PRAS >= @DanOd and pr.DAT_PRAS <= @DanDo
group by n.T_NER




END
GO


