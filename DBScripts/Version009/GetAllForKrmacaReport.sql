USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[GetAllForKrmacaReport]    Script Date: 3/9/2022 11:17:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetAllForKrmacaReport]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,MAX(P.PARIT) AS Pr
,pr.DAT_PRIP AS Dat_prip1
,MAX(pr2.DAT_PRIP) AS Dat_prip2
,MAX(pr3.DAT_PRIP) AS Dat_prip3
,MAX(n.T_NER) AS Nerast
,MAX(n2.T_NER) AS Nerast2
,MAX(n3.T_NER) AS Nerast3
,MAX(P.DAT_PRAS) AS DAT_PRAS
,ISNULL(MAX(P.ZIVO),0) - ISNULL(MAX(P.ZIVOZ),0) AS Musko
,MAX(P.ZIVOZ) AS Zensko
,MAX(P.MRTVO) AS MRTVO
,NULL AS Mumificirano
,ISNULL(MAX(P.ZIVO),0) + ISNULL(MAX(P.MRTVO),0) AS Ukup
,MAX(P.TEZ_LEG) AS TEZ_LEG
,MAX(P.ANOMALIJE) AS ANOMALIJE
,MAX(z.DAT_ZAL) AS DAT_ZAL
,MAX(z.ZALUC) AS ZALUC
,NULL AS SaDana
,MAX(z.TEZ_ZAL) AS TEZ_ZAL
,NULL AS OcenaZalucenja
,NULL AS OcenaKrmace
,NULL AS MuskihOdabrano
,NULL AS ZenskihOdabrano
from Krmaca k 
join Pripust pr on k.Id = pr.KrmacaId AND pr.RBP = 1
LEFT join Pripust pr2 on pr2.KrmacaId = pr.KrmacaId AND pr.CIKLUS = pr2.CIKLUS AND pr2.RBP = 2
LEFT join Pripust pr3 on pr3.KrmacaId = pr.KrmacaId AND pr.CIKLUS = pr3.CIKLUS AND pr3.RBP = 3
LEFT JOIN Prasenje p ON p.KrmacaId = pr.KrmacaId AND p.PARIT = pr.CIKLUS
LEFT JOIN Zaluc Z ON z.KrmacaId = p.KrmacaId AND z.PARIT = p.PARIT
LEFT JOIN Nerast n ON n.id = pr.NerastId
LEFT JOIN Nerast n2 ON n.id = pr2.NerastId
LEFT JOIN Nerast n3 ON n.id = pr3.NerastId
LEFT JOIN Pripust pr1 ON pr1.KrmacaId = k.Id AND pr.CIKLUS + 1 = pr1.CIKLUS
where k.Id = @Id
GROUP BY pr.DAT_PRIP
ORDER BY pr.DAT_PRIP
 
 
END
GO


