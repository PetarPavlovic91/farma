USE [FarmaProduction]
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Selekcija].[ProgeniTestKrmacaINerastovaSI3]
	-- Add the parameters for the stored procedure here
	@Rasa as Nvarchar(max),
	@Krmace as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @Krmace = 1
begin
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB,
MAX(k.RASA) as Rasa,
MAX(k.ODG) as Odg,
MAX(k.T_KRM) as Krmaca,
MAX(k.PARIT) as Parit,
SUM(CASE WHEN t.POL = 'Z' THEN 1 ELSE 0 END) as Zenskih,
SUM(CASE WHEN t.POL = 'M' THEN 1 ELSE 0 END) as Muskih,
count(t.Id) as Ukupno,
count(t.Id) as Testirano,
AVG(ISNULL(t.PRIRAST,0)) as Prirast,
AVG(ISNULL(t.L_SL,0)) as L_SL,
AVG(ISNULL(t.B_SL,0)) as B_SL,
AVG(ISNULL(t.PROC_M,0)) as P_mesa,
MAX(k.si3) as Si3
from Krmaca k
join Test t on t.MBR_MAJKE = k.MBR_KRM
left join Skart s on s.KrmacaId = k.Id
where s.Id is null and si3 > 0 and k.RASA like '%'+@Rasa+'%'
group by k.Id
end
ELSE
BEGIN
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB,
MAX(n.RASA) as Rasa,
MAX(n.ODG) as Odg,
MAX(n.T_NER) as Nerast,
SUM(CASE WHEN t.POL = 'Z' THEN 1 ELSE 0 END) as Zenskih,
SUM(CASE WHEN t.POL = 'M' THEN 1 ELSE 0 END) as Muskih,
count(t.Id) as Ukupno,
count(t.Id) as Testirano,
AVG(ISNULL(t.PRIRAST,0)) as Prirast,
AVG(ISNULL(t.L_SL,0)) as L_SL,
AVG(ISNULL(t.B_SL,0)) as B_SL,
AVG(ISNULL(t.PROC_M,0)) as P_mesa,
MAX(n.si3) as Si3
from Nerast n
join Test t on t.MBR_OCA = n.MBR_NER
left join Skart s on s.NerastId = n.Id
where s.Id is null and si3 > 0 and n.RASA like '%'+@Rasa+'%'
group by n.Id
END
END
GO


