USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Pripust].[Delete]    Script Date: 2/20/2022 11:05:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Pripust].[Delete]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Rbp INT = (SELECT top 1 p.RBP FROM Pripust p WHERE p.Id = @Id);

	IF @Rbp = 1 BEGIN  
		DECLARE @KrmacaId INT = (SELECT top 1 p.KrmacaId FROM Pripust p WHERE p.Id = @Id);
		
		UPDATE Krmaca
		SET CIKLUS = CIKLUS - 1
		WHERE Id = @KrmacaId AND CIKLUS > 1;
    END
    

	DELETE FROM dbo.Pripust
    WHERE Id = @Id
END
