USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Pripust].[Save]    Script Date: 2/14/2022 7:23:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Pripust].[Save]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@Krmaca AS NVARCHAR(MAX),
	@CIKLUS AS INT,
	@RBP AS INT,
	@RAZL_P AS NVARCHAR(MAX) = NULL,
	@OBJ AS NVARCHAR(MAX) = NULL,
	@BOX AS NVARCHAR(MAX) = NULL,
	@Nerast AS NVARCHAR(MAX) = NULL,
	@PV AS NVARCHAR(MAX) = NULL,
	@OSEM AS NVARCHAR(MAX) = NULL,
	@OCENA AS NVARCHAR(MAX) = NULL,
	@Nerast2 AS NVARCHAR(MAX) = NULL,
	@PV2 AS NVARCHAR(MAX) = NULL,
	@OSEM2 AS NVARCHAR(MAX) = NULL,
	@OCENA2 AS NVARCHAR(MAX) = NULL,
	@PRIPUSNICA AS NVARCHAR(MAX) = NULL,
	@PRIMEDBA AS NVARCHAR(MAX) = NULL,
	@DAT_PRIP AS DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @NerastId AS INT = (SELECT TOP 1 Id FROM Nerast n WHERE n.T_NER = @Nerast);
	DECLARE @NerastId2 AS INT = (SELECT TOP 1 Id FROM  Nerast n WHERE n.T_NER = @Nerast2);



IF @Id IS NULL OR @Id = 0 BEGIN  
	
	CREATE TABLE #KrmacaTemp(
		Id INT,
		CIKLUS INT
	)
	INSERT INTO	 #KrmacaTemp SELECT TOP 1 Id, k.CIKLUS FROM Krmaca k WHERE k.T_KRM = @Krmaca;
	DECLARE @KrmacaId AS INT = (SELECT Id FROM #KrmacaTemp);
	DECLARE @KrmacaCiklus AS INT = (SELECT CIKLUS FROM #KrmacaTemp);
	
	DECLARE @LastDateZalucenja AS DATE = (SELECT MAX(z.DAT_ZAL) FROM Zaluc z WHERE z.KrmacaId = @KrmacaId);
	
	CREATE TABLE #PrethodniPripustTemp(
		DAT_PRIP DATE,
		RBP INT
	)

	INSERT INTO #PrethodniPripustTemp SELECT TOP 1 P.DAT_PRIP, p.RBP FROM Pripust p WHERE p.KrmacaId = @KrmacaId ORDER BY p.DAT_PRIP DESC;

	DECLARE @LastDatePripusta AS DATE = (SELECT DAT_PRIP FROM #PrethodniPripustTemp);

	DECLARE @CiklusAdd INT = 0;
	DECLARE @RbpAdd INT = (SELECT RBP + 1 FROM #PrethodniPripustTemp);

	IF (@LastDateZalucenja IS NULL AND  @LastDatePripusta IS NULL) OR (@LastDateZalucenja > @LastDatePripusta) BEGIN  
    	SET @CiklusAdd = 1;
		SET @RbpAdd = 1;
    END

	DECLARE @RAZL_PTemp INT = CASE WHEN @RbpAdd > 1 THEN '2' ELSE NULL END;
    

	IF @KrmacaId IS NULL BEGIN  
    	;THROW 51000, N'Krmaca ne postoji', 1;
    END
    
    INSERT INTO Pripust(DAT_PRIP, CIKLUS, RBP, RAZL_P, OBJ, BOX, PV, OSEM, OCENA, PV2, OSEM2, OCENA2, PRIPUSNICA, PRIMEDBA, Nerast2Id, NerastId, KrmacaId, DateCreated, DateModified)
	VALUES(@DAT_PRIP, @KrmacaCiklus + @CiklusAdd, @RbpAdd, @RAZL_PTemp, @OBJ, @BOX, @PV, @OSEM, @OCENA, @PV2, @OSEM2, @OCENA2,@PRIPUSNICA,@PRIMEDBA,@NerastId2,@NerastId,@KrmacaId,GETDATE(),GETDATE())

	UPDATE Krmaca
	SET CIKLUS = @KrmacaCiklus + @CiklusAdd
	WHERE Id = @KrmacaId;
END
ELSE BEGIN
		UPDATE Pripust
		SET
		OBJ = @OBJ,
		BOX = @BOX,
		PV = @PV,
		OSEM = @OSEM,
		OCENA = @OCENA,
		PV2 = @PV2,
		OSEM2 = @OSEM2,
	    OCENA2 = @OCENA2,
		PRIPUSNICA = @PRIPUSNICA,
		PRIMEDBA = @PRIMEDBA,
		Nerast2Id = @NerastId2,
		NerastId = @NerastId,
		DateModified = GETDATE()
		WHERE Id = @Id
END

END
