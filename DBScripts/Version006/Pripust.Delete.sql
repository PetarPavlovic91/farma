USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[GetAllByDate]    Script Date: 2/9/2022 11:53:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Pripust].[Delete]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM dbo.Pripust
    WHERE Id = @Id
END
GO


