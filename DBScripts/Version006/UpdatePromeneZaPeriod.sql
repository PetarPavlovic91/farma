USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[PromeneZaPeriod]    Script Date: 2/13/2022 9:38:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PromeneZaPeriod]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
If(OBJECT_ID('tempdb..#PocetnoStanjeUlaz') Is Not Null)
Begin
    Drop Table #PocetnoStanjeUlaz
End

If(OBJECT_ID('tempdb..#PocetnoStanjeIzlaz') Is Not Null)
Begin
    Drop Table #PocetnoStanjeUlaz
End

If(OBJECT_ID('tempdb..#TrenutniUlaz') Is Not Null)
Begin
    Drop Table #TrenutniUlaz
End

If(OBJECT_ID('tempdb..#TrenutniIzlaz') Is Not Null)
Begin
    Drop Table #TrenutniIzlaz
End


create table #PocetnoStanjeUlaz
(
    ArtiklId int, 
    Ulaz DECIMAL(10,2), 
    VrednostUlaz DECIMAL(10,2)
)


insert into #PocetnoStanjeUlaz
select  k.ArtiklId as ArtiklId, COALESCE(SUM(k.Kolicina),0) as Ulaz, COALESCE(SUM(k.Cena * k.Kolicina),0) as VrednostUlaz
from Kartica k
join Artikl a on a.Id = k.ArtiklId
where k.Datum <= @dateTimeFrom and	k.U_I = 'U'
group by k.ArtiklId


create table #PocetnoStanjeIzlaz
(
    ArtiklId int, 
    Izlaz DECIMAL(10,2), 
    VrednostIzlaz DECIMAL(10,2)
)


insert into #PocetnoStanjeIzlaz
select  k.ArtiklId as ArtiklId, COALESCE(SUM(k.Kolicina),0) as Izlaz, COALESCE(SUM(k.Cena * k.Kolicina),0) as VrednostIzlaz
from Kartica k
join Artikl a on a.Id = k.ArtiklId
where k.Datum <= @dateTimeFrom and	k.U_I = 'I'
group by k.ArtiklId


create table #TrenutniUlaz
(
    ArtiklId int, 
    Ulaz DECIMAL(10,2), 
    VrednostUlaz DECIMAL(10,2)
)


insert into #TrenutniUlaz
select  k.ArtiklId as ArtiklId, COALESCE(SUM(k.Kolicina),0) as Ulaz, COALESCE(SUM(k.Cena * k.Kolicina),0) as VrednostUlaz
from Kartica k
join Artikl a on a.Id = k.ArtiklId
where k.Datum >= @dateTimeFrom and k.Datum <= @dateTimeTo and	k.U_I = 'U'
group by k.ArtiklId


create table #TrenutniIzlaz
(
    ArtiklId int, 
    Izlaz DECIMAL(10,2), 
    VrednostIzlaz DECIMAL(10,2)
)


insert into #TrenutniIzlaz
select  k.ArtiklId as ArtiklId, COALESCE(SUM(k.Kolicina),0) as Izlaz, COALESCE(SUM(k.Cena * k.Kolicina),0) as VrednostIzlaz
from Kartica k
join Artikl a on a.Id = k.ArtiklId
where k.Datum >= @dateTimeFrom and k.Datum <= @dateTimeTo and	k.U_I = 'I'
group by k.ArtiklId

select 
a.SifraArtikla as Lek, 
a.Naziv, 
jm.Naziv as Jm,
COALESCE(p.Kolicina,0) + COALESCE(pou.Ulaz,0) - COALESCE(poi.Izlaz,0) as Pocst,
COALESCE(p.Kolicina,0) * COALESCE(p.Cena,0) + COALESCE(pou.VrednostUlaz,0) - COALESCE(poi.VrednostIzlaz,0) as Vred_pocst,
tu.Ulaz as Ulaz,
tu.VrednostUlaz as Vred_ulaz,
ti.Izlaz as Izlaz,
ti.VrednostIzlaz as Vred_izlaz,
COALESCE(p.Kolicina,0) + COALESCE(pou.Ulaz,0) - COALESCE(poi.Izlaz,0) + COALESCE(tu.Ulaz,0) - COALESCE(ti.Izlaz,0) as Stanje,
COALESCE(p.Kolicina,0) * COALESCE(p.Cena,0) + COALESCE(pou.VrednostUlaz,0) - COALESCE(poi.VrednostIzlaz,0) + COALESCE(tu.VrednostUlaz,0) - COALESCE(ti.VrednostIzlaz,0) as Vrednost
from Artikl a
join JedinicaMere jm on jm.Id = a.JedinicaMereId
left join #PocetnoStanjeUlaz pou on pou.ArtiklId = a.Id
left join #PocetnoStanjeIzlaz poi on poi.ArtiklId = a.Id
left join #TrenutniUlaz tu on tu.ArtiklId = a.Id
left join #TrenutniIzlaz ti on ti.ArtiklId = a.Id
left join PocetnoStanje p on p.ArtiklId = a.Id


END
