USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[PlanPripustaZaGrupuNazimnica]    Script Date: 2/7/2022 11:04:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[PlanPripustaZaGrupuNazimnica]
	-- Add the parameters for the stored procedure here
	@datumTesta as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + n.T_NER + ']',
    '[' + n.T_NER + ']'
  )
from (
select distinct n.T_NER
FROM Test t 
cross join Nerast n
where t.D_TESTA >= @datumTesta and t.D_TESTA <= DATEADD(day, 1, @datumTesta) and t.POL = 'Z') n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select 
	t.T_TEST, 
	t.RASA, 
	n.T_NER,
	CASE
		WHEN p.MB_OCA = n.MBR_NER
		THEN 0
		WHEN p.MBM_DEDE = n.MBR_NER
		THEN 0
		WHEN p.MBMB_PRAD = n.MBR_NER
		THEN 0
		WHEN p.MBMD_PRAD = n.MBR_NER
		THEN 0
		WHEN p.MBO_DEDE = n.MBR_NER
		THEN 0
		WHEN p.MBOB_PRAD = n.MBR_NER
		THEN 0
		WHEN p.MBOD_PRAD = n.MBR_NER
		THEN 0
		ELSE 1
	END as Ukrstanje
	FROM Test t 
	join Pedigre p on t.MBR_TEST = p.MBR_GRLA
	cross join Nerast n
	where t.D_TESTA >= '''+ CONVERT(NVARCHAR(MAX), @datumTesta)  +''' 
			and t.D_TESTA <=  ''' +  CONVERT(NVARCHAR(MAX), DATEADD(day, 1, @datumTesta)) +''' and t.POL = ''Z'') as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'


EXECUTE(@PivotTableSQL)

END
