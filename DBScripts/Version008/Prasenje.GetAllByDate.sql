USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[AktivneKrmaceNaDan]    Script Date: 2/9/2022 8:28:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE SCHEMA Prasenje;
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Prasenje.GetAllByDate
	-- Add the parameters for the stored procedure here
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,p.Id
,k.T_KRM AS KRMACA
,[DAT_PRAS]
,p.[PARIT]
,[OBJ]
,[BOX]
,[BAB]
,[RAD]
,[MAT]
,[ZIVO]
,[MRTVO]
,[ZIVOZ]
,[UBIJ]
,[UGNJ]
,[ZAPRIM]
,[UGIN]
,[DODATO]
,[ODUZETO]
,[GAJI]
,[TEZ_LEG]
,[ANOMALIJE]
,[TMP_OD]
,[TMP_DO]
,[TZP_OD]
,[TZP_DO]
,[TOV_OD]
,[TOV_DO]
,[REG_PRAS]
,[OL_P]
,[REFK]
,p.[DateCreated]
,p.[DateModified]
from Prasenje p
join Krmaca k on k.Id = p.KrmacaId
where p.DAT_PRAS >= @dan and p.DAT_PRAS <= DATEADD(day, 1, @dan)
 


END
GO


