USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Pripust].[Delete]    Script Date: 2/21/2022 10:15:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Prasenje].[Delete]
	-- Add the parameters for the stored procedure here
	@Id as INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM dbo.Prase
    WHERE PrasenjeId = @Id

	DELETE FROM dbo.Pripust
    WHERE Id = @Id
END
GO


