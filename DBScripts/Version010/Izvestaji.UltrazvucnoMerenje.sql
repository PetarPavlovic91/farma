USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[UltrazvucnoMerenje]    Script Date: 5/15/2022 11:52:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[UltrazvucnoMerenje]
@DatumOd as Date,
@IsTest as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

if @IsTest = 1
BEGIN

select 
t.D_TESTA
,ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,t.POL as Pol
,t.GN as Gn
,T_TEST as T_test
,t.VLA as Vla
,t.ODG as Odg
,t.RASA as Rasa
,t.MBR_TEST as Mbr_test
,t.DAT_ROD as Dat_rod
,t.MBR_OCA as Mbr_oca
,t.MBR_MAJKE as Mbr_majke
,t.IZ_LEGLA as Iz_legla
, t.MARKICA as Markica
,t.EKST as Ekst
,t.BR_SISA as Br_sisa
,t.GEN_MAR as Gen_mar
,t.HALOTAN as Halotan
,t.U_TEST as U_test
,t.POC_TEZ as Poc_tez
,t.TEZINA as Tezina
,case when DATEDIFF(day, t.DAT_ROD, D_TESTA) = 0 then 0 else t.TEZINA * 1000.00 / DATEDIFF(day, t.DAT_ROD, D_TESTA) end as Prirast
,t.HRANA as Hrana
,t.L_SL as L_sl
,t.B_SL as B_sl
,t.DUB_S as Dub_s
,t.DUB_M as Dub_m
,t.PROC_M as Proc_m
,t.SI1F as Si1f
,t.SI1 as Si1
,s.D_SKART as D_skart
,s.RAZL_SK as Razl_sk
,t.NAPOMENA as Napomena
,t.U_PRIPLOD as U_priplod
,t.TETOVIRT10 as Tetovirt10
,n.GN as Gno
,n.TETOVIRN10 as T_oca10
,k.GN as Gnm
,k.TETOVIRK10 as T_majke10
,t.REFK as Refk
,DATEDIFF(day, t.DAT_ROD, GETDATE()) as Starost
,COALESCE(k.HBBROJ,k.RBBROJ) M_hbrb
,k.SI1 as M_Si
,k.KLASA as M_kl
,COALESCE(n.HBBROJ,n.RBBROJ) O_hbrb
,n.SI1 as O_Si
,n.KLASA as O_kl
,n.ODG as Odgo
, k.ODG as Odgm
from Test t
left join Nerast n on t.MBR_OCA = n.MBR_NER
left join Krmaca k on t.MBR_MAJKE = k.MBR_KRM
left join Skart s on t.Id = s.TestId

END
ELSE
BEGIN

select 
t.D_ODABIRA
,ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,t.POL as Pol
,t.GN as Gn
,T_TEST as T_test
,t.VLA as Vla
,t.ODG as Odg
,t.RASA as Rasa
,t.MBR_TEST as Mbr_test
,t.DAT_ROD as Dat_rod
,t.MBR_OCA as Mbr_oca
,t.MBR_MAJKE as Mbr_majke
,t.IZ_LEGLA as Iz_legla
, t.MARKICA as Markica
,t.EKST as Ekst
,t.BR_SISA as Br_sisa
,NULL as Gen_mar
,NULL as Halotan
,NULL as U_test
,NULL as Poc_tez
,t.TEZINA as Tezina
,case when DATEDIFF(day, t.DAT_ROD, t.D_ODABIRA) = 0 then 0 else t.TEZINA * 1000.00 / DATEDIFF(day, t.DAT_ROD, D_ODABIRA) end as Prirast
,NULL as Hrana
,NULL as L_sl
,NULL as B_sl
,NULL as Dub_s
,NULL as Dub_m
,NULL as Proc_m
,NULL as Si1f
,NULL as Si1
,s.D_SKART as D_skart
,s.RAZL_SK as Razl_sk
,t.NAPOMENA as Napomena
,t.U_PRIPLOD as U_priplod
,t.T_TEST as Tetovirt10
,n.GN as Gno
,n.TETOVIRN10 as T_oca10
,k.GN as Gnm
,k.TETOVIRK10 as T_majke10
,t.REFK as Refk
,DATEDIFF(day, t.DAT_ROD, GETDATE()) as Starost
,COALESCE(k.HBBROJ,k.RBBROJ) M_hbrb
,k.SI1 as M_Si
,k.KLASA as M_kl
,COALESCE(n.HBBROJ,n.RBBROJ) O_hbrb
,n.SI1 as O_Si
,n.KLASA as O_kl
,n.ODG as Odgo
, k.ODG as Odgm
from Odabir t
left join Nerast n on t.MBR_OCA = n.MBR_NER
left join Krmaca k on t.MBR_MAJKE = k.MBR_KRM
left join Skart s on t.Id = s.TestId

END

END
GO


