USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[SmotraMladihNerastovaUTestuRadniZapisnik]    Script Date: 5/22/2022 12:25:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SmotraMladihNerastovaUTestuRadniZapisnik]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,T_TEST as Tetovir
,t.RASA
,t.ODG
,t.DAT_ROD
,t.D_TESTA
,t.BR_SISA
,t.EKST
,DATEDIFF(day, t.DAT_ROD, GETDATE()) as Starost
,TEZINA
,case when DATEDIFF(day, t.DAT_ROD, D_TESTA) <= 0 then null else TEZINA * 1000.00 / DATEDIFF(day, t.DAT_ROD, D_TESTA) end as Prirast
,'' as Konver
,t.L_SL as L_sl
,t.B_SL as B_sl
,t.PROC_M as Proc_m
,t.SI1
from Test t
left join Nerast n on t.MBR_TEST = n.MBR_NER
left join Skart s on t.Id = s.TestId
where n.Id is null and s.Id is null and t.POL = 'M'
END
