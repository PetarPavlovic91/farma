USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[Registri_Pripusti]    Script Date: 5/5/2022 8:43:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[Registri_Pripusti]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		,p.DAT_PRIP
		, k.T_KRM as Krmaca
		, k.RASA as Rasa
		, k.MARKICA as Markica
		, SUBSTRING(k.MARKICA, 1, 3) as Id_s
		, SUBSTRING(k.MARKICA, 4,100) as Id_broj
		, p.CIKLUS as Ciklus
		, case when p.CIKLUS = 1 then 'N' else 'K' end as K_n 
		, s.RAZL_SK as Sk
		, p.RBP as Rbp
		, p.RAZL_P as Raz
		,n.T_NER as Nerast
		, n.RASA as Rasan
		, p.PV as Pv
		, p.OSEM as Osem
		, p.PRIPUSNICA as Pripusnica
		, p.PRIMEDBA as Primedba
		, k.VLA as Vla
		, k.MBR_KRM as Mbr_krm
	from Pripust p
	join Krmaca k on k.Id = p.KrmacaId
	left join Skart s on s.KrmacaId =k.Id
	left join Nerast n on n.Id =p.NerastId
	where p.DAT_PRIP >= @DatumOd and p.DAT_PRIP <= @DatumDo and k.RASA like '%' + @Rasa + '%'

END
GO


