USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Izvestaji].[ZahtevZaRegistracijuMuskaGrla]    Script Date: 5/15/2022 10:40:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO













-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Izvestaji].[ZahtevZaRegistracijuMuskaGrla]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, n.T_NER as Nerast
		, n.MARKICA as Markica
		, n.DAT_ROD as Dat_rod
		, n.RASA as Rasa
		, n.KLASA as Klasa
		, n.ODG as Odg
		, n.NA_FARM_OD as Na_farmi_od
		, n.D_P_SKOKA as D_p_skoka
		, null as Dat_prvo_l
		, o.T_NER as Otac
		, COALESCE(o.HBBROJ,o.RBBROJ) as Hr_o
		, o.MARKICA as Id_o
		, o.SI1 as Si_o
		, o.KLASA as Kl_o
		, m.T_KRM as Majka
		, COALESCE(m.HBBROJ,m.RBBROJ,m.NBBROJ) as Hr_m
		, m.MARKICA as Id_m
		, m.SI1 as Si_m
		, m.KLASA as Kl_m
		, n.GN as Gn
		, n.MBR_NER as Mbr_ner
		, o.GN as Gno
		, o.MBR_NER as Mbr_oca
		, m.GN as Gnm
		, m.MBR_KRM as Mbr_majke
		, null as Odgajivac
	from  Nerast n
	left join Skart s on s.NerastId = n.Id
	left join Nerast o on o.MBR_NER = n.MBR_OCA
	left join Krmaca m on m.MBR_KRM = n.MBR_MAJKE
	where (s.Id is null or s.D_SKART >= @DatumOd)
END
