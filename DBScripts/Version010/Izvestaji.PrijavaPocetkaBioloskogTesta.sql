USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[ZahtevZaRegistracijuMuskaGrla]    Script Date: 5/15/2022 10:43:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[PrijavaPocetkaBioloskogTesta]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, n.T_NER as Nerast
		, n.MARKICA as Markica
		, n.BR_DOZVOLE as Br_dozvole
		, n.DAT_ROD as Dat_rod
		, n.RASA as Rasa
		, n.D_P_SKOKA as D_p_skoka
		, n.VLA as Vla
		, 'VOGANJ' as Vlasnik
		, o.T_NER as Otac
		, COALESCE(o.HBBROJ,o.RBBROJ) as Hr_o
		, o.SI1 as Si_o
		, o.KLASA as Kl_o
		, m.T_KRM as Majka
		, COALESCE(m.HBBROJ,m.RBBROJ,m.NBBROJ) as Hr_m
		, m.SI1 as Si_m
		, m.KLASA as Kl_m
		, n.MBR_NER as Mbr_ner
		, o.MBR_NER as Mbr_oca
		, m.MBR_KRM as Mbr_majke
	from  Nerast n
	left join Skart s on s.NerastId = n.Id
	left join Nerast o on o.MBR_NER = n.MBR_OCA
	left join Krmaca m on m.MBR_KRM = n.MBR_MAJKE
	where (s.Id is null or s.D_SKART >= @DatumOd)
END
GO


