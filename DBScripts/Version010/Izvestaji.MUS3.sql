USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[MUS1]    Script Date: 5/24/2022 10:50:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
















-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[MUS3]
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
declare @countNum int = (select count(*) 
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Prasenje p1 on p1.KrmacaId = k.Id and p1.PARIT = p.PARIT + 1
where p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje);

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,max(k.RASA) as Genotip
,count(DISTINCT k.Id) as Krm
,sum(case when p.id is not null then 1 else 0 end) as Leg
,AVG(ISNULL(p.ZIVO,0)) as Zivo
,AVG(ISNULL(DATEDIFF(day,p.DAT_PRAS,z.DAT_ZAL),0)) as Lakt
,AVG(ISNULL(z.ZALUC,0)) as Zal
,AVG(ISNULL(z.TEZ_ZAL,0)) as Tez_l
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Prasenje p1 on p1.KrmacaId = k.Id and p1.PARIT = p.PARIT + 1
where p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje and k.KLASA = 'E'
group by k.RASA
END
GO


