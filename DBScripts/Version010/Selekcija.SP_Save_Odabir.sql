USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Selekcija].[SP_Save_Odabir]    Script Date: 5/29/2022 11:01:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Selekcija].[SP_Save_Odabir]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@Tezina AS INT,
	@Prirast AS INT,
	@IzLegla AS NVARCHAR(MAX) = NULL,
	@Ekst AS INT = NULL,
	@Markica AS NVARCHAR(MAX) = NULL,
	@Sisa AS NVARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		UPDATE Odabir
		SET
		TEZINA = @Tezina,
		PRIRAST = @Prirast,
		IZ_LEGLA = @IzLegla,
		EKST = @Ekst,
		MARKICA = @Markica,
		BR_SISA = @Sisa,
		DateModified = GETDATE()
		WHERE Id = @Id

END
