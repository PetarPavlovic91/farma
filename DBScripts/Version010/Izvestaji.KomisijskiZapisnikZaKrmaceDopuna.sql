USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[KomisijskiZapisnikZaKrmace]    Script Date: 5/15/2022 1:45:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[KomisijskiZapisnikZaKrmaceDopuna]
@DatumOd as Date,
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date,
@Paritet as Int,
@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,max(k.T_KRM) as Krmaca
,max(k.VLA) as Vla
,max(k.ODG) as Odg
,max(k.RASA) as Rasa
,max(k.MBR_KRM) as Mbr_krm
,max(k.DAT_ROD) as Dat_rod
,max(kl.KLASA) as Klasa
, CASE when ISNULL(max(k.HBBROJ), '') <> '' then 'HB' when ISNULL(max(k.RBBROJ), '') <> '' then 'RB' when ISNULL(max(k.NBBROJ), '') <> '' then 'NB' end as Hr
, COALESCE(max(k.HBBROJ),max(k.RBBROJ),max(k.NBBROJ)) as Broj
,max(k.MARKICA) as Markica
,max(k.GN) as Gn
,max(k.GNO) as GNo
,max(k.MBR_OCA) as Mbr_oca
,max(k.GNM) as Gnm
,max(k.MBR_MAJKE) as Mbr_majke
,max(k.SI1) as Si1
,max(k.SI2) as Si2
,max(kl.KL_P) as Kl_p
,max(k.IEZ) as Iez
,max(k.BR_SISA) as Br_sisa
,max(k.EKST) as Ekst
,null as St_1_pr
,max(p.DAT_PRAS) as Dat_pras
,max(p.PARIT) as Br_l
,sum(ISNULL(p.ZIVO,0)) as Uk_zivo
,sum(ISNULL(p.MRTVO,0)) as Uk_mrtvo
,sum(ISNULL(z.ZALUC,0)) as Uk_zaluc
,sum(ISNULL(p.ZIVO,0)) * 1.0 / MAX(p.Parit) as Pr_zivo
,sum(ISNULL(p.MRTVO,0)) * 1.0 / MAX(p.Parit) as Pr_mrtvo
,sum(ISNULL(z.ZALUC,0)) * 1.0 / MAX(z.Parit) as Pr_zaluc
,sum(ISNULL(z.TEZ_ZAL,0)) * 1.0 / MAX(z.Parit) as Masa_l
,sum(ISNULL(DATEDIFF(day,p.DAT_PRAS,z.DAT_ZAL),0)) as Uk_lakt
,sum(ISNULL(DATEDIFF(day,z.DAT_ZAL,p1.DAT_PRAS),0)) as Uk_praz_d
,sum(ISNULL(DATEDIFF(day,p.DAT_PRAS,p1.DAT_PRAS),0)) - sum(ISNULL(DATEDIFF(day,z.DAT_ZAL,p1.DAT_PRAS),0)) as Uk_proiz_d
,max(k.FAZA) as Faza
,max(k.PRED_FAZA) as Pred_faza
,max(k.NAPOMENA) as Napomena
,max(o.T_NER) as Otac
,COALESCE(max(o.HBBROJ),max(o.RBBROJ)) as Hr_o
,max(o.SI1) as Si_o
,max(o.KLASA) as Kl_o
,max(m.T_KRM) as Majka
,COALESCE(max(m.HBBROJ),max(m.RBBROJ),max(m.NBBROJ)) as Hr_m
,max(m.SI1) as Si_m
,max(m.KLASA) as Kl_m
,max(kl.DAT_SMOTRE) as Dat_smotre
from Krmaca k 
left join Skart s on k.Id = s.KrmacaId
join Klasa kl on kl.MBR_GRLA = k.MBR_KRM
join Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Prasenje p1 on p1.KrmacaId = k.Id and p1.PARIT = p.PARIT + 1
join Nerast o on o.MBR_NER =k.MBR_OCA
join Krmaca m on m.MBR_KRM = k.MBR_MAJKE
where kl.DAT_SMOTRE = @DatumOd and k.RASA like '%' + @Rasa +'%' and p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje and k.PARIT <= @Paritet
group by k.Id
END
GO


