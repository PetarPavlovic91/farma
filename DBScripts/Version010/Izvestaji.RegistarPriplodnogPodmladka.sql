USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[RegistarPriplodnogPodmladka]    Script Date: 5/7/2022 3:20:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[RegistarPriplodnogPodmladka]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max),
	@Muski as Bit,
	@Zenski as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	if	@Muski = 1 and @Zenski = 1
	begin
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, t.T_TEST as Tetov
		, t.DAT_ROD as Dat_rod
		, t.POL as Pol
		, t.RASA as Rasa
		, DATEDIFF(day, t.DAT_ROD, GETDATE()) as Starost
		, t.IZ_LEGLA as Iz_legla
		, SUBSTRING(t.IZ_LEGLA, 1, 1) as Rb_l
		, k.T_KRM as Majka
		, k.HBBROJ as Hb_Majke
		, k.RASA as Rasa_m
		, k.MBR_KRM as Mbr_majke
		, n.T_NER as Otac
		, n.HBBROJ as Hb_Oca
		, n.RASA as Rasa_o
		, n.MBR_NER as Mbr_oca
		, t.L_SL as S_l
		, t.B_SL as S_d
	from Test t
	left join Krmaca k on k.MBR_KRM = t.MBR_MAJKE
	left join Nerast n on n.MBR_NER = t.MBR_OCA
	where t.DAT_ROD >= @DatumOd and t.DAT_ROD <= @DatumDo and t.RASA like '%' + @Rasa + '%'
	END

	else if @Muski = 1 and @Zenski = 0
		begin
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, t.T_TEST as Tetov
		, t.DAT_ROD as Dat_rod
		, t.POL as Pol
		, t.RASA as Rasa
		, DATEDIFF(day, t.DAT_ROD, GETDATE()) as Starost
		, t.IZ_LEGLA as Iz_legla
		, SUBSTRING(t.IZ_LEGLA, 1, 1) as Rb_l
		, k.T_KRM as Majka
		, k.HBBROJ as Hb_Majke
		, k.RASA as Rasa_m
		, k.MBR_KRM as Mbr_majke
		, n.T_NER as Otac
		, n.HBBROJ as Hb_Oca
		, n.RASA as Rasa_o
		, n.MBR_NER as Mbr_oca
		, t.L_SL as S_l
		, t.B_SL as S_d
	from Test t
	left join Krmaca k on k.MBR_KRM = t.MBR_MAJKE
	left join Nerast n on n.MBR_NER = t.MBR_OCA
	where t.DAT_ROD >= @DatumOd and t.DAT_ROD <= @DatumDo and t.RASA like '%' + @Rasa + '%' and t.POL = 'M'
	END
	
	else if @Muski = 0 and @Zenski = 1
		begin
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, t.T_TEST as Tetov
		, t.DAT_ROD as Dat_rod
		, t.POL as Pol
		, t.RASA as Rasa
		, DATEDIFF(day, t.DAT_ROD, GETDATE()) as Starost
		, t.IZ_LEGLA as Iz_legla
		, SUBSTRING(t.IZ_LEGLA, 1, 1) as Rb_l
		, k.T_KRM as Majka
		, k.HBBROJ as Hb_Majke
		, k.RASA as Rasa_m
		, k.MBR_KRM as Mbr_majke
		, n.T_NER as Otac
		, n.HBBROJ as Hb_Oca
		, n.RASA as Rasa_o
		, n.MBR_NER as Mbr_oca
		, t.L_SL as S_l
		, t.B_SL as S_d
	from Test t
	left join Krmaca k on k.MBR_KRM = t.MBR_MAJKE
	left join Nerast n on n.MBR_NER = t.MBR_OCA
	where t.DAT_ROD >= @DatumOd and t.DAT_ROD <= @DatumDo and t.RASA like '%' + @Rasa + '%' and t.POL = 'Z'
	END
END
GO


