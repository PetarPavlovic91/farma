USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[Registri_Prasenja]    Script Date: 5/7/2022 3:55:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[Registri_RegistarPrasadi]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, case when IsNull(MAX(p.TMP_OD),0) <> 0 and IsNull(MAX(p.TMP_OD),0) < IsNull(MAX(p.TZP_OD),0) then IsNull(MAX(p.TMP_OD),0) else IsNull(MAX(p.TZP_OD),0) end as Tet_od
		, case when IsNull(MAX(p.TMP_DO),0) <> 0 and IsNull(MAX(p.TMP_DO),0) > IsNull(MAX(p.TZP_DO),0) then IsNull(MAX(p.TMP_DO),0) else IsNull(MAX(p.TZP_DO),0) end as Tet_do
		, MAX(p.DAT_PRAS) as Dat_rod
		, SUBSTRING(MAX(k.RASA), 1, 2) + SUBSTRING(MAX(n.RASA), 1, 2) as Rasa
		, MAX(k.T_KRM) as Majka
		, MAX(k.RASA) as Rasa_m
		, MAX(k.MARKICA) as Markica
		, SUBSTRING(MAX(k.MARKICA), 1, 3) as Id_s
		, SUBSTRING(MAX(k.MARKICA), 4,100) as Id_broj
		, COALESCE(MAX(k.HBBROJ),MAX(k.RBBROJ),MAX(k.NBBROJ)) as M_Hb_rb_n
		, MAX(n.T_NER) as Otac
		, MAX(n.RASA) as Rasa_o
		, COALESCE(MAX(n.HBBROJ),MAX(n.RBBROJ)) as O_Hb_rb
		, MAX(n.ODG) as Odg
		, MAX(k.MBR_KRM) as Mbr_majke
		, MAX(n.MBR_NER) as Mbr_oca
	from Prasenje p
	join Krmaca k on k.Id = p.KrmacaId
	join Pripust pr on pr.KrmacaId = k.Id and pr.CIKLUS = p.PARIT
	left join Nerast n on pr.NerastId = n.Id
	where p.DAT_PRAS >= @DatumOd and p.DAT_PRAS <= @DatumDo and k.RASA like '%' + @Rasa + '%'
	GROUP by p.Id

END
GO


