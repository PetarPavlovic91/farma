USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[KomisijskiZapisnikZaKrmaceDopuna]    Script Date: 5/22/2022 11:47:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO















-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[MUS1]
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
declare @countNum int = (select count(*) 
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Prasenje p1 on p1.KrmacaId = k.Id and p1.PARIT = p.PARIT + 1
where p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje);

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,max(k.RASA) as Genotip
,count(*) as Br_krm
,count(*) * 100.00 / @countNum as [Proc]
,count(*) as Br_leg
,sum(ISNULL(p.ZIVO,0)) * 1.0 / MAX(p.Parit) as Pr_zivo
,sum(ISNULL(p.MRTVO,0)) * 1.0 / MAX(p.Parit) as Pr_mrtvo
,(sum(ISNULL(p.ZIVO,0)) * 1.0 + sum(ISNULL(p.MRTVO,0)) * 1.0)/ MAX(p.Parit) as Ukupno
,sum(case when z.id is null then 0 else 1 end) as Zal_lp
,sum(case when z.id is null then 0 else 1 end) as Zal_l
,AVG(ISNULL(DATEDIFF(day,p.DAT_PRAS,z.DAT_ZAL),0)) as Lakt
,AVG(ISNULL(z.ZALUC,0)) as Zal_opr
,AVG(ISNULL(z.ZALUC,0)) as Zal_zal
,AVG(ISNULL(z.TEZ_ZAL,0)) as Tez_l
,AVG(case WHEN ISNULL(z.ZALUC,0) = 0 then 0 else ISNULL(z.TEZ_ZAL,0)/ISNULL(z.ZALUC,0) end) as Tez_p
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = k.Id and z.PARIT = p.PARIT
left join Prasenje p1 on p1.KrmacaId = k.Id and p1.PARIT = p.PARIT + 1
where p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje
group by k.RASA
END
GO


