USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[MUS4]    Script Date: 5/24/2022 11:42:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[MUS4]
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,max(t.RASA) as Rasa
,count(*) as Testirano
,avg(datediff(day,t.D_TESTA,GETDATE())) as Starost
,avg(isnull(t.TEZINA,0)) as Tezina
,avg(isnull(t.PRIRAST,0)) as Prirast
,avg(isnull(t.L_SL,0)) as L_sl
,avg(isnull(t.B_SL,0)) as B_sl
,avg(isnull(t.DUB_S,0)) as Dub_s
,avg(isnull(t.DUB_M,0)) as Mld
,avg(isnull(t.PROC_M,0)) as P_mesa
,avg(isnull(t.SI1,0)) as Si1
,avg(isnull(t.DUB_M,0)) as Mld
,sum(case when p.id is not null then 1 else 0 end) Osem
,sum(case when p.id is not null then 1 else 0 end) * 100.00 / count(*) as Pr_osem
from Test t 
left join Krmaca k on k.MBR_KRM = t.MBR_TEST
left join Pripust p on p.KrmacaId = k.Id
where t.D_TESTA >= @DatumOdPrasenje and t.D_TESTA <= @DatumDoPrasenje and t.POL = 'Z'
group by t.RASA
END
GO


