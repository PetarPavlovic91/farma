USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[GrlaNaGazdinstvu]    Script Date: 5/3/2022 9:55:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[GrlaNaGazdinstvu]
	-- Add the parameters for the stored procedure here
	@Dan as Date,
	@DanaOdTesta as int,
	@StarostPrasadiOd as Int,
	@StarostPrasadiDo as Int,
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


If(OBJECT_ID('tempdb..#TempGrlaNaGazdinstvu') Is Not Null)
Begin
    Drop Table #TempGrlaNaGazdinstvu
End

create table #TempGrlaNaGazdinstvu
(
    Grlo nvarchar(max),
    Kartegorija nvarchar(max),
    Rasa nvarchar(max),
    Markica nvarchar(max),
    HbRbNb nvarchar(max),
    Broj nvarchar(max),
    Red_s nvarchar(max),
    Dat_rod Date,
    Pol nvarchar(max),
    Mbr_grla nvarchar(max),
    Kat Int,
)

insert into #TempGrlaNaGazdinstvu
select 
k.T_KRM as Grlo
,'Krmaca' as Kartegorija
,k.Rasa as Rasa
,k.MARKICA as Markica
,case when k.HBBROJ is not null and k.HBBROJ <> '' then 'HB'
	when k.RBBROJ is not null and k.RBBROJ <> '' then 'RB'
	when k.NBBROJ is not null and k.NBBROJ <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(k.HBBROJ, k.RBBROJ,k.NBBROJ,null) as Broj
,null as Red_s
, k.DAT_ROD as Dat_rod
,'Z' as Pol
, k.MBR_KRM as Mbr_grla
, 1 as Kat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.RASA like '%' +@Rasa+ '%' and k.T_KRM is not null

insert into #TempGrlaNaGazdinstvu
select 
n.T_NER as Grlo
,'Nerast' as Kartegorija
,n.Rasa as Rasa
,n.MARKICA as Markica
,case when n.HBBROJ is not null and n.HBBROJ <> '' then 'HB'
	when n.RBBROJ is not null and n.RBBROJ <> '' then 'RB'
	else '' end as HbRbNb
,COALESCE(n.HBBROJ, n.RBBROJ,null) as Broj
,null as Red_s
,n.Dat_rod as Dat_rod 
,'M' as Pol
, n.MBR_NER as Mbr_grla
, 2 as Kat
from Nerast n
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and n.RASA like '%' +@Rasa+ '%' and n.T_NER is not null

insert into #TempGrlaNaGazdinstvu
select 
t.T_TEST as Grlo
,'Test' as Kartegorija
,t.Rasa as Rasa
,t.MARKICA as Markica
,null as HbRbNb
,null as Broj
,null as Red_s
,t.Dat_rod as Dat_rod 
,t.POL as Pol
,t.MBR_TEST as Mbr_grla
,3 as Kat
from Test t
left join Skart s on s.TestId = t.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and t.RASA like '%' +@Rasa+ '%' and DATEDIFF(day,t.D_TESTA,GETDATE()) < @DanaOdTesta
and t.T_TEST is not null and t.T_TEST not in 
(
select Grlo
from  #TempGrlaNaGazdinstvu
)


insert into #TempGrlaNaGazdinstvu
select 
o.T_TEST as Grlo
,'Odabir' as Kartegorija
,o.Rasa as Rasa
,o.MARKICA as Markica
,null as HbRbNb
,null as Broj
,null as Red_s
,o.Dat_rod as Dat_rod 
,o.POL as Pol
,o.MBR_TEST as Mbr_grla
,4 as Kat
from Odabir o
left join Skart s on s.OdabirId = o.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and o.RASA like '%' +@Rasa+ '%' and DATEDIFF(day,o.D_ODABIRA,GETDATE()) < @DanaOdTesta
and o.T_TEST is not null and o.T_TEST not in 
(
select Grlo
from  #TempGrlaNaGazdinstvu
)


insert into #TempGrlaNaGazdinstvu
select 
p.T_PRAS as Grlo
,'Prase' as Kartegorija
,null as Rasa
,null as Markica
,null as HbRbNb
,null as Broj
,null as Red_s
,p.Dat_rod as Dat_rod 
,p.POL as Pol
,p.MBR_PRAS as Mbr_grla
,5 as Kat
from Prase p
left join Skart s on s.PraseId = p.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
and DATEDIFF(day,p.DAT_ROD,GETDATE()) >= @StarostPrasadiOd
and DATEDIFF(day,p.DAT_ROD,GETDATE()) <= @StarostPrasadiDo
and p.T_PRAS is not null and p.T_PRAS not in 
(
select Grlo
from  #TempGrlaNaGazdinstvu
)

	Select
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
	,*
	FROM #TempGrlaNaGazdinstvu


END
GO


