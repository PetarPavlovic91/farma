USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[IzvodIzGlavneMaticneEvidencije]    Script Date: 5/3/2022 1:14:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[IzvodIzGlavneMaticneEvidencije]
	-- Add the parameters for the stored procedure here
	@Dan as Date,
	@Paritet as int,
	@IsHBRB as Bit,
	@IsNB as Bit,
	@IsNerastovi as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if	@IsHBRB = 1 and @IsNB = 1 and @IsNerastovi = 1
	begin
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,*
FROM
(
select 
'Z' as Pol
,case when MAX(k.HBBROJ) is not null and MAX(k.HBBROJ) <> '' then 'HB'
	when MAX(k.RBBROJ) is not null and MAX(k.RBBROJ) <> '' then 'RB'
	when MAX(k.NBBROJ) is not null and MAX(k.NBBROJ) <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(MAX(k.HBBROJ), MAX(k.RBBROJ),MAX(k.NBBROJ),null) as Broj
,MAX(k.T_KRM) as Grlo
,MAX(k.ODG) as Odg
,MAX(k.Rasa) as Rasa
,MAX(k.Dat_rod) as Dat_rod 
,MAX(k.Si1) as Si1
,MAX(p.DAT_PRAS) as Dat_pras
,null as Dat_pos_l
,MAX(k.PARIT) as Br_l
,MAX(k.MARKICA) as Markica
,MAX(k.Klasa) as Klasa
,null as Red_s
,null as Dat_z_skok
,MAX(k.CIKLUS) as Uk_leg
,AVG(p.ZIVO) as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,MAX(p.REG_PRAS) as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(k.GN) as Gn
,MAX(k.MBR_KRM) as Mbr_grla
,MAX(k.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(k.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet 
group by k.Id

union 

select 
'M' as Pol
,case when MAX(n.HBBROJ) is not null and MAX(n.HBBROJ) <> '' then 'HB'
	when MAX(n.RBBROJ) is not null and MAX(n.RBBROJ) <> '' then 'RB'
	else '' end as HbRbNb
,COALESCE(MAX(n.HBBROJ), MAX(n.RBBROJ),null) as Broj
,MAX(n.T_NER) as Grlo
,MAX(n.ODG) as Odg
,MAX(n.Rasa) as Rasa
,MAX(n.Dat_rod) as Dat_rod 
,MAX(n.Si1) as Si1
,null as Dat_pras
,null as Dat_pos_l
,null as Br_l
,MAX(n.MARKICA) as Markica
,MAX(n.Klasa) as Klasa
,null as Red_s
,MAX(p.DAT_PRIP) as Dat_z_skok
,null as Uk_leg
,null as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,null as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(n.GN) as Gn
,MAX(n.MBR_NER) as Mbr_grla
,MAX(n.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(n.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Nerast n
left join Pripust p on p.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id
) as t
 END

 ELSE IF @IsHBRB = 1 and @IsNB = 1 and @IsNerastovi = 0

 BEGIN
 select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,'Z' as Pol
,case when MAX(k.HBBROJ) is not null and MAX(k.HBBROJ) <> '' then 'HB'
	when MAX(k.RBBROJ) is not null and MAX(k.RBBROJ) <> '' then 'RB'
	when MAX(k.NBBROJ) is not null and MAX(k.NBBROJ) <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(MAX(k.HBBROJ), MAX(k.RBBROJ),MAX(k.NBBROJ),null) as Broj
,MAX(k.T_KRM) as Grlo
,MAX(k.ODG) as Odg
,MAX(k.Rasa) as Rasa
,MAX(k.Dat_rod) as Dat_rod 
,MAX(k.Si1) as Si1
,MAX(p.DAT_PRAS) as Dat_pras
,null as Dat_pos_l
,MAX(k.PARIT) as Br_l
,MAX(k.MARKICA) as Markica
,MAX(k.Klasa) as Klasa
,null as Red_s
,null as Dat_z_skok
,MAX(k.CIKLUS) as Uk_leg
,AVG(p.ZIVO) as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,MAX(p.REG_PRAS) as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(k.GN) as Gn
,MAX(k.MBR_KRM) as Mbr_grla
,MAX(k.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(k.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet 
group by k.Id

 END

 else if	@IsHBRB = 1 and @IsNB = 0 and @IsNerastovi = 1
	begin
	select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,*
FROM
(
select 
'Z' as Pol
,case when MAX(k.HBBROJ) is not null and MAX(k.HBBROJ) <> '' then 'HB'
	when MAX(k.RBBROJ) is not null and MAX(k.RBBROJ) <> '' then 'RB'
	when MAX(k.NBBROJ) is not null and MAX(k.NBBROJ) <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(MAX(k.HBBROJ), MAX(k.RBBROJ),MAX(k.NBBROJ),null) as Broj
,MAX(k.T_KRM) as Grlo
,MAX(k.ODG) as Odg
,MAX(k.Rasa) as Rasa
,MAX(k.Dat_rod) as Dat_rod 
,MAX(k.Si1) as Si1
,MAX(p.DAT_PRAS) as Dat_pras
,null as Dat_pos_l
,MAX(k.PARIT) as Br_l
,MAX(k.MARKICA) as Markica
,MAX(k.Klasa) as Klasa
,null as Red_s
,null as Dat_z_skok
,MAX(k.CIKLUS) as Uk_leg
,AVG(p.ZIVO) as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,MAX(p.REG_PRAS) as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(k.GN) as Gn
,MAX(k.MBR_KRM) as Mbr_grla
,MAX(k.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(k.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet and ((k.HBBROJ is not null and  k.HBBROJ <> '')  or (k.RBBROJ is not null and k.RBBROJ <> ''))
group by k.Id

union 

select 
'M' as Pol
,case when MAX(n.HBBROJ) is not null and MAX(n.HBBROJ) <> '' then 'HB'
	when MAX(n.RBBROJ) is not null and MAX(n.RBBROJ) <> '' then 'RB'
	else '' end as HbRbNb
,COALESCE(MAX(n.HBBROJ), MAX(n.RBBROJ),null) as Broj
,MAX(n.T_NER) as Grlo
,MAX(n.ODG) as Odg
,MAX(n.Rasa) as Rasa
,MAX(n.Dat_rod) as Dat_rod 
,MAX(n.Si1) as Si1
,null as Dat_pras
,null as Dat_pos_l
,null as Br_l
,MAX(n.MARKICA) as Markica
,MAX(n.Klasa) as Klasa
,null as Red_s
,MAX(p.DAT_PRIP) as Dat_z_skok
,null as Uk_leg
,null as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,null as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(n.GN) as Gn
,MAX(n.MBR_NER) as Mbr_grla
,MAX(n.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(n.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Nerast n
left join Pripust p on p.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id
) as t
 END

  else if	@IsHBRB = 1 and @IsNB = 0 and @IsNerastovi = 0
	begin
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,'Z' as Pol
,case when MAX(k.HBBROJ) is not null and MAX(k.HBBROJ) <> '' then 'HB'
	when MAX(k.RBBROJ) is not null and MAX(k.RBBROJ) <> '' then 'RB'
	when MAX(k.NBBROJ) is not null and MAX(k.NBBROJ) <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(MAX(k.HBBROJ), MAX(k.RBBROJ),MAX(k.NBBROJ),null) as Broj
,MAX(k.T_KRM) as Grlo
,MAX(k.ODG) as Odg
,MAX(k.Rasa) as Rasa
,MAX(k.Dat_rod) as Dat_rod 
,MAX(k.Si1) as Si1
,MAX(p.DAT_PRAS) as Dat_pras
,null as Dat_pos_l
,MAX(k.PARIT) as Br_l
,MAX(k.MARKICA) as Markica
,MAX(k.Klasa) as Klasa
,null as Red_s
,null as Dat_z_skok
,MAX(k.CIKLUS) as Uk_leg
,AVG(p.ZIVO) as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,MAX(p.REG_PRAS) as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(k.GN) as Gn
,MAX(k.MBR_KRM) as Mbr_grla
,MAX(k.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(k.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet and ((k.HBBROJ is not null and  k.HBBROJ <> '')  or (k.RBBROJ is not null and k.RBBROJ <> ''))
group by k.Id

 END
 
else if	@IsHBRB = 0 and @IsNB = 1 and @IsNerastovi = 1
	begin
	select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,*
FROM
(
select 
'Z' as Pol
,case when MAX(k.HBBROJ) is not null and MAX(k.HBBROJ) <> '' then 'HB'
	when MAX(k.RBBROJ) is not null and MAX(k.RBBROJ) <> '' then 'RB'
	when MAX(k.NBBROJ) is not null and MAX(k.NBBROJ) <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(MAX(k.HBBROJ), MAX(k.RBBROJ),MAX(k.NBBROJ),null) as Broj
,MAX(k.T_KRM) as Grlo
,MAX(k.ODG) as Odg
,MAX(k.Rasa) as Rasa
,MAX(k.Dat_rod) as Dat_rod 
,MAX(k.Si1) as Si1
,MAX(p.DAT_PRAS) as Dat_pras
,null as Dat_pos_l
,MAX(k.PARIT) as Br_l
,MAX(k.MARKICA) as Markica
,MAX(k.Klasa) as Klasa
,null as Red_s
,null as Dat_z_skok
,MAX(k.CIKLUS) as Uk_leg
,AVG(p.ZIVO) as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,MAX(p.REG_PRAS) as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(k.GN) as Gn
,MAX(k.MBR_KRM) as Mbr_grla
,MAX(k.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(k.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet and (k.HBBROJ is null or  k.HBBROJ = '')  and (k.RBBROJ is null or  k.RBBROJ = '') and k.NBBROJ is not null and k.NBBROJ <> ''
group by k.Id

union 

select 
'M' as Pol
,case when MAX(n.HBBROJ) is not null and MAX(n.HBBROJ) <> '' then 'HB'
	when MAX(n.RBBROJ) is not null and MAX(n.RBBROJ) <> '' then 'RB'
	else '' end as HbRbNb
,COALESCE(MAX(n.HBBROJ), MAX(n.RBBROJ),null) as Broj
,MAX(n.T_NER) as Grlo
,MAX(n.ODG) as Odg
,MAX(n.Rasa) as Rasa
,MAX(n.Dat_rod) as Dat_rod 
,MAX(n.Si1) as Si1
,null as Dat_pras
,null as Dat_pos_l
,null as Br_l
,MAX(n.MARKICA) as Markica
,MAX(n.Klasa) as Klasa
,null as Red_s
,MAX(p.DAT_PRIP) as Dat_z_skok
,null as Uk_leg
,null as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,null as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(n.GN) as Gn
,MAX(n.MBR_NER) as Mbr_grla
,MAX(n.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(n.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Nerast n
left join Pripust p on p.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id
) as t
 END

 ELSE IF @IsHBRB = 0 and @IsNB = 1 and @IsNerastovi = 0

 BEGIN
 select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,'Z' as Pol
,case when MAX(k.HBBROJ) is not null and MAX(k.HBBROJ) <> '' then 'HB'
	when MAX(k.RBBROJ) is not null and MAX(k.RBBROJ) <> '' then 'RB'
	when MAX(k.NBBROJ) is not null and MAX(k.NBBROJ) <> '' then 'NB'
	else '' end as HbRbNb
,COALESCE(MAX(k.HBBROJ), MAX(k.RBBROJ),MAX(k.NBBROJ),null) as Broj
,MAX(k.T_KRM) as Grlo
,MAX(k.ODG) as Odg
,MAX(k.Rasa) as Rasa
,MAX(k.Dat_rod) as Dat_rod 
,MAX(k.Si1) as Si1
,MAX(p.DAT_PRAS) as Dat_pras
,null as Dat_pos_l
,MAX(k.PARIT) as Br_l
,MAX(k.MARKICA) as Markica
,MAX(k.Klasa) as Klasa
,null as Red_s
,null as Dat_z_skok
,MAX(k.CIKLUS) as Uk_leg
,AVG(p.ZIVO) as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,MAX(p.REG_PRAS) as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(k.GN) as Gn
,MAX(k.MBR_KRM) as Mbr_grla
,MAX(k.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(k.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet  and (k.HBBROJ is null or  k.HBBROJ = '')  and (k.RBBROJ is null or  k.RBBROJ = '')  and k.NBBROJ is not null and k.NBBROJ <> ''
group by k.Id

 END

 else if	@IsHBRB = 0 and @IsNB = 0 and @IsNerastovi = 1
	begin

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,'M' as Pol
,case when MAX(n.HBBROJ) is not null and MAX(n.HBBROJ) <> '' then 'HB'
	when MAX(n.RBBROJ) is not null and MAX(n.RBBROJ) <> '' then 'RB'
	else '' end as HbRbNb
,COALESCE(MAX(n.HBBROJ), MAX(n.RBBROJ),null) as Broj
,MAX(n.T_NER) as Grlo
,MAX(n.ODG) as Odg
,MAX(n.Rasa) as Rasa
,MAX(n.Dat_rod) as Dat_rod 
,MAX(n.Si1) as Si1
,null as Dat_pras
,null as Dat_pos_l
,null as Br_l
,MAX(n.MARKICA) as Markica
,MAX(n.Klasa) as Klasa
,null as Red_s
,MAX(p.DAT_PRIP) as Dat_z_skok
,null as Uk_leg
,null as Zivo
,MAX(majka.KLASA) as Kl_m
,MAX(otac.T_NER) as Otac_l
,MAX(otac.HBBROJ) as Hb_oca_l
,null as Rb_reg_p
,MAX(otac.MARKICA) as Id_oca
,MAX(majka.MARKICA) as Id_majke
,MAX(n.GN) as Gn
,MAX(n.MBR_NER) as Mbr_grla
,MAX(n.GNO) as Gno
,MAX(otac.MBR_NER) as Mbr_oca
,MAX(n.GNM) as Gnm
,MAX(majka.MBR_MAJKE) as Mbr_majke
from Nerast n
left join Pripust p on p.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id

 END

END
GO


