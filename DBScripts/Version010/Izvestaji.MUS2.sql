USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[KomisijskiZapisnikZaNerastove]    Script Date: 5/24/2022 10:30:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
















-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[MUS2]
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,n.RASA as Rasan
,count(*) as Broj
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) as Os_n
,Sum(case when k.PARIT > 1 then 1 else 0 end) as Os_k
,Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) + Sum(case when k.PARIT > 1 then 1 else 0 end) as Uk_os
,Sum(case when pr.RBP > 1 then 1 else 0 end) as Povad 
,Sum(case when p.id is null then 1 else 0 end) as Ne_op 
,Sum(case when s.id is not null then 1 else 0 end) as Isklj 
,Sum(case when pob.id is not null then 1 else 0 end) as Pobac
,null as Prod
,Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) as Op_n
,Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end) as Op_k
,Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) + Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end) as Uk_opr
,case when Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) = 0 then 0 else Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) * 100.00 / Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) end as P_op_n
,case when Sum(case when k.PARIT > 1 then 1 else 0 end) = 0 then 0 else Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end) * 100.00 / Sum(case when k.PARIT > 1 then 1 else 0 end) end as P_op_k
,case when (Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) + Sum(case when k.PARIT > 1 then 1 else 0 end)) = 0 then 0 else (Sum(case when (k.PARIT = 0 or k.PARIT = 1) and p.Id is not null then 1 else 0 end) + Sum(case when k.PARIT > 1 and p.Id is not null then 1 else 0 end)) * 100.00 / (Sum(case when k.PARIT = 0 or k.PARIT = 1 then 1 else 0 end) + Sum(case when k.PARIT > 1 then 1 else 0 end)) end as P_op_uk
,AVG(ISNULL(p.ZIVO,0)) as Zivo
,AVG(ISNULL(p.MRTVO,0)) as Mrtvo
,AVG(ISNULL(p.ZIVO,0)) + AVG(ISNULL(p.MRTVO,0)) as Uk_pr
from Nerast n
join Pripust pr on pr.NerastId = n.Id
join Krmaca k on pr.KrmacaId = k.Id
left join Skart s on k.Id = s.KrmacaId
left join Prasenje p on p.KrmacaId = k.Id and p.PARIT = pr.CIKLUS
left join Pobacaj pob on pob.KrmacaId = k.Id and pob.CIKLUS = pr.CIKLUS
where p.DAT_PRAS >= @DatumOdPrasenje and p.DAT_PRAS <= @DatumDoPrasenje
group by n.Rasa
END
GO


