USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[Registri_Pripusti]    Script Date: 5/7/2022 3:24:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[Registar_Zalucenja]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select 
		ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, z.DAT_ZAL as Dat_zal
		, k.T_KRM as Krmaca
		, k.RASA as Rasa
		, k.MARKICA as Markica
		, SUBSTRING(k.MARKICA, 1, 3) as Id_s
		, SUBSTRING(k.MARKICA, 4,100) as Id_broj
		, z.PARIT as Parit
		, COALESCE(k.HBBROJ,k.RBBROJ,k.NBBROJ) as Hb_rb_n
		, z.ZALUC as Zaluc
		, z.TEZ_ZAL as Tez_zal
		, z.RBZ as Rbz
		, z.DOJARA as Dojara
		, k.ODG as Odg
		, k.MBR_KRM as Mbr_krm
	from Zaluc z
	join Krmaca k on k.Id = z.KrmacaId
	left join Skart s on s.KrmacaId =k.Id
	where z.DAT_ZAL >= @DatumOd and z.DAT_ZAL <= @DatumDo and k.RASA like '%' + @Rasa + '%'

END
GO


