USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[ZahtevZaRegistracijuMuskaGrla]    Script Date: 5/15/2022 10:54:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO














-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[ZahtevZaIzdavanjeDozvoleOKoriscenjuNerastovaUPriplodu]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		, n.T_NER as Nerast
		, n.RASA as Rasa
		, n.MARKICA as Markica
		, case when isnull(n.HBBROJ,'') <> '' then 'HB' when isnull(n.RBBROJ,'') <> '' then 'RB' end as Hr
		, COALESCE(n.HBBROJ,n.RBBROJ) as Broj
		, NULL as Dat_smotre
		, null as Red_s
		, n.DAT_ROD as Dat_rod
		, n.SI1 as Si1
		, n.NAPOMENA as Napomena
		, n.VLA as Vla
		, n.ODG as Odg
		, n.MBR_NER as Mbr_ner
		, 'VOGANJ' as Vlasnik
	from  Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.Id is null or s.D_SKART >= @DatumOd)
END
GO


