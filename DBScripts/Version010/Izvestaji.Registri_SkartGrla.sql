USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[Registri_Prasenja]    Script Date: 5/7/2022 4:21:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[Registri_SkartGrla]
	-- Add the parameters for the stored procedure here
	@DatumOd as Date,
	@DatumDo as Date,
	@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
		,*
	from (
	select 
		 s.D_SKART as D_skart
		, k.T_KRM as T_grla
		, k.RASA as Rasa
		, 'Z' as Pol
		 , 'Krmaca' as Kategorija
		, k.MARKICA as Markica
		, SUBSTRING(k.MARKICA, 1, 3) as Id_s
		, SUBSTRING(k.MARKICA, 4,100) as Id_broj
		, COALESCE(k.HBBROJ,k.RBBROJ,k.NBBROJ) as Hb_rb_n
		, s.RAZL_SK as Razl_sk
		, sf.NAZIV as Razlog_sk
		, k.NAPOMENA as Napomena
		, k.ODG as Odg
		, k.MBR_KRM as Mbr_grla
	from Skart s
	join SifreF sf on s.RAZL_SK = sf.SIFRA and sf.SIFARNIK = 'SKART_K'
	join Krmaca k on k.Id = s.KrmacaId
	where s.D_SKART >= @DatumOd and s.D_SKART <= @DatumDo 
	and k.RASA like '%' + @Rasa + '%'

	union 
	
	select 
		 s.D_SKART as D_skart
		, n.T_NER as T_grla
		, n.RASA as Rasa
		, 'M' as Pol
		 , 'Nerast' as Kategorija
		, n.MARKICA as Markica
		, SUBSTRING(n.MARKICA, 1, 3) as Id_s
		, SUBSTRING(n.MARKICA, 4,100) as Id_broj
		, COALESCE(n.HBBROJ,n.RBBROJ) as Hb_rb_n
		, s.RAZL_SK as Razl_sk
		, sf.NAZIV as Razlog_sk
		, n.NAPOMENA as Napomena
		, n.ODG as Odg
		, n.MBR_NER as Mbr_grla
	from Skart s
	join SifreF sf on s.RAZL_SK = sf.SIFRA and sf.SIFARNIK = 'SKART_N'
	join Nerast n on n.Id = s.NerastId
	where s.D_SKART >= @DatumOd and s.D_SKART <= @DatumDo 
	and n.RASA like '%' + @Rasa + '%'
	
	union 
	
	select 
		 s.D_SKART as D_skart
		, t.T_TEST as T_grla
		, t.RASA as Rasa
		, t.POL as Pol
		 , 'Test' as Kategorija
		, t.MARKICA as Markica
		, SUBSTRING(t.MARKICA, 1, 3) as Id_s
		, SUBSTRING(t.MARKICA, 4,100) as Id_broj
		, NULL as Hb_rb_n
		, s.RAZL_SK as Razl_sk
		, NULL as Razlog_sk
		, t.NAPOMENA as Napomena
		, t.ODG as Odg
		, t.MBR_TEST as Mbr_grla
	from Skart s
	join Test t on t.Id = s.TestId
	where s.D_SKART >= @DatumOd and s.D_SKART <= @DatumDo 
	and t.RASA like '%' + @Rasa + '%'
	
	union 
	
	select 
		 s.D_SKART as D_skart
		, o.T_TEST as T_grla
		, o.RASA as Rasa
		, o.POL as Pol
		, 'Odabir' as Kategorija
		, o.MARKICA as Markica
		, SUBSTRING(o.MARKICA, 1, 3) as Id_s
		, SUBSTRING(o.MARKICA, 4,100) as Id_broj
		, NULL as Hb_rb_n
		, s.RAZL_SK as Razl_sk
		, NULL as Razlog_sk
		, o.NAPOMENA as Napomena
		, o.ODG as Odg
		, o.MBR_TEST as Mbr_grla
	from Skart s
	join Odabir o on o.Id = s.OdabirId
	where s.D_SKART >= @DatumOd and s.D_SKART <= @DatumDo 
	and o.RASA like '%' + @Rasa + '%'

	union 
	
	select 
		 s.D_SKART as D_skart
		, p.T_PRAS as T_grla
		, SUBSTRING(majka.RASA,1,2) + SUBSTRING(otac.RASA,1,2) as Rasa
		, p.POL as Pol
		 , 'Prase' as Kategorija
		, NULL as Markica
		, NULL as Id_s
		, NULL as Id_broj
		, NULL as Hb_rb_n
		, s.RAZL_SK as Razl_sk
		, NULL as Razlog_sk
		, NULL as Napomena
		, majka.ODG as Odg
		, p.MBR_PRAS as Mbr_grla
	from Skart s
	join Prase p on p.Id = s.PraseId
	left join Krmaca majka on p.MBR_MAJKE = majka.MBR_KRM
	left join Nerast otac on p.MBR_OCA = otac.MBR_NER
	where s.D_SKART >= @DatumOd and s.D_SKART <= @DatumDo 
	and SUBSTRING(majka.RASA,1,2) + SUBSTRING(otac.RASA,1,2) like '%' + @Rasa + '%' 
	) as t
END
GO


