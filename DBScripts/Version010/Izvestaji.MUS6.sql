USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[KomisijskiZapisnikZaNerastove]    Script Date: 5/25/2022 11:32:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
















-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[MUS6]
@DatumOdPrasenje as Date,
@DatumDoPrasenje as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
,max(n.T_NER) as Nerast
,max(n.RASA) as Rasa
,max(n.DAT_ROD) as Dat_rod
,max(n.D_P_SKOKA) as D_p_skoka
,Sum(case when p.id is not null then 1 else 0 end) as Br_leg
,Sum(ISNULL(p.ZIVO,0)) as Br_pras
,Sum(case when p.ANOMALIJE is null or p.ANOMALIJE = '' then 0 else 1 end) as Leg_mane
,null as Pr_mane
,null as Hc
,null as Aa
,null as Rp
,null as Lp
,null as Av
,null as He
from Nerast n
join Test t on t.MBR_TEST = n.MBR_NER
join Pripust pr on pr.NerastId = n.Id
join Krmaca k on pr.KrmacaId = k.Id
left join Prasenje p on p.KrmacaId = k.Id and p.PARIT = pr.CIKLUS
where  t.D_TESTA >= @DatumOdPrasenje and t.D_TESTA <= @DatumDoPrasenje
group by n.Id

END
GO


