USE [FarmaProduction]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[KomisijskiSpisakZaNazimice]
@DatumOd as Date,
@IsGrlaUTestu as Bit,
@Rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

if @IsGrlaUTestu = 0

begin

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,max(k.T_KRM) as Nazimica
,max(k.ODG) as Odg
,max(k.VLA) as Vla
,max(k.RASA) as Rasa
,max(k.MARKICA) as Markica
,max(k.DAT_ROD) as Dat_rod
,max(k.BR_SISA) as Br_sisa
,max(k.EKST) as Ekst
,max(kl.KLASA) as Klasa
,max(k.SI1) as Si1
,max(k.GN) as Gn
,max(k.MBR_KRM) as Mbr_krm
,max(k.GNO) as Gno
,max(k.MBR_OCA) as Mbr_oca
,max(k.GNM) as Gnm
,max(k.MBR_MAJKE) as Mbr_majke
,max(p.DAT_PRIP) as Dat_prip
,max(n.T_NER) as Nerast
,max(n.GN) as Gnn
,max(n.MBR_NER) as Mbr_ner
,max(o.T_NER) as Otac
,COALESCE(max(o.HBBROJ),max(o.RBBROJ)) as Hr_o
,max(o.SI1) as Si_o
,max(o.KLASA) as Kl_o
,max(m.T_KRM) as Majka
,COALESCE(max(m.HBBROJ),max(m.RBBROJ),max(m.NBBROJ)) as Hr_m
,max(m.SI1) as Si_m
,max(m.KLASA) as Kl_m
,max(k.NAPOMENA) as Napomena
,max(kl.DAT_SMOTRE) as Dat_smotre
from Krmaca k 
left join Skart s on k.Id = s.KrmacaId
join Klasa kl on kl.MBR_GRLA = k.MBR_KRM
join Pripust p on p.KrmacaId = k.Id
left join Nerast n on p.NerastId = n.Id
left join Nerast o on o.MBR_NER =k.MBR_OCA
left join Krmaca m on m.MBR_KRM = k.MBR_MAJKE
where kl.DAT_SMOTRE = @DatumOd and k.RASA like '%' + @Rasa +'%' and (k.PARIT = 0 or k.PARIT = 1) and (s.Id is not null or s.D_SKART > @DatumOd)
group by k.Id

END

ELSE

BEGIN


select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,max(t.T_TEST) as Nazimica
,max(t.ODG) as Odg
,max(t.VLA) as Vla
,max(t.RASA) as Rasa
,max(t.MARKICA) as Markica
,max(t.DAT_ROD) as Dat_rod
,max(t.BR_SISA) as Br_sisa
,max(t.EKST) as Ekst
,max(kl.KLASA) as Klasa
,max(t.SI1) as Si1
,max(t.GN) as Gn
,max(t.MBR_TEST) as Mbr_krm
,max(t.GNO) as Gno
,max(t.MBR_OCA) as Mbr_oca
,max(t.GNM) as Gnm
,max(t.MBR_MAJKE) as Mbr_majke
,null as Dat_prip
,null as Nerast
,null as Gnn
,null as Mbr_ner
,max(o.T_NER) as Otac
,COALESCE(max(o.HBBROJ),max(o.RBBROJ)) as Hr_o
,max(o.SI1) as Si_o
,max(o.KLASA) as Kl_o
,max(m.T_KRM) as Majka
,COALESCE(max(m.HBBROJ),max(m.RBBROJ),max(m.NBBROJ)) as Hr_m
,max(m.SI1) as Si_m
,max(m.KLASA) as Kl_m
,max(t.NAPOMENA) as Napomena
,max(kl.DAT_SMOTRE) as Dat_smotre
from Test t 
left join Skart s on t.Id = s.TestId
join Klasa kl on kl.MBR_GRLA = t.MBR_TEST
left join Nerast o on o.MBR_NER = t.MBR_OCA
left join Krmaca m on m.MBR_KRM = t.MBR_MAJKE
where kl.DAT_SMOTRE = @DatumOd and t.RASA like '%' + @Rasa +'%' and (s.Id is not null or s.D_SKART > @DatumOd) AND t.POL = 'Z'

END

END
GO


