USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [Izvestaji].[IzvodIzGlavneMaticneEvidencije_AnalizaHbRbNb]    Script Date: 5/3/2022 10:33:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Izvestaji].[IzvodIzGlavneMaticneEvidencije_AnalizaRasa]
	-- Add the parameters for the stored procedure here
	@Dan as Date,
	@Paritet as int,
	@IsHBRB as Bit,
	@IsNB as Bit,
	@IsNerastovi as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if	@IsHBRB = 1 and @IsNB = 1 and @IsNerastovi = 1
	begin

select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'Z' as Pol
,MAX(k.Rasa) as Rasa
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet 
group by k.Id) as tbl
group by Pol,Rasa

union 

select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'M' as Pol
,MAX(n.Rasa) as Rasa
from Nerast n
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id) as tbl
group by Pol,Rasa

 END

 ELSE IF @IsHBRB = 1 and @IsNB = 1 and @IsNerastovi = 0

 BEGIN
 select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,'Z' as Pol
,MAX(k.Rasa) as Rasa
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet 
group by k.Id) as tbl
group by Pol,Rasa

 END

 else if	@IsHBRB = 1 and @IsNB = 0 and @IsNerastovi = 1
	begin
 select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'Z' as Pol
,MAX(k.Rasa) as Rasa
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet and ((k.HBBROJ is not null and  k.HBBROJ <> '')  or (k.RBBROJ is not null and k.RBBROJ <> ''))
group by k.Id) as tbl
group by Pol,Rasa

union 

 select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'M' as Pol
,MAX(n.Rasa) as Rasa
from Nerast n
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id) as tbl
group by Pol,Rasa

 END

  else if	@IsHBRB = 1 and @IsNB = 0 and @IsNerastovi = 0
	begin
 select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'Z' as Pol
,MAX(k.Rasa) as Rasa
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet and ((k.HBBROJ is not null and  k.HBBROJ <> '')  or (k.RBBROJ is not null and k.RBBROJ <> ''))
group by k.Id) as tbl
group by Pol,Rasa

 END
 
else if	@IsHBRB = 0 and @IsNB = 1 and @IsNerastovi = 1
	begin
 select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'Z' as Pol
,MAX(k.Rasa) as Rasa
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet and (k.HBBROJ is null or  k.HBBROJ = '')  and (k.RBBROJ is null or  k.RBBROJ = '') and k.NBBROJ is not null and k.NBBROJ <> ''
group by k.Id) as tbl
group by Pol,Rasa

union 

 select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'M' as Pol
,MAX(n.Rasa) as Rasa
from Nerast n
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id) as tbl
group by Pol,Rasa

 END

 ELSE IF @IsHBRB = 0 and @IsNB = 1 and @IsNerastovi = 0

 BEGIN
  select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'Z' as Pol
,MAX(k.Rasa) as Rasa
from Krmaca k
left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = k.MBR_OCA
left join Prasenje p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @Dan) and k.Ciklus	<= @Paritet  and (k.HBBROJ is null or  k.HBBROJ = '')  and (k.RBBROJ is null or  k.RBBROJ = '')  and k.NBBROJ is not null and k.NBBROJ <> ''
group by k.Id) as tbl
group by Pol,Rasa

 END

 else if	@IsHBRB = 0 and @IsNB = 0 and @IsNerastovi = 1
	begin

  select Pol, Rasa, count(*) as Broj,  case when sum(count(*)) OVER () = 0 then 0 else  count(*) * 100.00 / sum(count(*)) OVER () end as Procenat
from 
(select 
'M' as Pol
,MAX(n.Rasa) as Rasa
from Nerast n
left join Krmaca majka on majka.MBR_KRM = n.MBR_MAJKE
left join Nerast otac on otac.MBR_NER = n.MBR_OCA
left join Skart s on s.NerastId = n.Id
where (s.D_SKART is null or s.D_SKART >= @Dan)
group by n.Id) as tbl
group by Pol,Rasa

 END

END
GO


