USE [FarmaProduction]
GO

INSERT INTO [dbo].[GrupeSifaraSkartas]
           ([Name]
           ,[SifraGrupe]
           ,[DateCreated]
           ,[DateModified])
     VALUES
           ('Reproduktivni razlozi'
           ,'0'
           ,GETDATE()
           ,GETDATE()),
           ('Zdravstveni razlozi'
           ,'1'
           ,GETDATE()
           ,GETDATE()),
           ('Selekcijski razlozi'
           ,'3'
           ,GETDATE()
           ,GETDATE()),
           ('Prinudna klanja'
           ,'7'
           ,GETDATE()
           ,GETDATE()),
           ('Uginuca'
           ,'8'
           ,GETDATE()
           ,GETDATE())
GO


