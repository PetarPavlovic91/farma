USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[OprasivostPoOsemeniteljima]    Script Date: 12/1/2021 11:50:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[OprasivostPoOsemeniteljima]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
If(OBJECT_ID('tempdb..#Os_nT') Is Not Null)
Begin
    Drop Table #Os_nT
End

If(OBJECT_ID('tempdb..#Os_kT') Is Not Null)
Begin
    Drop Table #Os_kT
End

If(OBJECT_ID('tempdb..#PobT') Is Not Null)
Begin
    Drop Table #PobT
End

If(OBJECT_ID('tempdb..#OprasenoT') Is Not Null)
Begin
    Drop Table #OprasenoT
End

If(OBJECT_ID('tempdb..#SkartT') Is Not Null)
Begin
    Drop Table #SkartT
End

If(OBJECT_ID('tempdb..#Op_nT') Is Not Null)
Begin
    Drop Table #Op_nT
End

If(OBJECT_ID('tempdb..#Op_kT') Is Not Null)
Begin
    Drop Table #Op_kT
End

If(OBJECT_ID('tempdb..#PovadT') Is Not Null)
Begin
    Drop Table #PovadT
End

create table #Os_nT
(
    OSEM int,
	Os_n int
)

insert into #Os_nT
select p.OSEM, count(*) as Os_n
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by p.OSEM


create table #Os_kT
(
    OSEM int,
	Os_k int
)

insert into #Os_kT
select p.OSEM, count(*) as Os_k
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by p.OSEM



create table #PobT
(
    OSEM int,
	Pob int
)

insert into #PobT
select p.OSEM, count(*) as Pob
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Pobacaj pob on pob.KrmacaId = k.Id 
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #OprasenoT
(
    OSEM int,
	Opraseno int,
	Zivo int,
	Mrtvo int
)

insert into #OprasenoT
select p.OSEM, count(*) as Opraseno, SUM(pr.ZIVO) as Zivo, SUM(pr.MRTVO) as Mrtvo
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #SkartT
(
    OSEM int,
	Skart int
)

insert into #SkartT
select p.OSEM, count(*) as Skart
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #Op_nT
(
    OSEM int,
	Op_n int
)

insert into #Op_nT
select p.OSEM, count(*) as Op_n
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by p.OSEM


create table #Op_kT
(
    OSEM int,
	Op_k int
)

insert into #Op_kT
select p.OSEM, count(*) as Op_k
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by p.OSEM


create table #PovadT
(
    OSEM int,
	Povad int
)

insert into #PovadT
select p.OSEM, count(*) as Povad
from  Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.RAZL_P = 2
group by p.OSEM

select 
osk.OSEM,
osk.Os_k,
osn.Os_n,
osk.Os_k + osn.Os_n as Uk_os,
pov.Povad as Povad,
osk.Os_k + osn.Os_n - o.Opraseno as Ne_op,
s.Skart as Isklj,
pob.Pob as Pobac,
0 as Prod,
opn.Op_n,
opk.Op_k,
opn.Op_n + opk.Op_k as Uk_opr,
opn.Op_n / osn.Os_n * 100  as P_op_n,
opk.Op_k / osk.Os_k * 100  as P_op_k,
(opk.Op_k + opn.Op_n) / (osk.Os_k + osn.Os_n) * 100  as P_op_uk,
o.Zivo / opk.Op_k as Zivo,
o.Mrtvo / opk.Op_k as Mrtvo,
(o.Zivo + o.Mrtvo) / opk.Op_k as Uk_p
from #Os_kT osk
join #Os_nT osn on osn.OSEM = osk.OSEM
join #PobT pob on pob.OSEM = osk.OSEM
join #SkartT s on s.OSEM = osk.OSEM
join #OprasenoT o on o.OSEM = osk.OSEM
join #Op_nT opn on opn.OSEM = osk.OSEM
join #Op_kT opk on opk.OSEM = osk.OSEM
join #PovadT pov on pov.OSEM = osk.OSEM
END
GO


