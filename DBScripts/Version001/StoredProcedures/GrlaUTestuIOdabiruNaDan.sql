USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[GrlaUTestuIOdabiruNaDan]    Script Date: 12/21/2021 10:17:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GrlaUTestuIOdabiruNaDan]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@brDana as int,
	@nazimice as bit,
	@testirane as bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @nazimice = 1 and @testirane = 1
begin

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,T_TEST as Grlo
,Rasa
,Dat_rod 
,Markica 
,MBR_OCA as Otac
,MBR_MAJKE as Majka
,DATEDIFF(day,Dat_rod, @dan) as Starost 
,D_TESTA as D_testa
,EKST as Ekst
,BR_SISA as Br_sisa 
,TEZINA as Tezina 
,(TEZINA * 1000) / DATEDIFF(day,Dat_rod, @dan) as Prirast
,SI1 as Si1
from Test
where DATEDIFF(day,Dat_rod, @dan) <= @brDana and DATEDIFF(day,Dat_rod, @dan) > 0 and  POL = 'Z'
end


if @nazimice = 1 and @testirane = 0
begin

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,T_TEST as Grlo
,Rasa
,Dat_rod 
,Markica 
,MBR_OCA as Otac
,MBR_MAJKE as Majka
,DATEDIFF(day,Dat_rod, @dan) as Starost 
,D_ODABIRA as D_testa
,EKST as Ekst
,BR_SISA as Br_sisa 
,TEZINA as Tezina 
,(TEZINA * 1000) / DATEDIFF(day,Dat_rod, @dan) as Prirast
from Odabir
where DATEDIFF(day,Dat_rod, @dan) <= @brDana and DATEDIFF(day,Dat_rod, @dan) > 0 and  POL = 'Z'
end

if @nazimice = 0 and @testirane = 1
begin

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,T_TEST as Grlo
,Rasa
,Dat_rod 
,Markica 
,MBR_OCA as Otac
,MBR_MAJKE as Majka
,DATEDIFF(day,Dat_rod, @dan) as Starost 
,D_TESTA as D_testa
,EKST as Ekst
,BR_SISA as Br_sisa 
,TEZINA as Tezina 
,(TEZINA * 1000) / DATEDIFF(day,Dat_rod, @dan) as Prirast
,SI1 as Si1
from Test
where DATEDIFF(day,Dat_rod, @dan) <= @brDana and DATEDIFF(day,Dat_rod, @dan) > 0 and POL = 'M'
end


if @nazimice = 0 and @testirane = 0
begin

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
,T_TEST as Grlo
,Rasa
,Dat_rod 
,Markica 
,MBR_OCA as Otac
,MBR_MAJKE as Majka
,DATEDIFF(day,Dat_rod, @dan) as Starost 
,D_ODABIRA as D_testa
,EKST as Ekst
,BR_SISA as Br_sisa 
,TEZINA as Tezina 
,(TEZINA * 1000) / DATEDIFF(day,Dat_rod, @dan) as Prirast
from Odabir
where DATEDIFF(day,Dat_rod, @dan) <= @brDana and DATEDIFF(day,Dat_rod, @dan) > 0 and  POL = 'M'
end

END
GO


