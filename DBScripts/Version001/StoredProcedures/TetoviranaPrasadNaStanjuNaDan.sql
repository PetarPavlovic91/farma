USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[TetoviranaPrasadNaStanjuNaDan]    Script Date: 12/22/2021 2:33:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[TetoviranaPrasadNaStanjuNaDan]
	-- Add the parameters for the stored procedure here
	@prasadNaStanjuNaDan as Date,
	@starostiDoDana as int,
	@pol as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @pol ='M' or @pol = 'Z'
begin
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb,
T_PRAS as Tetov,
POL as Pol,
CONCAT(SUBSTRING(n.RASA,1,2), SUBSTRING(k.RASA,1,2)) as Rasa,
p.Dat_rod,
DATEDIFF(day, p.Dat_rod, @prasadNaStanjuNaDan) as Starost,
n.T_NER as Otac,
k.T_KRM as Majka
from Prase p
left join Nerast n on p.NerastId = n.Id
left join Krmaca k on p.KrmacaId = k.Id
where p.DAT_ROD <= @prasadNaStanjuNaDan and DATEDIFF(day, p.Dat_rod, @prasadNaStanjuNaDan) <= @starostiDoDana and DATEDIFF(day, p.Dat_rod, @prasadNaStanjuNaDan) > 0 and p.POL = @pol
end
else
begin
select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb,
T_PRAS as Tetov,
POL as Pol,
CONCAT(SUBSTRING(n.RASA,1,2), SUBSTRING(k.RASA,1,2)) as Rasa,
p.Dat_rod,
DATEDIFF(day, p.Dat_rod, @prasadNaStanjuNaDan) as Starost,
n.T_NER as Otac,
k.T_KRM as Majka
from Prase p
left join Nerast n on p.NerastId = n.Id
left join Krmaca k on p.KrmacaId = k.Id
where p.DAT_ROD <= @prasadNaStanjuNaDan and DATEDIFF(day, p.Dat_rod, @prasadNaStanjuNaDan) <= @starostiDoDana and DATEDIFF(day, p.Dat_rod, @prasadNaStanjuNaDan) > 0
end
END
GO


