USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmace]    Script Date: 12/30/2021 12:39:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmace]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max),
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'
 
If(OBJECT_ID('tempdb..#RasaKrmace') Is Not Null)
Begin
    Drop Table #RasaKrmace
End

create table #RasaKrmace
(
    Rasa nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%');

insert into #RasaKrmace
select 
case when RASA is null then '' else RASA end as Rasa,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%'
group by RASA


select 
case 
	when k.Rasa <> ''
	then k.Rasa
	else ''
end as Rasa,
k.Krm,
k.Pr_k
from #RasaKrmace k

END
GO


