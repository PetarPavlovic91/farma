USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PrenosPodatakaIzOdabiraUtest]    Script Date: 12/15/2021 1:29:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PrenosPodatakaIzOdabiraUtest]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date, 
	@datumTesta as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
Id
,@datumTesta as D_testa
,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Rb
,POL as Pol
,GN as Gn
,T_TEST as T_test 
,Vla
,Odg
,Rasa
,Mbr_test 
,Dat_rod
,Mbr_oca
,Mbr_majke
,Iz_legla 
,Br_sisa
from Odabir
where D_ODABIRA >= @dateTimeFrom and D_ODABIRA <=  @dateTimeTo and (IsPrenesenUTest is null or IsPrenesenUTest = 0)
 


END
GO


