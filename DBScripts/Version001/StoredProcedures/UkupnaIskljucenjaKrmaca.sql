USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[UkupnaIskljucenjaKrmaca]    Script Date: 12/10/2021 2:37:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UkupnaIskljucenjaKrmaca]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
select 
sk.RAZL_SK as Sif,
sk.RAZL_SK + ' ' + sf.NAZIV as Razlog,
sk.Pr_01,  
sk.Pr_02,  
sk.Pr_03,  
sk.Pr_04,  
sk.Pr_05,  
sk.Pr_06,  
sk.Pr_07,  
sk.Pr_08,  
sk.Pr_09,  
sk.Pr_10,
sk.Pr_11,
sk.Ukupno
from
(select 
s.RAZL_SK,
sum(case when k.parit = 1 then 1 else 0 end) as Pr_01,  
sum(case when k.parit = 2 then 1 else 0 end) as Pr_02,  
sum(case when k.parit = 3 then 1 else 0 end) as Pr_03,  
sum(case when k.parit = 4 then 1 else 0 end) as Pr_04,  
sum(case when k.parit = 5 then 1 else 0 end) as Pr_05,  
sum(case when k.parit = 6 then 1 else 0 end) as Pr_06,  
sum(case when k.parit = 7 then 1 else 0 end) as Pr_07,  
sum(case when k.parit = 8 then 1 else 0 end) as Pr_08,  
sum(case when k.parit = 9 then 1 else 0 end) as Pr_09,  
sum(case when k.parit = 10 then 1 else 0 end) as Pr_10,
sum(case when k.parit = 11 then 1 else 0 end) as Pr_11,
count(*) as Ukupno
from Skart s
join Krmaca k on k.Id = s.KrmacaId
where s.D_SKART >= @dateTimeFrom and s.D_SKART <= @dateTimeTo
group by s.RAZL_SK) sk
left join SifreF sf on sk.RAZL_SK = sf.SIFRA and sf.SIFARNIK = 'SKART_K'


 END
GO


