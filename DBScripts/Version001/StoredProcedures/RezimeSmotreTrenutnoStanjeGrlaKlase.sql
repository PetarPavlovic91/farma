USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaKlase]    Script Date: 12/22/2021 1:04:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaKlase]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'
 
  
If(OBJECT_ID('tempdb..#KlasaNerast') Is Not Null)
Begin
    Drop Table #KlasaNerast
End

create table #KlasaNerast
(
    Klasa nvarchar(max), 
    Ner int, 
    Pr_n decimal(5,2)
)

declare @allRowsNerast as int;
 
 set @allRowsNerast = (select count(*) 
				from Nerast n
				left join Skart s on s.NerastId = n.Id
				where (s.D_SKART is null) and RASA like '%' + @rasa + '%');

	insert into #KlasaNerast
	select 
case when KLASA is null then '' else KLASA end as Klasa,
	count(*) as Ner,
	(count(*) * 100.00) / @allRowsNerast AS Pr_n
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null) and RASA like '%' + @rasa + '%'
	group by KLASA



If(OBJECT_ID('tempdb..#KlasaKrmace') Is Not Null)
Begin
    Drop Table #KlasaKrmace
End

create table #KlasaKrmace
(
    Klasa nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null) and k.CIKLUS > 1 and RASA like '%' + @rasa + '%');

insert into #KlasaKrmace
select 
case when KLASA is null then '' else KLASA end as Klasa,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and k.CIKLUS > 1 and RASA like '%' + @rasa + '%'
group by KLASA


If(OBJECT_ID('tempdb..#KlasaNazimice') Is Not Null)
Begin
    Drop Table #KlasaNazimice
End

create table #KlasaNazimice
(
    Klasa nvarchar(max), 
    Naz int, 
    Pr_naz decimal(5,2)
)

declare @allRowsNazimice as int;
 
 set @allRowsNazimice = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null) and k.CIKLUS = 1 and RASA like '%' + @rasa + '%');

insert into #KlasaNazimice
select 
case when KLASA is null then '' else KLASA end as Klasa,
count(*) as Naz,
(count(*) * 100.00) / @allRowsNazimice AS Pr_naz
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and k.CIKLUS = 1 and RASA like '%' + @rasa + '%'
group by KLASA

select 
case 
	when n.Klasa <> ''
	then n.Klasa 
	when k.Klasa <> ''
	then k.Klasa
	when naz.Klasa <> ''
	then naz.Klasa
	else ''
end as Klasa,
n.Ner,
n.Pr_n,
k.Krm,
k.Pr_k,
naz.Naz,
naz.Pr_naz
from #KlasaNerast n
full join #KlasaKrmace k on k.Klasa = n.Klasa
full join #KlasaNazimice naz on naz.Klasa = k.Klasa

END
GO


