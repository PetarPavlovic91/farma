USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaParitet]    Script Date: 12/22/2021 3:34:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaParitet]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'
 

If(OBJECT_ID('tempdb..#ParitetKrmace') Is Not Null)
Begin
    Drop Table #ParitetKrmace
End

create table #ParitetKrmace
(
    Paritet nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null) and RASA like '%' + @rasa + '%');

insert into #ParitetKrmace
select 
PARIT as Paritet,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and RASA like '%' + @rasa + '%'
group by PARIT

select 
k.Paritet,
k.Krm,
k.Pr_k
from  #ParitetKrmace k

END
GO


