USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmace]    Script Date: 12/30/2021 12:36:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmace]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max),
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


If(OBJECT_ID('tempdb..#KlasaKrmace') Is Not Null)
Begin
    Drop Table #KlasaKrmace
End

create table #KlasaKrmace
(
    Klasa nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%');

insert into #KlasaKrmace
select 
case when KLASA is null then '' else KLASA end as Klasa,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%'
group by KLASA


select 
case 
	when k.Klasa <> ''
	then k.Klasa
	else ''
end as Klasa,
k.Krm,
k.Pr_k
from #KlasaKrmace k

END
GO


