USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PregledAktivnihNerastovaNaDanRasnaStruktura]    Script Date: 12/20/2021 11:49:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PregledAktivnihNerastovaNaDanRasnaStruktura]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@sviNerastovi as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
declare @allRows as int;

if @sviNerastovi = 1
  begin

   set @allRows = (select count(*) 
				from Nerast n
				left join Skart s on s.NerastId = n.Id
				where (s.D_SKART is null or s.D_SKART >= @dan));

	select 
	RASA as Rasa,
	count(*) as Broj,
	(count(*) * 100.00) / @allRows AS Procenat
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan)
	group by RASA
  end
else
  begin

  set @allRows = (select count(*) 
				from Nerast n
				left join Skart s on s.NerastId = n.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and substring(n.MBR_NER,1,3) = '092');

	select 
	RASA as Rasa,
	count(*) as Broj,
	(count(*) * 100.00) / @allRows AS Procenat
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and substring(n.MBR_NER,1,3) = '092'
	group by RASA
 end


END
GO


