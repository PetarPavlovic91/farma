USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KontrolaProduktivnostiOsemenitelja]    Script Date: 12/1/2021 11:49:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KontrolaProduktivnostiOsemenitelja]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
If(OBJECT_ID('tempdb..#Os_nT') Is Not Null)
Begin
    Drop Table #Os_nT
End

If(OBJECT_ID('tempdb..#Os_kT') Is Not Null)
Begin
    Drop Table #Os_kT
End

If(OBJECT_ID('tempdb..#PobT') Is Not Null)
Begin
    Drop Table #PobT
End

If(OBJECT_ID('tempdb..#OprasenoT') Is Not Null)
Begin
    Drop Table #OprasenoT
End

If(OBJECT_ID('tempdb..#SkartT') Is Not Null)
Begin
    Drop Table #SkartT
End

If(OBJECT_ID('tempdb..#Op_nT') Is Not Null)
Begin
    Drop Table #Op_nT
End

If(OBJECT_ID('tempdb..#Op_kT') Is Not Null)
Begin
    Drop Table #Op_kT
End

create table #Os_nT
(
    OSEM nvarchar(max),
	Os_n int
)

insert into #Os_nT
select p.OSEM, count(*) as Os_n
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= GETDATE() and p.DAT_PRIP <= GETDATE() and p.CIKLUS = 1
group by p.OSEM


create table #Os_kT
(
    OSEM nvarchar(max),
	Os_k int
)

insert into #Os_kT
select p.OSEM, count(*) as Os_k
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by p.OSEM



create table #PobT
(
    OSEM nvarchar(max),
	Pob int
)

insert into #PobT
select p.OSEM, count(*) as Pob
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Pobacaj pob on pob.KrmacaId = k.Id 
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #OprasenoT
(
    OSEM nvarchar(max),
	Opraseno int,
	Zivo int,
	Mrtvo int
)

insert into #OprasenoT
select p.OSEM, count(*) as Opraseno, SUM(pr.ZIVO) as Zivo, SUM(pr.MRTVO) as Mrtvo
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #SkartT
(
    OSEM nvarchar(max),
	Skart int
)

insert into #SkartT
select p.OSEM, count(*) as Skart
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.OSEM


create table #Op_nT
(
    OSEM nvarchar(max),
	Op_n int
)

insert into #Op_nT
select p.OSEM, count(*) as Op_n
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS = 1
group by p.OSEM


create table #Op_kT
(
    OSEM nvarchar(max),
	Op_k int
)

insert into #Op_kT
select p.OSEM, count(*) as Op_k
from Nerast n
join Pripust p on p.NerastId = n.Id
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.CIKLUS > 1
group by p.OSEM


select 
osk.OSEM as Osem,
osk.Os_k,
osn.Os_n,
osk.Os_k + osn.Os_n as Uk_os,
pob.Pob as Povad,
osk.Os_k + osn.Os_n - o.Opraseno as Ne_op,
s.Skart as Isklj,
0 as Pobac,
0 as Prod,
opn.Op_n,
opk.Op_k,
opn.Op_n + opk.Op_k as Uk_opr,
opn.Op_n / osn.Os_n * 100  as P_op_n,
opk.Op_k / osk.Os_k * 100  as P_op_k,
(opk.Op_k + opn.Op_n) / (osk.Os_k + osn.Os_n) * 100  as P_op_uk,
o.Zivo / opk.Op_k as Zivo,
o.Mrtvo / opk.Op_k as Mrtvo,
(o.Zivo + o.Mrtvo) / opk.Op_k as Uk_p
from #Os_kT osk
join #Os_nT osn on osn.OSEM = osk.OSEM
join #PobT pob on pob.OSEM = osk.OSEM
join #SkartT s on s.OSEM = osk.OSEM
join #OprasenoT o on o.OSEM = osk.OSEM
join #Op_nT opn on opn.OSEM = osk.OSEM
join #Op_kT opk on opk.OSEM = osk.OSEM




END
GO


