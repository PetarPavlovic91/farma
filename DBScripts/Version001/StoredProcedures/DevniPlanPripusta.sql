USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[DevniPlanPripusta]    Script Date: 12/8/2021 4:44:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DevniPlanPripusta]
	-- Add the parameters for the stored procedure here
	@datumZalucenja as Date,
	@t_KrmParam as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + T_NER + ']',
    '[' + T_NER + ']'
  )
  FROM
  ( select distinct T_NER
	FROM Nerast) n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select 
	k.T_KRM, 
	k.RASA, 
	n.T_NER,
	CASE
		WHEN p.MB_OCA = n.MBR_NER
		THEN 0
		WHEN p.MBM_DEDE = n.MBR_NER
		THEN 0
		WHEN p.MBMB_PRAD = n.MBR_NER
		THEN 0
		WHEN p.MBMD_PRAD = n.MBR_NER
		THEN 0
		WHEN p.MBO_DEDE = n.MBR_NER
		THEN 0
		WHEN p.MBOB_PRAD = n.MBR_NER
		THEN 0
		WHEN p.MBOD_PRAD = n.MBR_NER
		THEN 0
		ELSE 1
	END as Ukrstanje
	from Krmaca k
	join Pedigre p on k.MBR_KRM = p.MBR_GRLA
	join Zaluc z on z.KrmacaId = k.Id
	cross join Nerast n
	where z.DAT_ZAL >= '''+ CONVERT(NVARCHAR(MAX), @datumZalucenja)  +''' 
			and z.DAT_ZAL <=  ''' +  CONVERT(NVARCHAR(MAX), DATEADD(day, 1, @datumZalucenja)) +''' and k.T_KRM = ' + @t_KrmParam +') as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'


EXECUTE(@PivotTableSQL)

END
GO


