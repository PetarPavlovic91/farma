USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PregledNazimicaGrlaZaOsiguranje]    Script Date: 12/18/2021 12:00:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PregledNazimicaGrlaZaOsiguranje]
	-- Add the parameters for the stored procedure here
	@dateOd as Date, 
	@dateDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
 select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
, k.TETOVIRK10 as Nazimica
, k.RASA as Rasa
, k.MBR_KRM as Mbr_krm
, k.DAT_ROD as Dat_rod
, k.MARKICA as Markica
, case when k.HBBROJ is not null then k.HBBROJ else k.RBBROJ end as Hb_rb_broj
 from Krmaca k 
 join Pripust p on p.KrmacaId = k.Id
 where k.CIKLUS = 1 and p.DAT_PRIP >= @dateOd and p.DAT_PRIP <= @dateDo

END
GO


