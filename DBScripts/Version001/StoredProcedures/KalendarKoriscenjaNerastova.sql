USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KalendarKoriscenjaNerastova]    Script Date: 12/8/2021 4:54:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KalendarKoriscenjaNerastova]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + n.T_NER + ']',
    '[' + n.T_NER + ']'
  )
  from ( select distinct n.T_NER
FROM Pripust p 
join Nerast n on p.NerastId = n.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo) n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select p.DAT_PRIP, n.T_NER, 1 as Pripust
	from  Pripust p 
	join Nerast n on p.NerastId = n.Id
	where p.DAT_PRIP >= ''' + CONVERT(NVARCHAR(MAX), @dateTimeFrom)  + ''' and p.DAT_PRIP <= ''' + CONVERT(NVARCHAR(MAX), @dateTimeTo)  + ''') as DataTable
PIVOT
(
Sum(Pripust)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'

EXECUTE(@PivotTableSQL)

 
END
GO


