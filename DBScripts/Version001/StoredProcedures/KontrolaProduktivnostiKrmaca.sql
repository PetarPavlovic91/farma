USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KontrolaProduktivnostiKrmaca]    Script Date: 12/2/2021 12:17:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KontrolaProduktivnostiKrmaca]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
k.RASA as Genotip, 
count(distinct k.Id) Br_krm,
count(*) Br_leg,
118.5 as Supra,
SUM(p.ZIVO)/ count(*) as Zivo,
SUM(p.MRTVO)/ count(*) as Mrtvo,
SUM(p.ZIVO)/ count(*) + SUM(p.MRTVO)/ count(*) as Ukupno,
SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) as Zal_l,
AVG(DATEDIFF(day, p.DAT_PRAS, z.DAT_ZAL)) as Lak,
SUM(z.ZALUC)/SUM(CASE WHEN z.Id is not null THEN 1 ELSE 0 END) as Zal_p,
SUM(p.TEZ_LEG)/count(*) as Tez_l,
(SUM(p.TEZ_LEG)/count(*))/(SUM(p.ZIVO)/ count(*)) as Tez_p
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id and p.DAT_PRAS >= @dateTimeFrom and p.DAT_PRAS <= @dateTimeTo
left join Zaluc z on z.KrmacaId = k.Id and z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo
group by k.RASA

END
GO


