USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[SmotraNerastovaRadniZapisnik]    Script Date: 12/28/2021 10:00:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SmotraNerastovaRadniZapisnik]
	-- Add the parameters for the stored procedure here
	@pripustOd as Date,
	@pripustDo as Date,
	@sviNerastovi as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @sviNerastovi = 1
  begin
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
	,Nerast
	,RASA
	,ODG
	,DAT_ROD
	,NA_FARM_OD
	,BR_SISA
	,EKST
	,Klasa
	,SI1
	,NAPOMENA
	,Osem
	,Opras
	,Pr_opr
	,Zivo
	,Mrtvo
	,Ukupno
	from
	(select 
	n.Id
	,T_NER as Nerast
	,RASA
	,ODG
	,DAT_ROD
	,NA_FARM_OD
	,BR_SISA
	,EKST
	,Klasa
	,SI1
	,NAPOMENA
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null)
	) Ner join
	(select 
	n.Id
	, count(*) as Osem
	, SUM(CASE when pr.PARIT > 0 then 1 else 0 end) as Opras
	, case when count(*) <> 0 then (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 100.00) / (count(*) * 1.00) else 0 end as Pr_opr
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Zivo
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Mrtvo
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Ukupno
	from Nerast n
	join Pripust p on p.NerastId = n.Id or p.Nerast2Id = n.Id
	left join Prasenje pr on p.KrmacaId = pr.KrmacaId and p.CIKLUS = pr.PARIT
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null) and p.DAT_PRIP >= @pripustOd and p.DAT_PRIP <= @pripustDo
	group by n.Id) Grouped on Ner.Id = Grouped.Id
 end
else
 begin 
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Rb
	,Nerast
	,RASA
	,ODG
	,DAT_ROD
	,NA_FARM_OD
	,BR_SISA
	,EKST
	,Klasa
	,SI1
	,NAPOMENA
	,Osem
	,Opras
	,Pr_opr
	,Zivo
	,Mrtvo
	,Ukupno
	from
	(select 
	n.Id
	,T_NER as Nerast
	,RASA
	,ODG
	,DAT_ROD
	,NA_FARM_OD
	,BR_SISA
	,EKST
	,Klasa
	,SI1
	,NAPOMENA
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null)
	) Ner join
	(select 
	n.Id
	, count(*) as Osem
	, SUM(CASE when pr.PARIT > 0 then 1 else 0 end) as Opras
	, case when count(*) <> 0 then (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 100.00) / (count(*) * 1.00) else 0 end as Pr_opr
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Zivo
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Mrtvo
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Ukupno
	from Nerast n
	join Pripust p on p.NerastId = n.Id or p.Nerast2Id = n.Id
	left join Prasenje pr on p.KrmacaId = pr.KrmacaId and p.CIKLUS = pr.PARIT
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null) and p.DAT_PRIP >= @pripustOd and p.DAT_PRIP <= @pripustDo  and substring(n.MBR_NER,1,3) = '092'
	group by n.Id) Grouped on Ner.Id = Grouped.Id
 end


END
GO


