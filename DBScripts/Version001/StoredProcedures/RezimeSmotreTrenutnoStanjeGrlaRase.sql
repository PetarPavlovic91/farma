USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaRase]    Script Date: 12/22/2021 3:22:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaRase]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'
 
  
If(OBJECT_ID('tempdb..#RasaNerast') Is Not Null)
Begin
    Drop Table #RasaNerast
End

create table #RasaNerast
(
    Rasa nvarchar(max), 
    Ner int, 
    Pr_n decimal(5,2)
)

declare @allRowsNerast as int;
 
 set @allRowsNerast = (select count(*) 
				from Nerast n
				left join Skart s on s.NerastId = n.Id
				where (s.D_SKART is null) and RASA like '%' + @rasa + '%');

	insert into #RasaNerast
	select 
case when RASA is null then '' else RASA end as Rasa,
	count(*) as Ner,
	(count(*) * 100.00) / @allRowsNerast AS Pr_n
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null) and RASA like '%' + @rasa + '%'
	group by RASA



If(OBJECT_ID('tempdb..#RasaKrmace') Is Not Null)
Begin
    Drop Table #RasaKrmace
End

create table #RasaKrmace
(
    Rasa nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null) and k.CIKLUS > 1 and RASA like '%' + @rasa + '%');

insert into #RasaKrmace
select 
case when RASA is null then '' else RASA end as Rasa,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and k.CIKLUS > 1 and RASA like '%' + @rasa + '%'
group by RASA


If(OBJECT_ID('tempdb..#RasaNazimice') Is Not Null)
Begin
    Drop Table #RasaNazimice
End

create table #RasaNazimice
(
    Rasa nvarchar(max), 
    Naz int, 
    Pr_naz decimal(5,2)
)

declare @allRowsNazimice as int;
 
 set @allRowsNazimice = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null) and k.CIKLUS = 1 and RASA like '%' + @rasa + '%');

insert into #RasaNazimice
select 
case when RASA is null then '' else RASA end as Rasa,
count(*) as Naz,
(count(*) * 100.00) / @allRowsNazimice AS Pr_naz
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and k.CIKLUS = 1 and RASA like '%' + @rasa + '%'
group by RASA

select 
case 
	when n.Rasa <> ''
	then n.Rasa 
	when k.Rasa <> ''
	then k.Rasa
	when naz.Rasa <> ''
	then naz.Rasa
	else ''
end as Rasa,
n.Ner,
n.Pr_n,
k.Krm,
k.Pr_k,
naz.Naz,
naz.Pr_naz
from #RasaNerast n
full join #RasaKrmace k on k.Rasa = n.Rasa
full join #RasaNazimice naz on naz.Rasa = k.Rasa

END
GO


