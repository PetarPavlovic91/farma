USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[UkupnaIskljucenjaNerastova]    Script Date: 12/10/2021 2:38:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UkupnaIskljucenjaNerastova]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
select 
sk.Sif
,sk.Sif + ' ' + sf.NAZIV  as Razlog
,Broj
from
(
select 
s.RAZL_SK  as Sif,
count(*) as Broj 
from Skart s
join Nerast n on n.Id = s.NerastId
where s.D_SKART >= @dateTimeFrom and s.D_SKART <= @dateTimeTo
group by s.RAZL_SK
) sk
left join SifreF sf on sk.Sif = sf.SIFRA and sf.SIFARNIK = 'SKART_N'


 END
GO


