USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RegistarPrasadi]    Script Date: 12/21/2021 11:41:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RegistarPrasadi]
	-- Add the parameters for the stored procedure here
	@danRodjenjaOd as Date,
	@danRodjenjaDo as Date,
	@pol as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @pol ='M' or @pol = 'Z'
begin
select 
T_PRAS as T_pras,
POL as Pol,
CONCAT(SUBSTRING(n.RASA,1,2), SUBSTRING(k.RASA,1,2)) as Rasa,
p.Dat_rod,
n.MBR_NER as Mbr_oca,
k.MBR_KRM as Mbr_majke,
p.IZ_LEGLA as Iz_legla,
p.S_L as S_l,
p.S_D as S_d,
p.PRIPLOD as Priplod,
p.Mbr_pras
from Prase p
left join Nerast n on p.NerastId = n.Id
left join Krmaca k on p.KrmacaId = k.Id
where p.DAT_ROD >= @danRodjenjaOd and p.DAT_ROD <= @danRodjenjaDo and p.POL = @pol
end
else
begin
select 
T_PRAS as T_pras,
POL as Pol,
CONCAT(SUBSTRING(n.RASA,1,2), SUBSTRING(k.RASA,1,2)) as Rasa,
p.Dat_rod,
n.MBR_NER as Mbr_oca,
k.MBR_KRM as Mbr_majke,
p.IZ_LEGLA as Iz_legla,
p.S_L as S_l,
p.S_D as S_d,
p.PRIPLOD as Priplod,
p.Mbr_pras
from Prase p
left join Nerast n on p.NerastId = n.Id
left join Krmaca k on p.KrmacaId = k.Id
where p.DAT_ROD >= @danRodjenjaOd and p.DAT_ROD <= @danRodjenjaDo
end
END
GO


