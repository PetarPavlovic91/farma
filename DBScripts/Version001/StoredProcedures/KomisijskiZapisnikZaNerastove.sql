USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KomisijskiZapisnikZaNerastove]    Script Date: 12/28/2021 9:41:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KomisijskiZapisnikZaNerastove]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@prasenjeOd as Date,
	@prasenjeDo as Date,
	@rasa as Nvarchar(max),
	@sviNerastovi as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @sviNerastovi = 1
  begin
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red, *
	from
	(select 
	--ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
	n.Id
	,Klasa
	,EKST
	,VLA
	,ODG
	,GN
	,T_NER
	, RASA
	,MARKICA
	,HBBROJ	
	,BR_DOZVOLE
	,DAT_ROD
	,MBR_NER
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%'
	) Ner join
	(select 
	n.Id
	, SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) as Os_naz
	, SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) as Os_krm
	, count(*) as Uk_osem
	, SUM(CASE when pr.PARIT = 1 then 1 else 0 end) as Op_naz
	, SUM(CASE when pr.PARIT > 1 then 1 else 0 end) as Op_krm
	, SUM(CASE when pr.PARIT > 0 then 1 else 0 end) as Uk_opr
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then 1 else 0 end) * 100.00) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_op_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then 1 else 0 end) * 100.00) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_op_k
	, case when count(*) <> 0 then (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 100.00) / (count(*) * 1.00) else 0 end as Pr_op
	, SUM(CASE when pr.PARIT = 1 then pr.ZIVO else 0 end) as Zivo_n
	, SUM(CASE when pr.PARIT > 1 then pr.ZIVO else 0 end) as Zivo_k
	, SUM(CASE when pr.PARIT = 1 then pr.MRTVO else 0 end) as Mrtvo_n
	, SUM(CASE when pr.PARIT > 1 then pr.MRTVO else 0 end) as Mrtvo_k
	, SUM(CASE when pr.PARIT = 1 then pr.ZIVO + pr.MRTVO else 0 end) as Uk_pr_n
	, SUM(CASE when pr.PARIT > 1 then pr.ZIVO + pr.MRTVO else 0 end) as Uk_pr_k
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then pr.ZIVO else 0 end)) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_zi_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then pr.ZIVO else 0 end)) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_zi_k
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_zi
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_mr_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_mr_k
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_mr
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_uk_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_uk_k
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_uk
	from Nerast n
	join Pripust p on p.NerastId = n.Id or p.Nerast2Id = n.Id
	left join Prasenje pr on p.KrmacaId = pr.KrmacaId and p.CIKLUS = pr.PARIT
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and pr.DAT_PRAS >= @prasenjeOd and pr.DAT_PRAS <= @prasenjeDo
	group by n.Id) Grouped on Ner.Id = Grouped.Id
 end
else
 begin
 select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red, *
	from
	(select 
	--ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
	n.Id
	,Klasa
	,EKST
	,VLA
	,ODG
	,GN
	,T_NER
	, RASA
	,MARKICA
	,HBBROJ	
	,BR_DOZVOLE
	,DAT_ROD
	,MBR_NER
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%'
	) Ner join
	(select 
	n.Id
	, SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) as Os_naz
	, SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) as Os_krm
	, count(*) as Uk_osem
	, SUM(CASE when pr.PARIT = 1 then 1 else 0 end) as Op_naz
	, SUM(CASE when pr.PARIT > 1 then 1 else 0 end) as Op_krm
	, SUM(CASE when pr.PARIT > 0 then 1 else 0 end) as Uk_opr
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then 1 else 0 end) * 100.00) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_op_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then 1 else 0 end) * 100.00) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_op_k
	, case when count(*) <> 0 then (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 100.00) / (count(*) * 1.00) else 0 end as Pr_op
	, SUM(CASE when pr.PARIT = 1 then pr.ZIVO else 0 end) as Zivo_n
	, SUM(CASE when pr.PARIT > 1 then pr.ZIVO else 0 end) as Zivo_k
	, SUM(CASE when pr.PARIT = 1 then pr.MRTVO else 0 end) as Mrtvo_n
	, SUM(CASE when pr.PARIT > 1 then pr.MRTVO else 0 end) as Mrtvo_k
	, SUM(CASE when pr.PARIT = 1 then pr.ZIVO + pr.MRTVO else 0 end) as Uk_pr_n
	, SUM(CASE when pr.PARIT > 1 then pr.ZIVO + pr.MRTVO else 0 end) as Uk_pr_k
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then pr.ZIVO else 0 end)) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_zi_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then pr.ZIVO else 0 end)) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_zi_k
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_zi
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_mr_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_mr_k
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_mr
	, case when SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT = 1 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS = 1 then 1 else 0 end) * 1.00) else 0 end as Pr_uk_n
	, case when SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 1 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when p.CIKLUS > 1 then 1 else 0 end) * 1.00) else 0 end as Pr_uk_k
	, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO + pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_uk
	from Nerast n
	join Pripust p on p.NerastId = n.Id or p.Nerast2Id = n.Id
	left join Prasenje pr on p.KrmacaId = pr.KrmacaId and p.CIKLUS = pr.PARIT
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and RASA like '%' + @rasa + '%' and pr.DAT_PRAS >= @prasenjeOd and pr.DAT_PRAS <= @prasenjeDo and  substring(n.MBR_NER,1,3) = '092'
	group by n.Id) Grouped on Ner.Id = Grouped.Id
 end


END
GO


