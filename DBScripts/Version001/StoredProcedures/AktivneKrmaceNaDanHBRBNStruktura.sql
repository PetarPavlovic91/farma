USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[AktivneKrmaceNaDanHBRBNStruktura]    Script Date: 12/21/2021 12:58:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AktivneKrmaceNaDanHBRBNStruktura]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @allRows as int;
	 set @allRows = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA = @rasa);

If(OBJECT_ID('tempdb..#TempAktivneKrmaceNaDanHBRBNStruktura') Is Not Null)
Begin
    Drop Table #TempAktivneKrmaceNaDanHBRBNStruktura
End

create table #TempAktivneKrmaceNaDanHBRBNStruktura
(
    Tip nvarchar(max),
	Broj int,
	Procenat decimal
)

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'HB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA = @rasa and HBBROJ is not null

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'RB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA = @rasa and RBBROJ is not null

insert into #TempAktivneKrmaceNaDanHBRBNStruktura
select 
'NB' as Tip,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA = @rasa and NBBROJ is not null

select *
from #TempAktivneKrmaceNaDanHBRBNStruktura

END
GO


