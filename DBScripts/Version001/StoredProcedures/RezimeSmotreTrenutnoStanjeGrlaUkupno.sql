USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaUkupno]    Script Date: 12/22/2021 4:52:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaUkupno]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'


declare @Legala int = (select count(*) Broj
from Krmaca k
join Pripust p on p.KrmacaId = k.Id
--left join Zaluc z on z.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and RASA like '%' + @rasa + '%')

declare @Zalucenja int = (select count(*) as Zalucenja
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
--left join Zaluc z on z.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and RASA like '%' + @rasa + '%')


select 
'UKUPNO' as Opis
, count(*) as Krmaca
, @Legala as Legala
, sum(UK_ZIVO) as Zivo
, sum(UK_MRTVO) as Mrtvo
, sum(UK_ZIVO) + sum(UK_MRTVO) as Ukupno
, @Zalucenja as Zalucenja
, sum(UK_ZAL) as Zal_prasadi
, sum(UK_LAKT) as Laktacija
, sum(UK_PRAZNI) as Prazni_dani
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and RASA like '%' + @rasa + '%'

END
GO


