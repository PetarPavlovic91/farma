USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[ReproduktivniPokazateljiZalucenihKrmaca]    Script Date: 12/1/2021 11:50:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReproduktivniPokazateljiZalucenihKrmaca]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 

declare @zalPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @zalOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @iskljucenePosleZalucenjaPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)



declare @iskljucenePosleZalucenjaOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @pripustDo7DanaPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and DATEADD(day, 7, z.DAT_ZAL) <= p.DAT_PRIP
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @pripustDo7DanaOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and DATEADD(day, 7, z.DAT_ZAL) <= p.DAT_PRIP
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @osemenjenoPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRIP
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @osemenjenoOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRIP
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @povadjaloUkupnoPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRIP and p.RAZL_P = 2
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @povadjaloUkupnoOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRIP and p.RAZL_P = 2
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @povadjaloDo25PrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and DATEADD(day, 25, z.DAT_ZAL) <= p.DAT_PRIP and p.RAZL_P = 2
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @povadjaloDo25Ostale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and DATEADD(day, 25, z.DAT_ZAL) <= p.DAT_PRIP and p.RAZL_P = 2
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @povadjaloPreko25PrvoPr int = @povadjaloUkupnoPrvoPr - @povadjaloDo25PrvoPr;


declare @povadjaloPreko25Ostale int = @povadjaloUkupnoOstale - @povadjaloDo25Ostale;


declare @ukupnoOprasenoPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Prasenje p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRAS
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @ukupnoOprasenoOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Prasenje p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRAS
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)

declare @oprasenoOdPrvogPripustaPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRIP and p.RAZL_P = 1
join Prasenje pr on p.KrmacaId = k.Id and z.DAT_ZAL <= pr.DAT_PRAS
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @oprasenoOdPrvogPripustaOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Pripust p on p.KrmacaId = k.Id and z.DAT_ZAL <= p.DAT_PRIP and p.RAZL_P = 1
join Prasenje pr on p.KrmacaId = k.Id and z.DAT_ZAL <= pr.DAT_PRAS
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @iskljucenoBezJalovihPrvoPr int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT = 1)


declare @iskljucenoBezJalovihOstale int =
(select count(*)
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo and z.PARIT > 1)


declare @oprasenoPlusIskljucenoPrvoPr int = @ukupnoOprasenoPrvoPr + @iskljucenoBezJalovihPrvoPr;


declare @oprasenoPlusIskljucenoOstale int = @ukupnoOprasenoOstale + @iskljucenoBezJalovihOstale;


declare @jaloveINeoprasenePrvoPr int = @osemenjenoPrvoPr - @oprasenoPlusIskljucenoPrvoPr;


declare @jaloveINeopraseneOstale int = @osemenjenoOstale - @oprasenoPlusIskljucenoOstale;


select 
@zalPrvoPr as ZalPrvoPr,
@zalOstale as ZalOstale,
@iskljucenePosleZalucenjaPrvoPr as IskljucenePosleZalucenjaPrvoPr,
@iskljucenePosleZalucenjaOstale as IskljucenePosleZalucenjaOstale,
@pripustDo7DanaPrvoPr as PripustDo7DanaPrvoPr,
@pripustDo7DanaOstale as PripustDo7DanaOstale,
@osemenjenoPrvoPr as OsemenjenoPrvoPr,
@osemenjenoOstale as OsemenjenoOstale,
@povadjaloUkupnoPrvoPr as PovadjaloUkupnoPrvoPr,
@povadjaloUkupnoOstale as PovadjaloUkupnoOstale,
@povadjaloDo25PrvoPr as PovadjaloDo25PrvoPr,
@povadjaloDo25Ostale as PovadjaloDo25Ostale,
@povadjaloPreko25PrvoPr as PovadjaloPreko25PrvoPr,
@povadjaloPreko25Ostale as PovadjaloPreko25Ostale,
@ukupnoOprasenoPrvoPr as UkupnoOprasenoPrvoPr,
@ukupnoOprasenoOstale as UkupnoOprasenoOstale,
@oprasenoOdPrvogPripustaPrvoPr as OprasenoOdPrvogPripustaPrvoPr,
@oprasenoOdPrvogPripustaOstale as OprasenoOdPrvogPripustaOstale,
@iskljucenoBezJalovihPrvoPr as IskljucenoBezJalovihPrvoPr,
@iskljucenoBezJalovihOstale as IskljucenoBezJalovihOstale,
@oprasenoPlusIskljucenoPrvoPr as OprasenoPlusIskljucenoPrvoPr,
@oprasenoPlusIskljucenoOstale as OprasenoPlusIskljucenoOstale,
@jaloveINeoprasenePrvoPr as JaloveINeoprasenePrvoPr,
@jaloveINeopraseneOstale as JaloveINeopraseneOstale
 
END
GO


