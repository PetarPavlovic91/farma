USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PregledAktivnihNerastovaNaDan]    Script Date: 12/20/2021 11:00:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PregledAktivnihNerastovaNaDan]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@sviNerastovi as Bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @sviNerastovi = 1
  begin
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
	,T_ner
	,Rasa
	,Mbr_ner
	,Dat_rod
	,'' as Gn
	,Markica
	,MBR_OCA as Otac
	,MBR_MAJKE as Majka
	,HBBROJ as HB_Broj
	,RBBROJ as RB_Broj
	,'' as Red_s
	,Klasa
	,Ekst
	,Si1
	,NA_FARM_OD as Na_farmi_od
	,D_p_skoka
	,Br_os
	,Br_op
	,Uk_zivo
	,UK_MRTVO as Uk_mrt
	,Tetovirn10 
	,Napomena
	,s.D_skart
	,s.RAZL_SK as Sk
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan)
  end
else
  begin
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
	,T_ner
	,Rasa
	,Mbr_ner
	,Dat_rod
	,'' as Gn
	,Markica
	,MBR_OCA as Otac
	,MBR_MAJKE as Majka
	,HBBROJ as HB_Broj
	,RBBROJ as RB_Broj
	,'' as Red_s
	,Klasa
	,Ekst
	,Si1
	,NA_FARM_OD as Na_farmi_od
	,D_p_skoka
	,Br_os
	,Br_op
	,Uk_zivo
	,UK_MRTVO as Uk_mrt
	,Tetovirn10 
	,Napomena
	,s.D_skart
	,s.RAZL_SK as Sk
	from Nerast n
	left join Skart s on s.NerastId = n.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and substring(n.MBR_NER,1,3) = '092'
 end


END
GO


