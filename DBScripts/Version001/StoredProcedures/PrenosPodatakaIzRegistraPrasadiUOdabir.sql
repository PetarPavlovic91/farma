USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PrenosPodatakaIzRegistraPrasadiUOdabir]    Script Date: 12/15/2021 12:30:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PrenosPodatakaIzRegistraPrasadiUOdabir]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date, 
	@datumOdabira as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
p.Id as Id
,@datumOdabira as D_odabira
,ROW_NUMBER() OVER(ORDER BY (SELECT 1)) as Rb
,p.POL as Pol
,p.GN as Gn
,p.T_PRAS as T_test 
,SUBSTRING(p.MBR_PRAS, 1, 3) as Vla
,SUBSTRING(p.MBR_PRAS, 4, 3) as Odg
,SUBSTRING(p.MBR_PRAS, 7, 4) as Rasa
,p.MBR_PRAS as Mbr_test 
,p.DAT_ROD as Dat_rod
,n.MBR_NER as Mbr_oca
,k.MBR_KRM as Mbr_majke
,p.IZ_LEGLA as Iz_legla 
,'' as Br_sisa
from Prase p 
left join Krmaca k on k.Id = p.KrmacaId
left join Nerast n on n.Id = p.NerastId
where p.DAT_ROD >= @dateTimeFrom and p.DAT_ROD <=  @dateTimeTo and p.IsPrenesenaUOdabir <> 1
 


END
GO


