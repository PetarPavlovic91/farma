USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KombinacijePripusta]    Script Date: 12/8/2021 4:43:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KombinacijePripusta]
	-- Add the parameters for the stored procedure here
	@datumPripustaOd as Date,
	@datumPripustaDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	
	
DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + nRasa.RASA	 + ']',
    '[' + nRasa.RASA + ']'
  )
from  (
select distinct n.RASA
FROM Krmaca k
join Pripust p on k.Id = p.KrmacaId and p.DAT_PRIP >= @datumPripustaOd and p.DAT_PRIP <= @datumPripustaDo
join Nerast n on n.Id = p.NerastId) nRasa


DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'
select *
from
   (select k.RASA as KRasa, n.RASA as NRasa, 1 as BrojPripustaPoRasi
	from Krmaca k
	join Pripust p on k.Id = p.KrmacaId and p.DAT_PRIP >= ''' + CONVERT(NVARCHAR(MAX), @datumPripustaOd)  + '''and p.DAT_PRIP <= ''' + CONVERT(NVARCHAR(MAX), @datumPripustaDo)  + '''
	join Nerast n on n.Id = p.NerastId) as DataTable
PIVOT
(
SUM(BrojPripustaPoRasi)
FOR NRasa
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'


EXECUTE(@PivotTableSQL)

END
GO


