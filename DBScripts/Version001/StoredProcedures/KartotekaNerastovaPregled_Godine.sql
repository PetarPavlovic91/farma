USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KartotekaNerastovaPregled_Godine]    Script Date: 12/18/2021 12:07:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KartotekaNerastovaPregled_Godine]
	-- Add the parameters for the stored procedure here
	@maticniBrojNerasta as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
If(OBJECT_ID('tempdb..#TempKartotekaNerastovaPregled_Godine') Is Not Null)
Begin
    Drop Table TempKartotekaNerastovaPregled_Godine
End

create table #TempKartotekaNerastovaPregled_Godine
(
	Godina nvarchar(max),
    Kat nvarchar(max),
	Osemen int,
	Povadj int,
	Pobac int,
	Opras int,
	Zal_krm int,
	Zivo int,
	Mrtvo int,
	Zaluc int
)

insert into #TempKartotekaNerastovaPregled_Godine
select 
CONVERT(nvarchar(max),p.Godina) as Godina
, 'Krmaca' as Kat
, count(*) as Osemen
, SUM(p.Povadjanja) as Povadj
, SUM(CASE when po.id is not null then 1 else 0 end) as Pobac
, SUM(CASE when pr.id is not null then 1 else 0 end) as Opras
, SUM(CASE when z.id is not null then 1 else 0 end) as Zal_krm
, SUM(pr.ZIVO) as Zivo
, SUM(pr.Mrtvo) as Mrtvo
, SUM(z.ZALUC) as Zaluc 
from
(select k.Id, p.CIKLUS, YEAR(p.DAT_PRIP) as Godina, p.RBP - 1 as Povadjanja
from Pripust p
join Krmaca k on k.Id = p.KrmacaId
left join Nerast n1 on n1.Id = p.NerastId
left join Nerast n2 on n2.Id = p.Nerast2Id
where 
(n1.MBR_NER = @maticniBrojNerasta
or n2.MBR_NER = @maticniBrojNerasta)
and k.CIKLUS > 1) p
left join Pobacaj po on po.KrmacaId = p.Id and po.CIKLUS = p.CIKLUS
left join Prasenje pr on pr.KrmacaId = p.Id and pr.PARIT = p.CIKLUS
left join Zaluc z on z.KrmacaId = p.Id and z.PARIT = p.CIKLUS
group by p.Godina

insert into #TempKartotekaNerastovaPregled_Godine
select 
CONVERT(nvarchar(max),p.Godina) as Godina
, 'Nazimica' as Kat
, count(*) as Osemen
, SUM(p.Povadjanja) as Povadj
, SUM(CASE when po.id is not null then 1 else 0 end) as Pobac
, SUM(CASE when pr.id is not null then 1 else 0 end) as Opras
, SUM(CASE when z.id is not null then 1 else 0 end) as Zal_krm
, SUM(pr.ZIVO) as Zivo
, SUM(pr.Mrtvo) as Mrtvo
, SUM(z.ZALUC) as Zaluc 
from
(select k.Id, p.CIKLUS, YEAR(p.DAT_PRIP) as Godina, p.RBP - 1 as Povadjanja
from Pripust p
join Krmaca k on k.Id = p.KrmacaId
left join Nerast n1 on n1.Id = p.NerastId
left join Nerast n2 on n2.Id = p.Nerast2Id
where 
(n1.MBR_NER = @maticniBrojNerasta
or n2.MBR_NER = @maticniBrojNerasta)
and k.CIKLUS = 1) p
left join Pobacaj po on po.KrmacaId = p.Id and po.CIKLUS = p.CIKLUS
left join Prasenje pr on pr.KrmacaId = p.Id and pr.PARIT = p.CIKLUS
left join Zaluc z on z.KrmacaId = p.Id and z.PARIT = p.CIKLUS
group by p.Godina

insert into #TempKartotekaNerastovaPregled_Godine
select 
CONVERT(nvarchar(max),p.Godina) as Godina
, 'Ukupno' as Kat
, count(*) as Osemen
, SUM(p.Povadjanja) as Povadj
, SUM(CASE when po.id is not null then 1 else 0 end) as Pobac
, SUM(CASE when pr.id is not null then 1 else 0 end) as Opras
, SUM(CASE when z.id is not null then 1 else 0 end) as Zal_krm
, SUM(pr.ZIVO) as Zivo
, SUM(pr.Mrtvo) as Mrtvo
, SUM(z.ZALUC) as Zaluc 
from
(select k.Id, p.CIKLUS, YEAR(p.DAT_PRIP) as Godina, p.RBP - 1 as Povadjanja
from Pripust p
join Krmaca k on k.Id = p.KrmacaId
left join Nerast n1 on n1.Id = p.NerastId
left join Nerast n2 on n2.Id = p.Nerast2Id
where 
(n1.MBR_NER = @maticniBrojNerasta
or n2.MBR_NER = @maticniBrojNerasta)) p
left join Pobacaj po on po.KrmacaId = p.Id and po.CIKLUS = p.CIKLUS
left join Prasenje pr on pr.KrmacaId = p.Id and pr.PARIT = p.CIKLUS
left join Zaluc z on z.KrmacaId = p.Id and z.PARIT = p.CIKLUS
group by p.Godina


insert into #TempKartotekaNerastovaPregled_Godine
select 
'UKUP' as Godina 
,Kat 
,SUM(Osemen) as Osemen
,SUM(Povadj) as Povadj
,SUM(Pobac) as Pobac
,SUM(Opras) as Opras
,SUM(Zal_krm) as Zal_krm
,SUM(Zivo) as Zivo
,SUM(Mrtvo) as Mrtvo
,SUM(Zaluc) as Zaluc
from #TempKartotekaNerastovaPregled_Godine
group by Kat

select *
from #TempKartotekaNerastovaPregled_Godine
order by Godina

END
GO


