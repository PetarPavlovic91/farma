USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[AnalizaPotomstvaPoOcu]    Script Date: 12/21/2021 1:53:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AnalizaPotomstvaPoOcu]
	-- Add the parameters for the stored procedure here
	@mbr_oca as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


select 
'Cerka' as Rod,
MAX(k.TETOVIRK10) as Krmaca,
MAX(k.RASA) as Rasa,
count(p.Id) as Pr,
MAX(s.RAZL_SK) as Sk,
sum(p.ZIVO) as Zivo,
sum(p.ZIVO) * 1.0 / count(p.Id) as P_ziv,
sum(p.MRTVO) as Mrt,
sum(p.MRTVO) * 1.0 / count(p.Id) as P_mr,
sum(z.ZALUC) as Zal,
sum(z.ZALUC) * 1.0 / count(z.Id) as P_zal,
sum(datediff(day, p.DAT_PRAS , z.DAT_ZAL)) as Lakt,
sum(datediff(day, p.DAT_PRAS , z.DAT_ZAL)) * 1.0 / count(z.Id) as P_lakt,
sum(datediff(day, zPrethodno.DAT_ZAL, p.DAT_PRAS)) as Prazn,
sum(datediff(day, zPrethodno.DAT_ZAL, p.DAT_PRAS)) * 1.0 / count(zPrethodno.Id) as P_praz,
MAX(k.MBR_KRM) as Mbr_krm
from Krmaca k
join  Prasenje p on p.KrmacaId = k.Id
left join Zaluc z on z.KrmacaId = p.KrmacaId and z.PARIT = p.PARIT
left join Zaluc zPrethodno on zPrethodno.KrmacaId = p.KrmacaId and zPrethodno.PARIT = p.PARIT - 1
left join Skart s on s.KrmacaId = k.Id
where MBR_OCA = @mbr_oca
group by k.Id
order by Krmaca
 


END
GO


