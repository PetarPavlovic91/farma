USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[AktivneKrmaceNaDanKlasnaStruktura]    Script Date: 12/21/2021 12:42:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AktivneKrmaceNaDanKlasnaStruktura]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @allRows as int;
	 set @allRows = (select count(*) 
				from Krmaca k
				left join Skart s on s.KrmacaId = k.Id
				where (s.D_SKART is null or s.D_SKART >= @dan) and RASA = @rasa);

select 
KLASA as Klasa,
count(*) as Broj,
(count(*) * 100.00) / @allRows AS Procenat
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null or s.D_SKART >= @dan) and RASA = @rasa
 group by KLASA


END
GO


