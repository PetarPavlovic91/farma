USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Iskljucenje].[Save]    Script Date: 7/15/2022 2:52:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Iskljucenje].[Save]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@TETOVIR AS NVARCHAR(MAX),
	@RAZLOG AS NVARCHAR(MAX) = NULL,
	@D_SKART AS DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @KrmacaId AS INT = (SELECT TOP 1 k.Id FROM Krmaca k WHERE k.T_KRM = @TETOVIR);
	DECLARE @NerastId AS INT = (SELECT TOP 1 n.Id FROM Nerast n WHERE n.T_NER = @TETOVIR);
	DECLARE @PraseId AS INT = (SELECT TOP 1 p.Id FROM Prase p WHERE P.T_PRAS = @TETOVIR);
	DECLARE @OdabirId AS INT = (SELECT TOP 1 o.Id FROM Odabir o WHERE o.T_TEST = @TETOVIR);
	DECLARE @TestId AS INT = (SELECT TOP 1 t.Id FROM Test t WHERE T.T_TEST = @TETOVIR);

	IF @KrmacaId IS NULL AND @NerastId IS NULL AND @PraseId IS NULL AND @OdabirId IS NULL AND @TestId IS NULL BEGIN  
    	;THROW 51000, N'Grlo sa ovim tetovirom ne postoji', 1;
    END
	
    INSERT INTO Skart
	(
	   D_SKART
	   , RAZL_SK
	   , NerastId
	   ,PraseId
	   ,OdabirId
	   ,TestId
      ,[KrmacaId]
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@D_SKART, @RAZLOG, @NerastId, @PraseId, @OdabirId, @TestId, @KrmacaId,GETDATE(),GETDATE());

	IF @KrmacaId IS NOT NULL BEGIN  
    	UPDATE Krmaca SET FAZA = 4 WHERE Id = @KrmacaId
    END
END
ELSE BEGIN
		UPDATE Skart
		SET
		RAZL_SK = @RAZLOG
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END

END
