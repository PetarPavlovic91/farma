USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Prasenje].[Save]    Script Date: 7/15/2022 2:48:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Prasenje].[Save]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@KRMACA AS NVARCHAR(MAX),
	@DAT_PRAS AS DATE,
	@PARIT AS INT,
	@OBJ AS NVARCHAR(MAX) = NULL,
	@BOX AS NVARCHAR(MAX) = NULL,
	@BAB AS NVARCHAR(MAX) = NULL,
	@RAD AS NVARCHAR(MAX) = NULL,
	@MAT AS NVARCHAR(MAX) = NULL,
	@ZIVO AS INT = NULL,
	@MRTVO AS INT = NULL,
	@ZIVOZ AS INT = NULL,
	@UBIJ AS INT = NULL,
	@UGNJ AS INT = NULL,
	@ZAPRIM AS INT = NULL,
	@UGIN AS INT = NULL,
	@DODATO AS INT = NULL,
	@ODUZETO AS INT = NULL,
	@GAJI AS INT = NULL,
	@TEZ_LEG AS FLOAT = NULL,
	@ANOMALIJE AS NVARCHAR(MAX) = NULL,
	@TMP_OD AS INT = NULL,
	@TMP_DO AS INT = NULL,
	@TZP_OD AS INT = NULL,
	@TZP_DO AS INT = NULL,
	@TOV_OD AS INT = NULL,
	@TOV_DO AS INT = NULL,
	@REG_PRAS AS NVARCHAR(MAX) = NULL,
	@OL_P AS NVARCHAR(MAX) = NULL,
	@REFK AS NVARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @zaprimljeno as int = isnull(@ZIVO,0) - isnull(@UBIJ,0) - isnull(@UGNJ,0); 
	declare @gajila as int = @zaprimljeno + isnull(@DODATO,0) - isnull(@ODUZETO,0);


IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @KrmacaId AS INT = (SELECT TOP 1 k.Id FROM Krmaca k WHERE k.T_KRM = @KRMACA);

	IF @KrmacaId IS NULL BEGIN  
    	;THROW 51000, N'Krmaca ne postoji', 1;
    END
	DECLARE @Paritet AS INT = (SELECT TOP 1 p.CIKLUS FROM Pripust p WHERE p.KrmacaId = @KrmacaId order by p.DAT_PRIP desc);

	
    INSERT INTO Prasenje
	(
	   [DAT_PRAS]
      ,[PARIT]
      ,[OBJ]
      ,[BOX]
      ,[BAB]
      ,[RAD]
      ,[MAT]
      ,[ZIVO]
      ,[MRTVO]
      ,[ZIVOZ]
      ,[UBIJ]
      ,[UGNJ]
      ,[ZAPRIM]
      ,[UGIN]
      ,[DODATO]
      ,[ODUZETO]
      ,[GAJI]
      ,[TEZ_LEG]
      ,[ANOMALIJE]
      ,[TMP_OD]
      ,[TMP_DO]
      ,[TZP_OD]
      ,[TZP_DO]
      ,[TOV_OD]
      ,[TOV_DO]
      ,[REG_PRAS]
      ,[OL_P]
      ,[REFK]
      ,[KrmacaId]
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@DAT_PRAS, @Paritet, @OBJ, @BOX, @BAB, @RAD, @MAT, @ZIVO, @MRTVO, @ZIVOZ, @UBIJ, @UGNJ,@zaprimljeno,@UGIN,@DODATO,@ODUZETO
	,@gajila,@TEZ_LEG,@ANOMALIJE,@TMP_OD,@TMP_DO,@TZP_OD,@TZP_DO,@TOV_OD,@TOV_DO,@REG_PRAS,@OL_P,@REFK,@KrmacaId,GETDATE(),GETDATE());
	
	

	UPDATE Krmaca
	SET FAZA = 2, PRED_FAZA = 1
	WHERE Id = @KrmacaId;
END
ELSE BEGIN

		UPDATE Prasenje
		SET
		[OBJ] = @OBJ
      ,[BOX] = @BOX
      ,[BAB] = @BAB
      ,[RAD] = @RAD
      ,[MAT] = @MAT
      ,[ZIVO] = @ZIVO
      ,[MRTVO] = @MRTVO
      ,[ZIVOZ] = @ZIVOZ
      ,[UBIJ] = @UBIJ
      ,[UGNJ] = @UGNJ
      ,[ZAPRIM] = @zaprimljeno
      ,[UGIN] = @UGIN
      ,[DODATO] = @DODATO
      ,[ODUZETO] = @ODUZETO
      ,[GAJI] = @gajila
      ,[TEZ_LEG] = @TEZ_LEG
      ,[ANOMALIJE] = @ANOMALIJE
      ,[TMP_OD] = @TMP_OD
      ,[TMP_DO] = @TMP_DO
      ,[TZP_OD] = @TZP_OD
      ,[TZP_DO] = @TZP_DO
      ,[TOV_OD] = @TOV_OD
      ,[TOV_DO] = @TOV_DO
      ,[REG_PRAS] = @REG_PRAS
      ,[OL_P] = @OL_P
      ,[REFK] = @REFK
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END

END
