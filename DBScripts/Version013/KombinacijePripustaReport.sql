USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KombinacijePripusta]    Script Date: 7/3/2022 2:44:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KombinacijePripustaReport]
	-- Add the parameters for the stored procedure here
	@datumPripustaOd as Date,
	@datumPripustaDo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select k.RASA as KRasa, n.RASA as NRasa, 1 as BrojPripustaPoRasi
	from Krmaca k
	join Pripust p on k.Id = p.KrmacaId and p.DAT_PRIP >= @datumPripustaOd and p.DAT_PRIP <= @datumPripustaDo
	join Nerast n on n.Id = p.NerastId

END
GO


