USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[GrlaBezPotpunogPorekla]    Script Date: 7/15/2022 8:00:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GrlaBezPotpunogPorekla]
	-- Add the parameters for the stored procedure here
	@isPoreklo as bit,
	@vrstaGrla as nvarchar(max),
	@aktivan as bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;


if @isPoreklo = 0 and @vrstaGrla = 'Krmaca' and @aktivan = 0
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Pedigre p on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS > 1
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end


else if @isPoreklo = 0 and @vrstaGrla = 'Krmaca' and @aktivan = 1
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Pedigre p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS > 1 and s.Id is null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 1 and @vrstaGrla = 'Krmaca' and @aktivan = 0
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Poreklo p on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS > 1
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 1 and @vrstaGrla = 'Krmaca' and @aktivan = 1
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Poreklo p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS > 1 and s.Id is null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 0 and @vrstaGrla = 'Nerast' and @aktivan = 0
begin
select 
ner.T_NER as Tetovir
,'M' as POL
,ner.RASA
,ner.MBR_NER as MBR_GRLA
,ner.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Nerast ner
left join Pedigre p on p.NerastId = ner.Id
join Skart s on s.NerastId = ner.Id
where p.Id is not null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 0 and @vrstaGrla = 'Nerast' and @aktivan = 1
begin
select 
ner.T_NER as Tetovir
,'M' as POL
,ner.RASA
,ner.MBR_NER as MBR_GRLA
,ner.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Nerast ner
left join Pedigre p on p.NerastId = ner.Id
left join Skart s on s.NerastId = ner.Id
where p.Id is not null and s.Id is null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 1 and @vrstaGrla = 'Nerast' and @aktivan = 0
begin
select 
ner.T_NER as Tetovir
,'M' as POL
,ner.RASA
,ner.MBR_NER as MBR_GRLA
,ner.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Nerast ner
left join Poreklo p on p.NerastId = ner.Id
join Skart s on s.NerastId = ner.Id
where p.Id is not null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 1 and @vrstaGrla = 'Nerast' and @aktivan = 1
begin
select 
ner.T_NER as Tetovir
,'M' as POL
,ner.RASA
,ner.MBR_NER as MBR_GRLA
,ner.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Nerast ner
left join Poreklo p on p.NerastId = ner.Id
left join Skart s on s.NerastId = ner.Id
where p.Id is not null and s.Id is null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 0 and @vrstaGrla = 'Nazimica' and @aktivan = 0
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Pedigre p on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS <= 1
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 0 and @vrstaGrla = 'Nazimica' and @aktivan = 1
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Pedigre p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS <= 1 and s.Id is null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 1 and @vrstaGrla = 'Nazimica' and @aktivan = 0
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Poreklo p on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS <= 1
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

else if @isPoreklo = 1 and @vrstaGrla = 'Nazimica' and @aktivan = 1
begin
select 
k.T_KRM as Tetovir
,'Z' as POL
,k.RASA
,k.MBR_KRM as MBR_GRLA
,k.DAT_ROD
,p.MB_MAJKE 
,p.MB_OCA
,p.MBM_BABE 
,p.MBM_DEDE 
,p.MBMB_PRAB
,p.MBMB_PRAD
,p.MBMD_PRAB
,p.MBMD_PRAD
,p.MBO_BABE 
,p.MBO_DEDE 
,p.MBOB_PRAB
,p.MBOB_PRAD
,p.MBOD_PRAB
,p.MBOD_PRAD
from Krmaca k
left join Poreklo p on p.KrmacaId = k.Id
left join Skart s on s.KrmacaId = k.Id
where p.Id is not null and k.CIKLUS <= 1 and s.Id is null
and
(
p.MB_MAJKE is null or p.MB_MAJKE = ''
or p.MB_OCA is null or p.MB_OCA = ''
or p.MBM_BABE is null or p.MBM_BABE = ''
or p.MBM_DEDE is null or p.MBM_DEDE = ''
or p.MBMB_PRAB is null or p.MBMB_PRAB = ''
or p.MBMB_PRAD is null or p.MBMB_PRAD = ''
or p.MBMD_PRAB is null or p.MBMD_PRAB = ''
or p.MBMD_PRAD is null or p.MBMD_PRAD = ''
or p.MBO_BABE is null or p.MBO_BABE = ''
or p.MBO_DEDE is null or p.MBO_DEDE = ''
or p.MBOB_PRAB is null or p.MBOB_PRAB = ''
or p.MBOB_PRAD is null or p.MBOB_PRAD = ''
or p.MBOD_PRAB is null or p.MBOD_PRAB = ''
or p.MBOD_PRAD is null or p.MBOD_PRAD = ''
)
end

END
