USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[ProizvodniRezultatiKrmaca]    Script Date: 7/3/2022 11:24:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[ProizvodniRezultatiKrmaca]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB,
k.T_KRM,
k.RASA, 
k.DAT_ROD,
k.KLASA,
(
select COUNT(*)
from Prasenje p1
where p1.KrmacaId = k.Id and p1.DAT_PRAS < p.DAT_PRAS
) + 1 as Rb_pra,
pr.DAT_PRIP,
p.DAT_PRAS,
p.ZIVO,
p.MRTVO,
z.DAT_ZAL,
z.ZALUC,
DATEDIFF(day, pr.DAT_PRIP, p.DAT_PRAS) as Supras,
DATEDIFF(day, p.DAT_PRAS, z.DAT_ZAL) as Lakt
from Krmaca k
join Pripust pr on pr.KrmacaId = k.Id
join Prasenje p on p.KrmacaId = k.Id and p.DAT_PRAS >= @dateTimeFrom and p.DAT_PRAS <= @dateTimeTo and p.PARIT = pr.CIKLUS
left join Zaluc z on z.KrmacaId = k.Id and z.DAT_ZAL >= @dateTimeFrom and z.DAT_ZAL <= @dateTimeTo
END
