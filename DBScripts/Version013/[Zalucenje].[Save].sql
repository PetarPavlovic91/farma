USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Zalucenje].[Save]    Script Date: 7/15/2022 2:51:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Zalucenje].[Save]
	-- Add the parameters for the stored procedure here
	@Id as INT,
	@KRMACA AS NVARCHAR(MAX),
	@DAT_ZAL AS DATE,
	@PARIT AS INT,
	@OBJ AS NVARCHAR(MAX) = NULL,
	@BOX AS NVARCHAR(MAX) = NULL,
	@PZ AS NVARCHAR(MAX) = NULL,
	@RAZ_PZ AS NVARCHAR(MAX) = NULL,
	@ZDR_ST AS NVARCHAR(MAX) = NULL,
	@OL_Z AS NVARCHAR(MAX) = NULL,
	@ZALUC AS INT = NULL,
	@TEZ_ZAL AS FLOAT = NULL,
	@RBZ AS INT = NULL,
	@DOJARA AS NVARCHAR(MAX) = NULL,
	@REFK AS NVARCHAR(MAX) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



IF @Id IS NULL OR @Id = 0 BEGIN  

	DECLARE @KrmacaId AS INT = (SELECT TOP 1 k.Id FROM Krmaca k WHERE k.T_KRM = @KRMACA);

	IF @KrmacaId IS NULL BEGIN  
    	;THROW 51000, N'Krmaca ne postoji', 1;
    END
	DECLARE @Paritet AS INT = (SELECT TOP 1 p.PARIT FROM Prasenje p WHERE p.KrmacaId = @KrmacaId order by p.DAT_PRAS desc);
	
    INSERT INTO Zaluc
	(
	   [DAT_ZAL]
      ,[PARIT]
      ,[OBJ]
      ,[BOX]
      ,[PZ]
      ,[RAZ_PZ]
      ,[ZDR_ST]
      ,[OL_Z]
      ,[ZALUC]
      ,[TEZ_ZAL]
      ,[RBZ]
      ,[DOJARA]
      ,[REFK]
      ,[KrmacaId]
      ,[DateCreated]
      ,[DateModified]
	)
	VALUES(@DAT_ZAL, @Paritet, @OBJ, @BOX, @PZ, @RAZ_PZ, @ZDR_ST, @OL_Z
			, @ZALUC, @TEZ_ZAL, @RBZ, @DOJARA,@REFK,@KrmacaId,GETDATE(),GETDATE());

			

	UPDATE Krmaca SET FAZA = 3, PRED_FAZA = 2 WHERE Id = @KrmacaId

END
ELSE BEGIN
		UPDATE Zaluc
		SET
		[OBJ] = @OBJ
      ,[BOX] = @BOX
      ,[PZ] = @PZ
      ,[RAZ_PZ] = @RAZ_PZ
      ,[ZDR_ST] = @ZDR_ST
      ,[OL_Z] = @OL_Z
      ,[ZALUC] = @ZALUC
      ,[TEZ_ZAL] = @TEZ_ZAL
      ,[RBZ] = @RBZ
      ,[DOJARA] = @DOJARA
      ,[REFK] = @REFK
      ,[DateModified] = GETDATE()
		WHERE Id = @Id
END

END
