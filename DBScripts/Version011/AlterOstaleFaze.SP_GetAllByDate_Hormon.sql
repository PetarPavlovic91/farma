USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [OstaleFaze].[SP_GetAllByDate_Hormon]    Script Date: 6/14/2022 10:15:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [OstaleFaze].[SP_GetAllByDate_Hormon]
	-- Add the parameters for the stored procedure here
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
h.Id
,k.T_KRM  AS Krmaca
,h.TipHormona
from Hormons h 
JOIN Krmaca k on k.Id = h.KrmacaId
where h.DatumDavanjaHormona >= @dan and h.DatumDavanjaHormona < DATEADD(day, 1, @dan)
 


END
