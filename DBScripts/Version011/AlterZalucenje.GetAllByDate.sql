USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Zalucenje].[GetAllByDate]    Script Date: 6/14/2022 10:14:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Zalucenje].[GetAllByDate]
	-- Add the parameters for the stored procedure here
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,z.Id
,k.T_KRM AS KRMACA
,[z].[DAT_ZAL]
,z.[PARIT]
,[OBJ]
,[BOX]
,[PZ]
,[RAZ_PZ]
,[ZALUC]
,[TEZ_ZAL]
,[ZDR_ST]
,[OL_Z]
,[RBZ]
,[DOJARA]
,[REFK]
,z.[DateCreated]
,z.[DateModified]
from Zaluc z
join Krmaca k on k.Id = z.KrmacaId
where z.DAT_ZAL >= @dan and z.DAT_ZAL < DATEADD(day, 1, @dan)
 


END
