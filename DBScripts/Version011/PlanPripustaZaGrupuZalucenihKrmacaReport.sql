USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[PlanPripustaZaGrupuZalucenihKrmacaReport]    Script Date: 6/5/2022 3:37:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PlanPripustaZaGrupuZalucenihKrmacaReport]
	-- Add the parameters for the stored procedure here
	@datumZalucenja as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
	k.T_KRM as Krmaca, 
	k.RASA as Rasa, 
	n.T_GRLA as Nerast,
	CASE
		WHEN p.MBR_GRLA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MB_OCA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MB_MAJKE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBM_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBM_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBMB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBMB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBMD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBMD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBO_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBO_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBOB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBOB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBOD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		WHEN p.MBOD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN '-'
		ELSE '+'
	END as [Value]
	from Krmaca k
	join Pedigre p on k.Id = p.KrmacaId
	join Zaluc z on z.KrmacaId = k.Id
	cross join 
	(
	select p1.*
	from Nerast n1
	join Pedigre p1 on n1.Id = p1.NerastId
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where z.DAT_ZAL >=  @datumZalucenja
			and z.DAT_ZAL <=  DATEADD(day, 1, @datumZalucenja)

END
GO


