USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Pripust].[GetAllByDate]    Script Date: 6/14/2022 10:10:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Pripust].[GetAllByDate]
	-- Add the parameters for the stored procedure here
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,p.Id
,k.T_KRM AS Krmaca
,k.MBR_KRM AS MaticniBroj
,p.CIKLUS
,P.RBP
,P.RAZL_P
,P.OBJ
,P.BOX
,n.T_NER AS Nerast
,P.PV
,P.OSEM
,P.OCENA
,n2.T_NER AS Nerast2
,P.PV2
,P.OSEM2
,P.OCENA2
,P.PRIPUSNICA
,P.PRIMEDBA
from Pripust p
join Krmaca k on k.Id = p.KrmacaId
left join Nerast n on n.Id = p.NerastId
left join Nerast n2 on n2.Id = p.Nerast2Id
where p.DAT_PRIP >= @dan and p.DAT_PRIP < DATEADD(day, 1, @dan)
 


END
