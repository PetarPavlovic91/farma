USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [OstaleFaze].[SP_GetAllByDate_Uginuca]    Script Date: 6/14/2022 10:15:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [OstaleFaze].[SP_GetAllByDate_Uginuca]
	-- Add the parameters for the stored procedure here
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
u.Id
,n.T_NER  AS Nerast
,u.Kategorija
,u.Razlog
,u.Komada
,u.MestoRodjenja
from Uginucas u
JOIN Nerast n on n.Id = u.NerastId
where u.DatumUginuca >= @dan and u.DatumUginuca < DATEADD(day, 1, @dan)
 


END
