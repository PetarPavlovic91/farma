USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [Iskljucenje].[GetAllByDate]    Script Date: 6/14/2022 10:16:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [Iskljucenje].[GetAllByDate]
	-- Add the parameters for the stored procedure here
	@dan as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as RB
,s.Id
,COALESCE(k.T_KRM, n.T_NER,P.T_PRAS,o.T_TEST,T.T_TEST)  AS TETOVIR
,COALESCE(k.MBR_KRM, n.MBR_NER,P.MBR_PRAS,o.MBR_TEST,T.MBR_TEST) AS MBR_GRLA
,s.RAZL_SK AS RAZLOG
from Skart s
LEFT JOIN Krmaca k on k.Id = s.KrmacaId
LEFT JOIN Nerast n ON s.NerastId = n.Id
LEFT JOIN Prase p ON s.PraseId = p.Id
LEFT JOIN Odabir o ON s.OdabirId = o.Id
LEFT JOIN Test t ON s.TestId = t.Id
where s.D_SKART >= @dan and s.D_SKART < DATEADD(day, 1, @dan)
 


END
