USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[DevniPlanPripusta]    Script Date: 6/10/2022 10:10:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DevniPlanPripusta]
	-- Add the parameters for the stored procedure here
	@datumZalucenja as Date,
	@t_KrmParam as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @PivotColumnHeaders NVARCHAR(MAX)
SELECT @PivotColumnHeaders =
  COALESCE(
    @PivotColumnHeaders + ',[' + T_NER + ']',
    '[' + T_NER + ']'
  )
  FROM
  (
	select distinct n1.RASA + ' ' + n1.T_NER as T_NER
	from Nerast n1
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n

DECLARE @PivotTableSQL NVARCHAR(MAX)
SET @PivotTableSQL = N'

If(OBJECT_ID(''tempdb..#Krmace'') Is Not Null)
Begin
    Drop Table #Krmace
End

If(OBJECT_ID(''tempdb..#Test'') Is Not Null)
Begin
    Drop Table #Test
End

create table #Krmace(
Id INT,
T_KRM nvarchar(max),
RASA nvarchar(max),
SI1 INT,
KLASA nvarchar(max),
Pr_oprasenih decimal(10,2),
STAROST INT
)


insert into #Krmace
select 
k.Id, 
MAX(k.T_KRM) as T_KRM, 
MAX(k.RASA) as RASA, 
MAX(k.SI1) as SI1, 
MAX(k.KLASA) as KLASA, 
AVG(ISNULL(p.ZIVO,0)) as Pr_oprasenih,
DATEDIFF(day,MAX(k.DAT_ROD),GETDATE()) as STAROST
from Krmaca k
join Prasenje p on p.KrmacaId = k.Id
where k.T_KRM in ' + @t_KrmParam +'
group by k.Id


create table #Test(
Id INT,
T_KRM nvarchar(max),
RASA nvarchar(max),
SI1 INT,
KLASA nvarchar(max),
Pr_oprasenih decimal(10,2),
STAROST INT
)


insert into #Test
select 
k.Id, 
MAX(k.T_TEST) as T_KRM, 
MAX(k.RASA) as RASA, 
MAX(k.SI1) as SI1, 
'''' as KLASA, 
0 as Pr_oprasenih,
DATEDIFF(day,MAX(k.DAT_ROD),GETDATE()) as STAROST
from Test k
where k.T_TEST in ' + @t_KrmParam +'
and k.T_TEST not in (
	select T_KRM
	from #Krmace
)
group by k.Id

select *
from
   (
   (
select 
	k.T_KRM, 
	k.RASA,
	k.SI1,
	k.KLASA,
	k.Pr_oprasenih,
	k.STAROST,
	n.RASA + '' '' + n.T_GRLA as T_NER,
	CASE
		WHEN p.MBR_GRLA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MB_OCA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MB_MAJKE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBM_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBM_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBO_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBO_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		ELSE ''+''
	END as Ukrstanje
	from #Krmace k
	join Pedigre p on k.Id = p.KrmacaId
	cross join 
	(
	select n1.RASA, p1.*
	from Nerast n1
	join Pedigre p1 on n1.Id = p1.NerastId
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where k.T_KRM in ' + @t_KrmParam +'

UNION

select 
	k.T_KRM, 
	k.RASA,
	k.SI1,
	k.KLASA,
	k.Pr_oprasenih,
	k.STAROST,
	n.RASA + '' '' + n.T_GRLA as T_NER,
	CASE
		WHEN p.MBR_GRLA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MB_OCA in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MB_MAJKE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBM_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBM_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBMD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBO_DEDE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBO_BABE in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOB_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOB_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOD_PRAD in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		WHEN p.MBOD_PRAB in (n.MBR_GRLA, n.MB_OCA, n.MBO_DEDE, n.MB_MAJKE, n.MBO_BABE, n.MBM_DEDE, n.MBM_BABE, n.MBOD_PRAD, n.MBOD_PRAB, n.MBOB_PRAD, n.MBOB_PRAB, n.MBMD_PRAD, n.MBMD_PRAB, n.MBMB_PRAD, n.MBMB_PRAB)
		THEN ''''
		ELSE ''+''
	END as Ukrstanje
	from #Test k
	join Pedigre p on k.Id = p.TestId
	cross join 
	(
	select n1.RASA, p1.*
	from Nerast n1
	join Pedigre p1 on n1.Id = p1.NerastId
	left join Skart s on s.NerastId = n1.Id
	where s.Id is null
	) as n
	where k.T_KRM in ' + @t_KrmParam +'
	and k.T_KRM not in (
		select T_KRM
		from #Krmace
	)
	)
) as DataTable
PIVOT
(
MAX(Ukrstanje)
FOR T_NER
IN (
 ' + @PivotColumnHeaders + '
)
) PivotTable
'


EXECUTE(@PivotTableSQL)
END
