USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[DatabaseBackup]    Script Date: 1/3/2022 11:49:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DatabaseBackup]
	-- Add the parameters for the stored procedure here
	@backupLocation nvarchar(200) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @name NVARCHAR(256) = 'FarmaProduction'; -- database name  
DECLARE @fileName NVARCHAR(512) -- filename for backup  
DECLARE @fileDate NVARCHAR(40) -- used for file name
DECLARE @fileTime NVARCHAR(40) -- used for file name
 
-- specify filename format
SELECT @fileDate = CONVERT(NVARCHAR(20),GETDATE(),112) 
SELECT @fileTime = REPLACE(CONVERT(NVARCHAR(20), GETDATE(),108),':','')
 
SET @fileName = @backupLocation + @name + '_' + @fileDate + '_' + @fileTime + '.BAK'  
BACKUP DATABASE @name TO DISK = @fileName 
 

END
GO


