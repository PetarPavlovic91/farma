USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmaceDopuna]    Script Date: 1/6/2022 12:14:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO












-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaRaseSamoKrmaceDopuna]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max),
	@dan as Date,
	@danDo as Date,
	@paritet as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'
 
If(OBJECT_ID('tempdb..#RasaKrmace') Is Not Null)
Begin
    Drop Table #RasaKrmace
End

create table #RasaKrmace
(
    Rasa nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Prasenje p on p.KrmacaId = k.Id
				where p.DAT_PRAS >= @dan and p.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet);

insert into #RasaKrmace
select 
case when RASA is null then '' else RASA end as Rasa,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Prasenje p on p.KrmacaId = k.Id
where p.DAT_PRAS >= @dan and p.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet
group by RASA


select 
case 
	when k.Rasa <> ''
	then k.Rasa
	else ''
end as Rasa,
k.Krm,
k.Pr_k
from #RasaKrmace k

END
GO


