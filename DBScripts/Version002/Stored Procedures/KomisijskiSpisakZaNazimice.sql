USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[KomisijskiSpisakZaNazimice]    Script Date: 1/7/2022 4:45:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[KomisijskiSpisakZaNazimice]
	-- Add the parameters for the stored procedure here
	@dan as Date,
	@rasa as Nvarchar(max),
	@isTest as bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

if @isTest is null or @isTest = 0
begin
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) 
	, k.T_KRM as Nazimica
	, k.ODG
	, k.VLA
	, k.RASA
	, k.MARKICA
	, k.DAT_ROD
	, k.BR_SISA
	, k.EKST
	, k.KLASA
	, k.SI1
	, k.GN
	, k.MBR_KRM
	, k.GNO
	, k.MBR_OCA
	, k.GNM
	, k.MBR_MAJKE
	, p.DAT_PRIP
	, n.T_NER as Nerast
	, n.GN as Gnn
	, n.MBR_NER
	, otac.T_NER as Otac
	, otac.HBBROJ Hb_o
	, otac.SI1 as Si_o
	, otac.KLASA as Kl_o
	, majka.T_KRM as Majka
	, majka.HBBROJ as Hb_m
	, majka.SI1 as Si_m
	, majka.KLASA as Kl_m
	, k.NAPOMENA
	from
	(
	select k.Id as KrmacaId, max(p.RBP) as RBP
	from Krmaca k 
	join Pripust p on p.KrmacaId = k.Id
	where k.CIKLUS = 1
	group by k.Id ) as grouped
	join Krmaca k on k.Id = grouped.KrmacaId
	join Pripust p on p.KrmacaId = grouped.KrmacaId and p.RBP = grouped.RBP
	left join Nerast n on p.NerastId = n.Id
	left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
	left join Nerast otac on otac.MBR_NER = k.MBR_OCA
	left join Skart s on s.KrmacaId = k.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and k.RASA like '%' + @rasa + '%'
end
else
begin
	select 
	ROW_NUMBER() OVER(ORDER BY (SELECT 2)) 
	, k.T_TEST as Nazimica
	, k.ODG
	, k.VLA
	, k.RASA
	, k.MARKICA
	, k.DAT_ROD
	, k.BR_SISA
	, k.EKST
	, '' as KLASA
	, k.SI1
	, k.GN
	, k.MBR_TEST
	, k.GNO
	, k.MBR_OCA
	, k.GNM
	, k.MBR_MAJKE
	, null as DAT_PRIP
	, '' as Nerast
	, '' as Gnn
	, '' as MBR_NER
	, otac.T_NER as Otac
	, otac.HBBROJ Hb_o
	, otac.SI1 as Si_o
	, otac.KLASA as Kl_o
	, majka.T_KRM as Majka
	, majka.HBBROJ as Hb_m
	, majka.SI1 as Si_m
	, majka.KLASA as Kl_m
	, k.NAPOMENA
	from Test k
	left join Krmaca majka on majka.MBR_KRM = k.MBR_MAJKE
	left join Nerast otac on otac.MBR_NER = k.MBR_OCA
	left join Skart s on s.TestId = k.Id
	where (s.D_SKART is null or s.D_SKART >= @dan) and k.RASA like '%' + @rasa + '%'
end
END
GO

