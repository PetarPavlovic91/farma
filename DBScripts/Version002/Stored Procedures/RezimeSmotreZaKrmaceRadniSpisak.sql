USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreZaKrmaceRadniSpisak]    Script Date: 1/6/2022 12:34:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreZaKrmaceRadniSpisak]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


select 
ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red, *
from
(select 
--ROW_NUMBER() OVER(ORDER BY (SELECT 2)) as Red
k.Id
,T_KRM as Krmaca
,Klasa
,EKST
,VLA
,ODG
,MBR_KRM
,PARIT
,GN
,RASA
,MARKICA
,HBBROJ
,DAT_ROD
,GNO
,MBR_OCA
,GNM
,MBR_MAJKE
,SI1
,SI2
,KL_P
,BR_SISA
,FAZA
,PRED_FAZA
,NAPOMENA
from Krmaca k
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and RASA like '%' + @rasa + '%'
) Ner join
(select 
k.Id
, SUM(pr.ZIVO) as Uk_zivo
, SUM(pr.MRTVO) as Uk_mrtvo
, SUM(z.ZALUC) as Uk_zal
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_zi
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_mr
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then z.ZALUC else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_zal
from Krmaca k
join Pripust p on p.KrmacaId = k.Id
left join Prasenje pr on p.KrmacaId = pr.KrmacaId and p.CIKLUS = pr.PARIT
left join Zaluc z on z.KrmacaId = pr.KrmacaId and z.PARIT = pr.PARIT
left join Skart s on s.KrmacaId = k.Id
where (s.D_SKART is null) and RASA like '%' + @rasa + '%'
group by k.Id) Grouped on Ner.Id = Grouped.Id
 

END
GO


