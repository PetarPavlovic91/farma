USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[SmotraProizvodnjaKrmacaPoParitetima]    Script Date: 1/9/2022 3:03:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SmotraProizvodnjaKrmacaPoParitetima]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @allRows as int;
	 set @allRows = (select count(*) 
				from Krmaca k
				join Pripust p on p.KrmacaId = k.Id);

select 
p.CIKLUS as Parit
, count(p.Id) as Br_krm
, count(p.Id) * 100.00 / @allRows as Procenat
, SUM(pr.ZIVO) as Uk_zivo
, SUM(pr.MRTVO) as Uk_mrtvo
, SUM(pr.ZIVO + pr.MRTVO) as Uk_opr
, SUM(z.ZALUC) as Uk_zal
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.ZIVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_zi
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_mr
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then pr.MRTVO + pr.MRTVO else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Ukupno
, case when SUM(CASE when pr.PARIT > 0 then 1 else 0 end) <> 0 then (SUM(CASE when pr.PARIT > 0 then z.ZALUC else 0 end)) / (SUM(CASE when pr.PARIT > 0 then 1 else 0 end) * 1.00) else 0 end as Pr_zal
from Krmaca k
join Pripust p on p.KrmacaId = k.Id
left join Prasenje pr on p.KrmacaId = pr.KrmacaId and p.CIKLUS = pr.PARIT
left join Zaluc z on z.KrmacaId = pr.KrmacaId and z.PARIT = pr.PARIT
group by p.CIKLUS
order by Parit
 


END
GO


