USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmaceDopuna]    Script Date: 1/6/2022 12:07:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaKlaseSamoKrmaceDopuna]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max),
	@dan as Date,
	@danDo as Date,
	@paritet as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


If(OBJECT_ID('tempdb..#KlasaKrmace') Is Not Null)
Begin
    Drop Table #KlasaKrmace
End

create table #KlasaKrmace
(
    Klasa nvarchar(max), 
    Krm int, 
    Pr_k decimal(5,2)
)

declare @allRowsKrmace as int;
 
 set @allRowsKrmace = (select count(*) 
				from Krmaca k
				left join Prasenje p on p.KrmacaId = k.Id
				where p.DAT_PRAS >= @dan and p.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet);

insert into #KlasaKrmace
select 
case when KLASA is null then '' else KLASA end as Klasa,
count(*) as Krm,
(count(*) * 100.00) / @allRowsKrmace AS Pr_k
from Krmaca k
left join Prasenje p on p.KrmacaId = k.Id
where p.DAT_PRAS >= @dan and p.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet
group by KLASA


select 
case 
	when k.Klasa <> ''
	then k.Klasa
	else ''
end as Klasa,
k.Krm,
k.Pr_k
from #KlasaKrmace k

END
GO


