USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[RezimeSmotreTrenutnoStanjeGrlaUkupnoSamoKrmaceDopuna]    Script Date: 1/6/2022 12:21:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO













-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RezimeSmotreTrenutnoStanjeGrlaUkupnoSamoKrmaceDopuna]
	-- Add the parameters for the stored procedure here
	@rasa as Nvarchar(max),
	@dan as Date,
	@danDo as Date,
	@paritet as int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- RASA like '%' + @rasa + '%'


declare @Legala int = (select count(*) Broj
from Krmaca k
join Pripust p on p.KrmacaId = k.Id
left join Prasenje pr on pr.KrmacaId = k.Id
where pr.DAT_PRAS >= @dan and pr.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet)

declare @Zalucenja int = (select count(*) as Zalucenja
from Krmaca k
join Zaluc z on z.KrmacaId = k.Id
left join Prasenje pr on pr.KrmacaId = k.Id
where pr.DAT_PRAS >= @dan and pr.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet)


select 
'UKUPNO' as Opis
, count(*) as Krmaca
, @Legala as Legala
, sum(UK_ZIVO) as Zivo
, sum(UK_MRTVO) as Mrtvo
, sum(UK_ZIVO) + sum(UK_MRTVO) as Ukupno
, @Zalucenja as Zalucenja
, sum(UK_ZAL) as Zal_prasadi
, sum(UK_LAKT) as Laktacija
, sum(UK_PRAZNI) as Prazni_dani
from Krmaca k
left join Prasenje pr on pr.KrmacaId = k.Id
where pr.DAT_PRAS >= @dan and pr.DAT_PRAS <= @danDo and RASA like '%' + @rasa + '%' and k.PARIT <= @paritet

END
GO


