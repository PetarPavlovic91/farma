USE [FarmaProduction]
GO

/****** Object:  StoredProcedure [dbo].[GrlaBezOtvorenogPorekla]    Script Date: 1/28/2022 10:34:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO











-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GrlaBezOtvorenogPorekla]
	-- Add the parameters for the stored procedure here
	@isPoreklo as bit,
	@vrstaGrla as nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;


if @isPoreklo = 0 and @vrstaGrla = 'Krmaca'
begin
select 
k.T_KRM
,'Z' as POL
,k.RASA
,k.MBR_KRM
,k.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Krmaca k
left join Pedigre p on p.KrmacaId = k.Id
left join Nerast n on n.Id = k.OtacId
left join Pedigre pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.Id = k.MajkaId
left join Pedigre pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null and k.CIKLUS > 1
end

else if @isPoreklo = 1 and @vrstaGrla = 'Krmaca'
begin
select 
k.T_KRM
,'Z' as POL
,k.RASA
,k.MBR_KRM
,k.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Krmaca k
left join Poreklo p on p.KrmacaId = k.Id
left join Nerast n on n.Id = k.OtacId
left join Poreklo pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.Id = k.MajkaId
left join Poreklo pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null and k.CIKLUS > 1
end


else if @isPoreklo = 0 and @vrstaGrla = 'Nerast'
begin
select 
ner.T_NER
,'M' as POL
,ner.RASA
,ner.MBR_NER
,ner.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Nerast ner
left join Pedigre p on p.NerastId = ner.Id
left join Nerast n on n.Id = ner.OtacId
left join Pedigre pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.Id = ner.MajkaId
left join Pedigre pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null
end

else if @isPoreklo = 1 and @vrstaGrla = 'Nerast'
begin
select 
ner.T_NER
,'M' as POL
,ner.RASA
,ner.MBR_NER
,ner.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Nerast ner
left join Poreklo p on p.NerastId = ner.Id
left join Nerast n on n.Id = ner.OtacId
left join Poreklo pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.Id = ner.MajkaId
left join Poreklo pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null
end


else if @isPoreklo = 0 and @vrstaGrla = 'Nazimica'
begin
select 
k.T_KRM
,'Z' as POL
,k.RASA
,k.MBR_KRM
,k.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Krmaca k
left join Pedigre p on p.KrmacaId = k.Id
left join Nerast n on n.Id = k.OtacId
left join Pedigre pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.Id = k.MajkaId
left join Pedigre pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null and k.CIKLUS <= 1
end

else if @isPoreklo = 1 and @vrstaGrla = 'Nazimica'
begin
select 
k.T_KRM
,'Z' as POL
,k.RASA
,k.MBR_KRM
,k.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Krmaca k
left join Poreklo p on p.KrmacaId = k.Id
left join Nerast n on n.Id = k.OtacId
left join Poreklo pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.Id = k.MajkaId
left join Poreklo pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null and k.CIKLUS <= 1
end


else if @isPoreklo = 0 and @vrstaGrla = 'Test'
begin
select 
t.T_TEST
,t.POL as POL
,t.RASA
,t.MBR_TEST
,t.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Test t
left join Pedigre p on p.MBR_GRLA = t.MBR_TEST
left join Nerast n on n.MBR_NER = t.MBR_OCA
left join Pedigre pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = t.MBR_MAJKE
left join Pedigre pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null
end

else if @isPoreklo = 1 and @vrstaGrla = 'Test'
begin
select 
t.T_TEST
,t.POL as POL
,t.RASA
,t.MBR_TEST
,t.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Test t
left join Poreklo p on p.MBR_GRLA = t.MBR_TEST
left join Nerast n on n.MBR_NER = t.MBR_OCA
left join Poreklo pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = t.MBR_MAJKE
left join Poreklo pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null
end


else if @isPoreklo = 0 and @vrstaGrla = 'Odabir'
begin
select 
t.T_TEST
,t.POL as POL
,t.RASA
,t.MBR_TEST
,t.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Odabir t
left join Pedigre p on p.MBR_GRLA = t.MBR_TEST
left join Nerast n on n.MBR_NER = t.MBR_OCA
left join Pedigre pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = t.MBR_MAJKE
left join Pedigre pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null
end

else if @isPoreklo = 1 and @vrstaGrla = 'Odabir'
begin
select 
t.T_TEST
,t.POL as POL
,t.RASA
,t.MBR_TEST
,t.DAT_ROD
,n.T_NER as Otac
,case when pOca.Id is null then '' else 'DA' end as Ped_o
,majka.T_KRM as Majka
,case when pMajke.Id is null then '' else 'DA' end as Ped_m
from Odabir t
left join Poreklo p on p.MBR_GRLA = t.MBR_TEST
left join Nerast n on n.MBR_NER = t.MBR_OCA
left join Poreklo pOca on pOca.NerastId = n.Id
left join Krmaca majka on majka.MBR_KRM = t.MBR_MAJKE
left join Poreklo pMajke on pMajke.KrmacaId = majka.Id
where p.Id is null
end

END
GO
