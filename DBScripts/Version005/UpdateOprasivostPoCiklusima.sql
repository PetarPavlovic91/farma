USE [FarmaProduction]
GO
/****** Object:  StoredProcedure [dbo].[OprasivostPoCiklusima]    Script Date: 2/6/2022 11:53:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[OprasivostPoCiklusima]
	-- Add the parameters for the stored procedure here
	@dateTimeFrom as Date, 
	@dateTimeTo as Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
If(OBJECT_ID('tempdb..#OsT') Is Not Null)
Begin
    Drop Table #OsT
End


If(OBJECT_ID('tempdb..#PobT') Is Not Null)
Begin
    Drop Table #PobT
End

If(OBJECT_ID('tempdb..#OprasenoT') Is Not Null)
Begin
    Drop Table #OprasenoT
End

If(OBJECT_ID('tempdb..#SkartT') Is Not Null)
Begin
    Drop Table #SkartT
End

If(OBJECT_ID('tempdb..#OpT') Is Not Null)
Begin
    Drop Table #OpT
End

If(OBJECT_ID('tempdb..#PovadT') Is Not Null)
Begin
    Drop Table #PovadT
End

create table #OsT
(
    CIKLUS int,
	Os int
)

insert into #OsT
select p.CIKLUS, count(*) as Os
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.CIKLUS


create table #PobT
(
    CIKLUS int,
	Pob int
)

insert into #PobT
select p.CIKLUS, count(*) as Pob
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Pobacaj pob on pob.KrmacaId = k.Id 
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.CIKLUS


create table #OprasenoT
(
    CIKLUS int,
	Opraseno int,
	Zivo int,
	Mrtvo int
)

insert into #OprasenoT
select p.CIKLUS, count(*) as Opraseno, SUM(pr.ZIVO) as Zivo, SUM(pr.MRTVO) as Mrtvo
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.CIKLUS


create table #SkartT
(
    CIKLUS int,
	Skart int
)

insert into #SkartT
select p.CIKLUS, count(*) as Skart
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Skart s on s.KrmacaId = k.Id
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.CIKLUS


create table #OpT
(
    CIKLUS int,
	Op int
)

insert into #OpT
select p.CIKLUS, count(*) as Op
from Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo
group by p.CIKLUS


create table #PovadT
(
    CIKLUS int,
	Povad int
)

insert into #PovadT
select p.CIKLUS, count(*) as Povad
from  Pripust p
join Krmaca k on p.KrmacaId = k.Id
join Prasenje pr on pr.KrmacaId = k.Id and pr.DAT_PRAS > p.DAT_PRIP
where p.DAT_PRIP >= @dateTimeFrom and p.DAT_PRIP <= @dateTimeTo and p.RAZL_P = 2
group by p.CIKLUS

select 
os.CIKLUS as Ciklus,
os.Os as Osemenjeno,
pov.Povad as Povad,
os.Os  - o.Opraseno as Ne_op,
s.Skart as Isklj,
pob.Pob as Pobac,
0 as Prod,
op.Op as Opraseno,
op.Op / os.Os * 100  as Proc_opr,
o.Zivo / op.Op as Zivo,
o.Mrtvo / op.Op as Mrtvo,
(o.Zivo + o.Mrtvo) / op.Op as Uk_pras
from #OsT os
join #PobT pob on pob.CIKLUS = os.CIKLUS
join #SkartT s on s.CIKLUS = os.CIKLUS
join #OprasenoT o on o.CIKLUS = os.CIKLUS
join #OpT op on op.CIKLUS = os.CIKLUS
join #PovadT pov on pov.CIKLUS = os.CIKLUS
END
