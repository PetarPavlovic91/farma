/****** Script for SelectTopNRows command from SSMS  ******/
insert into FarmaProduction.dbo.Klasa
(
[POL]
      ,[T_GRLA]
      ,[MBR_GRLA]
      ,[GODINA]
      ,[KLASA]
      ,[KL_P]
      ,[EKST]
      ,[RED]
      ,[DAT_SMOTRE]
      ,[DateCreated]
      ,[DateModified]
)
SELECT [POL]
      ,[T_GRLA]
      ,[MBR_GRLA]
      ,[GODINA]
      ,[KLASA]
      ,[KL_P]
      ,[EKST]
      ,[RED]
      ,[DAT_SMOTRE]
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniPigs].[dbo].[klase]