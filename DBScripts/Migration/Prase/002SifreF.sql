insert into FarmaProduction.dbo.SifreF
(
		[SIFARNIK]
      ,[SIFRA]
      ,[NAZIV]
      ,[DateCreated]
      ,[DateModified]
)
SELECT [SIFARNIK]
      ,[SIFRA]
      ,[NAZIV]
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniPigs].[dbo].[SifreF]