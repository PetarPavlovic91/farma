insert into FarmaProduction.dbo.Prase
(
	[T_PRAS]
    ,[POL]
    ,[GN]
    ,[DAT_ROD]
    ,[IZ_LEGLA]
    ,[PRIPLOD]
    ,[MBR_PRAS]
    ,[D_SKART]
    ,[RAZL_SK]
    ,[S_L]
    ,[S_D]
    ,[PRAS_BROJ]
    ,[MBR_PRAS1]
    ,[KrmacaId]
    ,[NerastId]
    ,[DateCreated]
    ,[DateModified]
	,[MBR_MAJKE]
	,[MBR_OCA]
)
SELECT 
	 p.[T_PRAS]
    ,p.[POL]
    ,p.[GN]
    ,p.[DAT_ROD]
    ,p.[IZ_LEGLA]
    ,p.[PRIPLOD]
    ,p.[MBR_PRAS]
    ,p.[D_SKART]
    ,p.[RAZL_SK]
    ,p.[S_L]
    ,p.[S_D]
    ,p.[PRAS_BROJ]
    ,p.[MBR_PRAS1]
    ,k.Id
    ,n.Id
    ,GETDATE()
    ,GETDATE()
	,p.MBR_MAJKE
	,p.MBR_OCA
  FROM [ImportovaniPigs].[dbo].[PRASE] p
  left join FarmaProduction.dbo.Krmaca k on k.MBR_KRM = p.MBR_MAJKE
  left join FarmaProduction.dbo.Nerast n on n.MBR_NER = p.MBR_OCA
  where k.Id is not null and n.Id is not null

  