insert into FarmaProduction.dbo.Pobacaj
(
[DAT_POBAC]
      ,[RB]
      ,[CIKLUS]
      ,[REFK]
      ,[KrmacaId]
      ,[DateCreated]
      ,[DateModified]
)
SELECT p.[DAT_POBAC]
      ,p.[RB]
      ,p.[CIKLUS]
      ,p.[REFK]
	  ,k.Id
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniPigs].[dbo].[POBACAJ] p 
  join FarmaProduction.dbo.Krmaca k on k.MBR_KRM = p.MBR_KRM