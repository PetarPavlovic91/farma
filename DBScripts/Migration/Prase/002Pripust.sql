insert into FarmaProduction.dbo.Pripust
(
[DAT_PRIP]
,[RB]
,[CIKLUS]
,[RBP]
,[RAZL_P]
,[OBJ]
,[BOX]
,[PV]
,[OSEM]
,[OCENA]
,[PV2]
,[OSEM2]
,[OCENA2]
,[PRIPUSNICA]
,[PRIMEDBA]
,[REFK]
,[Nerast2Id]
,[NerastId]
,[KrmacaId]
,[DateCreated]
,[DateModified]
)
select 
[DAT_PRIP]
,[RB]
,p.[CIKLUS]
,[RBP]
,[RAZL_P]
,[OBJ]
,[BOX]
,p.[PV]
,[OSEM]
,[OCENA]
,[PV2]
,[OSEM2]
,[OCENA2]
,[PRIPUSNICA]
,[PRIMEDBA]
,[REFK]
,n2.Id
,n.Id
,k.Id
,GETDATE()
,GETDATE()
from [ImportovaniPigs].[dbo].PRIPUST p
join FarmaProduction.dbo.Krmaca k on k.MBR_KRM = p.MBR_KRM
left join FarmaProduction.dbo.Nerast n on n.MBR_NER = p.MBR_NER
left join FarmaProduction.dbo.Nerast n2 on n2.MBR_NER = p.MBR_NER2
