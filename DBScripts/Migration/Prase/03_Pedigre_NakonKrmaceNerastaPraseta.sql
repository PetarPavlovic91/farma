insert into FarmaProduction.dbo.Pedigre
(
		[T_GRLA]
      ,[MBR_GRLA]
      ,[POL]
      ,[MB_OCA]
      ,[MB_MAJKE]
      ,[MBO_DEDE]
      ,[MBO_BABE]
      ,[MBM_DEDE]
      ,[MBM_BABE]
      ,[MBOD_PRAD]
      ,[MBOD_PRAB]
      ,[MBOB_PRAD]
      ,[MBOB_PRAB]
      ,[MBMD_PRAD]
      ,[MBMD_PRAB]
      ,[MBMB_PRAD]
      ,[MBMB_PRAB]
      ,[HR_GRLA]
      ,[HR_OCA]
      ,[HR_MAJKE]
      ,[HRO_DEDE]
      ,[HRO_BABE]
      ,[HRM_DEDE]
      ,[HRM_BABE]
      ,[HROD_PRAD]
      ,[HROD_PRAB]
      ,[HROB_PRAD]
      ,[HROB_PRAB]
      ,[HRMD_PRAD]
      ,[HRMD_PRAB]
      ,[HRMB_PRAD]
      ,[HRMB_PRAB]
      ,[DateCreated]
      ,[DateModified]
      ,[KrmacaId]
)
SELECT p.[T_GRLA]
      ,p.[MBR_GRLA]
      ,p.[POL]
      ,p.[MB_OCA]
      ,p.[MB_MAJKE]
      ,p.[MBO_DEDE]
      ,p.[MBO_BABE]
      ,p.[MBM_DEDE]
      ,p.[MBM_BABE]
      ,p.[MBOD_PRAD]
      ,p.[MBOD_PRAB]
      ,p.[MBOB_PRAD]
      ,p.[MBOB_PRAB]
      ,p.[MBMD_PRAD]
      ,p.[MBMD_PRAB]
      ,p.[MBMB_PRAD]
      ,p.[MBMB_PRAB]
      ,p.[HR_GRLA]
      ,p.[HR_OCA]
      ,p.[HR_MAJKE]
      ,p.[HRO_DEDE]
      ,p.[HRO_BABE]
      ,p.[HRM_DEDE]
      ,p.[HRM_BABE]
      ,p.[HROD_PRAD]
      ,p.[HROD_PRAB]
      ,p.[HROB_PRAD]
      ,p.[HROB_PRAB]
      ,p.[HRMD_PRAD]
      ,p.[HRMD_PRAB]
      ,p.[HRMB_PRAD]
      ,p.[HRMB_PRAB]
      ,GETDATE()
      ,GETDATE()
      ,k.Id
  FROM [ImportovaniPigs].[dbo].[PEDIGRE] p
  left join FarmaProduction.dbo.Krmaca k on k.MBR_KRM = p.MBR_GRLA

  Update p
  set p.NerastId = n.Id 
  from FarmaProduction.dbo.Pedigre p
  left join FarmaProduction.dbo.Nerast n on n.MBR_NER = p.MBR_GRLA

  Update p
  set p.OdabirId = o.Id 
  from FarmaProduction.dbo.Pedigre p
  left join FarmaProduction.dbo.Odabir o on o.MBR_TEST = p.MBR_GRLA
  
  Update p
  set p.TestId = t.Id 
  from FarmaProduction.dbo.Pedigre p
  left join FarmaProduction.dbo.Test t on t.MBR_TEST = p.MBR_GRLA