insert into FarmaProduction.dbo.Zaluc
(
      [DAT_ZAL]
      ,[RB]
      ,[PARIT]
      ,[OBJ]
      ,[BOX]
      ,[PZ]
      ,[RAZ_PZ]
      ,[ZALUC]
      ,[TEZ_ZAL]
      ,[ZDR_ST]
      ,[OL_Z]
      ,[RBZ]
      ,[DOJARA]
      ,[REFK]
      ,[KrmacaId]
      ,[DateCreated]
      ,[DateModified]
)
SELECT z.[DAT_ZAL]
      ,z.[RB]
      ,z.[PARIT]
      ,z.[OBJ]
      ,z.[BOX]
      ,z.[PZ]
      ,z.[RAZ_PZ]
      ,z.[ZALUC]
      ,z.[TEZ_ZAL]
      ,z.[ZDR_ST]
      ,z.[OL_Z]
      ,z.[RBZ]
      ,z.[DOJARA]
      ,z.[REFK]
	  ,k.Id
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniPigs].[dbo].[ZALUC] z
  join FarmaProduction.dbo.Krmaca k on k.MBR_KRM = z.MBR_KRM
