insert into FarmaProduction.dbo.Sifra
(
[SIFRA]
      ,[NAZIV]
      ,[OPIS]
      ,[DUZINA]
      ,[DateCreated]
      ,[DateModified]
)
SELECT [SIFRA]
      ,[NAZIV]
      ,[OPIS]
      ,[DUZINA]
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniPigs].[dbo].[sifre]