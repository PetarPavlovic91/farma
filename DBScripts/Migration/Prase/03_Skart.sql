insert into FarmaProduction.dbo.Skart
(
[D_SKART]
      ,[RB]
      ,[RAZL_SK]
      ,[REFK]
      ,[DateCreated]
      ,[DateModified]
      ,[NerastId]
      ,[KrmacaId]
	  ,[TestId]
	  ,[OdabirId]
	  ,[PraseId]
)
select 
MAX(s.[D_SKART])
,MAX(s.[RB])
,MAX(s.[RAZL_SK])
,MAX(s.[REFK])
,GETDATE()
,GETDATE()
,MAX(n.Id)
,MAX(k.Id)
,MAX(t.Id)
,MAX(o.Id)
,MAX(p.Id)
from [ImportovaniPigs].[dbo].SKART s
left join FarmaProduction.dbo.Krmaca k on k.MBR_KRM = s.MBR_GRLA
left join FarmaProduction.dbo.Nerast n on n.MBR_NER = s.MBR_GRLA
left join FarmaProduction.dbo.Test t on t.MBR_TEST = s.MBR_GRLA
left join FarmaProduction.dbo.Odabir o on o.MBR_TEST = s.MBR_GRLA
left join FarmaProduction.dbo.Prase p on p.MBR_PRAS = s.MBR_GRLA
GROUP BY s.MBR_GRLA
