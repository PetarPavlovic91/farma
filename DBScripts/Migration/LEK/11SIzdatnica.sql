insert into [FarmaProduction].[dbo].[SIzdatnica]
(
	   [BR_DOC]
      ,[RB]
      ,[Cena]
      ,[Kolicina]
      ,[ArtiklId]
      ,[VrstaTroskaId]
      ,[DateCreated]
      ,[DateModified]
      ,[ZIzdatnicaId]
	  )
SELECT [BR_DOK]
	  ,[RB]
      ,[CENA]
      ,[KOLICINA]
      ,a.Id
      ,v.Id
	  ,GETDATE()
	  ,GETDATE()
	  ,z.Id
  FROM [ImportovaniLekovi].[dbo].[SIZDAT] s
  join [FarmaProduction].[dbo].[Artikl] a on a.SifraArtikla = s.ARTIKL
  join [FarmaProduction].[dbo].[VrstaTroska] v on v.VT = s.VT
  join [FarmaProduction].[dbo].[ZIzdatnica] z on z.BR_DOC = s.BR_DOK
  where z.Datum >= '01-01-2022'

