insert into [FarmaProduction].[dbo].[ZIzdatnica]
(
	   [BR_DOC]
      ,[Datum]
      ,[DatumTrebovanja]
      ,[BR_TREB]
      ,[VeterinarskiTehnicarId]
      ,[DateCreated]
      ,[DateModified]
	  )
SELECT z.[BR_DOK]
      ,z.[DATUM]
      ,z.[DAT_TREB]
      ,z.[BR_TREB]
	  ,v.Id
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniLekovi].[dbo].[ZIZDAT] z
  join [FarmaProduction].[dbo].VeterinarskiTehnicar v on v.VET = z.VET
  where z.Datum >= '01-01-2022'
