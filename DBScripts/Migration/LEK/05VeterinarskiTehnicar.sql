insert into Farma_Lekovi.dbo.VeterinarskiTehnicar
(
      [VET]
      ,[Naziv]
      ,[Mesto]
      ,[Ulica]
      ,[Telefon]
      ,[DateCreated]
      ,[DateModified]
)
SELECT [VET]
      ,[NAZIV]
      ,[MESTO]
      ,[ULICA]
      ,[TELEFON]
	  ,GETDATE()
	  ,GETDATE()
  FROM [FarmaProduction01102023].[dbo].VeterinarskiTehnicar