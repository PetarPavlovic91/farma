INSERT INTO [FarmaProduction].[dbo].[Kartica]
           ([Datum]
           ,[BR_DOK]
           ,[U_I]
           ,[Kolicina]
           ,[Cena]
           ,[VrstaDokumentaId]
           ,[VeterinarskiTehnicarId]
           ,[ArtiklId]
           ,[VrstaTroskaId]
           ,[DateCreated]
           ,[DateModified]
           ,[PartnerId]
           ,[SPrijemnicaId]
           ,[SIzdatnicaId])
SELECT k.[DATUM]
      ,k.[BR_DOK]
      ,k.[U_I]
      ,k.[KOLICINA]
      ,k.[CENA]
	  ,vd.Id
	  ,vet.Id
      ,a.Id
      ,vt.Id
      ,GETDATE()
	  ,GETDATE()
      ,p.Id
	  ,sp.Id
	  ,si.Id
  FROM [ImportovaniLekovi].[dbo].[KARTICA] k
  left join [FarmaProduction].[dbo].[VrstaDokumenta] vd on vd.VD = k.VD
  join [FarmaProduction].[dbo].[Artikl] a on a.SifraArtikla = k.ARTIKL
  left join [FarmaProduction].[dbo].[VrstaTroska] vt on vt.VT = k.VT
  left join [FarmaProduction].[dbo].[VeterinarskiTehnicar] vet on vet.VET = k.VET
  left join [FarmaProduction].[dbo].[Partner] p on p.SIFPP = k.VEZA
  left join [FarmaProduction].[dbo].[SIzdatnica] si on si.BR_DOC = k.BR_DOK and a.Id = si.ArtiklId and k.U_I = 'I'
  left join [FarmaProduction].[dbo].[SPrijemnica] sp on sp.BR_DOC = k.BR_DOK and a.Id = sp.ArtiklId and k.U_I = 'U'
  where k.DATUM >= '01-01-2022'
GO
