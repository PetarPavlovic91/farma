insert into Farma_Lekovi.dbo.Artikl
(
[SifraArtikla]
      ,[Naziv]
      ,[GR_NAB]
      ,[PocetnoStanje]
      ,[Ulaz]
      ,[Izlaz]
      ,[Stanje]
      ,[PR_CENA]
      ,[JedinicaMereId]
      ,[VrstaArtiklaId]
      ,[DateCreated]
      ,[DateModified]
)
SELECT a.SifraArtikla
      ,a.[NAZIV]
      ,[GR_NAB]
      ,0
      ,0
      ,0
      ,0
      ,0
      ,j.Id
      ,va.Id
	  ,GETDATE()
	  ,GETDATE()
  FROM [FarmaProduction01102023].[dbo].Artikl a
  join [FarmaProduction01102023].[dbo].JedinicaMere jmO on jmO.Id = a.JedinicaMereId
  join [FarmaProduction01102023].[dbo].VrstaArtikla vrO on vrO.Id = a.VrstaArtiklaId
  join Farma_Lekovi.dbo.JedinicaMere j on j.Naziv = jmO.Naziv
  join Farma_Lekovi.dbo.VrstaArtikla va on va.VGP = vrO.VGP 
