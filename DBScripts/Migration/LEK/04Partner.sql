insert into Farma_Lekovi.dbo.Partner
(
      [SIFPP]
      ,[Naziv]
      ,[Adresa]
      ,[Mesto]
      ,[ZiroRacun]
      ,[Telefon]
      ,[Saradnik]
      ,[DateCreated]
      ,[DateModified]
)
SELECT [SIFPP]
      ,[NAZIV]
      ,[ADRESA]
      ,[MESTO]
      ,[ZiroRacun]
      ,[TELEFON]
      ,[SARADNIK]
	  ,GETDATE()
	  ,GETDATE()
  FROM [FarmaProduction01102023].[dbo].Partner