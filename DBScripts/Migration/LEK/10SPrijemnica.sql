insert into [FarmaProduction].[dbo].[SPrijemnica]
(
	   [BR_DOC]
      ,[RB]
      ,[Cena]
      ,[Kolicina]
      ,[ArtiklId]
      ,[DateCreated]
      ,[DateModified]
      ,[ZPrijemnicaId]
	  )
SELECT [BR_DOK]
      ,[RB]
      ,[CENA]
      ,[KOLICINA]
      ,a.Id
	  ,GETDATE()
	  ,GETDATE()
	  ,z.Id
  FROM [ImportovaniLekovi].[dbo].[SPRIJEM] s
  join [FarmaProduction].[dbo].[Artikl] a on a.SifraArtikla = s.ARTIKL
  join [FarmaProduction].[dbo].[ZPrijemnica] z on z.BR_DOC = s.BR_DOK
  where z.Datum >= '01-01-2022'