insert into [FarmaProduction].[dbo].[ZPrijemnica]
(
	   [BR_DOC]
      ,[Datum]
      ,[DokumentDobavljaca]
      ,[PartnerId]
      ,[DateCreated]
      ,[DateModified]
	  )
SELECT z.[BR_DOK]
      ,z.[DATUM]
      ,z.[DOK_DOBAV]
	  ,p.Id
	  ,GETDATE()
	  ,GETDATE()
  FROM [ImportovaniLekovi].[dbo].[ZPRIJEM] z
  join [FarmaProduction].[dbo].[Partner] p on p.SIFPP = z.SIFPP
  where z.Datum >= '01-01-2022'